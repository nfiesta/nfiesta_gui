﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Schema nfiesta_results,
            /// NIL output data for web presentation.
            /// </summary>
            public class ResultsSchema
                : IDatabaseSchema
            {

                #region Constants

                /// <summary>
                /// Schema name,
                /// contains default value,
                /// value is not constant now,
                /// extension can be created with different schemas
                /// </summary>
                public static string Name = "nfiesta_results";

                /// <summary>
                /// Extension name
                /// </summary>
                public const string ExtensionName = "nfiesta_results";

                /// <summary>
                /// Extension version
                /// </summary>
                public static readonly ExtensionVersion ExtensionVersion =
                    new(version: "1.0.15");

                #endregion Constants


                #region Private Fields

                #region General

                /// <summary>
                /// Database
                /// </summary>
                private IDatabase database;

                /// <summary>
                /// Omitted tables
                /// </summary>
                private List<string> omittedTables;

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// Table of area domains.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.AreaDomainList cAreaDomain;

                /// <summary>
                /// Table of area domain categories.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.AreaDomainCategoryList cAreaDomainCategory;

                /// <summary>
                /// Table of area domain categories, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList cAreaDomainCategoryLanguage;

                /// <summary>
                /// Table of area domains, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.AreaDomainLanguageList cAreaDomainLanguage;

                /// <summary>
                /// Table of estimate types.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimateTypeList cEstimateType;

                /// <summary>
                /// Table of estimation cells.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimationCellList cEstimationCell;

                /// <summary>
                /// Table of estimate cell collections.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimationCellCollectionList cEstimationCellCollection;

                /// <summary>
                /// Table of estimation cell collections, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList cEstimationCellCollectionLanguage;

                /// <summary>
                /// Table of area estimation cells, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimationCellLanguageList cEstimationCellLanguage;

                /// <summary>
                /// Lookup table with estimation periods specifically bounded with two dates (begin, end).
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimationPeriodList cEstimationPeriod;

                /// <summary>
                /// Table of estimation periods, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList cEstimationPeriodLanguage;

                /// <summary>
                /// Table of gui headers.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.GuiHeaderList cGuiHeader;

                /// <summary>
                /// Table of gui headers for more foreing languages.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList cGuiHeaderLanguage;

                /// <summary>
                /// Table of languages.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.LanguageList cLanguage;

                /// <summary>
                /// Lookup table with named aggregated sets of panels and corresponding reference year sets.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList cPanelRefYearSetGroup;

                /// <summary>
                /// Table of panel refyearset groups, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList cPanelRefYearSetGroupLanguage;

                /// <summary>
                /// Table of phase estimate types.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList cPhaseEstimateType;

                /// <summary>
                /// Table of sub populations.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.SubPopulationList cSubPopulation;

                /// <summary>
                /// Table of sub population categories.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.SubPopulationCategoryList cSubPopulationCategory;

                /// <summary>
                /// Table of sub population categories, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList cSubPopulationCategoryLanguage;

                /// <summary>
                /// Table of sub populations, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.SubPopulationLanguageList cSubPopulationLanguage;

                /// <summary>
                /// Table of topics.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.TopicList cTopic;

                /// <summary>
                /// Table of topics, language versions.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.TopicLanguageList cTopicLanguage;

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// Mapping table of area domain categories.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList cmAreaDomainCategory;

                /// <summary>
                /// Mapping between target variable (total, ratio) and topic.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList cmResultToTopic;

                /// <summary>
                /// Mapping table of sub population categories.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList cmSubPopulationCategory;

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// Table of areas, contains geometries.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.CellList faCell;

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// Table of target variables.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.TargetVariableList cTargetVariable;

                /// <summary>
                /// Table of estimate configurations (configuration of total or combination of two totals in case of ratio).
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimateConfList tEstimateConf;

                /// <summary>
                /// Maping table for table c_estimation_cell. Storing hierarchy of cells.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList tEstimationCellHierarchy;

                /// <summary>
                /// Table with the final estimates.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.ResultList tResult;

                /// <summary>
                /// Table of result groups.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.ResultGroupList tResultGroup;

                /// <summary>
                /// Table of total estimate configurations (only totals with defined estimation cell).
                /// </summary>
                private ZaJi.NfiEstaPg.Results.TotalEstimateConfList tTotalEstimateConf;

                /// <summary>
                /// Table with available combinations of variables (target/aux) and sub-populations/area domains.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.VariableList tVariable;

                /// <summary>
                /// Table of variable hierarchies.
                /// </summary>
                private ZaJi.NfiEstaPg.Results.VariableHierarchyList tVariableHierarchy;

                #endregion Data Tables

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database</param>
                public ResultsSchema(NfiEstaDB database)
                {

                    #region General

                    Database = database;

                    OmittedTables = [];

                    #endregion General


                    #region Lookup Tables

                    CAreaDomain = new ZaJi.NfiEstaPg.Results.AreaDomainList(
                        database: (NfiEstaDB)Database);

                    CAreaDomainCategory = new ZaJi.NfiEstaPg.Results.AreaDomainCategoryList(
                        database: (NfiEstaDB)Database);

                    CAreaDomainCategoryLanguage = new ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList(
                        database: (NfiEstaDB)Database);

                    CAreaDomainLanguage = new ZaJi.NfiEstaPg.Results.AreaDomainLanguageList(
                        database: (NfiEstaDB)Database);

                    CEstimateType = new ZaJi.NfiEstaPg.Results.EstimateTypeList(
                        database: (NfiEstaDB)Database);

                    CEstimationCell = new ZaJi.NfiEstaPg.Results.EstimationCellList(
                        database: (NfiEstaDB)Database);

                    CEstimationCellCollection = new ZaJi.NfiEstaPg.Results.EstimationCellCollectionList(
                        database: (NfiEstaDB)Database);

                    CEstimationCellCollectionLanguage = new ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList(
                        database: (NfiEstaDB)Database);

                    CEstimationCellLanguage = new ZaJi.NfiEstaPg.Results.EstimationCellLanguageList(
                        database: (NfiEstaDB)Database);

                    CEstimationPeriod = new ZaJi.NfiEstaPg.Results.EstimationPeriodList(
                        database: (NfiEstaDB)Database);

                    CEstimationPeriodLanguage = new ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList(
                        database: (NfiEstaDB)Database);

                    CGuiHeader = new ZaJi.NfiEstaPg.Results.GuiHeaderList(
                        database: (NfiEstaDB)Database);

                    CGuiHeaderLanguage = new ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList(
                        database: (NfiEstaDB)Database);

                    CLanguage = new ZaJi.NfiEstaPg.Results.LanguageList(
                        database: (NfiEstaDB)Database);

                    CPanelRefYearSetGroup = new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList(
                        database: (NfiEstaDB)Database);

                    CPanelRefYearSetGroupLanguage = new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList(
                        database: (NfiEstaDB)Database);

                    CPhaseEstimateType = new ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList(
                        database: (NfiEstaDB)Database);

                    CSubPopulation = new ZaJi.NfiEstaPg.Results.SubPopulationList(
                        database: (NfiEstaDB)Database);

                    CSubPopulationCategory = new ZaJi.NfiEstaPg.Results.SubPopulationCategoryList(
                        database: (NfiEstaDB)Database);

                    CSubPopulationCategoryLanguage = new ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList(
                        database: (NfiEstaDB)Database);

                    CSubPopulationLanguage = new ZaJi.NfiEstaPg.Results.SubPopulationLanguageList(
                        database: (NfiEstaDB)Database);

                    CTopic = new ZaJi.NfiEstaPg.Results.TopicList(
                        database: (NfiEstaDB)Database);

                    CTopicLanguage = new ZaJi.NfiEstaPg.Results.TopicLanguageList(
                        database: (NfiEstaDB)Database);

                    #endregion Lookup Tables


                    #region Mapping tables

                    CmAreaDomainCategory = new ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList(
                        database: (NfiEstaDB)Database);

                    CmResultToTopic = new ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList(
                        database: (NfiEstaDB)Database);

                    CmSubPopulationCategory = new ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList(
                        database: (NfiEstaDB)Database);

                    #endregion Mapping tables


                    #region Spatial data tables

                    FaCell = new ZaJi.NfiEstaPg.Results.CellList(
                        database: (NfiEstaDB)Database);

                    #endregion Spatial data tables


                    #region Data tables

                    CTargetVariable = new ZaJi.NfiEstaPg.Results.TargetVariableList(
                        database: (NfiEstaDB)Database);

                    TEstimateConf = new ZaJi.NfiEstaPg.Results.EstimateConfList(
                        database: (NfiEstaDB)Database);

                    TEstimationCellHierarchy = new ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList(
                        database: (NfiEstaDB)Database);

                    TResult = new ZaJi.NfiEstaPg.Results.ResultList(
                        database: (NfiEstaDB)Database);

                    TResultGroup = new ZaJi.NfiEstaPg.Results.ResultGroupList(
                        database: (NfiEstaDB)Database);

                    TTotalEstimateConf = new ZaJi.NfiEstaPg.Results.TotalEstimateConfList(
                        database: (NfiEstaDB)Database);

                    TVariable = new ZaJi.NfiEstaPg.Results.VariableList(
                        database: (NfiEstaDB)Database);

                    TVariableHierarchy = new ZaJi.NfiEstaPg.Results.VariableHierarchyList(
                        database: (NfiEstaDB)Database);

                    #endregion Data tables

                }

                #endregion Constructor


                #region Properties

                #region General

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                public string SchemaName
                {
                    get
                    {
                        return ResultsSchema.Name;
                    }
                }

                /// <summary>
                /// Database (not-null) (read-only)
                /// </summary>
                public IDatabase Database
                {
                    get
                    {
                        if (database == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (database is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        return database;
                    }
                    private set
                    {
                        if (value == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (value is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        database = value;
                    }
                }

                /// <summary>
                /// List of lookup tables (not-null) (read-only)
                /// </summary>
                public List<ILookupTable> LookupTables
                {
                    get
                    {
                        return
                        [
                            CAreaDomain,
                            CAreaDomainCategory,
                            CAreaDomainCategoryLanguage,
                            CAreaDomainLanguage,
                            CEstimateType,
                            CEstimationCell,
                            CEstimationCellCollection,
                            CEstimationCellCollectionLanguage,
                            CEstimationCellLanguage,
                            CEstimationPeriod,
                            CEstimationPeriodLanguage,
                            CGuiHeader,
                            CGuiHeaderLanguage,
                            CLanguage,
                            CPanelRefYearSetGroup,
                            CPanelRefYearSetGroupLanguage,
                            CPhaseEstimateType,
                            CSubPopulation,
                            CSubPopulationCategory,
                            CSubPopulationCategoryLanguage,
                            CSubPopulationLanguage,
                            CTopic,
                            CTopicLanguage
                        ];
                    }
                }

                /// <summary>
                /// List of mapping tables (not-null) (read-only)
                /// </summary>
                public List<IMappingTable> MappingTables
                {
                    get
                    {
                        return
                        [
                            CmAreaDomainCategory,
                            CmResultToTopic,
                            CmSubPopulationCategory
                        ];
                    }
                }

                /// <summary>
                /// List of spatial data tables (not-null) (read-only)
                /// </summary>
                public List<ISpatialTable> SpatialTables
                {
                    get
                    {
                        return
                        [
                            FaCell
                        ];
                    }
                }

                /// <summary>
                /// List of data tables (not-null) (read-only)
                /// </summary>
                public List<IDataTable> DataTables
                {
                    get
                    {
                        return
                        [
                            CTargetVariable,
                            TEstimateConf,
                            TEstimationCellHierarchy,
                            TResult,
                            TResultGroup,
                            TTotalEstimateConf,
                            TVariable,
                            TVariableHierarchy
                        ];
                    }
                }

                /// <summary>
                /// List of all tables (not-null) (read-only)
                /// </summary>
                public List<IDatabaseTable> Tables
                {
                    get
                    {
                        List<IDatabaseTable> tables = [];
                        tables.AddRange(collection: LookupTables);
                        tables.AddRange(collection: MappingTables);
                        tables.AddRange(collection: SpatialTables);
                        tables.AddRange(collection: DataTables);
                        tables.Sort(comparison: (a, b) => a.TableName.CompareTo(strB: b.TableName));
                        return tables;
                    }
                }

                /// <summary>
                /// Omitted tables
                /// </summary>
                public List<string> OmittedTables
                {
                    get
                    {
                        return omittedTables ?? [];
                    }
                    set
                    {
                        omittedTables = value ?? [];
                    }
                }

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// Table of area domains.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.AreaDomainList CAreaDomain
                {
                    get
                    {
                        return cAreaDomain ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAreaDomain = value ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of area domain categories.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.AreaDomainCategoryList CAreaDomainCategory
                {
                    get
                    {
                        return cAreaDomainCategory ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAreaDomainCategory = value ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of area domain categories, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList CAreaDomainCategoryLanguage
                {
                    get
                    {
                        return cAreaDomainCategoryLanguage ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAreaDomainCategoryLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of area domains, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.AreaDomainLanguageList CAreaDomainLanguage
                {
                    get
                    {
                        return cAreaDomainLanguage ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAreaDomainLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of estimate types.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimateTypeList CEstimateType
                {
                    get
                    {
                        return cEstimateType ??
                            new ZaJi.NfiEstaPg.Results.EstimateTypeList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimateType = value ??
                            new ZaJi.NfiEstaPg.Results.EstimateTypeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of estimation cells.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimationCellList CEstimationCell
                {
                    get
                    {
                        return cEstimationCell ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCell = value ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of estimate cell collections.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimationCellCollectionList CEstimationCellCollection
                {
                    get
                    {
                        return cEstimationCellCollection ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellCollectionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCellCollection = value ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellCollectionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of estimation cell collections, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList CEstimationCellCollectionLanguage
                {
                    get
                    {
                        return cEstimationCellCollectionLanguage ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCellCollectionLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of area estimation cells, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimationCellLanguageList CEstimationCellLanguage
                {
                    get
                    {
                        return cEstimationCellLanguage ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCellLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Lookup table with estimation periods specifically bounded with two dates (begin, end).
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimationPeriodList CEstimationPeriod
                {
                    get
                    {
                        return cEstimationPeriod ??
                            new ZaJi.NfiEstaPg.Results.EstimationPeriodList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationPeriod = value ??
                            new ZaJi.NfiEstaPg.Results.EstimationPeriodList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of estimation periods, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList CEstimationPeriodLanguage
                {
                    get
                    {
                        return cEstimationPeriodLanguage ??
                            new ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationPeriodLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of gui headers.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.GuiHeaderList CGuiHeader
                {
                    get
                    {
                        return cGuiHeader ??
                            new ZaJi.NfiEstaPg.Results.GuiHeaderList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cGuiHeader = value ??
                            new ZaJi.NfiEstaPg.Results.GuiHeaderList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of gui headers for more foreing languages.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList CGuiHeaderLanguage
                {
                    get
                    {
                        return cGuiHeaderLanguage ??
                            new ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cGuiHeaderLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of languages.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.LanguageList CLanguage
                {
                    get
                    {
                        return cLanguage ??
                            new ZaJi.NfiEstaPg.Results.LanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.LanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Lookup table with named aggregated sets of panels and corresponding reference year sets.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList CPanelRefYearSetGroup
                {
                    get
                    {
                        return cPanelRefYearSetGroup ??
                            new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cPanelRefYearSetGroup = value ??
                            new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of panel refyearset groups, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList CPanelRefYearSetGroupLanguage
                {
                    get
                    {
                        return cPanelRefYearSetGroupLanguage ??
                            new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cPanelRefYearSetGroupLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of phase estimate types.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList CPhaseEstimateType
                {
                    get
                    {
                        return cPhaseEstimateType ??
                            new ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cPhaseEstimateType = value ??
                            new ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of sub populations.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.SubPopulationList CSubPopulation
                {
                    get
                    {
                        return cSubPopulation ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cSubPopulation = value ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of sub population categories.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.SubPopulationCategoryList CSubPopulationCategory
                {
                    get
                    {
                        return cSubPopulationCategory ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cSubPopulationCategory = value ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of sub population categories, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList CSubPopulationCategoryLanguage
                {
                    get
                    {
                        return cSubPopulationCategoryLanguage ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cSubPopulationCategoryLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of sub populations, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.SubPopulationLanguageList CSubPopulationLanguage
                {
                    get
                    {
                        return cSubPopulationLanguage ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cSubPopulationLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of topics.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.TopicList CTopic
                {
                    get
                    {
                        return cTopic ??
                            new ZaJi.NfiEstaPg.Results.TopicList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cTopic = value ??
                            new ZaJi.NfiEstaPg.Results.TopicList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of topics, language versions.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.TopicLanguageList CTopicLanguage
                {
                    get
                    {
                        return cTopicLanguage ??
                            new ZaJi.NfiEstaPg.Results.TopicLanguageList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cTopicLanguage = value ??
                            new ZaJi.NfiEstaPg.Results.TopicLanguageList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// Mapping table of area domain categories.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList CmAreaDomainCategory
                {
                    get
                    {
                        return cmAreaDomainCategory ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmAreaDomainCategory = value ??
                            new ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Mapping between target variable (total, ratio) and topic.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList CmResultToTopic
                {
                    get
                    {
                        return cmResultToTopic ??
                            new ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmResultToTopic = value ??
                            new ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Mapping table of sub population categories.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList CmSubPopulationCategory
                {
                    get
                    {
                        return cmSubPopulationCategory ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmSubPopulationCategory = value ??
                            new ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// Table of areas, contains geometries.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.CellList FaCell
                {
                    get
                    {
                        return faCell ??
                            new ZaJi.NfiEstaPg.Results.CellList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        faCell = value ??
                            new ZaJi.NfiEstaPg.Results.CellList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// Table of target variables.
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Results.TargetVariableList CTargetVariable
                {
                    get
                    {
                        return cTargetVariable ??
                            new ZaJi.NfiEstaPg.Results.TargetVariableList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cTargetVariable = value ??
                            new ZaJi.NfiEstaPg.Results.TargetVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of estimate configurations (configuration of total or combination of two totals in case of ratio).
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimateConfList TEstimateConf
                {
                    get
                    {
                        return tEstimateConf ??
                            new ZaJi.NfiEstaPg.Results.EstimateConfList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tEstimateConf = value ??
                            new ZaJi.NfiEstaPg.Results.EstimateConfList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Maping table for table c_estimation_cell. Storing hierarchy of cells.
                /// </summary>
                public ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList TEstimationCellHierarchy
                {
                    get
                    {
                        return tEstimationCellHierarchy ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tEstimationCellHierarchy = value ??
                            new ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table with the final estimates.
                /// </summary>
                public ZaJi.NfiEstaPg.Results.ResultList TResult
                {
                    get
                    {
                        return tResult ??
                            new ZaJi.NfiEstaPg.Results.ResultList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tResult = value ??
                            new ZaJi.NfiEstaPg.Results.ResultList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of result groups.
                /// </summary>
                public ZaJi.NfiEstaPg.Results.ResultGroupList TResultGroup
                {
                    get
                    {
                        return tResultGroup ??
                            new ZaJi.NfiEstaPg.Results.ResultGroupList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tResultGroup = value ??
                            new ZaJi.NfiEstaPg.Results.ResultGroupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of total estimate configurations (only totals with defined estimation cell).
                /// </summary>
                public ZaJi.NfiEstaPg.Results.TotalEstimateConfList TTotalEstimateConf
                {
                    get
                    {
                        return tTotalEstimateConf ??
                            new ZaJi.NfiEstaPg.Results.TotalEstimateConfList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tTotalEstimateConf = value ??
                            new ZaJi.NfiEstaPg.Results.TotalEstimateConfList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table with available combinations of variables (target/aux) and sub-populations/area domains.
                /// </summary>
                public ZaJi.NfiEstaPg.Results.VariableList TVariable
                {
                    get
                    {
                        return tVariable ??
                            new ZaJi.NfiEstaPg.Results.VariableList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tVariable = value ??
                            new ZaJi.NfiEstaPg.Results.VariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Table of variable hierarchies.
                /// </summary>
                public ZaJi.NfiEstaPg.Results.VariableHierarchyList TVariableHierarchy
                {
                    get
                    {
                        return tVariableHierarchy ??
                            new ZaJi.NfiEstaPg.Results.VariableHierarchyList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tVariableHierarchy = value ??
                            new ZaJi.NfiEstaPg.Results.VariableHierarchyList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Data Tables


                #region Differences

                /// <summary>
                /// Differences in database schema (not-null) (read-only)
                /// </summary>
                public List<string> Differences
                {
                    get
                    {
                        List<string> result = [];
                        Database.Postgres.Catalog.Load();

                        DBExtension dbExtension = Database.Postgres.Catalog.Extensions.Items
                            .Where(a => a.Name == ResultsSchema.ExtensionName)
                            .FirstOrDefault();

                        if (dbExtension == null)
                        {
                            result.Add(item: String.Concat(
                                $"Extension {ResultsSchema.ExtensionName} does not exist in database."));
                            return result;
                        }

                        ExtensionVersion dbExtensionVersion = new(
                             version: dbExtension.Version);
                        if (!dbExtensionVersion.Equals(obj: ResultsSchema.ExtensionVersion))
                        {
                            result.Add(item: String.Concat(
                                $"Database extension {ResultsSchema.ExtensionName} is in version {dbExtension.Version}. ",
                                $"But the version {ResultsSchema.ExtensionVersion} was expected."));
                        }

                        DBSchema dbSchema = Database.Postgres.Catalog.Schemas.Items
                            .Where(a => a.Name == ResultsSchema.Name)
                            .FirstOrDefault();

                        if (dbSchema == null)
                        {
                            result.Add(item: String.Concat(
                                $"Schema {ResultsSchema.Name} does not exist in database."));
                            return result;
                        }

                        foreach (DBTable dbTable in dbSchema.Tables.Items.OrderBy(a => a.Name))
                        {
                            if (OmittedTables.Contains(item: dbTable.Name))
                            {
                                continue;
                            }

                            IDatabaseTable iTable = ((NfiEstaDB)Database).SResults.Tables
                                .Where(a => a.TableName == dbTable.Name)
                                .FirstOrDefault();

                            if (iTable == null)
                            {
                                result.Add(item: String.Concat(
                                    $"Class for table {dbTable.Name} does not exist in dll library."));
                            }
                        }

                        foreach (IDatabaseTable iTable in ((NfiEstaDB)Database).SResults.Tables.OrderBy(a => a.TableName))
                        {
                            DBTable dbTable = dbSchema.Tables.Items
                                    .Where(a => a.Name == iTable.TableName)
                                    .FirstOrDefault();

                            if (dbTable == null)
                            {
                                if (iTable.Columns.Values.Where(a => a.Elemental).Any())
                                {
                                    result.Add(item: String.Concat(
                                        $"Table {iTable.TableName} does not exist in database."));
                                }
                                else
                                {
                                    // Table doesn't contain any column that is loaded from database.
                                }
                            }
                            else
                            {
                                foreach (DBColumn dbColumn in dbTable.Columns.Items.OrderBy(a => a.Name))
                                {
                                    ColumnMetadata iColumn = iTable.Columns.Values
                                        .Where(a => a.Elemental)
                                        .Where(a => a.DbName == dbColumn.Name)
                                        .FirstOrDefault();

                                    if (iColumn == null)
                                    {
                                        result.Add(item: String.Concat(
                                            $"Column {dbTable.Name}.{dbColumn.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (ColumnMetadata iColumn in iTable.Columns.Values.Where(a => a.Elemental).OrderBy(a => a.DbName))
                                {
                                    DBColumn dbColumn = dbTable.Columns.Items
                                        .Where(a => a.Name == iColumn.DbName)
                                        .FirstOrDefault();

                                    if (dbColumn == null)
                                    {
                                        result.Add(item: String.Concat(
                                            $"Column {iTable.TableName}.{iColumn.DbName} does not exist in database."));
                                    }
                                    else
                                    {
                                        if (dbColumn.TypeName != iColumn.DbDataType)
                                        {
                                            result.Add(item: String.Concat(
                                                $"Column {iTable.TableName}.{iColumn.DbName} is type of {dbColumn.TypeName}. ",
                                                $"But type of {iColumn.DbDataType} was expected."));
                                        }

                                        if (dbColumn.NotNull != iColumn.NotNull)
                                        {
                                            string strDbNull = (bool)dbColumn.NotNull ? "has NOT NULL constraint" : "is NULLABLE";
                                            string strINull = iColumn.NotNull ? "NOT NULL contraint" : "NULLABLE";
                                            result.Add(String.Concat(
                                                $"Column {iTable.TableName}.{iColumn.DbName} {strDbNull}. ",
                                                $"But {strINull} was expected."));
                                        }
                                    }
                                }
                            }
                        }

                        Dictionary<string, string> iStoredProcedures = [];
                        foreach (
                            Type nestedType in typeof(ZaJi.NfiEstaPg.Results.RSFunctions)
                            .GetNestedTypes()
                            .AsEnumerable()
                            .OrderBy(a => a.FullName)
                            )
                        {
                            PropertyInfo pSignature = nestedType.GetProperty(name: "Signature");
                            PropertyInfo pName = nestedType.GetProperty(name: "Name");
                            if ((pSignature != null) && (pName != null))
                            {
                                // signature = null označeje uložené procedury,
                                // pro které není požadována implementace v databázi
                                // nekontrolují se
                                if (pSignature.GetValue(obj: null) != null)
                                {
                                    string a = pSignature.GetValue(obj: null).ToString();
                                    string b = pName.GetValue(obj: null).ToString();

                                    iStoredProcedures.TryAdd(key: a, value: b);
                                }
                            }
                        }

                        foreach (DBStoredProcedure dbStoredProcedure in dbSchema.StoredProcedures.Items.OrderBy(a => a.Name))
                        {
                            if (!iStoredProcedures.ContainsKey(key: dbStoredProcedure.Signature))
                            {
                                result.Add(item: String.Concat(
                                   $"Class for stored procedure {dbStoredProcedure.Name} does not exist in dll library."));
                            }
                        }

                        foreach (KeyValuePair<string, string> iStoredProcedure in iStoredProcedures)
                        {
                            DBStoredProcedure dbStoredProcedure = dbSchema.StoredProcedures.Items
                                    .Where(a => a.Signature == iStoredProcedure.Key)
                                    .FirstOrDefault();
                            if (dbStoredProcedure == null)
                            {
                                result.Add(item: String.Concat(
                                       $"Stored procedure {iStoredProcedure.Value} does not exist in database."));
                            }
                        }

                        return result;
                    }
                }

                #endregion Differences

                #endregion Properties


                #region Methods

                /// <summary>
                /// Returns the string that represents current object
                /// </summary>
                /// <returns>Returns the string that represents current object</returns>
                public override string ToString()
                {
                    return Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat($"Data schématu {ResultsSchema.Name}."),
                        LanguageVersion.International =>
                            String.Concat($"Data from schema {ResultsSchema.Name}."),
                        _ =>
                            String.Concat($"Data from schema {ResultsSchema.Name}."),
                    };
                }

                #endregion Methods


                #region Static Methods

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                [SupportedOSPlatform("windows")]
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    List<string> tableNames = null)
                {
                    System.Windows.Forms.FolderBrowserDialog dlg = new();

                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ExportData(
                            database: database,
                            fileFormat: fileFormat,
                            folder: dlg.SelectedPath,
                            tableNames: tableNames);
                    }
                }

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="folder">Folder for writing file with data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    string folder,
                    List<string> tableNames = null)
                {
                    List<ADatabaseTable> databaseTables =
                    [
                        // c_area_domain
                        new ZaJi.NfiEstaPg.Results.AreaDomainList(database: database),

                        // c_area_domain_category
                        new ZaJi.NfiEstaPg.Results.AreaDomainCategoryList(database: database),

                        // c_area_domain_category_language
                        new ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList(database: database),

                        // c_area_domain_language
                        new ZaJi.NfiEstaPg.Results.AreaDomainLanguageList(database: database),

                        // c_estimate_type
                        new ZaJi.NfiEstaPg.Results.EstimateTypeList(database: database),

                        // c_estimation_cell
                        new ZaJi.NfiEstaPg.Results.EstimationCellList(database: database),

                        // c_estimation_cell_collection
                        new ZaJi.NfiEstaPg.Results.EstimationCellCollectionList(database: database),

                        // c_estimation_cell_collection_language
                        new ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList(database: database),

                        // c_estimation_cell_language
                        new ZaJi.NfiEstaPg.Results.EstimationCellLanguageList(database: database),

                        // c_estimation_period
                        new ZaJi.NfiEstaPg.Results.EstimationPeriodList(database: database),

                        // c_estimation_period_language
                        new ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList(database: database),

                        // c_gui_header
                        new ZaJi.NfiEstaPg.Results.GuiHeaderList(database: database),

                        // c_gui_header_language
                        new ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList(database: database),

                        // c_language
                        new ZaJi.NfiEstaPg.Results.LanguageList(database: database),

                        // c_panel_refyearset_group
                        new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList(database: database),

                        // c_panel_refyearset_group_language
                        new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList(database: database),

                        // c_phase_estimate_type
                        new ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList(database: database),

                        // c_sub_population
                        new ZaJi.NfiEstaPg.Results.SubPopulationList(database: database),

                        // c_sub_population_category
                        new ZaJi.NfiEstaPg.Results.SubPopulationCategoryList(database: database),

                        // c_sub_population_category_language
                        new ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList(database: database),

                        // c_sub_population_language
                        new ZaJi.NfiEstaPg.Results.SubPopulationLanguageList(database: database),

                        // c_target_variable
                        new ZaJi.NfiEstaPg.Results.TargetVariableList(database: database),

                        // c_topic
                        new ZaJi.NfiEstaPg.Results.TopicList(database: database),

                        // c_topic_language
                        new ZaJi.NfiEstaPg.Results.TopicLanguageList(database: database),

                        // cm_area_domain_category
                        new ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList(database: database),

                        // cm_result2topic
                        new ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList(database: database),

                        // cm_sub_population_category
                        new ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList(database: database),

                        // f_a_cell
                        new ZaJi.NfiEstaPg.Results.CellList(database: database),

                        // t_estimate_conf
                        new ZaJi.NfiEstaPg.Results.EstimateConfList(database: database),

                        // t_estimation_cell_hierarchy
                        new ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList(database: database),

                        // t_result
                        new ZaJi.NfiEstaPg.Results.ResultList(database: database),

                        // t_result_group
                        new ZaJi.NfiEstaPg.Results.ResultGroupList(database: database),

                        // t_total_estimate_conf
                        new ZaJi.NfiEstaPg.Results.TotalEstimateConfList(database: database),

                        // t_variable
                        new ZaJi.NfiEstaPg.Results.VariableList(database: database),

                        // t_variable_hierarchy
                        new ZaJi.NfiEstaPg.Results.VariableHierarchyList(database: database)
                    ];

                    foreach (ADatabaseTable databaseTable in databaseTables)
                    {
                        if ((tableNames == null) ||
                             tableNames.Contains(item: databaseTable.TableName))
                        {
                            if (databaseTable is ALookupTable lookupTable)
                            {
                                lookupTable.ReLoad(extended: false);
                            }
                            else if (databaseTable is AMappingTable mappingTable)
                            {
                                mappingTable.ReLoad();
                            }
                            else if (databaseTable is ASpatialTable spatialTable)
                            {
                                spatialTable.ReLoad(withGeom: false);
                            }
                            else if (databaseTable is ADataTable dataTable)
                            {
                                dataTable.ReLoad();
                            }
                            else
                            {
                                throw new Exception(message: $"Argument {nameof(databaseTable)} unknown type.");
                            }

                            databaseTable.ExportData(
                                fileFormat: fileFormat,
                                folder: folder);
                        }
                    }
                }

                #endregion Static Methods

            }

        }
    }
}
