﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // c_gui_header_language

            /// <summary>
            /// GUI header, language version
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class GuiHeaderLanguage(
                GuiHeaderLanguageList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<GuiHeaderLanguageList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// GUI header identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColId.Name,
                            defaultValue: Int32.Parse(s: GuiHeaderLanguageList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Language identifier
                /// </summary>
                public int LanguageId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColLanguageId.Name,
                            defaultValue: Int32.Parse(s: GuiHeaderLanguageList.ColLanguageId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColLanguageId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Language object (read-only)
                /// </summary>
                public Language Language
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CLanguage[LanguageId];
                    }
                }


                /// <summary>
                /// GUI header identifier
                /// </summary>
                public int GuiHeaderId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColGuiHeaderId.Name,
                            defaultValue: Int32.Parse(s: GuiHeaderLanguageList.ColGuiHeaderId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColGuiHeaderId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// GUI header object (read-only)
                /// </summary>
                public GuiHeader GuiHeader
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CGuiHeader[GuiHeaderId];
                    }
                }


                /// <summary>
                /// Label of GUI header in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColLabelCs.Name,
                            defaultValue: GuiHeaderLanguageList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: GuiHeaderLanguageList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of GUI header in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return LabelCs;
                    }
                    set
                    {
                        LabelCs = value;
                    }
                }

                /// <summary>
                /// Extended label of GUI header in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label of GUI header in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return GuiHeader?.LabelEn;
                    }
                    set
                    {
                        if (GuiHeader != null)
                        {
                            GuiHeader.LabelEn = value;
                        }
                    }
                }

                /// <summary>
                /// Description of GUI header in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return GuiHeader?.DescriptionEn;
                    }
                    set
                    {
                        if (GuiHeader != null)
                        {
                            GuiHeader.DescriptionEn = value;
                        }
                    }
                }

                /// <summary>
                /// Extended label of GUI header in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not GuiHeaderLanguage Type, return False
                    if (obj is not GuiHeaderLanguage)
                    {
                        return false;
                    }

                    return
                        Id == ((GuiHeaderLanguage)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of GUI headers, language versions
            /// </summary>
            public class GuiHeaderLanguageList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_gui_header_language";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_gui_header_language";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam GUI hlaviček, jazykové verze";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of GUI headers, language versions";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "language", new ColumnMetadata()
                    {
                        Name = "language",
                        DbName = "language",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LANGUAGE",
                        HeaderTextEn = "LANGUAGE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "gui_header", new ColumnMetadata()
                    {
                        Name = "gui_header",
                        DbName = "gui_header",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_HEADER",
                        HeaderTextEn = "GUI_HEADER",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column language metadata
                /// </summary>
                public static readonly ColumnMetadata ColLanguageId = Cols["language"];

                /// <summary>
                /// Column gui_header metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiHeaderId = Cols["gui_header"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public GuiHeaderLanguageList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public GuiHeaderLanguageList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of GUI headers (read-only)
                /// </summary>
                public List<GuiHeaderLanguage> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new GuiHeaderLanguage(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// GUI header from list by identifier (read-only)
                /// </summary>
                /// <param name="id">GUI header identifier</param>
                /// <returns>GUI header from list by identifier (null if not found)</returns>
                public GuiHeaderLanguage this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new GuiHeaderLanguage(composite: this, data: a))
                                .FirstOrDefault<GuiHeaderLanguage>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public GuiHeaderLanguageList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new GuiHeaderLanguageList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new GuiHeaderLanguageList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
