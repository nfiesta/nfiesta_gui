﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_attribute_categories4target_variable

                #region FnGetAttributeCategoriesForTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_get_attribute_categories4target_variable.
                /// Function returns table with attribute categories (area domain and sub population categories)
                /// for given target variable and area domain/sub population.
                /// It also solves the right combination of numerator and denominator categories.
                /// </summary>
                public static class FnGetAttributeCategoriesForTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_attribute_categories4target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_lang integer, ",
                            $"_numerator_target_variable integer, ",
                            $"_numerator_area_domain integer DEFAULT NULL::integer, ",
                            $"_numerator_sub_population integer DEFAULT NULL::integer, ",
                            $"_denominator_target_variable integer DEFAULT NULL::integer, ",
                            $"_denominator_area_domain integer DEFAULT NULL::integer, ",
                            $"_denominator_sub_population integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"nominator_variable integer, ",
                            $"denominator_variable integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    nominator_variable AS numerator_variable,",
                                $"    denominator_variable,",
                                $"    label,",
                                $"    description,",
                                $"    label_en,",
                                $"    description_en",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@lang, ",
                                $"@numeratorTargetVariable, @numeratorAreaDomain, @numeratorSubPopulation, ",
                                $"@denominatorTargetVariable, @denominatorAreaDomain, @denominatorSubPopulation);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="lang"></param>
                    /// <param name="numeratorTargetVariable"></param>
                    /// <param name="numeratorAreaDomain"></param>
                    /// <param name="numeratorSubPopulation"></param>
                    /// <param name="denominatorTargetVariable"></param>
                    /// <param name="denominatorAreaDomain"></param>
                    /// <param name="denominatorSubPopulation"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> lang,
                        Nullable<int> numeratorTargetVariable,
                        Nullable<int> numeratorAreaDomain,
                        Nullable<int> numeratorSubPopulation,
                        Nullable<int> denominatorTargetVariable,
                        Nullable<int> denominatorAreaDomain,
                        Nullable<int> denominatorSubPopulation)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@lang",
                             newValue: Functions.PrepNIntArg(arg: lang));

                        result = result.Replace(
                             oldValue: "@numeratorTargetVariable",
                             newValue: Functions.PrepNIntArg(arg: numeratorTargetVariable));

                        result = result.Replace(
                             oldValue: "@numeratorAreaDomain",
                             newValue: Functions.PrepNIntArg(arg: numeratorAreaDomain));

                        result = result.Replace(
                             oldValue: "@numeratorSubPopulation",
                             newValue: Functions.PrepNIntArg(arg: numeratorSubPopulation));

                        result = result.Replace(
                             oldValue: "@denominatorTargetVariable",
                             newValue: Functions.PrepNIntArg(arg: denominatorTargetVariable));

                        result = result.Replace(
                             oldValue: "@denominatorAreaDomain",
                             newValue: Functions.PrepNIntArg(arg: denominatorAreaDomain));

                        result = result.Replace(
                             oldValue: "@denominatorSubPopulation",
                             newValue: Functions.PrepNIntArg(arg: denominatorSubPopulation));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_attribute_categories4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="lang"></param>
                    /// <param name="numeratorTargetVariable"></param>
                    /// <param name="numeratorAreaDomain"></param>
                    /// <param name="numeratorSubPopulation"></param>
                    /// <param name="denominatorTargetVariable"></param>
                    /// <param name="denominatorAreaDomain"></param>
                    /// <param name="denominatorSubPopulation"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> lang,
                        Nullable<int> numeratorTargetVariable,
                        Nullable<int> numeratorAreaDomain,
                        Nullable<int> numeratorSubPopulation,
                        Nullable<int> denominatorTargetVariable,
                        Nullable<int> denominatorAreaDomain,
                        Nullable<int> denominatorSubPopulation,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            lang: lang,
                            numeratorTargetVariable: numeratorTargetVariable,
                            numeratorAreaDomain: numeratorAreaDomain,
                            numeratorSubPopulation: numeratorSubPopulation,
                            denominatorTargetVariable: denominatorTargetVariable,
                            denominatorAreaDomain: denominatorAreaDomain,
                            denominatorSubPopulation: denominatorSubPopulation);

                        NpgsqlParameter pLang = new(
                          parameterName: "lang",
                          parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumeratorTargetVariable = new(
                            parameterName: "numeratorTargetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumeratorAreaDomain = new(
                           parameterName: "numeratorAreaDomain",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumeratorSubPopulation = new(
                          parameterName: "numeratorSubPopulation",
                          parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDenominatorTargetVariable = new(
                             parameterName: "denominatorTargetVariable",
                             parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDenominatorAreaDomain = new(
                           parameterName: "denominatorAreaDomain",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDenominatorSubPopulation = new(
                          parameterName: "denominatorSubPopulation",
                          parameterType: NpgsqlDbType.Integer);

                        if (lang != null)
                        {
                            pLang.Value = (int)lang;
                        }
                        else
                        {
                            pLang.Value = DBNull.Value;
                        }

                        if (numeratorTargetVariable != null)
                        {
                            pNumeratorTargetVariable.Value = (int)numeratorTargetVariable;
                        }
                        else
                        {
                            pNumeratorTargetVariable.Value = DBNull.Value;
                        }

                        if (numeratorAreaDomain != null)
                        {
                            pNumeratorAreaDomain.Value = (int)numeratorAreaDomain;
                        }
                        else
                        {
                            pNumeratorAreaDomain.Value = DBNull.Value;
                        }

                        if (numeratorSubPopulation != null)
                        {
                            pNumeratorSubPopulation.Value = (int)numeratorSubPopulation;
                        }
                        else
                        {
                            pNumeratorSubPopulation.Value = DBNull.Value;
                        }

                        if (denominatorTargetVariable != null)
                        {
                            pDenominatorTargetVariable.Value = (int)denominatorTargetVariable;
                        }
                        else
                        {
                            pDenominatorTargetVariable.Value = DBNull.Value;
                        }

                        if (denominatorAreaDomain != null)
                        {
                            pDenominatorAreaDomain.Value = (int)denominatorAreaDomain;
                        }
                        else
                        {
                            pDenominatorAreaDomain.Value = DBNull.Value;
                        }

                        if (denominatorSubPopulation != null)
                        {
                            pDenominatorSubPopulation.Value = (int)denominatorSubPopulation;
                        }
                        else
                        {
                            pDenominatorSubPopulation.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pLang, pNumeratorTargetVariable, pNumeratorAreaDomain, pNumeratorSubPopulation,
                                pDenominatorTargetVariable, pDenominatorAreaDomain, pDenominatorSubPopulation);
                    }

                    #endregion Methods

                }

                #endregion FnGetAttributeCategoriesForTargetVariable

            }

        }
    }
}