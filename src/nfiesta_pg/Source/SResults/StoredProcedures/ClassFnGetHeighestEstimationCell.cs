﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_heighest_estimation_cell

                #region FnGetHeighestEstimationCell

                /// <summary>
                /// Wrapper for stored procedure fn_get_heighest_estimation_cell.
                /// The function returns the heighest estimation cell from t_estimation_cell_hierarchy table for given estimation_cell.
                /// </summary>
                public static class FnGetHeighestEstimationCell
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_heighest_estimation_cell";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_cells integer[]; ",
                            $"returns: integer[]");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    array_to_string($SchemaName.$Name",
                                $"(@estimationCells),'{(char)59}','{Functions.StrNull}')::text AS $Name;{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationCells"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(List<Nullable<int>> estimationCells)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCells, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_heighest_estimation_cell
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCells"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> estimationCells,
                        NpgsqlTransaction transaction = null)
                    {
                        List<Nullable<int>> list = Execute(
                            database: database,
                            estimationCells: estimationCells,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        if (list == null)
                        {
                            return dt;
                        }

                        foreach (Nullable<int> val in list)
                        {
                            DataRow row = dt.NewRow();

                            Functions.SetNIntArg(
                                row: row,
                                name: Name,
                                val: val);

                            dt.Rows.Add(row: row);
                        }

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_heighest_estimation_cell
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCells"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static List<Nullable<int>> Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> estimationCells,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(estimationCells: estimationCells);

                        NpgsqlParameter pEstimationCells = new(
                            parameterName: "estimationCells",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (estimationCells != null)
                        {
                            pEstimationCells.Value = estimationCells.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCells.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationCells);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            return
                            Functions.StringToNIntList(
                                text: strResult,
                                separator: (char)59,
                                defaultValue: null);
                        }
                    }

                    #endregion Methods

                }

                #endregion FnGetHeighestEstimationCell

            }

        }
    }
}