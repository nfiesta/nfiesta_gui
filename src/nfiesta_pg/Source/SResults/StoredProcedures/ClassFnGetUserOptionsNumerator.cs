﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_user_options_numerator

                #region FnGetUserOptionsNumerator

                /// <summary>
                /// Wrapper for stored procedure fn_get_user_options_numerator.
                /// The function returns list of available IDs and labels of topic,
                /// estimation period, estimation cell collection, indicator, state or change,
                /// unit of indicator and unit of measurement
                /// from t_result_group table for given input arguments.
                /// </summary>
                public static class FnGetUserOptionsNumarator
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_user_options_numerator";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_jlang character varying, ",
                            $"_topic integer DEFAULT NULL::integer, ",
                            $"_num_estimation_period integer DEFAULT NULL::integer, ",
                            $"_num_estimation_cell_collection integer DEFAULT NULL::integer, ",
                            $"_num_id_group integer[] DEFAULT NULL::integer[], ",
                            $"_num_indicator boolean DEFAULT false, ",
                            $"_num_state boolean DEFAULT false, ",
                            $"_num_unit_of_measure boolean DEFAULT false, ",
                            $"_unit_of_measurement boolean DEFAULT false; ",
                            $"returns: ",
                            $"TABLE(",
                            $"res_id integer, ",
                            $"res_label text, ",
                            $"res_id_group integer[])");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    res_id,",
                            $"    res_label,",
                            $"    res_id_group",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@jlang, @topic, @numEstimationPeriod, @numEstimationCellCollection," +
                            $"@numIdGroup, @numIndicator, @numState, @numUnitOfMeasure, @unitOfMeasurement);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="jlang"></param>
                    /// <param name="topic"></param>
                    /// <param name="numEstimationPeriod"></param>
                    /// <param name="numEstimationCellCollection"></param>
                    /// <param name="numIdGroup"></param>
                    /// <param name="numIndicator"></param>
                    /// <param name="numState"></param>
                    /// <param name="numUnitOfMeasure"></param>
                    /// <param name="unitOfMeasurement"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string jlang,
                        Nullable<int> topic,
                        Nullable<int> numEstimationPeriod,
                        Nullable<int> numEstimationCellCollection,
                        List<Nullable<int>> numIdGroup,
                        Nullable<bool> numIndicator,
                        Nullable<bool> numState,
                        Nullable<bool> numUnitOfMeasure,
                        Nullable<bool> unitOfMeasurement)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@jlang",
                            newValue: Functions.PrepStringArg(arg: jlang));

                        result = result.Replace(
                            oldValue: "@topic",
                            newValue: Functions.PrepNIntArg(arg: topic));

                        result = result.Replace(
                            oldValue: "@numEstimationPeriod",
                            newValue: Functions.PrepNIntArg(arg: numEstimationPeriod));

                        result = result.Replace(
                            oldValue: "@numEstimationCellCollection",
                            newValue: Functions.PrepNIntArg(arg: numEstimationCellCollection));

                        result = result.Replace(
                            oldValue: "@numIdGroup",
                            newValue: Functions.PrepNIntArrayArg(args: numIdGroup, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@numIndicator",
                            newValue: Functions.PrepNBoolArg(arg: numIndicator));

                        result = result.Replace(
                            oldValue: "@numState",
                            newValue: Functions.PrepNBoolArg(arg: numState));

                        result = result.Replace(
                            oldValue: "@numUnitOfMeasure",
                            newValue: Functions.PrepNBoolArg(arg: numUnitOfMeasure));

                        result = result.Replace(
                            oldValue: "@unitOfMeasurement",
                            newValue: Functions.PrepNBoolArg(arg: unitOfMeasurement));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_user_options_numerator
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="jlang"></param>
                    /// <param name="topic"></param>
                    /// <param name="numEstimationPeriod"></param>
                    /// <param name="numEstimationCellCollection"></param>
                    /// <param name="numIdGroup"></param>
                    /// <param name="numIndicator"></param>
                    /// <param name="numState"></param>
                    /// <param name="numUnitOfMeasure"></param>
                    /// <param name="unitOfMeasurement"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string jlang,
                        Nullable<int> topic,
                        Nullable<int> numEstimationPeriod,
                        Nullable<int> numEstimationCellCollection,
                        List<Nullable<int>> numIdGroup,
                        Nullable<bool> numIndicator,
                        Nullable<bool> numState,
                        Nullable<bool> numUnitOfMeasure,
                        Nullable<bool> unitOfMeasurement,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            jlang: jlang,
                            topic: topic,
                            numEstimationPeriod: numEstimationPeriod,
                            numEstimationCellCollection: numEstimationCellCollection,
                            numIdGroup: numIdGroup,
                            numIndicator: numIndicator,
                            numState: numState,
                            numUnitOfMeasure: numUnitOfMeasure,
                            unitOfMeasurement: unitOfMeasurement);

                        NpgsqlParameter pJlang = new(
                            parameterName: "jlang",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pTopic = new(
                            parameterName: "topic",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumEstimationPeriod = new(
                            parameterName: "numEstimationPeriod",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumEstimationCellCollection = new(
                            parameterName: "numEstimationCellCollection",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumIdGroup = new(
                            parameterName: "numIdGroup",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pNumIndicator = new(
                            parameterName: "numIndicator",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pNumState = new(
                            parameterName: "numState",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pNumUnitOfMeasure = new(
                            parameterName: "numUnitOfMeasure",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pUnitOfMeasurement = new(
                            parameterName: "unitOfMeasurement",
                            parameterType: NpgsqlDbType.Boolean);

                        if (jlang != null)
                        {
                            pJlang.Value = (string)jlang;
                        }
                        else
                        {
                            pJlang.Value = DBNull.Value;
                        }

                        if (topic != null)
                        {
                            pTopic.Value = (int)topic;
                        }
                        else
                        {
                            pTopic.Value = DBNull.Value;
                        }

                        if (numEstimationPeriod != null)
                        {
                            pNumEstimationPeriod.Value = (int)numEstimationPeriod;
                        }
                        else
                        {
                            pNumEstimationPeriod.Value = DBNull.Value;
                        }

                        if (numEstimationCellCollection != null)
                        {
                            pNumEstimationCellCollection.Value = (int)numEstimationCellCollection;
                        }
                        else
                        {
                            pNumEstimationCellCollection.Value = DBNull.Value;
                        }

                        if (numIdGroup != null)
                        {
                            pNumIdGroup.Value = numIdGroup.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pNumIdGroup.Value = DBNull.Value;
                        }

                        if (numIndicator != null)
                        {
                            pNumIndicator.Value = (bool)numIndicator;
                        }
                        else
                        {
                            pNumIndicator.Value = DBNull.Value;
                        }

                        if (numState != null)
                        {
                            pNumState.Value = (bool)numState;
                        }
                        else
                        {
                            pNumState.Value = DBNull.Value;
                        }

                        if (numUnitOfMeasure != null)
                        {
                            pNumUnitOfMeasure.Value = (bool)numUnitOfMeasure;
                        }
                        else
                        {
                            pNumUnitOfMeasure.Value = DBNull.Value;
                        }

                        if (unitOfMeasurement != null)
                        {
                            pUnitOfMeasurement.Value = (bool)unitOfMeasurement;
                        }
                        else
                        {
                            pUnitOfMeasurement.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pJlang, pTopic, pNumEstimationPeriod, pNumEstimationCellCollection, pNumIdGroup,
                                pNumIndicator, pNumState, pNumUnitOfMeasure, pUnitOfMeasurement);
                    }

                    #endregion Methods

                }

                #endregion FnGetUserOptionsNumerator

            }

        }
    }
}