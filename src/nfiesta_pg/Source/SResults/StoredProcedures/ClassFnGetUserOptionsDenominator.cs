﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_user_options_denominator

                #region FnGetUserOptionsDenominator

                /// <summary>
                /// Wrapper for stored procedure fn_get_user_options_denominator.
                /// The function returns list of available IDs from t_result_group table for given input arguments.
                /// </summary>
                public static class FnGetUserOptionsDenominator
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_user_options_denominator";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_jlang character varying, ",
                            $"_denom_id_group integer[], ",
                            $"_denom_indicator boolean DEFAULT false, ",
                            $"_denom_state boolean DEFAULT false; ",
                            $"returns: ",
                            $"TABLE(",
                            $"res_id integer, ",
                            $"res_label text, ",
                            $"res_id_group integer[])");


                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    res_id,",
                            $"    res_label,",
                            $"    res_id_group",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@jlang, @denomIdGroup, @denomIndicator, @denomState);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="jlang"></param>
                    /// <param name="denomIdGroup"></param>
                    /// <param name="denomIndicator"></param>
                    /// <param name="denomState"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string jlang,
                        List<Nullable<int>> denomIdGroup,
                        Nullable<bool> denomIndicator,
                        Nullable<bool> denomState)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@jlang",
                            newValue: Functions.PrepStringArg(arg: jlang));

                        result = result.Replace(
                            oldValue: "@denomIdGroup",
                            newValue: Functions.PrepNIntArrayArg(args: denomIdGroup, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@denomIndicator",
                            newValue: Functions.PrepNBoolArg(arg: denomIndicator));

                        result = result.Replace(
                            oldValue: "@denomState",
                            newValue: Functions.PrepNBoolArg(arg: denomState));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_user_options_denominator
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="jlang"></param>
                    /// <param name="denomIdGroup"></param>
                    /// <param name="denomIndicator"></param>
                    /// <param name="denomState"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string jlang,
                        List<Nullable<int>> denomIdGroup,
                        Nullable<bool> denomIndicator,
                        Nullable<bool> denomState,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            jlang: jlang,
                            denomIdGroup: denomIdGroup,
                            denomIndicator: denomIndicator,
                            denomState: denomState);

                        NpgsqlParameter pJlang = new(
                            parameterName: "jlang",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDenomIdGroup = new(
                            parameterName: "denomIdGroup",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pDenomIndicator = new(
                            parameterName: "denomIndicator",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pDenomState = new(
                            parameterName: "denomState",
                            parameterType: NpgsqlDbType.Boolean);

                        if (jlang != null)
                        {
                            pJlang.Value = (string)jlang;
                        }
                        else
                        {
                            pJlang.Value = DBNull.Value;
                        }

                        if (denomIdGroup != null)
                        {
                            pDenomIdGroup.Value = denomIdGroup.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pDenomIdGroup.Value = DBNull.Value;
                        }

                        if (denomIndicator != null)
                        {
                            pDenomIndicator.Value = (bool)denomIndicator;
                        }
                        else
                        {
                            pDenomIndicator.Value = DBNull.Value;
                        }

                        if (denomState != null)
                        {
                            pDenomState.Value = (bool)denomState;
                        }
                        else
                        {
                            pDenomState.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pJlang, pDenomIdGroup, pDenomIndicator, pDenomState);
                    }

                    #endregion Methods

                }

                #endregion FnGetUserOptionsDenominator

            }

        }
    }
}