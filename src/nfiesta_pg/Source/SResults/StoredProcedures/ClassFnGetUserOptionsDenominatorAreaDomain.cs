﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_user_options_denominator_area_domain

                #region FnGetUserOptionsDenominatorAreaDomain

                /// <summary>
                /// Wrapper for stored procedure fn_get_user_options_denominator_area_domain.
                /// The function returns list of available area domains for given input arguments.
                /// The first input argument is an indentifator of language mutation.
                /// A possible variants are "cs-CZ" or "en-GB".
                /// The second input argument is a list of IDs from t_result_group table.
                /// The third input argument is a list of area domains.
                /// </summary>
                public static class FnGetUserOptionsDenominatorAreaDomain
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_user_options_denominator_area_domain";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature = String.Concat(
                        $"$SchemaName.$Name; ",
                        $"args: ",
                        $"_jlang character varying, ",
                        $"_id_group integer[], ",
                        $"_area_domain integer[] DEFAULT NULL::integer[]; ",
                        $"returns: ",
                        $"TABLE(",
                        $"res_id integer, ",
                        $"res_label text, ",
                        $"res_id_group integer[], ",
                        $"res_area_domain integer[])");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    res_id,",
                                $"    res_label,",
                                $"    res_id_group,",
                                $"    res_area_domain",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@jlang, @idGroup, @areaDomain);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="jlang"></param>
                    /// <param name="idGroup"></param>
                    /// <param name="areaDomain"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string jlang,
                        List<Nullable<int>> idGroup,
                        List<Nullable<int>> areaDomain)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@jlang",
                            newValue: Functions.PrepStringArg(arg: jlang));

                        result = result.Replace(
                            oldValue: "@idGroup",
                            newValue: Functions.PrepNIntArrayArg(args: idGroup, dbType: "int4"));

                        result = result.Replace(
                             oldValue: "@areaDomain",
                             newValue: Functions.PrepNIntArrayArg(args: areaDomain, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_user_options_denominator_area_domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="jlang"></param>
                    /// <param name="idGroup"></param>
                    /// <param name="areaDomain"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string jlang,
                        List<Nullable<int>> idGroup,
                        List<Nullable<int>> areaDomain,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            jlang: jlang,
                            idGroup: idGroup,
                            areaDomain: areaDomain);

                        NpgsqlParameter pJlang = new(
                            parameterName: "jlang",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pIdGroup = new(
                            parameterName: "idGroup",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomain = new(
                            parameterName: "areaDomain",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (jlang != null)
                        {
                            pJlang.Value = (string)jlang;
                        }
                        else
                        {
                            pJlang.Value = DBNull.Value;
                        }

                        if (idGroup != null)
                        {
                            pIdGroup.Value = idGroup.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pIdGroup.Value = DBNull.Value;
                        }

                        if (areaDomain != null)
                        {
                            pAreaDomain.Value = areaDomain.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomain.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pJlang, pIdGroup, pAreaDomain);
                    }

                    #endregion Methods

                }

                #endregion FnGetUserOptionsDenominatorAreaDomain

            }

        }
    }
}