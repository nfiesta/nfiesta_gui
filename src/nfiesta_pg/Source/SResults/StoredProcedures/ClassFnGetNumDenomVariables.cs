﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_num_denom_variables

                #region FnGetNumDenomVariables

                /// <summary>
                /// Wrapper for stored procedure fn_get_num_denom_variables.
                /// Function returns table with assigned denominator variables to numerator variables.
                /// </summary>
                public static class FnGetNumDenomVariables
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_num_denom_variables";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_num_var integer[], ",
                            $"_denom_var integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"variable_numerator integer, ",
                            $"variable_denominator integer)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    variable_numerator,",
                            $"    variable_denominator",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@numVar, @denomVar);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="numVar"></param>
                    /// <param name="denomVar"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> numVar,
                        List<Nullable<int>> denomVar)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@numVar",
                            newValue: Functions.PrepNIntArrayArg(args: numVar, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@denomVar",
                            newValue: Functions.PrepNIntArrayArg(args: denomVar, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_num_denom_variables
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="numVar"></param>
                    /// <param name="denomVar"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> numVar,
                        List<Nullable<int>> denomVar,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            numVar: numVar,
                            denomVar: denomVar);

                        NpgsqlParameter pNumVar = new(
                            parameterName: "numVar",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pDenomVar = new(
                            parameterName: "denomVar",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (numVar != null)
                        {
                            pNumVar.Value = numVar.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pNumVar.Value = DBNull.Value;
                        }

                        if (denomVar != null)
                        {
                            pDenomVar.Value = denomVar.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pDenomVar.Value = DBNull.Value;
                        }


                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pNumVar, pDenomVar);
                    }

                    #endregion Methods

                }

                #endregion FnGetNumDenomVariables

            }

        }
    }
}