﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_area_sub_population_categories

                #region FnGetAreaSubPopulationCategories

                /// <summary>
                /// Wrapper for stored procedure fn_get_area_sub_population_categories.
                /// Function returns table with all hierarchically superior variables and its complementary categories
                /// within area domain or sub_population. Input parameters are target_variable (id from table c_target_variable),
                /// area_or_sub_pop (100 for area_domain, 200 for sub_population) and id (id from table c_area_domain or id from table c_sub_population)
                /// </summary>
                public static class FnGetAreaSubPopulationCategories
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_area_sub_population_categories";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_area_or_sub_pop integer, ",
                            $"_id integer, ",
                            $"_lang integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"variable integer, ",
                            $"attype integer, ",
                            $"category integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");


                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    variable,",
                            $"    attype,",
                            $"    category,",
                            $"    label,",
                            $"    description,",
                            $"    label_en,",
                            $"    description_en",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@targetVariable, @areaOrSubPop, @id, @lang);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariable"></param>
                    /// <param name="areaOrSubPop"></param>
                    /// <param name="id"></param>
                    /// <param name="lang"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariable,
                        Nullable<int> areaOrSubPop,
                        Nullable<int> id,
                        Nullable<int> lang)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@targetVariable",
                             newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                             oldValue: "@areaOrSubPop",
                             newValue: Functions.PrepNIntArg(arg: areaOrSubPop));

                        result = result.Replace(
                             oldValue: "@id",
                             newValue: Functions.PrepNIntArg(arg: id));

                        result = result.Replace(
                             oldValue: "@lang",
                             newValue: Functions.PrepNIntArg(arg: lang));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_area_sub_population_categories
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable"></param>
                    /// <param name="areaOrSubPop"></param>
                    /// <param name="id"></param>
                    /// <param name="lang"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        Nullable<int> areaOrSubPop,
                        Nullable<int> id,
                        Nullable<int> lang,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            targetVariable: targetVariable,
                            areaOrSubPop: areaOrSubPop,
                            id: id,
                            lang: lang);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaOrSubPop = new(
                           parameterName: "areaOrSubPop",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pId = new(
                          parameterName: "id",
                          parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLang = new(
                           parameterName: "lang",
                           parameterType: NpgsqlDbType.Integer);


                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        if (areaOrSubPop != null)
                        {
                            pAreaOrSubPop.Value = (int)areaOrSubPop;
                        }
                        else
                        {
                            pAreaOrSubPop.Value = DBNull.Value;
                        }

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        if (lang != null)
                        {
                            pLang.Value = (int)lang;
                        }
                        else
                        {
                            pLang.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pTargetVariable, pAreaOrSubPop, pId, pLang);
                    }

                    #endregion Methods

                }

                #endregion FnGetAreaSubPopulationCategories

            }

        }
    }
}