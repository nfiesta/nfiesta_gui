﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {

            /// <summary>
            /// Stored procedures of schema nfiesta_results
            /// </summary>
            public static partial class RSFunctions
            {
                // fn_get_language4query

                #region FnGetLanguageForQuery

                /// <summary>
                /// Wrapper for stored procedure fn_get_language4query.
                /// The function returns suffix from c_language table for given input language.
                /// </summary>
                public static class FnGetLanguageForQuery
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ResultsSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_language4query";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_jlang character varying; ",
                            $"returns: character varying");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@jlang)::character varying AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="jlang"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(string jlang)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@jlang",
                            newValue: Functions.PrepStringArg(arg: jlang));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_language4query
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="jlang"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string jlang,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            jlang: jlang,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_language4query
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="jlang"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        string jlang,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(jlang: jlang);

                        NpgsqlParameter pJlang = new(
                            parameterName: "jlang",
                            parameterType: NpgsqlDbType.Varchar);

                        if (jlang != null)
                        {
                            pJlang.Value = (string)jlang;
                        }
                        else
                        {
                            pJlang.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pJlang);
                    }

                    #endregion Methods

                }

                #endregion FnGetLanguageForQuery

            }

        }
    }
}