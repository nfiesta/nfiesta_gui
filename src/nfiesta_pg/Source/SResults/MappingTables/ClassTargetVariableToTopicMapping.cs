﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // cm_result2topic

            /// <summary>
            /// Mapping between target variable (total, ratio) and topic
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TargetVariableToTopicMapping(
                TargetVariableToTopicMappingList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<TargetVariableToTopicMappingList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between target variable (total, ratio) and topic - identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColId.Name,
                            defaultValue: Int32.Parse(s: TargetVariableToTopicMappingList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of target variable,
                /// foreign key to table c_target_variable.
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: TargetVariableToTopicMappingList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object
                /// </summary>
                public TargetVariable TargetVariable
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CTargetVariable[TargetVariableId];
                    }
                }


                /// <summary>
                /// Identifier of target variable,
                /// foreign key to table c_target_variable.
                /// </summary>
                public Nullable<int> DenominatorId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColDenominatorId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TargetVariableToTopicMappingList.ColDenominatorId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TargetVariableToTopicMappingList.ColDenominatorId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColDenominatorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object
                /// </summary>
                public TargetVariable Denominator
                {
                    get
                    {
                        return (DenominatorId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CTargetVariable[(int)DenominatorId] : null;
                    }
                }


                /// <summary>
                /// Identifier of topic,
                /// foreign key to table c_topic.
                /// </summary>
                public int TopicId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColTopicId.Name,
                            defaultValue: Int32.Parse(s: TargetVariableToTopicMappingList.ColTopicId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetVariableToTopicMappingList.ColTopicId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Topic object
                /// </summary>
                public Topic Topic
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CTopic[TopicId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TargetVariableToTopicMapping Type, return False
                    if (obj is not TargetVariableToTopicMapping)
                    {
                        return false;
                    }

                    return
                        Id == ((TargetVariableToTopicMapping)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between target variable (total, ratio) and topic
            /// </summary>
            public class TargetVariableToTopicMappingList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_result2topic";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_result2topic";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování mezi cílovou proměnnou (úhrn, podíl) a tématickým okruhem";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between target variable (total, ratio) and topic";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "target_variable", new ColumnMetadata()
                    {
                        Name = "target_variable",
                        DbName = "target_variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE",
                        HeaderTextEn = "TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "denominator", new ColumnMetadata()
                    {
                        Name = "denominator",
                        DbName = "denominator",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR",
                        HeaderTextEn = "DENOMINATOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "topic", new ColumnMetadata()
                    {
                        Name = "topic",
                        DbName = "topic",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOPIC",
                        HeaderTextEn = "TOPIC",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable"];

                /// <summary>
                /// Column denominator
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorId = Cols["denominator"];

                /// <summary>
                /// Column topic
                /// </summary>
                public static readonly ColumnMetadata ColTopicId = Cols["topic"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TargetVariableToTopicMappingList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TargetVariableToTopicMappingList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between target variable (total, ratio) and topic (read-only)
                /// </summary>
                public List<TargetVariableToTopicMapping> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TargetVariableToTopicMapping(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between target variable (total, ratio) and topic from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between target variable (total, ratio) and topic identifier</param>
                /// <returns>Mapping between target variable (total, ratio) and topic from list by identifier (null if not found)</returns>
                public TargetVariableToTopicMapping this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TargetVariableToTopicMapping(composite: this, data: a))
                                .FirstOrDefault<TargetVariableToTopicMapping>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TargetVariableToTopicMappingList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TargetVariableToTopicMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TargetVariableToTopicMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
