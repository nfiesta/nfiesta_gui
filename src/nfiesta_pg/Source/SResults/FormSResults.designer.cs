﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.NfiEstaPg.Results
{

    partial class FormSResults
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            msMain = new System.Windows.Forms.MenuStrip();
            tsmiLookupTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAreaDomain = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAreaDomainLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAreaDomainCategory = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAreaDomainCategoryLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimateType = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCellLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCellCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCellCollectionLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationPeriod = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationPeriodLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCGuiHeader = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCGuiHeaderLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCPanelRefYearSetGroup = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCPanelRefYearSetGroupLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCPhaseEstimateType = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCSubPopulation = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCSubPopulationLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCSubPopulationCategory = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCSubPopulationCategoryLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCTopic = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCTopicLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiMappingTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmAreaDomainCategory = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmResultToTopic = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmSubPopulationCategory = new System.Windows.Forms.ToolStripMenuItem();
            tsmiSpatialTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFaCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiETL = new System.Windows.Forms.ToolStripMenuItem();
            tsmiExtractData = new System.Windows.Forms.ToolStripMenuItem();
            tsmiGetDifferences = new System.Windows.Forms.ToolStripMenuItem();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlClose = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            grpMain = new System.Windows.Forms.GroupBox();
            splitMain = new System.Windows.Forms.SplitContainer();
            pnlData = new System.Windows.Forms.Panel();
            txtSQL = new System.Windows.Forms.RichTextBox();
            tsmiDataTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTEstimateConf = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTEstimationCellHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTResult = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTResultGroup = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTTotalEstimateConf = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTVariable = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTVariableHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            msMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlClose.SuspendLayout();
            grpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitMain).BeginInit();
            splitMain.Panel1.SuspendLayout();
            splitMain.Panel2.SuspendLayout();
            splitMain.SuspendLayout();
            SuspendLayout();
            // 
            // msMain
            // 
            msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiLookupTables, tsmiMappingTables, tsmiSpatialTables, tsmiDataTables, tsmiETL });
            msMain.Location = new System.Drawing.Point(0, 0);
            msMain.Name = "msMain";
            msMain.Size = new System.Drawing.Size(944, 24);
            msMain.TabIndex = 0;
            // 
            // tsmiLookupTables
            // 
            tsmiLookupTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiCAreaDomain, tsmiCAreaDomainLanguage, tsmiCAreaDomainCategory, tsmiCAreaDomainCategoryLanguage, tsmiCEstimateType, tsmiCEstimationCell, tsmiCEstimationCellLanguage, tsmiCEstimationCellCollection, tsmiCEstimationCellCollectionLanguage, tsmiCEstimationPeriod, tsmiCEstimationPeriodLanguage, tsmiCGuiHeader, tsmiCGuiHeaderLanguage, tsmiCLanguage, tsmiCPanelRefYearSetGroup, tsmiCPanelRefYearSetGroupLanguage, tsmiCPhaseEstimateType, tsmiCSubPopulation, tsmiCSubPopulationLanguage, tsmiCSubPopulationCategory, tsmiCSubPopulationCategoryLanguage, tsmiCTargetVariable, tsmiCTopic, tsmiCTopicLanguage });
            tsmiLookupTables.Name = "tsmiLookupTables";
            tsmiLookupTables.Size = new System.Drawing.Size(114, 20);
            tsmiLookupTables.Text = "tsmiLookupTables";
            // 
            // tsmiCAreaDomain
            // 
            tsmiCAreaDomain.Name = "tsmiCAreaDomain";
            tsmiCAreaDomain.Size = new System.Drawing.Size(287, 22);
            tsmiCAreaDomain.Text = "tsmiCAreaDomain";
            // 
            // tsmiCAreaDomainLanguage
            // 
            tsmiCAreaDomainLanguage.Name = "tsmiCAreaDomainLanguage";
            tsmiCAreaDomainLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCAreaDomainLanguage.Text = "tsmiCAreaDomainLanguage";
            // 
            // tsmiCAreaDomainCategory
            // 
            tsmiCAreaDomainCategory.Name = "tsmiCAreaDomainCategory";
            tsmiCAreaDomainCategory.Size = new System.Drawing.Size(287, 22);
            tsmiCAreaDomainCategory.Text = "tsmiCAreaDomainCategory";
            // 
            // tsmiCAreaDomainCategoryLanguage
            // 
            tsmiCAreaDomainCategoryLanguage.Name = "tsmiCAreaDomainCategoryLanguage";
            tsmiCAreaDomainCategoryLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCAreaDomainCategoryLanguage.Text = "tsmiCAreaDomainCategoryLanguage";
            // 
            // tsmiCEstimateType
            // 
            tsmiCEstimateType.Name = "tsmiCEstimateType";
            tsmiCEstimateType.Size = new System.Drawing.Size(287, 22);
            tsmiCEstimateType.Text = "tsmiCEstimateType";
            // 
            // tsmiCEstimationCell
            // 
            tsmiCEstimationCell.Name = "tsmiCEstimationCell";
            tsmiCEstimationCell.Size = new System.Drawing.Size(287, 22);
            tsmiCEstimationCell.Text = "tsmiCEstimationCell";
            // 
            // tsmiCEstimationCellLanguage
            // 
            tsmiCEstimationCellLanguage.Name = "tsmiCEstimationCellLanguage";
            tsmiCEstimationCellLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCEstimationCellLanguage.Text = "tsmiCEstimationCellLanguage";
            // 
            // tsmiCEstimationCellCollection
            // 
            tsmiCEstimationCellCollection.Name = "tsmiCEstimationCellCollection";
            tsmiCEstimationCellCollection.Size = new System.Drawing.Size(287, 22);
            tsmiCEstimationCellCollection.Text = "tsmiCEstimationCellCollection";
            // 
            // tsmiCEstimationCellCollectionLanguage
            // 
            tsmiCEstimationCellCollectionLanguage.Name = "tsmiCEstimationCellCollectionLanguage";
            tsmiCEstimationCellCollectionLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCEstimationCellCollectionLanguage.Text = "tsmiCEstimationCellCollectionLanguage";
            // 
            // tsmiCEstimationPeriod
            // 
            tsmiCEstimationPeriod.Name = "tsmiCEstimationPeriod";
            tsmiCEstimationPeriod.Size = new System.Drawing.Size(287, 22);
            tsmiCEstimationPeriod.Text = "tsmiCEstimationPeriod";
            // 
            // tsmiCEstimationPeriodLanguage
            // 
            tsmiCEstimationPeriodLanguage.Name = "tsmiCEstimationPeriodLanguage";
            tsmiCEstimationPeriodLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCEstimationPeriodLanguage.Text = "tsmiCEstimationPeriodLanguage";
            // 
            // tsmiCGuiHeader
            // 
            tsmiCGuiHeader.Name = "tsmiCGuiHeader";
            tsmiCGuiHeader.Size = new System.Drawing.Size(287, 22);
            tsmiCGuiHeader.Text = "tsmiCGuiHeader";
            // 
            // tsmiCGuiHeaderLanguage
            // 
            tsmiCGuiHeaderLanguage.Name = "tsmiCGuiHeaderLanguage";
            tsmiCGuiHeaderLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCGuiHeaderLanguage.Text = "tsmiCGuiHeaderLanguage";
            // 
            // tsmiCLanguage
            // 
            tsmiCLanguage.Name = "tsmiCLanguage";
            tsmiCLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCLanguage.Text = "tsmiCLanguage";
            // 
            // tsmiCPanelRefYearSetGroup
            // 
            tsmiCPanelRefYearSetGroup.Name = "tsmiCPanelRefYearSetGroup";
            tsmiCPanelRefYearSetGroup.Size = new System.Drawing.Size(287, 22);
            tsmiCPanelRefYearSetGroup.Text = "tsmiCPanelRefYearSetGroup";
            // 
            // tsmiCPanelRefYearSetGroupLanguage
            // 
            tsmiCPanelRefYearSetGroupLanguage.Name = "tsmiCPanelRefYearSetGroupLanguage";
            tsmiCPanelRefYearSetGroupLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCPanelRefYearSetGroupLanguage.Text = "tsmiCPanelRefYearSetGroupLanguage";
            // 
            // tsmiCPhaseEstimateType
            // 
            tsmiCPhaseEstimateType.Name = "tsmiCPhaseEstimateType";
            tsmiCPhaseEstimateType.Size = new System.Drawing.Size(287, 22);
            tsmiCPhaseEstimateType.Text = "tsmiCPhaseEstimateType";
            // 
            // tsmiCSubPopulation
            // 
            tsmiCSubPopulation.Name = "tsmiCSubPopulation";
            tsmiCSubPopulation.Size = new System.Drawing.Size(287, 22);
            tsmiCSubPopulation.Text = "tsmiCSubPopulation";
            // 
            // tsmiCSubPopulationLanguage
            // 
            tsmiCSubPopulationLanguage.Name = "tsmiCSubPopulationLanguage";
            tsmiCSubPopulationLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCSubPopulationLanguage.Text = "tsmiCSubPopulationLanguage";
            // 
            // tsmiCSubPopulationCategory
            // 
            tsmiCSubPopulationCategory.Name = "tsmiCSubPopulationCategory";
            tsmiCSubPopulationCategory.Size = new System.Drawing.Size(287, 22);
            tsmiCSubPopulationCategory.Text = "tsmiCSubPopulationCategory";
            // 
            // tsmiCSubPopulationCategoryLanguage
            // 
            tsmiCSubPopulationCategoryLanguage.Name = "tsmiCSubPopulationCategoryLanguage";
            tsmiCSubPopulationCategoryLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCSubPopulationCategoryLanguage.Text = "tsmiCSubPopulationCategoryLanguage";
            // 
            // tsmiCTargetVariable
            // 
            tsmiCTargetVariable.Name = "tsmiCTargetVariable";
            tsmiCTargetVariable.Size = new System.Drawing.Size(287, 22);
            tsmiCTargetVariable.Text = "tsmiCTargetVariable";
            // 
            // tsmiCTopic
            // 
            tsmiCTopic.Name = "tsmiCTopic";
            tsmiCTopic.Size = new System.Drawing.Size(287, 22);
            tsmiCTopic.Text = "tsmiCTopic";
            // 
            // tsmiCTopicLanguage
            // 
            tsmiCTopicLanguage.Name = "tsmiCTopicLanguage";
            tsmiCTopicLanguage.Size = new System.Drawing.Size(287, 22);
            tsmiCTopicLanguage.Text = "tsmiCTopicLanguage";
            // 
            // tsmiMappingTables
            // 
            tsmiMappingTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiCmAreaDomainCategory, tsmiCmResultToTopic, tsmiCmSubPopulationCategory });
            tsmiMappingTables.Name = "tsmiMappingTables";
            tsmiMappingTables.Size = new System.Drawing.Size(122, 20);
            tsmiMappingTables.Text = "tsmiMappingTables";
            // 
            // tsmiCmAreaDomainCategory
            // 
            tsmiCmAreaDomainCategory.Name = "tsmiCmAreaDomainCategory";
            tsmiCmAreaDomainCategory.Size = new System.Drawing.Size(242, 22);
            tsmiCmAreaDomainCategory.Text = "tsmiCmAreaDomainCategory";
            // 
            // tsmiCmResultToTopic
            // 
            tsmiCmResultToTopic.Name = "tsmiCmResultToTopic";
            tsmiCmResultToTopic.Size = new System.Drawing.Size(242, 22);
            tsmiCmResultToTopic.Text = "tsmiCmResultToTopic";
            // 
            // tsmiCmSubPopulationCategory
            // 
            tsmiCmSubPopulationCategory.Name = "tsmiCmSubPopulationCategory";
            tsmiCmSubPopulationCategory.Size = new System.Drawing.Size(242, 22);
            tsmiCmSubPopulationCategory.Text = "tsmiCmSubPopulationCategory";
            // 
            // tsmiSpatialTables
            // 
            tsmiSpatialTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiFaCell });
            tsmiSpatialTables.Name = "tsmiSpatialTables";
            tsmiSpatialTables.Size = new System.Drawing.Size(109, 20);
            tsmiSpatialTables.Text = "tsmiSpatialTables";
            // 
            // tsmiFaCell
            // 
            tsmiFaCell.Name = "tsmiFaCell";
            tsmiFaCell.Size = new System.Drawing.Size(129, 22);
            tsmiFaCell.Text = "tsmiFaCell";
            // 
            // tsmiETL
            // 
            tsmiETL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiExtractData, tsmiGetDifferences });
            tsmiETL.Name = "tsmiETL";
            tsmiETL.Size = new System.Drawing.Size(60, 20);
            tsmiETL.Text = "tsmiETL";
            // 
            // tsmiExtractData
            // 
            tsmiExtractData.Name = "tsmiExtractData";
            tsmiExtractData.Size = new System.Drawing.Size(174, 22);
            tsmiExtractData.Text = "tsmiExtractData";
            // 
            // tsmiGetDifferences
            // 
            tsmiGetDifferences.Name = "tsmiGetDifferences";
            tsmiGetDifferences.Size = new System.Drawing.Size(174, 22);
            tsmiGetDifferences.Text = "tsmiGetDifferences";
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(grpMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 24);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(944, 477);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 2;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpButtons.Controls.Add(pnlClose, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 437);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 28;
            // 
            // pnlClose
            // 
            pnlClose.Controls.Add(btnClose);
            pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClose.Location = new System.Drawing.Point(784, 0);
            pnlClose.Margin = new System.Windows.Forms.Padding(0);
            pnlClose.Name = "pnlClose";
            pnlClose.Padding = new System.Windows.Forms.Padding(5);
            pnlClose.Size = new System.Drawing.Size(160, 40);
            pnlClose.TabIndex = 12;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnClose.Location = new System.Drawing.Point(5, 5);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(150, 30);
            btnClose.TabIndex = 13;
            btnClose.Text = "btnClose";
            btnClose.UseVisualStyleBackColor = true;
            // 
            // grpMain
            // 
            grpMain.Controls.Add(splitMain);
            grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpMain.ForeColor = System.Drawing.SystemColors.ControlText;
            grpMain.Location = new System.Drawing.Point(3, 3);
            grpMain.Name = "grpMain";
            grpMain.Size = new System.Drawing.Size(938, 431);
            grpMain.TabIndex = 5;
            grpMain.TabStop = false;
            // 
            // splitMain
            // 
            splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splitMain.Location = new System.Drawing.Point(3, 18);
            splitMain.Name = "splitMain";
            splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            splitMain.Panel1.Controls.Add(pnlData);
            // 
            // splitMain.Panel2
            // 
            splitMain.Panel2.Controls.Add(txtSQL);
            splitMain.Size = new System.Drawing.Size(932, 410);
            splitMain.SplitterDistance = 204;
            splitMain.SplitterWidth = 1;
            splitMain.TabIndex = 11;
            // 
            // pnlData
            // 
            pnlData.AutoScroll = true;
            pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlData.Location = new System.Drawing.Point(0, 0);
            pnlData.Name = "pnlData";
            pnlData.Size = new System.Drawing.Size(932, 204);
            pnlData.TabIndex = 3;
            // 
            // txtSQL
            // 
            txtSQL.BackColor = System.Drawing.SystemColors.Window;
            txtSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSQL.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtSQL.Location = new System.Drawing.Point(0, 0);
            txtSQL.Name = "txtSQL";
            txtSQL.ReadOnly = true;
            txtSQL.Size = new System.Drawing.Size(932, 205);
            txtSQL.TabIndex = 0;
            txtSQL.Text = "";
            // 
            // tsmiDataTables
            // 
            tsmiDataTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiTEstimateConf, tsmiTEstimationCellHierarchy, tsmiTResult, tsmiTResultGroup, tsmiTTotalEstimateConf, tsmiTVariable, tsmiTVariableHierarchy });
            tsmiDataTables.Name = "tsmiDataTables";
            tsmiDataTables.Size = new System.Drawing.Size(98, 20);
            tsmiDataTables.Text = "tsmiDataTables";
            // 
            // tsmiTEstimateConf
            // 
            tsmiTEstimateConf.Name = "tsmiTEstimateConf";
            tsmiTEstimateConf.Size = new System.Drawing.Size(230, 22);
            tsmiTEstimateConf.Text = "tsmiTEstimateConf";
            // 
            // tsmiTEstimationCellHierarchy
            // 
            tsmiTEstimationCellHierarchy.Name = "tsmiTEstimationCellHierarchy";
            tsmiTEstimationCellHierarchy.Size = new System.Drawing.Size(230, 22);
            tsmiTEstimationCellHierarchy.Text = "tsmiTEstimationCellHierarchy";
            // 
            // tsmiTResult
            // 
            tsmiTResult.Name = "tsmiTResult";
            tsmiTResult.Size = new System.Drawing.Size(230, 22);
            tsmiTResult.Text = "tsmiTResult";
            // 
            // tsmiTResultGroup
            // 
            tsmiTResultGroup.Name = "tsmiTResultGroup";
            tsmiTResultGroup.Size = new System.Drawing.Size(230, 22);
            tsmiTResultGroup.Text = "tsmiTResultGroup";
            // 
            // tsmiTTotalEstimateConf
            // 
            tsmiTTotalEstimateConf.Name = "tsmiTTotalEstimateConf";
            tsmiTTotalEstimateConf.Size = new System.Drawing.Size(230, 22);
            tsmiTTotalEstimateConf.Text = "tsmiTTotalEstimateConf";
            // 
            // tsmiTVariable
            // 
            tsmiTVariable.Name = "tsmiTVariable";
            tsmiTVariable.Size = new System.Drawing.Size(230, 22);
            tsmiTVariable.Text = "tsmiTVariable";
            // 
            // tsmiTVariableHierarchy
            // 
            tsmiTVariableHierarchy.Name = "tsmiTVariableHierarchy";
            tsmiTVariableHierarchy.Size = new System.Drawing.Size(230, 22);
            tsmiTVariableHierarchy.Text = "tsmiTVariableHierarchy";
            // 
            // FormSResults
            // 
            AcceptButton = btnClose;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnClose;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            Controls.Add(msMain);
            MainMenuStrip = msMain;
            Name = "FormSResults";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            msMain.ResumeLayout(false);
            msMain.PerformLayout();
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlClose.ResumeLayout(false);
            grpMain.ResumeLayout(false);
            splitMain.Panel1.ResumeLayout(false);
            splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitMain).EndInit();
            splitMain.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.RichTextBox txtSQL;
        private System.Windows.Forms.ToolStripMenuItem tsmiMappingTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiSpatialTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomain;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmAreaDomainCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmResultToTopic;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmSubPopulationCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiFaCell;
        

        private System.Windows.Forms.ToolStripMenuItem tsmiETL;
        private System.Windows.Forms.ToolStripMenuItem tsmiGetDifferences;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ToolStripMenuItem tsmiExtractData;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomainCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomainLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomainCategoryLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimateType;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCellLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCellCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCellCollectionLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationPeriod;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationPeriodLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCGuiHeader;
        private System.Windows.Forms.ToolStripMenuItem tsmiCGuiHeaderLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiCPanelRefYearSetGroupLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCPhaseEstimateType;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulationLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulationCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulationCategoryLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiCTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiCTopic;
        private System.Windows.Forms.ToolStripMenuItem tsmiCTopicLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiTEstimateConf;
        private System.Windows.Forms.ToolStripMenuItem tsmiTEstimationCellHierarchy;
        private System.Windows.Forms.ToolStripMenuItem tsmiTResult;
        private System.Windows.Forms.ToolStripMenuItem tsmiTResultGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiTTotalEstimateConf;
        private System.Windows.Forms.ToolStripMenuItem tsmiTVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiTVariableHierarchy;
    }

}