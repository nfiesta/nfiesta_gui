﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // t_total_estimate_conf

            /// <summary>
            /// Total estimate configuration
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TotalEstimateConf(
                TotalEstimateConfList composite = null,
                DataRow data = null)
                    : ADataTableEntry<TotalEstimateConfList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Total estimate configuration identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColId.Name,
                            defaultValue: Int32.Parse(s: TotalEstimateConfList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: TotalEstimateConfList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SResults.CEstimationCell[EstimationCellId];
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - identifier
                /// </summary>
                public int VariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColVariableId.Name,
                            defaultValue: Int32.Parse(s: TotalEstimateConfList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SResults.TVariable[VariableId];
                    }
                }


                /// <summary>
                /// Estimate phase identifier
                /// </summary>
                public int PhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: TotalEstimateConfList.ColPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType PhaseEstimateType
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SResults.CPhaseEstimateType[PhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Force synthetic estimate
                /// </summary>
                public Nullable<bool> ForceSynthetic
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TotalEstimateConfList.ColForceSynthetic.Name,
                            defaultValue: String.IsNullOrEmpty(value: TotalEstimateConfList.ColForceSynthetic.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TotalEstimateConfList.ColForceSynthetic.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TotalEstimateConfList.ColForceSynthetic.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public Nullable<int> EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColEstimationPeriodId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TotalEstimateConfList.ColEstimationPeriodId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TotalEstimateConfList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return (EstimationPeriodId != null) ?
                            ((NfiEstaDB)Composite.Database).SResults.CEstimationPeriod[(int)EstimationPeriodId] : null;
                    }
                }


                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TotalEstimateConfList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TotalEstimateConfList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TotalEstimateConfList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return (PanelRefYearSetGroupId != null) ?
                            ((NfiEstaDB)Composite.Database).SResults.CPanelRefYearSetGroup[(int)PanelRefYearSetGroupId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TotalEstimateConf Type, return False
                    if (obj is not TotalEstimateConf)
                    {
                        return false;
                    }

                    return
                        Id == ((TotalEstimateConf)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of total estimate configurations
            /// </summary>
            public class TotalEstimateConfList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_total_estimate_conf";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_total_estimate_conf";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam konfigurací odhadu úhrnů";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of total estimate configurations";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        NotNull = true,
                        PKey = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimation_cell", new ColumnMetadata()
                    {
                        Name = "estimation_cell",
                        DbName = "estimation_cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL",
                        HeaderTextEn = "ESTIMATION_CELL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "variable", new ColumnMetadata()
                    {
                        Name = "variable",
                        DbName = "variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE",
                        HeaderTextEn = "VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "phase_estimate_type", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type",
                        DbName = "phase_estimate_type",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "force_synthetic", new ColumnMetadata()
                    {
                        Name = "force_synthetic",
                        DbName = "force_synthetic",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "FORCE_SYNTHETIC",
                        HeaderTextEn = "FORCE_SYNTHETIC",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "estimation_period", new ColumnMetadata()
                    {
                        Name = "estimation_period",
                        DbName = "estimation_period",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "panel_refyearset_group", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group",
                        DbName = "panel_refyearset_group",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimation_cell metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell"];

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable"];

                /// <summary>
                /// Column phase_estimate_type metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeId = Cols["phase_estimate_type"];

                /// <summary>
                /// Column force_synthetic metadata
                /// </summary>
                public static readonly ColumnMetadata ColForceSynthetic = Cols["force_synthetic"];

                /// <summary>
                /// Column estimation_period metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period"];

                /// <summary>
                /// Column panel_refyearset_group metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TotalEstimateConfList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TotalEstimateConfList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of total estimate configurations (read-only)
                /// </summary>
                public List<TotalEstimateConf> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TotalEstimateConf(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Total estimate configuration from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Total estimate configuration identifier</param>
                /// <returns>Total estimate configuration from list by identifier (null if not found)</returns>
                public TotalEstimateConf this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TotalEstimateConf(composite: this, data: a))
                                .FirstOrDefault<TotalEstimateConf>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TotalEstimateConfList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TotalEstimateConfList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TotalEstimateConfList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
