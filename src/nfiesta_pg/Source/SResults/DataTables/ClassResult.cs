﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // t_result

            /// <summary>
            /// Final estimate
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class Result(
                ResultList composite = null,
                DataRow data = null)
                    : ADataTableEntry<ResultList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Final estimate identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultList.ColId.Name,
                            defaultValue: Int32.Parse(s: ResultList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate configuration identifier
                /// </summary>
                public int EstimateConfId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultList.ColEstimateConfId.Name,
                            defaultValue: Int32.Parse(s: ResultList.ColEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultList.ColEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration object (read-only)
                /// </summary>
                public EstimateConf EstimateConf
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.TEstimateConf[EstimateConfId];
                    }
                }


                /// <summary>
                /// Point estimate
                /// </summary>
                public Nullable<double> PointEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: ResultList.ColPointEstimate.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultList.ColPointEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: ResultList.ColPointEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: ResultList.ColPointEstimate.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate of variability
                /// </summary>
                public Nullable<double> VarianceEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: ResultList.ColVarianceEstimate.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultList.ColVarianceEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: ResultList.ColVarianceEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: ResultList.ColVarianceEstimate.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Time when calculation started
                /// </summary>
                public DateTime CalcStarted
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: ResultList.ColCalcStarted.Name,
                            defaultValue: DateTime.Parse(s: ResultList.ColCalcStarted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: ResultList.ColCalcStarted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started as text
                /// </summary>
                public string CalcStartedText
                {
                    get
                    {
                        return
                            ((DateTime)CalcStarted).ToString(
                                format: ResultList.ColCalcStarted.NumericFormat);
                    }
                }


                /// <summary>
                /// Time necessary for calculation
                /// </summary>
                public TimeSpan CalcDuration
                {
                    get
                    {
                        return Functions.GetTimeSpanArg(
                            row: Data,
                            name: ResultList.ColCalcDuration.Name,
                            defaultValue: TimeSpan.Parse(s: ResultList.ColCalcDuration.DefaultValue));
                    }
                    set
                    {
                        Functions.SetTimeSpanArg(
                            row: Data,
                            name: ResultList.ColCalcDuration.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation as text
                /// </summary>
                public string CalcDurationText
                {
                    get
                    {
                        return
                            ((TimeSpan)CalcDuration).ToString(
                                format: ResultList.ColCalcDuration.NumericFormat);
                    }
                }


                /// <summary>
                /// Minimal sample size
                /// </summary>
                public Nullable<double> MinSSize
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: ResultList.ColMinSSize.Name,
                            defaultValue: String.IsNullOrEmpty(ResultList.ColMinSSize.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: ResultList.ColMinSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: ResultList.ColMinSSize.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Actual sample size
                /// </summary>
                public Nullable<long> ActSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: ResultList.ColActSSize.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultList.ColActSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: ResultList.ColActSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: ResultList.ColActSSize.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Flag for identification actual value of final estimate
                /// </summary>
                public bool IsLatest
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ResultList.ColIsLatest.Name,
                            defaultValue: Boolean.Parse(value: ResultList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ResultList.ColIsLatest.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not Result Type, return False
                    if (obj is not Result)
                    {
                        return false;
                    }

                    return
                        Id == ((Result)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of final estimates
            /// </summary>
            public class ResultList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_result";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_result";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam výsledků";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of final estimates";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimate_conf", new ColumnMetadata()
                    {
                        Name = "estimate_conf",
                        DbName = "estimate_conf",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue =default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF",
                        HeaderTextEn = "ESTIMATE_CONF",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "point", new ColumnMetadata()
                    {
                        Name = "point",
                        DbName = "point",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "POINT",
                        HeaderTextEn = "POINT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "var", new ColumnMetadata()
                    {
                        Name = "var",
                        DbName = "var",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VAR",
                        HeaderTextEn = "VAR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "calc_started", new ColumnMetadata()
                    {
                        Name = "calc_started",
                        DbName = "calc_started",
                        DataType = "System.DateTime",
                        DbDataType = "timestamptz",
                        NewDataType = "timestamptz",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new System.DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_STARTED",
                        HeaderTextEn = "CALC_STARTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "calc_duration", new ColumnMetadata()
                    {
                        Name = "calc_duration",
                        DbName = "calc_duration",
                        DataType = "System.TimeSpan",
                        DbDataType = "interval",
                        NewDataType = "interval",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new System.TimeSpan().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "g",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_DURATION",
                        HeaderTextEn = "CALC_DURATION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "min_ssize", new ColumnMetadata()
                    {
                        Name = "min_ssize",
                        DbName = "min_ssize",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MIN_SSIZE",
                        HeaderTextEn = "MIN_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "act_ssize", new ColumnMetadata()
                    {
                        Name = "act_ssize",
                        DbName = "act_ssize",
                        DataType = "System.Int64",
                        DbDataType = "int8",
                        NewDataType = "int8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ACT_SSIZE",
                        HeaderTextEn = "ACT_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = "is_latest",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimate_conf metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfId = Cols["estimate_conf"];

                /// <summary>
                /// Column point metadata
                /// </summary>
                public static readonly ColumnMetadata ColPointEstimate = Cols["point"];

                /// <summary>
                /// Column var metadata
                /// </summary>
                public static readonly ColumnMetadata ColVarianceEstimate = Cols["var"];

                /// <summary>
                /// Column calc_started metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcStarted = Cols["calc_started"];

                /// <summary>
                /// Column calc_duration metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcDuration = Cols["calc_duration"];

                /// <summary>
                /// Column min_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColMinSSize = Cols["min_ssize"];

                /// <summary>
                /// Column act_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColActSSize = Cols["act_ssize"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ResultList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ResultList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of final estimates (read-only)
                /// </summary>
                public List<Result> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new Result(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Final estimate from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Final estimate identifier</param>
                /// <returns>Final estimate from list by identifier (null if not found)</returns>
                public Result this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new Result(composite: this, data: a))
                                .FirstOrDefault<Result>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ResultList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ResultList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ResultList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of final estimates
                /// for selected estimate configuration
                /// </summary>
                /// <param name="estimateConfId">Selected estimate configurations identifiers</param>
                /// <param name="isLatest">Identification of the actual value</param>
                /// <returns>
                /// List of final estimates
                /// for selected estimate configuration
                /// </returns>
                public ResultList Reduce(
                    List<int> estimateConfId = null,
                    Nullable<bool> isLatest = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((estimateConfId != null) && estimateConfId.Count != 0)
                    {
                        rows = rows.Where(a => estimateConfId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColEstimateConfId.Name) ?? 0)));
                    }

                    if (isLatest != null)
                    {
                        rows = rows.Where(a => (bool)(a.Field<Nullable<bool>>(columnName: ColIsLatest.Name) ?? false));
                    }

                    return
                        rows.Any() ?
                        new ResultList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ResultList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}