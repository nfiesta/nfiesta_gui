﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // t_variable

            /// <summary>
            /// Combination of target variable and sub-population category and area domain category
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class Variable(
                VariableList composite = null,
                DataRow data = null)
                    : ADataTableEntry<VariableList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VariableList.ColId.Name,
                            defaultValue: Int32.Parse(s: VariableList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VariableList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Target variable identifier
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VariableList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: VariableList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VariableList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object (read-only)
                /// </summary>
                public TargetVariable TargetVariable
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CTargetVariable[TargetVariableId];
                    }
                }


                /// <summary>
                /// Sub-population category identifier
                /// </summary>
                public Nullable<int> SubPopulationCategoryId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: VariableList.ColSubPopulationCategoryId.Name,
                            defaultValue: String.IsNullOrEmpty(value: VariableList.ColSubPopulationCategoryId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: VariableList.ColSubPopulationCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: VariableList.ColSubPopulationCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sub-population category object (read-only)
                /// </summary>
                public SubPopulationCategory SubPopulationCategory
                {
                    get
                    {
                        return (SubPopulationCategoryId != null) ?
                            ((NfiEstaDB)Composite.Database).SResults.CSubPopulationCategory[(int)SubPopulationCategoryId] : null;
                    }
                }


                /// <summary>
                /// Area domain category identifier
                /// </summary>
                public Nullable<int> AreaDomainCategoryId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: VariableList.ColAreaDomainCategoryId.Name,
                            defaultValue: String.IsNullOrEmpty(value: VariableList.ColAreaDomainCategoryId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: VariableList.ColAreaDomainCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: VariableList.ColAreaDomainCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain category object (read-only)
                /// </summary>
                public AreaDomainCategory AreaDomainCategory
                {
                    get
                    {
                        return (AreaDomainCategoryId != null) ?
                            ((NfiEstaDB)Composite.Database).SResults.CAreaDomainCategory[(int)AreaDomainCategoryId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not Variable Type, return False
                    if (obj is not Variable)
                    {
                        return false;
                    }

                    return
                        Id == ((Variable)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of combinations of target variable and sub-population category and area domain category
            /// </summary>
            public class VariableList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_variable";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_variable";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam kombinací cílové proměnné a kategorie sub-populace a kategorie plošné domény";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of combination of target variable and sub-population category and area domain category";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "target_variable", new ColumnMetadata()
                    {
                        Name = "target_variable",
                        DbName = "target_variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE",
                        HeaderTextEn = "TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "sub_population_category", new ColumnMetadata()
                    {
                        Name = "sub_population_category",
                        DbName = "sub_population_category",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_CATEGORY",
                        HeaderTextEn = "SUB_POPULATION_CATEGORY",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "area_domain_category", new ColumnMetadata()
                    {
                        Name = "area_domain_category",
                        DbName = "area_domain_category",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_CATEGORY",
                        HeaderTextEn = "AREA_DOMAIN_CATEGORY",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable"];

                /// <summary>
                /// Column sub_population_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategoryId = Cols["sub_population_category"];

                /// <summary>
                /// Column area_domain_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategoryId = Cols["area_domain_category"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VariableList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VariableList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of combinations of target variable and sub-population category and area domain category (read-only)
                /// </summary>
                public List<Variable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new Variable(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Combination of target variable and sub-population category and area domain category identifier</param>
                /// <returns>Combination of target variable and sub-population category and area domain category from list by identifier (null if not found)</returns>
                public Variable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new Variable(composite: this, data: a))
                                .FirstOrDefault<Variable>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public VariableList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new VariableList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VariableList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}