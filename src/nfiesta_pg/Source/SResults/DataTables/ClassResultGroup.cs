﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // t_result_group

            /// <summary>
            /// Result group
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ResultGroup(
                ResultGroupList composite = null,
                DataRow data = null)
                    : ADataTableEntry<ResultGroupList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Result group identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultGroupList.ColId.Name,
                            defaultValue: Int32.Parse(s: ResultGroupList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultGroupList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate type (total or ratio) identifier,
                /// foreign key to table c_estimate_type
                /// </summary>
                public int EstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultGroupList.ColEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: ResultGroupList.ColEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultGroupList.ColEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate type (total or ratio) object (read-only)
                /// </summary>
                public EstimateType EstimateType
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CEstimateType[EstimateTypeId];
                    }
                }


                /// <summary>
                /// Target variable identifier,
                /// foreign key to table c_target_variable
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultGroupList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: ResultGroupList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultGroupList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object (read-only)
                /// </summary>
                public TargetVariable TargetVariable
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CTargetVariable[TargetVariableId];
                    }
                }


                /// <summary>
                /// Target variable for denominator identifier,
                /// foreign key to table c_target_variable
                /// </summary>
                public Nullable<int> TargetVariableDenomId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColTargetVariableDenomId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultGroupList.ColTargetVariableDenomId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultGroupList.ColTargetVariableDenomId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColTargetVariableDenomId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable for denominator for denominator object (read-only)
                /// </summary>
                public TargetVariable TargetVariableDenom
                {
                    get
                    {
                        return (TargetVariableDenomId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CTargetVariable[(int)TargetVariableDenomId] : null;
                    }
                }


                /// <summary>
                /// Estimation period identifier,
                /// foreign key to table c_estimation_period
                /// </summary>
                public int EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultGroupList.ColEstimationPeriodId.Name,
                            defaultValue: Int32.Parse(s: ResultGroupList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultGroupList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CEstimationPeriod[EstimationPeriodId];
                    }
                }


                /// <summary>
                /// Panel refyearset group identifier,
                /// foreign key to table c_panel_refyearset_group
                /// </summary>
                public int PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultGroupList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: Int32.Parse(s: ResultGroupList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultGroupList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel refyearset group object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CPanelRefYearSetGroup[PanelRefYearSetGroupId];
                    }
                }


                /// <summary>
                /// Phase estimate type identifier,
                /// foreign key to table c_phase_estimate_type
                /// </summary>
                public int PhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultGroupList.ColPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: ResultGroupList.ColPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultGroupList.ColPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Phase estimate type for denominator object (read-only)
                /// </summary>
                public PhaseEstimateType PhaseEstimateType
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SResults.CPhaseEstimateType[PhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Phase estimate type for denominator identifier,
                /// foreign key to table c_phase_estimate_type
                /// </summary>
                public Nullable<int> PhaseEstimateTypeDenomId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColPhaseEstimateTypeDenomId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultGroupList.ColPhaseEstimateTypeDenomId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultGroupList.ColPhaseEstimateTypeDenomId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColPhaseEstimateTypeDenomId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable for denominator object (read-only)
                /// </summary>
                public PhaseEstimateType PhaseEstimateTypeDenom
                {
                    get
                    {
                        return (PhaseEstimateTypeDenomId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CPhaseEstimateType[(int)PhaseEstimateTypeDenomId] : null;
                    }
                }


                /// <summary>
                /// Estimation cell collection identifier,
                /// foreign key to table c_estimation_cell_collection
                /// </summary>
                public Nullable<int> EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColEstimationCellCollectionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultGroupList.ColEstimationCellCollectionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultGroupList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection object (read-only)
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return (EstimationCellCollectionId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CEstimationCellCollection[(int)EstimationCellCollectionId] : null;
                    }
                }


                /// <summary>
                /// Area domain identifier,
                /// foreign key to table c_area_domain,
                /// null value determines "without distinction"
                /// </summary>
                public Nullable<int> AreaDomainId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColAreaDomainDenomId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultGroupList.ColAreaDomainDenomId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultGroupList.ColAreaDomainDenomId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColAreaDomainDenomId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain object (read-only)
                /// </summary>
                public AreaDomain AreaDomain
                {
                    get
                    {
                        return (AreaDomainId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CAreaDomain[(int)AreaDomainId] : null;
                    }
                }


                /// <summary>
                /// Area domain for denominator identifier,
                /// foreign key to table c_area_domain,
                /// Null value determines "without distinction"
                /// </summary>
                public Nullable<int> AreaDomainDenomId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColAreaDomainDenomId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultGroupList.ColAreaDomainDenomId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultGroupList.ColAreaDomainDenomId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColAreaDomainDenomId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain for denominator object (read-only)
                /// </summary>
                public AreaDomain AreaDomainDenom
                {
                    get
                    {
                        return (AreaDomainDenomId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CAreaDomain[(int)AreaDomainDenomId] : null;
                    }
                }


                /// <summary>
                /// Subpopulation identifier,
                /// foreign key to table c_sub_population,
                /// Null value determines "without distinction"
                /// </summary>
                public Nullable<int> SubPopulationId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColSubPopulationId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultGroupList.ColSubPopulationId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultGroupList.ColSubPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColSubPopulationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Subpopulation object (read-only)
                /// </summary>
                public SubPopulation SubPopulation
                {
                    get
                    {
                        return (SubPopulationId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CSubPopulation[(int)SubPopulationId] : null;
                    }
                }


                /// <summary>
                /// Subpopulation for denominator identifier,
                /// foreign key to table c_sub_population,
                /// null value determines "without distinction"
                /// </summary>
                public Nullable<int> SubPopulationDenomId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColSubPopulationDenomId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultGroupList.ColSubPopulationDenomId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultGroupList.ColSubPopulationDenomId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultGroupList.ColSubPopulationDenomId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Subpopulation for denominator object (read-only)
                /// </summary>
                public SubPopulation SubPopulationDenom
                {
                    get
                    {
                        return (SubPopulationDenomId != null) ?
                             ((NfiEstaDB)Composite.Database).SResults.CSubPopulation[(int)SubPopulationDenomId] : null;
                    }
                }


                /// <summary>
                /// Flag for result that can be displayed on public website
                /// </summary>
                public bool Web
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ResultGroupList.ColWeb.Name,
                            defaultValue: Boolean.Parse(value: ResultGroupList.ColWeb.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ResultGroupList.ColWeb.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ResultGroup Type, return False
                    if (obj is not ResultGroup)
                    {
                        return false;
                    }

                    return
                        Id == ((ResultGroup)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of result groups
            /// </summary>
            public class ResultGroupList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_result_group";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_result_group";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam skupin výsledků";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of result groups";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimate_type", new ColumnMetadata()
                    {
                        Name = "estimate_type",
                        DbName = "estimate_type",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_TYPE",
                        HeaderTextEn = "ESTIMATE_TYPE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "target_variable", new ColumnMetadata()
                    {
                        Name = "target_variable",
                        DbName = "target_variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE",
                        HeaderTextEn = "TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "target_variable_denom", new ColumnMetadata()
                    {
                        Name = "target_variable_denom",
                        DbName = "target_variable_denom",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE_DENOM",
                        HeaderTextEn = "TARGET_VARIABLE_DENOM",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_period", new ColumnMetadata()
                    {
                        Name = "estimation_period",
                        DbName = "estimation_period",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD",
                        HeaderTextEn = "ESTIMATION_PERIOD",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "panel_refyearset_group", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group",
                        DbName = "panel_refyearset_group",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "phase_estimate_type", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type",
                        DbName = "phase_estimate_type",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "phase_estimate_type_denom", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_denom",
                        DbName = "phase_estimate_type_denom",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE_DENOM",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_DENOM",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "estimation_cell_collection", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection",
                        DbName = "estimation_cell_collection",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "area_domain", new ColumnMetadata()
                    {
                        Name = "area_domain",
                        DbName = "area_domain",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN",
                        HeaderTextEn = "AREA_DOMAIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "area_domain_denom", new ColumnMetadata()
                    {
                        Name = "area_domain_denom",
                        DbName = "area_domain_denom",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_DENOM",
                        HeaderTextEn = "AREA_DOMAIN_DENOM",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "sub_population", new ColumnMetadata()
                    {
                        Name = "sub_population",
                        DbName = "sub_population",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION",
                        HeaderTextEn = "SUB_POPULATION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "sub_population_denom", new ColumnMetadata()
                    {
                        Name = "sub_population_denom",
                        DbName = "sub_population_denom",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_DENOM",
                        HeaderTextEn = "SUB_POPULATION_DENOM",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "web", new ColumnMetadata()
                    {
                        Name = "web",
                        DbName = "web",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "false",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "WEB",
                        HeaderTextEn = "WEB",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimate_type metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateTypeId = Cols["estimate_type"];

                /// <summary>
                /// Column target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable"];

                /// <summary>
                /// Column target_variable_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableDenomId = Cols["target_variable_denom"];

                /// <summary>
                /// Column estimation_period metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period"];

                /// <summary>
                /// Column panel_refyearset_group metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group"];

                /// <summary>
                /// Column phase_estimate_type metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeId = Cols["phase_estimate_type"];

                /// <summary>
                /// Column phase_estimate_type_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeDenomId = Cols["phase_estimate_type_denom"];

                /// <summary>
                /// Column estimation_cell_collection metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection"];

                /// <summary>
                /// Column area_domain metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainId = Cols["area_domain"];

                /// <summary>
                /// Column area_domain_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainDenomId = Cols["area_domain_denom"];

                /// <summary>
                /// Column sub_population metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationId = Cols["sub_population"];

                /// <summary>
                /// Column sub_population_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationDenomId = Cols["sub_population_denom"];

                /// <summary>
                /// Column web metadata
                /// </summary>
                public static readonly ColumnMetadata ColWeb = Cols["web"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ResultGroupList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ResultGroupList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of result groups (read-only)
                /// </summary>
                public List<ResultGroup> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ResultGroup(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Result group from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Result group identifier</param>
                /// <returns>Result group from list by identifier (null if not found)</returns>
                public ResultGroup this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ResultGroup(composite: this, data: a))
                                .FirstOrDefault<ResultGroup>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ResultGroupList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ResultGroupList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ResultGroupList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}