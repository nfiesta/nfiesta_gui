﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // t_estimation_cell_hierarchy

            /// <summary>
            /// Estimation cell mapping
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class EstimationCellHierarchy(
                EstimationCellHierarchyList composite = null,
                DataRow data = null)
                    : ADataTableEntry<EstimationCellHierarchyList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Estimation cell mapping identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimationCellHierarchyList.ColId.Name,
                            defaultValue: Int32.Parse(s: EstimationCellHierarchyList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimationCellHierarchyList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimationCellHierarchyList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: EstimationCellHierarchyList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimationCellHierarchyList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SResults.CEstimationCell[EstimationCellId];
                    }
                }


                /// <summary>
                /// Superior estimation cell identifier
                /// </summary>
                public int EstimationCellSuperiorId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimationCellHierarchyList.ColEstimationCellSuperiorId.Name,
                            defaultValue: Int32.Parse(s: EstimationCellHierarchyList.ColEstimationCellSuperiorId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimationCellHierarchyList.ColEstimationCellSuperiorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCellSuperior
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SResults.CEstimationCell[EstimationCellSuperiorId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not EstimationCellHierarchy Type, return False
                    if (obj is not EstimationCellHierarchy)
                    {
                        return false;
                    }

                    return
                        Id == ((EstimationCellHierarchy)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of estimation cell mappings
            /// </summary>
            public class EstimationCellHierarchyList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_estimation_cell_hierarchy";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_estimation_cell_hierarchy";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování výpočetních buněk";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of estimation cell mappings";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "cell", new ColumnMetadata()
                    {
                        Name = "cell",
                        DbName = "cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CELL",
                        HeaderTextEn = "CELL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "cell_superior", new ColumnMetadata()
                    {
                        Name = "cell_superior",
                        DbName = "cell_superior",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue= default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CELL_SUPERIOR",
                        HeaderTextEn = "CELL_SUPERIOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column cell metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["cell"];

                /// <summary>
                /// Column cell_superior metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellSuperiorId = Cols["cell_superior"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimationCellHierarchyList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimationCellHierarchyList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of estimation cell mappings (read-only)
                /// </summary>
                public List<EstimationCellHierarchy> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new EstimationCellHierarchy(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Estimation cell mapping from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Estimation cell mapping identifier</param>
                /// <returns>Estimation cell mapping from list by identifier (null if not found)</returns>
                public EstimationCellHierarchy this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new EstimationCellHierarchy(composite: this, data: a))
                                .FirstOrDefault<EstimationCellHierarchy>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public EstimationCellHierarchyList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new EstimationCellHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new EstimationCellHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of estimation cell mappings
                /// for selected inferior and superior estimation cells
                /// </summary>
                /// <param name="inferiorEstimationCellId">Selected inferior estimation cells identifiers</param>
                /// <param name="superiorEstimationCellId">Selected superior estimation cells identifiers</param>
                /// <returns>
                /// List of estimation cell mappings
                /// for selected inferior and superior estimation cells
                /// </returns>
                public EstimationCellHierarchyList Reduce(
                    List<int> inferiorEstimationCellId = null,
                    List<int> superiorEstimationCellId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((inferiorEstimationCellId != null) && inferiorEstimationCellId.Count != 0)
                    {
                        rows = rows.Where(a => inferiorEstimationCellId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColEstimationCellId.Name) ?? 0)));
                    }

                    if ((superiorEstimationCellId != null) && superiorEstimationCellId.Count != 0)
                    {
                        rows = rows.Where(a => superiorEstimationCellId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColEstimationCellSuperiorId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new EstimationCellHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new EstimationCellHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
