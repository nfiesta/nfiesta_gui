﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Results
        {
            // t_variable_hierarchy

            /// <summary>
            /// Variable mapping
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class VariableHierarchy(
                VariableHierarchyList composite = null,
                DataRow data = null)
                    : ADataTableEntry<VariableHierarchyList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Variable mapping identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VariableHierarchyList.ColId.Name,
                            defaultValue: Int32.Parse(s: VariableHierarchyList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VariableHierarchyList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Variable identifier
                /// </summary>
                public int VariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VariableHierarchyList.ColVariableId.Name,
                            defaultValue: Int32.Parse(s: VariableHierarchyList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VariableHierarchyList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Variable object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SResults.TVariable[VariableId];
                    }
                }


                /// <summary>
                /// Superior variable identifier
                /// </summary>
                public int VariableSuperiorId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VariableHierarchyList.ColVariableSuperiorId.Name,
                            defaultValue: Int32.Parse(s: VariableHierarchyList.ColVariableSuperiorId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VariableHierarchyList.ColVariableSuperiorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior variable object (read-only)
                /// </summary>
                public Variable VariableSuperior
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SResults.TVariable[VariableSuperiorId];
                    }
                }


                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not VariableHierarchy Type, return False
                    if (obj is not VariableHierarchy)
                    {
                        return false;
                    }

                    return
                        Id == ((VariableHierarchy)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of variable mappings
            /// </summary>
            public class VariableHierarchyList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ResultsSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_variable_hierarchy";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_variable_hierarchy";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování proměnných";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of variable mappings";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "variable_id", new ColumnMetadata()
                    {
                        Name = "variable_id",
                        DbName = "variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE",
                        HeaderTextEn = "VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "variable_superior_id", new ColumnMetadata()
                    {
                        Name = "variable_superior_id",
                        DbName = "variable_superior",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_SUPERIOR",
                        HeaderTextEn = "VARIABLE_SUPERIOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable_id"];

                /// <summary>
                /// Column variable_superior_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableSuperiorId = Cols["variable_superior_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VariableHierarchyList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VariableHierarchyList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of variable mappings (read-only)
                /// </summary>
                public List<VariableHierarchy> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new VariableHierarchy(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Variable mapping from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Variable mapping identifier</param>
                /// <returns>Variable mapping from list by identifier (null if not found)</returns>
                public VariableHierarchy this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new VariableHierarchy(composite: this, data: a))
                                .FirstOrDefault<VariableHierarchy>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public VariableHierarchyList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new VariableHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VariableHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of variable mappings
                /// for selected inferior and superior variables
                /// </summary>
                /// <param name="inferiorVariableId">Selected inferior variables identifiers</param>
                /// <param name="superiorVariableId">Selected superior variables identifiers</param>
                /// <returns>
                /// List of variable mappings
                /// for selected inferior and superior variables
                /// </returns>
                public VariableHierarchyList Reduce(
                    List<int> inferiorVariableId = null,
                    List<int> superiorVariableId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((inferiorVariableId != null) && inferiorVariableId.Count != 0)
                    {
                        rows = rows.Where(a => inferiorVariableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColVariableId.Name) ?? 0)));
                    }

                    if ((superiorVariableId != null) && superiorVariableId.Count != 0)
                    {
                        rows = rows.Where(a => superiorVariableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColVariableSuperiorId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new VariableHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VariableHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
