﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.NfiEstaPg.Results
{

    /// <summary>
    /// <para lang="cs">Formulář zobrazení dat extenze nfiesta_results</para>
    /// <para lang="en">Form for displaying nfiesta_results extension data</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormSResults
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        private Nullable<int> limit;

        private string msgDataExportComplete = String.Empty;
        private string msgNoDifferencesFound = String.Empty;

        #endregion Private Fields


        #region Contructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormSResults(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        public Nullable<int> Limit
        {
            get
            {
                return limit;
            }
            set
            {
                limit = value;
            }
        }

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Limit = null;

            InitializeLabels();

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            tsmiCAreaDomain.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.AreaDomainList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.AreaDomainList.Cols);
                });

            tsmiCAreaDomainLanguage.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.AreaDomainLanguageList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.AreaDomainLanguageList.Cols);
                });

            tsmiCAreaDomainCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.AreaDomainCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.AreaDomainCategoryList.Cols);
                });

            tsmiCAreaDomainCategoryLanguage.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList.Cols);
                });

            tsmiCEstimateType.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.EstimateTypeList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.EstimateTypeList.Cols);
                });

            tsmiCEstimationCell.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.EstimationCellList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.EstimationCellList.Cols);
                });

            tsmiCEstimationCellLanguage.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.EstimationCellLanguageList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.EstimationCellLanguageList.Cols);
                });

            tsmiCEstimationCellCollection.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.EstimationCellCollectionList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.EstimationCellCollectionList.Cols);
                });

            tsmiCEstimationCellCollectionLanguage.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList(database: Database),
                        columns: ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList.Cols);
                });

            tsmiCEstimationPeriod.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Results.EstimationPeriodList(database: Database),
                       columns: ZaJi.NfiEstaPg.Results.EstimationPeriodList.Cols);
               });

            tsmiCEstimationPeriodLanguage.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList(database: Database),
                       columns: ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList.Cols);
               });

            tsmiCGuiHeader.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Results.GuiHeaderList(database: Database),
                       columns: ZaJi.NfiEstaPg.Results.GuiHeaderList.Cols);
               });

            tsmiCGuiHeaderLanguage.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList(database: Database),
                       columns: ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList.Cols);
               });

            tsmiCLanguage.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.LanguageList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.LanguageList.Cols);
              });

            tsmiCPanelRefYearSetGroup.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList.Cols);
              });

            tsmiCPanelRefYearSetGroupLanguage.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList.Cols);
              });

            tsmiCPhaseEstimateType.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList.Cols);
              });

            tsmiCSubPopulation.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.SubPopulationList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.SubPopulationList.Cols);
              });

            tsmiCSubPopulationLanguage.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.SubPopulationLanguageList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.SubPopulationLanguageList.Cols);
              });

            tsmiCSubPopulationCategory.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.SubPopulationCategoryList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.SubPopulationCategoryList.Cols);
              });

            tsmiCSubPopulationCategoryLanguage.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList.Cols);
              });

            tsmiCTargetVariable.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.TargetVariableList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.TargetVariableList.Cols);
              });

            tsmiCTopic.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Results.TopicList(database: Database),
                       columns: ZaJi.NfiEstaPg.Results.TopicList.Cols);
               });

            tsmiCTopicLanguage.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.TopicLanguageList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.TopicLanguageList.Cols);
              });

            tsmiCmAreaDomainCategory.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList.Cols);
              });

            tsmiCmResultToTopic.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList.Cols);
              });

            tsmiCmSubPopulationCategory.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList.Cols);
              });

            tsmiFaCell.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.CellList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.CellList.Cols);
              });

            tsmiTEstimateConf.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.EstimateConfList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.EstimateConfList.Cols);
              });

            tsmiTEstimationCellHierarchy.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList.Cols);
              });

            tsmiTResult.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.ResultList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.ResultList.Cols);
              });

            tsmiTResultGroup.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.ResultGroupList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.ResultGroupList.Cols);
              });

            tsmiTTotalEstimateConf.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.TotalEstimateConfList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.TotalEstimateConfList.Cols);
              });

            tsmiTVariable.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.VariableList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.VariableList.Cols);
              });

            tsmiTVariableHierarchy.Click += new EventHandler(
              (sender, e) =>
              {
                  DisplayTable(
                      table: new ZaJi.NfiEstaPg.Results.VariableHierarchyList(database: Database),
                      columns: ZaJi.NfiEstaPg.Results.VariableHierarchyList.Cols);
              });

            tsmiExtractData.Click += new EventHandler(
              (sender, e) => { ExportData(); });

            tsmiGetDifferences.Click += new EventHandler(
              (sender, e) => { DisplayDifferences(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    Text = $"{ResultsSchema.ExtensionName} ({ResultsSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Číselníky";
                    tsmiCAreaDomain.Text = ZaJi.NfiEstaPg.Results.AreaDomainList.Name;
                    tsmiCAreaDomainLanguage.Text = ZaJi.NfiEstaPg.Results.AreaDomainLanguageList.Name;
                    tsmiCAreaDomainCategory.Text = ZaJi.NfiEstaPg.Results.AreaDomainCategoryList.Name;
                    tsmiCAreaDomainCategoryLanguage.Text = ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList.Name;
                    tsmiCEstimateType.Text = ZaJi.NfiEstaPg.Results.EstimateTypeList.Name;
                    tsmiCEstimationCell.Text = ZaJi.NfiEstaPg.Results.EstimationCellList.Name;
                    tsmiCEstimationCellLanguage.Text = ZaJi.NfiEstaPg.Results.EstimationCellLanguageList.Name;
                    tsmiCEstimationCellCollection.Text = ZaJi.NfiEstaPg.Results.EstimationCellCollectionList.Name;
                    tsmiCEstimationCellCollectionLanguage.Text = ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList.Name;
                    tsmiCEstimationPeriod.Text = ZaJi.NfiEstaPg.Results.EstimationPeriodList.Name;
                    tsmiCEstimationPeriodLanguage.Text = ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList.Name;
                    tsmiCGuiHeader.Text = ZaJi.NfiEstaPg.Results.GuiHeaderList.Name;
                    tsmiCGuiHeaderLanguage.Text = ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList.Name;
                    tsmiCLanguage.Text = ZaJi.NfiEstaPg.Results.LanguageList.Name;
                    tsmiCPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList.Name;
                    tsmiCPanelRefYearSetGroupLanguage.Text = ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList.Name;
                    tsmiCPhaseEstimateType.Text = ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList.Name;
                    tsmiCSubPopulation.Text = ZaJi.NfiEstaPg.Results.SubPopulationList.Name;
                    tsmiCSubPopulationLanguage.Text = ZaJi.NfiEstaPg.Results.SubPopulationLanguageList.Name;
                    tsmiCSubPopulationCategory.Text = ZaJi.NfiEstaPg.Results.SubPopulationCategoryList.Name;
                    tsmiCSubPopulationCategoryLanguage.Text = ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList.Name;
                    tsmiCTargetVariable.Text = ZaJi.NfiEstaPg.Results.TargetVariableList.Name;
                    tsmiCTopic.Text = ZaJi.NfiEstaPg.Results.TopicList.Name;
                    tsmiCTopicLanguage.Text = ZaJi.NfiEstaPg.Results.TopicLanguageList.Name;

                    tsmiMappingTables.Text = "Mapovací tabulky";
                    tsmiCmAreaDomainCategory.Text = ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList.Name;
                    tsmiCmResultToTopic.Text = ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList.Name;
                    tsmiCmSubPopulationCategory.Text = ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList.Name;

                    tsmiSpatialTables.Text = "Prostorové tabulky";
                    tsmiFaCell.Text = ZaJi.NfiEstaPg.Results.CellList.Name;

                    tsmiDataTables.Text = "Datové tabulky";
                    tsmiTEstimateConf.Text = ZaJi.NfiEstaPg.Results.EstimateConfList.Name;
                    tsmiTEstimationCellHierarchy.Text = ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList.Name;
                    tsmiTResult.Text = ZaJi.NfiEstaPg.Results.ResultList.Name;
                    tsmiTResultGroup.Text = ZaJi.NfiEstaPg.Results.ResultGroupList.Name;
                    tsmiTTotalEstimateConf.Text = ZaJi.NfiEstaPg.Results.TotalEstimateConfList.Name;
                    tsmiTVariable.Text = ZaJi.NfiEstaPg.Results.VariableList.Name;
                    tsmiTVariableHierarchy.Text = ZaJi.NfiEstaPg.Results.VariableHierarchyList.Name;

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export dat do vybrané složky";
                    tsmiGetDifferences.Text = "Rozdíly mezi dll a databázovou extenzí";

                    btnClose.Text = "Zavřít";

                    msgDataExportComplete = "Export dat byl dokončen.";
                    msgNoDifferencesFound = "Nebyly nalezeny žádné rozdíly.";

                    break;

                case LanguageVersion.International:
                    Text = $"{ResultsSchema.ExtensionName} ({ResultsSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Lookup tables";
                    tsmiCAreaDomain.Text = ZaJi.NfiEstaPg.Results.AreaDomainList.Name;
                    tsmiCAreaDomainLanguage.Text = ZaJi.NfiEstaPg.Results.AreaDomainLanguageList.Name;
                    tsmiCAreaDomainCategory.Text = ZaJi.NfiEstaPg.Results.AreaDomainCategoryList.Name;
                    tsmiCAreaDomainCategoryLanguage.Text = ZaJi.NfiEstaPg.Results.AreaDomainCategoryLanguageList.Name;
                    tsmiCEstimateType.Text = ZaJi.NfiEstaPg.Results.EstimateTypeList.Name;
                    tsmiCEstimationCell.Text = ZaJi.NfiEstaPg.Results.EstimationCellList.Name;
                    tsmiCEstimationCellLanguage.Text = ZaJi.NfiEstaPg.Results.EstimationCellLanguageList.Name;
                    tsmiCEstimationCellCollection.Text = ZaJi.NfiEstaPg.Results.EstimationCellCollectionList.Name;
                    tsmiCEstimationCellCollectionLanguage.Text = ZaJi.NfiEstaPg.Results.EstimationCellCollectionLanguageList.Name;
                    tsmiCEstimationPeriod.Text = ZaJi.NfiEstaPg.Results.EstimationPeriodList.Name;
                    tsmiCEstimationPeriodLanguage.Text = ZaJi.NfiEstaPg.Results.EstimationPeriodLanguageList.Name;
                    tsmiCGuiHeader.Text = ZaJi.NfiEstaPg.Results.GuiHeaderList.Name;
                    tsmiCGuiHeaderLanguage.Text = ZaJi.NfiEstaPg.Results.GuiHeaderLanguageList.Name;
                    tsmiCLanguage.Text = ZaJi.NfiEstaPg.Results.LanguageList.Name;
                    tsmiCPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupList.Name;
                    tsmiCPanelRefYearSetGroupLanguage.Text = ZaJi.NfiEstaPg.Results.PanelRefYearSetGroupLanguageList.Name;
                    tsmiCPhaseEstimateType.Text = ZaJi.NfiEstaPg.Results.PhaseEstimateTypeList.Name;
                    tsmiCSubPopulation.Text = ZaJi.NfiEstaPg.Results.SubPopulationList.Name;
                    tsmiCSubPopulationLanguage.Text = ZaJi.NfiEstaPg.Results.SubPopulationLanguageList.Name;
                    tsmiCSubPopulationCategory.Text = ZaJi.NfiEstaPg.Results.SubPopulationCategoryList.Name;
                    tsmiCSubPopulationCategoryLanguage.Text = ZaJi.NfiEstaPg.Results.SubPopulationCategoryLanguageList.Name;
                    tsmiCTargetVariable.Text = ZaJi.NfiEstaPg.Results.TargetVariableList.Name;
                    tsmiCTopic.Text = ZaJi.NfiEstaPg.Results.TopicList.Name;
                    tsmiCTopicLanguage.Text = ZaJi.NfiEstaPg.Results.TopicLanguageList.Name;

                    tsmiMappingTables.Text = "Mapping tables";
                    tsmiCmAreaDomainCategory.Text = ZaJi.NfiEstaPg.Results.AreaDomainCategoryMappingList.Name;
                    tsmiCmResultToTopic.Text = ZaJi.NfiEstaPg.Results.TargetVariableToTopicMappingList.Name;
                    tsmiCmSubPopulationCategory.Text = ZaJi.NfiEstaPg.Results.SubPopulationCategoryMappingList.Name;

                    tsmiSpatialTables.Text = "Spatial tables";
                    tsmiFaCell.Text = ZaJi.NfiEstaPg.Results.CellList.Name;

                    tsmiDataTables.Text = "Data tables";
                    tsmiTEstimateConf.Text = ZaJi.NfiEstaPg.Results.EstimateConfList.Name;
                    tsmiTEstimationCellHierarchy.Text = ZaJi.NfiEstaPg.Results.EstimationCellHierarchyList.Name;
                    tsmiTResult.Text = ZaJi.NfiEstaPg.Results.ResultList.Name;
                    tsmiTResultGroup.Text = ZaJi.NfiEstaPg.Results.ResultGroupList.Name;
                    tsmiTTotalEstimateConf.Text = ZaJi.NfiEstaPg.Results.TotalEstimateConfList.Name;
                    tsmiTVariable.Text = ZaJi.NfiEstaPg.Results.VariableList.Name;
                    tsmiTVariableHierarchy.Text = ZaJi.NfiEstaPg.Results.VariableHierarchyList.Name;

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export data to folder";
                    tsmiGetDifferences.Text = "Differences between dll database extension";

                    btnClose.Text = "Close";

                    msgDataExportComplete = "Data export has been completed.";
                    msgNoDifferencesFound = "No differences have been found.";

                    break;

                default:
                    Text = nameof(FormSResults);

                    tsmiLookupTables.Text = nameof(tsmiLookupTables);
                    tsmiCAreaDomain.Text = nameof(tsmiCAreaDomain);
                    tsmiCAreaDomainLanguage.Text = nameof(tsmiCAreaDomainLanguage);
                    tsmiCAreaDomainCategory.Text = nameof(tsmiCAreaDomainCategory);
                    tsmiCAreaDomainCategoryLanguage.Text = nameof(tsmiCAreaDomainCategoryLanguage);
                    tsmiCEstimateType.Text = nameof(tsmiCEstimateType);
                    tsmiCEstimationCell.Text = nameof(tsmiCEstimationCell);
                    tsmiCEstimationCellLanguage.Text = nameof(tsmiCEstimationCellLanguage);
                    tsmiCEstimationCellCollection.Text = nameof(tsmiCEstimationCellCollection);
                    tsmiCEstimationCellCollectionLanguage.Text = nameof(tsmiCEstimationCellCollectionLanguage);
                    tsmiCEstimationPeriod.Text = nameof(tsmiCEstimationPeriod);
                    tsmiCEstimationPeriodLanguage.Text = nameof(tsmiCEstimationPeriodLanguage);
                    tsmiCGuiHeader.Text = nameof(tsmiCGuiHeader);
                    tsmiCGuiHeaderLanguage.Text = nameof(tsmiCGuiHeaderLanguage);
                    tsmiCLanguage.Text = nameof(tsmiCLanguage);
                    tsmiCPanelRefYearSetGroup.Text = nameof(tsmiCPanelRefYearSetGroup);
                    tsmiCPanelRefYearSetGroupLanguage.Text = nameof(tsmiCPanelRefYearSetGroupLanguage);
                    tsmiCPhaseEstimateType.Text = nameof(tsmiCPhaseEstimateType);
                    tsmiCSubPopulation.Text = nameof(tsmiCSubPopulation);
                    tsmiCSubPopulationLanguage.Text = nameof(tsmiCSubPopulationLanguage);
                    tsmiCSubPopulationCategory.Text = nameof(tsmiCSubPopulationCategory);
                    tsmiCSubPopulationCategoryLanguage.Text = nameof(tsmiCSubPopulationCategoryLanguage);
                    tsmiCTargetVariable.Text = nameof(tsmiCTargetVariable);
                    tsmiCTopic.Text = nameof(tsmiCTopic);
                    tsmiCTopicLanguage.Text = nameof(tsmiCTopicLanguage);

                    tsmiMappingTables.Text = nameof(tsmiMappingTables);
                    tsmiCmAreaDomainCategory.Text = nameof(tsmiCmAreaDomainCategory);
                    tsmiCmResultToTopic.Text = nameof(tsmiCmResultToTopic);
                    tsmiCmSubPopulationCategory.Text = nameof(tsmiCmSubPopulationCategory);

                    tsmiSpatialTables.Text = nameof(tsmiSpatialTables);
                    tsmiFaCell.Text = nameof(tsmiFaCell);

                    tsmiDataTables.Text = nameof(tsmiDataTables);
                    tsmiTEstimateConf.Text = nameof(tsmiTEstimateConf);
                    tsmiTEstimationCellHierarchy.Text = nameof(tsmiTEstimationCellHierarchy);
                    tsmiTResult.Text = nameof(tsmiTResult);
                    tsmiTResultGroup.Text = nameof(tsmiTResultGroup);
                    tsmiTTotalEstimateConf.Text = nameof(tsmiTTotalEstimateConf);
                    tsmiTVariable.Text = nameof(tsmiTVariable);
                    tsmiTVariableHierarchy.Text = nameof(tsmiTVariableHierarchy);

                    tsmiETL.Text = nameof(tsmiETL);
                    tsmiExtractData.Text = nameof(tsmiExtractData);
                    tsmiGetDifferences.Text = nameof(tsmiGetDifferences);

                    btnClose.Text = nameof(btnClose);

                    msgDataExportComplete = nameof(msgDataExportComplete);
                    msgNoDifferencesFound = nameof(msgNoDifferencesFound);

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazí data číselníku</para>
        /// <para lang="en">Display lookup table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ALookupTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, extended: false);
            ALookupTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data mapovací tabulky</para>
        /// <para lang="en">Display mapping table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AMappingTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            AMappingTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky s prostorovými daty</para>
        /// <para lang="en">Display data table with spatial data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ASpatialTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, withGeom: false);
            ASpatialTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky</para>
        /// <para lang="en">Display table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ADataTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data vrácená uloženou procedurou</para>
        /// <para lang="en">Display data returned from stored procedure</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka s daty vrácenými uloženou procedurou</para>
        /// <para lang="en">Table with data returned from stored procedure</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AParametrizedView table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Export dat</para>
        /// <para lang="en">Data export</para>
        /// </summary>
        private void ExportData()
        {
            ResultsSchema.ExportData(
               database: Database,
               fileFormat: ExportFileFormat.Xml);

            MessageBox.Show(
                caption: String.Empty,
                text: msgDataExportComplete,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí provedené změny v databázi</para>
        /// <para lang="en">Displays changes made to the database</para>
        /// </summary>
        private void DisplayDifferences()
        {
            grpMain.Text = String.Empty;
            pnlData.Controls.Clear();
            txtSQL.Text = String.Empty;

            List<string> differences = Database.SResults.Differences;
            System.Text.StringBuilder stringBuilder = new();
            if ((differences == null) || (differences.Count == 0))
            {
                stringBuilder.AppendLine(value: msgNoDifferencesFound);
            }
            else
            {
                foreach (string difference in differences)
                {
                    stringBuilder.AppendLine(value: $"* {difference}");
                }
            }

            Label lblReport = new()
            {
                AutoSize = true,
                Dock = DockStyle.Top,
                Text = stringBuilder.ToString()
            };
            pnlData.Controls.Add(value: lblReport);
        }

        #endregion Methods

    }
}