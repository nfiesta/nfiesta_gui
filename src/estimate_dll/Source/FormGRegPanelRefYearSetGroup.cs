﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">
    /// Formulář pro konfiguraci regresních odhadů:
    /// Výběr skupiny panelů a referenčních období
    /// </para>
    /// <para lang="en">
    /// Form for configuring regression estimates:
    /// Selecting panel group and reference periods
    /// </para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormGRegPanelRefYearSetGroup
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">
        /// Seznam panelů a referenčních období,
        /// pro které již byly nakonfigurované jednofázové odhady
        /// </para>
        /// <para lang="en">
        /// List of panels and reference year sets,
        /// for which single-phase estimates have already been configured
        /// </para>
        /// </summary>
        private TFnApiGetOnePGroupsForGRegMapList onePGroupsForGRegMap;

        private string strAllConfigured = String.Empty;
        private string strNotAllConfigured = String.Empty;

        #endregion Private Fields


        #region Controls

        private GroupBox grpCountry;
        private GroupBox grpStrataSet;
        private GroupBox grpStratum;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormGRegPanelRefYearSetGroup(
            Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">
        /// Seznam panelů a referenčních období,
        /// pro které již byly nakonfigurované jednofázové odhady
        /// </para>
        /// <para lang="en">
        /// List of panels and reference year sets,
        /// for which single-phase estimates have already been configured
        /// </para>
        /// </summary>
        public TFnApiGetOnePGroupsForGRegMapList OnePGroupsForGRegMap
        {
            get
            {
                return onePGroupsForGRegMap;
            }
            set
            {
                onePGroupsForGRegMap = value;
                DisplayListOfPanelReferenceYearSetGroups();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam všech přepínačů pro skupiny panelů a roků měření v každé kombinaci strat</para>
        /// <para lang="en">List of all radiobuttons for group of panels and reference year sets in all strata combination</para>
        /// </summary>
        private IEnumerable<RadioButton> RadioButtons
        {
            get
            {
                foreach (GroupBox grpCountry in pnlWorkSpace.Controls.OfType<GroupBox>())
                {
                    foreach (GroupBox grpStrataSet in grpCountry.Controls.OfType<GroupBox>())
                    {
                        foreach (GroupBox grpStratum in grpStrataSet.Controls.OfType<GroupBox>())
                        {
                            foreach (RadioButton rdoPanelReferenceYearSetGroup in grpStratum.Controls.OfType<RadioButton>())
                            {
                                yield return rdoPanelReferenceYearSetGroup;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam všech skupin pro kombibinace strat</para>
        /// <para lang="en">List of all group boxes for strata combinations</para>
        /// </summary>
        private IEnumerable<GroupBox> StataGroupBoxes
        {
            get
            {
                foreach (GroupBox grpCountry in pnlWorkSpace.Controls.OfType<GroupBox>())
                {
                    foreach (GroupBox grpStrataSet in grpCountry.Controls.OfType<GroupBox>())
                    {
                        foreach (GroupBox grpStratum in grpStrataSet.Controls.OfType<GroupBox>())
                        {
                            yield return grpStratum;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané skupiny panelů a roků měření pro jednotlivé kombinace strat</para>
        /// <para lang="en">Selected groups of panels and reference year set for each stratum combination</para>
        /// </summary>
        private IEnumerable<DataRow> SelectedPanelReferenceYearSetGroups
        {
            get
            {
                return
                    RadioButtons
                        .Where(a => a.Checked)
                        .Select(a => (DataRow)a.Tag);
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikátory vybraných skupin panelů a roků měření pro jednotlivé kombinace strat</para>
        /// <para lang="en">Identifiers of selected groups of panels and reference year set for each stratum combination</para>
        /// </summary>
        public List<Nullable<int>> PanelReferenceYearSetGroupIds
        {
            get
            {
                return
                SelectedPanelReferenceYearSetGroups
                  .Select(a => a.Field<Nullable<int>>(
                      columnName: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupId.Name))
                  .Distinct<Nullable<int>>()
                  .ToList<Nullable<int>>();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegPanelRefYearSetGroup),     "Výběr skupiny panelů a referenčních období pro každé stratum" },
                        { nameof(btnCancel),                        "Zrušit" },
                        { nameof(btnOK),                            "Další" },
                        { nameof(grpCountry),                       "Země:" },
                        { nameof(grpStrataSet),                     "Soubor strat:" },
                        { nameof(grpStratum),                       "Stratum:" },
                        { nameof(lblNoSinglePhaseTotal),            String.Concat(
                            $"Pro některé nebo pro všechny kombinace strat nebyl nakonfigurován jednofázový odhad úhrnu, který by umožnil výpočet odhadů pro všechny odpovídající buňky.$0",
                            $"Prosím, nejprve nakonfigurujte jednofázové odhady úhrnu pro všechny výpočetní buňky nebo zmenšete množinu výpočetních buněk pro konfiguraci GREG-odhadu.") },
                        { nameof(strAllConfigured),                 "(všechny 1p odhady nakonfigurované)" },
                        { nameof(strNotAllConfigured),              "(chybí nakonfigurované 1p odhady)" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormGRegPanelRefYearSetGroup),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegPanelRefYearSetGroup),     "Selection of panel reference year set group for each stratum" },
                        { nameof(btnCancel),                        "Cancel" },
                        { nameof(btnOK),                            "Next" },
                        { nameof(grpCountry),                       "Country:" },
                        { nameof(grpStrataSet),                     "Strata set:" },
                        { nameof(grpStratum),                       "Stratum:" },
                        { nameof(lblNoSinglePhaseTotal),            String.Concat(
                            $"For some or for all strata combinations no single-phase total was configured allowing for the calculation of estimates for all corresponding cells.$0",
                            $"Please, first configure single-phase totals for all desired estimation cells or reduce the set of estimation cells for GREG-map configuration.") },
                        { nameof(strAllConfigured),                 "(all 1p estimates configured)" },
                        { nameof(strNotAllConfigured),              "(not all 1p estimates configured)" },
                    } :
                    languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormGRegPanelRefYearSetGroup),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            SetStyle();

            InitializeLabels();

            LoadContent();

            EnableNext();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;

            lblNoSinglePhaseTotal.Font = Setting.LabelErrorFont;
            lblNoSinglePhaseTotal.ForeColor = Setting.LabelErrorForeColor;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormGRegPanelRefYearSetGroup),
                out string frmGRegPanelRefYearSetGroupText)
                    ? frmGRegPanelRefYearSetGroupText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            strAllConfigured =
                labels.TryGetValue(key: nameof(strAllConfigured),
                out strAllConfigured)
                    ? strAllConfigured
                    : String.Empty;

            strNotAllConfigured =
                labels.TryGetValue(key: nameof(strNotAllConfigured),
                out strNotAllConfigured)
                    ? strNotAllConfigured
                    : String.Empty;

            lblNoSinglePhaseTotal.Text =
                (labels.TryGetValue(key: nameof(lblNoSinglePhaseTotal),
                out string lblNoSinglePhaseTotalText)
                    ? lblNoSinglePhaseTotalText
                    : String.Empty)
                .Replace(oldValue: "$0", newValue: Environment.NewLine);

            if (grpCountry != null)
            {
                grpCountry.Text =
                    labels.TryGetValue(key: nameof(grpCountry),
                        out string grpCountryText)
                            ? grpCountryText
                            : String.Empty;
            }

            if (grpStrataSet != null)
            {
                grpStrataSet.Text =
                   labels.TryGetValue(key: nameof(grpStrataSet),
                       out string grpStrataSetText)
                           ? grpStrataSetText
                           : String.Empty;
            }

            if (grpStratum != null)
            {
                grpStratum.Text =
                   labels.TryGetValue(key: nameof(grpStratum),
                       out string grpStratumText)
                           ? grpStratumText
                           : String.Empty;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazení seznamu skupin panelů a referenčních období</para>
        /// <para lang="en">Display a list of panel groups and reference periods</para>
        /// </summary>
        private void DisplayListOfPanelReferenceYearSetGroups()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            pnlWorkSpace.Controls.Clear();

            var countries =
                OnePGroupsForGRegMap.Items
                    .GroupBy(g => g.CountryId)
                    .Select(g => new
                    {
                        CountryId = g.Key,
                        CountryLabelCs = g.FirstOrDefault()?.CountryLabelCs ?? String.Empty,
                        CountryLabelEn = g.FirstOrDefault()?.CountryLabelEn ?? String.Empty,
                        CountryDescriptionCs = g.FirstOrDefault()?.CountryDescriptionCs ?? String.Empty,
                        CountryDescriptionEn = g.FirstOrDefault()?.CountryDescriptionEn ?? String.Empty,
                    });

            countries =
                (LanguageVersion == LanguageVersion.International)
                    ? countries.OrderByDescending(a => a.CountryLabelEn)
                    : (LanguageVersion == LanguageVersion.National)
                        ? countries.OrderByDescending(a => a.CountryLabelCs)
                        : countries.OrderByDescending(a => a.CountryLabelEn);

            // COUNTRY
            foreach (var country in countries)
            {
                grpCountry = new GroupBox()
                {
                    Dock = DockStyle.Top,
                    Font = Setting.GroupBoxFont,
                    ForeColor = Setting.GroupBoxForeColor,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 10),
                    Tag = country,
                    Text = String.Concat(
                        labels.TryGetValue(key: nameof(grpCountry),
                            out string grpCountryText)
                                ? grpCountryText
                                : String.Empty,
                        (char)32,
                        (LanguageVersion == LanguageVersion.National)
                            ? country.CountryLabelCs
                            : country.CountryLabelEn)
                };

                var strataSets =
                    OnePGroupsForGRegMap.Items
                        .Where(a => a.CountryId == country.CountryId)
                        .GroupBy(g => g.StrataSetId)
                        .Select(g => new
                        {
                            StrataSetId = g.Key,
                            StrataSetLabelCs = g.FirstOrDefault()?.StrataSetLabelCs ?? String.Empty,
                            StrataSetLabelEn = g.FirstOrDefault()?.StrataSetLabelEn ?? String.Empty,
                            StrataSetDescriptionCs = g.FirstOrDefault()?.StrataSetDescriptionCs ?? String.Empty,
                            StrataSetDescriptionEn = g.FirstOrDefault()?.StrataSetDescriptionEn ?? String.Empty,
                        });

                strataSets =
                    (LanguageVersion == LanguageVersion.International)
                        ? strataSets.OrderByDescending(a => a.StrataSetLabelEn)
                        : (LanguageVersion == LanguageVersion.National)
                            ? strataSets.OrderByDescending(a => a.StrataSetLabelCs)
                            : strataSets.OrderByDescending(a => a.StrataSetLabelEn);

                // STRATA-SET
                foreach (var strataSet in strataSets)
                {
                    grpStrataSet = new GroupBox()
                    {
                        Dock = DockStyle.Top,
                        Font = Setting.GroupBoxFont,
                        ForeColor = Setting.GroupBoxForeColor,
                        Margin = new Padding(all: 0),
                        Padding = new Padding(all: 10),
                        Tag = strataSet,
                        Text = String.Concat(
                            labels.TryGetValue(key: nameof(grpStrataSet),
                                out string grpStrataSetText)
                                    ? grpStrataSetText
                                    : String.Empty,
                            (char)32,
                            (LanguageVersion == LanguageVersion.National)
                                ? strataSet.StrataSetLabelCs
                                : strataSet.StrataSetLabelEn)
                    };

                    var strata =
                        OnePGroupsForGRegMap.Items
                            .Where(a => a.CountryId == country.CountryId)
                            .Where(a => a.StrataSetId == strataSet.StrataSetId)
                            .GroupBy(g => g.StratumId)
                            .Select(g => new
                            {
                                StratumId = g.Key,
                                StratumLabelCs = g.FirstOrDefault()?.StratumLabelCs ?? String.Empty,
                                StratumLabelEn = g.FirstOrDefault()?.StratumLabelEn ?? String.Empty,
                                StratumDescriptionCs = g.FirstOrDefault()?.StratumDescriptionCs ?? String.Empty,
                                StratumDescriptionEn = g.FirstOrDefault()?.StratumDescriptionEn ?? String.Empty,
                            });

                    strata =
                        (LanguageVersion == LanguageVersion.International)
                            ? strata.OrderByDescending(a => a.StratumLabelEn)
                            : (LanguageVersion == LanguageVersion.National)
                                ? strata.OrderByDescending(a => a.StratumLabelCs)
                                : strata.OrderByDescending(a => a.StratumLabelEn);

                    // STRATUM
                    foreach (var stratum in strata)
                    {
                        grpStratum = new GroupBox()
                        {
                            Dock = DockStyle.Top,
                            Font = Setting.GroupBoxFont,
                            ForeColor = Setting.GroupBoxForeColor,
                            Margin = new Padding(all: 0),
                            Padding = new Padding(all: 10),
                            Tag = stratum,
                            Text = String.Concat(
                                labels.TryGetValue(key: nameof(grpStratum),
                                    out string grpStratumText)
                                        ? grpStratumText
                                        : String.Empty,
                                (char)32,
                                (LanguageVersion == LanguageVersion.National)
                                    ? stratum.StratumLabelCs
                                    : stratum.StratumLabelEn)
                        };

                        var panelReferenceYearSetGroups =
                            OnePGroupsForGRegMap.Items
                                .Where(a => a.CountryId == country.CountryId)
                                .Where(a => a.StrataSetId == strataSet.StrataSetId)
                                .Where(a => a.StratumId == stratum.StratumId)
                                .GroupBy(g => g.PanelRefYearSetGroupId)
                                .Select(g => new
                                {
                                    PanelRefYearSetGroupId = g.Key,
                                    PanelRefYearSetGroupLabelCs = g.FirstOrDefault()?.PanelRefYearSetGroupLabelCs ?? String.Empty,
                                    PanelRefYearSetGroupLabelEn = g.FirstOrDefault()?.PanelRefYearSetGroupLabelEn ?? String.Empty,
                                    PanelRefYearSetGroupDescriptionCs = g.FirstOrDefault()?.PanelRefYearSetGroupDescriptionCs ?? String.Empty,
                                    PanelRefYearSetGroupDescriptionEn = g.FirstOrDefault()?.PanelRefYearSetGroupDescriptionEn ?? String.Empty,
                                    CompleteGroup = g.FirstOrDefault()?.CompleteGroup ?? false,
                                    g.FirstOrDefault()?.Data
                                });

                        panelReferenceYearSetGroups =
                            (LanguageVersion == LanguageVersion.International)
                                ? panelReferenceYearSetGroups
                                    .OrderByDescending(a => a.PanelRefYearSetGroupLabelEn)
                                    .ThenBy(a => a.CompleteGroup)
                                : (LanguageVersion == LanguageVersion.National)
                                    ? panelReferenceYearSetGroups
                                        .OrderByDescending(a => a.PanelRefYearSetGroupLabelCs)
                                        .ThenBy(a => a.CompleteGroup)
                                    : panelReferenceYearSetGroups
                                        .OrderByDescending(a => a.PanelRefYearSetGroupLabelEn)
                                        .ThenBy(a => a.CompleteGroup);

                        // PANEL-REFYEARSET-GROUP
                        foreach (var panelRefYearSetGroup in panelReferenceYearSetGroups)
                        {
                            string strCompleteGroupInfo =
                                panelRefYearSetGroup.CompleteGroup
                                ? strAllConfigured
                                : strNotAllConfigured;

                            string strPanelRefYearSetGroupInfo =
                                (LanguageVersion == LanguageVersion.International)
                                    ? $"{panelRefYearSetGroup.PanelRefYearSetGroupLabelEn} {strCompleteGroupInfo}"
                                    : (LanguageVersion == LanguageVersion.National)
                                        ? $"{panelRefYearSetGroup.PanelRefYearSetGroupLabelCs} {strCompleteGroupInfo}"
                                        : $"{panelRefYearSetGroup.PanelRefYearSetGroupLabelEn} {strCompleteGroupInfo}";

                            RadioButton rdoPanelRefYearSetGroup = new()
                            {
                                Checked = false,
                                Dock = DockStyle.Top,
                                Font = Setting.RadioButtonFont,
                                ForeColor = Setting.RadioButtonForeColor,
                                Enabled = panelRefYearSetGroup.CompleteGroup,
                                Height = 20,
                                Margin = new Padding(all: 0),
                                Padding = new Padding(all: 0),
                                Tag = panelRefYearSetGroup.Data,
                                Text = strPanelRefYearSetGroupInfo
                            };
                            rdoPanelRefYearSetGroup.CheckedChanged += new EventHandler(
                                (sender, e) => { EnableNext(); });

                            rdoPanelRefYearSetGroup.CreateControl();
                            grpStratum.Controls.Add(value: rdoPanelRefYearSetGroup);
                        }

                        if (grpStratum.Controls.OfType<RadioButton>().Where(a => a.Enabled).Any())
                        {
                            grpStratum.Controls.OfType<RadioButton>().Where(a => a.Enabled).Last().Checked = true;
                        }

                        grpStratum.Height = 40 + grpStratum.Controls.OfType<RadioButton>().Sum(rdo => rdo.Height);
                        grpStratum.CreateControl();
                        grpStrataSet.Controls.Add(value: grpStratum);
                    }

                    grpStrataSet.Height = 40 + grpStrataSet.Controls.OfType<GroupBox>().Sum(box => box.Height);
                    grpStrataSet.CreateControl();
                    grpCountry.Controls.Add(value: grpStrataSet);
                }

                grpCountry.Height = 40 + grpCountry.Controls.OfType<GroupBox>().Sum(box => box.Height);
                grpCountry.CreateControl();
                pnlWorkSpace.Controls.Add(value: grpCountry);
            }

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            bool radioButtonsChecked = true;
            foreach (GroupBox grpStratum in StataGroupBoxes)
            {
                if (!grpStratum.Controls.OfType<RadioButton>()
                    .Where(a => a.Enabled && a.Checked).Any())
                {
                    radioButtonsChecked = false;
                    break;
                }
            }
            btnOK.Enabled = radioButtonsChecked;
            lblNoSinglePhaseTotal.Visible = !radioButtonsChecked;
            tlpWorkSpace.RowStyles[tlpWorkSpace.RowStyles.Count - 1].Height = radioButtonsChecked ? 0 : 40;
        }

        #endregion Methods

    }

}