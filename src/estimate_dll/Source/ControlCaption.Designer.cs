﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlCaption
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlEmpty = new System.Windows.Forms.Panel();
            this.lblEmptyCaption = new System.Windows.Forms.Label();
            this.pnlBrief = new System.Windows.Forms.Panel();
            this.lblBriefCaption = new System.Windows.Forms.Label();
            this.pnlExtended = new System.Windows.Forms.Panel();
            this.splExtended = new System.Windows.Forms.SplitContainer();
            this.tlpExtendedLabels = new System.Windows.Forms.TableLayoutPanel();
            this.lblExStateOrChangeLabel = new System.Windows.Forms.Label();
            this.lblExLocalDensityLabel = new System.Windows.Forms.Label();
            this.lblExAttrSubPopulationLabel = new System.Windows.Forms.Label();
            this.lblExAttrAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblExVersionLabel = new System.Windows.Forms.Label();
            this.lblExPopulationLabel = new System.Windows.Forms.Label();
            this.lblExAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblExDefinitionVariantLabel = new System.Windows.Forms.Label();
            this.tlpExtendedValues = new System.Windows.Forms.TableLayoutPanel();
            this.lblExStateOrChangeValue = new System.Windows.Forms.Label();
            this.lblExLocalDensityValue = new System.Windows.Forms.Label();
            this.lblExAttrSubPopulationValue = new System.Windows.Forms.Label();
            this.lblExAttrAreaDomainValue = new System.Windows.Forms.Label();
            this.lblExVersionValue = new System.Windows.Forms.Label();
            this.lblExDefinitionVariantValue = new System.Windows.Forms.Label();
            this.lblExPopulationValue = new System.Windows.Forms.Label();
            this.lblExAreaDomainValue = new System.Windows.Forms.Label();
            this.pnlWide = new System.Windows.Forms.Panel();
            this.tlpWide = new System.Windows.Forms.TableLayoutPanel();
            this.lblWideAttrSubPopulationValue = new System.Windows.Forms.Label();
            this.lblWideAttrSubPopulationLabel = new System.Windows.Forms.Label();
            this.lblWideAttrAreaDomainValue = new System.Windows.Forms.Label();
            this.lblWideLocalDensityValue = new System.Windows.Forms.Label();
            this.lblWideAttrAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblWideStateOrChangeValue = new System.Windows.Forms.Label();
            this.lblWidePopulationValue = new System.Windows.Forms.Label();
            this.lblWideDefinitionVariantValue = new System.Windows.Forms.Label();
            this.lblWideAreaDomainValue = new System.Windows.Forms.Label();
            this.lblWidePopulationLabel = new System.Windows.Forms.Label();
            this.lblWideVersionValue = new System.Windows.Forms.Label();
            this.lblWideAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblWideStateOrChangeLabel = new System.Windows.Forms.Label();
            this.lblWideDefinitionVariantLabel = new System.Windows.Forms.Label();
            this.lblWideLocalDensityLabel = new System.Windows.Forms.Label();
            this.lblWideVersionLabel = new System.Windows.Forms.Label();
            this.lblWidePlaceHolderLabel = new System.Windows.Forms.Label();
            this.lblWidePlaceHolderValue = new System.Windows.Forms.Label();
            this.pnlRatio = new System.Windows.Forms.Panel();
            this.splRatio = new System.Windows.Forms.SplitContainer();
            this.pnlNumerator = new System.Windows.Forms.Panel();
            this.splNumerator = new System.Windows.Forms.SplitContainer();
            this.tlpNumeratorLabels = new System.Windows.Forms.TableLayoutPanel();
            this.lblNumStateOrChangeLabel = new System.Windows.Forms.Label();
            this.lblNumLocalDensityLabel = new System.Windows.Forms.Label();
            this.lblNumAttrSubPopulationLabel = new System.Windows.Forms.Label();
            this.lblNumAttrAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblNumVersionLabel = new System.Windows.Forms.Label();
            this.lblNumPopulationLabel = new System.Windows.Forms.Label();
            this.lblNumAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblNumDefinitionVariantLabel = new System.Windows.Forms.Label();
            this.lblNumeratorCaptionPlaceHolder = new System.Windows.Forms.Label();
            this.tlpNumeratorValues = new System.Windows.Forms.TableLayoutPanel();
            this.lblNumLocalDensityValue = new System.Windows.Forms.Label();
            this.lblNumAttrSubPopulationValue = new System.Windows.Forms.Label();
            this.lblNumStateOrChangeValue = new System.Windows.Forms.Label();
            this.lblNumeratorCaption = new System.Windows.Forms.Label();
            this.lblNumAttrAreaDomainValue = new System.Windows.Forms.Label();
            this.lblNumVersionValue = new System.Windows.Forms.Label();
            this.lblNumPopulationValue = new System.Windows.Forms.Label();
            this.lblNumAreaDomainValue = new System.Windows.Forms.Label();
            this.lblNumDefinitionVariantValue = new System.Windows.Forms.Label();
            this.pnlDenominator = new System.Windows.Forms.Panel();
            this.splDenominator = new System.Windows.Forms.SplitContainer();
            this.tlpDenominatorLabels = new System.Windows.Forms.TableLayoutPanel();
            this.lblDenomVersionLabel = new System.Windows.Forms.Label();
            this.lblDenomAttrSubPopulationLabel = new System.Windows.Forms.Label();
            this.lblDenomAttrAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblDenomPopulationLabel = new System.Windows.Forms.Label();
            this.lblDenomLocalDensityLabel = new System.Windows.Forms.Label();
            this.lblDenomAreaDomainLabel = new System.Windows.Forms.Label();
            this.lblDenomStateOrChangeLabel = new System.Windows.Forms.Label();
            this.lblDenomDefinitionVariantLabel = new System.Windows.Forms.Label();
            this.lblDenominatorCaptionPlaceHolder = new System.Windows.Forms.Label();
            this.tlpDenominatorValues = new System.Windows.Forms.TableLayoutPanel();
            this.lblDenomLocalDensityValue = new System.Windows.Forms.Label();
            this.lblDenomAttrSubPopulationValue = new System.Windows.Forms.Label();
            this.lblDenomStateOrChangeValue = new System.Windows.Forms.Label();
            this.lblDenominatorCaption = new System.Windows.Forms.Label();
            this.lblDenomAttrAreaDomainValue = new System.Windows.Forms.Label();
            this.lblDenomPopulationValue = new System.Windows.Forms.Label();
            this.lblDenomVersionValue = new System.Windows.Forms.Label();
            this.lblDenomAreaDomainValue = new System.Windows.Forms.Label();
            this.lblDenomDefinitionVariantValue = new System.Windows.Forms.Label();
            this.pnlEmpty.SuspendLayout();
            this.pnlBrief.SuspendLayout();
            this.pnlExtended.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splExtended)).BeginInit();
            this.splExtended.Panel1.SuspendLayout();
            this.splExtended.Panel2.SuspendLayout();
            this.splExtended.SuspendLayout();
            this.tlpExtendedLabels.SuspendLayout();
            this.tlpExtendedValues.SuspendLayout();
            this.pnlWide.SuspendLayout();
            this.tlpWide.SuspendLayout();
            this.pnlRatio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splRatio)).BeginInit();
            this.splRatio.Panel1.SuspendLayout();
            this.splRatio.Panel2.SuspendLayout();
            this.splRatio.SuspendLayout();
            this.pnlNumerator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splNumerator)).BeginInit();
            this.splNumerator.Panel1.SuspendLayout();
            this.splNumerator.Panel2.SuspendLayout();
            this.splNumerator.SuspendLayout();
            this.tlpNumeratorLabels.SuspendLayout();
            this.tlpNumeratorValues.SuspendLayout();
            this.pnlDenominator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splDenominator)).BeginInit();
            this.splDenominator.Panel1.SuspendLayout();
            this.splDenominator.Panel2.SuspendLayout();
            this.splDenominator.SuspendLayout();
            this.tlpDenominatorLabels.SuspendLayout();
            this.tlpDenominatorValues.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlEmpty
            // 
            this.pnlEmpty.Controls.Add(this.lblEmptyCaption);
            this.pnlEmpty.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlEmpty.Location = new System.Drawing.Point(0, 0);
            this.pnlEmpty.Margin = new System.Windows.Forms.Padding(0);
            this.pnlEmpty.Name = "pnlEmpty";
            this.pnlEmpty.Size = new System.Drawing.Size(960, 25);
            this.pnlEmpty.TabIndex = 1;
            // 
            // lblEmptyCaption
            // 
            this.lblEmptyCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEmptyCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblEmptyCaption.Location = new System.Drawing.Point(0, 0);
            this.lblEmptyCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblEmptyCaption.Name = "lblEmptyCaption";
            this.lblEmptyCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblEmptyCaption.Size = new System.Drawing.Size(960, 25);
            this.lblEmptyCaption.TabIndex = 0;
            this.lblEmptyCaption.Text = "lblEmptyCaption";
            this.lblEmptyCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlBrief
            // 
            this.pnlBrief.Controls.Add(this.lblBriefCaption);
            this.pnlBrief.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBrief.Location = new System.Drawing.Point(0, 25);
            this.pnlBrief.Margin = new System.Windows.Forms.Padding(0);
            this.pnlBrief.Name = "pnlBrief";
            this.pnlBrief.Size = new System.Drawing.Size(960, 25);
            this.pnlBrief.TabIndex = 2;
            // 
            // lblBriefCaption
            // 
            this.lblBriefCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBriefCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBriefCaption.Location = new System.Drawing.Point(0, 0);
            this.lblBriefCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblBriefCaption.Name = "lblBriefCaption";
            this.lblBriefCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblBriefCaption.Size = new System.Drawing.Size(960, 25);
            this.lblBriefCaption.TabIndex = 1;
            this.lblBriefCaption.Text = "lblBriefCaption";
            this.lblBriefCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlExtended
            // 
            this.pnlExtended.Controls.Add(this.splExtended);
            this.pnlExtended.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlExtended.Location = new System.Drawing.Point(0, 50);
            this.pnlExtended.Margin = new System.Windows.Forms.Padding(0);
            this.pnlExtended.Name = "pnlExtended";
            this.pnlExtended.Size = new System.Drawing.Size(960, 120);
            this.pnlExtended.TabIndex = 5;
            // 
            // splExtended
            // 
            this.splExtended.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splExtended.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splExtended.IsSplitterFixed = true;
            this.splExtended.Location = new System.Drawing.Point(0, 0);
            this.splExtended.Margin = new System.Windows.Forms.Padding(0);
            this.splExtended.Name = "splExtended";
            // 
            // splExtended.Panel1
            // 
            this.splExtended.Panel1.Controls.Add(this.tlpExtendedLabels);
            // 
            // splExtended.Panel2
            // 
            this.splExtended.Panel2.Controls.Add(this.tlpExtendedValues);
            this.splExtended.Size = new System.Drawing.Size(960, 120);
            this.splExtended.SplitterDistance = 150;
            this.splExtended.SplitterWidth = 1;
            this.splExtended.TabIndex = 1;
            // 
            // tlpExtendedLabels
            // 
            this.tlpExtendedLabels.ColumnCount = 1;
            this.tlpExtendedLabels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpExtendedLabels.Controls.Add(this.lblExStateOrChangeLabel, 0, 0);
            this.tlpExtendedLabels.Controls.Add(this.lblExLocalDensityLabel, 0, 1);
            this.tlpExtendedLabels.Controls.Add(this.lblExAttrSubPopulationLabel, 0, 7);
            this.tlpExtendedLabels.Controls.Add(this.lblExAttrAreaDomainLabel, 0, 6);
            this.tlpExtendedLabels.Controls.Add(this.lblExVersionLabel, 0, 2);
            this.tlpExtendedLabels.Controls.Add(this.lblExPopulationLabel, 0, 5);
            this.tlpExtendedLabels.Controls.Add(this.lblExAreaDomainLabel, 0, 4);
            this.tlpExtendedLabels.Controls.Add(this.lblExDefinitionVariantLabel, 0, 3);
            this.tlpExtendedLabels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpExtendedLabels.Location = new System.Drawing.Point(0, 0);
            this.tlpExtendedLabels.Margin = new System.Windows.Forms.Padding(0);
            this.tlpExtendedLabels.Name = "tlpExtendedLabels";
            this.tlpExtendedLabels.RowCount = 9;
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpExtendedLabels.Size = new System.Drawing.Size(150, 120);
            this.tlpExtendedLabels.TabIndex = 0;
            // 
            // lblExStateOrChangeLabel
            // 
            this.lblExStateOrChangeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExStateOrChangeLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExStateOrChangeLabel.Location = new System.Drawing.Point(0, 0);
            this.lblExStateOrChangeLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExStateOrChangeLabel.Name = "lblExStateOrChangeLabel";
            this.lblExStateOrChangeLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExStateOrChangeLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExStateOrChangeLabel.TabIndex = 10;
            this.lblExStateOrChangeLabel.Text = "lblExStateOrChangeLabel";
            this.lblExStateOrChangeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExLocalDensityLabel
            // 
            this.lblExLocalDensityLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExLocalDensityLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExLocalDensityLabel.Location = new System.Drawing.Point(0, 15);
            this.lblExLocalDensityLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExLocalDensityLabel.Name = "lblExLocalDensityLabel";
            this.lblExLocalDensityLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExLocalDensityLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExLocalDensityLabel.TabIndex = 7;
            this.lblExLocalDensityLabel.Text = "lblExLocalDensityLabel";
            this.lblExLocalDensityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExAttrSubPopulationLabel
            // 
            this.lblExAttrSubPopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExAttrSubPopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExAttrSubPopulationLabel.Location = new System.Drawing.Point(0, 105);
            this.lblExAttrSubPopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExAttrSubPopulationLabel.Name = "lblExAttrSubPopulationLabel";
            this.lblExAttrSubPopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExAttrSubPopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExAttrSubPopulationLabel.TabIndex = 9;
            this.lblExAttrSubPopulationLabel.Text = "lblExAttrSubPopulationLabel";
            this.lblExAttrSubPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExAttrAreaDomainLabel
            // 
            this.lblExAttrAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExAttrAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExAttrAreaDomainLabel.Location = new System.Drawing.Point(0, 90);
            this.lblExAttrAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExAttrAreaDomainLabel.Name = "lblExAttrAreaDomainLabel";
            this.lblExAttrAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExAttrAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExAttrAreaDomainLabel.TabIndex = 7;
            this.lblExAttrAreaDomainLabel.Text = "lblExAttrAreaDomainLabel";
            this.lblExAttrAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExVersionLabel
            // 
            this.lblExVersionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExVersionLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExVersionLabel.Location = new System.Drawing.Point(0, 30);
            this.lblExVersionLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExVersionLabel.Name = "lblExVersionLabel";
            this.lblExVersionLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExVersionLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExVersionLabel.TabIndex = 3;
            this.lblExVersionLabel.Text = "lblExVersionLabel";
            this.lblExVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExPopulationLabel
            // 
            this.lblExPopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExPopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExPopulationLabel.Location = new System.Drawing.Point(0, 75);
            this.lblExPopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExPopulationLabel.Name = "lblExPopulationLabel";
            this.lblExPopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExPopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExPopulationLabel.TabIndex = 2;
            this.lblExPopulationLabel.Text = "lblExPopulationLabel";
            this.lblExPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExAreaDomainLabel
            // 
            this.lblExAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExAreaDomainLabel.Location = new System.Drawing.Point(0, 60);
            this.lblExAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExAreaDomainLabel.Name = "lblExAreaDomainLabel";
            this.lblExAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExAreaDomainLabel.TabIndex = 1;
            this.lblExAreaDomainLabel.Text = "lblExAreaDomainLabel";
            this.lblExAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExDefinitionVariantLabel
            // 
            this.lblExDefinitionVariantLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExDefinitionVariantLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblExDefinitionVariantLabel.Location = new System.Drawing.Point(0, 45);
            this.lblExDefinitionVariantLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblExDefinitionVariantLabel.Name = "lblExDefinitionVariantLabel";
            this.lblExDefinitionVariantLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblExDefinitionVariantLabel.Size = new System.Drawing.Size(150, 15);
            this.lblExDefinitionVariantLabel.TabIndex = 11;
            this.lblExDefinitionVariantLabel.Text = "lblExDefinitionVariantLabel";
            this.lblExDefinitionVariantLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpExtendedValues
            // 
            this.tlpExtendedValues.ColumnCount = 1;
            this.tlpExtendedValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpExtendedValues.Controls.Add(this.lblExStateOrChangeValue, 0, 0);
            this.tlpExtendedValues.Controls.Add(this.lblExLocalDensityValue, 0, 1);
            this.tlpExtendedValues.Controls.Add(this.lblExAttrSubPopulationValue, 0, 7);
            this.tlpExtendedValues.Controls.Add(this.lblExAttrAreaDomainValue, 0, 6);
            this.tlpExtendedValues.Controls.Add(this.lblExVersionValue, 0, 2);
            this.tlpExtendedValues.Controls.Add(this.lblExDefinitionVariantValue, 0, 3);
            this.tlpExtendedValues.Controls.Add(this.lblExPopulationValue, 0, 5);
            this.tlpExtendedValues.Controls.Add(this.lblExAreaDomainValue, 0, 4);
            this.tlpExtendedValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpExtendedValues.Location = new System.Drawing.Point(0, 0);
            this.tlpExtendedValues.Margin = new System.Windows.Forms.Padding(0);
            this.tlpExtendedValues.Name = "tlpExtendedValues";
            this.tlpExtendedValues.RowCount = 9;
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpExtendedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpExtendedValues.Size = new System.Drawing.Size(809, 120);
            this.tlpExtendedValues.TabIndex = 1;
            // 
            // lblExStateOrChangeValue
            // 
            this.lblExStateOrChangeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExStateOrChangeValue.Location = new System.Drawing.Point(0, 0);
            this.lblExStateOrChangeValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExStateOrChangeValue.Name = "lblExStateOrChangeValue";
            this.lblExStateOrChangeValue.Size = new System.Drawing.Size(809, 15);
            this.lblExStateOrChangeValue.TabIndex = 12;
            this.lblExStateOrChangeValue.Text = "lblExStateOrChangeValue";
            this.lblExStateOrChangeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExLocalDensityValue
            // 
            this.lblExLocalDensityValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExLocalDensityValue.Location = new System.Drawing.Point(0, 15);
            this.lblExLocalDensityValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExLocalDensityValue.Name = "lblExLocalDensityValue";
            this.lblExLocalDensityValue.Size = new System.Drawing.Size(809, 15);
            this.lblExLocalDensityValue.TabIndex = 11;
            this.lblExLocalDensityValue.Text = "lblExLocalDensityValue";
            this.lblExLocalDensityValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExAttrSubPopulationValue
            // 
            this.lblExAttrSubPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExAttrSubPopulationValue.Location = new System.Drawing.Point(0, 105);
            this.lblExAttrSubPopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExAttrSubPopulationValue.Name = "lblExAttrSubPopulationValue";
            this.lblExAttrSubPopulationValue.Size = new System.Drawing.Size(809, 15);
            this.lblExAttrSubPopulationValue.TabIndex = 10;
            this.lblExAttrSubPopulationValue.Text = "lblExAttrSubPopulationValue";
            this.lblExAttrSubPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExAttrAreaDomainValue
            // 
            this.lblExAttrAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExAttrAreaDomainValue.Location = new System.Drawing.Point(0, 90);
            this.lblExAttrAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExAttrAreaDomainValue.Name = "lblExAttrAreaDomainValue";
            this.lblExAttrAreaDomainValue.Size = new System.Drawing.Size(809, 15);
            this.lblExAttrAreaDomainValue.TabIndex = 8;
            this.lblExAttrAreaDomainValue.Text = "lblExAttrAreaDomainValue";
            this.lblExAttrAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExVersionValue
            // 
            this.lblExVersionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExVersionValue.Location = new System.Drawing.Point(0, 30);
            this.lblExVersionValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExVersionValue.Name = "lblExVersionValue";
            this.lblExVersionValue.Size = new System.Drawing.Size(809, 15);
            this.lblExVersionValue.TabIndex = 3;
            this.lblExVersionValue.Text = "lblExVersionValue";
            this.lblExVersionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExDefinitionVariantValue
            // 
            this.lblExDefinitionVariantValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExDefinitionVariantValue.Location = new System.Drawing.Point(0, 45);
            this.lblExDefinitionVariantValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExDefinitionVariantValue.Name = "lblExDefinitionVariantValue";
            this.lblExDefinitionVariantValue.Size = new System.Drawing.Size(809, 15);
            this.lblExDefinitionVariantValue.TabIndex = 0;
            this.lblExDefinitionVariantValue.Text = "lblExDefinitionVariantValue";
            this.lblExDefinitionVariantValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExPopulationValue
            // 
            this.lblExPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExPopulationValue.Location = new System.Drawing.Point(0, 75);
            this.lblExPopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExPopulationValue.Name = "lblExPopulationValue";
            this.lblExPopulationValue.Size = new System.Drawing.Size(809, 15);
            this.lblExPopulationValue.TabIndex = 2;
            this.lblExPopulationValue.Text = "lblExPopulationValue";
            this.lblExPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExAreaDomainValue
            // 
            this.lblExAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblExAreaDomainValue.Location = new System.Drawing.Point(0, 60);
            this.lblExAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblExAreaDomainValue.Name = "lblExAreaDomainValue";
            this.lblExAreaDomainValue.Size = new System.Drawing.Size(809, 15);
            this.lblExAreaDomainValue.TabIndex = 1;
            this.lblExAreaDomainValue.Text = "lblExAreaDomainValue";
            this.lblExAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlWide
            // 
            this.pnlWide.Controls.Add(this.tlpWide);
            this.pnlWide.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlWide.Location = new System.Drawing.Point(0, 170);
            this.pnlWide.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWide.Name = "pnlWide";
            this.pnlWide.Size = new System.Drawing.Size(960, 45);
            this.pnlWide.TabIndex = 7;
            // 
            // tlpWide
            // 
            this.tlpWide.ColumnCount = 6;
            this.tlpWide.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpWide.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpWide.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpWide.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpWide.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpWide.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpWide.Controls.Add(this.lblWideAttrSubPopulationValue, 5, 1);
            this.tlpWide.Controls.Add(this.lblWideAttrSubPopulationLabel, 4, 1);
            this.tlpWide.Controls.Add(this.lblWideAttrAreaDomainValue, 5, 0);
            this.tlpWide.Controls.Add(this.lblWideLocalDensityValue, 1, 1);
            this.tlpWide.Controls.Add(this.lblWideAttrAreaDomainLabel, 4, 0);
            this.tlpWide.Controls.Add(this.lblWideStateOrChangeValue, 1, 0);
            this.tlpWide.Controls.Add(this.lblWidePopulationValue, 3, 2);
            this.tlpWide.Controls.Add(this.lblWideDefinitionVariantValue, 3, 0);
            this.tlpWide.Controls.Add(this.lblWideAreaDomainValue, 3, 1);
            this.tlpWide.Controls.Add(this.lblWidePopulationLabel, 2, 2);
            this.tlpWide.Controls.Add(this.lblWideVersionValue, 1, 2);
            this.tlpWide.Controls.Add(this.lblWideAreaDomainLabel, 2, 1);
            this.tlpWide.Controls.Add(this.lblWideStateOrChangeLabel, 0, 0);
            this.tlpWide.Controls.Add(this.lblWideDefinitionVariantLabel, 2, 0);
            this.tlpWide.Controls.Add(this.lblWideLocalDensityLabel, 0, 1);
            this.tlpWide.Controls.Add(this.lblWideVersionLabel, 0, 2);
            this.tlpWide.Controls.Add(this.lblWidePlaceHolderLabel, 4, 2);
            this.tlpWide.Controls.Add(this.lblWidePlaceHolderValue, 5, 2);
            this.tlpWide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpWide.Location = new System.Drawing.Point(0, 0);
            this.tlpWide.Margin = new System.Windows.Forms.Padding(0);
            this.tlpWide.Name = "tlpWide";
            this.tlpWide.RowCount = 4;
            this.tlpWide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpWide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpWide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpWide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWide.Size = new System.Drawing.Size(960, 45);
            this.tlpWide.TabIndex = 0;
            // 
            // lblWideAttrSubPopulationValue
            // 
            this.lblWideAttrSubPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideAttrSubPopulationValue.Location = new System.Drawing.Point(790, 15);
            this.lblWideAttrSubPopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideAttrSubPopulationValue.Name = "lblWideAttrSubPopulationValue";
            this.lblWideAttrSubPopulationValue.Size = new System.Drawing.Size(170, 15);
            this.lblWideAttrSubPopulationValue.TabIndex = 10;
            this.lblWideAttrSubPopulationValue.Text = "lblWideAttrSubPopulationValue";
            this.lblWideAttrSubPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideAttrSubPopulationLabel
            // 
            this.lblWideAttrSubPopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideAttrSubPopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWideAttrSubPopulationLabel.Location = new System.Drawing.Point(640, 15);
            this.lblWideAttrSubPopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideAttrSubPopulationLabel.Name = "lblWideAttrSubPopulationLabel";
            this.lblWideAttrSubPopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWideAttrSubPopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWideAttrSubPopulationLabel.TabIndex = 9;
            this.lblWideAttrSubPopulationLabel.Text = "lblWideAttrSubPopulationLabel";
            this.lblWideAttrSubPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideAttrAreaDomainValue
            // 
            this.lblWideAttrAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideAttrAreaDomainValue.Location = new System.Drawing.Point(790, 0);
            this.lblWideAttrAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideAttrAreaDomainValue.Name = "lblWideAttrAreaDomainValue";
            this.lblWideAttrAreaDomainValue.Size = new System.Drawing.Size(170, 15);
            this.lblWideAttrAreaDomainValue.TabIndex = 8;
            this.lblWideAttrAreaDomainValue.Text = "lblWideAttrAreaDomainValue";
            this.lblWideAttrAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideLocalDensityValue
            // 
            this.lblWideLocalDensityValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideLocalDensityValue.Location = new System.Drawing.Point(150, 15);
            this.lblWideLocalDensityValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideLocalDensityValue.Name = "lblWideLocalDensityValue";
            this.lblWideLocalDensityValue.Size = new System.Drawing.Size(170, 15);
            this.lblWideLocalDensityValue.TabIndex = 11;
            this.lblWideLocalDensityValue.Text = "lblWideLocalDensityValue";
            this.lblWideLocalDensityValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideAttrAreaDomainLabel
            // 
            this.lblWideAttrAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideAttrAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWideAttrAreaDomainLabel.Location = new System.Drawing.Point(640, 0);
            this.lblWideAttrAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideAttrAreaDomainLabel.Name = "lblWideAttrAreaDomainLabel";
            this.lblWideAttrAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWideAttrAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWideAttrAreaDomainLabel.TabIndex = 7;
            this.lblWideAttrAreaDomainLabel.Text = "lblWideAttrAreaDomainLabel";
            this.lblWideAttrAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideStateOrChangeValue
            // 
            this.lblWideStateOrChangeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideStateOrChangeValue.Location = new System.Drawing.Point(150, 0);
            this.lblWideStateOrChangeValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideStateOrChangeValue.Name = "lblWideStateOrChangeValue";
            this.lblWideStateOrChangeValue.Size = new System.Drawing.Size(170, 15);
            this.lblWideStateOrChangeValue.TabIndex = 12;
            this.lblWideStateOrChangeValue.Text = "lblWideStateOrChangeValue";
            this.lblWideStateOrChangeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWidePopulationValue
            // 
            this.lblWidePopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWidePopulationValue.Location = new System.Drawing.Point(470, 30);
            this.lblWidePopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWidePopulationValue.Name = "lblWidePopulationValue";
            this.lblWidePopulationValue.Size = new System.Drawing.Size(170, 15);
            this.lblWidePopulationValue.TabIndex = 2;
            this.lblWidePopulationValue.Text = "lblWidePopulationValue";
            this.lblWidePopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideDefinitionVariantValue
            // 
            this.lblWideDefinitionVariantValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideDefinitionVariantValue.Location = new System.Drawing.Point(470, 0);
            this.lblWideDefinitionVariantValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideDefinitionVariantValue.Name = "lblWideDefinitionVariantValue";
            this.lblWideDefinitionVariantValue.Size = new System.Drawing.Size(170, 15);
            this.lblWideDefinitionVariantValue.TabIndex = 0;
            this.lblWideDefinitionVariantValue.Text = "lblWideDefinitionVariantValue";
            this.lblWideDefinitionVariantValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideAreaDomainValue
            // 
            this.lblWideAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideAreaDomainValue.Location = new System.Drawing.Point(470, 15);
            this.lblWideAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideAreaDomainValue.Name = "lblWideAreaDomainValue";
            this.lblWideAreaDomainValue.Size = new System.Drawing.Size(170, 15);
            this.lblWideAreaDomainValue.TabIndex = 1;
            this.lblWideAreaDomainValue.Text = "lblWideAreaDomainValue";
            this.lblWideAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWidePopulationLabel
            // 
            this.lblWidePopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWidePopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWidePopulationLabel.Location = new System.Drawing.Point(320, 30);
            this.lblWidePopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWidePopulationLabel.Name = "lblWidePopulationLabel";
            this.lblWidePopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWidePopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWidePopulationLabel.TabIndex = 2;
            this.lblWidePopulationLabel.Text = "lblWidePopulationLabel";
            this.lblWidePopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideVersionValue
            // 
            this.lblWideVersionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideVersionValue.Location = new System.Drawing.Point(150, 30);
            this.lblWideVersionValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideVersionValue.Name = "lblWideVersionValue";
            this.lblWideVersionValue.Size = new System.Drawing.Size(170, 15);
            this.lblWideVersionValue.TabIndex = 3;
            this.lblWideVersionValue.Text = "lblWideVersionValue";
            this.lblWideVersionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideAreaDomainLabel
            // 
            this.lblWideAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWideAreaDomainLabel.Location = new System.Drawing.Point(320, 15);
            this.lblWideAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideAreaDomainLabel.Name = "lblWideAreaDomainLabel";
            this.lblWideAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWideAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWideAreaDomainLabel.TabIndex = 1;
            this.lblWideAreaDomainLabel.Text = "lblWideAreaDomainLabel";
            this.lblWideAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideStateOrChangeLabel
            // 
            this.lblWideStateOrChangeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideStateOrChangeLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWideStateOrChangeLabel.Location = new System.Drawing.Point(0, 0);
            this.lblWideStateOrChangeLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideStateOrChangeLabel.Name = "lblWideStateOrChangeLabel";
            this.lblWideStateOrChangeLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWideStateOrChangeLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWideStateOrChangeLabel.TabIndex = 11;
            this.lblWideStateOrChangeLabel.Text = "lblWideStateOrChangeLabel";
            this.lblWideStateOrChangeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideDefinitionVariantLabel
            // 
            this.lblWideDefinitionVariantLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideDefinitionVariantLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWideDefinitionVariantLabel.Location = new System.Drawing.Point(320, 0);
            this.lblWideDefinitionVariantLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideDefinitionVariantLabel.Name = "lblWideDefinitionVariantLabel";
            this.lblWideDefinitionVariantLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWideDefinitionVariantLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWideDefinitionVariantLabel.TabIndex = 11;
            this.lblWideDefinitionVariantLabel.Text = "lblWideDefinitionVariantLabel";
            this.lblWideDefinitionVariantLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideLocalDensityLabel
            // 
            this.lblWideLocalDensityLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideLocalDensityLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWideLocalDensityLabel.Location = new System.Drawing.Point(0, 15);
            this.lblWideLocalDensityLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideLocalDensityLabel.Name = "lblWideLocalDensityLabel";
            this.lblWideLocalDensityLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWideLocalDensityLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWideLocalDensityLabel.TabIndex = 12;
            this.lblWideLocalDensityLabel.Text = "lblWideLocalDensityLabel";
            this.lblWideLocalDensityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWideVersionLabel
            // 
            this.lblWideVersionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWideVersionLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWideVersionLabel.Location = new System.Drawing.Point(0, 30);
            this.lblWideVersionLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWideVersionLabel.Name = "lblWideVersionLabel";
            this.lblWideVersionLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWideVersionLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWideVersionLabel.TabIndex = 13;
            this.lblWideVersionLabel.Text = "lblWideVersionLabel";
            this.lblWideVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWidePlaceHolderLabel
            // 
            this.lblWidePlaceHolderLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWidePlaceHolderLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblWidePlaceHolderLabel.Location = new System.Drawing.Point(640, 30);
            this.lblWidePlaceHolderLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblWidePlaceHolderLabel.Name = "lblWidePlaceHolderLabel";
            this.lblWidePlaceHolderLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblWidePlaceHolderLabel.Size = new System.Drawing.Size(150, 15);
            this.lblWidePlaceHolderLabel.TabIndex = 15;
            this.lblWidePlaceHolderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWidePlaceHolderValue
            // 
            this.lblWidePlaceHolderValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWidePlaceHolderValue.Location = new System.Drawing.Point(790, 30);
            this.lblWidePlaceHolderValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblWidePlaceHolderValue.Name = "lblWidePlaceHolderValue";
            this.lblWidePlaceHolderValue.Size = new System.Drawing.Size(170, 15);
            this.lblWidePlaceHolderValue.TabIndex = 16;
            this.lblWidePlaceHolderValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlRatio
            // 
            this.pnlRatio.Controls.Add(this.splRatio);
            this.pnlRatio.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRatio.Location = new System.Drawing.Point(0, 215);
            this.pnlRatio.Margin = new System.Windows.Forms.Padding(0);
            this.pnlRatio.Name = "pnlRatio";
            this.pnlRatio.Size = new System.Drawing.Size(960, 135);
            this.pnlRatio.TabIndex = 9;
            // 
            // splRatio
            // 
            this.splRatio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splRatio.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splRatio.Location = new System.Drawing.Point(0, 0);
            this.splRatio.Margin = new System.Windows.Forms.Padding(0);
            this.splRatio.Name = "splRatio";
            // 
            // splRatio.Panel1
            // 
            this.splRatio.Panel1.Controls.Add(this.pnlNumerator);
            // 
            // splRatio.Panel2
            // 
            this.splRatio.Panel2.Controls.Add(this.pnlDenominator);
            this.splRatio.Size = new System.Drawing.Size(960, 135);
            this.splRatio.SplitterDistance = 320;
            this.splRatio.SplitterWidth = 1;
            this.splRatio.TabIndex = 0;
            // 
            // pnlNumerator
            // 
            this.pnlNumerator.Controls.Add(this.splNumerator);
            this.pnlNumerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNumerator.Location = new System.Drawing.Point(0, 0);
            this.pnlNumerator.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNumerator.Name = "pnlNumerator";
            this.pnlNumerator.Size = new System.Drawing.Size(320, 135);
            this.pnlNumerator.TabIndex = 6;
            // 
            // splNumerator
            // 
            this.splNumerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splNumerator.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splNumerator.IsSplitterFixed = true;
            this.splNumerator.Location = new System.Drawing.Point(0, 0);
            this.splNumerator.Margin = new System.Windows.Forms.Padding(0);
            this.splNumerator.Name = "splNumerator";
            // 
            // splNumerator.Panel1
            // 
            this.splNumerator.Panel1.Controls.Add(this.tlpNumeratorLabels);
            // 
            // splNumerator.Panel2
            // 
            this.splNumerator.Panel2.Controls.Add(this.tlpNumeratorValues);
            this.splNumerator.Size = new System.Drawing.Size(320, 135);
            this.splNumerator.SplitterDistance = 150;
            this.splNumerator.SplitterWidth = 1;
            this.splNumerator.TabIndex = 1;
            // 
            // tlpNumeratorLabels
            // 
            this.tlpNumeratorLabels.ColumnCount = 1;
            this.tlpNumeratorLabels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpNumeratorLabels.Controls.Add(this.lblNumStateOrChangeLabel, 0, 1);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumLocalDensityLabel, 0, 2);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumAttrSubPopulationLabel, 0, 8);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumAttrAreaDomainLabel, 0, 7);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumVersionLabel, 0, 3);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumPopulationLabel, 0, 6);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumAreaDomainLabel, 0, 5);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumDefinitionVariantLabel, 0, 4);
            this.tlpNumeratorLabels.Controls.Add(this.lblNumeratorCaptionPlaceHolder, 0, 0);
            this.tlpNumeratorLabels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpNumeratorLabels.Location = new System.Drawing.Point(0, 0);
            this.tlpNumeratorLabels.Margin = new System.Windows.Forms.Padding(0);
            this.tlpNumeratorLabels.Name = "tlpNumeratorLabels";
            this.tlpNumeratorLabels.RowCount = 10;
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpNumeratorLabels.Size = new System.Drawing.Size(150, 135);
            this.tlpNumeratorLabels.TabIndex = 0;
            // 
            // lblNumStateOrChangeLabel
            // 
            this.lblNumStateOrChangeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumStateOrChangeLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumStateOrChangeLabel.Location = new System.Drawing.Point(0, 15);
            this.lblNumStateOrChangeLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumStateOrChangeLabel.Name = "lblNumStateOrChangeLabel";
            this.lblNumStateOrChangeLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumStateOrChangeLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumStateOrChangeLabel.TabIndex = 15;
            this.lblNumStateOrChangeLabel.Text = "lblNumStateOrChangeLabel";
            this.lblNumStateOrChangeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumLocalDensityLabel
            // 
            this.lblNumLocalDensityLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumLocalDensityLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumLocalDensityLabel.Location = new System.Drawing.Point(0, 30);
            this.lblNumLocalDensityLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumLocalDensityLabel.Name = "lblNumLocalDensityLabel";
            this.lblNumLocalDensityLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumLocalDensityLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumLocalDensityLabel.TabIndex = 14;
            this.lblNumLocalDensityLabel.Text = "lblNumLocalDensityLabel";
            this.lblNumLocalDensityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumAttrSubPopulationLabel
            // 
            this.lblNumAttrSubPopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumAttrSubPopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumAttrSubPopulationLabel.Location = new System.Drawing.Point(0, 120);
            this.lblNumAttrSubPopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumAttrSubPopulationLabel.Name = "lblNumAttrSubPopulationLabel";
            this.lblNumAttrSubPopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumAttrSubPopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumAttrSubPopulationLabel.TabIndex = 13;
            this.lblNumAttrSubPopulationLabel.Text = "lblNumAttrSubPopulationLabel";
            this.lblNumAttrSubPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumAttrAreaDomainLabel
            // 
            this.lblNumAttrAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumAttrAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumAttrAreaDomainLabel.Location = new System.Drawing.Point(0, 105);
            this.lblNumAttrAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumAttrAreaDomainLabel.Name = "lblNumAttrAreaDomainLabel";
            this.lblNumAttrAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumAttrAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumAttrAreaDomainLabel.TabIndex = 12;
            this.lblNumAttrAreaDomainLabel.Text = "lblNumAttrAreaDomainLabel";
            this.lblNumAttrAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumVersionLabel
            // 
            this.lblNumVersionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumVersionLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumVersionLabel.Location = new System.Drawing.Point(0, 45);
            this.lblNumVersionLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumVersionLabel.Name = "lblNumVersionLabel";
            this.lblNumVersionLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumVersionLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumVersionLabel.TabIndex = 3;
            this.lblNumVersionLabel.Text = "lblNumVersionLabel";
            this.lblNumVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumPopulationLabel
            // 
            this.lblNumPopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumPopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumPopulationLabel.Location = new System.Drawing.Point(0, 90);
            this.lblNumPopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumPopulationLabel.Name = "lblNumPopulationLabel";
            this.lblNumPopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumPopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumPopulationLabel.TabIndex = 2;
            this.lblNumPopulationLabel.Text = "lblNumPopulationLabel";
            this.lblNumPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumAreaDomainLabel
            // 
            this.lblNumAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumAreaDomainLabel.Location = new System.Drawing.Point(0, 75);
            this.lblNumAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumAreaDomainLabel.Name = "lblNumAreaDomainLabel";
            this.lblNumAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumAreaDomainLabel.TabIndex = 1;
            this.lblNumAreaDomainLabel.Text = "lblNumAreaDomainLabel";
            this.lblNumAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumDefinitionVariantLabel
            // 
            this.lblNumDefinitionVariantLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumDefinitionVariantLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumDefinitionVariantLabel.Location = new System.Drawing.Point(0, 60);
            this.lblNumDefinitionVariantLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumDefinitionVariantLabel.Name = "lblNumDefinitionVariantLabel";
            this.lblNumDefinitionVariantLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumDefinitionVariantLabel.Size = new System.Drawing.Size(150, 15);
            this.lblNumDefinitionVariantLabel.TabIndex = 0;
            this.lblNumDefinitionVariantLabel.Text = "lblNumDefinitionVariantLabel";
            this.lblNumDefinitionVariantLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorCaptionPlaceHolder
            // 
            this.lblNumeratorCaptionPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumeratorCaptionPlaceHolder.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumeratorCaptionPlaceHolder.Location = new System.Drawing.Point(0, 0);
            this.lblNumeratorCaptionPlaceHolder.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumeratorCaptionPlaceHolder.Name = "lblNumeratorCaptionPlaceHolder";
            this.lblNumeratorCaptionPlaceHolder.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNumeratorCaptionPlaceHolder.Size = new System.Drawing.Size(150, 15);
            this.lblNumeratorCaptionPlaceHolder.TabIndex = 16;
            this.lblNumeratorCaptionPlaceHolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpNumeratorValues
            // 
            this.tlpNumeratorValues.ColumnCount = 1;
            this.tlpNumeratorValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpNumeratorValues.Controls.Add(this.lblNumLocalDensityValue, 0, 2);
            this.tlpNumeratorValues.Controls.Add(this.lblNumAttrSubPopulationValue, 0, 8);
            this.tlpNumeratorValues.Controls.Add(this.lblNumStateOrChangeValue, 0, 1);
            this.tlpNumeratorValues.Controls.Add(this.lblNumeratorCaption, 0, 0);
            this.tlpNumeratorValues.Controls.Add(this.lblNumAttrAreaDomainValue, 0, 7);
            this.tlpNumeratorValues.Controls.Add(this.lblNumVersionValue, 0, 3);
            this.tlpNumeratorValues.Controls.Add(this.lblNumPopulationValue, 0, 6);
            this.tlpNumeratorValues.Controls.Add(this.lblNumAreaDomainValue, 0, 5);
            this.tlpNumeratorValues.Controls.Add(this.lblNumDefinitionVariantValue, 0, 4);
            this.tlpNumeratorValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpNumeratorValues.Location = new System.Drawing.Point(0, 0);
            this.tlpNumeratorValues.Margin = new System.Windows.Forms.Padding(0);
            this.tlpNumeratorValues.Name = "tlpNumeratorValues";
            this.tlpNumeratorValues.RowCount = 10;
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpNumeratorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpNumeratorValues.Size = new System.Drawing.Size(169, 135);
            this.tlpNumeratorValues.TabIndex = 1;
            // 
            // lblNumLocalDensityValue
            // 
            this.lblNumLocalDensityValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumLocalDensityValue.Location = new System.Drawing.Point(0, 30);
            this.lblNumLocalDensityValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumLocalDensityValue.Name = "lblNumLocalDensityValue";
            this.lblNumLocalDensityValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumLocalDensityValue.TabIndex = 14;
            this.lblNumLocalDensityValue.Text = "lblNumLocalDensityValue";
            this.lblNumLocalDensityValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumAttrSubPopulationValue
            // 
            this.lblNumAttrSubPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumAttrSubPopulationValue.Location = new System.Drawing.Point(0, 120);
            this.lblNumAttrSubPopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumAttrSubPopulationValue.Name = "lblNumAttrSubPopulationValue";
            this.lblNumAttrSubPopulationValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumAttrSubPopulationValue.TabIndex = 13;
            this.lblNumAttrSubPopulationValue.Text = "lblNumAttrSubPopulationValue";
            this.lblNumAttrSubPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumStateOrChangeValue
            // 
            this.lblNumStateOrChangeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumStateOrChangeValue.Location = new System.Drawing.Point(0, 15);
            this.lblNumStateOrChangeValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumStateOrChangeValue.Name = "lblNumStateOrChangeValue";
            this.lblNumStateOrChangeValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumStateOrChangeValue.TabIndex = 15;
            this.lblNumStateOrChangeValue.Text = "lblNumStateOrChangeValue";
            this.lblNumStateOrChangeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorCaption
            // 
            this.lblNumeratorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumeratorCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNumeratorCaption.Location = new System.Drawing.Point(0, 0);
            this.lblNumeratorCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumeratorCaption.Name = "lblNumeratorCaption";
            this.lblNumeratorCaption.Size = new System.Drawing.Size(169, 15);
            this.lblNumeratorCaption.TabIndex = 14;
            this.lblNumeratorCaption.Text = "lblNumeratorCaption";
            this.lblNumeratorCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumAttrAreaDomainValue
            // 
            this.lblNumAttrAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumAttrAreaDomainValue.Location = new System.Drawing.Point(0, 105);
            this.lblNumAttrAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumAttrAreaDomainValue.Name = "lblNumAttrAreaDomainValue";
            this.lblNumAttrAreaDomainValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumAttrAreaDomainValue.TabIndex = 13;
            this.lblNumAttrAreaDomainValue.Text = "lblNumAttrAreaDomainValue";
            this.lblNumAttrAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumVersionValue
            // 
            this.lblNumVersionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumVersionValue.Location = new System.Drawing.Point(0, 45);
            this.lblNumVersionValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumVersionValue.Name = "lblNumVersionValue";
            this.lblNumVersionValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumVersionValue.TabIndex = 3;
            this.lblNumVersionValue.Text = "lblNumVersionValue";
            this.lblNumVersionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumPopulationValue
            // 
            this.lblNumPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumPopulationValue.Location = new System.Drawing.Point(0, 90);
            this.lblNumPopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumPopulationValue.Name = "lblNumPopulationValue";
            this.lblNumPopulationValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumPopulationValue.TabIndex = 2;
            this.lblNumPopulationValue.Text = "lblNumPopulationValue";
            this.lblNumPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumAreaDomainValue
            // 
            this.lblNumAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumAreaDomainValue.Location = new System.Drawing.Point(0, 75);
            this.lblNumAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumAreaDomainValue.Name = "lblNumAreaDomainValue";
            this.lblNumAreaDomainValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumAreaDomainValue.TabIndex = 1;
            this.lblNumAreaDomainValue.Text = "lblNumAreaDomainValue";
            this.lblNumAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumDefinitionVariantValue
            // 
            this.lblNumDefinitionVariantValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumDefinitionVariantValue.Location = new System.Drawing.Point(0, 60);
            this.lblNumDefinitionVariantValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumDefinitionVariantValue.Name = "lblNumDefinitionVariantValue";
            this.lblNumDefinitionVariantValue.Size = new System.Drawing.Size(169, 15);
            this.lblNumDefinitionVariantValue.TabIndex = 0;
            this.lblNumDefinitionVariantValue.Text = "lblNumDefinitionVariantValue";
            this.lblNumDefinitionVariantValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlDenominator
            // 
            this.pnlDenominator.Controls.Add(this.splDenominator);
            this.pnlDenominator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDenominator.Location = new System.Drawing.Point(0, 0);
            this.pnlDenominator.Margin = new System.Windows.Forms.Padding(0);
            this.pnlDenominator.Name = "pnlDenominator";
            this.pnlDenominator.Size = new System.Drawing.Size(639, 135);
            this.pnlDenominator.TabIndex = 6;
            // 
            // splDenominator
            // 
            this.splDenominator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splDenominator.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splDenominator.IsSplitterFixed = true;
            this.splDenominator.Location = new System.Drawing.Point(0, 0);
            this.splDenominator.Margin = new System.Windows.Forms.Padding(0);
            this.splDenominator.Name = "splDenominator";
            // 
            // splDenominator.Panel1
            // 
            this.splDenominator.Panel1.Controls.Add(this.tlpDenominatorLabels);
            // 
            // splDenominator.Panel2
            // 
            this.splDenominator.Panel2.Controls.Add(this.tlpDenominatorValues);
            this.splDenominator.Size = new System.Drawing.Size(639, 135);
            this.splDenominator.SplitterDistance = 150;
            this.splDenominator.SplitterWidth = 1;
            this.splDenominator.TabIndex = 11;
            // 
            // tlpDenominatorLabels
            // 
            this.tlpDenominatorLabels.ColumnCount = 1;
            this.tlpDenominatorLabels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomVersionLabel, 0, 3);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomAttrSubPopulationLabel, 0, 8);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomAttrAreaDomainLabel, 0, 7);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomPopulationLabel, 0, 6);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomLocalDensityLabel, 0, 2);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomAreaDomainLabel, 0, 5);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomStateOrChangeLabel, 0, 1);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenomDefinitionVariantLabel, 0, 4);
            this.tlpDenominatorLabels.Controls.Add(this.lblDenominatorCaptionPlaceHolder, 0, 0);
            this.tlpDenominatorLabels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDenominatorLabels.Location = new System.Drawing.Point(0, 0);
            this.tlpDenominatorLabels.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDenominatorLabels.Name = "tlpDenominatorLabels";
            this.tlpDenominatorLabels.RowCount = 10;
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorLabels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDenominatorLabels.Size = new System.Drawing.Size(150, 135);
            this.tlpDenominatorLabels.TabIndex = 1;
            // 
            // lblDenomVersionLabel
            // 
            this.lblDenomVersionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomVersionLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomVersionLabel.Location = new System.Drawing.Point(0, 45);
            this.lblDenomVersionLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomVersionLabel.Name = "lblDenomVersionLabel";
            this.lblDenomVersionLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomVersionLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomVersionLabel.TabIndex = 3;
            this.lblDenomVersionLabel.Text = "lblDenomVersionLabel";
            this.lblDenomVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomAttrSubPopulationLabel
            // 
            this.lblDenomAttrSubPopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomAttrSubPopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomAttrSubPopulationLabel.Location = new System.Drawing.Point(0, 120);
            this.lblDenomAttrSubPopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomAttrSubPopulationLabel.Name = "lblDenomAttrSubPopulationLabel";
            this.lblDenomAttrSubPopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomAttrSubPopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomAttrSubPopulationLabel.TabIndex = 17;
            this.lblDenomAttrSubPopulationLabel.Text = "lblDenomAttrSubPopulationLabel";
            this.lblDenomAttrSubPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomAttrAreaDomainLabel
            // 
            this.lblDenomAttrAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomAttrAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomAttrAreaDomainLabel.Location = new System.Drawing.Point(0, 105);
            this.lblDenomAttrAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomAttrAreaDomainLabel.Name = "lblDenomAttrAreaDomainLabel";
            this.lblDenomAttrAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomAttrAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomAttrAreaDomainLabel.TabIndex = 11;
            this.lblDenomAttrAreaDomainLabel.Text = "lblDenomAttrAreaDomainLabel";
            this.lblDenomAttrAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomPopulationLabel
            // 
            this.lblDenomPopulationLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomPopulationLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomPopulationLabel.Location = new System.Drawing.Point(0, 90);
            this.lblDenomPopulationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomPopulationLabel.Name = "lblDenomPopulationLabel";
            this.lblDenomPopulationLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomPopulationLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomPopulationLabel.TabIndex = 2;
            this.lblDenomPopulationLabel.Text = "lblDenomPopulationLabel";
            this.lblDenomPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomLocalDensityLabel
            // 
            this.lblDenomLocalDensityLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomLocalDensityLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomLocalDensityLabel.Location = new System.Drawing.Point(0, 30);
            this.lblDenomLocalDensityLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomLocalDensityLabel.Name = "lblDenomLocalDensityLabel";
            this.lblDenomLocalDensityLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomLocalDensityLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomLocalDensityLabel.TabIndex = 19;
            this.lblDenomLocalDensityLabel.Text = "lblDenomLocalDensityLabel";
            this.lblDenomLocalDensityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomAreaDomainLabel
            // 
            this.lblDenomAreaDomainLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomAreaDomainLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomAreaDomainLabel.Location = new System.Drawing.Point(0, 75);
            this.lblDenomAreaDomainLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomAreaDomainLabel.Name = "lblDenomAreaDomainLabel";
            this.lblDenomAreaDomainLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomAreaDomainLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomAreaDomainLabel.TabIndex = 1;
            this.lblDenomAreaDomainLabel.Text = "lblDenomAreaDomainLabel";
            this.lblDenomAreaDomainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomStateOrChangeLabel
            // 
            this.lblDenomStateOrChangeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomStateOrChangeLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomStateOrChangeLabel.Location = new System.Drawing.Point(0, 15);
            this.lblDenomStateOrChangeLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomStateOrChangeLabel.Name = "lblDenomStateOrChangeLabel";
            this.lblDenomStateOrChangeLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomStateOrChangeLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomStateOrChangeLabel.TabIndex = 18;
            this.lblDenomStateOrChangeLabel.Text = "lblDenomStateOrChangeLabel";
            this.lblDenomStateOrChangeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomDefinitionVariantLabel
            // 
            this.lblDenomDefinitionVariantLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomDefinitionVariantLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenomDefinitionVariantLabel.Location = new System.Drawing.Point(0, 60);
            this.lblDenomDefinitionVariantLabel.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomDefinitionVariantLabel.Name = "lblDenomDefinitionVariantLabel";
            this.lblDenomDefinitionVariantLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenomDefinitionVariantLabel.Size = new System.Drawing.Size(150, 15);
            this.lblDenomDefinitionVariantLabel.TabIndex = 0;
            this.lblDenomDefinitionVariantLabel.Text = "lblDenomDefinitionVariantLabel";
            this.lblDenomDefinitionVariantLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorCaptionPlaceHolder
            // 
            this.lblDenominatorCaptionPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenominatorCaptionPlaceHolder.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenominatorCaptionPlaceHolder.Location = new System.Drawing.Point(0, 0);
            this.lblDenominatorCaptionPlaceHolder.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenominatorCaptionPlaceHolder.Name = "lblDenominatorCaptionPlaceHolder";
            this.lblDenominatorCaptionPlaceHolder.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDenominatorCaptionPlaceHolder.Size = new System.Drawing.Size(150, 15);
            this.lblDenominatorCaptionPlaceHolder.TabIndex = 20;
            this.lblDenominatorCaptionPlaceHolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpDenominatorValues
            // 
            this.tlpDenominatorValues.ColumnCount = 1;
            this.tlpDenominatorValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDenominatorValues.Controls.Add(this.lblDenomLocalDensityValue, 0, 2);
            this.tlpDenominatorValues.Controls.Add(this.lblDenomAttrSubPopulationValue, 0, 8);
            this.tlpDenominatorValues.Controls.Add(this.lblDenomStateOrChangeValue, 0, 1);
            this.tlpDenominatorValues.Controls.Add(this.lblDenominatorCaption, 0, 0);
            this.tlpDenominatorValues.Controls.Add(this.lblDenomAttrAreaDomainValue, 0, 7);
            this.tlpDenominatorValues.Controls.Add(this.lblDenomPopulationValue, 0, 6);
            this.tlpDenominatorValues.Controls.Add(this.lblDenomVersionValue, 0, 3);
            this.tlpDenominatorValues.Controls.Add(this.lblDenomAreaDomainValue, 0, 5);
            this.tlpDenominatorValues.Controls.Add(this.lblDenomDefinitionVariantValue, 0, 4);
            this.tlpDenominatorValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDenominatorValues.Location = new System.Drawing.Point(0, 0);
            this.tlpDenominatorValues.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDenominatorValues.Name = "tlpDenominatorValues";
            this.tlpDenominatorValues.RowCount = 10;
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDenominatorValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDenominatorValues.Size = new System.Drawing.Size(488, 135);
            this.tlpDenominatorValues.TabIndex = 1;
            // 
            // lblDenomLocalDensityValue
            // 
            this.lblDenomLocalDensityValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomLocalDensityValue.Location = new System.Drawing.Point(0, 30);
            this.lblDenomLocalDensityValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomLocalDensityValue.Name = "lblDenomLocalDensityValue";
            this.lblDenomLocalDensityValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomLocalDensityValue.TabIndex = 17;
            this.lblDenomLocalDensityValue.Text = "lblDenomLocalDensityValue";
            this.lblDenomLocalDensityValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomAttrSubPopulationValue
            // 
            this.lblDenomAttrSubPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomAttrSubPopulationValue.Location = new System.Drawing.Point(0, 120);
            this.lblDenomAttrSubPopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomAttrSubPopulationValue.Name = "lblDenomAttrSubPopulationValue";
            this.lblDenomAttrSubPopulationValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomAttrSubPopulationValue.TabIndex = 13;
            this.lblDenomAttrSubPopulationValue.Text = "lblDenomAttrSubPopulationValue";
            this.lblDenomAttrSubPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomStateOrChangeValue
            // 
            this.lblDenomStateOrChangeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomStateOrChangeValue.Location = new System.Drawing.Point(0, 15);
            this.lblDenomStateOrChangeValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomStateOrChangeValue.Name = "lblDenomStateOrChangeValue";
            this.lblDenomStateOrChangeValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomStateOrChangeValue.TabIndex = 18;
            this.lblDenomStateOrChangeValue.Text = "lblDenomStateOrChangeValue";
            this.lblDenomStateOrChangeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorCaption
            // 
            this.lblDenominatorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenominatorCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDenominatorCaption.Location = new System.Drawing.Point(0, 0);
            this.lblDenominatorCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenominatorCaption.Name = "lblDenominatorCaption";
            this.lblDenominatorCaption.Size = new System.Drawing.Size(488, 15);
            this.lblDenominatorCaption.TabIndex = 17;
            this.lblDenominatorCaption.Text = "lblDenominatorCaption";
            this.lblDenominatorCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomAttrAreaDomainValue
            // 
            this.lblDenomAttrAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomAttrAreaDomainValue.Location = new System.Drawing.Point(0, 105);
            this.lblDenomAttrAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomAttrAreaDomainValue.Name = "lblDenomAttrAreaDomainValue";
            this.lblDenomAttrAreaDomainValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomAttrAreaDomainValue.TabIndex = 16;
            this.lblDenomAttrAreaDomainValue.Text = "lblDenomAttrAreaDomainValue";
            this.lblDenomAttrAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomPopulationValue
            // 
            this.lblDenomPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomPopulationValue.Location = new System.Drawing.Point(0, 90);
            this.lblDenomPopulationValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomPopulationValue.Name = "lblDenomPopulationValue";
            this.lblDenomPopulationValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomPopulationValue.TabIndex = 2;
            this.lblDenomPopulationValue.Text = "lblDenomPopulationValue";
            this.lblDenomPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomVersionValue
            // 
            this.lblDenomVersionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomVersionValue.Location = new System.Drawing.Point(0, 45);
            this.lblDenomVersionValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomVersionValue.Name = "lblDenomVersionValue";
            this.lblDenomVersionValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomVersionValue.TabIndex = 3;
            this.lblDenomVersionValue.Text = "lblDenomVersionValue";
            this.lblDenomVersionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomAreaDomainValue
            // 
            this.lblDenomAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomAreaDomainValue.Location = new System.Drawing.Point(0, 75);
            this.lblDenomAreaDomainValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomAreaDomainValue.Name = "lblDenomAreaDomainValue";
            this.lblDenomAreaDomainValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomAreaDomainValue.TabIndex = 1;
            this.lblDenomAreaDomainValue.Text = "lblDenomAreaDomainValue";
            this.lblDenomAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenomDefinitionVariantValue
            // 
            this.lblDenomDefinitionVariantValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenomDefinitionVariantValue.Location = new System.Drawing.Point(0, 60);
            this.lblDenomDefinitionVariantValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDenomDefinitionVariantValue.Name = "lblDenomDefinitionVariantValue";
            this.lblDenomDefinitionVariantValue.Size = new System.Drawing.Size(488, 15);
            this.lblDenomDefinitionVariantValue.TabIndex = 0;
            this.lblDenomDefinitionVariantValue.Text = "lblDenomDefinitionVariantValue";
            this.lblDenomDefinitionVariantValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlCaption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlRatio);
            this.Controls.Add(this.pnlWide);
            this.Controls.Add(this.pnlExtended);
            this.Controls.Add(this.pnlBrief);
            this.Controls.Add(this.pnlEmpty);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlCaption";
            this.Size = new System.Drawing.Size(960, 350);
            this.pnlEmpty.ResumeLayout(false);
            this.pnlBrief.ResumeLayout(false);
            this.pnlExtended.ResumeLayout(false);
            this.splExtended.Panel1.ResumeLayout(false);
            this.splExtended.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splExtended)).EndInit();
            this.splExtended.ResumeLayout(false);
            this.tlpExtendedLabels.ResumeLayout(false);
            this.tlpExtendedValues.ResumeLayout(false);
            this.pnlWide.ResumeLayout(false);
            this.tlpWide.ResumeLayout(false);
            this.pnlRatio.ResumeLayout(false);
            this.splRatio.Panel1.ResumeLayout(false);
            this.splRatio.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splRatio)).EndInit();
            this.splRatio.ResumeLayout(false);
            this.pnlNumerator.ResumeLayout(false);
            this.splNumerator.Panel1.ResumeLayout(false);
            this.splNumerator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splNumerator)).EndInit();
            this.splNumerator.ResumeLayout(false);
            this.tlpNumeratorLabels.ResumeLayout(false);
            this.tlpNumeratorValues.ResumeLayout(false);
            this.pnlDenominator.ResumeLayout(false);
            this.splDenominator.Panel1.ResumeLayout(false);
            this.splDenominator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splDenominator)).EndInit();
            this.splDenominator.ResumeLayout(false);
            this.tlpDenominatorLabels.ResumeLayout(false);
            this.tlpDenominatorValues.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlEmpty;
        private System.Windows.Forms.Label lblEmptyCaption;
        private System.Windows.Forms.Panel pnlBrief;
        private System.Windows.Forms.Label lblBriefCaption;
        private System.Windows.Forms.Panel pnlExtended;
        private System.Windows.Forms.SplitContainer splExtended;
        private System.Windows.Forms.TableLayoutPanel tlpExtendedLabels;
        private System.Windows.Forms.Label lblExVersionLabel;
        private System.Windows.Forms.Label lblExPopulationLabel;
        private System.Windows.Forms.Label lblExAreaDomainLabel;
        private System.Windows.Forms.TableLayoutPanel tlpExtendedValues;
        private System.Windows.Forms.Label lblExVersionValue;
        private System.Windows.Forms.Label lblExPopulationValue;
        private System.Windows.Forms.Label lblExAreaDomainValue;
        private System.Windows.Forms.Label lblExDefinitionVariantValue;
        private System.Windows.Forms.Label lblExAttrSubPopulationLabel;
        private System.Windows.Forms.Label lblExAttrAreaDomainLabel;
        private System.Windows.Forms.Label lblExAttrSubPopulationValue;
        private System.Windows.Forms.Label lblExAttrAreaDomainValue;
        private System.Windows.Forms.Label lblExStateOrChangeLabel;
        private System.Windows.Forms.Label lblExLocalDensityLabel;
        private System.Windows.Forms.Label lblExStateOrChangeValue;
        private System.Windows.Forms.Label lblExLocalDensityValue;
        private System.Windows.Forms.Label lblExDefinitionVariantLabel;
        private System.Windows.Forms.Panel pnlWide;
        private System.Windows.Forms.Panel pnlRatio;
        private System.Windows.Forms.SplitContainer splRatio;
        private System.Windows.Forms.Panel pnlNumerator;
        private System.Windows.Forms.SplitContainer splNumerator;
        private System.Windows.Forms.TableLayoutPanel tlpNumeratorLabels;
        private System.Windows.Forms.Label lblNumStateOrChangeLabel;
        private System.Windows.Forms.Label lblNumLocalDensityLabel;
        private System.Windows.Forms.Label lblNumAttrSubPopulationLabel;
        private System.Windows.Forms.Label lblNumAttrAreaDomainLabel;
        private System.Windows.Forms.Label lblNumVersionLabel;
        private System.Windows.Forms.Label lblNumPopulationLabel;
        private System.Windows.Forms.Label lblNumAreaDomainLabel;
        private System.Windows.Forms.Label lblNumDefinitionVariantLabel;
        private System.Windows.Forms.Label lblNumeratorCaptionPlaceHolder;
        private System.Windows.Forms.TableLayoutPanel tlpNumeratorValues;
        private System.Windows.Forms.Label lblNumLocalDensityValue;
        private System.Windows.Forms.Label lblNumAttrSubPopulationValue;
        private System.Windows.Forms.Label lblNumStateOrChangeValue;
        private System.Windows.Forms.Label lblNumeratorCaption;
        private System.Windows.Forms.Label lblNumAttrAreaDomainValue;
        private System.Windows.Forms.Label lblNumVersionValue;
        private System.Windows.Forms.Label lblNumPopulationValue;
        private System.Windows.Forms.Label lblNumAreaDomainValue;
        private System.Windows.Forms.Label lblNumDefinitionVariantValue;
        private System.Windows.Forms.Panel pnlDenominator;
        private System.Windows.Forms.TableLayoutPanel tlpWide;
        private System.Windows.Forms.Label lblWideAttrSubPopulationValue;
        private System.Windows.Forms.Label lblWideAttrSubPopulationLabel;
        private System.Windows.Forms.Label lblWideAttrAreaDomainValue;
        private System.Windows.Forms.Label lblWideLocalDensityValue;
        private System.Windows.Forms.Label lblWideAttrAreaDomainLabel;
        private System.Windows.Forms.Label lblWideStateOrChangeValue;
        private System.Windows.Forms.Label lblWidePopulationValue;
        private System.Windows.Forms.Label lblWideDefinitionVariantValue;
        private System.Windows.Forms.Label lblWideAreaDomainValue;
        private System.Windows.Forms.Label lblWidePopulationLabel;
        private System.Windows.Forms.Label lblWideVersionValue;
        private System.Windows.Forms.Label lblWideAreaDomainLabel;
        private System.Windows.Forms.Label lblWideStateOrChangeLabel;
        private System.Windows.Forms.Label lblWideDefinitionVariantLabel;
        private System.Windows.Forms.Label lblWideLocalDensityLabel;
        private System.Windows.Forms.Label lblWideVersionLabel;
        private System.Windows.Forms.SplitContainer splDenominator;
        private System.Windows.Forms.TableLayoutPanel tlpDenominatorLabels;
        private System.Windows.Forms.Label lblDenomVersionLabel;
        private System.Windows.Forms.Label lblDenomAttrSubPopulationLabel;
        private System.Windows.Forms.Label lblDenomAttrAreaDomainLabel;
        private System.Windows.Forms.Label lblDenomPopulationLabel;
        private System.Windows.Forms.Label lblDenomLocalDensityLabel;
        private System.Windows.Forms.Label lblDenomAreaDomainLabel;
        private System.Windows.Forms.Label lblDenomStateOrChangeLabel;
        private System.Windows.Forms.Label lblDenomDefinitionVariantLabel;
        private System.Windows.Forms.Label lblDenominatorCaptionPlaceHolder;
        private System.Windows.Forms.TableLayoutPanel tlpDenominatorValues;
        private System.Windows.Forms.Label lblDenomLocalDensityValue;
        private System.Windows.Forms.Label lblDenomAttrSubPopulationValue;
        private System.Windows.Forms.Label lblDenomStateOrChangeValue;
        private System.Windows.Forms.Label lblDenominatorCaption;
        private System.Windows.Forms.Label lblDenomAttrAreaDomainValue;
        private System.Windows.Forms.Label lblDenomPopulationValue;
        private System.Windows.Forms.Label lblDenomVersionValue;
        private System.Windows.Forms.Label lblDenomAreaDomainValue;
        private System.Windows.Forms.Label lblDenomDefinitionVariantValue;
        private System.Windows.Forms.Label lblWidePlaceHolderLabel;
        private System.Windows.Forms.Label lblWidePlaceHolderValue;
    }

}