﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormFilterRowNumeric
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            pnlCaption = new System.Windows.Forms.Panel();
            lblCaption = new System.Windows.Forms.Label();
            tlpValues = new System.Windows.Forms.TableLayoutPanel();
            pnlLogicalOperators = new System.Windows.Forms.Panel();
            rdoOr = new System.Windows.Forms.RadioButton();
            rdoAnd = new System.Windows.Forms.RadioButton();
            pnlRelationOperators1 = new System.Windows.Forms.Panel();
            cboRelationOperators1 = new System.Windows.Forms.ComboBox();
            pnlRelationOperators2 = new System.Windows.Forms.Panel();
            cboRelationOperators2 = new System.Windows.Forms.ComboBox();
            pnlNumericValue1 = new System.Windows.Forms.Panel();
            cboNumericValue1 = new System.Windows.Forms.ComboBox();
            pnlNumericValue2 = new System.Windows.Forms.Panel();
            cboNumericValue2 = new System.Windows.Forms.ComboBox();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            tlpWorkSpace.SuspendLayout();
            pnlCaption.SuspendLayout();
            tlpValues.SuspendLayout();
            pnlLogicalOperators.SuspendLayout();
            pnlRelationOperators1.SuspendLayout();
            pnlRelationOperators2.SuspendLayout();
            pnlNumericValue1.SuspendLayout();
            pnlNumericValue2.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.Color.DarkSeaGreen;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlMain.Size = new System.Drawing.Size(960, 540);
            pnlMain.TabIndex = 4;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.SystemColors.Control;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Controls.Add(tlpWorkSpace, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(4, 3);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(952, 534);
            tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            pnlButtons.BackColor = System.Drawing.SystemColors.Control;
            pnlButtons.Controls.Add(tlpButtons);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 488);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(952, 46);
            pnlButtons.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(952, 46);
            tlpButtons.TabIndex = 2;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(765, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(6);
            pnlCancel.Size = new System.Drawing.Size(187, 46);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(6, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 34);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(578, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(6);
            pnlOK.Size = new System.Drawing.Size(187, 46);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(6, 6);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(175, 34);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // tlpWorkSpace
            // 
            tlpWorkSpace.ColumnCount = 1;
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Controls.Add(pnlCaption, 0, 0);
            tlpWorkSpace.Controls.Add(tlpValues, 0, 1);
            tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpWorkSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tlpWorkSpace.Location = new System.Drawing.Point(0, 0);
            tlpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            tlpWorkSpace.Name = "tlpWorkSpace";
            tlpWorkSpace.RowCount = 2;
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpWorkSpace.Size = new System.Drawing.Size(952, 488);
            tlpWorkSpace.TabIndex = 4;
            // 
            // pnlCaption
            // 
            pnlCaption.BackColor = System.Drawing.Color.DarkBlue;
            pnlCaption.Controls.Add(lblCaption);
            pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            pnlCaption.ForeColor = System.Drawing.Color.White;
            pnlCaption.Location = new System.Drawing.Point(0, 0);
            pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            pnlCaption.Name = "pnlCaption";
            pnlCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            pnlCaption.Size = new System.Drawing.Size(952, 35);
            pnlCaption.TabIndex = 0;
            // 
            // lblCaption
            // 
            lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCaption.Location = new System.Drawing.Point(12, 0);
            lblCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCaption.Name = "lblCaption";
            lblCaption.Size = new System.Drawing.Size(940, 35);
            lblCaption.TabIndex = 0;
            lblCaption.Text = "lblCaption";
            lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpValues
            // 
            tlpValues.BackColor = System.Drawing.Color.White;
            tlpValues.ColumnCount = 5;
            tlpValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 292F));
            tlpValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            tlpValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpValues.Controls.Add(pnlLogicalOperators, 1, 2);
            tlpValues.Controls.Add(pnlRelationOperators1, 1, 1);
            tlpValues.Controls.Add(pnlRelationOperators2, 1, 3);
            tlpValues.Controls.Add(pnlNumericValue1, 3, 1);
            tlpValues.Controls.Add(pnlNumericValue2, 3, 3);
            tlpValues.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpValues.Location = new System.Drawing.Point(0, 35);
            tlpValues.Margin = new System.Windows.Forms.Padding(0);
            tlpValues.Name = "tlpValues";
            tlpValues.RowCount = 5;
            tlpValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpValues.Size = new System.Drawing.Size(952, 453);
            tlpValues.TabIndex = 1;
            // 
            // pnlLogicalOperators
            // 
            pnlLogicalOperators.Controls.Add(rdoOr);
            pnlLogicalOperators.Controls.Add(rdoAnd);
            pnlLogicalOperators.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLogicalOperators.Location = new System.Drawing.Point(23, 52);
            pnlLogicalOperators.Margin = new System.Windows.Forms.Padding(0);
            pnlLogicalOperators.Name = "pnlLogicalOperators";
            pnlLogicalOperators.Size = new System.Drawing.Size(292, 29);
            pnlLogicalOperators.TabIndex = 1;
            // 
            // rdoOr
            // 
            rdoOr.Dock = System.Windows.Forms.DockStyle.Fill;
            rdoOr.Location = new System.Drawing.Point(117, 0);
            rdoOr.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            rdoOr.Name = "rdoOr";
            rdoOr.Size = new System.Drawing.Size(175, 29);
            rdoOr.TabIndex = 1;
            rdoOr.TabStop = true;
            rdoOr.Text = "rdoOr";
            rdoOr.UseVisualStyleBackColor = true;
            // 
            // rdoAnd
            // 
            rdoAnd.Dock = System.Windows.Forms.DockStyle.Left;
            rdoAnd.Location = new System.Drawing.Point(0, 0);
            rdoAnd.Margin = new System.Windows.Forms.Padding(0);
            rdoAnd.Name = "rdoAnd";
            rdoAnd.Size = new System.Drawing.Size(117, 29);
            rdoAnd.TabIndex = 0;
            rdoAnd.TabStop = true;
            rdoAnd.Text = "rdoAnd";
            rdoAnd.UseVisualStyleBackColor = true;
            // 
            // pnlRelationOperators1
            // 
            pnlRelationOperators1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlRelationOperators1.Controls.Add(cboRelationOperators1);
            pnlRelationOperators1.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlRelationOperators1.Location = new System.Drawing.Point(23, 23);
            pnlRelationOperators1.Margin = new System.Windows.Forms.Padding(0);
            pnlRelationOperators1.Name = "pnlRelationOperators1";
            pnlRelationOperators1.Size = new System.Drawing.Size(292, 29);
            pnlRelationOperators1.TabIndex = 2;
            // 
            // cboRelationOperators1
            // 
            cboRelationOperators1.BackColor = System.Drawing.Color.White;
            cboRelationOperators1.Dock = System.Windows.Forms.DockStyle.Fill;
            cboRelationOperators1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboRelationOperators1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cboRelationOperators1.FormattingEnabled = true;
            cboRelationOperators1.Location = new System.Drawing.Point(0, 0);
            cboRelationOperators1.Margin = new System.Windows.Forms.Padding(0);
            cboRelationOperators1.Name = "cboRelationOperators1";
            cboRelationOperators1.Size = new System.Drawing.Size(290, 23);
            cboRelationOperators1.TabIndex = 1;
            // 
            // pnlRelationOperators2
            // 
            pnlRelationOperators2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlRelationOperators2.Controls.Add(cboRelationOperators2);
            pnlRelationOperators2.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlRelationOperators2.Location = new System.Drawing.Point(23, 81);
            pnlRelationOperators2.Margin = new System.Windows.Forms.Padding(0);
            pnlRelationOperators2.Name = "pnlRelationOperators2";
            pnlRelationOperators2.Size = new System.Drawing.Size(292, 29);
            pnlRelationOperators2.TabIndex = 3;
            // 
            // cboRelationOperators2
            // 
            cboRelationOperators2.BackColor = System.Drawing.Color.White;
            cboRelationOperators2.Dock = System.Windows.Forms.DockStyle.Fill;
            cboRelationOperators2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboRelationOperators2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cboRelationOperators2.FormattingEnabled = true;
            cboRelationOperators2.Location = new System.Drawing.Point(0, 0);
            cboRelationOperators2.Margin = new System.Windows.Forms.Padding(0);
            cboRelationOperators2.Name = "cboRelationOperators2";
            cboRelationOperators2.Size = new System.Drawing.Size(290, 23);
            cboRelationOperators2.TabIndex = 1;
            // 
            // pnlNumericValue1
            // 
            pnlNumericValue1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlNumericValue1.Controls.Add(cboNumericValue1);
            pnlNumericValue1.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNumericValue1.Location = new System.Drawing.Point(327, 23);
            pnlNumericValue1.Margin = new System.Windows.Forms.Padding(0);
            pnlNumericValue1.Name = "pnlNumericValue1";
            pnlNumericValue1.Size = new System.Drawing.Size(602, 29);
            pnlNumericValue1.TabIndex = 4;
            // 
            // cboNumericValue1
            // 
            cboNumericValue1.BackColor = System.Drawing.Color.White;
            cboNumericValue1.Dock = System.Windows.Forms.DockStyle.Fill;
            cboNumericValue1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboNumericValue1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cboNumericValue1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            cboNumericValue1.FormattingEnabled = true;
            cboNumericValue1.Location = new System.Drawing.Point(0, 0);
            cboNumericValue1.Margin = new System.Windows.Forms.Padding(0);
            cboNumericValue1.Name = "cboNumericValue1";
            cboNumericValue1.Size = new System.Drawing.Size(600, 23);
            cboNumericValue1.TabIndex = 1;
            // 
            // pnlNumericValue2
            // 
            pnlNumericValue2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlNumericValue2.Controls.Add(cboNumericValue2);
            pnlNumericValue2.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNumericValue2.Location = new System.Drawing.Point(327, 81);
            pnlNumericValue2.Margin = new System.Windows.Forms.Padding(0);
            pnlNumericValue2.Name = "pnlNumericValue2";
            pnlNumericValue2.Size = new System.Drawing.Size(602, 29);
            pnlNumericValue2.TabIndex = 3;
            // 
            // cboNumericValue2
            // 
            cboNumericValue2.BackColor = System.Drawing.Color.White;
            cboNumericValue2.Dock = System.Windows.Forms.DockStyle.Fill;
            cboNumericValue2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboNumericValue2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cboNumericValue2.FormattingEnabled = true;
            cboNumericValue2.Location = new System.Drawing.Point(0, 0);
            cboNumericValue2.Margin = new System.Windows.Forms.Padding(0);
            cboNumericValue2.Name = "cboNumericValue2";
            cboNumericValue2.Size = new System.Drawing.Size(600, 23);
            cboNumericValue2.TabIndex = 1;
            // 
            // FormFilterRowNumeric
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(960, 540);
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormFilterRowNumeric";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormFilterRowNumeric";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            tlpWorkSpace.ResumeLayout(false);
            pnlCaption.ResumeLayout(false);
            tlpValues.ResumeLayout(false);
            pnlLogicalOperators.ResumeLayout(false);
            pnlRelationOperators1.ResumeLayout(false);
            pnlRelationOperators2.ResumeLayout(false);
            pnlNumericValue1.ResumeLayout(false);
            pnlNumericValue2.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.TableLayoutPanel tlpValues;
        private System.Windows.Forms.Panel pnlLogicalOperators;
        private System.Windows.Forms.RadioButton rdoOr;
        private System.Windows.Forms.RadioButton rdoAnd;
        private System.Windows.Forms.Panel pnlNumericValue2;
        private System.Windows.Forms.ComboBox cboNumericValue2;
        private System.Windows.Forms.Panel pnlRelationOperators1;
        private System.Windows.Forms.ComboBox cboRelationOperators1;
        private System.Windows.Forms.Panel pnlNumericValue1;
        private System.Windows.Forms.ComboBox cboNumericValue1;
        private System.Windows.Forms.Panel pnlRelationOperators2;
        private System.Windows.Forms.ComboBox cboRelationOperators2;
    }

}