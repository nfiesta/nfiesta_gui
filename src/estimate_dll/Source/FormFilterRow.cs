﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro nastavení filtru řádků</para>
    /// <para lang="en">Form for setting row filter</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormFilterRow
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </summary>
        private DataGridView dgvData;

        /// <summary>
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </summary>
        private DataGridViewColumn dgvColumn;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="dgvData">
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </param>
        /// <param name="dgvColumn">
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </param>
        public FormFilterRow(
            Control controlOwner,
            DataGridView dgvData,
            DataGridViewColumn dgvColumn)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                dgvData: dgvData,
                dgvColumn: dgvColumn);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlViewOLAP)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlViewOLAP)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlViewOLAP)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlViewOLAP)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)
        /// </para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)
        /// </para>
        /// </summary>
        public DisplayType DisplayType
        {
            get
            {
                return ((ControlViewOLAP)ControlOwner).DisplayType;
            }
        }

        /// <summary>
        /// <para lang="cs">Filter řádků v DataGridView (read-only)</para>
        /// <para lang="en">Row filter in DataGridView (read-only)</para>
        /// </summary>
        public FilterRow RowFilter
        {
            get
            {
                return ((ControlViewOLAP)ControlOwner).RowFilter;
            }
            set
            {
                ((ControlViewOLAP)ControlOwner).RowFilter = value;
            }
        }

        /// <summary>
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </summary>
        public DataGridView DgvData
        {
            get
            {
                return dgvData ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvData)} must not be null.",
                        paramName: nameof(DgvData));
            }
            set
            {
                dgvData = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvData)} must not be null.",
                        paramName: nameof(DgvData));
            }
        }

        /// <summary>
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </summary>
        public DataGridViewColumn DgvColumn
        {
            get
            {
                return dgvColumn ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvColumn)} must not be null.",
                        paramName: nameof(DgvColumn));
            }
            set
            {
                dgvColumn = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvColumn)} must not be null.",
                        paramName: nameof(DgvColumn));
            }
        }

        /// <summary>
        /// <para lang="cs">ColumnMetadata (read-only)</para>
        /// <para lang="en">ColumnMetadata (read-only)</para>
        /// </summary>
        public ColumnMetadata ColMetadata
        {
            get
            {
                return
                     ControlViewOLAP.GetColumnMetadata(
                        displayType: DisplayType,
                        dgvData: DgvData,
                        dgvColumn: DgvColumn);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam unikátních hodnot vyskytujících se ve zvoleném datovém sloupci (read-only)</para>
        /// <para lang="en">List of unique values occurring in the selected data column (read-only)</para>
        /// </summary>
        public List<string> Items
        {
            get
            {
                if (DgvData.Tag == null)
                {
                    return [];
                }

                if (ColMetadata == null)
                {
                    return [];
                }

                return DisplayType switch
                {
                    DisplayType.Total =>
                        [.. ((OLAPTotalEstimateList)DgvData.Tag).Data.AsEnumerable()
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: ColMetadata.Name,
                                        defaultValue: Functions.StrNull,
                                        format: ColMetadata.NumericFormat))
                                .Distinct()
                                .OrderBy(a => a)],

                    DisplayType.Ratio =>
                        [.. ((OLAPRatioEstimateList)DgvData.Tag).Data.AsEnumerable()
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: ColMetadata.Name,
                                        defaultValue: Functions.StrNull,
                                        format: ColMetadata.NumericFormat))
                                .Distinct()
                                .OrderBy(a => a)],

                    _ => [],
                };
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormFilterRow),    "Výběr řádků tabulky" },
                        { nameof(chkCheckAll),      "(Vybrat vše)" },
                        { nameof(btnCancel),        "Zrušit" },
                        { nameof(btnOK),            "OK" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormFilterRow),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormFilterRow),    "Table rows selection" },
                        { nameof(chkCheckAll),      "(Select all)" },
                        { nameof(btnCancel),        "Cancel" },
                        { nameof(btnOK),            "OK" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormFilterRow),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="dgvData">
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </param>
        /// <param name="dgvColumn">
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </param>
        public void Initialize(
            Control controlOwner,
            DataGridView dgvData,
            DataGridViewColumn dgvColumn)
        {
            ControlOwner = controlOwner;
            DgvData = dgvData;
            DgvColumn = dgvColumn;
            onEdit = false;

            InitializeLabels();

            LoadContent();

            InitializeList();

            chkCheckAll.CheckStateChanged += new EventHandler(
                (sender, e) =>
                {
                    if (onEdit) { return; }

                    switch (chkCheckAll.CheckState)
                    {
                        case CheckState.Checked:
                            foreach (CheckBox control in pnlCheckedList.Controls.OfType<CheckBox>())
                            {
                                control.Checked = true;
                            }
                            return;

                        case CheckState.Unchecked:
                            foreach (CheckBox control in pnlCheckedList.Controls.OfType<CheckBox>())
                            {
                                control.Checked = false;
                            }
                            return;

                        case CheckState.Indeterminate:
                            return;

                        default:
                            return;
                    }
                });

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    SetRowFilter();
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormFilterRow),
                    out string frmFilterRowText)
                        ? frmFilterRowText
                        : String.Empty;

            chkCheckAll.Text =
                labels.TryGetValue(key: nameof(chkCheckAll),
                    out string chkCheckAllText)
                        ? chkCheckAllText
                        : String.Empty;

            btnOK.Text =
               labels.TryGetValue(key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            btnCancel.Text =
               labels.TryGetValue(key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            lblCaption.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? ColMetadata.HeaderTextEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ColMetadata.HeaderTextCs
                        : ColMetadata.HeaderTextEn;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace zaškrtávacího seznamu unikátních hodnot
        /// vyskytujících se ve zvoleném datovém sloupci</para>
        /// <para lang="en">
        /// Initializing checked list box with unique values
        /// occurring in the selected data column</para>
        /// </summary>
        private void InitializeList()
        {
            pnlCheckedList.Controls.Clear();

            if (ColMetadata == null)
            {
                return;
            }

            FilterRow.FilterRowItemEnumeration filter =
                (FilterRow.FilterRowItemEnumeration)RowFilter.Get(column: ColMetadata);

            int i = (-1);
            foreach (var item in Items
                .Select(a => new
                {
                    Text = a,
                    Number = Double.TryParse(s: a, out double result) ? result : (Nullable<double>)null
                })
                .OrderBy(a => a.Number)     // seřazeno podle číselné hodnoty
                .ThenBy(a => a.Text))       // pokud neobsahuje číslo pak se řadí podle textu
            {
                i++;
                CheckBox control = new()
                {
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    Checked =
                        (filter != null) &&
                         filter.SelectedValues.Contains(item: item.Text),
                    Height = 20,
                    Left = 10,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    Tag = item.Text,
                    Text = item.Text,
                    Top = (i * 25) + 5,
                    Width = pnlCheckedList.Width - 30
                };

                control.CheckedChanged += new EventHandler(
                    (sender, e) =>
                    {
                        SetCheckedAllValue();
                    });

                control.CreateControl();
                pnlCheckedList.Controls.Add(value: control);
            };
            SetCheckedAllValue();
        }

        /// <summary>
        /// <para lang="cs">Nastaví hodnotu CheckBoxu chkCheckAll</para>
        /// <para lang="en">Sets chkCheckAll value</para>
        /// </summary>
        private void SetCheckedAllValue()
        {
            onEdit = true;
            if (pnlCheckedList.Controls.OfType<CheckBox>().All(a => a.Checked))
            {
                chkCheckAll.CheckState = CheckState.Checked;
            }
            else if (pnlCheckedList.Controls.OfType<CheckBox>().All(a => !a.Checked))
            {
                chkCheckAll.CheckState = CheckState.Unchecked;
            }
            else
            {
                chkCheckAll.CheckState = CheckState.Indeterminate;
            }
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">
        /// Nastavit filtr řádků tabulky</para>
        /// <para lang="en">
        /// Set table rows filter</para>
        /// </summary>
        private void SetRowFilter()
        {
            FilterRow rowFilter = RowFilter;

            if (!pnlCheckedList.Controls
                .OfType<CheckBox>()
                .Where(a => a.Checked)
                .Any())
            {
                rowFilter.Remove(
                    column: ColMetadata);
            }
            else
            {
                rowFilter.Add(
                  column: ColMetadata,
                  selectedValues:
                      pnlCheckedList.Controls.OfType<CheckBox>()
                      .Where(a => a.Checked)
                      .Select(a => a.Text)
                      .ToList<string>());
            }

            RowFilter = rowFilter;
        }

        #endregion Methods

    }

}