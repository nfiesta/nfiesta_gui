﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Controls;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba indikátoru"</para>
    /// <para lang="en">Control "Indicator selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTargetVariable
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Vybraný indikátor (read-only)</para>
        /// <para lang="en">Selected indicator (read-only)</para>
        /// </summary>
        private ITargetVariable selectedTargetVariable;

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru (read-only)</para>
        /// <para lang="en">Selected indicator metadata (read-only)</para>
        /// </summary>
        private ITargetVariableMetadata selectedTargetVariableMetadata;

        private string strMetadataIndicatorText = String.Empty;
        private string strMetadataElementsText = String.Empty;
        private string strRowsCountInsufficient = String.Empty;
        private string strRowsCountPrecise = String.Empty;
        private string strRowsCountTooMany = String.Empty;
        private string lstTopicDisplayMember = String.Empty;
        private string msgInvalidJsonFormat = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení nadpisu</para>
        /// <para lang="en">Control for display caption</para>
        /// </summary>
        private ControlCaption CtrCaption
        {
            get
            {
                if (pnlCaption.Controls.Count != 1)
                {
                    CreateNewCaption();
                }
                if (pnlCaption.Controls[0] is not ControlCaption)
                {
                    CreateNewCaption();
                }
                return (ControlCaption)pnlCaption.Controls[0];
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Control "Target variable selection from json file of metadata"</para>
        /// </summary>
        public ControlTargetVariableSelector CtrTargetVariableSelector
        {
            get
            {
                if (pnlTargetVariableSelector.Controls.Count != 1)
                {
                    CreateNewTargetVariableSelector();
                }
                if (pnlTargetVariableSelector.Controls[0] is not ControlTargetVariableSelector)
                {
                    CreateNewTargetVariableSelector();
                }
                return (ControlTargetVariableSelector)pnlTargetVariableSelector.Controls[0];
            }
        }

        /// <summary>
        /// <para lang="cs">ListBox pro zobrazení seznamu tématických okruhů</para>
        /// <para lang="en">ListBox to display a list of topics</para>
        /// </summary>
        private ListBox LstTopic
        {
            get
            {
                if (pnlTopic.Controls.Count != 1)
                {
                    InitializeListBoxTopic();
                }
                if (pnlTopic.Controls[0] is not ListBox)
                {
                    InitializeListBoxTopic();
                }
                return (ListBox)pnlTopic.Controls[0];
            }
        }

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTargetVariable(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        #region Input Properties

        /// <summary>
        /// <para lang="cs">
        /// Jednotka
        /// (pro kterou má komponenta zobrazit cílové proměnné)
        /// (pokud je null, pak se zobrazují všechny cílové proměnné)
        /// </para>
        /// <para lang="en">
        /// Unit
        /// (for which the component should display the target variables)
        /// (if null, then all target variables are displayed)</para>
        /// </summary>
        public Unit SelectedUnitOfMeasure
        {
            get
            {
                return CtrTargetVariableSelector.Unit;
            }
            set
            {
                CtrTargetVariableSelector.Unit = value;
                CtrTargetVariableSelector.LoadContent();
            }
        }

        #endregion Input Properties

        #region Output Properties

        /// <summary>
        /// <para lang="cs">Vybraný indikátor (read-only)</para>
        /// <para lang="en">Selected indicator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedTargetVariable
        {
            get
            {
                return
                   selectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru (read-only)</para>
        /// <para lang="en">Selected indicator metadata (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedTargetVariableMetadata
        {
            get
            {
                return
                    selectedTargetVariableMetadata;
            }
        }

        #endregion Output Properties

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnNext),                  "Další" },
                        { nameof(grpTopic),                 "Tématický okruh:" },
                        { nameof(strMetadataIndicatorText), "Indikátor:" },
                        { nameof(strMetadataElementsText),  "Metadata:" },
                        { nameof(strRowsCountInsufficient), "Počet vybraných řádků: $1 (to není dost)." },
                        { nameof(strRowsCountPrecise),      "Počet vybraných řádků: $1." },
                        { nameof(strRowsCountTooMany),      "Počet vybraných řádků: $1 (to je moc)." },
                        { nameof(lstTopicDisplayMember),    "LabelCs" },
                        { nameof(msgInvalidJsonFormat),     String.Concat(
                                                                $"V databázové tabulce $1.$2 $0",
                                                                $"ve sloupci $3 existují záznamy $0",
                                                                $"s neplatným formátem JSON souboru. $0",
                                                                $"Tyto záznamy se nepodařilo z databáze načíst $0",
                                                                $"a jejich cílové proměné nebude možné zvolit pro výpočet. $0") }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTargetVariable),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnNext),                  "Next" },
                        { nameof(grpTopic),                 "Topic:" },
                        { nameof(strMetadataIndicatorText), "Indicator:" },
                        { nameof(strMetadataElementsText),  "Metadata:" },
                        { nameof(strRowsCountInsufficient), "Number of selected rows: $1 (not enough)."},
                        { nameof(strRowsCountPrecise),      "Number of selected rows: $1." },
                        { nameof(strRowsCountTooMany),      "Number of selected rows: $1 (too many)." },
                        { nameof(lstTopicDisplayMember),    "LabelEn" },
                        { nameof(msgInvalidJsonFormat),     String.Concat(
                                                                $"There are records with invalid JSON file format in database table ",
                                                                $"$1.$2 $0",
                                                                $"and column $3. $0",
                                                                $"Loading data from database failed. $0") }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTargetVariable),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            onEdit = false;

            selectedTargetVariable = null;
            selectedTargetVariableMetadata = null;

            Visible = (Database != null) &&
               (Database.Postgres != null) &&
               Database.Postgres.Initialized;

            SetFrameColor();

            CreateNewCaption();

            CreateNewTargetVariableSelector();

            InitializeListBoxTopic();

            InitializeLabels();

            EnableButtonNext();

            // Obsluhy událostí
            btnNext.Click += new EventHandler(
                (sender, e) =>
                {
                    selectedTargetVariable = CtrTargetVariableSelector.SelectedTargetVariable;
                    selectedTargetVariableMetadata = CtrTargetVariableSelector.SelectedTargetVariableMetadata;

                    NextClick?.Invoke(
                        sender: this,
                        e: new EventArgs());
                });

            splCaption.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplTargetVariableCaptionDistance =
                            splCaption.SplitterDistance;
                    }
                });

            splTopic.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplTopicDistance =
                            splTopic.SplitterDistance;
                    }
                });

            Resize += new EventHandler((sender, e) =>
            {
                ReDraw();
            });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                    out string btnNextText)
                        ? btnNextText
                        : String.Empty;

            grpTopic.Text =
                labels.TryGetValue(key: nameof(grpTopic),
                    out string grpTopicText)
                        ? grpTopicText
                        : String.Empty;

            strMetadataIndicatorText =
                labels.TryGetValue(key: nameof(strMetadataIndicatorText),
                    out strMetadataIndicatorText)
                        ? strMetadataIndicatorText
                        : String.Empty;

            strMetadataElementsText =
                labels.TryGetValue(key: nameof(strMetadataElementsText),
                    out strMetadataElementsText)
                        ? strMetadataElementsText
                        : String.Empty;

            strRowsCountInsufficient =
                labels.TryGetValue(key: nameof(strRowsCountInsufficient),
                    out strRowsCountInsufficient)
                        ? strRowsCountInsufficient
                        : String.Empty;

            strRowsCountPrecise =
                labels.TryGetValue(key: nameof(strRowsCountPrecise),
                    out strRowsCountPrecise)
                        ? strRowsCountPrecise
                        : String.Empty;

            strRowsCountTooMany =
                labels.TryGetValue(key: nameof(strRowsCountTooMany),
                    out strRowsCountTooMany)
                        ? strRowsCountTooMany
                        : String.Empty;

            lstTopicDisplayMember =
                labels.TryGetValue(key: nameof(lstTopicDisplayMember),
                    out lstTopicDisplayMember)
                        ? lstTopicDisplayMember
                        : ID;

            msgInvalidJsonFormat =
                labels.TryGetValue(key: nameof(msgInvalidJsonFormat),
                    out msgInvalidJsonFormat)
                        ? msgInvalidJsonFormat
                        : String.Empty;

            // Skryje ovládací prvek zobrazující metadata, aby neproblikával
            // když se přenačítá do jiného jazyka
            grpTopic.Visible = false;
            pnlTargetVariableSelector.Visible = false;

            CtrCaption.InitializeLabels();

            InitializeListBoxTopic();

            CtrTargetVariableSelector.MetadataIndicatorText = strMetadataIndicatorText;
            CtrTargetVariableSelector.MetadataElementsText = strMetadataElementsText;
            CtrTargetVariableSelector.InitializeLabels();

            SetLabelRowsCount();

            // Znovu zobrazí ovládací prvek zobrazující metadata
            grpTopic.Visible = true;
            pnlTargetVariableSelector.Visible = true;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            CtrTargetVariableSelector.Data = Database.SNfiEsta.CTargetVariable;

            if (!CtrTargetVariableSelector.JsonValidity)
            {
                MessageBox.Show(
                    text: msgInvalidJsonFormat
                        .Replace(oldValue: "$1", newValue: NfiEstaSchema.Name)
                        .Replace(oldValue: "$2", newValue: TargetVariableList.Name)
                        .Replace(oldValue: "$3", newValue: TargetVariableList.ColMetadata.DbName)
                        .Replace(oldValue: "$0", newValue: Environment.NewLine),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            InitializeListBoxTopic();
        }

        /// <summary>
        /// <para lang="cs">Nastaví barvu orámování</para>
        /// <para lang="en">Sets frame color</para>
        /// </summary>
        private void SetFrameColor()
        {
            onEdit = true;

            pnlMain.BackColor = Setting.BorderColor;
            pnlWorkSpace.BackColor = Setting.BorderColor;

            splCaption.BackColor = Setting.SpliterColor;
            splCaption.SplitterDistance = Setting.SplTargetVariableCaptionDistance;

            splTopic.BackColor = Setting.SpliterColor;
            splTopic.SplitterDistance = Setting.SplTopicDistance;

            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nový ovládací prvek pro zobrazení nadpisu</para>
        /// <para lang="en">Creates new control for display caption</para>
        /// </summary>
        private void CreateNewCaption()
        {
            pnlCaption.Visible = false;
            pnlCaption.Controls.Clear();
            ControlCaption ctrCaption =
                new(
                    controlOwner: this,
                    numeratorTargetVariableMetadata: null,
                    denominatorTargetVariableMetadata: null,
                    variablePairs: null,
                    captionVersion: CaptionVersion.Brief,
                    withAttributeCategories: false,
                    withIdentifiers: true)
                {
                    Dock = DockStyle.Top
                };
            ctrCaption.CreateControl();
            pnlCaption.Controls.Add(value: ctrCaption);
            pnlCaption.Visible = true;
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nový ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Creates new control "Target variable selection from json file of metadata"</para>
        /// </summary>
        private void CreateNewTargetVariableSelector()
        {
            pnlTargetVariableSelector.Visible = false;
            pnlTargetVariableSelector.Controls.Clear();

            ControlTargetVariableSelector ctrTargetVariableSelector =
                new(controlOwner: this)
                {
                    Design = TargetVariableSelectorDesign.Standard,
                    Dock = DockStyle.Fill,
                    MetadataElementsText = strMetadataElementsText,
                    MetadataIndicatorText = strMetadataIndicatorText,
                    NotAssignedCategory = false,
                    Spacing = [10],
                    SplitterDistance = Setting.SplIndicatorDistance
                };

            ctrTargetVariableSelector.SelectedTargetVariableChanged +=
                new EventHandler((sender, e) =>
                {
                    CtrCaption.NumeratorTargetVariableMetadata
                       = ctrTargetVariableSelector.SelectedTargetVariableMetadata;
                    SetLabelRowsCount();
                    EnableButtonNext();
                });

            ctrTargetVariableSelector.SplitterMoved +=
                new EventHandler((sender, e) =>
                {
                    Setting.SplIndicatorDistance = ctrTargetVariableSelector.SplitterDistance;
                });

            ctrTargetVariableSelector.CreateControl();
            pnlTargetVariableSelector.Controls.Add(value: ctrTargetVariableSelector);
            pnlTargetVariableSelector.Visible = true;
        }

        /// <summary>
        /// <para lang="cs">
        /// Povolí nebo zakáže přístup na další komponentu,
        /// podle toho, zda je nebo není vybraný indikátor a jeho metadata,
        /// jednotka a kategorie atributového členění
        /// </para>
        /// <para lang="en">
        /// Enables or disables access to the next component
        /// depending on whether or not the indicator and its metadata are selected,
        /// the unit of measure and attribute categories are selected
        /// </para>
        /// </summary>
        private void EnableButtonNext()
        {
            btnNext.Enabled =
                (CtrTargetVariableSelector.SelectedTargetVariable != null) &&
                (CtrTargetVariableSelector.SelectedTargetVariableMetadata != null);
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu tématických okruhů</para>
        /// <para lang="en">Initialization of the list of topics</para>
        /// </summary>
        private void InitializeListBoxTopic()
        {
            onEdit = true;

            // Zapamatuje si ID vybraného tématického okruhu,
            // pokud vůbec nějaký tématický okruh vybrán byl
            Nullable<int> selectedTopicId;
            if (pnlTopic.Controls.OfType<ListBox>().Any())
            {
                Topic selectedTopic =
                    (Topic)pnlTopic.Controls.OfType<ListBox>()
                        .First()
                        .SelectedItem;
                selectedTopicId = (selectedTopic != null)
                    ? selectedTopic.Id
                    : (Nullable<int>)null;
            }
            else
            {
                selectedTopicId = null;
            }

            // Vytvoření datového zdroje (seznam tématických okruhů)
            List<Topic> items = [];
            items = LanguageVersion switch
            {
                // Inner join mezi c_topic a párovací tabulkou cm_target_variable2topic
                // vyřadí prázdné tématické okruhy bez cílových proměnných
                // které není potřeba zobrazovat
                // Seznamy se seřadí podle popisků zvoleného jazyka
                LanguageVersion.National =>
                    [.. (from a in Database.SNfiEsta.CTopic.Items
                             join b in Database.SNfiEsta.CmTargetVariableToTopic.Items
                             on a.Id equals b.TopicId
                             select a)
                            .Distinct<Topic>()
                            .OrderBy(a => a.LabelCs)],

                LanguageVersion.International =>
                    [.. (from a in Database.SNfiEsta.CTopic.Items
                             join b in Database.SNfiEsta.CmTargetVariableToTopic.Items
                             on a.Id equals b.TopicId
                             select a)
                            .Distinct<Topic>()
                            .OrderBy(a => a.LabelEn)],

                _ =>
                    [.. (from a in Database.SNfiEsta.CTopic.Items
                             join b in Database.SNfiEsta.CmTargetVariableToTopic.Items
                             on a.Id equals b.TopicId
                             select a)
                            .Distinct<Topic>()
                            .OrderBy(a => a.Id)],
            };

            // Vytvoření ListBoxu
            pnlTopic.Controls.Clear();

            ListBox listBox = new()
            {
                BackColor = System.Drawing.SystemColors.Window,
                DataSource = items,
                DisplayMember = lstTopicDisplayMember,
                Dock = DockStyle.Fill,
                Font = Setting.ListBoxFont,
                ForeColor = Setting.ListBoxForeColor,
                Margin = new Padding(all: 0),
                SelectedIndex = -1,
                SelectionMode = SelectionMode.One,
                ValueMember = ID
            };
            listBox.SelectedIndexChanged += (sender, e) =>
            {
                if (!onEdit) { SelectTopic(); }
            };
            listBox.CreateControl();
            pnlTopic.Controls.Add(value: listBox);

            // Pokud tématický okruh byl již dříve vybrán,
            // znovu se vybere
            if (selectedTopicId != null)
            {
                foreach (Topic item in listBox.Items)
                {
                    if (item.Id == selectedTopicId)
                    {
                        listBox.SelectedItem = item;
                        break;
                    }
                }

                // Pokud dříve vybraná položka nebyla v seznamu nalezena vybere se první položka v seznamu
                if (listBox.SelectedItem == null)
                {
                    if (listBox.Items.OfType<Topic>().Any())
                    {
                        listBox.SelectedItem = listBox.Items.OfType<Topic>().ToList()[0];
                    }
                }
            }

            // Pokud vybrán nebyl vybere se první položka v seznamu
            else
            {
                if (listBox.Items.OfType<Topic>().Any())
                {
                    listBox.SelectedItem = listBox.Items.OfType<Topic>().ToList()[0];
                }
            }

            onEdit = false;

            SelectTopic();
        }

        /// <summary>
        /// <para lang="cs">
        /// V seznamu tématických okruhů byl vybrán jeden tématický okruh.
        /// Pro vybraný tématický okruh vybere skupinu cílových proměnných
        /// a její členy zobrazí v seznamu pro cílové proměnné
        /// </para>
        /// <para lang="en">
        /// In the list of topics, one topic was selected.
        /// For the selected topic, selects a group of target variables
        /// and displays its members in the list for the target variables.
        /// </para>
        /// </summary>
        private void SelectTopic()
        {
            Topic selectedTopic =
                (LstTopic.SelectedItem != null) ?
                (Topic)LstTopic.SelectedItem : null;

            CtrTargetVariableSelector.Topic = selectedTopic;
            CtrTargetVariableSelector.LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Nastaví popisek s počtem vybraných cílových proměnných</para>
        /// <para lang="en">Sets the label with the number of selected target variables</para>
        /// </summary>
        private void SetLabelRowsCount()
        {
            int count = CtrTargetVariableSelector.SelectedTargetVariables.Items.Count;

            if (count < 1)
            {
                lblRowsCount.Text = strRowsCountInsufficient
                    .Replace(oldValue: "$1", newValue: count.ToString());
                return;
            }

            if (count > 1)
            {
                lblRowsCount.Text = strRowsCountTooMany
                    .Replace(oldValue: "$1", newValue: count.ToString());
                return;
            }

            lblRowsCount.Text = strRowsCountPrecise
                    .Replace(oldValue: "$1", newValue: count.ToString());
            return;
        }

        /// <summary>
        /// <para lang="cs">Skryje panel s tlačítky</para>
        /// <para lang="en">Hides a panel with buttons</para>
        /// </summary>
        public void HideButtonsPanel()
        {
            tlpButtons.Visible = false;
            tlpMain.RowStyles[1].SizeType = SizeType.Absolute;
            tlpMain.RowStyles[1].Height = 0;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí panel s tlačítky</para>
        /// <para lang="en">Displays a panel with buttons</para>
        /// </summary>
        public void ShowButtonsPanel()
        {
            tlpButtons.Visible = true;
            tlpMain.RowStyles[1].SizeType = SizeType.Absolute;
            tlpMain.RowStyles[1].Height = 40;
        }

        /// <summary>
        /// <para lang="cs">Překreslení ovládacího prvku</para>
        /// <para lang="en">Redraw user control</para>
        /// </summary>
        public void ReDraw()
        {
            foreach (ControlTargetVariableSelector selector
                in pnlTargetVariableSelector.Controls.OfType<ControlTargetVariableSelector>())
            {
                selector.ReDraw();
            }
            Refresh();
        }

        #endregion Methods

    }

}