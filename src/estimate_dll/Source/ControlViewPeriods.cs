﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Zobrazení období"</para>
    /// <para lang="en">Control "Display periods"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlViewPeriods
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Seznam období</para>
        /// <para lang="en">List of estimation periods</para>
        /// </summary>
        private EstimationPeriodList dataSource;

        private string strRowsCountNone = String.Empty;
        private string strRowsCountOne = String.Empty;
        private string strRowsCountSome = String.Empty;
        private string strRowsCountMany = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">DataGridView pro zobrazení období</para>
        /// <para lang="en">DataGridView for display estimation periods</para>
        /// </summary>
        private DataGridView dgvPeriods;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Zavřít zobrazení výpočtetních období"</para>
        /// <para lang="en">Event
        /// "Close the display of estimation periods"</para>
        /// </summary>
        public event EventHandler Closed;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlViewPeriods(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Seznam období</para>
        /// <para lang="en">List of estimation periods</para>
        /// </summary>
        public EstimationPeriodList DataSource
        {
            get
            {
                return
                    dataSource ??
                    new EstimationPeriodList(database: Database);
            }
            set
            {
                dataSource = value ??
                    new EstimationPeriodList(database: Database);
                InitializeDataGridView();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazené sloupce</para>
        /// <para lang="en">Displayed columns</para>
        /// </summary>
        public string[] VisibleColumns
        {
            get
            {
                return
                    Setting.ViewPeriodsVisibleColumns;
            }
            set
            {
                Setting.ViewPeriodsVisibleColumns = value;
                EstimationPeriodList.SetColumnsVisibility(
                    columnNames: Setting.ViewPeriodsVisibleColumns);
                InitializeDataGridView();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Podmínka pro výběr záznamů v DataGridView
        /// </para>
        /// <para lang="en">
        /// Condition for selecting records in DataGridView
        /// </para>
        /// </summary>
        public string RowFilter
        {
            get
            {
                return DataSource.Data.DefaultView.RowFilter;
            }
            set
            {
                DataSource.RowFilter = value;
                SetLabelRowsCountValue(count: dgvPeriods.Rows.Count);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazit tlačítko pro výběr sloupců tabulky?</para>
        /// <para lang="en">Show button for selecting table columns?</para>
        /// </summary>
        public bool DisplayButtonFilterColumns
        {
            get
            {
                return btnFilterColumns.Visible;
            }
            set
            {
                btnFilterColumns.Visible = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazit tlačítko pro zavření ovládacího prvku?</para>
        /// <para lang="en">Show button to close the control?</para>
        /// </summary>
        public bool DisplayButtonClose
        {
            get
            {
                return btnClose.Visible;
            }
            set
            {
                btnClose.Visible = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnFilterColumns),                       "Výběr sloupců tabulky období" },
                        { nameof(btnClose),                               "Zavřít" },
                        { nameof(grpData),                                "Období:" },
                        { nameof(strRowsCountNone),                       "Není vybráno žádné období." },
                        { nameof(strRowsCountOne),                        "Je vybráno $1 období." },
                        { nameof(strRowsCountSome),                       "Jsou vybrány $1 období." },
                        { nameof(strRowsCountMany),                       "Je vybráno $1 období." },
                        { nameof(tsrTools),                               String.Empty },

                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}",
                            "Identifikační číslo období" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}",
                            "Identifikační číslo období" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}",
                            "Začátek období" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}",
                            "Začátek období" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}",
                            "Konec období" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}",
                            "Konec období" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelCs.Name}",
                            "Popisek období (národní)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelCs.Name}",
                            "Popisek období (národní)" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionCs.Name}",
                            "Popis období (národní)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionCs.Name}",
                            "Popis období (národní)" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelEn.Name}",
                            "Popisek období (anglický)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelEn.Name}",
                            "Popisek období (anglický)" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionEn.Name}",
                            "Popis období (anglický)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionEn.Name}",
                            "Popis období (anglický)" }
                    }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: nameof(ControlViewPeriods),
                            out Dictionary<string, string> dictNational)
                                ? dictNational
                                : [],

                LanguageVersion.International => (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                        { nameof(btnFilterColumns),                       "Table periods column selection" },
                        { nameof(btnClose),                               "Close" },
                        { nameof(grpData),                                "Periods:" },
                        { nameof(strRowsCountNone),                       "No estimation period is selected." },
                        { nameof(strRowsCountOne),                        "$1 estimation period is selected." },
                        { nameof(strRowsCountSome),                       "$1 estimation periods are selected." },
                        { nameof(strRowsCountMany),                       "$1 estimation periods are selected." },
                        { nameof(tsrTools),                               String.Empty },

                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}",
                            "Estimation period identifier" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}",
                            "Estimation period identifier" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}",
                            "Estimate date begin" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}",
                            "Estimate date begin" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}",
                            "Estimate date end" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}",
                            "Estimate date end" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelCs.Name}",
                            "Estimation period label (national)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelCs.Name}",
                            "Estimation period label (national)" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionCs.Name}",
                            "Estimation period description (national)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionCs.Name}",
                            "Estimation period description (national)" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelEn.Name}",
                            "Estimation period label (English)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColLabelEn.Name}",
                            "Estimation period label (English)" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionEn.Name}",
                            "Estimation period description (English)" },
                        { $"tip-{EstimationPeriodList.Name}.{EstimationPeriodList.ColDescriptionEn.Name}",
                            "Estimation period description (English)" }
                     }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlViewPeriods),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            DataSource = new EstimationPeriodList(database: Database);

            InitializeLabels();

            LoadContent();

            DisplayButtonFilterColumns = false;

            DisplayButtonClose = false;

            Setting.LoadViewPeriodsColumnsWidth();

            btnClose.Click += new EventHandler(
                (sender, e) => { Closed?.Invoke(sender: sender, e: e); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace DataGridView pro zobrazení období</para>
        /// <para lang="en">Initialization of the DataGridView for display estimation periods</para>
        /// </summary>
        private void InitializeDataGridView()
        {
            pnlData.Controls.Clear();

            dgvPeriods = new DataGridView()
            {
                Dock = DockStyle.Fill
            };

            dgvPeriods.SelectionChanged += (sender, e) =>
            {
                dgvPeriods.ClearSelection();
            };

            dgvPeriods.ColumnWidthChanged += (sender, e) =>
            {
                ColumnMetadata col =
                    EstimationPeriodList.Cols.Values
                        .Where(a => a.Name == e.Column.DataPropertyName)
                        .FirstOrDefault<ColumnMetadata>();
                if (col != null)
                {
                    col.Width = e.Column.Width;
                    Setting.SaveViewPeriodsColumnsWidth();
                }
            };

            dgvPeriods.CreateControl();

            pnlData.Controls.Add(value: dgvPeriods);

            DataSource.SetDataGridViewDataSource(
                dataGridView: dgvPeriods);

            DataSource.FormatDataGridView(
                dataGridView: dgvPeriods);

            SetLabelRowsCountValue(count: dgvPeriods.Rows.Count);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            #region Dictionary

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Dictionary<string, string> labelsCs = Dictionary(
                languageVersion: LanguageVersion.National,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsEn = Dictionary(
                languageVersion: LanguageVersion.International,
                languageFile: LanguageFile);

            #endregion Dictionary

            #region Controls

            btnFilterColumns.Text =
                labels.TryGetValue(key: nameof(btnFilterColumns),
                out string btnFilterColumnsText)
                    ? btnFilterColumnsText
                    : String.Empty;

            btnClose.Text =
                labels.TryGetValue(key: nameof(btnClose),
                out string btnCloseText)
                    ? btnCloseText
                    : String.Empty;

            grpData.Text =
                labels.TryGetValue(key: nameof(grpData),
                out string grpDataText)
                    ? grpDataText
                    : String.Empty;

            tsrTools.Text =
                labels.TryGetValue(key: nameof(tsrTools),
                out string tsrToolsText)
                    ? tsrToolsText
                    : String.Empty;

            #endregion Controls

            #region Visible Columns

            VisibleColumns =
                (LanguageVersion == LanguageVersion.National)
                ? Setting.ViewPeriodsVisibleColumns
                        .Select(a => a
                            .Replace(EstimationPeriodList.ColLabelCs.Name, EstimationPeriodList.ColLabelCs.Name)
                            .Replace(EstimationPeriodList.ColLabelEn.Name, EstimationPeriodList.ColLabelCs.Name)
                            .Replace(EstimationPeriodList.ColDescriptionCs.Name, EstimationPeriodList.ColDescriptionCs.Name)
                            .Replace(EstimationPeriodList.ColDescriptionEn.Name, EstimationPeriodList.ColDescriptionCs.Name))
                        .Distinct()
                        .ToArray<string>()
                : Setting.ViewPeriodsVisibleColumns
                        .Select(a => a
                            .Replace(EstimationPeriodList.ColLabelCs.Name, EstimationPeriodList.ColLabelEn.Name)
                            .Replace(EstimationPeriodList.ColLabelEn.Name, EstimationPeriodList.ColLabelEn.Name)
                            .Replace(EstimationPeriodList.ColDescriptionCs.Name, EstimationPeriodList.ColDescriptionEn.Name)
                            .Replace(EstimationPeriodList.ColDescriptionEn.Name, EstimationPeriodList.ColDescriptionEn.Name))
                        .Distinct()
                        .ToArray<string>();

            #endregion Visible Columns

            #region EstimationPeriodList

            foreach (ColumnMetadata column in EstimationPeriodList.Cols.Values)
            {
                EstimationPeriodList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{EstimationPeriodList.Name}.{column.Name}") ?
                    labelsCs[$"col-{EstimationPeriodList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{EstimationPeriodList.Name}.{column.Name}") ?
                    labelsEn[$"col-{EstimationPeriodList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{EstimationPeriodList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{EstimationPeriodList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{EstimationPeriodList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{EstimationPeriodList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion EstimationPeriodList
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
        }

        /// <summary>
        /// <para lang="cs">Nastavení popisku s počtem vybraných řádků</para>
        /// <para lang="en">Setting the label with the number of selected rows</para>
        /// </summary>
        /// <param name="count">
        /// <para lang="cs">Počet vybraných řádků</para>
        /// <para lang="en">Number of selected rows</para>
        /// </param>
        private void SetLabelRowsCountValue(int count)
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            switch (count)
            {
                case 0:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountNone),
                            out strRowsCountNone)
                                ? strRowsCountNone
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 1:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountOne),
                            out strRowsCountOne)
                                ? strRowsCountOne
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 2:
                case 3:
                case 4:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountSome),
                            out strRowsCountSome)
                                ? strRowsCountSome
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                default:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountMany),
                            out strRowsCountMany)
                                ? strRowsCountMany
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;
            }
        }

        #endregion Methods

    }

}