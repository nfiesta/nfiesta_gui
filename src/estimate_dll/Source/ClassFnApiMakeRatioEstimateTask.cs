﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Úkol pracovního vlákna pro výpočet odhadů podílu</para>
        /// <para lang="en">Task for worker thread for calculation of estimates of ratios</para>
        /// </summary>
        /// <remarks>
        /// <para lang="cs">Konstruktor</para>
        /// <para lang="en">Constructor</para>
        /// </remarks>
        /// <param name="estimate">
        /// <para lang="cs">Odhad podílu</para>
        /// <para lang="en">Ratio estimate</para>
        /// </param>
        internal class FnApiMakeRatioEstimateTask(OLAPRatioEstimate estimate)
            : ThreadTask<OLAPRatioEstimate>(element: estimate, priority: 1)
        { }

    }
}