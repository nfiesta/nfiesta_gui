﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlViewPanels
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlViewPanels));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.grpData = new System.Windows.Forms.GroupBox();
            this.pnlData = new System.Windows.Forms.Panel();
            this.pnlTools = new System.Windows.Forms.Panel();
            this.tsrTools = new System.Windows.Forms.ToolStrip();
            this.btnFilterColumns = new System.Windows.Forms.ToolStripButton();
            this.btnClose = new System.Windows.Forms.ToolStripButton();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.lblRowsCount = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.grpData.SuspendLayout();
            this.pnlTools.SuspendLayout();
            this.tsrTools.SuspendLayout();
            this.pnlStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(958, 538);
            this.pnlMain.TabIndex = 1;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.grpData, 0, 1);
            this.tlpMain.Controls.Add(this.pnlTools, 0, 0);
            this.tlpMain.Controls.Add(this.pnlStatus, 0, 2);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.Size = new System.Drawing.Size(958, 538);
            this.tlpMain.TabIndex = 0;
            // 
            // grpData
            // 
            this.grpData.Controls.Add(this.pnlData);
            this.grpData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpData.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpData.Location = new System.Drawing.Point(5, 35);
            this.grpData.Margin = new System.Windows.Forms.Padding(5);
            this.grpData.Name = "grpData";
            this.grpData.Padding = new System.Windows.Forms.Padding(5);
            this.grpData.Size = new System.Drawing.Size(948, 478);
            this.grpData.TabIndex = 11;
            this.grpData.TabStop = false;
            this.grpData.Text = "grpData";
            // 
            // pnlData
            // 
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(5, 20);
            this.pnlData.Margin = new System.Windows.Forms.Padding(0);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(938, 453);
            this.pnlData.TabIndex = 1;
            // 
            // pnlTools
            // 
            this.pnlTools.Controls.Add(this.tsrTools);
            this.pnlTools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTools.Location = new System.Drawing.Point(0, 0);
            this.pnlTools.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTools.Name = "pnlTools";
            this.pnlTools.Size = new System.Drawing.Size(958, 30);
            this.pnlTools.TabIndex = 2;
            // 
            // tsrTools
            // 
            this.tsrTools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFilterColumns,
            this.btnClose});
            this.tsrTools.Location = new System.Drawing.Point(0, 0);
            this.tsrTools.Name = "tsrTools";
            this.tsrTools.Padding = new System.Windows.Forms.Padding(0);
            this.tsrTools.Size = new System.Drawing.Size(958, 30);
            this.tsrTools.TabIndex = 0;
            this.tsrTools.Text = "tsrTools";
            // 
            // btnFilterColumns
            // 
            this.btnFilterColumns.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFilterColumns.Image = ((System.Drawing.Image)(resources.GetObject("btnFilterColumns.Image")));
            this.btnFilterColumns.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFilterColumns.Margin = new System.Windows.Forms.Padding(0);
            this.btnFilterColumns.Name = "btnFilterColumns";
            this.btnFilterColumns.Size = new System.Drawing.Size(23, 30);
            this.btnFilterColumns.Text = "btnFilterColumns";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(23, 30);
            this.btnClose.Text = "btnClose";
            // 
            // pnlStatus
            // 
            this.pnlStatus.Controls.Add(this.lblRowsCount);
            this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStatus.Location = new System.Drawing.Point(0, 518);
            this.pnlStatus.Margin = new System.Windows.Forms.Padding(0);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(958, 20);
            this.pnlStatus.TabIndex = 12;
            // 
            // lblRowsCount
            // 
            this.lblRowsCount.BackColor = System.Drawing.SystemColors.Control;
            this.lblRowsCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRowsCount.Location = new System.Drawing.Point(0, 0);
            this.lblRowsCount.Margin = new System.Windows.Forms.Padding(0);
            this.lblRowsCount.Name = "lblRowsCount";
            this.lblRowsCount.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblRowsCount.Size = new System.Drawing.Size(958, 20);
            this.lblRowsCount.TabIndex = 5;
            this.lblRowsCount.Text = "lblRowsCount";
            this.lblRowsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlViewPanels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlViewPanels";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.grpData.ResumeLayout(false);
            this.pnlTools.ResumeLayout(false);
            this.pnlTools.PerformLayout();
            this.tsrTools.ResumeLayout(false);
            this.tsrTools.PerformLayout();
            this.pnlStatus.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpData;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.Panel pnlTools;
        private System.Windows.Forms.ToolStrip tsrTools;
        private System.Windows.Forms.ToolStripButton btnFilterColumns;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.Label lblRowsCount;
        private System.Windows.Forms.ToolStripButton btnClose;
    }

}
