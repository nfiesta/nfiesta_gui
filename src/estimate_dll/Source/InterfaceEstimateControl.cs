﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Rozhraní ovládacích prvků modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Interface of the controls in the module for configuration and calculation estimates</para>
        /// </summary>
        internal interface IEstimateControl
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi (read-only)</para>
            /// <para lang="en">Files with control labels for national and international version (read-only)</para>
            /// </summary>
            LanguageFile LanguageFile { get; }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů (read-only)</para>
            /// <para lang="en">Module for configuration and calculation estimates setting (read-only)</para>
            /// </summary>
            Setting Setting { get; }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Vyvolá vyjímku když vlastník je null nebo neimplementuje rozhraní INfiEstaControl a IEstimateControl</para>
            /// <para lang="en">Throws exception when owner is null or does not implement interface INfiEstaControl and IEstimateControl</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník</para>
            /// <para lang="en">Owner</para>
            /// </param>
            /// <param name="name">
            /// <para lang="cs">Jméno vlastníka</para>
            /// <para lang="en">Owner name</para>
            /// </param>
            /// <exception cref="System.ArgumentNullException">
            /// <para lang="cs">Výjímka</para>
            /// <para lang="en">Exception</para>
            /// </exception>
            static void CheckOwner(System.Windows.Forms.Control owner, string name)
            {
                if (owner == null)
                {
                    throw new System.ArgumentNullException(
                        message: $"Argument {name} must not be null.",
                        paramName: name);
                }

                if (owner is not ZaJi.NfiEstaPg.INfiEstaControl)
                {
                    throw new System.ArgumentNullException(
                        message: $"Argument {name} must be type of {nameof(ZaJi.NfiEstaPg.INfiEstaControl)}.",
                        paramName: name);
                }

                if (owner is not IEstimateControl)
                {
                    throw new System.ArgumentNullException(
                        message: $"Argument {name} must be type of {nameof(IEstimateControl)}.",
                        paramName: name);
                }
            }

            #endregion Static Methods

        }

    }
}
