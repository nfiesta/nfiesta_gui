﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro výběr skupiny panelů a referenčních období pro konfiguraci nového odhadu</para>
    /// <para lang="en">Form for selecting panels reference year sets group for configuring a new estimate</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormEstimateConfiguration
         : Form, INfiEstaControl, IEstimateControl
    {

        #region Constants

        private const int LevelCountry = 0;
        private const int LevelStrataSet = 1;
        private const int LevelStratum = 2;

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;

        private const string Space = " ";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Seznam párů panel a referenční období</para>
        /// <para lang="en">List of panel and reference year set pairs</para>
        /// </summary>
        private TFnApiGetDataOptionsForSinglePhaseConfigList panelReferenceYearSetPairs;

        /// <summary>
        /// <para lang="cs">Výpočetní období</para>
        /// <para lang="en">Estimation period</para>
        /// </summary>
        private EstimationPeriod estimationPeriod;

        /// <summary>
        /// <para lang="cs">Seznam agregovaných skupin výpočetních buněk</para>
        /// <para lang="en">List of aggregated groups of estimation cells</para>
        /// </summary>
        private List<AggregatedEstimationCellGroup> aggregatedEstimationCellGroups;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Slovník s kontejnery ovládacích prvků se skupinami panelů a referenčních období pro jedno stratum</para>
        /// <para lang="en">Dictionary with containers of controls with panel reference year sets groups for one stratum</para>
        /// </summary>
        private Dictionary<int, System.Windows.Forms.Panel> dictPanelRefYearSetGroups;

        /// <summary>
        /// <para lang="cs">Slovník s kontejnery ovládacích prvků s páry panelů a referenčních období pro jedno stratum</para>
        /// <para lang="en">Dictionary with containers of controls with panel reference year sets pairs for one stratum</para>
        /// </summary>
        private Dictionary<int, System.Windows.Forms.Panel> dictPanelRefYearSetPairs;

        private string strSelectedPanelRefYearSetGroupForStratumCountNone = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumCountOne = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumCountSome = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumCountMany = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumCountTooMany = String.Empty;
        private string strPanelRefYearSetGroupsForStratumCountNone = String.Empty;
        private string strPanelRefYearSetGroupsForStratumCountOne = String.Empty;
        private string strPanelRefYearSetGroupsForStratumCountSome = String.Empty;
        private string strPanelRefYearSetGroupsForStratumCountMany = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumPlotsCountNone = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumPlotsCountOne = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumPlotsCountSome = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumPlotsCountMany = String.Empty;
        private string strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany = String.Empty;
        private string strPanelRefYearSetGroupsForStratumPlotsCountNone = String.Empty;
        private string strPanelRefYearSetGroupsForStratumPlotsCountOne = String.Empty;
        private string strPanelRefYearSetGroupsForStratumPlotsCountSome = String.Empty;
        private string strPanelRefYearSetGroupsForStratumPlotsCountMany = String.Empty;
        private string msgNoPanelRefYearSetPairs = String.Empty;
        private string msgNoPanelRefYearSetGroups = String.Empty;
        private string msgNotAllStrataResolved = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormEstimateConfiguration(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam párů panel a referenční období</para>
        /// <para lang="en">List of panel and reference year set pairs</para>
        /// </summary>
        public TFnApiGetDataOptionsForSinglePhaseConfigList PanelReferenceYearSetPairs
        {
            get
            {
                return panelReferenceYearSetPairs ??
                    new TFnApiGetDataOptionsForSinglePhaseConfigList(database: Database);
            }
            set
            {
                panelReferenceYearSetPairs = value ??
                   new TFnApiGetDataOptionsForSinglePhaseConfigList(database: Database);

                InitializeTreeView();

                DictPanelRefYearSetGroups = CreateNewPanelRefYearSetGroupsContainersForStrata();

                DictPanelRefYearSetPairs = CreateNewPanelRefYearSetPairsContainersForStrata();

                foreach (StratumCategory stratum in Strata)
                {
                    AfterSelectedPanelRefYearSetPairsChanged(stratum: stratum);
                }

                AfterSelectedStratumChanged();
            }
        }

        /// <summary>
        /// <para lang="cs">Výpočetní období</para>
        /// <para lang="en">Estimation period</para>
        /// </summary>
        public EstimationPeriod EstimationPeriod
        {
            get
            {
                return estimationPeriod;
            }
            set
            {
                estimationPeriod = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam agregovaných skupin výpočetních buněk</para>
        /// <para lang="en">List of aggregated groups of estimation cells</para>
        /// </summary>
        public List<AggregatedEstimationCellGroup> AggregatedEstimationCellGroups
        {
            get
            {
                return aggregatedEstimationCellGroups ?? [];
            }
            set
            {
                aggregatedEstimationCellGroups = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Slovník s kontejnery ovládacích prvků se skupinami panelů a referenčních období pro jedno stratum</para>
        /// <para lang="en">Dictionary with containers of controls with panel reference year sets groups for one stratum</para>
        /// </summary>
        private Dictionary<int, System.Windows.Forms.Panel> DictPanelRefYearSetGroups
        {
            get
            {
                return dictPanelRefYearSetGroups ?? [];
            }
            set
            {
                dictPanelRefYearSetGroups = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Slovník s kontejnery ovládacích prvků s páry panelů a referenčních období pro jedno stratum</para>
        /// <para lang="en">Dictionary with containers of controls with panel reference year sets pairs for one stratum</para>
        /// </summary>
        private Dictionary<int, System.Windows.Forms.Panel> DictPanelRefYearSetPairs
        {
            get
            {
                return dictPanelRefYearSetPairs ?? [];
            }
            set
            {
                dictPanelRefYearSetPairs = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam zemí</para>
        /// <para lang="en">List of countries</para>
        /// </summary>
        private IEnumerable<CountryCategory> Countries
        {
            get
            {
                return Nodes
                    .Where(a => a.Level == LevelCountry)
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is CountryCategory)
                    .Select(a => (CountryCategory)a.Tag);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam skupin strat</para>
        /// <para lang="en">List of strata sets</para>
        /// </summary>
        private IEnumerable<StrataSetCategory> StrataSets
        {
            get
            {
                return Nodes
                    .Where(a => a.Level == LevelStrataSet)
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is StrataSetCategory)
                    .Select(a => (StrataSetCategory)a.Tag);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam strat</para>
        /// <para lang="en">List of strata</para>
        /// </summary>
        private IEnumerable<StratumCategory> Strata
        {
            get
            {
                return Nodes
                    .Where(a => a.Level == LevelStratum)
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is StratumCategory)
                    .Select(a => (StratumCategory)a.Tag);
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná země</para>
        /// <para lang="en">Selected country</para>
        /// </summary>
        private CountryCategory SelectedCountry
        {
            get
            {
                if (tvwCountryStrataSetStratum.SelectedNode == null) { return null; }

                TreeNode node = tvwCountryStrataSetStratum.SelectedNode.Level switch
                {
                    LevelCountry => tvwCountryStrataSetStratum.SelectedNode,
                    LevelStrataSet => tvwCountryStrataSetStratum.SelectedNode?.Parent,
                    LevelStratum => tvwCountryStrataSetStratum.SelectedNode?.Parent?.Parent,
                    _ => null
                };

                if (node == null) { return null; }
                if (node.Tag == null) { return null; }
                if (node.Tag is not CountryCategory) { return null; }

                return (CountryCategory)node.Tag;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina strat</para>
        /// <para lang="en">Selected strata set</para>
        /// </summary>
        private StrataSetCategory SelectedStrataSet
        {
            get
            {
                if (tvwCountryStrataSetStratum.SelectedNode == null) { return null; }

                TreeNode node = tvwCountryStrataSetStratum.SelectedNode.Level switch
                {
                    LevelCountry => null,
                    LevelStrataSet => tvwCountryStrataSetStratum.SelectedNode,
                    LevelStratum => tvwCountryStrataSetStratum.SelectedNode?.Parent,
                    _ => null,
                };

                if (node == null) { return null; }
                if (node.Tag == null) { return null; }
                if (node.Tag is not StrataSetCategory) { return null; }

                return (StrataSetCategory)node.Tag;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané stratum</para>
        /// <para lang="en">Selected stratum</para>
        /// </summary>
        private StratumCategory SelectedStratum
        {
            get
            {
                if (tvwCountryStrataSetStratum.SelectedNode == null) { return null; }

                TreeNode node = tvwCountryStrataSetStratum.SelectedNode.Level switch
                {
                    LevelCountry => null,
                    LevelStrataSet => null,
                    LevelStratum => tvwCountryStrataSetStratum.SelectedNode,
                    _ => null,
                };

                if (node == null) { return null; }
                if (node.Tag == null) { return null; }
                if (node.Tag is not StratumCategory) { return null; }

                return (StratumCategory)node.Tag;
            }
        }

        /// <summary>
        /// <para lang="cs">Mají všechna strata potvrzenou skupinu panelů a referenčních období?</para>
        /// <para lang="en">Do all strata have a confirmed group of panels and reference year sets?</para>
        /// </summary>
        private bool AllStrataResolved
        {
            get
            {
                if (Strata == null) { return false; }
                if (!Strata.Any<StratumCategory>()) { return false; }

                return Strata.All(a => a.IsResolved);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimateConfiguration),                                    "Výběr skupiny kombinací panelů a referenčních období pro strata" },
                        { nameof(btnCancel),                                                    "Zrušit" },
                        { nameof(btnOK),                                                        "Uložit konfiguraci" },
                        { nameof(btnConfirm),                                                   "Potvrdit skupinu" },
                        { nameof(grpCountryStrataSetStratum),                                   "Země - Strata set - Stratum:" },
                        { nameof(grpPanelRefYearSetGroups),                                     "Skupiny kombinací:" },
                        { nameof(grpPanelRefYearSetPairs),                                      "Kombinace panelů a referenčních období:" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountNone),           "Vybraná skupina panelů a referenčních období nemá žádnou kombinaci" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountOne),            "Vybraná skupina panelů a referenčních období má $1 kombinaci" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountSome),           "Vybraná skupina panelů a referenčních období má $1 kombinace"  },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountMany),           "Vybraná skupina panelů a referenčních období má $1 kombinací" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountTooMany),        "Chyba: Vybraná skupina panelů a referenčních období má více kombinací ($1) než je dostupných ($2) ve stratu $3." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountNone),                  "z $1 dostupných ve stratu $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountOne),                   "z $1 dostupného ve stratu $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountSome),                  "z $1 dostupných ve stratu $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountMany),                  "z $1 dostupných ve stratu $2." },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountNone),      "Vybraná skupina panelů a referenčních období nemá žádnou inventarizační plochu" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountOne),       "Vybraná skupina panelů a referenčních období má $1 inventarizační plochu" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountSome),      "Vybraná skupina panelů a referenčních období má $1 inventarizační plochy" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountMany),      "Vybraná skupina panelů a referenčních období má $1 inventarizačních ploch" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany),   "Chyba: Vybraná skupina panelů a referenčních období má více inventarizačních ploch ($1) než je dostupných ($2) ve stratu $3." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountNone),             "z $1 dostupných inventarizačních ploch ve stratu $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountOne),              "z $1 dostupné inventarizační plochy ve stratu $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountSome),             "z $1 dostupných inventarizačních ploch ve stratu $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountMany),             "z $1 dostupných inventarizačních ploch ve stratu $2." },
                        { nameof(msgNoPanelRefYearSetPairs),                                    "Nejsou vybrané žádné kombinace panelů a referenčních období pro stratum $1." },
                        { nameof(msgNoPanelRefYearSetGroups),                                   "Není vybraná žádná skupina panelů a referenčních období pro stratum $1." },
                        { nameof(msgNotAllStrataResolved),                                      "Pro některá strata nejsou potvrzeny skupiny panelů a referenčních období." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimateConfiguration),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimateConfiguration),                                    "Groups of combinations of panels and reference year sets for strata selection" },
                        { nameof(btnCancel),                                                    "Cancel" },
                        { nameof(btnOK),                                                        "Save configuration" },
                        { nameof(btnConfirm),                                                   "Confirm group" },
                        { nameof(grpCountryStrataSetStratum),                                   "Country - Strata set - Stratum:" },
                        { nameof(grpPanelRefYearSetGroups),                                     "Groups of combinations:" },
                        { nameof(grpPanelRefYearSetPairs),                                      "Combination of panels and reference year sets:" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountNone),           "Selected group of panels and reference year sets has no combination " },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountOne),            "Selected group of panels and reference year sets has $1 combination" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountSome),           "Selected group of panels and reference year sets has $1 combinations"  },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountMany),           "Selected group of panels and reference year sets has $1 combinations" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumCountTooMany),        "Error: Selected group of panels and reference year sets has more combinations ($1) than available ($2) in stratum $3." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountNone),                  "from $1 available in stratum $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountOne),                   "from $1 available in stratum $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountSome),                  "from $1 available in stratum $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumCountMany),                  "from $1 available in stratum $2." },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountNone),      "Selected group of panels and reference year sets has no inventory plot" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountOne),       "Selected group of panels and reference year sets has $1 inventory plot" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountSome),      "Selected group of panels and reference year sets has $1 inventory plots" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountMany),      "Selected group of panels and reference year sets has $1 inventory plot" },
                        { nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany),   "Error: Selected group of panels and reference year sets has more inventory plots ($1) than available ($2) in stratum $3." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountNone),             "from $1 available inventory plots in stratum $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountOne),              "from $1 available inventory plot in stratum $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountSome),             "from $1 available inventory plots in stratum $2." },
                        { nameof(strPanelRefYearSetGroupsForStratumPlotsCountMany),             "from $1 available inventory plots in stratum $2." },
                        { nameof(msgNoPanelRefYearSetPairs),                                    "No panel and reference year set combinations are selected for stratum $1." },
                        { nameof(msgNoPanelRefYearSetGroups),                                   "No group of panels and reference year sets is selected for stratum $1." },
                        { nameof(msgNotAllStrataResolved),                                      "Groups of panels and reference year sets are not confirmed for some strata."}
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimateConfiguration),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            Nodes = [];

            PanelReferenceYearSetPairs =
                new TFnApiGetDataOptionsForSinglePhaseConfigList(
                    database: Database);

            EstimationPeriod = null;

            AggregatedEstimationCellGroups = [];

            OnEdit = false;

            DictPanelRefYearSetGroups = [];

            DictPanelRefYearSetPairs = [];

            LoadContent();

            InitializeLabels();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    AggregatedEstimationCellGroups =
                            GetAggregatedEstimationCellGroups();

                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    AggregatedEstimationCellGroups = [];

                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            btnConfirm.Click += new EventHandler(
                (sender, e) =>
                {
                    Confirm(stratum: SelectedStratum);
                });

            KeyDown += new KeyEventHandler(
                (sender, e) =>
                {
                    if (e.KeyCode == Keys.Space)
                    {
                        Confirm(stratum: SelectedStratum);
                    }
                });

            tvwCountryStrataSetStratum.AfterSelect += new TreeViewEventHandler(
                (sender, e) =>
                {
                    AfterSelectedStratumChanged();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        if (!VerifyAggregatedEstimationCellGroups(groups: AggregatedEstimationCellGroups))
                        {
                            // Pokud pro některou skupinu výpočetních buněk není určena skupina panelů a roků měření
                            // pak zabrání zavření formuláře
                            e.Cancel = true;
                        }
                    }
                });
        }


        /// <summary>
        /// <para lang="cs">Akce spouštěné po změně vybraného strata</para>
        /// <para lang="en">Actions triggered after a change of the selected stratum</para>
        /// </summary>
        private void AfterSelectedStratumChanged()
        {
            ShowPanelRefYearSetGroupsContainerForStratum(stratum: SelectedStratum);
            ShowPanelRefYearSetPairsContainerForStratum(stratum: SelectedStratum);

            ShowNumberOfPanelRefYearSetPairsForStratum(stratum: SelectedStratum);
            ShowSampleSizeForStratum(stratum: SelectedStratum);
        }

        /// <summary>
        /// <para lang="cs">Akce spouštěné po změně vybraných párů panelů a referenčních období</para>
        /// <para lang="en">Actions triggered after a change of the selected panel reference year set pairs</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        private void AfterSelectedPanelRefYearSetPairsChanged(StratumCategory stratum)
        {
            if (stratum == null) { return; }

            // Pokud bylo zadáno uživatelem (kliknutím)
            if (!OnEdit)
            {
                CheckPanelReferenceYearSetGroupForPairs(
                    stratum: stratum,
                    pairs: GetSelectedPanelRefYearSetPairsForStratum(stratum: stratum));

                // Vybrané páry musí uživatel potvrdit
                stratum.Reset(group: null, pairs: null);
                EnableNext();
            }

            ShowNumberOfPanelRefYearSetPairsForStratum(stratum: stratum);
            ShowSampleSizeForStratum(stratum: stratum);
        }

        /// <summary>
        /// <para lang="cs">Akce spouštěné po změně vybrané skupiny panelů a referenčních období</para>
        /// <para lang="en">Actions triggered after a change of the selected panel reference year set group</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <param name="selectedPanelRefYearSetGroup">
        /// <para lang="cs">Skupina panelů a roků měření</para>
        /// <para lang="en">Panel reference year set group</para>
        /// </param>
        private void AfterSelectedPanelRefYearSetGroupChanged(StratumCategory stratum, PanelRefYearSetGroup group)
        {
            if (stratum == null) { return; }

            // Pokud bylo zadáno uživatelem (kliknutím)
            if (!OnEdit)
            {
                CheckPanelReferenceYearSetPairsForGroup(
                    stratum: SelectedStratum,
                    group: group);

                // Vybranou skupinu musí uživatel potvrdit
                stratum.Reset(group: null, pairs: null);
                EnableNext();
            }

            ShowNumberOfPanelRefYearSetPairsForStratum(stratum: stratum);
            ShowSampleSizeForStratum(stratum: stratum);
        }


        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormEstimateConfiguration),
                out string formEstimateConfigurationText)
                    ? formEstimateConfigurationText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnConfirm.Text =
                labels.TryGetValue(key: nameof(btnConfirm),
                out string btnConfirmText)
                    ? btnConfirmText
                    : String.Empty;

            grpCountryStrataSetStratum.Text =
                labels.TryGetValue(key: nameof(grpCountryStrataSetStratum),
                out string grpCountryStrataSetStratumText)
                    ? grpCountryStrataSetStratumText
                    : String.Empty;

            grpPanelRefYearSetGroups.Text =
                labels.TryGetValue(key: nameof(grpPanelRefYearSetGroups),
                out string grpPanelRefYearSetGroupsText)
                    ? grpPanelRefYearSetGroupsText
                    : String.Empty;

            grpPanelRefYearSetPairs.Text =
                labels.TryGetValue(key: nameof(grpPanelRefYearSetPairs),
                out string grpPanelRefYearSetPairsText)
                    ? grpPanelRefYearSetPairsText
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumCountNone =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumCountNone),
                out strSelectedPanelRefYearSetGroupForStratumCountNone)
                    ? strSelectedPanelRefYearSetGroupForStratumCountNone
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumCountOne =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumCountOne),
                out strSelectedPanelRefYearSetGroupForStratumCountOne)
                    ? strSelectedPanelRefYearSetGroupForStratumCountOne
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumCountSome =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumCountSome),
                out strSelectedPanelRefYearSetGroupForStratumCountSome)
                    ? strSelectedPanelRefYearSetGroupForStratumCountSome
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumCountMany =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumCountMany),
                out strSelectedPanelRefYearSetGroupForStratumCountMany)
                    ? strSelectedPanelRefYearSetGroupForStratumCountMany
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumCountTooMany =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumCountTooMany),
                out strSelectedPanelRefYearSetGroupForStratumCountTooMany)
                    ? strSelectedPanelRefYearSetGroupForStratumCountTooMany
                    : String.Empty;

            strPanelRefYearSetGroupsForStratumCountNone =
                labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumCountNone),
                out strPanelRefYearSetGroupsForStratumCountNone)
                    ? strPanelRefYearSetGroupsForStratumCountNone
                    : String.Empty;

            strPanelRefYearSetGroupsForStratumCountOne =
                labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumCountOne),
                out strPanelRefYearSetGroupsForStratumCountOne)
                    ? strPanelRefYearSetGroupsForStratumCountOne
                    : String.Empty;

            strPanelRefYearSetGroupsForStratumCountSome =
                labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumCountSome),
                out strPanelRefYearSetGroupsForStratumCountSome)
                    ? strPanelRefYearSetGroupsForStratumCountSome
                    : String.Empty;

            strPanelRefYearSetGroupsForStratumCountMany =
                labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumCountMany),
                out strPanelRefYearSetGroupsForStratumCountMany)
                    ? strPanelRefYearSetGroupsForStratumCountMany
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumPlotsCountNone =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountNone),
                out strSelectedPanelRefYearSetGroupForStratumPlotsCountNone)
                    ? strSelectedPanelRefYearSetGroupForStratumPlotsCountNone
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumPlotsCountOne =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountOne),
                out strSelectedPanelRefYearSetGroupForStratumPlotsCountOne)
                    ? strSelectedPanelRefYearSetGroupForStratumPlotsCountOne
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumPlotsCountSome =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountSome),
                out strSelectedPanelRefYearSetGroupForStratumPlotsCountSome)
                    ? strSelectedPanelRefYearSetGroupForStratumPlotsCountSome
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumPlotsCountMany =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountMany),
                out strSelectedPanelRefYearSetGroupForStratumPlotsCountMany)
                    ? strSelectedPanelRefYearSetGroupForStratumPlotsCountMany
                    : String.Empty;

            strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany =
                labels.TryGetValue(key: nameof(strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany),
                out strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany)
                    ? strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany
                    : String.Empty;

            strPanelRefYearSetGroupsForStratumPlotsCountNone =
               labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumPlotsCountNone),
               out strPanelRefYearSetGroupsForStratumPlotsCountNone)
                   ? strPanelRefYearSetGroupsForStratumPlotsCountNone
                   : String.Empty;

            strPanelRefYearSetGroupsForStratumPlotsCountOne =
                labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumPlotsCountOne),
                out strPanelRefYearSetGroupsForStratumPlotsCountOne)
                    ? strPanelRefYearSetGroupsForStratumPlotsCountOne
                    : String.Empty;

            strPanelRefYearSetGroupsForStratumPlotsCountSome =
                labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumPlotsCountSome),
                out strPanelRefYearSetGroupsForStratumPlotsCountSome)
                    ? strPanelRefYearSetGroupsForStratumPlotsCountSome
                    : String.Empty;

            strPanelRefYearSetGroupsForStratumPlotsCountMany =
                labels.TryGetValue(key: nameof(strPanelRefYearSetGroupsForStratumPlotsCountMany),
                out strPanelRefYearSetGroupsForStratumPlotsCountMany)
                    ? strPanelRefYearSetGroupsForStratumPlotsCountMany
                    : String.Empty;

            msgNoPanelRefYearSetPairs =
                labels.TryGetValue(key: nameof(msgNoPanelRefYearSetPairs),
                out msgNoPanelRefYearSetPairs)
                    ? msgNoPanelRefYearSetPairs
                    : String.Empty;

            msgNoPanelRefYearSetGroups =
                labels.TryGetValue(key: nameof(msgNoPanelRefYearSetGroups),
                out msgNoPanelRefYearSetGroups)
                    ? msgNoPanelRefYearSetGroups
                    : String.Empty;

            msgNotAllStrataResolved =
                labels.TryGetValue(key: nameof(msgNotAllStrataResolved),
                out msgNotAllStrataResolved)
                    ? msgNotAllStrataResolved
                    : String.Empty;

            foreach (TreeNode node in Nodes
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is Category))
            {
                Category category = (Category)node.Tag;
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                        ? category.LabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? category.LabelCs
                            : category.LabelEn;
                node.ToolTipText =
                    (LanguageVersion == LanguageVersion.International)
                        ? category.DescriptionEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? category.DescriptionCs
                            : category.DescriptionEn;
            }

            foreach (RadioButton radioButton in pnlPanelRefYearSetGroups.Controls
                .OfType<RadioButton>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is PanelRefYearSetGroup))
            {
                PanelRefYearSetGroup panelRefYearSetGroup = (PanelRefYearSetGroup)radioButton.Tag;
                radioButton.Text =
                    (panelRefYearSetGroup == null)
                        ? String.Empty
                        : (LanguageVersion == LanguageVersion.International)
                            ? panelRefYearSetGroup.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? panelRefYearSetGroup.LabelCs
                                : panelRefYearSetGroup.LabelEn;
            }

            foreach (ControlPanelReferenceYearSet ctrPanelReferenceYearSet in pnlPanelRefYearSetPairs.Controls
                .OfType<ControlPanelReferenceYearSet>())
            {
                ctrPanelReferenceYearSet.InitializeLabels();
            }

            ShowNumberOfPanelRefYearSetPairsForStratum(stratum: SelectedStratum);
            ShowSampleSizeForStratum(stratum: SelectedStratum);
        }

        /// <summary>
        /// <para lang="cs">Zobrazení strat v uzlech TreeView</para>
        /// <para lang="en">Displaying strata in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            tvwCountryStrataSetStratum.BeginUpdate();
            tvwCountryStrataSetStratum.Nodes.Clear();
            Nodes.Clear();

            // Přidání uzlů první úrovně
            // (Seznam zemí)
            IEnumerable<DataRow> cteCountries = PanelReferenceYearSetPairs
                .GetCountries()
                .AsEnumerable()
                .Where(a => a.Field<Nullable<int>>(
                    columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.Name) != null);
            foreach (DataRow rowCountry in cteCountries)
            {
                CountryCategory country = new()
                {
                    Id = rowCountry.Field<int>(
                        columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.Name),
                    LabelCs = rowCountry.Field<string>(
                        columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelCs.Name),
                    DescriptionCs = rowCountry.Field<string>(
                        columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionCs.Name),
                    LabelEn = rowCountry.Field<string>(
                        columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelEn.Name),
                    DescriptionEn = rowCountry.Field<string>(
                        columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionEn.Name),
                };

                TreeNode nodeCountry = new()
                {
                    ImageIndex = ImageGrayBall,
                    Name = $"Country-{country.Id}",
                    SelectedImageIndex = ImageGrayBall,
                    Tag = country,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? country.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? country.LabelCs
                                : country.LabelEn,
                    ToolTipText =
                         (LanguageVersion == LanguageVersion.International)
                                ? country.DescriptionEn
                                : (LanguageVersion == LanguageVersion.National)
                                    ? country.DescriptionCs
                                    : country.DescriptionEn,
                };
                country.Node = nodeCountry;

                // Přidání uzlů druhé úrovně
                // (Seznam skupin strat)
                IEnumerable<DataRow> cteStrataSets = PanelReferenceYearSetPairs
                    .GetStrataSets(countryId: country.Id)
                    .AsEnumerable()
                    .Where(a => a.Field<Nullable<int>>(
                        columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.Name) != null);
                foreach (DataRow rowStrataSet in cteStrataSets)
                {
                    StrataSetCategory strataSet = new()
                    {
                        Id = rowStrataSet.Field<int>(
                            columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.Name),
                        LabelCs = rowStrataSet.Field<string>(
                            columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelCs.Name),
                        DescriptionCs = rowStrataSet.Field<string>(
                            columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionCs.Name),
                        LabelEn = rowStrataSet.Field<string>(
                            columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelEn.Name),
                        DescriptionEn = rowStrataSet.Field<string>(
                            columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionEn.Name),
                    };

                    TreeNode nodeStrataSet = new()
                    {
                        ImageIndex = ImageGrayBall,
                        Name = $"StrataSet-{strataSet.Id}",
                        SelectedImageIndex = ImageGrayBall,
                        Tag = strataSet,
                        Text =
                            (LanguageVersion == LanguageVersion.International)
                                ? strataSet.LabelEn
                                : (LanguageVersion == LanguageVersion.National)
                                    ? strataSet.LabelCs
                                    : strataSet.LabelEn,
                        ToolTipText =
                            (LanguageVersion == LanguageVersion.International)
                                ? strataSet.DescriptionEn
                                : (LanguageVersion == LanguageVersion.National)
                                    ? strataSet.DescriptionCs
                                    : strataSet.DescriptionEn,
                    };
                    strataSet.Node = nodeStrataSet;

                    // Přidání uzlů třetí úrovně
                    // (Seznam strat)
                    IEnumerable<DataRow> cteStrata = PanelReferenceYearSetPairs
                        .GetStrata(countryId: country.Id, strataSetId: strataSet.Id)
                        .AsEnumerable()
                        .Where(a => a.Field<Nullable<int>>(
                            columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.Name) != null);
                    foreach (DataRow rowStratum in cteStrata)
                    {
                        StratumCategory stratum = new()
                        {
                            Id = rowStratum.Field<int>(
                                columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.Name),
                            LabelCs = rowStratum.Field<string>(
                                columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelCs.Name),
                            DescriptionCs = rowStratum.Field<string>(
                                columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionCs.Name),
                            LabelEn = rowStratum.Field<string>(
                                columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelEn.Name),
                            DescriptionEn = rowStratum.Field<string>(
                                columnName: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionEn.Name),
                        };

                        TreeNode nodeStratum = new()
                        {
                            ImageIndex = ImageGrayBall,
                            Name = $"Stratum-{stratum.Id}",
                            SelectedImageIndex = ImageGrayBall,
                            Tag = stratum,
                            Text =
                                (LanguageVersion == LanguageVersion.International)
                                    ? stratum.LabelEn
                                    : (LanguageVersion == LanguageVersion.National)
                                        ? stratum.LabelCs
                                        : stratum.LabelEn,
                            ToolTipText =
                                (LanguageVersion == LanguageVersion.International)
                                    ? stratum.DescriptionEn
                                    : (LanguageVersion == LanguageVersion.National)
                                        ? stratum.DescriptionCs
                                        : stratum.DescriptionEn,
                        };
                        stratum.Node = nodeStratum;
                        stratum.Reset(group: null, pairs: null);
                        EnableNext();

                        nodeStrataSet.Nodes.Add(node: nodeStratum);
                        Nodes.Add(item: nodeStratum);
                    }
                    nodeCountry.Nodes.Add(node: nodeStrataSet);
                    Nodes.Add(item: nodeStrataSet);
                }
                tvwCountryStrataSetStratum.Nodes.Add(node: nodeCountry);
                Nodes.Add(item: nodeCountry);
            }

            tvwCountryStrataSetStratum.EndUpdate();

            if (tvwCountryStrataSetStratum.Nodes.Count > 0)
            {
                tvwCountryStrataSetStratum.SelectedNode = tvwCountryStrataSetStratum.Nodes[0];
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Vytvoří nový kontejner SKUPIN panelů a referenčních období pro jedno stratum
        /// </para>
        /// <para lang="en">
        /// Creates a new panel reference year set GROUPS container for one stratum
        /// </para>
        /// </summary>
        private System.Windows.Forms.Panel CreateNewPanelRefYearSetGroupContainerForStratum(StratumCategory stratum)
        {
            // Pokud stratum není zadané vrátí null.
            if (stratum == null)
            {
                return null;
            }

            // Vytvoří nový konteiner skupin pro stratum.
            System.Windows.Forms.Panel pnlContainer = new()
            {
                AutoScroll = true,
                BackColor = Setting.PanelBackColor,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Font = Setting.PanelFont,
                ForeColor = Setting.PanelForeColor,
                Margin = Setting.PanelMargin,
                Name = $"pnlGroupsForStratum-{stratum.Id}",
                Padding = Setting.PanelPadding,
                Tag = stratum,
                Visible = true
            };

            // Řazení skupin v kontejneru je podle popisku.
            Func<PanelRefYearSetGroup, string> labelSorting = new(a =>
            {
                return
                    (LanguageVersion == LanguageVersion.International)
                        ? a.LabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? a.LabelCs
                            : a.LabelEn;
            });

            // Získá skupinu voláním uložené procedury z databáze.
            PanelRefYearSetGroupList panelRefYearSetGroupsForStratum =
                NfiEstaFunctions.FnApiGetPanelRefYearSetGroupsForStratum.Execute(
                    database: Database,
                    stratumId: stratum.Id);

            OnEdit = true;
            foreach (PanelRefYearSetGroup panelRefYearSetGroup in
                panelRefYearSetGroupsForStratum.Items.OrderByDescending(labelSorting))
            {
                // Vytvoření RadioButtons pro jednotlivé skupiny
                RadioButton radioButton = new()
                {
                    BackColor = Color.White,
                    Checked = false,
                    Dock = DockStyle.Top,
                    Font = Setting.RadioButtonFont,
                    ForeColor = Setting.RadioButtonForeColor,
                    Height = 25,
                    Tag = panelRefYearSetGroup,
                    Text = (LanguageVersion == LanguageVersion.International)
                        ? panelRefYearSetGroup.LabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? panelRefYearSetGroup.LabelCs
                            : panelRefYearSetGroup.LabelEn
                };

                // Nastavení ToolTipu pro RadioButton skupiny
                ToolTip toolTip = new();
                toolTip.SetToolTip(
                    control: radioButton,
                    caption: (LanguageVersion == LanguageVersion.International)
                        ? panelRefYearSetGroup.DescriptionEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? panelRefYearSetGroup.DescriptionCs
                            : panelRefYearSetGroup.DescriptionEn);

                // Obsluha události, při změně výběru RadioButton skupiny
                radioButton.CheckedChanged += new EventHandler(
                    (sender, e) =>
                    {
                        if (!((RadioButton)sender).Checked) { return; }
                        if (((RadioButton)sender).Tag == null) { return; }
                        if (((RadioButton)sender).Tag is not PanelRefYearSetGroup) { return; }

                        AfterSelectedPanelRefYearSetGroupChanged(
                            stratum: SelectedStratum,
                            group: (PanelRefYearSetGroup)((RadioButton)sender).Tag);
                    });

                // Přidání RadioButton skupiny do kontejneru
                pnlContainer.Controls.Add(value: radioButton);
            }
            OnEdit = false;

            // Vrací kontejner s RadioButtons skupin panelů a referenčních období pro jedno stratum
            return pnlContainer;
        }

        /// <summary>
        /// <para lang="cs">
        /// Vytvoří nový slovník s kontejnery ovládacích prvků (RadioButtons)
        /// se SKUPINAMI panelů a referenčních období pro všechna strata
        /// </para>
        /// <para lang="en">
        /// Creates new dictionary with containers of controls (RadioButtons)
        /// with panel reference year sets GROUPS for all strata
        /// </para>
        /// </summary>
        private Dictionary<int, System.Windows.Forms.Panel> CreateNewPanelRefYearSetGroupsContainersForStrata()
        {
            Dictionary<int, System.Windows.Forms.Panel> dictionary = [];

            // Přidává do slovníku kontejnery skupin pro jednotlivá strata
            foreach (StratumCategory stratum in Strata)
            {
                dictionary.TryAdd(
                    key: stratum.Id,
                    value: CreateNewPanelRefYearSetGroupContainerForStratum(stratum: stratum));
            }

            return dictionary;
        }


        /// <summary>
        /// <para lang="cs">
        /// Vytvoří nový kontejner PÁRů panelů a referenčních období pro jedno stratum
        /// </para>
        /// <para lang="en">
        /// Creates a new panel reference year set PAIRS container for one stratum
        /// </para>
        /// </summary>
        private System.Windows.Forms.Panel CreateNewPanelRefYearSetPairsContainerForStratum(StratumCategory stratum)
        {
            // Pokud stratum není zadané vrátí null.
            if (stratum == null)
            {
                return null;
            }

            // Vytvoří nový konteiner párů pro stratum.
            System.Windows.Forms.Panel pnlContainer = new()
            {
                AutoScroll = true,
                BackColor = Setting.PanelBackColor,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Font = Setting.PanelFont,
                ForeColor = Setting.PanelForeColor,
                Margin = Setting.PanelMargin,
                Name = $"pnlCombinationsForStratum-{stratum.Id}",
                Padding = Setting.PanelPadding,
                Tag = stratum,
                Visible = true
            };

            // Řazení skupin v kontejneru
            // --------------------------
            // Order this subset of records according to:
            // Reference year set fully within estimation period (YES first)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, int> refYearSetFullyWithinEstimationPeriodSorting = new(a =>
            {
                return
                    (a.RefYearSetFullyWithinEstimationPeriod == null)
                        ? 2
                        : ((bool)a.RefYearSetFullyWithinEstimationPeriod)
                            ? 0
                            : 1;
            });

            // Share of ref. year set intersected by estimation period (DESC)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, double> shareOfRefYearSetIntersectedByEstimationPeriodSorting = new(a =>
            {
                return (double)(a.ShareOfRefYearSetIntersectedByEstimationPeriod ?? 0.0);
            });

            // Share of estimation period intersected ref. year set (DESC)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, double> shareOfEstimationPeriodIntersectedByRefYearSetSorting = new(a =>
            {
                return (double)(a.ShareOfEstimationPeriodIntersectedByRefYearSet ?? 0.0);
            });

            // Distance of the reference-year set range (time interval) from the estimation period range (intersection means zero distance) (ASC)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, double> referenceYearSetRangeDistanceSorting = new(a =>
            {
                IEnumerable<DateTime> beginnings =
                     new List<Nullable<DateTime>>() { a?.ReferenceDateBegin, EstimationPeriod?.EstimateDateBegin }
                     .Where(a => a != null)
                     .Select(a => (DateTime)a);

                IEnumerable<DateTime> endings =
                     new List<Nullable<DateTime>>() { a?.ReferenceDateEnd, EstimationPeriod?.EstimateDateEnd }
                     .Where(a => a != null)
                     .Select(a => (DateTime)a);

                if (!beginnings.Any()) { return default; }
                if (!endings.Any()) { return default; }

                TimeSpan span = beginnings.Max<DateTime>() - endings.Min<DateTime>();

                return (span.TotalSeconds <= (double)0.0)
                    ? (double)0.0 :
                    span.TotalSeconds;
            });

            // Width of the reference-year set range (ASC)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, double> referenceYearSetRangeWidthSorting = new(a =>
            {
                if ((a?.ReferenceDateBegin == null) || (a?.ReferenceDateEnd == null))
                {
                    return default;
                }

                return ((DateTime)a.ReferenceDateEnd - (DateTime)a.ReferenceDateBegin).TotalSeconds;
            });

            // Sample size (DESC)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, double> sSizeSorting = new(a =>
            {
                return (int)(a.SSize ?? 0);
            });

            // Panel (alphabetically, ASC)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, string> panelLabelSorting = new(a =>
            {
                return
                    (LanguageVersion == LanguageVersion.International)
                        ? a.PanelLabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? a.PanelLabelCs
                            : a.PanelLabelEn;
            });

            // Reference-year set (alphabetically, ASC)
            Func<TFnApiGetDataOptionsForSinglePhaseConfig, string> referenceYearSetLabelSorting = new(a =>
            {
                return
                    (LanguageVersion == LanguageVersion.International)
                        ? a.ReferenceYearSetLabelCs
                        : (LanguageVersion == LanguageVersion.National)
                            ? a.ReferenceYearSetLabelCs
                            : a.ReferenceYearSetLabelEn;
            });

            OnEdit = true;
            foreach (TFnApiGetDataOptionsForSinglePhaseConfig panelRefYearSetPair in
                PanelReferenceYearSetPairs.Items
                    .Where(a => a.StratumId == stratum.Id)
                    .OrderByDescending(refYearSetFullyWithinEstimationPeriodSorting)
                    .ThenBy(shareOfRefYearSetIntersectedByEstimationPeriodSorting)
                    .ThenBy(shareOfEstimationPeriodIntersectedByRefYearSetSorting)
                    .ThenByDescending(referenceYearSetRangeDistanceSorting)
                    .ThenByDescending(referenceYearSetRangeWidthSorting)
                    .ThenBy(sSizeSorting)
                    .ThenByDescending(panelLabelSorting)
                    .ThenByDescending(referenceYearSetLabelSorting))
            {
                // Zabraňuje zobrazení stejných párů panelů a referenčních období
                // Zobrazen je vždy pouze první pár panel-referenční období ve skupině country-strataset-stratum
                // z návratu funkce fn_api_get_data_options_for_single_phase_config
                if (pnlContainer.Controls
                    .OfType<ControlPanelReferenceYearSet>()
                    .Where(a => a.PanelReferenceYearSetPair != null)
                    .Where(a => a.PanelReferenceYearSetPair.PanelId == panelRefYearSetPair.PanelId)
                    .Where(a => a.PanelReferenceYearSetPair.ReferenceYearSetId == panelRefYearSetPair.ReferenceYearSetId)
                    .Any())
                {
                    continue;
                }

                // Vytvoření ovládacího prvku pro výběr páru panelu a referenčního období
                ControlPanelReferenceYearSet ctrPanelReferenceYearSet = new(
                    controlOwner: this,
                    panelReferenceYearSetPair: panelRefYearSetPair)
                {
                    Dock = DockStyle.Top,
                    IsCollapsed = true,
                    IsCheckBoxDisplayed = true,
                    Visible = true
                };

                // Obsluha události, při změně výběru páru panelu a referenčního období
                ctrPanelReferenceYearSet.OnSelectionChanged += new EventHandler(
                    (sender, e) =>
                    {
                        AfterSelectedPanelRefYearSetPairsChanged(
                            stratum: SelectedStratum);
                    });

                // Předvýběr párů panelů a referenčního období, které jsou plně uvnitř výpočetního období
                ctrPanelReferenceYearSet.PreSelect();

                // Přidání ovládacího prveku pro výběr páru panelu a referenčního období do kontejneru
                pnlContainer.Controls.Add(value: ctrPanelReferenceYearSet);
            }
            OnEdit = false;

            // Vrátí kontejner s ovládacími prvky pro výběr páru panelu a referenčního období pro jedno stratum
            return pnlContainer;
        }

        /// <summary>
        /// <para lang="cs">
        /// Vytvoří nový slovník s kontejnery ovládacích prvků
        /// s PÁRY panelů a referenčních období pro všechna strata</para>
        /// <para lang="en">
        /// Creates new dictionary with containers of controls
        /// with panel reference year sets PAIRS for all strata</para>
        /// </summary>
        private Dictionary<int, System.Windows.Forms.Panel> CreateNewPanelRefYearSetPairsContainersForStrata()
        {
            Dictionary<int, System.Windows.Forms.Panel> dictionary = [];

            // Přidává do slovníku kontejnery skupin pro jednotlivá strata
            foreach (StratumCategory stratum in Strata)
            {
                dictionary.TryAdd(
                    key: stratum.Id,
                    value: CreateNewPanelRefYearSetPairsContainerForStratum(stratum: stratum));
            }

            return dictionary;
        }


        /// <summary>
        /// <summary>
        /// <para lang="cs">Kontejner se SKUPINAMI panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets groups for stratum</para>
        /// </summary>
        /// </summary>
        /// <param name="stratumId">
        /// <para lang="cs">Stratum identifier</para>
        /// <para lang="en">Stratum identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Kontejner se skupinami panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets groups for stratum</para>
        /// </returns>
        private System.Windows.Forms.Panel GetContainerPanelRefYearSetGroupsForStratum(Nullable<int> stratumId)
        {
            if (stratumId == null) { return null; }
            if (!DictPanelRefYearSetGroups.TryGetValue(key: (int)stratumId, out System.Windows.Forms.Panel container)) { return null; }
            return container;
        }

        /// <summary>
        /// <summary>
        /// <para lang="cs">Kontejner se SKUPINAMI panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets groups for stratum</para>
        /// </summary>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Kontejner se skupinami panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets groups for stratum</para>
        /// </returns>
        private System.Windows.Forms.Panel GetContainerPanelRefYearSetGroupsForStratum(StratumCategory stratum)
        {
            if (stratum == null) { return null; }
            if (!DictPanelRefYearSetGroups.TryGetValue(key: stratum.Id, out System.Windows.Forms.Panel container)) { return null; }
            return container;
        }

        /// <summary>
        /// <para lang="cs">SKUPINY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panel reference year set groups for stratum</para>
        /// </summary>
        /// <param name="stratumId">
        /// <para lang="cs">Stratum identifier</para>
        /// <para lang="en">Stratum identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Skupiny panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panel reference year set groups for stratum</para>
        /// </returns>
        private List<PanelRefYearSetGroup> GetPanelRefYearSetGroupsForStratum(Nullable<int> stratumId)
        {
            System.Windows.Forms.Panel container =
                    GetContainerPanelRefYearSetGroupsForStratum(stratumId: stratumId);

            if (container == null)
            {
                return [];
            }

            return container.Controls
                    .OfType<RadioButton>()
                    .Where(a => (a.Tag != null) && (a.Tag is PanelRefYearSetGroup))
                    .Select(a => (PanelRefYearSetGroup)a.Tag)
                    .ToList<PanelRefYearSetGroup>();
        }

        /// <summary>
        /// <para lang="cs">SKUPINY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panel reference year set groups for stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Skupiny panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panel reference year set groups for stratum</para>
        /// </returns>
        private List<PanelRefYearSetGroup> GetPanelRefYearSetGroupsForStratum(StratumCategory stratum)
        {
            return GetPanelRefYearSetGroupsForStratum(stratumId: stratum?.Id);
        }

        /// <summary>
        /// <para lang="cs">Vybraná SKUPINA panelů a referenčních období pro stratum</para>
        /// <para lang="en">Selected panels reference year sets group for stratum</para>
        /// </summary>
        /// <param name="stratumId">
        /// <para lang="cs">Stratum identifier</para>
        /// <para lang="en">Stratum identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vybraná skupina panelů a referenčních období pro stratum</para>
        /// <para lang="en">Selected panels reference year sets group for stratum</para>
        /// </returns>
        private PanelRefYearSetGroup GetSelectedPanelRefYearSetGroupForStratum(Nullable<int> stratumId)
        {
            System.Windows.Forms.Panel container =
                    GetContainerPanelRefYearSetGroupsForStratum(stratumId: stratumId);

            if (container == null)
            {
                return null;
            }

            return container.Controls
                .OfType<RadioButton>()
                .Where(a => (a.Tag != null) && (a.Tag is PanelRefYearSetGroup))
                .Where(a => a.Checked)
                .Select(a => (PanelRefYearSetGroup)a.Tag)
                .FirstOrDefault<PanelRefYearSetGroup>();
        }

        /// <summary>
        /// <para lang="cs">Vybraná SKUPINA panelů a referenčních období pro vybrané stratum</para>
        /// <para lang="en">Selected panels reference year sets group for selected stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vybraná skupina panelů a referenčních období pro vybrané stratum</para>
        /// <para lang="en">Selected panels reference year sets group for selected stratum</para>
        /// </returns>
        private PanelRefYearSetGroup GetSelectedPanelRefYearSetGroupForStratum(StratumCategory stratum)
        {
            return GetSelectedPanelRefYearSetGroupForStratum(stratumId: stratum?.Id);
        }


        /// <summary>
        /// <para lang="cs">Kontejner s PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets pairs for stratum</para>
        /// </summary>
        /// <param name="stratumId">
        /// <para lang="cs">Stratum identifier</para>
        /// <para lang="en">Stratum identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Kontejner s PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets pairs for stratum</para>
        /// </returns>
        private System.Windows.Forms.Panel GetContainerPanelRefYearSetPairsForStratum(Nullable<int> stratumId)
        {
            if (stratumId == null) { return null; }
            if (!DictPanelRefYearSetPairs.TryGetValue(key: (int)stratumId, out System.Windows.Forms.Panel container)) { return null; }
            return container;
        }

        /// <summary>
        /// <para lang="cs">Kontejner s PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets pairs for stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Kontejner s PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Container with panels reference year sets pairs for stratum</para>
        /// </returns>
        private System.Windows.Forms.Panel GetContainerPanelRefYearSetPairsForStratum(StratumCategory stratum)
        {
            if (stratum == null) { return null; }
            if (!DictPanelRefYearSetPairs.TryGetValue(key: stratum.Id, out System.Windows.Forms.Panel container)) { return null; }
            return container;
        }

        /// <summary>
        /// <para lang="cs">PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panels and reference year sets pairs for stratum</para>
        /// </summary>
        /// <param name="stratumId">
        /// <para lang="cs">Stratum identifier</para>
        /// <para lang="en">Stratum identifier</para>
        /// </param>
        /// <para lang="cs">PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panels and reference year sets pairs for stratum</para>
        /// </returns>
        private List<TFnApiGetDataOptionsForSinglePhaseConfig> GetPanelRefYearSetPairsForStratum(Nullable<int> stratumId)
        {
            System.Windows.Forms.Panel container =
                GetContainerPanelRefYearSetPairsForStratum(stratumId: stratumId);

            if (container == null)
            {
                return [];
            }

            return container.Controls
                .OfType<ControlPanelReferenceYearSet>()
                .Where(a => a.PanelReferenceYearSetPair != null)
                .Select(a => a.PanelReferenceYearSetPair)
                .ToList<TFnApiGetDataOptionsForSinglePhaseConfig>();
        }

        /// <summary>
        /// <para lang="cs">PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panels and reference year sets pairs for stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <para lang="cs">PÁRY panelů a referenčních období pro stratum</para>
        /// <para lang="en">Panels and reference year sets pairs for stratum</para>
        /// </returns>
        private List<TFnApiGetDataOptionsForSinglePhaseConfig> GetPanelRefYearSetPairsForStratum(StratumCategory stratum)
        {
            return GetPanelRefYearSetPairsForStratum(stratumId: stratum?.Id);
        }

        /// <summary>
        /// <para lang="cs">Vybrané páry panelů a referenčních období pro stratum</para>
        /// <para lang="en">Selected panels and reference year sets pairs for stratum</para>
        /// </summary>
        /// <param name="stratumId">
        /// <para lang="cs">Stratum identifier</para>
        /// <para lang="en">Stratum identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vybrané páry panelů a referenčních období pro stratum</para>
        /// <para lang="en">Selected panels and reference year sets pairs for stratum</para>
        /// </returns>
        private List<TFnApiGetDataOptionsForSinglePhaseConfig> GetSelectedPanelRefYearSetPairsForStratum(Nullable<int> stratumId)
        {
            System.Windows.Forms.Panel container =
                GetContainerPanelRefYearSetPairsForStratum(stratumId: stratumId);

            if (container == null)
            {
                return [];
            }

            return container.Controls
                .OfType<ControlPanelReferenceYearSet>()
                .Where(a => a.PanelReferenceYearSetPair != null)
                .Where(a => a.IsSelected)
                .Select(a => a.PanelReferenceYearSetPair)
                .ToList<TFnApiGetDataOptionsForSinglePhaseConfig>();
        }

        /// <summary>
        /// <para lang="cs">Vybrané páry panelů a referenčních období pro stratum</para>
        /// <para lang="en">Selected panels and reference year sets pairs for stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vybrané páry panelů a referenčních období pro stratum</para>
        /// <para lang="en">Selected panels and reference year sets pairs for stratum</para>
        /// </returns>
        private List<TFnApiGetDataOptionsForSinglePhaseConfig> GetSelectedPanelRefYearSetPairsForStratum(StratumCategory stratum)
        {
            return GetSelectedPanelRefYearSetPairsForStratum(stratumId: stratum?.Id);
        }


        /// <summary>
        /// <para lang="cs">Zobrazí kontejner SKUPIN panelů a referenčních období pro stratum</para>
        /// <para lang="en">Show panel reference year set groups container for stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        private void ShowPanelRefYearSetGroupsContainerForStratum(StratumCategory stratum)
        {
            tlpPanelRefYearSetGroups.Visible = false;
            pnlPanelRefYearSetGroups.Controls.Clear();

            if (stratum == null)
            {
                return;
            }

            DictPanelRefYearSetGroups.TryGetValue(
                key: stratum.Id,
                out System.Windows.Forms.Panel pnlContainer);

            if (pnlContainer != null)
            {
                pnlPanelRefYearSetGroups.Controls.Add(value: pnlContainer);
            }

            tlpPanelRefYearSetGroups.Visible = true;
            return;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí kontejner PÁRů panelů a referenčních období pro stratum</para>
        /// <para lang="en">Show panel reference year set pairs container for stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        private void ShowPanelRefYearSetPairsContainerForStratum(StratumCategory stratum)
        {
            tlpPanelRefYearSetGroups.Visible = false;
            pnlPanelRefYearSetPairs.Controls.Clear();

            if (stratum == null)
            {
                return;
            }

            DictPanelRefYearSetPairs.TryGetValue(
                key: stratum.Id,
                out System.Windows.Forms.Panel pnlContainer);

            if (pnlContainer != null)
            {
                pnlPanelRefYearSetPairs.Controls.Add(value: pnlContainer);
            }

            tlpPanelRefYearSetGroups.Visible = true;
            return;
        }


        /// <summary>
        /// <para lang="cs">Zaškrtne páry panelů a referenčních období ve vybrané skupině</para>
        /// <para lang="en">Check panels reference year sets pairs in selected group</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <param name="group">
        /// <para lang="cs">Vybraná skupina</para>
        /// <para lang="en">Selected group</para>
        /// </param>
        private void CheckPanelReferenceYearSetPairsForGroup(StratumCategory stratum, PanelRefYearSetGroup group)
        {
            if ((stratum == null) || (group == null))
            {
                return;
            }

            OnEdit = true;
            System.Windows.Forms.Panel container =
                    GetContainerPanelRefYearSetPairsForStratum(stratum: stratum);

            container.Visible = false;

            TFnApiGetPanelRefYearSetCombinationsForGroupsList pairs =
                NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups.Execute(
                    database: Database,
                    panelRefYearSetGroups: [group.Id]);

            foreach (ControlPanelReferenceYearSet control in
                container.Controls
                    .OfType<ControlPanelReferenceYearSet>())
            {
                control.IsSelected =
                   pairs.Items
                        .Where(a =>
                            (a.PanelId != null) &&
                            (a.ReferenceYearSetId != null) &&
                            (control.PanelReferenceYearSetPair?.PanelId != null) &&
                            (control.PanelReferenceYearSetPair?.ReferenceYearSetId != null) &&
                            (a.PanelId == control.PanelReferenceYearSetPair.PanelId) &&
                            (a.ReferenceYearSetId == control.PanelReferenceYearSetPair.ReferenceYearSetId))
                    .Any<TFnApiGetPanelRefYearSetCombinationsForGroups>();
            }

            container.Visible = true;
            OnEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Zaškrtne skupinu panelů a referenčních období pro vybrané páry</para>
        /// <para lang="en">Check panels reference year sets groups for selected pairs</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        /// <param name="pairs">
        /// <para lang="cs">Vybrané páry</para>
        /// <para lang="en">Selected pairs</para>
        /// </param>
        private void CheckPanelReferenceYearSetGroupForPairs(StratumCategory stratum, List<TFnApiGetDataOptionsForSinglePhaseConfig> pairs)
        {
            if ((stratum == null) || (pairs == null))
            {
                return;
            }

            OnEdit = true;
            pnlPanelRefYearSetGroups.Visible = false;

            System.Windows.Forms.Panel container =
                GetContainerPanelRefYearSetGroupsForStratum(stratum: stratum);

            foreach (RadioButton radioButton in container.Controls)
            {
                radioButton.Checked = false;
            }

            if (pairs.Count == 0)
            {
                pnlPanelRefYearSetGroups.Visible = true;
                OnEdit = false;
                return;
            }

            List<Nullable<int>> panels = pairs
                .OrderBy(a => a.PanelId)
                .ThenBy(a => a.ReferenceYearSetId)
                .Select(a => a.PanelId)
                .ToList<Nullable<int>>();

            List<Nullable<int>> refYearSets = pairs
                .OrderBy(a => a.PanelId)
                .ThenBy(a => a.ReferenceYearSetId)
                .Select(a => a.ReferenceYearSetId)
                .ToList<Nullable<int>>();

            TFnApiGetGroupForPanelRefYearSetCombinationsList foundGroups =
                NfiEstaFunctions.FnApiGetGroupForPanelRefYearSetCombinations.Execute(
                    database: Database,
                    panels: panels,
                    refYearSets: refYearSets);

            TFnApiGetGroupForPanelRefYearSetCombinations foundGroup =
                foundGroups.Items.FirstOrDefault<TFnApiGetGroupForPanelRefYearSetCombinations>();

            if (foundGroup != null)
            {
                RadioButton radioButton =
                    container.Controls
                        .OfType<RadioButton>()
                        .Where(a =>
                            (a.Tag != null) &&
                            (a.Tag is PanelRefYearSetGroup group) &&
                            (group.Id == foundGroup.Id))
                        .FirstOrDefault<RadioButton>();

                if (radioButton != null)
                {
                    radioButton.Checked = true;
                }
            }

            pnlPanelRefYearSetGroups.Visible = true;
            OnEdit = false;
        }


        /// <summary>
        /// <para lang="cs">Zobrazí počet vybraných párů panelů a referenčních období pro vybrané stratum</para>
        /// <para lang="en">Show number of selected panel reference year set pairs in selected stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        private void ShowNumberOfPanelRefYearSetPairsForStratum(StratumCategory stratum)
        {
            lblNumberOfPanelRefYearSetPairsForStratum.Visible = stratum != null;
            if (stratum == null) { return; }

            string stratumName =
                (LanguageVersion == LanguageVersion.International)
                    ? stratum?.LabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? stratum?.LabelCs ?? String.Empty
                        : stratum?.LabelEn ?? String.Empty;

            // Počet párů panelů a referenčních období pro vybrané stratum
            int numberOfPanelRefYearSetPairsForStratum =
                GetPanelRefYearSetPairsForStratum(stratum: stratum)
                .Count;

            // Počet vybraných párů panelů a referenčních období pro vybrané stratum
            int numberOfSelectedPanelRefYearSetPairsForStratum =
                GetSelectedPanelRefYearSetPairsForStratum(stratum: stratum)
                .Count;

            if (numberOfSelectedPanelRefYearSetPairsForStratum > numberOfPanelRefYearSetPairsForStratum)
            {
                lblNumberOfPanelRefYearSetPairsForStratum.Text =
                    String.Concat(strSelectedPanelRefYearSetGroupForStratumCountTooMany
                        .Replace(oldValue: "$1", newValue: numberOfSelectedPanelRefYearSetPairsForStratum.ToString())
                        .Replace(oldValue: "$2", newValue: numberOfPanelRefYearSetPairsForStratum.ToString())
                        .Replace(oldValue: "$3", newValue: stratumName.ToString()));
                return;
            }

            lblNumberOfPanelRefYearSetPairsForStratum.Text =
                numberOfSelectedPanelRefYearSetPairsForStratum switch
                {
                    0 =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumCountNone
                            .Replace(oldValue: "$1", newValue: numberOfSelectedPanelRefYearSetPairsForStratum.ToString())),

                    1 =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumCountOne
                            .Replace(oldValue: "$1", newValue: numberOfSelectedPanelRefYearSetPairsForStratum.ToString())),

                    2 or 3 or 4 =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumCountSome
                            .Replace(oldValue: "$1", newValue: numberOfSelectedPanelRefYearSetPairsForStratum.ToString())),

                    _ =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumCountMany
                            .Replace(oldValue: "$1", newValue: numberOfSelectedPanelRefYearSetPairsForStratum.ToString()))
                };

            lblNumberOfPanelRefYearSetPairsForStratum.Text =
                numberOfPanelRefYearSetPairsForStratum switch
                {
                    0 =>
                        String.Concat(
                            lblNumberOfPanelRefYearSetPairsForStratum.Text,
                            Space,
                            strPanelRefYearSetGroupsForStratumCountNone
                                .Replace(oldValue: "$1", newValue: numberOfPanelRefYearSetPairsForStratum.ToString())
                                .Replace(oldValue: "$2", newValue: stratumName)),

                    1 =>
                        String.Concat(
                        lblNumberOfPanelRefYearSetPairsForStratum.Text,
                        Space,
                        strPanelRefYearSetGroupsForStratumCountOne
                            .Replace(oldValue: "$1", newValue: numberOfPanelRefYearSetPairsForStratum.ToString())
                            .Replace(oldValue: "$2", newValue: stratumName)),

                    2 or 3 or 4 =>
                        String.Concat(
                            lblNumberOfPanelRefYearSetPairsForStratum.Text,
                            Space,
                            strPanelRefYearSetGroupsForStratumCountSome
                                .Replace(oldValue: "$1", newValue: numberOfPanelRefYearSetPairsForStratum.ToString())
                                .Replace(oldValue: "$2", newValue: stratumName)),

                    _ =>
                        String.Concat(
                            lblNumberOfPanelRefYearSetPairsForStratum.Text,
                            Space,
                            strPanelRefYearSetGroupsForStratumCountMany
                                .Replace(oldValue: "$1", newValue: numberOfPanelRefYearSetPairsForStratum.ToString())
                                .Replace(oldValue: "$2", newValue: stratumName))
                };
        }

        /// <summary>
        /// <para lang="cs">Zobrazí počet vybraných párů panelů a referenčních období pro vybrané stratum</para>
        /// <para lang="en">Show number of selected panel reference year set pairs in selected stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        private void ShowSampleSizeForStratum(StratumCategory stratum)
        {
            lblNumberOfPanelRefYearSetPairsForStratum.Visible = stratum != null;
            if (stratum == null) { return; }

            string stratumName =
                (LanguageVersion == LanguageVersion.International)
                    ? stratum?.LabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? stratum?.LabelCs ?? String.Empty
                        : stratum?.LabelEn ?? String.Empty;

            // Počet inventarizačních ploch pro vybrané stratum
            int sampleSizeForStratum =
                GetPanelRefYearSetPairsForStratum(stratum: stratum)
                .Sum(a => a.SSize ?? 0);

            // Počet vybraných inventarizačních ploch pro vybrané stratum
            int selectedSampleSizeForStratum =
                GetSelectedPanelRefYearSetPairsForStratum(stratum: stratum)
                .Sum(a => a.SSize ?? 0);

            if (selectedSampleSizeForStratum > sampleSizeForStratum)
            {
                lblSampleSizeForStratum.Text =
                    String.Concat(strSelectedPanelRefYearSetGroupForStratumPlotsCountTooMany
                        .Replace(oldValue: "$1", newValue: selectedSampleSizeForStratum.ToString())
                        .Replace(oldValue: "$2", newValue: sampleSizeForStratum.ToString())
                        .Replace(oldValue: "$3", newValue: stratumName.ToString()));
                return;
            }

            lblSampleSizeForStratum.Text =
                selectedSampleSizeForStratum switch
                {
                    0 =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumPlotsCountNone
                            .Replace(oldValue: "$1", newValue: selectedSampleSizeForStratum.ToString())),

                    1 =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumPlotsCountOne
                            .Replace(oldValue: "$1", newValue: selectedSampleSizeForStratum.ToString())),

                    2 or 3 or 4 =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumPlotsCountSome
                            .Replace(oldValue: "$1", newValue: selectedSampleSizeForStratum.ToString())),

                    _ =>
                        String.Concat(strSelectedPanelRefYearSetGroupForStratumPlotsCountMany
                            .Replace(oldValue: "$1", newValue: selectedSampleSizeForStratum.ToString()))
                };

            lblSampleSizeForStratum.Text =
                sampleSizeForStratum switch
                {
                    0 =>
                        String.Concat(
                            lblSampleSizeForStratum.Text,
                            Space,
                            strPanelRefYearSetGroupsForStratumPlotsCountNone
                                .Replace(oldValue: "$1", newValue: sampleSizeForStratum.ToString())
                                .Replace(oldValue: "$2", newValue: stratumName)),

                    1 =>
                        String.Concat(
                        lblSampleSizeForStratum.Text,
                        Space,
                        strPanelRefYearSetGroupsForStratumPlotsCountOne
                            .Replace(oldValue: "$1", newValue: sampleSizeForStratum.ToString())
                            .Replace(oldValue: "$2", newValue: stratumName)),

                    2 or 3 or 4 =>
                        String.Concat(
                            lblSampleSizeForStratum.Text,
                            Space,
                            strPanelRefYearSetGroupsForStratumPlotsCountSome
                                .Replace(oldValue: "$1", newValue: sampleSizeForStratum.ToString())
                                .Replace(oldValue: "$2", newValue: stratumName)),

                    _ =>
                        String.Concat(
                            lblSampleSizeForStratum.Text,
                            Space,
                            strPanelRefYearSetGroupsForStratumPlotsCountMany
                                .Replace(oldValue: "$1", newValue: sampleSizeForStratum.ToString())
                                .Replace(oldValue: "$2", newValue: stratumName))
                };
        }


        /// <summary>
        /// <para lang="cs">Potvrzení skupiny nebo vytvoření nové skupiny panelů a referenčních období pro stratum</para>
        /// <para lang="en">Confirm group or create new group of panels and reference year sets for stratum</para>
        /// </summary>
        /// <param name="stratum">
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </param>
        private void Confirm(StratumCategory stratum)
        {
            if (stratum == null) { return; }

            // Na formuláři vybrané páry panelů a referenčních období
            List<TFnApiGetDataOptionsForSinglePhaseConfig> selectedPanelRefYearSetPairsForStratum =
                 GetSelectedPanelRefYearSetPairsForStratum(stratum: stratum);

            // Na formuláři vybraná skupina panelů a referenčních období
            PanelRefYearSetGroup selectedPanelRefYearSetGroupForStratum =
                GetSelectedPanelRefYearSetGroupForStratum(stratum: stratum);

            // Nejsou vybrané žádné páry -> zobrazit chybu, nastavit stratum za nevyřešené, a hotovo
            if ((selectedPanelRefYearSetPairsForStratum == null) ||
                (selectedPanelRefYearSetPairsForStratum.Count == 0))
            {
                MessageBox.Show(
                    caption: String.Empty,
                    text: (LanguageVersion == LanguageVersion.International)
                        ? msgNoPanelRefYearSetPairs.Replace(oldValue: "$1", newValue: stratum.LabelEn)
                        : (LanguageVersion == LanguageVersion.National)
                            ? msgNoPanelRefYearSetPairs.Replace(oldValue: "$1", newValue: stratum.LabelCs)
                            : msgNoPanelRefYearSetPairs.Replace(oldValue: "$1", newValue: stratum.LabelEn),
                    icon: MessageBoxIcon.Information,
                    buttons: MessageBoxButtons.OK);
                stratum.Reset(group: null, pairs: null);
                EnableNext();
                return;
            }

            // Jsou vybrané páry a je k nim vybraná skupina -> nastavit stratum za vyřešené, a hotovo
            if (selectedPanelRefYearSetGroupForStratum != null)
            {
                stratum.Reset(
                    group: selectedPanelRefYearSetGroupForStratum,
                    pairs: selectedPanelRefYearSetPairsForStratum);
                EnableNext();
                return;
            }

            // Jsou vybrané páry, ale nevytvářejí žádnou skupinu -> skupina se musí vytvořit
            FormPanelRefYearSetGroupNew frmPanelRefYearSetGroupNew =
                new(controlOwner: this);

            // Zobrazí formulář pro vytvoření nové skupiny
            if (frmPanelRefYearSetGroupNew.ShowDialog() == DialogResult.OK)
            {
                List<Nullable<int>> panels = selectedPanelRefYearSetPairsForStratum
                    .OrderBy(a => a.PanelId)
                    .ThenBy(a => a.ReferenceYearSetId)
                    .Select(a => a.PanelId)
                    .ToList<Nullable<int>>();

                List<Nullable<int>> refYearSets = selectedPanelRefYearSetPairsForStratum
                    .OrderBy(a => a.PanelId)
                    .ThenBy(a => a.ReferenceYearSetId)
                    .Select(a => a.ReferenceYearSetId)
                    .ToList<Nullable<int>>();

                // Zápis nové skupiny do databáze
                Database.Postgres.ExceptionFlag = false;
                Nullable<int> panelReferenceYearSetId =
                    NfiEstaFunctions.FnApiSavePanelRefYearSetGroup.Execute(
                        database: Database,
                        panels: panels,
                        refYearSets: refYearSets,
                        labelCs: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.LabelCs,
                        descriptionCs: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.DescriptionCs,
                        labelEn: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.LabelEn,
                        descriptionEn: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.DescriptionEn);

                if (Database.Postgres.ExceptionFlag)
                {
                    // Zápis do databáze se nezdařil, skupina není vytvořená
                    // -> nastavit stratum za nevyřešené, a hotovo
                    stratum.Reset(group: null, pairs: null);
                    EnableNext();
                    return;
                }

                // Vytvořená skupina se musí zobrazit na formuláři
                // Inicializuje znovu kontejner skupin pro vybrané stratum, ve kterém skupina byla vytvořena
                if (DictPanelRefYearSetGroups.ContainsKey(key: stratum.Id))
                {
                    DictPanelRefYearSetGroups[stratum.Id] =
                        CreateNewPanelRefYearSetGroupContainerForStratum(stratum: stratum);
                }
                ShowPanelRefYearSetGroupsContainerForStratum(stratum: stratum);

                // Zaškrne nově vytvořenou skupinu podle zadané kombinace panelů
                CheckPanelReferenceYearSetGroupForPairs(
                    stratum: stratum,
                    pairs: selectedPanelRefYearSetPairsForStratum);

                // Vybere zaškrnutou nově vytvořenou skupinu
                selectedPanelRefYearSetGroupForStratum =
                    GetSelectedPanelRefYearSetGroupForStratum(stratum: stratum);

                // Pokud nově vytvořená skupina existuje nastaví ji na vyřešnou a hotovo
                if (selectedPanelRefYearSetGroupForStratum != null)
                {
                    stratum.Reset(
                        group: selectedPanelRefYearSetGroupForStratum,
                        pairs: selectedPanelRefYearSetPairsForStratum);
                    EnableNext();
                    return;
                }
            }

            // Pro stratum nebyla přiřazena skupina s páry panelů a referenčních období
            stratum.Reset(group: null, pairs: null);
            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            btnOK.Enabled = AllStrataResolved;
        }

        /// <summary>
        /// <para lang="cs">Vytvoření seznamu agregovaných skupin výpočetních buněk</para>
        /// <para lang="en">Creating a list of aggregated groups of estimation cells</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Seznam agregovaných skupin výpočetních buněk</para>
        /// <para lang="en">List of aggregated groups of estimation cells</para>
        /// </returns>
        private List<AggregatedEstimationCellGroup> GetAggregatedEstimationCellGroups()
        {
            if (!AllStrataResolved)
            {
                MessageBox.Show(
                    caption: String.Empty,
                    text: msgNotAllStrataResolved,
                    icon: MessageBoxIcon.Information,
                    buttons: MessageBoxButtons.OK);
                return [];
            }

            // Node 20
            // Skupiny výpočetních buněk (distinct country + strataset + stratum + list of estimation cells)
            List<EstimationCellGroup> estimationCellGroups =
                PanelReferenceYearSetPairs.GetEstimationCellGroups();

            // Doplnění zvolené skupiny panelů a referenčních období pro každou skupinu výpočetních buněk podle strata
            foreach (EstimationCellGroup estimationCellGroup in estimationCellGroups)
            {
                estimationCellGroup.Country =
                     Countries
                     .Where(a => a.Id == estimationCellGroup.CountryId)
                     .FirstOrDefault<CountryCategory>();

                estimationCellGroup.StrataSet =
                    StrataSets
                    .Where(a => a.Id == estimationCellGroup.StrataSetId)
                    .FirstOrDefault<StrataSetCategory>();

                estimationCellGroup.Stratum =
                    Strata
                    .Where(a => a.Id == estimationCellGroup.StratumId)
                    .FirstOrDefault<StratumCategory>();

                estimationCellGroup.PanelRefYearSetGroup =
                    GetSelectedPanelRefYearSetGroupForStratum(stratumId: estimationCellGroup.StratumId);
            }

            // Agregace skupin výpočetních buněk
            // Pro každou skupinu výpočetních buněk je vytvořen seznam skupin panelů a referenčních období
            // z různých strat, strata setů a zemí
            List<AggregatedEstimationCellGroup> estimationCellGroupsAggregate =
                estimationCellGroups
                    .GroupBy(a => a.EstimationCellsIdsSortedText)
                    .Select(a => new AggregatedEstimationCellGroup
                    {
                        EstimationCellsIdsSortedText = a.Key,
                        CountryIds = a.Select(b => b.CountryId).ToList<int>(),
                        Countries = a.Select(b => b.Country).ToList<object>(),
                        StrataSetIds = a.Select(b => b.StrataSetId).ToList<int>(),
                        StrataSets = a.Select(b => b.StrataSet).ToList<object>(),
                        StrataIds = a.Select(b => b.StratumId).ToList<int>(),
                        Strata = a.Select(b => b.Stratum).ToList<object>(),
                        PartialPanelReferenceYearSetGroups =
                            a.Select(b => new PartialPanelRefYearSetGroup()
                            {
                                PanelRefYearSetGroup = b.PanelRefYearSetGroup,
                                Country = b.Country,
                                StrataSet = b.StrataSet,
                                Stratum = b.Stratum,
                            }).ToList<PartialPanelRefYearSetGroup>(),
                        PanelsIds = [],
                        ReferenceYearSetsIds = [],
                        GlobalPanelReferenceYearSetGroup = null
                    })
                    .ToList<AggregatedEstimationCellGroup>();

            foreach (AggregatedEstimationCellGroup estimationCellGroupAggregate in estimationCellGroupsAggregate)
            {
                // Node 21
                // Načtení unikátní kombinace panelů a referenčních období,
                // pro každou agregovanou skupinu výpočetních buněk, z databáze
                TFnApiGetPanelRefYearSetCombinationsForGroupsList
                    list = NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups.Execute(
                        database: Database,
                        panelRefYearSetGroups: estimationCellGroupAggregate
                            .PartialPanelReferenceYearSetGroups
                            .Select(a => (Nullable<int>)a?.PanelRefYearSetGroup?.Id)
                            .ToList<Nullable<int>>());

                // Node 22
                // Uložení unikátní kombinace panelů a referenčních období,
                // pro každou agregovanou skupinu výpočetních buněk
                estimationCellGroupAggregate.PanelsIds = list.PanelsIds;
                estimationCellGroupAggregate.ReferenceYearSetsIds = list.ReferenceYearSetsIds;

                // Node 23
                // Získání skupiny panelů a referenčních období podle unikátní kombinace panelů a referenčních období
                // pro každou agregovanou skupinu výpočetních buněk, z databáze
                // Pokud skupina v databázi neexistuje, pak je do promměné zapsaná hodnota null
                estimationCellGroupAggregate.GlobalPanelReferenceYearSetGroup =
                     NfiEstaFunctions.FnApiGetGroupForPanelRefYearSetCombinations.Execute(
                        database: Database,
                        panels: list.PanelsIds,
                        refYearSets: list.ReferenceYearSetsIds)
                    .Items
                    .FirstOrDefault<TFnApiGetGroupForPanelRefYearSetCombinations>();

                // Node 23
                // Pokud skupina v databázi neexistuje, hodnota proměnné je null, bude se skupina vytvářet
                if (estimationCellGroupAggregate.GlobalPanelReferenceYearSetGroup == null)
                {
                    // Node 24
                    // Metoda Aggregate pospojuje popisky
                    // Creating the label and label_en in the same way as descriptions
                    string labelCs =
                        estimationCellGroupAggregate.PartialPanelReferenceYearSetGroups
                            .Select(a => a?.PanelRefYearSetGroup?.LabelCs ?? String.Empty)
                            .Aggregate((a, b) => $"{a}+{b}");

                    if (labelCs.Length > PanelRefYearSetGroup.LabelCsMaxLength)
                    {
                        // Switch to the country code and estimation period pattern
                        // (+separator without any space between countries,
                        // then a space and estimation period label in round brackets)
                        // only in case the length of 200 characters is exceeded.
                        labelCs = String.Concat(
                            estimationCellGroupAggregate.PartialPanelReferenceYearSetGroups
                                .Select(a => (CountryCategory)a?.Country)
                                .Select(a => a?.LabelCs ?? String.Empty)
                                .Distinct()
                                .Aggregate((a, b) => $"{a}+{b}"),
                            Space,
                            $"({EstimationPeriod?.LabelCs ?? String.Empty})");
                    }

                    string labelEn =
                        estimationCellGroupAggregate.PartialPanelReferenceYearSetGroups
                            .Select(a => a?.PanelRefYearSetGroup?.LabelEn ?? String.Empty)
                            .Aggregate((a, b) => $"{a}+{b}");

                    if (labelEn.Length > PanelRefYearSetGroup.LabelEnMaxLength)
                    {
                        labelEn = String.Concat(
                            estimationCellGroupAggregate.PartialPanelReferenceYearSetGroups
                                .Select(a => (CountryCategory)a?.Country)
                                .Select(a => a?.LabelEn ?? String.Empty)
                                .Distinct()
                                .Aggregate((a, b) => $"{a}+{b}"),
                            Space,
                            $"({EstimationPeriod?.LabelEn ?? String.Empty})");
                    }

                    string descriptionCs =
                        estimationCellGroupAggregate.PartialPanelReferenceYearSetGroups
                            .Select(a => a?.PanelRefYearSetGroup?.DescriptionCs ?? String.Empty)
                            .Aggregate((a, b) => $"{a}+{b}");

                    string descriptionEn =
                         estimationCellGroupAggregate.PartialPanelReferenceYearSetGroups
                            .Select(a => a?.PanelRefYearSetGroup?.DescriptionEn ?? String.Empty)
                            .Aggregate((a, b) => $"{a}+{b}");

                    if (VerifyGlobalPanelReferenceYearSetGroupLabels(
                        labelCs: labelCs,
                        labelEn: labelEn,
                        descriptionCs: descriptionCs,
                        descriptionEn: descriptionEn))
                    {
                        // Node 25
                        // Do databáze se zapíše nová agregovaná skupina
                        Nullable<int> globalPanelReferenceYearSetGroupId =
                            NfiEstaFunctions.FnApiSavePanelRefYearSetGroup.Execute(
                                database: Database,
                                panels: list.PanelsIds,
                                refYearSets: list.ReferenceYearSetsIds,
                                labelCs: labelCs,
                                descriptionCs: descriptionCs,
                                labelEn: labelEn,
                                descriptionEn: descriptionEn);
                    }
                    else
                    {
                        FormPanelRefYearSetGroupNew frmPanelRefYearSetGroupNew =
                            new(controlOwner: this);

                        frmPanelRefYearSetGroupNew.txtLabelCsValue.Text = labelCs;
                        frmPanelRefYearSetGroupNew.txtLabelEnValue.Text = labelEn;
                        frmPanelRefYearSetGroupNew.txtDescriptionCsValue.Text = descriptionCs;
                        frmPanelRefYearSetGroupNew.txtDescriptionEnValue.Text = descriptionEn;
                        frmPanelRefYearSetGroupNew.txtDescriptionCsValue.Enabled = false;
                        frmPanelRefYearSetGroupNew.txtDescriptionEnValue.Enabled = false;

                        if (frmPanelRefYearSetGroupNew.ShowDialog() == DialogResult.OK)
                        {
                            Nullable<int> globalPanelReferenceYearSetGroupId =
                            NfiEstaFunctions.FnApiSavePanelRefYearSetGroup.Execute(
                                database: Database,
                                panels: list.PanelsIds,
                                refYearSets: list.ReferenceYearSetsIds,
                                labelCs: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.LabelCs,
                                descriptionCs: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.DescriptionCs,
                                labelEn: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.LabelEn,
                                descriptionEn: frmPanelRefYearSetGroupNew.PanelReferenceYearSetGroup.DescriptionEn);
                        }
                    }

                    // Node 26
                    // Získání skupiny panelů a referenčních období podle unikátní kombinace panelů a referenčních období
                    // pro každou agregovanou skupinu výpočetních buněk, z databáze
                    // Skupina byla vytvořena v Node 25,
                    // pokud byla uložena do databáze v pořadku, nemůže se zde už vrátit null
                    estimationCellGroupAggregate.GlobalPanelReferenceYearSetGroup =
                        NfiEstaFunctions.FnApiGetGroupForPanelRefYearSetCombinations.Execute(
                           database: Database,
                           panels: list.PanelsIds,
                           refYearSets: list.ReferenceYearSetsIds)
                       .Items
                       .FirstOrDefault<TFnApiGetGroupForPanelRefYearSetCombinations>();
                }
            }

            // Vrácení agregované skupiny výpočetních buněk s doplněnou skupinu panelů a refernčních období
            return estimationCellGroupsAggregate;
        }

        /// <summary>
        /// <para lang="cs">
        /// Ověří validitu popisků celkové skupiny panelů a referenčních období
        /// </para>
        /// <para lang="en">
        /// Verifies validity of the labels for global panel reference year set group
        /// </para>
        /// </summary>
        private bool VerifyGlobalPanelReferenceYearSetGroupLabels(
          string labelCs,
          string labelEn,
          string descriptionCs,
          string descriptionEn,
          int labelCsMaxLength = PanelRefYearSetGroup.LabelCsMaxLength,
          int labelEnMaxLength = PanelRefYearSetGroup.LabelEnMaxLength)
        {
            PanelRefYearSetGroup group = new(
                    data: PanelRefYearSetGroupList.EmptyDataTable().NewRow())
            {
                LabelCs = labelCs,
                LabelEn = labelEn,
                DescriptionCs = descriptionCs,
                DescriptionEn = descriptionEn,
            };

            if (String.IsNullOrEmpty(value: group.LabelCs))
            {
                // LabelCs - prázdné
                return false;
            }

            if (String.IsNullOrEmpty(value: group.LabelEn))
            {
                // LabelEn - prázdné
                return false;
            }

            if (String.IsNullOrEmpty(value: group.DescriptionCs))
            {
                // DescriptionCs - prázdné
                return false;
            }

            if (String.IsNullOrEmpty(value: group.DescriptionEn))
            {
                // DescriptionEn - prázdné
                return false;
            }

            if (group.LabelCs.Length > labelCsMaxLength)
            {
                return false;
            }

            if (group.LabelEn.Length > labelEnMaxLength)
            {
                return false;
            }

            Database.Postgres.ExceptionFlag = false;
            TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList uniqueChecks =
                NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist.Execute(
                database: Database,
                labelCs: group.LabelCs,
                labelEn: group.LabelEn,
                descriptionCs: group.DescriptionCs,
                descriptionEn: group.DescriptionEn);

            if (Database.Postgres.ExceptionFlag)
            {
                return false;
            }

            if ((uniqueChecks == null) || (uniqueChecks.Items.Count == 0))
            {
                return false;
            }

            if (uniqueChecks.Items.Count != 1)
            {
                return false;
            }

            TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist uniqueCheck =
                uniqueChecks.Items[0];

            if (uniqueCheck.LabelCsFound ?? false)
            {
                // LabelCs - duplicitní
                return false;
            }

            if (uniqueCheck.LabelEnFound ?? false)
            {
                // LabelEn - duplicitní
                return false;
            }

            if (uniqueCheck.DescriptionCsFound ?? false)
            {
                // DescriptionCs - duplicitní
                return false;
            }

            if (uniqueCheck.DescriptionEnFound ?? false)
            {
                // DescriptionEn - duplicitní
                return false;
            }

            return true;
        }

        /// <summary>
        /// <para lang="cs">
        /// Ověří seznam agregovaných skupin výpočetních buněk,
        /// každá skupina výpočetních buněk v seznamu musí mít přiřazenou skupinu panelů a referenčních období
        /// </para>
        /// <para lang="en">
        /// Verifies the list of aggregated estimation cell groups,
        /// each estimation cell group in the list must have an associated panel group and reference periods
        /// </para>
        /// </summary>
        public static bool VerifyAggregatedEstimationCellGroups(List<AggregatedEstimationCellGroup> groups)
        {
            if ((groups == null) ||
                (groups.Count == 0))
            {
                return false;
            }

            foreach (AggregatedEstimationCellGroup group in groups)
            {
                if (group.GlobalPanelReferenceYearSetGroup == null)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion Methods


        #region Private Classes

        /// <summary>
        /// <para lang="cs">Kategorie</para>
        /// <para lang="en">Category</para>
        /// </summary>
        private abstract class Category
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Identifikátor kategorie</para>
            /// <para lang="en">Category identifier</para>
            /// </summary>
            private int id;

            /// <summary>
            /// <para lang="cs">Popisek kategorie v národním jazyku</para>
            /// <para lang="en">Category label in national language</para>
            /// </summary>
            private string labelCs;

            /// <summary>
            /// <para lang="cs">Popis kategorie v národním jazyku</para>
            /// <para lang="en">Category description in national language</para>
            /// </summary>
            private string descriptionCs;

            /// <summary>
            /// <para lang="cs">Popisek kategorie anglický</para>
            /// <para lang="en">Category label in English</para>
            /// </summary>
            private string labelEn;

            /// <summary>
            /// <para lang="cs">Popis kategorie anglický</para>
            /// <para lang="en">Category description in English</para>
            /// </summary>
            private string descriptionEn;

            /// <summary>
            /// <para lang="cs">Uzel v TreeView</para>
            /// <para lang="en">TreeView node</para>
            /// </summary>
            private TreeNode node;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// <para lang="cs">Identifikátor kategorie</para>
            /// <para lang="en">Category identifier</para>
            /// </summary>
            public virtual int Id
            {
                get
                {
                    return id;
                }
                set
                {
                    id = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Popisek kategorie v národním jazyku</para>
            /// <para lang="en">Category label in national language</para>
            /// </summary>
            public virtual string LabelCs
            {
                get
                {
                    return labelCs ?? String.Empty;
                }
                set
                {
                    labelCs = value ?? String.Empty;
                }
            }

            /// <summary>
            /// <para lang="cs">Popis kategorie v národním jazyku</para>
            /// <para lang="en">Category description in national language</para>
            /// </summary>
            public virtual string DescriptionCs
            {
                get
                {
                    return descriptionCs ?? String.Empty;
                }
                set
                {
                    descriptionCs = value ?? String.Empty;
                }
            }

            /// <summary>
            /// <para lang="cs">Popisek kategorie anglický</para>
            /// <para lang="en">Category label in English</para>
            /// </summary>
            public virtual string LabelEn
            {
                get
                {
                    return labelEn ?? String.Empty;
                }
                set
                {
                    labelEn = value ?? String.Empty;
                }
            }

            /// <summary>
            /// <para lang="cs">Popis kategorie anglický</para>
            /// <para lang="en">Category description in English</para>
            /// </summary>
            public virtual string DescriptionEn
            {
                get
                {
                    return descriptionEn ?? String.Empty;
                }
                set
                {
                    descriptionEn = value ?? String.Empty;
                }
            }

            /// <summary>
            /// <para lang="cs">Uzel v TreeView</para>
            /// <para lang="en">TreeView node</para>
            /// </summary>
            public virtual TreeNode Node
            {
                get
                {
                    return node;
                }
                set
                {
                    node = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Jsou pro danou kategorii nastaveny skupiny panelů a referenčních období pro každé stratum?</para>
            /// <para lang="en">Are groups of panels and reference year sets set for each stratum in this category?</para>
            /// </summary>
            public virtual bool IsResolved
            {
                get
                {
                    return false;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true|false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not Category Type, return False
                if (obj is not Category)
                {
                    return false;
                }

                return ((Category)obj).Id == Id;
            }

            /// <summary>
            /// Returns a hash code for the current object
            /// </summary>
            /// <returns>Hash code for the current object</returns>
            public override int GetHashCode()
            {
                return
                    Id.GetHashCode();
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>String that represents the current object</returns>
            public override string ToString()
            {
                return String.Concat(
                    $"{nameof(Category)}: [",
                    $"{nameof(Id)}: {Functions.PrepIntArg(Id)}; ",
                    $"{nameof(IsResolved)}: {Functions.PrepBoolArg(IsResolved)}; ",
                    $"{nameof(LabelCs)}: {Functions.PrepStringArg(LabelCs)}; ",
                    $"{nameof(LabelEn)}: {Functions.PrepStringArg(LabelEn)}; ",
                    $"{nameof(DescriptionCs)}: {Functions.PrepStringArg(DescriptionCs)}; ",
                    $"{nameof(DescriptionEn)}: {Functions.PrepStringArg(DescriptionEn)}]");
            }

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">Země</para>
        /// <para lang="en">Country</para>
        /// </summary>
        private class CountryCategory : Category
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Jsou pro danou zemi nastaveny skupiny panelů a referenčních období pro každou skupinu strat?</para>
            /// <para lang="en">Are groups of panels and reference year sets set for each strata set in this country?</para>
            /// </summary>
            public override bool IsResolved
            {
                get
                {
                    if (Node == null) { return false; }

                    return Node.Nodes.OfType<TreeNode>()
                       .Where(a => a.Tag != null)
                       .Where(a => a.Tag is StrataSetCategory)
                       .Select(a => (StrataSetCategory)a.Tag)
                       .All(a => a.IsResolved);
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Barevně označí uzly v TreeView pro které je zvolena skupina panelů a referenčních období</para>
            /// <para lang="en">Color-codes the nodes in the TreeView for which a group of panels and reference year sets is selected</para>
            /// </summary>
            public void Reset()
            {
                if (Node != null)
                {
                    Node.ImageIndex = IsResolved ? ImageGreenBall : ImageGrayBall;
                    Node.SelectedImageIndex = Node.ImageIndex;
                }
            }


            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true|false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not CountryCategory Type, return False
                if (obj is not CountryCategory)
                {
                    return false;
                }

                return ((CountryCategory)obj).Id == Id;
            }

            /// <summary>
            /// Returns a hash code for the current object
            /// </summary>
            /// <returns>Hash code for the current object</returns>
            public override int GetHashCode()
            {
                return
                    Id.GetHashCode();
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>String that represents the current object</returns>
            public override string ToString()
            {
                return String.Concat(
                    $"{nameof(CountryCategory)}: [",
                    $"{nameof(Id)}: {Functions.PrepIntArg(Id)}; ",
                    $"{nameof(IsResolved)}: {Functions.PrepBoolArg(IsResolved)}; ",
                    $"{nameof(LabelCs)}: {Functions.PrepStringArg(LabelCs)}; ",
                    $"{nameof(LabelEn)}: {Functions.PrepStringArg(LabelEn)}; ",
                    $"{nameof(DescriptionCs)}: {Functions.PrepStringArg(DescriptionCs)}; ",
                    $"{nameof(DescriptionEn)}: {Functions.PrepStringArg(DescriptionEn)}]");
            }

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">Strata set</para>
        /// <para lang="en">Strata set</para>
        /// </summary>
        private class StrataSetCategory : Category
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Země (vyšší kategorie)</para>
            /// <para lang="en">Country (parent category)</para>
            /// </summary>
            public CountryCategory Country
            {
                get
                {
                    return (CountryCategory)Node?.Parent?.Tag;
                }
            }

            /// <summary>
            /// <para lang="cs">Jsou pro danou skupinu strat nastaveny skupiny panelů a referenčních období pro každé stratum?</para>
            /// <para lang="en">Are groups of panels and reference year sets set for each stratum in this strata set?</para>
            /// </summary>
            public override bool IsResolved
            {
                get
                {
                    if (Node == null) { return false; }

                    return Node.Nodes.OfType<TreeNode>()
                       .Where(a => a.Tag != null)
                       .Where(a => a.Tag is StratumCategory)
                       .Select(a => (StratumCategory)a.Tag)
                       .All(a => a.IsResolved);
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Barevně označí uzly v TreeView pro které je zvolena skupina panelů a referenčních období</para>
            /// <para lang="en">Color-codes the nodes in the TreeView for which a group of panels and reference year sets is selected</para>
            /// </summary>
            public void Reset()
            {
                if (Node != null)
                {
                    Node.ImageIndex = IsResolved ? ImageGreenBall : ImageGrayBall;
                    Node.SelectedImageIndex = Node.ImageIndex;
                }
                Country?.Reset();
            }


            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true|false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not StrataSetCategory Type, return False
                if (obj is not StrataSetCategory)
                {
                    return false;
                }

                return ((StrataSetCategory)obj).Id == Id;
            }

            /// <summary>
            /// Returns a hash code for the current object
            /// </summary>
            /// <returns>Hash code for the current object</returns>
            public override int GetHashCode()
            {
                return
                    Id.GetHashCode();
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>String that represents the current object</returns>
            public override string ToString()
            {
                return String.Concat(
                    $"{nameof(StrataSetCategory)}: [",
                    $"{nameof(Id)}: {Functions.PrepIntArg(Id)}; ",
                    $"{nameof(IsResolved)}: {Functions.PrepBoolArg(IsResolved)}; ",
                    $"{nameof(LabelCs)}: {Functions.PrepStringArg(LabelCs)}; ",
                    $"{nameof(LabelEn)}: {Functions.PrepStringArg(LabelEn)}; ",
                    $"{nameof(DescriptionCs)}: {Functions.PrepStringArg(DescriptionCs)}; ",
                    $"{nameof(DescriptionEn)}: {Functions.PrepStringArg(DescriptionEn)}]");
            }

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">Stratum</para>
        /// <para lang="en">Stratum</para>
        /// </summary>
        private class StratumCategory : Category
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Skupina panelů a referenčních období</para>
            /// <para lang="en">Panel reference year set group</para>
            /// </summary>
            private PanelRefYearSetGroup panelRefYearSetGroup;

            /// <summary>
            /// <para lang="cs">Páry panelů a referenčních období</para>
            /// <para lang="en">Panel reference year set pairs</para>
            /// </summary>
            private List<TFnApiGetDataOptionsForSinglePhaseConfig> panelRefYearSetPairs;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// <para lang="cs">Skupina strat (vyšší kategorie)</para>
            /// <para lang="en">Strata set (parent category)</para>
            /// </summary>
            public StrataSetCategory StrataSet
            {
                get
                {
                    return (StrataSetCategory)Node?.Parent?.Tag;
                }
            }

            /// <summary>
            /// <para lang="cs">Skupina panelů a referenčních období</para>
            /// <para lang="en">Panel reference year set group</para>
            /// </summary>
            public PanelRefYearSetGroup PanelRefYearSetGroup
            {
                get
                {
                    return panelRefYearSetGroup;
                }
                private set
                {
                    panelRefYearSetGroup = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Páry panelů a referenčních období</para>
            /// <para lang="en">Panel reference year set pairs</para>
            /// </summary>
            public List<TFnApiGetDataOptionsForSinglePhaseConfig> PanelRefYearSetPairs
            {
                get
                {
                    return panelRefYearSetPairs ?? [];
                }
                set
                {
                    panelRefYearSetPairs = value ?? [];
                }
            }

            /// <summary>
            /// <para lang="cs">Je pro stratum zvolena skupina panelů a referenčních období, která není prázdná</para>
            /// <para lang="en">A non-empty group of panels and reference periods is selected for the stratum</para>
            /// </summary>
            public override bool IsResolved
            {
                get
                {
                    return
                        (PanelRefYearSetGroup != null) &&
                        ((PanelRefYearSetPairs != null) || (PanelRefYearSetPairs.Count > 0));
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Nastaví skupinu a páry panelů a referenčních období pro stratum</para>
            /// <para lang="en">Sets group and pairs of panels and reference year sets for stratum</para>
            /// </summary>
            /// <param name="group">
            /// <para lang="cs">Skupina panelů a referenčních období</para>
            /// <para lang="en">Panel reference year set group</para>
            /// </param>
            /// <param name="pairs">
            /// <para lang="cs">Páry panelů a referenčních období</para>
            /// <para lang="en">Panel reference year set pairs</para>
            /// </param>
            public void Reset(
                PanelRefYearSetGroup group,
                List<TFnApiGetDataOptionsForSinglePhaseConfig> pairs)
            {
                PanelRefYearSetGroup = group;
                PanelRefYearSetPairs = pairs;

                Reset();
            }

            /// <summary>
            /// <para lang="cs">Barevně označí uzly v TreeView pro které je zvolena skupina panelů a referenčních období</para>
            /// <para lang="en">Color-codes the nodes in the TreeView for which a group of panels and reference year sets is selected</para>
            /// </summary>
            protected void Reset()
            {
                if (Node != null)
                {
                    Node.ImageIndex = IsResolved ? ImageGreenBall : ImageGrayBall;
                    Node.SelectedImageIndex = Node.ImageIndex;
                }
                StrataSet?.Reset();
            }


            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true|false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not StratumCategory Type, return False
                if (obj is not StratumCategory)
                {
                    return false;
                }

                return ((StratumCategory)obj).Id == Id;
            }

            /// <summary>
            /// Returns a hash code for the current object
            /// </summary>
            /// <returns>Hash code for the current object</returns>
            public override int GetHashCode()
            {
                return
                    Id.GetHashCode();
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>String that represents the current object</returns>
            public override string ToString()
            {
                return String.Concat(
                    $"{nameof(StratumCategory)}: [",
                    $"{nameof(Id)}: {Functions.PrepIntArg(Id)}; ",
                    $"{nameof(IsResolved)}: {Functions.PrepBoolArg(IsResolved)}; ",
                    $"{nameof(LabelCs)}: {Functions.PrepStringArg(LabelCs)}; ",
                    $"{nameof(LabelEn)}: {Functions.PrepStringArg(LabelEn)}; ",
                    $"{nameof(DescriptionCs)}: {Functions.PrepStringArg(DescriptionCs)}; ",
                    $"{nameof(DescriptionEn)}: {Functions.PrepStringArg(DescriptionEn)}]");
            }

            #endregion Methods

        }

        #endregion Private Classes

    }

}