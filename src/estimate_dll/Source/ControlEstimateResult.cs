﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Konfigurace a výpočet odhadů"</para>
    /// <para lang="en">Control "Configuration and calculation estimates"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlEstimateResult
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Vybrané kolekce výpočetních buněk</para>
        /// <para lang="en">Selected estimation cell collections)</para>
        /// </summary>
        private EstimationCellCollectionList selectedEstimationCellCollections;

        /// <summary>
        /// <para lang="cs">Vybraná skupina výpočetních období</para>
        /// <para lang="en">Selected estimation periods</para>
        /// </summary>
        private EstimationPeriodList selectedEstimationPeriods;

        /// <summary>
        /// <para lang="cs">Vybrané seznamy fází odhadů pro čitatel a jmenovatel</para>
        /// <para lang="en">Selected phase estimate type lists for numerator and denominator</para>
        /// </summary>
        private SelectedPhaseEstimateTypes selectedPhaseEstimateTypes;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení nadpisu</para>
        /// <para lang="en">Control for display caption</para>
        /// </summary>
        private ControlCaption ctrCaption;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Zobrazení výsledků odhadů"</para>
        /// <para lang="en">Control "Display estimate results"</para>
        /// </summary>
        private ControlViewOLAP ctrViewOLAP;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Zobrazení panelů pro vybraný odhad"</para>
        /// <para lang="en">Control "Display panels for selected estimate"</para>
        /// </summary>
        private ControlViewPanels ctrViewPanels;

        #endregion Controls;


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlEstimateResult(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)</para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)</para>
        /// </summary>
        public DisplayType DisplayType
        {
            get
            {
                return
                    SelectedUnitOfMeasure.IsTotal ?
                        DisplayType.Total :
                        DisplayType.Ratio;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Zobrazení výsledků odhadů"</para>
        /// <para lang="en">Control "Display estimate results"</para>
        /// </summary>
        public ControlViewOLAP CtrViewOLAP
        {
            get
            {
                return ctrViewOLAP;
            }
        }

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraný indikátor čitatele (read-only)</para>
        /// <para lang="en">Selected indicator for numerator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedTargetVariable
        {
            get
            {
                return
                    ((ControlEstimate)ControlOwner)
                        .CtrTargetVariable
                        .SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru čitatele (read-only)</para>
        /// <para lang="en">Selected indicator metadata for numerator (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedTargetVariableMetadata
        {
            get
            {
                return
                    ((ControlEstimate)ControlOwner)
                        .CtrTargetVariable
                        .SelectedTargetVariableMetadata;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná jednotka (read-only)</para>
        /// <para lang="en">Selected unit of measure (read-only)</para>
        /// </summary>
        public Unit SelectedUnitOfMeasure
        {
            get
            {
                return
                    ((ControlEstimate)ControlOwner)
                        .CtrUnit
                        .SelectedUnitOfMeasure;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný indikátor jmenovatele (read-only)</para>
        /// <para lang="en">Selected indicator for denominator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedDenominatorTargetVariable
        {
            get
            {
                return DisplayType switch
                {
                    DisplayType.Total =>
                        null,
                    DisplayType.Ratio =>
                        ((ControlEstimate)ControlOwner)
                            .CtrUnit
                            .SelectedDenominatorTargetVariable,
                    _ =>
                        throw new ArgumentException(
                            message: $"Argument {nameof(DisplayType)} unknown value.",
                            paramName: nameof(DisplayType)),
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru jmenovatele (read-only)</para>
        /// <para lang="en">Selected indicator metadata for denominator (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedDenominatorTargetVariableMetadata
        {
            get
            {
                return DisplayType switch
                {
                    DisplayType.Total =>
                        null,
                    DisplayType.Ratio =>
                        ((ControlEstimate)ControlOwner)
                            .CtrUnit
                            .SelectedDenominatorTargetVariableMetadata,
                    _ =>
                        throw new ArgumentException(
                            message: $"Argument {nameof(DisplayType)} unknown value.",
                            paramName: nameof(DisplayType)),
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané atributové kategorie (read-only)</para>
        /// <para lang="en">Selected attribute categories (read-only)</para>
        /// </summary>
        public VariablePairList SelectedVariables
        {
            get
            {
                switch (DisplayType)
                {
                    case DisplayType.Total:
                        if (((ControlEstimate)ControlOwner).CtrVariable == null)
                        {
                            return null;
                        }
                        return
                            ((ControlEstimate)ControlOwner)
                                .CtrVariable
                                .SelectedVariables;

                    case DisplayType.Ratio:
                        if (((ControlEstimate)ControlOwner).CtrVariableRatio == null)
                        {
                            return null;
                        }
                        return
                            ((ControlEstimate)ControlOwner)
                                .CtrVariableRatio
                                .SelectedVariables;

                    default:
                        throw new ArgumentException(
                            message: $"Argument {nameof(DisplayType)} unknown value.",
                            paramName: nameof(DisplayType));
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané kolekce výpočetních buněk</para>
        /// <para lang="en">Selected estimation cell collections</para>
        /// </summary>
        public EstimationCellCollectionList SelectedEstimationCellCollections
        {
            get
            {
                return selectedEstimationCellCollections ??
                    ((ControlEstimate)ControlOwner).DefaultEstimationCellCollectionList;
            }
            set
            {
                selectedEstimationCellCollections = value ??
                    ((ControlEstimate)ControlOwner).DefaultEstimationCellCollectionList;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané výpočetní buňky (read-only)</para>
        /// <para lang="en">Selected estimation cells (read-only)</para>
        /// </summary>
        public EstimationCellList SelectedEstimationCells
        {
            get
            {
                // Omezení seznamu výpočetních buněk pouze na výpočetní buňky uvnitř zvolených kolekcí
                return Database.SNfiEsta.CEstimationCell.Reduce(
                    estimationCellCollectionId: SelectedEstimationCellCollections?.Items.Select(a => a.Id).ToList<int>());
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané seznamy fází odhadů pro čitatel a jmenovatel</para>
        /// <para lang="en">Selected phase estimate type lists for numerator and denominator</para>
        /// </summary>
        public SelectedPhaseEstimateTypes SelectedPhaseEstimateTypes
        {
            get
            {
                return selectedPhaseEstimateTypes ??
                    new SelectedPhaseEstimateTypes()
                    {
                        Numerator = ((ControlEstimate)ControlOwner).DefaultNumeratorPhaseEstimateTypeList,
                        Denominator = ((ControlEstimate)ControlOwner).DefaultNumeratorPhaseEstimateTypeList
                    };
            }
            set
            {
                selectedPhaseEstimateTypes = value ??
                    new SelectedPhaseEstimateTypes()
                    {
                        Numerator = ((ControlEstimate)ControlOwner).DefaultNumeratorPhaseEstimateTypeList,
                        Denominator = ((ControlEstimate)ControlOwner).DefaultNumeratorPhaseEstimateTypeList
                    };
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina výpočetních období</para>
        /// <para lang="en">Selected estimation periods</para>
        /// </summary>
        public EstimationPeriodList SelectedEstimationPeriods
        {
            get
            {
                if (selectedEstimationPeriods == null)
                {
                    return ((ControlEstimate)ControlOwner).DefaultSelectedEstimationPeriodList;
                }

                return selectedEstimationPeriods;
            }
            set
            {
                if (value == null)
                {
                    selectedEstimationPeriods = ((ControlEstimate)ControlOwner).DefaultSelectedEstimationPeriodList;
                }

                selectedEstimationPeriods = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                            "Předchozí" },
                        { nameof(grpSelectedTargetVariable),              "Indikátor:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlEstimateResult),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                            "Previous" },
                        { nameof(grpSelectedTargetVariable),              "Indicator:" },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlEstimateResult),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            SelectedEstimationCellCollections =
                   ((ControlEstimate)ControlOwner).DefaultEstimationCellCollectionList;

            SelectedEstimationPeriods =
                 ((ControlEstimate)ControlOwner).DefaultSelectedEstimationPeriodList;

            SelectedPhaseEstimateTypes = new SelectedPhaseEstimateTypes()
            {
                Numerator = ((ControlEstimate)ControlOwner).DefaultNumeratorPhaseEstimateTypeList,
                Denominator = ((ControlEstimate)ControlOwner).DefaultNumeratorPhaseEstimateTypeList
            };

            InitializeCaption();

            InitializeViewOLAP();

            InitializeViewPanels();

            InitializeLabels();

            onEdit = true;

            pnlMain.BackColor = Setting.BorderColor;
            pnlWorkSpace.BackColor = Setting.BorderColor;

            splCaption.BackColor = Setting.SpliterColor;
            splCaption.SplitterDistance = Setting.SplEstimateResultCaptionDistance;

            splEstimateResult.BackColor = Setting.SpliterColor;
            splEstimateResult.SplitterDistance = Setting.SplEstimateResultDistance;

            splEstimateResult.Panel1Collapsed = true;

            onEdit = false;

            btnPrevious.Click += new EventHandler(
              (sender, e) =>
              {
                  PreviousClick?.Invoke(
                   sender: this,
                   e: new EventArgs());
              });

            splCaption.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplEstimateResultCaptionDistance =
                            splCaption.SplitterDistance;
                    }
                });

            splEstimateResult.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplEstimateResultDistance =
                            splEstimateResult.SplitterDistance;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku nadpisu</para>
        /// <para lang="en">Initialization of the control for display caption</para>
        /// </summary>
        private void InitializeCaption()
        {
            pnlCaption.Controls.Clear();

            ctrCaption =
                new ControlCaption(
                    controlOwner: this,
                    numeratorTargetVariableMetadata: SelectedTargetVariableMetadata,
                    denominatorTargetVariableMetadata: SelectedDenominatorTargetVariableMetadata,
                    variablePairs: SelectedVariables,
                    captionVersion: CaptionVersion.Extended,
                    withAttributeCategories: true,
                    withIdentifiers: true)
                {
                    Dock = DockStyle.Fill
                };

            ctrCaption.CreateControl();

            pnlCaption.Controls.Add(value: ctrCaption);
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku pro "Zobrazení výsledků odhadů"</para>
        /// <para lang="en">Initialization of the control for "Display estimate results"</para>
        /// </summary>
        private void InitializeViewOLAP()
        {
            pnlViewOLAP.Controls.Clear();

            ctrViewOLAP =
                new ControlViewOLAP(controlOwner: this)
                {
                    DisplayButtonFilterColumns = true,
                    DisplayButtonClose = false,
                    Dock = DockStyle.Fill
                };

            ctrViewOLAP.SelectedEstimateChanged += (sender, e) =>
            {
                SetRowFilters();
            };

            ctrViewOLAP.RowFilterChanged += (sender, e) =>
            {
            };

            ctrViewOLAP.EstimatesConfigured += (sender, e) =>
            {
                // Pokud došlo, k nějaké změně v databázi
                // (vytvořena konfigurace, vytvořena, smazána nebo upravena skupina panelů)

                ReloadEstimates();
                ReloadAdditivityData();
                ReloadOLAPCube();
                ctrViewOLAP.RowFilter = ctrViewOLAP.RowFilter;
            };

            ctrViewOLAP.EstimatesCalculated += (sender, e) =>
            {
                // Po dokončení výpočtu odhadů je třeba vypočetené
                // odhady načíst z databáze, zkontrolovat aditivitu a zobrazit

                ReloadEstimates();
                ReloadAdditivityData();
                ReloadOLAPCube();
                ctrViewOLAP.RowFilter = ctrViewOLAP.RowFilter;
            };

            ctrViewOLAP.DisplayHistoryClick += (sender, e) =>
            {
                // Při zobrazení historie je nutné znovu načíst OLAP s historickými odhady
                ReloadOLAPCube();
                ctrViewOLAP.RowFilter = ctrViewOLAP.RowFilter;
            };

            ctrViewOLAP.DisplayPanelsClick += (sender, e) =>
            {
                SetRowFilters();
                splViewOLAP.Panel1Collapsed = true;         // OLAP
                splViewOLAP.Panel2Collapsed = false;
                splViewPanels.Panel1Collapsed = true;       // Historie
                splViewPanels.Panel2Collapsed = false;      // Panely
            };

            ctrViewOLAP.EstimationPeriodChanged += (sender, e) =>
            {
                ReloadEstimates();
                ReloadAdditivityData();
                ReloadOLAPCube();
                ctrViewOLAP.RowFilter = ctrViewOLAP.RowFilter;
            };

            ctrViewOLAP.PanelReferenceYearSetGroupChanged += (sender, e) =>
            {
                ReloadEstimates();
                ReloadAdditivityData();
                ReloadOLAPCube();
                ctrViewOLAP.RowFilter = ctrViewOLAP.RowFilter;
            };

            ctrViewOLAP.EstimationCellCollectionChanged += (sender, e) =>
            {
                ((ControlEstimate)ControlOwner).ReloadEstimationCells(
                    selectedEstimationCellCollections: SelectedEstimationCellCollections,
                    verbose: false);

                ReloadEstimates();
                ReloadAdditivityData();
                ReloadOLAPCube();
                ctrViewOLAP.RowFilter = ctrViewOLAP.RowFilter;
            };

            ctrViewOLAP.CreateControl();

            pnlViewOLAP.Controls.Add(value: ctrViewOLAP);
        }

        /// <summary>
        /// <para lang="cs">Nastaví filtr řádků pro DataGridView seznam panelů</para>
        /// <para lang="en">Sets row filter for DataGridView list of panels"</para>
        /// </summary>
        private void SetRowFilters()
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    if (ctrViewPanels != null)
                    {
                        ctrViewPanels.RowFilter = (ctrViewOLAP.SelectedEstimateTotal == null)
                            ? "false"
                            : $"{VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name} = {ctrViewOLAP.SelectedEstimateTotal.PanelRefYearSetGroupId}";
                    }
                    return;

                case DisplayType.Ratio:
                    if (ctrViewPanels != null)
                    {
                        ctrViewPanels.RowFilter = (ctrViewOLAP.SelectedEstimateRatio == null)
                            ? "false"
                            : $"{VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name} = {ctrViewOLAP.SelectedEstimateRatio.PanelRefYearSetGroupId}";
                    }
                    return;

                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(DisplayType)} unknown value.",
                        paramName: nameof(DisplayType));
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku pro "Zobrazení panelů pro vybraný odhad"</para>
        /// <para lang="en">Initialization of the control for "Display panels for selected estimate"</para>
        /// </summary>
        private void InitializeViewPanels()
        {
            pnlViewPanels.Controls.Clear();

            ctrViewPanels =
                new ControlViewPanels(controlOwner: this)
                {
                    DisplayButtonFilterColumns = false,
                    DisplayButtonClose = true,
                    Dock = DockStyle.Fill
                };

            ctrViewPanels.Closed += (sender, e) =>
            {
                splViewOLAP.Panel1Collapsed = false;        // OLAP
                splViewOLAP.Panel2Collapsed = true;
                splViewPanels.Panel1Collapsed = false;      // Historie
                splViewPanels.Panel2Collapsed = false;      // Panely
            };

            ctrViewPanels.CreateControl();

            pnlViewPanels.Controls.Add(value: ctrViewPanels);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            ctrCaption?.InitializeLabels();

            ctrViewOLAP?.InitializeLabels();

            ctrViewPanels?.InitializeLabels();

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            grpSelectedTargetVariable.Text =
                labels.TryGetValue(key: nameof(grpSelectedTargetVariable),
                out string grpSelectedTargetVariableText)
                    ? grpSelectedTargetVariableText
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            ReloadEstimates();

            ReloadAdditivityData();

            ReloadOLAPCube();
        }

        /// <summary>
        /// <para lang="cs">
        /// Nahraje znovu pohledy na tabulky t_estimate_conf, t_total_estimate_conf a t_result pro čitatel, jmenovatel a podíl
        /// Nahraje znovu číselníky c_estimation_period, c_panel_refyearset_group, t_panel_refyearset_group
        /// Znovu nahraní dat je nutné při vytvoření nového období, nové skupiny panelů nebo nové konfigurace
        /// </para>
        /// <para lang="en">
        /// Reloading the t_estimate_conf, t_total_estimate_conf and t_result table views for numerator, denominator and ratio estimate
        /// Reloading the c_estimation_period, c_panel_refyearset_group, t_panel_refyearset_group tables
        /// Reloading of data is required when a new period, new panel refyearset or new configuration is created</para>
        /// </summary>
        public void ReloadEstimates()
        {
            Database.SNfiEsta.CEstimationPeriod.ReLoad();
            Database.SNfiEsta.CPanelRefYearSetGroup.ReLoad();
            Database.SNfiEsta.TPanelRefYearSetGroup.ReLoad();

            Database.SNfiEsta.TTotalEstimateConf.ReLoad();
            Database.SNfiEsta.TEstimateConf.ReLoad();
            Database.SNfiEsta.TResult.ReLoad();

            Database.SNfiEsta.VPanelRefYearSetPairs.Load(
                tPanelRefYearSetGroup: Database.SNfiEsta.TPanelRefYearSetGroup,
                tPanel: Database.SDesign.TPanel,
                tReferenceYearSet: Database.SDesign.TReferenceYearSet);

            Database.SNfiEsta.TAuxConf.ReLoad();
            Database.SNfiEsta.TModel.ReLoad();
            Database.SNfiEsta.FaParamArea.ReLoad();
            Database.SNfiEsta.CParamAreaType.ReLoad();
        }

        /// <summary>
        /// <para lang="cs">Načtení dat additivity odhadů z databázových pohledů</para>
        /// <para lang="en">Retrieving additivity estimation data from database views</para>
        /// </summary>
        public void ReloadAdditivityData()
        {
            TotalEstimateConfList selectedTotalEstimateConfList =
                Database.SNfiEsta.TTotalEstimateConf.Reduce(
                    phaseEstimateTypeId: SelectedPhaseEstimateTypes.Numerator.Items.Select(a => a.Id).ToList<int>(),
                    variableId: SelectedVariables.Numerator.Items.Select(a => a.VariableId).ToList<int>(),
                    estimationCellId: SelectedEstimationCells.Items.Select(a => a.Id).ToList<int>(),
                    estimationPeriodId: null,
                    panelRefYearSetGroupId: null,
                    auxConfId: null);

            switch (DisplayType)
            {
                case DisplayType.Total:
                    EstimateConfList estimateConfsForTotal =
                        Database.SNfiEsta.TEstimateConf.Reduce(
                            estimateTypeId: [Database.SNfiEsta.CEstimateType.Total.Id],
                            totalEstimateConfId: selectedTotalEstimateConfList.Items.Select(a => a.Id).ToList<int>());

                    Database.SNfiEsta.VTotalAdditivityAttr =
                            NfiEstaFunctions.FnAddResTotalAttr.Execute(
                                database: Database,
                                estConfs: estimateConfsForTotal.Items
                                            .Select(a => (Nullable<int>)a.Id)
                                            .ToList<Nullable<int>>(),
                                minDiff: Setting.AdditivityLimit,
                                includeNullDiff: false);

                    Database.SNfiEsta.VTotalAdditivityGeo =
                            NfiEstaFunctions.FnAddResTotalGeo.Execute(
                                database: Database,
                                estConfs: estimateConfsForTotal.Items
                                            .Select(a => (Nullable<int>)a.Id)
                                            .ToList<Nullable<int>>(),
                                minDiff: Setting.AdditivityLimit,
                                includeNullDiff: false);
                    break;

                case DisplayType.Ratio:
                    EstimateConfList estimateConfsForRatio =
                        Database.SNfiEsta.TEstimateConf.Reduce(
                            estimateTypeId: [Database.SNfiEsta.CEstimateType.Ratio.Id],
                            totalEstimateConfId: selectedTotalEstimateConfList.Items.Select(a => a.Id).ToList<int>());

                    Database.SNfiEsta.VRatioAdditivityAttr =
                        NfiEstaFunctions.FnAddResRatioAttr.Execute(
                            database: Database,
                            estConfs: estimateConfsForRatio.Items
                                        .Select(a => (Nullable<int>)a.Id)
                                        .ToList<Nullable<int>>(),
                            minDiff: Setting.AdditivityLimit,
                            includeNullDiff: false);
                    break;

                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(DisplayType)} unknown value.",
                        paramName: nameof(DisplayType));
            }
        }

        /// <summary>
        /// <para lang="cs">Načtení dat do OLAP kostky z databáze</para>
        /// <para lang="en">Loading OLAP cube data from the database</para>
        /// </summary>
        public void ReloadOLAPCube()
        {
            switch (DisplayType)
            {
                case DisplayType.Total:

                    // OLAP pro úhrn - dimenze
                    OLAPTotalEstimateDimensionList tTotalOLAPDimension =
                        new(database: Database);
                    tTotalOLAPDimension.ReLoad(
                        cPhaseEstimateType: SelectedPhaseEstimateTypes.Numerator,
                        cEstimationPeriod: SelectedEstimationPeriods,
                        variables: SelectedVariables.Numerator.Items,
                        cEstimationCell: SelectedEstimationCells,
                        cEstimationCellCollection: Database.SNfiEsta.CEstimationCellCollection,
                        condition: null,
                        limit: null);

                    // OLAP pro úhrn - hodnoty
                    OLAPTotalEstimateValueList tTotalOLAPValue =
                         new(database: Database);
                    tTotalOLAPValue.ReLoad(
                        cPhaseEstimateType: SelectedPhaseEstimateTypes.Numerator,
                        cEstimationPeriod: SelectedEstimationPeriods,
                        variables: SelectedVariables.Numerator.Items,
                        cEstimationCell: SelectedEstimationCells,
                        cPanelRefYearSetGroup: Database.SNfiEsta.CPanelRefYearSetGroup,
                        tEstimateConf: Database.SNfiEsta.TEstimateConf,
                        tTotalEstimateConf: Database.SNfiEsta.TTotalEstimateConf,
                        tResult: Database.SNfiEsta.TResult,
                        fnAddResTotalAttr: Database.SNfiEsta.VTotalAdditivityAttr,
                        fnAddResTotalGeo: Database.SNfiEsta.VTotalAdditivityGeo,
                        additivityLimit: Setting.AdditivityLimit,
                        isLatest: !ctrViewOLAP.HistoryDisplayed,
                        condition: null,
                        limit: null,
                        tAuxConf: Database.SNfiEsta.TAuxConf,
                        tModel: Database.SNfiEsta.TModel,
                        faParamArea: Database.SNfiEsta.FaParamArea,
                        cParamAreaType: Database.SNfiEsta.CParamAreaType);

                    // OLAP pro úhrn - celá
                    Database.SNfiEsta.VTotalOLAP.ReLoad(
                        olapDimensions: tTotalOLAPDimension,
                        olapValues: tTotalOLAPValue,
                        cEstimateConfStatus: Database.SNfiEsta.CEstimateConfStatus,
                        additivityLimit: Setting.AdditivityLimit,
                        condition: null,
                        limit: null);

                    ctrViewOLAP.DataSourceTotal = Database.SNfiEsta.VTotalOLAP;
                    ctrViewPanels.DataSource = Database.SNfiEsta.VPanelRefYearSetPairs;
                    break;

                case DisplayType.Ratio:

                    // OLAP pro podíl - dimenze
                    OLAPRatioEstimateDimensionList tRatioOLAPDimension =
                        new(database: Database);
                    tRatioOLAPDimension.ReLoad(
                        cPhaseEstimateTypeNumerator: SelectedPhaseEstimateTypes.Numerator,
                        cPhaseEstimateTypeDenominator: SelectedPhaseEstimateTypes.Denominator,
                        cEstimationPeriod: SelectedEstimationPeriods,
                        variablePairs: SelectedVariables,
                        cEstimationCell: SelectedEstimationCells,
                        cEstimationCellCollection: Database.SNfiEsta.CEstimationCellCollection,
                        condition: null,
                        limit: null);

                    // OLAP pro podíl - hodnoty
                    OLAPRatioEstimateValueList tRatioOLAPValue =
                         new(database: Database);
                    tRatioOLAPValue.ReLoad(
                        cPhaseEstimateTypeNumerator: SelectedPhaseEstimateTypes.Numerator,
                        cPhaseEstimateTypeDenominator: SelectedPhaseEstimateTypes.Denominator,
                        cEstimationPeriod: SelectedEstimationPeriods,
                        variablePairs: SelectedVariables,
                        cEstimationCell: SelectedEstimationCells,
                        cPanelRefYearSetGroup: Database.SNfiEsta.CPanelRefYearSetGroup,
                        tEstimateConf: Database.SNfiEsta.TEstimateConf,
                        tTotalEstimateConf: Database.SNfiEsta.TTotalEstimateConf,
                        tResult: Database.SNfiEsta.TResult,
                        fnAddResRatioAttr: Database.SNfiEsta.VRatioAdditivityAttr,
                        additivityLimit: Setting.AdditivityLimit,
                        isLatest: !ctrViewOLAP.HistoryDisplayed,
                        condition: null,
                        limit: null);

                    // OLAP pro podíl - celá
                    Database.SNfiEsta.VRatioOLAP.ReLoad(
                        olapDimensions: tRatioOLAPDimension,
                        olapValues: tRatioOLAPValue,
                        cEstimateConfStatus: Database.SNfiEsta.CEstimateConfStatus,
                        additivityLimit: Setting.AdditivityLimit,
                        condition: null,
                        limit: null);

                    ctrViewOLAP.DataSourceRatio = Database.SNfiEsta.VRatioOLAP;
                    ctrViewPanels.DataSource = Database.SNfiEsta.VPanelRefYearSetPairs;
                    break;

                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(DisplayType)} unknown value.",
                        paramName: nameof(DisplayType));
            }
        }

        #endregion Methods

    }

    #region enum DisplayType

    /// <summary>
    /// <para lang="cs">Typ výsledků odhadů - úhrn nebo podíl</para>
    /// <para lang="en">Estimate result type - total or ratio</para>
    /// </summary>
    internal enum DisplayType
    {
        /// <summary>
        /// <para lang="cs">Zobrazení výsledků odhadů pro úhrn</para>
        /// <para lang="en">Display estimate results for total</para>
        /// </summary>
        Total = 0,

        /// <summary>
        /// <para lang="cs">Zobrazení výsledků odhadů pro podíl</para>
        /// <para lang="en">Display estimate results for ratio</para>
        /// </summary>
        Ratio = 1
    }

    #endregion enum DisplayType

}