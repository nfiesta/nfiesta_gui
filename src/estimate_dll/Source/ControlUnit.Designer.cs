﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlUnit
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlPrevious = new System.Windows.Forms.Panel();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.pnlNext = new System.Windows.Forms.Panel();
            this.btnNext = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splCaption = new System.Windows.Forms.SplitContainer();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.grpNumerator = new System.Windows.Forms.GroupBox();
            this.tlpSelectedUnitOfMeasure = new System.Windows.Forms.TableLayoutPanel();
            this.grpSelectedUnitOfMeasure = new System.Windows.Forms.GroupBox();
            this.cboSelectedUnitOfMeasure = new System.Windows.Forms.ComboBox();
            this.grpDenominator = new System.Windows.Forms.GroupBox();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlPrevious.SuspendLayout();
            this.pnlNext.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).BeginInit();
            this.splCaption.Panel1.SuspendLayout();
            this.splCaption.Panel2.SuspendLayout();
            this.splCaption.SuspendLayout();
            this.grpNumerator.SuspendLayout();
            this.tlpSelectedUnitOfMeasure.SuspendLayout();
            this.grpSelectedUnitOfMeasure.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.SystemColors.Control;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 1);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(3, 3);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(954, 534);
            this.tlpMain.TabIndex = 2;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.Controls.Add(this.pnlPrevious, 1, 0);
            this.tlpButtons.Controls.Add(this.pnlNext, 2, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 494);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(954, 40);
            this.tlpButtons.TabIndex = 11;
            // 
            // pnlPrevious
            // 
            this.pnlPrevious.Controls.Add(this.btnPrevious);
            this.pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrevious.Location = new System.Drawing.Point(634, 0);
            this.pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPrevious.Name = "pnlPrevious";
            this.pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPrevious.Size = new System.Drawing.Size(160, 40);
            this.pnlPrevious.TabIndex = 17;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrevious.Location = new System.Drawing.Point(5, 5);
            this.btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(150, 30);
            this.btnPrevious.TabIndex = 15;
            this.btnPrevious.Text = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlNext
            // 
            this.pnlNext.Controls.Add(this.btnNext);
            this.pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNext.Location = new System.Drawing.Point(794, 0);
            this.pnlNext.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNext.Name = "pnlNext";
            this.pnlNext.Padding = new System.Windows.Forms.Padding(5);
            this.pnlNext.Size = new System.Drawing.Size(160, 40);
            this.pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnNext.Location = new System.Drawing.Point(5, 5);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 15;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlWorkSpace.Controls.Add(this.splCaption);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 494);
            this.pnlWorkSpace.TabIndex = 7;
            // 
            // splCaption
            // 
            this.splCaption.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCaption.Location = new System.Drawing.Point(0, 0);
            this.splCaption.Margin = new System.Windows.Forms.Padding(0);
            this.splCaption.Name = "splCaption";
            this.splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            this.splCaption.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splCaption.Panel1.Controls.Add(this.pnlCaption);
            this.splCaption.Panel1MinSize = 0;
            // 
            // splCaption.Panel2
            // 
            this.splCaption.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splCaption.Panel2.Controls.Add(this.grpDenominator);
            this.splCaption.Panel2.Controls.Add(this.grpNumerator);
            this.splCaption.Panel2MinSize = 0;
            this.splCaption.Size = new System.Drawing.Size(954, 491);
            this.splCaption.SplitterDistance = 130;
            this.splCaption.SplitterWidth = 1;
            this.splCaption.TabIndex = 2;
            // 
            // pnlCaption
            // 
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Size = new System.Drawing.Size(954, 130);
            this.pnlCaption.TabIndex = 12;
            // 
            // grpNumerator
            // 
            this.grpNumerator.Controls.Add(this.tlpSelectedUnitOfMeasure);
            this.grpNumerator.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpNumerator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpNumerator.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpNumerator.Location = new System.Drawing.Point(0, 0);
            this.grpNumerator.Margin = new System.Windows.Forms.Padding(0);
            this.grpNumerator.Name = "grpNumerator";
            this.grpNumerator.Padding = new System.Windows.Forms.Padding(5);
            this.grpNumerator.Size = new System.Drawing.Size(954, 90);
            this.grpNumerator.TabIndex = 14;
            this.grpNumerator.TabStop = false;
            this.grpNumerator.Text = "grpNumerator";
            // 
            // tlpSelectedUnitOfMeasure
            // 
            this.tlpSelectedUnitOfMeasure.ColumnCount = 2;
            this.tlpSelectedUnitOfMeasure.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tlpSelectedUnitOfMeasure.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSelectedUnitOfMeasure.Controls.Add(this.grpSelectedUnitOfMeasure, 0, 0);
            this.tlpSelectedUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSelectedUnitOfMeasure.Location = new System.Drawing.Point(5, 20);
            this.tlpSelectedUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            this.tlpSelectedUnitOfMeasure.Name = "tlpSelectedUnitOfMeasure";
            this.tlpSelectedUnitOfMeasure.RowCount = 1;
            this.tlpSelectedUnitOfMeasure.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSelectedUnitOfMeasure.Size = new System.Drawing.Size(944, 65);
            this.tlpSelectedUnitOfMeasure.TabIndex = 12;
            // 
            // grpSelectedUnitOfMeasure
            // 
            this.grpSelectedUnitOfMeasure.Controls.Add(this.cboSelectedUnitOfMeasure);
            this.grpSelectedUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpSelectedUnitOfMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSelectedUnitOfMeasure.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpSelectedUnitOfMeasure.Location = new System.Drawing.Point(5, 5);
            this.grpSelectedUnitOfMeasure.Margin = new System.Windows.Forms.Padding(5);
            this.grpSelectedUnitOfMeasure.Name = "grpSelectedUnitOfMeasure";
            this.grpSelectedUnitOfMeasure.Padding = new System.Windows.Forms.Padding(5);
            this.grpSelectedUnitOfMeasure.Size = new System.Drawing.Size(240, 55);
            this.grpSelectedUnitOfMeasure.TabIndex = 4;
            this.grpSelectedUnitOfMeasure.TabStop = false;
            this.grpSelectedUnitOfMeasure.Text = "grpSelectedUnitOfMeasure";
            // 
            // cboSelectedUnitOfMeasure
            // 
            this.cboSelectedUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboSelectedUnitOfMeasure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSelectedUnitOfMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboSelectedUnitOfMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cboSelectedUnitOfMeasure.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboSelectedUnitOfMeasure.FormattingEnabled = true;
            this.cboSelectedUnitOfMeasure.Location = new System.Drawing.Point(5, 20);
            this.cboSelectedUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            this.cboSelectedUnitOfMeasure.Name = "cboSelectedUnitOfMeasure";
            this.cboSelectedUnitOfMeasure.Size = new System.Drawing.Size(230, 24);
            this.cboSelectedUnitOfMeasure.TabIndex = 0;
            // 
            // grpDenominator
            // 
            this.grpDenominator.BackColor = System.Drawing.SystemColors.Control;
            this.grpDenominator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDenominator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpDenominator.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDenominator.Location = new System.Drawing.Point(0, 90);
            this.grpDenominator.Margin = new System.Windows.Forms.Padding(0);
            this.grpDenominator.Name = "grpDenominator";
            this.grpDenominator.Padding = new System.Windows.Forms.Padding(5);
            this.grpDenominator.Size = new System.Drawing.Size(954, 270);
            this.grpDenominator.TabIndex = 15;
            this.grpDenominator.TabStop = false;
            this.grpDenominator.Text = "grpDenominator";
            // 
            // ControlUnit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlUnit";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlPrevious.ResumeLayout(false);
            this.pnlNext.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splCaption.Panel1.ResumeLayout(false);
            this.splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).EndInit();
            this.splCaption.ResumeLayout(false);
            this.grpNumerator.ResumeLayout(false);
            this.tlpSelectedUnitOfMeasure.ResumeLayout(false);
            this.grpSelectedUnitOfMeasure.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.GroupBox grpDenominator;
        private System.Windows.Forms.GroupBox grpNumerator;
        private System.Windows.Forms.TableLayoutPanel tlpSelectedUnitOfMeasure;
        private System.Windows.Forms.GroupBox grpSelectedUnitOfMeasure;
        private System.Windows.Forms.ComboBox cboSelectedUnitOfMeasure;
    }

}
