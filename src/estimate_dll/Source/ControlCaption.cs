﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení nadpisu</para>
    /// <para lang="en">Control for display caption</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlCaption
        : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru pro čitatel</para>
        /// <para lang="en">Metadata of the selected indicator for the numerator</para>
        /// </summary>
        private ITargetVariableMetadata numeratorTargetVariableMetadata;

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru pro jmenovatel</para>
        /// <para lang="en">Metadata of the selected indicator for the denominator</para>
        /// </summary>
        private ITargetVariableMetadata denominatorTargetVariableMetadata;

        /// <summary>
        /// <para lang="cs">Kategorie atributového členění pro čitatel a jmenovatel</para>
        /// <para lang="en">Attribute breakdown categories for numerator and denominator</para>
        /// </summary>
        private VariablePairList variablePairs;

        /// <summary>
        /// <para lang="cs">Stručná nebo rozšířená verze nadpisu?</para>
        /// <para lang="en">Brief or extended version of the caption?</para>
        /// </summary>
        private CaptionVersion captionVersion;

        /// <summary>
        /// <para lang="cs">Zobrazovat výpis atributových kategorií?</para>
        /// <para lang="en">Display attribute category list?</para>
        /// </summary>
        private bool withAttributeCategories;

        /// <summary>
        /// <para lang="cs">Budou se zobrazovat také identifikační čísla?</para>
        /// <para lang="en">Will identification numbers also be displayed?</para>
        /// </summary>
        private bool withIdentifiers;

        private string strAltogether = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="numeratorTargetVariableMetadata">
        /// <para lang="cs">Metadata vybraného indikátoru pro čitatel</para>
        /// <para lang="en">Metadata of the selected indicator for the numerator</para>
        /// </param>
        /// <param name="denominatorTargetVariableMetadata">
        /// <para lang="cs">Metadata vybraného indikátoru pro jmenovatel</para>
        /// <para lang="en">Metadata of the selected indicator for the denominator</para>
        /// </param>
        /// <param name="variablePairs">
        /// <para lang="cs">Kategorie atributového členění pro čitatel a jmenovatel</para>
        /// <para lang="en">Attribute breakdown categories for numerator and denominator</para>
        /// </param>
        /// <param name="captionVersion">
        /// <para lang="cs">Stručná nebo rozšířená verze nadpisu?</para>
        /// <para lang="en">Brief or extended version of the caption?</para>
        /// </param>
        /// <param name="withAttributeCategories">
        /// <para lang="cs">Zobrazovat výpis atributových kategorií?</para>
        /// <para lang="en">Display attribute category list?</para>
        /// </param>
        /// <param name="withIdentifiers">
        /// <para lang="cs">Budou se zobrazovat také identifikační čísla?</para>
        /// <para lang="en">Will identification numbers also be displayed?</para>
        /// </param>
        public ControlCaption(
            Control controlOwner,
            ITargetVariableMetadata numeratorTargetVariableMetadata = null,
            ITargetVariableMetadata denominatorTargetVariableMetadata = null,
            VariablePairList variablePairs = null,
            CaptionVersion captionVersion = CaptionVersion.Brief,
            bool withAttributeCategories = false,
            bool withIdentifiers = true)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                numeratorTargetVariableMetadata: numeratorTargetVariableMetadata,
                denominatorTargetVariableMetadata: denominatorTargetVariableMetadata,
                variablePairs: variablePairs,
                captionVersion: captionVersion,
                withAttributeCategories: withAttributeCategories,
                withIdentifiers: withIdentifiers);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru pro čitatel</para>
        /// <para lang="en">Metadata of the selected indicator for the numerator</para>
        /// </summary>
        public ITargetVariableMetadata NumeratorTargetVariableMetadata
        {
            get
            {
                return numeratorTargetVariableMetadata;
            }
            set
            {
                numeratorTargetVariableMetadata = value;
                SetControlDesign();
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru pro jmenovatel</para>
        /// <para lang="en">Metadata of the selected indicator for the denominator</para>
        /// </summary>
        public ITargetVariableMetadata DenominatorTargetVariableMetadata
        {
            get
            {
                return denominatorTargetVariableMetadata;
            }
            set
            {
                denominatorTargetVariableMetadata = value;
                SetControlDesign();
            }
        }

        /// <summary>
        /// <para lang="cs">Kategorie atributového členění pro čitatel a jmenovatel</para>
        /// <para lang="en">Attribute breakdown categories for numerator and denominator</para>
        /// </summary>
        public VariablePairList VariablePairs
        {
            get
            {
                return variablePairs;
            }
            set
            {
                variablePairs = value;
                SetControlDesign();
            }
        }

        /// <summary>
        /// <para lang="cs">Stručná nebo rozšířená verze nadpisu?</para>
        /// <para lang="en">Brief or extended version of the caption?</para>
        /// </summary>
        public CaptionVersion CaptionVersion
        {
            get
            {
                return captionVersion;
            }
            set
            {
                captionVersion = value;
                SetControlDesign();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazovat výpis atributových kategorií?</para>
        /// <para lang="en">Display attribute category list?</para>
        /// </summary>
        public bool WithAttributeCategories
        {
            get
            {
                return withAttributeCategories;
            }
            set
            {
                withAttributeCategories = value;
                SetControlDesign();
            }
        }

        /// <summary>
        /// <para lang="cs">Budou se zobrazovat také identifikační čísla?</para>
        /// <para lang="en">Will identification numbers also be displayed?</para>
        /// </summary>
        public bool WithIdentifiers
        {
            get
            {
                return withIdentifiers;
            }
            set
            {
                withIdentifiers = value;
                InitializeLabels();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(lblEmptyCaption),                    "Indikátor není vybrán" },
                        { nameof(lblExStateOrChangeLabel),            "stav nebo změna" },
                        { nameof(lblExLocalDensityLabel),             "příspěvek lokální hustoty" },
                        { nameof(lblExVersionLabel),                  "verze" },
                        { nameof(lblExDefinitionVariantLabel),        "definiční varianta" },
                        { nameof(lblExAreaDomainLabel),               "plošná doména" },
                        { nameof(lblExPopulationLabel),               "populace" },
                        { nameof(lblExAttrAreaDomainLabel),           "plošná doména" },
                        { nameof(lblExAttrSubPopulationLabel),        "subpopulace" },
                        { nameof(lblWideStateOrChangeLabel),          "stav nebo změna" },
                        { nameof(lblWideLocalDensityLabel),           "příspěvek lokální hustoty" },
                        { nameof(lblWideVersionLabel),                "verze" },
                        { nameof(lblWideDefinitionVariantLabel),      "definiční varianta" },
                        { nameof(lblWideAreaDomainLabel),             "plošná doména" },
                        { nameof(lblWidePopulationLabel),             "populace" },
                        { nameof(lblWideAttrAreaDomainLabel),         "plošná doména" },
                        { nameof(lblWideAttrSubPopulationLabel),      "subpopulace" },
                        { nameof(lblWidePlaceHolderLabel),            String.Empty },
                        { nameof(lblWidePlaceHolderValue),            String.Empty },
                        { nameof(lblNumeratorCaption),                "čitatel" },
                        { nameof(lblNumeratorCaptionPlaceHolder),     String.Empty },
                        { nameof(lblDenominatorCaption),              "jmenovatel" },
                        { nameof(lblDenominatorCaptionPlaceHolder),   String.Empty },
                        { nameof(lblNumStateOrChangeLabel),           "stav nebo změna" },
                        { nameof(lblNumLocalDensityLabel),            "příspěvek lokální hustoty" },
                        { nameof(lblNumVersionLabel),                 "verze" },
                        { nameof(lblNumDefinitionVariantLabel),       "definiční varianta" },
                        { nameof(lblNumAreaDomainLabel),              "plošná doména" },
                        { nameof(lblNumPopulationLabel),              "populace" },
                        { nameof(lblNumAttrAreaDomainLabel),          "plošná doména" },
                        { nameof(lblNumAttrSubPopulationLabel),       "subpopulace" },
                        { nameof(lblDenomStateOrChangeLabel),         "stav nebo změna" },
                        { nameof(lblDenomLocalDensityLabel),          "příspěvek lokální hustoty" },
                        { nameof(lblDenomVersionLabel),               "verze" },
                        { nameof(lblDenomDefinitionVariantLabel),     "definiční varianta" },
                        { nameof(lblDenomAreaDomainLabel),            "plošná doména" },
                        { nameof(lblDenomPopulationLabel),            "populace" },
                        { nameof(lblDenomAttrAreaDomainLabel),        "plošná doména" },
                        { nameof(lblDenomAttrSubPopulationLabel),     "subpopulace" },
                        { nameof(strAltogether),                      "bez rozlišení" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlCaption),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(lblEmptyCaption),                    "Indicator is not selected" },
                        { nameof(lblExStateOrChangeLabel),            "state or change" },
                        { nameof(lblExLocalDensityLabel),             "local density contribution" },
                        { nameof(lblExVersionLabel),                  "version" },
                        { nameof(lblExDefinitionVariantLabel),        "definition variant" },
                        { nameof(lblExAreaDomainLabel),               "area domain" },
                        { nameof(lblExPopulationLabel),               "population" },
                        { nameof(lblExAttrAreaDomainLabel),           "area domain" },
                        { nameof(lblExAttrSubPopulationLabel),        "subpopulation" },
                        { nameof(lblWideStateOrChangeLabel),          "state or change" },
                        { nameof(lblWideLocalDensityLabel),           "local density contribution" },
                        { nameof(lblWideVersionLabel),                "version" },
                        { nameof(lblWideDefinitionVariantLabel),      "definition variant" },
                        { nameof(lblWideAreaDomainLabel),             "area domain"  },
                        { nameof(lblWidePopulationLabel),             "population" },
                        { nameof(lblWideAttrAreaDomainLabel),         "area domain" },
                        { nameof(lblWideAttrSubPopulationLabel),      "subpopulation" },
                        { nameof(lblWidePlaceHolderLabel),            String.Empty },
                        { nameof(lblWidePlaceHolderValue),            String.Empty },
                        { nameof(lblNumeratorCaption),                "numerator" },
                        { nameof(lblNumeratorCaptionPlaceHolder),     String.Empty },
                        { nameof(lblDenominatorCaption),              "denominator" },
                        { nameof(lblDenominatorCaptionPlaceHolder),   String.Empty },
                        { nameof(lblNumStateOrChangeLabel),           "state or change" },
                        { nameof(lblNumLocalDensityLabel),            "local density contribution" },
                        { nameof(lblNumVersionLabel),                 "version" },
                        { nameof(lblNumDefinitionVariantLabel),       "definition variant" },
                        { nameof(lblNumAreaDomainLabel),              "area domain" },
                        { nameof(lblNumPopulationLabel),              "population" },
                        { nameof(lblNumAttrAreaDomainLabel),          "area domain" },
                        { nameof(lblNumAttrSubPopulationLabel),       "subpopulation" },
                        { nameof(lblDenomStateOrChangeLabel),         "state or change" },
                        { nameof(lblDenomLocalDensityLabel),          "local density contribution" },
                        { nameof(lblDenomVersionLabel),               "version" },
                        { nameof(lblDenomDefinitionVariantLabel),     "definition variant" },
                        { nameof(lblDenomAreaDomainLabel),            "area domain" },
                        { nameof(lblDenomPopulationLabel),            "population" },
                        { nameof(lblDenomAttrAreaDomainLabel),        "area domain" },
                        { nameof(lblDenomAttrSubPopulationLabel),     "subpopulation" },
                        { nameof(strAltogether),                      "altogether"}
                      }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlCaption),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="numeratorTargetVariableMetadata">
        /// <para lang="cs">Metadata vybraného indikátoru pro čitatel</para>
        /// <para lang="en">Metadata of the selected indicator for the numerator</para>
        /// </param>
        /// <param name="denominatorTargetVariableMetadata">
        /// <para lang="cs">Metadata vybraného indikátoru pro jmenovatel</para>
        /// <para lang="en">Metadata of the selected indicator for the denominator</para>
        /// </param>
        /// <param name="variablePairs">
        /// <para lang="cs">Kategorie atributového členění pro čitatel a jmenovatel</para>
        /// <para lang="en">Attribute breakdown categories for numerator and denominator</para>
        /// </param>
        /// <param name="captionVersion">
        /// <para lang="cs">Stručná nebo rozšířená verze nadpisu?</para>
        /// <para lang="en">Brief or extended version of the caption?</para>
        /// </param>
        /// <param name="withAttributeCategories">
        /// <para lang="cs">Zobrazovat výpis atributových kategorií?</para>
        /// <para lang="en">Display attribute category list?</para>
        /// </param>
        /// <param name="withIdentifiers">
        /// <para lang="cs">Budou se zobrazovat také identifikační čísla?</para>
        /// <para lang="en">Will identification numbers also be displayed?</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ITargetVariableMetadata numeratorTargetVariableMetadata = null,
            ITargetVariableMetadata denominatorTargetVariableMetadata = null,
            VariablePairList variablePairs = null,
            CaptionVersion captionVersion = CaptionVersion.Brief,
            bool withAttributeCategories = false,
            bool withIdentifiers = true)
        {
            ControlOwner = controlOwner;

            this.numeratorTargetVariableMetadata = numeratorTargetVariableMetadata;
            this.denominatorTargetVariableMetadata = denominatorTargetVariableMetadata;
            this.variablePairs = variablePairs;

            this.captionVersion = captionVersion;
            this.withAttributeCategories = withAttributeCategories;
            this.withIdentifiers = withIdentifiers;

            SetControlDesign();
            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            #region Labels

            lblEmptyCaption.Text =
                labels.TryGetValue(key: nameof(lblEmptyCaption),
                out string lblEmptyCaptionText)
                    ? lblEmptyCaptionText
                    : String.Empty;

            lblExStateOrChangeLabel.Text =
                labels.TryGetValue(key: nameof(lblExStateOrChangeLabel),
                out string lblExStateOrChangeLabelText)
                    ? lblExStateOrChangeLabelText
                    : String.Empty;

            lblExLocalDensityLabel.Text =
                labels.TryGetValue(key: nameof(lblExLocalDensityLabel),
                out string lblExLocalDensityLabelText)
                    ? lblExLocalDensityLabelText
                    : String.Empty;

            lblExVersionLabel.Text =
                 labels.TryGetValue(key: nameof(lblExVersionLabel),
                 out string lblExVersionLabelText)
                     ? lblExVersionLabelText
                     : String.Empty;

            lblExDefinitionVariantLabel.Text =
                 labels.TryGetValue(key: nameof(lblExDefinitionVariantLabel),
                 out string lblExDefinitionVariantLabelText)
                     ? lblExDefinitionVariantLabelText
                     : String.Empty;

            lblExAreaDomainLabel.Text =
                 labels.TryGetValue(key: nameof(lblExAreaDomainLabel),
                 out string lblExAreaDomainLabelText)
                     ? lblExAreaDomainLabelText
                     : String.Empty;

            lblExPopulationLabel.Text =
                 labels.TryGetValue(key: nameof(lblExPopulationLabel),
                 out string lblExPopulationLabelText)
                     ? lblExPopulationLabelText
                     : String.Empty;

            lblExAttrAreaDomainLabel.Text =
                 labels.TryGetValue(key: nameof(lblExAttrAreaDomainLabel),
                 out string lblExAttrAreaDomainLabelText)
                     ? lblExAttrAreaDomainLabelText
                     : String.Empty;

            lblExAttrSubPopulationLabel.Text =
                 labels.TryGetValue(key: nameof(lblExAttrSubPopulationLabel),
                 out string lblExAttrSubPopulationLabelText)
                     ? lblExAttrSubPopulationLabelText
                     : String.Empty;

            lblWideStateOrChangeLabel.Text =
                 labels.TryGetValue(key: nameof(lblWideStateOrChangeLabel),
                 out string lblWideStateOrChangeLabelText)
                     ? lblWideStateOrChangeLabelText
                     : String.Empty;

            lblWideLocalDensityLabel.Text =
                 labels.TryGetValue(key: nameof(lblWideLocalDensityLabel),
                 out string lblWideLocalDensityLabelText)
                     ? lblWideLocalDensityLabelText
                     : String.Empty;

            lblWideVersionLabel.Text =
                 labels.TryGetValue(key: nameof(lblWideVersionLabel),
                 out string lblWideVersionLabelText)
                     ? lblWideVersionLabelText
                     : String.Empty;

            lblWideDefinitionVariantLabel.Text =
                 labels.TryGetValue(key: nameof(lblWideDefinitionVariantLabel),
                 out string lblWideDefinitionVariantLabelText)
                     ? lblWideDefinitionVariantLabelText
                     : String.Empty;

            lblWideAreaDomainLabel.Text =
                 labels.TryGetValue(key: nameof(lblWideAreaDomainLabel),
                 out string lblWideAreaDomainLabelText)
                     ? lblWideAreaDomainLabelText
                     : String.Empty;

            lblWidePopulationLabel.Text =
                 labels.TryGetValue(key: nameof(lblWidePopulationLabel),
                 out string lblWidePopulationLabelText)
                     ? lblWidePopulationLabelText
                     : String.Empty;

            lblWideAttrAreaDomainLabel.Text =
                 labels.TryGetValue(key: nameof(lblWideAttrAreaDomainLabel),
                 out string lblWideAttrAreaDomainLabelText)
                     ? lblWideAttrAreaDomainLabelText
                     : String.Empty;

            lblWideAttrSubPopulationLabel.Text =
                  labels.TryGetValue(key: nameof(lblWideAttrSubPopulationLabel),
                  out string lblWideAttrSubPopulationLabelText)
                      ? lblWideAttrSubPopulationLabelText
                      : String.Empty;

            lblWidePlaceHolderLabel.Text =
                  labels.TryGetValue(key: nameof(lblWidePlaceHolderLabel),
                  out string lblWidePlaceHolderLabelText)
                  ? lblWidePlaceHolderLabelText
                  : String.Empty;

            lblWidePlaceHolderValue.Text =
                  labels.TryGetValue(key: nameof(lblWidePlaceHolderValue),
                  out string lblWidePlaceHolderValueText)
                      ? lblWidePlaceHolderValueText
                      : String.Empty;

            lblNumeratorCaption.Text =
                   labels.TryGetValue(key: nameof(lblNumeratorCaption),
                   out string lblNumeratorCaptionText)
                       ? lblNumeratorCaptionText
                       : String.Empty;

            lblNumeratorCaptionPlaceHolder.Text =
                   labels.TryGetValue(key: nameof(lblNumeratorCaptionPlaceHolder),
                   out string lblNumeratorCaptionPlaceHolderText)
                       ? lblNumeratorCaptionPlaceHolderText
                       : String.Empty;

            lblDenominatorCaption.Text =
                   labels.TryGetValue(key: nameof(lblDenominatorCaption),
                   out string lblDenominatorCaptionText)
                       ? lblDenominatorCaptionText
                       : String.Empty;

            lblDenominatorCaptionPlaceHolder.Text =
                   labels.TryGetValue(key: nameof(lblDenominatorCaptionPlaceHolder),
                   out string lblDenominatorCaptionPlaceHolderText)
                       ? lblDenominatorCaptionPlaceHolderText
                       : String.Empty;

            lblNumStateOrChangeLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumStateOrChangeLabel),
                   out string lblNumStateOrChangeLabelText)
                       ? lblNumStateOrChangeLabelText
                       : String.Empty;

            lblNumLocalDensityLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumLocalDensityLabel),
                   out string lblNumLocalDensityLabelText)
                       ? lblNumLocalDensityLabelText
                       : String.Empty;

            lblNumVersionLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumVersionLabel),
                   out string lblNumVersionLabelText)
                       ? lblNumVersionLabelText
                       : String.Empty;

            lblNumDefinitionVariantLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumDefinitionVariantLabel),
                   out string lblNumDefinitionVariantLabelText)
                       ? lblNumDefinitionVariantLabelText
                       : String.Empty;

            lblNumAreaDomainLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumAreaDomainLabel),
                   out string lblNumAreaDomainLabelText)
                       ? lblNumAreaDomainLabelText
                       : String.Empty;

            lblNumPopulationLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumPopulationLabel),
                   out string lblNumPopulationLabelText)
                       ? lblNumPopulationLabelText
                       : String.Empty;

            lblNumAttrAreaDomainLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumAttrAreaDomainLabel),
                   out string lblNumAttrAreaDomainLabelText)
                       ? lblNumAttrAreaDomainLabelText
                       : String.Empty;

            lblNumAttrSubPopulationLabel.Text =
                   labels.TryGetValue(key: nameof(lblNumAttrSubPopulationLabel),
                   out string lblNumAttrSubPopulationLabelText)
                       ? lblNumAttrSubPopulationLabelText
                       : String.Empty;

            lblDenomStateOrChangeLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomStateOrChangeLabel),
                   out string lblDenomStateOrChangeLabelText)
                       ? lblDenomStateOrChangeLabelText
                       : String.Empty;

            lblDenomLocalDensityLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomLocalDensityLabel),
                   out string lblDenomLocalDensityLabelText)
                       ? lblDenomLocalDensityLabelText
                       : String.Empty;

            lblDenomVersionLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomVersionLabel),
                   out string lblDenomVersionLabelText)
                       ? lblDenomVersionLabelText
                       : String.Empty;

            lblDenomDefinitionVariantLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomDefinitionVariantLabel),
                   out string lblDenomDefinitionVariantLabelText)
                       ? lblDenomDefinitionVariantLabelText
                       : String.Empty;

            lblDenomAreaDomainLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomAreaDomainLabel),
                   out string lblDenomAreaDomainLabelText)
                       ? lblDenomAreaDomainLabelText
                       : String.Empty;

            lblDenomPopulationLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomPopulationLabel),
                   out string lblDenomPopulationLabelText)
                       ? lblDenomPopulationLabelText
                       : String.Empty;

            lblDenomAttrAreaDomainLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomAttrAreaDomainLabel),
                   out string lblDenomAttrAreaDomainLabelText)
                       ? lblDenomAttrAreaDomainLabelText
                       : String.Empty;

            lblDenomAttrSubPopulationLabel.Text =
                   labels.TryGetValue(key: nameof(lblDenomAttrSubPopulationLabel),
                   out string lblDenomAttrSubPopulationLabelText)
                       ? lblDenomAttrSubPopulationLabelText
                       : String.Empty;

            strAltogether =
                labels.TryGetValue(key: nameof(strAltogether),
                out string strAltogetherText)
                   ? strAltogetherText
                   : String.Empty;

            #endregion Labels

            #region Values

            switch (LanguageVersion)
            {
                case LanguageVersion.National:

                    #region STRUČNÝ NADPIS PRO ÚHRN

                    if
                    (
                        (NumeratorTargetVariableMetadata != null) &&
                        (DenominatorTargetVariableMetadata == null))
                    {
                        string strId =
                            $"({NumeratorTargetVariableMetadata.TargetVariableId})";
                        string strIndicator =
                            NumeratorTargetVariableMetadata.IndicatorLabelCs;
                        string strUnit =
                            (NumeratorTargetVariableMetadata.MetadataElements[MetadataElementType.Unit] == null) ?
                            String.Empty :
                                NumeratorTargetVariableMetadata
                                    .MetadataElements[MetadataElementType.Unit].LabelCs;

                        lblBriefCaption.Text =
                            String.IsNullOrEmpty(value: strIndicator) ?
                                lblEmptyCaption.Text :
                                String.IsNullOrEmpty(value: strUnit) ?
                                    strIndicator :
                                    WithIdentifiers ?
                                        $"{strId} {strIndicator} [{strUnit}]" :
                                        $"{strIndicator} [{strUnit}]";
                    }

                    #endregion STRUČNÝ NADPIS PRO ÚHRN

                    #region STRUČNÝ NADPIS PRO PODÍL

                    else if
                    (
                        (NumeratorTargetVariableMetadata != null) &&
                        (DenominatorTargetVariableMetadata != null))
                    {
                        string strId = String.Concat(
                            $"({NumeratorTargetVariableMetadata.TargetVariableId}/",
                            $"{DenominatorTargetVariableMetadata.TargetVariableId})");

                        string strIndicator = String.Concat(
                             $"{NumeratorTargetVariableMetadata.IndicatorLabelCs} / ",
                             $"{DenominatorTargetVariableMetadata.IndicatorLabelCs}");

                        string strNumUnit =
                            (NumeratorTargetVariableMetadata.MetadataElements[MetadataElementType.Unit] == null) ?
                            String.Empty :
                                NumeratorTargetVariableMetadata
                                    .MetadataElements[MetadataElementType.Unit].LabelCs;

                        string strDenomUnit =
                           (DenominatorTargetVariableMetadata.MetadataElements[MetadataElementType.Unit] == null) ?
                           String.Empty :
                               DenominatorTargetVariableMetadata
                                   .MetadataElements[MetadataElementType.Unit].LabelCs;

                        string strUnit = (strNumUnit == strDenomUnit) ? "%" :
                            $"{strNumUnit}/{strDenomUnit}";

                        lblBriefCaption.Text =
                            String.IsNullOrEmpty(strIndicator) ?
                                lblEmptyCaption.Text :
                                String.IsNullOrEmpty(strUnit) ?
                                    strIndicator :
                                    WithIdentifiers ?
                                        $"{strId} {strIndicator} [{strUnit}]" :
                                        $"{strIndicator} [{strUnit}]";
                    }

                    else
                    {
                        lblBriefCaption.Text = String.Empty;
                    }

                    #endregion STRUČNÝ NADPIS PRO PODÍL

                    #region ROZŠÍŘENÝ NADPIS PRO ÚHRN

                    lblExStateOrChangeValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.StateOrChangeLabelCs :
                            String.Empty;

                    lblExLocalDensityValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.LocalDensityLabelCs :
                            String.Empty;

                    lblExVersionValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.VersionLabelCs :
                            String.Empty;

                    lblExDefinitionVariantValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.DefinitionVariantLabelCs :
                            String.Empty;

                    lblExAreaDomainValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.AreaDomainLabelCs :
                            String.Empty;

                    lblExPopulationValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.PopulationLabelCs :
                            String.Empty;

                    if (variablePairs != null)
                    {
                        if (variablePairs.Numerator != null)
                        {
                            IEnumerable<AreaDomain> areaDomains =
                                variablePairs.Numerator.Items
                                .Where(a => a.AreaDomain != null)
                                .Select(a => a.AreaDomain);

                            lblExAttrAreaDomainValue.Text =
                                (areaDomains.Any()) ?
                                    (!String.IsNullOrEmpty(value: areaDomains.First().LabelCs)) ?
                                        areaDomains.First().LabelCs :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblExAttrAreaDomainValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblExAttrAreaDomainValue.Text = String.Empty;
                    }

                    if (variablePairs != null)
                    {
                        if (variablePairs.Numerator != null)
                        {
                            IEnumerable<SubPopulation> subPopulations =
                                variablePairs.Numerator.Items
                                .Where(a => a.SubPopulation != null)
                                .Select(a => a.SubPopulation);

                            lblExAttrSubPopulationValue.Text =
                                (subPopulations.Any()) ?
                                    (!String.IsNullOrEmpty(value: subPopulations.First().LabelCs)) ?
                                        subPopulations.First().LabelCs :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblExAttrSubPopulationValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblExAttrSubPopulationValue.Text = String.Empty;
                    }

                    #endregion ROZŠÍŘENÝ NADPIS PRO ÚHRN

                    #region ŠIROKÝ NADPIS PRO ÚHRN

                    lblWideStateOrChangeValue.Text = lblExStateOrChangeValue.Text;
                    lblWideLocalDensityValue.Text = lblExLocalDensityValue.Text;
                    lblWideVersionValue.Text = lblExVersionValue.Text;
                    lblWideDefinitionVariantValue.Text = lblExDefinitionVariantValue.Text;
                    lblWideAreaDomainValue.Text = lblExAreaDomainValue.Text;
                    lblWidePopulationValue.Text = lblExPopulationValue.Text;
                    lblWideAttrAreaDomainValue.Text = lblExAttrAreaDomainValue.Text;
                    lblWideAttrSubPopulationValue.Text = lblExAttrSubPopulationValue.Text;

                    #endregion ŠIROKÝ NADPIS PRO ÚHRN

                    #region ROZŠÍŘENÝ NADPIS PRO PODÍL

                    lblNumStateOrChangeValue.Text = lblExStateOrChangeValue.Text;
                    lblNumLocalDensityValue.Text = lblExLocalDensityValue.Text;
                    lblNumVersionValue.Text = lblExVersionValue.Text;
                    lblNumDefinitionVariantValue.Text = lblExDefinitionVariantValue.Text;
                    lblNumAreaDomainValue.Text = lblExAreaDomainValue.Text;
                    lblNumPopulationValue.Text = lblExPopulationValue.Text;
                    lblNumAttrAreaDomainValue.Text = lblExAttrAreaDomainValue.Text;
                    lblNumAttrSubPopulationValue.Text = lblExAttrSubPopulationValue.Text;

                    lblDenomStateOrChangeValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.StateOrChangeLabelCs :
                            String.Empty;

                    lblDenomLocalDensityValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.LocalDensityLabelCs :
                            String.Empty;

                    lblDenomVersionValue.Text =
                          (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.VersionLabelCs :
                            String.Empty;

                    lblDenomDefinitionVariantValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.DefinitionVariantLabelCs :
                            String.Empty;

                    lblDenomAreaDomainValue.Text =
                        (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.AreaDomainLabelCs :
                            String.Empty;

                    lblDenomPopulationValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.PopulationLabelCs :
                            String.Empty;

                    if (variablePairs != null)
                    {
                        if (variablePairs.Denominator != null)
                        {
                            IEnumerable<AreaDomain> areaDomains =
                                variablePairs.Denominator.Items
                                .Where(a => a.AreaDomain != null)
                                .Select(a => a.AreaDomain);

                            lblDenomAttrAreaDomainValue.Text =
                                (areaDomains.Any()) ?
                                    (!String.IsNullOrEmpty(value: areaDomains.First().LabelCs)) ?
                                        areaDomains.First().LabelCs :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblDenomAttrAreaDomainValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblDenomAttrAreaDomainValue.Text = String.Empty;
                    }

                    if (variablePairs != null)
                    {
                        if (variablePairs.Denominator != null)
                        {
                            IEnumerable<SubPopulation> subPopulations =
                                variablePairs.Denominator.Items
                                .Where(a => a.SubPopulation != null)
                                .Select(a => a.SubPopulation);

                            lblDenomAttrSubPopulationValue.Text =
                                (subPopulations.Any()) ?
                                    (!String.IsNullOrEmpty(value: subPopulations.First().LabelCs)) ?
                                        subPopulations.First().LabelCs :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblDenomAttrSubPopulationValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblDenomAttrSubPopulationValue.Text = String.Empty;
                    }

                    #endregion ROZŠÍŘENÝ NADPIS PRO PODÍL

                    break;

                case LanguageVersion.International:

                    #region STRUČNÝ NADPIS PRO ÚHRN

                    if (
                        (NumeratorTargetVariableMetadata != null) &&
                        (DenominatorTargetVariableMetadata == null))
                    {
                        string strId =
                            $"({NumeratorTargetVariableMetadata.TargetVariableId})";
                        string strIndicator =
                            NumeratorTargetVariableMetadata.IndicatorLabelEn;
                        string strUnit =
                            (NumeratorTargetVariableMetadata.MetadataElements[MetadataElementType.Unit] == null) ?
                            String.Empty :
                                NumeratorTargetVariableMetadata
                                    .MetadataElements[MetadataElementType.Unit].LabelEn;

                        lblBriefCaption.Text =
                            String.IsNullOrEmpty(strIndicator) ?
                                lblEmptyCaption.Text :
                                String.IsNullOrEmpty(strUnit) ?
                                    strIndicator :
                                    WithIdentifiers ?
                                        $"{strId} {strIndicator} [{strUnit}]" :
                                        $"{strIndicator} [{strUnit}]";
                    }

                    #endregion STRUČNÝ NADPIS PRO ÚHRN

                    #region STRUČNÝ NADPIS PRO PODÍL

                    else if
                    (
                        (NumeratorTargetVariableMetadata != null) &&
                        (DenominatorTargetVariableMetadata != null))
                    {
                        string strId = String.Concat(
                            $"({NumeratorTargetVariableMetadata.TargetVariableId}/",
                            $"{DenominatorTargetVariableMetadata.TargetVariableId})");

                        string strIndicator = String.Concat(
                             $"{NumeratorTargetVariableMetadata.IndicatorLabelEn} / ",
                             $"{DenominatorTargetVariableMetadata.IndicatorLabelEn}");

                        string strNumUnit =
                           (NumeratorTargetVariableMetadata.MetadataElements[MetadataElementType.Unit] == null) ?
                           String.Empty :
                               NumeratorTargetVariableMetadata
                                   .MetadataElements[MetadataElementType.Unit].LabelEn;

                        string strDenomUnit =
                           (DenominatorTargetVariableMetadata.MetadataElements[MetadataElementType.Unit] == null) ?
                           String.Empty :
                               DenominatorTargetVariableMetadata
                                   .MetadataElements[MetadataElementType.Unit].LabelEn;

                        string strUnit = $"{strNumUnit} / {strDenomUnit}";

                        lblBriefCaption.Text =
                            String.IsNullOrEmpty(strIndicator) ?
                                lblEmptyCaption.Text :
                                String.IsNullOrEmpty(strUnit) ?
                                    strIndicator :
                                    WithIdentifiers ?
                                        $"{strId} {strIndicator} [{strUnit}]" :
                                        $"{strIndicator} [{strUnit}]";
                    }

                    else
                    {
                        lblBriefCaption.Text = String.Empty;
                    }

                    #endregion STRUČNÝ NADPIS PRO PODÍL

                    #region ROZŠÍŘENÝ NADPIS PRO ÚHRN

                    lblExStateOrChangeValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.StateOrChangeLabelEn :
                            String.Empty;

                    lblExLocalDensityValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.LocalDensityLabelEn :
                            String.Empty;

                    lblExVersionValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.VersionLabelEn :
                            String.Empty;

                    lblExDefinitionVariantValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.DefinitionVariantLabelEn :
                            String.Empty;

                    lblExAreaDomainValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.AreaDomainLabelEn :
                            String.Empty;

                    lblExPopulationValue.Text =
                        (NumeratorTargetVariableMetadata != null) ?
                            NumeratorTargetVariableMetadata.PopulationLabelEn :
                            String.Empty;

                    if (variablePairs != null)
                    {
                        if (variablePairs.Numerator != null)
                        {
                            IEnumerable<AreaDomain> areaDomains =
                                variablePairs.Numerator.Items
                                .Where(a => a.AreaDomain != null)
                                .Select(a => a.AreaDomain);

                            lblExAttrAreaDomainValue.Text =
                                (areaDomains.Any()) ?
                                    (!String.IsNullOrEmpty(value: areaDomains.First().LabelEn)) ?
                                        areaDomains.First().LabelEn :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblExAttrAreaDomainValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblExAttrAreaDomainValue.Text = String.Empty;
                    }

                    if (variablePairs != null)
                    {
                        if (variablePairs.Numerator != null)
                        {
                            IEnumerable<SubPopulation> subPopulations =
                                variablePairs.Numerator.Items
                                .Where(a => a.SubPopulation != null)
                                .Select(a => a.SubPopulation);

                            lblExAttrSubPopulationValue.Text =
                                (subPopulations.Any()) ?
                                    (!String.IsNullOrEmpty(value: subPopulations.First().LabelEn)) ?
                                        subPopulations.First().LabelEn :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblExAttrSubPopulationValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblExAttrSubPopulationValue.Text = String.Empty;
                    }

                    #endregion ROZŠÍŘENÝ NADPIS PRO ÚHRN

                    #region ŠIROKÝ NADPIS PRO ÚHRN

                    lblWideStateOrChangeValue.Text = lblExStateOrChangeValue.Text;
                    lblWideLocalDensityValue.Text = lblExLocalDensityValue.Text;
                    lblWideVersionValue.Text = lblExVersionValue.Text;
                    lblWideDefinitionVariantValue.Text = lblExDefinitionVariantValue.Text;
                    lblWideAreaDomainValue.Text = lblExAreaDomainValue.Text;
                    lblWidePopulationValue.Text = lblExPopulationValue.Text;
                    lblWideAttrAreaDomainValue.Text = lblExAttrAreaDomainValue.Text;
                    lblWideAttrSubPopulationValue.Text = lblExAttrSubPopulationValue.Text;

                    #endregion ŠIROKÝ NADPIS PRO ÚHRN

                    #region ROZŠÍŘENÝ NADPIS PRO PODÍL

                    lblNumStateOrChangeValue.Text = lblExStateOrChangeValue.Text;
                    lblNumLocalDensityValue.Text = lblExLocalDensityValue.Text;
                    lblNumVersionValue.Text = lblExVersionValue.Text;
                    lblNumDefinitionVariantValue.Text = lblExDefinitionVariantValue.Text;
                    lblNumAreaDomainValue.Text = lblExAreaDomainValue.Text;
                    lblNumPopulationValue.Text = lblExPopulationValue.Text;
                    lblNumAttrAreaDomainValue.Text = lblExAttrAreaDomainValue.Text;
                    lblNumAttrSubPopulationValue.Text = lblExAttrSubPopulationValue.Text;

                    lblDenomStateOrChangeValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.StateOrChangeLabelEn :
                            String.Empty;

                    lblDenomLocalDensityValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.LocalDensityLabelEn :
                            String.Empty;

                    lblDenomVersionValue.Text =
                          (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.VersionLabelEn :
                            String.Empty;

                    lblDenomDefinitionVariantValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.DefinitionVariantLabelEn :
                            String.Empty;

                    lblDenomAreaDomainValue.Text =
                        (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.AreaDomainLabelEn :
                            String.Empty;

                    lblDenomPopulationValue.Text =
                         (DenominatorTargetVariableMetadata != null) ?
                            DenominatorTargetVariableMetadata.PopulationLabelEn :
                            String.Empty;

                    if (variablePairs != null)
                    {
                        if (variablePairs.Denominator != null)
                        {
                            IEnumerable<AreaDomain> areaDomains =
                                variablePairs.Denominator.Items
                                .Where(a => a.AreaDomain != null)
                                .Select(a => a.AreaDomain);

                            lblDenomAttrAreaDomainValue.Text =
                                (areaDomains.Any()) ?
                                    (!String.IsNullOrEmpty(value: areaDomains.First().LabelEn)) ?
                                        areaDomains.First().LabelEn :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblDenomAttrAreaDomainValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblDenomAttrAreaDomainValue.Text = String.Empty;
                    }

                    if (variablePairs != null)
                    {
                        if (variablePairs.Denominator != null)
                        {
                            IEnumerable<SubPopulation> subPopulations =
                                variablePairs.Denominator.Items
                                .Where(a => a.SubPopulation != null)
                                .Select(a => a.SubPopulation);

                            lblDenomAttrSubPopulationValue.Text =
                                (subPopulations.Any()) ?
                                    (!String.IsNullOrEmpty(value: subPopulations.First().LabelEn)) ?
                                        subPopulations.First().LabelEn :
                                    strAltogether :
                                strAltogether;
                        }
                        else
                        {
                            lblDenomAttrSubPopulationValue.Text = String.Empty;
                        }
                    }
                    else
                    {
                        lblDenomAttrSubPopulationValue.Text = String.Empty;
                    }

                    #endregion ROZŠÍŘENÝ NADPIS PRO PODÍL

                    break;

                default:

                    #region PRÁZDNÝ NADPIS

                    lblEmptyCaption.Text = lblEmptyCaption.Name;

                    #endregion PRÁZDNÝ NADPIS

                    #region STRUČNÝ NADPIS

                    lblBriefCaption.Text = lblBriefCaption.Name;

                    #endregion STRUČNÝ NADPIS

                    #region ROZŠÍŘENÝ NADPIS PRO ÚHRN

                    lblExStateOrChangeLabel.Text = lblExStateOrChangeLabel.Name;
                    lblExStateOrChangeValue.Text = lblExStateOrChangeValue.Name;
                    lblExLocalDensityLabel.Text = lblExLocalDensityLabel.Name;
                    lblExLocalDensityValue.Text = lblExLocalDensityValue.Name;
                    lblExVersionLabel.Text = lblExVersionLabel.Name;
                    lblExVersionValue.Text = lblExVersionValue.Name;
                    lblExDefinitionVariantLabel.Text = lblExDefinitionVariantLabel.Name;
                    lblExDefinitionVariantValue.Text = lblExDefinitionVariantValue.Name;
                    lblExAreaDomainLabel.Text = lblExAreaDomainLabel.Name;
                    lblExAreaDomainValue.Text = lblExAreaDomainValue.Name;
                    lblExPopulationLabel.Text = lblExPopulationLabel.Name;
                    lblExPopulationValue.Text = lblExPopulationValue.Name;
                    lblExAttrAreaDomainLabel.Text = lblExAttrAreaDomainLabel.Name;
                    lblExAttrAreaDomainValue.Text = lblExAttrAreaDomainValue.Name;
                    lblExAttrSubPopulationLabel.Text = lblExAttrSubPopulationLabel.Name;
                    lblExAttrSubPopulationValue.Text = lblExAttrSubPopulationValue.Name;

                    #endregion ROZŠÍŘENÝ NADPIS PRO ÚHRN

                    #region ŠIROKÝ NADPIS PRO ÚHRN

                    lblWideStateOrChangeLabel.Text = lblWideStateOrChangeLabel.Name;
                    lblWideStateOrChangeValue.Text = lblWideStateOrChangeValue.Name;
                    lblWideLocalDensityLabel.Text = lblWideLocalDensityLabel.Name;
                    lblWideLocalDensityValue.Text = lblWideLocalDensityValue.Name;
                    lblWideVersionLabel.Text = lblWideVersionLabel.Name;
                    lblWideVersionValue.Text = lblWideVersionValue.Name;
                    lblWideDefinitionVariantLabel.Text = lblWideDefinitionVariantLabel.Name;
                    lblWideDefinitionVariantValue.Text = lblWideDefinitionVariantValue.Name;
                    lblWideAreaDomainLabel.Text = lblWideAreaDomainLabel.Name;
                    lblWideAreaDomainValue.Text = lblWideAreaDomainValue.Name;
                    lblWidePopulationLabel.Text = lblWidePopulationLabel.Name;
                    lblWidePopulationValue.Text = lblWidePopulationValue.Name;
                    lblWideAttrAreaDomainLabel.Text = lblWideAttrAreaDomainLabel.Name;
                    lblWideAttrAreaDomainValue.Text = lblWideAttrAreaDomainValue.Name;
                    lblWideAttrSubPopulationLabel.Text = lblWideAttrSubPopulationLabel.Name;
                    lblWideAttrSubPopulationValue.Text = lblWideAttrSubPopulationValue.Name;

                    #endregion ŠIROKÝ NADPIS PRO ÚHRN

                    #region ROZŠÍŘENÝ NADPIS PRO PODÍL

                    lblNumeratorCaption.Text = lblNumeratorCaption.Name;
                    lblNumeratorCaptionPlaceHolder.Text = lblNumeratorCaptionPlaceHolder.Name;
                    lblNumStateOrChangeLabel.Text = lblNumStateOrChangeLabel.Name;
                    lblNumStateOrChangeValue.Text = lblNumStateOrChangeValue.Name;
                    lblNumLocalDensityLabel.Text = lblNumLocalDensityLabel.Name;
                    lblNumLocalDensityValue.Text = lblNumLocalDensityValue.Name;
                    lblNumVersionLabel.Text = lblNumVersionLabel.Name;
                    lblNumVersionValue.Text = lblNumVersionValue.Name;
                    lblNumDefinitionVariantLabel.Text = lblNumDefinitionVariantLabel.Name;
                    lblNumDefinitionVariantValue.Text = lblNumDefinitionVariantValue.Name;
                    lblNumAreaDomainLabel.Text = lblNumAreaDomainLabel.Name;
                    lblNumAreaDomainValue.Text = lblNumAreaDomainValue.Name;
                    lblNumPopulationLabel.Text = lblNumPopulationLabel.Name;
                    lblNumPopulationValue.Text = lblNumPopulationValue.Name;
                    lblNumAttrAreaDomainLabel.Text = lblNumAttrAreaDomainLabel.Name;
                    lblNumAttrAreaDomainValue.Text = lblNumAttrAreaDomainValue.Name;
                    lblNumAttrSubPopulationLabel.Text = lblNumAttrSubPopulationLabel.Name;
                    lblNumAttrSubPopulationValue.Text = lblNumAttrSubPopulationValue.Name;
                    lblDenominatorCaption.Text = lblDenominatorCaption.Name;
                    lblDenominatorCaptionPlaceHolder.Text = lblDenominatorCaptionPlaceHolder.Name;
                    lblDenomStateOrChangeLabel.Text = lblDenomStateOrChangeLabel.Name;
                    lblDenomStateOrChangeValue.Text = lblDenomStateOrChangeValue.Name;
                    lblDenomLocalDensityLabel.Text = lblDenomLocalDensityLabel.Name;
                    lblDenomLocalDensityValue.Text = lblDenomLocalDensityValue.Name;
                    lblDenomVersionLabel.Text = lblDenomVersionLabel.Name;
                    lblDenomVersionValue.Text = lblDenomVersionValue.Name;
                    lblDenomDefinitionVariantLabel.Text = lblDenomDefinitionVariantLabel.Name;
                    lblDenomDefinitionVariantValue.Text = lblDenomDefinitionVariantValue.Name;
                    lblDenomAreaDomainLabel.Text = lblDenomAreaDomainLabel.Name;
                    lblDenomAreaDomainValue.Text = lblDenomAreaDomainValue.Name;
                    lblDenomPopulationLabel.Text = lblDenomPopulationLabel.Name;
                    lblDenomPopulationValue.Text = lblDenomPopulationValue.Name;
                    lblDenomAttrAreaDomainLabel.Text = lblDenomAttrAreaDomainLabel.Name;
                    lblDenomAttrAreaDomainValue.Text = lblDenomAttrAreaDomainValue.Name;
                    lblDenomAttrSubPopulationLabel.Text = lblDenomAttrSubPopulationLabel.Name;
                    lblDenomAttrSubPopulationValue.Text = lblDenomAttrSubPopulationValue.Name;

                    #endregion ROZŠÍŘENÝ NADPIS PRO PODÍL

                    break;
            }

            #endregion Values
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Nastaví vzhled komponenty</para>
        /// <para lang="en">Sets the appearance of the component</para>
        /// </summary>
        private void SetControlDesign()
        {
            lblExAttrAreaDomainLabel.Visible = withAttributeCategories;
            lblExAttrAreaDomainValue.Visible = withAttributeCategories;
            lblExAttrSubPopulationLabel.Visible = withAttributeCategories;
            lblExAttrSubPopulationValue.Visible = withAttributeCategories;
            lblWideAttrAreaDomainLabel.Visible = withAttributeCategories;
            lblWideAttrAreaDomainValue.Visible = withAttributeCategories;
            lblWideAttrSubPopulationLabel.Visible = withAttributeCategories;
            lblWideAttrSubPopulationValue.Visible = withAttributeCategories;
            lblNumAttrAreaDomainLabel.Visible = withAttributeCategories;
            lblNumAttrAreaDomainValue.Visible = withAttributeCategories;
            lblNumAttrSubPopulationLabel.Visible = withAttributeCategories;
            lblNumAttrSubPopulationValue.Visible = withAttributeCategories;
            lblDenomAttrAreaDomainLabel.Visible = withAttributeCategories;
            lblDenomAttrAreaDomainValue.Visible = withAttributeCategories;
            lblDenomAttrSubPopulationLabel.Visible = withAttributeCategories;
            lblDenomAttrSubPopulationValue.Visible = withAttributeCategories;

            pnlEmpty.Height = 25;
            pnlBrief.Height = 25;
            pnlWide.Height = 75;
            pnlExtended.Height = withAttributeCategories ? 120 : 90;
            pnlRatio.Height = withAttributeCategories ? 135 : 105;

            if ((NumeratorTargetVariableMetadata == null) &&
                (DenominatorTargetVariableMetadata == null))
            {
                // Určena není cílová proměnná pro čitatel
                // Určena není cílová proměnná pro jmenovatel
                // PRÁZDNÝ NADPIS
                Height = pnlEmpty.Height;
                pnlEmpty.Visible = true;
                pnlBrief.Visible = false;
                pnlExtended.Visible = false;
                pnlWide.Visible = false;
                pnlRatio.Visible = false;
                pnlEmpty.Dock = DockStyle.Fill;
                pnlBrief.Dock = DockStyle.None;
                pnlExtended.Dock = DockStyle.None;
                pnlWide.Dock = DockStyle.None;
                pnlRatio.Dock = DockStyle.None;
            }

            else if
                ((NumeratorTargetVariableMetadata != null) &&
                (DenominatorTargetVariableMetadata == null))
            {
                // Určena je cílová proměnná pro čitatel
                // Určena není cílová proměnná pro jmenovatel

                switch (CaptionVersion)
                {
                    // STRUČNÝ NADPIS PRO INDIKÁTOR
                    case CaptionVersion.Brief:
                        Height = pnlBrief.Height;
                        pnlEmpty.Visible = false;
                        pnlBrief.Visible = true;
                        pnlExtended.Visible = false;
                        pnlWide.Visible = false;
                        pnlRatio.Visible = false;
                        pnlEmpty.Dock = DockStyle.None;
                        pnlBrief.Dock = DockStyle.Fill;
                        pnlExtended.Dock = DockStyle.None;
                        pnlWide.Dock = DockStyle.None;
                        pnlRatio.Dock = DockStyle.None;
                        break;

                    // ROZŠÍŘENÝ NADPIS PRO INDIKÁTOR (na šířku)
                    case CaptionVersion.Wide:
                        Height = pnlBrief.Height + pnlWide.Height;
                        pnlEmpty.Visible = false;
                        pnlBrief.Visible = true;
                        pnlExtended.Visible = false;
                        pnlWide.Visible = true;
                        pnlRatio.Visible = false;
                        pnlEmpty.Dock = DockStyle.None;
                        pnlBrief.Dock = DockStyle.Top;
                        pnlExtended.Dock = DockStyle.None;
                        pnlWide.Dock = DockStyle.Fill;
                        pnlRatio.Dock = DockStyle.None;
                        break;

                    // ROZŠÍŘENÝ NADPIS PRO INDIKÁTOR (na výšku)
                    case CaptionVersion.Extended:
                        Height = pnlBrief.Height + pnlExtended.Height;
                        pnlEmpty.Visible = false;
                        pnlBrief.Visible = true;
                        pnlExtended.Visible = true;
                        pnlWide.Visible = false;
                        pnlRatio.Visible = false;
                        pnlEmpty.Dock = DockStyle.None;
                        pnlBrief.Dock = DockStyle.Top;
                        pnlExtended.Dock = DockStyle.Fill;
                        pnlWide.Dock = DockStyle.None;
                        pnlRatio.Dock = DockStyle.None;
                        break;

                    default:
                        Height = pnlBrief.Height;
                        pnlEmpty.Visible = false;
                        pnlBrief.Visible = true;
                        pnlExtended.Visible = false;
                        pnlWide.Visible = false;
                        pnlRatio.Visible = false;
                        pnlEmpty.Dock = DockStyle.None;
                        pnlBrief.Dock = DockStyle.Fill;
                        pnlExtended.Dock = DockStyle.None;
                        pnlWide.Dock = DockStyle.None;
                        pnlRatio.Dock = DockStyle.None;
                        break;
                }
            }

            else if
                ((NumeratorTargetVariableMetadata != null) &&
                (DenominatorTargetVariableMetadata != null))
            {
                // Určena je cílová proměnná pro čitatel
                // Určena je cílová proměnná pro jmenovatel

                switch (CaptionVersion)
                {
                    // STRUČNÝ NADPIS PRO PODÍL
                    case CaptionVersion.Brief:
                        Height = pnlBrief.Height;
                        pnlEmpty.Visible = false;
                        pnlBrief.Visible = true;
                        pnlExtended.Visible = false;
                        pnlWide.Visible = false;
                        pnlRatio.Visible = false;
                        pnlEmpty.Dock = DockStyle.None;
                        pnlBrief.Dock = DockStyle.Fill;
                        pnlExtended.Dock = DockStyle.None;
                        pnlWide.Dock = DockStyle.None;
                        pnlRatio.Dock = DockStyle.None;
                        break;

                    // ROZŠÍŘENÝ NADPIS PRO PODÍL
                    // rozšiřovat lze pouze na výšku
                    // do šířky jdou sloupečky pro čitatel a jmenovatel
                    case CaptionVersion.Wide:
                    case CaptionVersion.Extended:
                        Height = pnlBrief.Height + pnlRatio.Height;
                        pnlEmpty.Visible = false;
                        pnlBrief.Visible = true;
                        pnlExtended.Visible = false;
                        pnlWide.Visible = false;
                        pnlRatio.Visible = true;
                        pnlEmpty.Dock = DockStyle.None;
                        pnlBrief.Dock = DockStyle.Top;
                        pnlExtended.Dock = DockStyle.None;
                        pnlWide.Dock = DockStyle.None;
                        pnlRatio.Dock = DockStyle.Fill;
                        break;

                    default:
                        Height = pnlBrief.Height;
                        pnlEmpty.Visible = false;
                        pnlBrief.Visible = true;
                        pnlExtended.Visible = false;
                        pnlWide.Visible = false;
                        pnlRatio.Visible = false;
                        pnlEmpty.Dock = DockStyle.None;
                        pnlBrief.Dock = DockStyle.Fill;
                        pnlExtended.Dock = DockStyle.None;
                        pnlWide.Dock = DockStyle.None;
                        pnlRatio.Dock = DockStyle.None;
                        break;
                }
            }
            else
            {
                // Určena není cílová proměnná pro čitatel
                // Určena je cílová proměnná pro jmenovatel
                // NEPOVOLENÁ KOMBINACE
                Height = pnlEmpty.Height;
                pnlEmpty.Visible = true;
                pnlBrief.Visible = false;
                pnlExtended.Visible = false;
                pnlWide.Visible = false;
                pnlRatio.Visible = false;
                pnlEmpty.Dock = DockStyle.Fill;
                pnlBrief.Dock = DockStyle.None;
                pnlExtended.Dock = DockStyle.None;
                pnlWide.Dock = DockStyle.None;
                pnlRatio.Dock = DockStyle.None;
            }

            InitializeLabels();
        }

        #endregion Methods

    }

    #region enum CaptionVersion

    /// <summary>
    /// <para lang="cs">Verze nadpisu</para>
    /// <para lang="en">Version of the caption</para>
    /// </summary>
    internal enum CaptionVersion
    {
        /// <summary>
        /// <para lang="cs">Stručná verze nadpisu</para>
        /// <para lang="en">Brief version of the caption</para>
        /// </summary>
        Brief = 100,

        /// <summary>
        /// <para lang="cs">Podrobná verze nadpisu (na šířku)</para>
        /// <para lang="en">Extended version of the caption (width)</para>
        /// </summary>
        Wide = 200,

        /// <summary>
        /// <para lang="cs">Podrobná verze nadpisu (na výšku)</para>
        /// <para lang="en">Extended version of the caption (height)</para>
        /// </summary>
        Extended = 300
    }

    #endregion enum CaptionVersion

}