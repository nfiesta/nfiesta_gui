﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">
    /// Formulář pro konfiguraci regresních odhadů úhrnů:
    /// Výběr modelu a typu parametrizační oblasti
    /// </para>
    /// <para lang="en">
    /// Form for configuring regression estimates of totals:
    /// Selecting model and parametrization area type
    /// </para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormGRegModelParamAreaType
        : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Kombinace modelu, sigma a typu parametrizační oblasti</para>
        /// <para lang="en">Combination of model, sigma and parametrization area type</para>
        /// </summary>
        private TFnApiGetWModelsSigmaParamTypesList modelsSigmaParamTypes;

        /// <summary>
        /// <para lang="cs">Seznam RadioButtons pro zobrazené kombinace modulu, sigma a typu parametrizační oblasti</para>
        /// <para lang="en">List of RadioButtons for displayed combination of model, sigma na parametrization area type</para>
        /// </summary>
        private List<RadioButton> radioButtons;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        private string colModel = String.Empty;
        private string colSigma = String.Empty;
        private string colParamAreaType = String.Empty;
        private string colNumberOfCellsCovered = String.Empty;
        private string colAllCellsIncluded = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormGRegModelParamAreaType(
            Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Kombinace modelu, sigma a typu parametrizační oblasti</para>
        /// <para lang="en">Combination of model, sigma and parametrization area type</para>
        /// </summary>
        public TFnApiGetWModelsSigmaParamTypesList ModelsSigmaParamTypes
        {
            get
            {
                return modelsSigmaParamTypes;
            }
            set
            {
                modelsSigmaParamTypes = value;
                InitializeTableLayoutPanel();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná kombinace modelu, sigma a typu parametrizační oblasti (read-only)</para>
        /// <para lang="en">Selected combination of working model, sigma and parametrization area type (read-only)</para>
        /// </summary>
        public TFnApiGetWModelsSigmaParamTypes SelectedModelSigmaParamType
        {
            get
            {
                RadioButton rdoSelected = radioButtons
                    .Where(a => a.Checked)
                    .FirstOrDefault<RadioButton>();

                if (rdoSelected == null)
                {
                    return null;
                }

                if ((rdoSelected.Tag == null) ||
                    (rdoSelected.Tag is not TFnApiGetWModelsSigmaParamTypes))
                {
                    return null;
                }

                return
                    (TFnApiGetWModelsSigmaParamTypes)rdoSelected.Tag;
            }
        }

        /// <summary>
        /// <para lang="cs">Vynucení syntetického odhadu</para>
        /// <para lang="en">Force synthetic estimate</para>
        /// </summary>
        public bool ForceSynthetic
        {
            get
            {
                return rdoForceSyntheticYes.Checked;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegModelParamAreaType),           "Výběr modelu a typu parametrizační oblasti" },
                        { nameof(btnCancel),                            "Zrušit" },
                        { nameof(btnOK),                                "Další" },
                        { nameof(grpForceSynthetic),                    "Vynutit syntetický odhad?" },
                        { nameof(rdoForceSyntheticYes),                 "ano" },
                        { nameof(rdoForceSyntheticNo),                  "ne" },
                        { nameof(lblNoModelSigmaParamAreaCombination),  String.Concat(
                            "Neexistuje žádná kombinace modelu, sigma a typu parametrizační oblasti, která by umožňovala konfiguraci všech požadovaných GREG-odhadů. $0",
                            "Prosím, definujte vhodné modely a parametrizační oblasti pro všechny požadované výpočetní buňky nebo omezte množinu výpočetních puněk pro konfiguraci GREG-odhadů.") },
                        { nameof(colModel),                             "Model" },
                        { nameof(colSigma),                             "Sigma" },
                        { nameof(colParamAreaType),                     "Typ parametrizační oblasti" },
                        { nameof(colNumberOfCellsCovered),              "Počet zahrnutých buněk" },
                        { nameof(colAllCellsIncluded),                  "Všechny buňky zahrnuté" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(key: nameof(FormGRegModelParamAreaType),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegModelParamAreaType),           "Selection of model and parametrization area type" },
                        { nameof(btnCancel),                            "Cancel" },
                        { nameof(btnOK),                                "Next" },
                        { nameof(grpForceSynthetic),                    "Force synthetic estimation?" },
                        { nameof(rdoForceSyntheticYes),                 "yes" },
                        { nameof(rdoForceSyntheticNo),                  "no" },
                        { nameof(lblNoModelSigmaParamAreaCombination),  String.Concat(
                            "There is no combination of working model, sigma and parametrisation area type allowing for configuration of all desired GREG-map estimates. $0",
                            "Please define suitable working models and parametrisation areas for all desired estimation cells or reduce the set of estimation cells for GREG-map total configuration.") },
                        { nameof(colModel),                             "Model" },
                        { nameof(colSigma),                             "Sigma" },
                        { nameof(colParamAreaType),                     "Parametrization area type" },
                        { nameof(colNumberOfCellsCovered),              "Number of cells covered" },
                        { nameof(colAllCellsIncluded),                  "All cells included" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(key: nameof(FormGRegModelParamAreaType),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            radioButtons = [];
            onEdit = false;

            SetStyle();

            InitializeLabels();

            LoadContent();

            EnableNext();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;

            grpForceSynthetic.Font = Setting.FormFont;
            grpForceSynthetic.ForeColor = Setting.FormForeColor;

            rdoForceSyntheticYes.Font = Setting.RadioButtonFont;
            rdoForceSyntheticYes.ForeColor = Setting.RadioButtonForeColor;

            rdoForceSyntheticNo.Font = Setting.RadioButtonFont;
            rdoForceSyntheticNo.ForeColor = Setting.RadioButtonForeColor;

            lblNoModelSigmaParamAreaCombination.Font = Setting.LabelErrorFont;
            lblNoModelSigmaParamAreaCombination.ForeColor = Setting.LabelErrorForeColor;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormGRegModelParamAreaType),
                out string frmGRegModelParamAreaTypeText)
                    ? frmGRegModelParamAreaTypeText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            grpForceSynthetic.Text =
                labels.TryGetValue(key: nameof(grpForceSynthetic),
                out string grpForceSyntheticText)
                    ? grpForceSyntheticText
                    : String.Empty;

            rdoForceSyntheticYes.Text =
                labels.TryGetValue(key: nameof(rdoForceSyntheticYes),
                out string rdoForceSyntheticYesText)
                    ? rdoForceSyntheticYesText
                    : String.Empty;

            rdoForceSyntheticNo.Text =
                labels.TryGetValue(key: nameof(rdoForceSyntheticNo),
                out string rdoForceSyntheticNoText)
                    ? rdoForceSyntheticNoText
                    : String.Empty;

            lblNoModelSigmaParamAreaCombination.Text =
                (labels.TryGetValue(key: nameof(lblNoModelSigmaParamAreaCombination),
                out string lblNoModelSigmaParamAreaCombinationText)
                    ? lblNoModelSigmaParamAreaCombinationText
                    : String.Empty)
                .Replace(oldValue: "$0", newValue: Environment.NewLine);

            colModel =
                labels.TryGetValue(key: nameof(colModel),
                out colModel)
                    ? colModel
                    : String.Empty;

            colSigma =
                labels.TryGetValue(key: nameof(colSigma),
                out colSigma)
                    ? colSigma
                    : String.Empty;

            colParamAreaType =
                labels.TryGetValue(key: nameof(colParamAreaType),
                out colParamAreaType)
                    ? colParamAreaType
                    : String.Empty;

            colNumberOfCellsCovered =
                labels.TryGetValue(key: nameof(colNumberOfCellsCovered),
                out colNumberOfCellsCovered)
                    ? colNumberOfCellsCovered
                    : String.Empty;

            colAllCellsIncluded =
                labels.TryGetValue(key: nameof(colAllCellsIncluded),
                out colAllCellsIncluded)
                    ? colAllCellsIncluded
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazení přehledu kombinací modelu, sigma a typů parametrizačních oblastí</para>
        /// <para lang="en">Display combinations of model, sigma and parametrization area type</para>
        /// </summary>
        private void InitializeTableLayoutPanel()
        {
            Dictionary<string, string> labels = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

            pnlWorkSpace.Controls.Clear();
            radioButtons.Clear();

            if ((ModelsSigmaParamTypes == null) ||
                 (ModelsSigmaParamTypes.Items.Count == 0))
            {
                return;
            }

            TableLayoutPanel tlpModelsSigmaParamTypes = new()
            {
                ColumnCount = 6,
                Dock = DockStyle.Top,
                Height = 60 + 25 * ModelsSigmaParamTypes.Items.Count,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                RowCount = ModelsSigmaParamTypes.Items.Count + 2
            };

            tlpModelsSigmaParamTypes.ColumnStyles.Clear();
            tlpModelsSigmaParamTypes.RowStyles.Clear();

            // colRadioButton
            tlpModelsSigmaParamTypes.ColumnStyles.Add(
                   columnStyle: new ColumnStyle(sizeType: SizeType.Absolute, width: 100));

            // colModel
            tlpModelsSigmaParamTypes.ColumnStyles.Add(
                   columnStyle: new ColumnStyle(sizeType: SizeType.Percent, width: 50));

            // colSigma
            tlpModelsSigmaParamTypes.ColumnStyles.Add(
                   columnStyle: new ColumnStyle(sizeType: SizeType.Absolute, width: 100));

            // colParamAreaType
            tlpModelsSigmaParamTypes.ColumnStyles.Add(
                   columnStyle: new ColumnStyle(sizeType: SizeType.Percent, width: 50));

            // colNumberOfCellsCovered
            tlpModelsSigmaParamTypes.ColumnStyles.Add(
                   columnStyle: new ColumnStyle(sizeType: SizeType.Absolute, width: 100));

            // colAllCellsIncluded
            tlpModelsSigmaParamTypes.ColumnStyles.Add(
                   columnStyle: new ColumnStyle(sizeType: SizeType.Absolute, width: 100));

            // row Header
            tlpModelsSigmaParamTypes.RowStyles.Add(
                    rowStyle: new RowStyle(sizeType: SizeType.Absolute, height: 50));

            // rows for Models
            for (int i = 1; i <= ModelsSigmaParamTypes.Items.Count; i++)
            {
                tlpModelsSigmaParamTypes.RowStyles.Add(
                    rowStyle: new RowStyle(sizeType: SizeType.Absolute, height: 25));
            }

            // row Footer
            tlpModelsSigmaParamTypes.RowStyles.Add(
                    rowStyle: new RowStyle(sizeType: SizeType.Percent, height: 50));

            // colRadioButton header
            tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       BackColor = Setting.TableHeaderBackColor,
                       Dock = DockStyle.Fill,
                       Font = Setting.TableHeaderFont,
                       ForeColor = Setting.TableHeaderForeColor,
                       Margin = new Padding(all: 1),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = String.Empty
                   },
                   column: 0, row: 0);

            // colModel header
            tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       BackColor = Setting.TableHeaderBackColor,
                       Dock = DockStyle.Fill,
                       Font = Setting.TableHeaderFont,
                       ForeColor = Setting.TableHeaderForeColor,
                       Margin = new Padding(all: 1),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = colModel
                   },
                   column: 1, row: 0);

            // colSigma header
            tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       BackColor = Setting.TableHeaderBackColor,
                       Dock = DockStyle.Fill,
                       Font = Setting.TableHeaderFont,
                       ForeColor = Setting.TableHeaderForeColor,
                       Margin = new Padding(all: 1),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = colSigma
                   },
                   column: 2, row: 0);

            // colParamAreaType header
            tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       BackColor = Setting.TableHeaderBackColor,
                       Dock = DockStyle.Fill,
                       Font = Setting.TableHeaderFont,
                       ForeColor = Setting.TableHeaderForeColor,
                       Margin = new Padding(all: 1),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = colParamAreaType
                   },
                   column: 3, row: 0);

            // colNumberOfCellsCovered header
            tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       BackColor = Setting.TableHeaderBackColor,
                       Dock = DockStyle.Fill,
                       Font = Setting.TableHeaderFont,
                       ForeColor = Setting.TableHeaderForeColor,
                       Margin = new Padding(all: 1),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = colNumberOfCellsCovered
                   },
                   column: 4, row: 0);

            // colAllCellsIncluded header
            tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       BackColor = Setting.TableHeaderBackColor,
                       Dock = DockStyle.Fill,
                       Font = Setting.TableHeaderFont,
                       ForeColor = Setting.TableHeaderForeColor,
                       Margin = new Padding(all: 1),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = colAllCellsIncluded
                   },
                   column: 5, row: 0);

            // row for each model
            int rowIndex = 0;

            // seřadit podle popisku
            IEnumerable<TFnApiGetWModelsSigmaParamTypes> models =
                (LanguageVersion == LanguageVersion.International)
                    ? ModelsSigmaParamTypes.Items
                        .OrderBy(a => a.WorkingModelLabelEn)
                        .ThenBy(a => a.ParamAreaTypeLabelEn)
                        .ThenByDescending(a => (a.AllCellsIncluded ?? false).ToString())
                    : (LanguageVersion == LanguageVersion.National)
                        ? ModelsSigmaParamTypes.Items
                            .OrderBy(a => a.WorkingModelLabelCs)
                            .ThenBy(a => a.ParamAreaTypeLabelCs)
                            .ThenByDescending(a => (a.AllCellsIncluded ?? false).ToString())
                        : ModelsSigmaParamTypes.Items
                            .OrderBy(a => a.WorkingModelLabelEn)
                            .ThenBy(a => a.ParamAreaTypeLabelEn)
                            .ThenByDescending(a => (a.AllCellsIncluded ?? false).ToString());

            foreach (TFnApiGetWModelsSigmaParamTypes model in models)
            {
                rowIndex++;

                // colRadioButton
                RadioButton rdoOption = new()
                {
                    CheckAlign = System.Drawing.ContentAlignment.MiddleCenter,
                    Checked = false,
                    Enabled = model.AllCellsIncluded ?? false,
                    Dock = DockStyle.Fill,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    Tag = model,
                };

                rdoOption.CheckedChanged += new EventHandler(
                    (sender, e) =>
                    {
                        if (!onEdit)
                        {
                            onEdit = true;
                            foreach (RadioButton option in radioButtons)
                            {
                                option.Checked = false;
                            }
                            rdoOption.Checked = true;
                            onEdit = false;

                            int selectedRowIndex =
                                tlpModelsSigmaParamTypes.GetRow(control: rdoOption);

                            TableLayoutControlCollection controls = tlpModelsSigmaParamTypes.Controls;
                            for (int i = 0; i < controls.Count; i++)
                            {
                                rowIndex = tlpModelsSigmaParamTypes.GetRow(control: controls[i]);
                                if (rowIndex == 0)
                                {
                                    controls[i].BackColor = Setting.TableHeaderBackColor;
                                }
                                else if (rowIndex == selectedRowIndex)
                                {
                                    controls[i].BackColor = Setting.TableSelectedRowBackColor;
                                }
                                else
                                {
                                    controls[i].BackColor = Setting.TableRowBackColor;
                                }
                            }

                            EnableNext();
                        }
                    });

                rdoOption.CreateControl();

                radioButtons.Add(item: rdoOption);
                tlpModelsSigmaParamTypes.Controls.Add(
                    control: rdoOption,
                    column: 0, row: rowIndex);

                // colModel
                tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       Dock = DockStyle.Fill,
                       Enabled = model.AllCellsIncluded ?? false,
                       Margin = new Padding(all: 0),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text =
                        (LanguageVersion == LanguageVersion.National)
                            ? !String.IsNullOrEmpty(value: model.WorkingModelLabelCs)
                                ? model.WorkingModelLabelCs
                                : model.WorkingModelDescriptionCs
                            : !String.IsNullOrEmpty(value: model.WorkingModelLabelEn)
                                ? model.WorkingModelLabelEn
                                : model.WorkingModelDescriptionEn
                   },
                   column: 1, row: rowIndex);

                // colSigma
                tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       Dock = DockStyle.Fill,
                       Enabled = model.AllCellsIncluded ?? false,
                       Margin = new Padding(all: 0),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = model.Sigma.ToString()
                   },
                   column: 2, row: rowIndex);

                // colParamAreaType
                tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       Dock = DockStyle.Fill,
                       Enabled = model.AllCellsIncluded ?? false,
                       Margin = new Padding(all: 0),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text =
                        (LanguageVersion == LanguageVersion.National)
                            ? !String.IsNullOrEmpty(value: model.ParamAreaTypeLabelCs)
                                ? model.ParamAreaTypeLabelCs
                                : model.ParamAreaTypeDescriptionCs
                            : !String.IsNullOrEmpty(value: model.ParamAreaTypeLabelEn)
                                ? model.ParamAreaTypeLabelEn
                                : model.ParamAreaTypeDescriptionEn
                   },
                   column: 3, row: rowIndex);

                // colNumberOfCellsCovered
                tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       Dock = DockStyle.Fill,
                       Enabled = model.AllCellsIncluded ?? false,
                       Margin = new Padding(all: 0),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text =
                            (model.NoOfCellsCovered != null)
                                ? (model.NoOfCellsCovered ?? 0).ToString()
                                : Functions.StrNull
                   },
                   column: 4, row: rowIndex);

                // colAllCellsIncluded
                tlpModelsSigmaParamTypes.Controls.Add(
                   control: new Label()
                   {
                       AutoSize = false,
                       Dock = DockStyle.Fill,
                       Enabled = model.AllCellsIncluded ?? false,
                       Margin = new Padding(all: 0),
                       Padding = new Padding(all: 0),
                       TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                       Text = model.AllCellsIncluded.ToString()
                   },
                   column: 5, row: rowIndex);
            }

            if (radioButtons.Where(a => a.Enabled).Any())
            {
                radioButtons.Where(a => a.Enabled).First<RadioButton>().Checked = true;
            }

            tlpModelsSigmaParamTypes.CreateControl();
            pnlWorkSpace.Controls.Add(value: tlpModelsSigmaParamTypes);

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            bool radioButtonsChecked = radioButtons.OfType<RadioButton>().Where(a => a.Checked).Any();
            btnOK.Enabled = radioButtonsChecked;
            lblNoModelSigmaParamAreaCombination.Visible = !radioButtonsChecked;
            tlpWorkSpace.RowStyles[tlpWorkSpace.RowStyles.Count - 1].Height = radioButtonsChecked ? 0 : 40;
        }

        #endregion Methods

    }

}