﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nového období pro konfiguraci odhadů</para>
    /// <para lang="en">Form to create a new period for configuring estimates</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormEstimationPeriodNew
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;
        private string msgEstimationPeriodExists = String.Empty;
        private string msgInvalidEstimateDateBegin = String.Empty;
        private string msgInvalidEstimateDateEnd = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormEstimationPeriodNew(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimationPeriodNew),        "Nové období pro výpočet odhadu" },
                        { nameof(btnCancel),                      "Zrušit" },
                        { nameof(btnInsert),                      "Vytvořit" },
                        { nameof(grpNewPeriod),                   "Nové období:" },
                        { nameof(lblDate),                        "Období odhadu:" },
                        { nameof(lblFrom),                        "Od" },
                        { nameof(lblTo),                          "Do" },
                        { nameof(lblLabel),                       "Zkratka (národní):" },
                        { nameof(lblDescription),                 "Popis (národní):" },
                        { nameof(lblLabelEn),                     "Zkratka (anglicky):" },
                        { nameof(lblDescriptionEn),               "Popis (anglicky):" },
                        { nameof(lblDefaultInOlap),               "Aktivní jako výchozí:" },
                        { nameof(tipDefaultInOlap),               "Omezuje zobrazení období na období nastavená na ano (projeví se až při dalším výpočtu)." },
                        { nameof(rdoFalse),                       "ne" },
                        { nameof(rdoTrue),                        "ano" },
                        { nameof(msgLabelCsIsEmpty),              "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),        "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),              "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),        "Popis (anglický) musí být vyplněn." },
                        { nameof(msgLabelCsExists),               "Použitá zkratka (národní) již existuje pro jiné výpočetní období." },
                        { nameof(msgLabelEnExists),               "Použitá zkratka (anglická) již existuje pro jiné výpočetní období." },
                        { nameof(msgDescriptionCsExists),         "Použitý popis (národní) již existuje pro jiné výpočetní období." },
                        { nameof(msgDescriptionEnExists),         "Použitý popis (anglický) již existuje pro jiné výpočetní období." },
                        { nameof(msgEstimationPeriodExists),      "Zadané výpočetní období již v databázi existuje." },
                        { nameof(msgInvalidEstimateDateBegin),    "Chybný formát datumu pro začátek výpočetního období." },
                        { nameof(msgInvalidEstimateDateEnd),      "Chybný formát datumu pro konec výpočetního období." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimationPeriodNew),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimationPeriodNew),        "New estimation period" },
                        { nameof(btnCancel),                      "Cancel" },
                        { nameof(btnInsert),                      "Insert" },
                        { nameof(grpNewPeriod),                   "New estimation period:" },
                        { nameof(lblDate),                        "Estimation period:" },
                        { nameof(lblFrom),                        "From" },
                        { nameof(lblTo),                          "To" },
                        { nameof(lblLabel),                       "Label (national):" },
                        { nameof(lblDescription),                 "Description (national):" },
                        { nameof(lblLabelEn),                     "Label (English):" },
                        { nameof(lblDescriptionEn),               "Description (English):" },
                        { nameof(lblDefaultInOlap),               "Active by default:" },
                        { nameof(tipDefaultInOlap),               "Limits the display of periods to those set to yes (it performs in the next calculation)." },
                        { nameof(rdoFalse),                       "no" },
                        { nameof(rdoTrue),                        "yes" },
                        { nameof(msgLabelCsIsEmpty),              "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),        "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),              "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),        "Description (English) cannot be empty." },
                        { nameof(msgLabelCsExists),               "This label (national) already exists for another estimation period." },
                        { nameof(msgLabelEnExists),               "This label (English) already exists for another estimation period." },
                        { nameof(msgDescriptionCsExists),         "This description (national) already exists for another estimation period." },
                        { nameof(msgDescriptionEnExists),         "This description (English) already exists for another estimation period." },
                        { nameof(msgEstimationPeriodExists),      "The specified estimation period already exists in the database." },
                        { nameof(msgInvalidEstimateDateBegin),    "Incorrect date format for the beginning of the estimation period." },
                        { nameof(msgInvalidEstimateDateEnd),      "Incorrect date format for the end of the estimation period." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimationPeriodNew),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            LoadContent();

            InitializeLabels();

            dtpFrom.ValueChanged += new EventHandler(
               (sender, e) =>
               {
                   // Když uživatel změní datum v dtpFrom, nastaví se dtpTo tak, aby nemohl vybrat dřívější datum
                   // Date changed in dtpFrom, dtpTo set to the state a user is not able to pick an earlier date
                   dtpTo.MinDate = dtpFrom.Value.AddDays(1);

                   // Pokud je hodnota dtpTo menší než nová minimální hodnota, automaticky se posune
                   // If the value of dtpTo is less than the new minimum value, it is automatically moved to the new minimum value.
                   if (dtpTo.Value <= dtpTo.MinDate)
                   {
                       dtpTo.Value = dtpTo.MinDate;
                   }
               });
            btnInsert.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.OK;
                   Close();
               });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !SetNewEstimationPeriod();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormEstimationPeriodNew),
                out string frmEstimationPeriodNewText)
                    ? frmEstimationPeriodNewText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            btnInsert.Text =
                labels.TryGetValue(key: nameof(btnInsert),
                out string btnInsertText)
                    ? btnInsertText
                    : String.Empty;

            grpNewPeriod.Text =
                labels.TryGetValue(key: nameof(grpNewPeriod),
                out string grpNewPeriodText)
                    ? grpNewPeriodText
                    : String.Empty;

            lblDate.Text =
                labels.TryGetValue(key: nameof(lblDate),
                out string lblDateText)
                    ? lblDateText
                    : String.Empty;

            lblFrom.Text =
                labels.TryGetValue(key: nameof(lblFrom),
                out string lblFromText)
                    ? lblFromText
                    : String.Empty;

            lblTo.Text =
                labels.TryGetValue(key: nameof(lblTo),
                out string lblToText)
                    ? lblToText
                    : String.Empty;

            lblLabel.Text =
                labels.TryGetValue(key: nameof(lblLabel),
                out string lblLabelText)
                    ? lblLabelText
                    : String.Empty;

            lblDescription.Text =
                labels.TryGetValue(key: nameof(lblDescription),
                out string lblDescriptionText)
                    ? lblDescriptionText
                    : String.Empty;

            lblLabelEn.Text =
                labels.TryGetValue(key: nameof(lblLabelEn),
                out string lblLabelEnText)
                    ? lblLabelEnText
                    : String.Empty;

            lblDescriptionEn.Text =
                labels.TryGetValue(key: nameof(lblDescriptionEn),
                out string lblDescriptionEnText)
                    ? lblDescriptionEnText
                    : String.Empty;

            lblDefaultInOlap.Text =
                labels.TryGetValue(key: nameof(lblDefaultInOlap),
                out string lblDefaultInOlapText)
                    ? lblDefaultInOlapText
                    : String.Empty;

            rdoFalse.Text =
                labels.TryGetValue(key: nameof(rdoFalse),
                out string rdoFalseText)
                    ? rdoFalseText
                    : String.Empty;

            rdoTrue.Text =
                labels.TryGetValue(key: nameof(rdoTrue),
                out string rdoTrueText)
                    ? rdoTrueText
                    : String.Empty;

            msgLabelCsIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                out msgLabelCsIsEmpty)
                    ? msgLabelCsIsEmpty
                    : String.Empty;

            msgDescriptionCsIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                out msgDescriptionCsIsEmpty)
                    ? msgDescriptionCsIsEmpty
                    : String.Empty;

            msgLabelEnIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                out msgLabelEnIsEmpty)
                    ? msgLabelEnIsEmpty
                    : String.Empty;

            msgDescriptionEnIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                out msgDescriptionEnIsEmpty)
                    ? msgDescriptionEnIsEmpty
                    : String.Empty;

            msgLabelCsExists =
                labels.TryGetValue(key: nameof(msgLabelCsExists),
                out msgLabelCsExists)
                    ? msgLabelCsExists
                    : String.Empty;

            msgLabelEnExists =
                labels.TryGetValue(key: nameof(msgLabelEnExists),
                out msgLabelEnExists)
                    ? msgLabelEnExists
                    : String.Empty;

            msgDescriptionCsExists =
                labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                    out msgDescriptionCsExists)
                        ? msgDescriptionCsExists
                        : String.Empty;

            msgDescriptionEnExists =
               labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                   out msgDescriptionEnExists)
                       ? msgDescriptionEnExists
                       : String.Empty;

            msgEstimationPeriodExists =
                labels.TryGetValue(key: nameof(msgEstimationPeriodExists),
                out msgEstimationPeriodExists)
                    ? msgEstimationPeriodExists
                    : String.Empty;

            msgInvalidEstimateDateBegin =
                labels.TryGetValue(key: nameof(msgInvalidEstimateDateBegin),
                out msgInvalidEstimateDateBegin)
                    ? msgInvalidEstimateDateBegin
                    : String.Empty;

            msgInvalidEstimateDateEnd =
                labels.TryGetValue(key: nameof(msgInvalidEstimateDateEnd),
                out msgInvalidEstimateDateEnd)
                    ? msgInvalidEstimateDateEnd
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            tipDefaultInOlap.SetToolTip(lblDefaultInOlap, labels.ContainsKey(key: nameof(tipDefaultInOlap)) ? labels[nameof(tipDefaultInOlap)] : String.Empty);

            rdoFalse.Checked = true;

            // dtpTo je nastaven na zítřejší datum při zobrazení formuláře
            // dtpTo set to the next day date
            dtpTo.Value = DateTime.Today.AddDays(1);
            dtpTo.MinDate = dtpFrom.Value.AddDays(1);
        }

        /// <summary>
        /// <para lang="cs">Nastaví vlastnosti objektu nového výpočetního období</para>
        /// <para lang="en">Sets the object properties of the new estimation period</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud je možné uložit objekt nového výpočentího období do databázové tabulky, jinak false</para>
        /// <para lang="en">It returns true if it is possible to save the new estimation period object into database table, else false</para>
        /// </returns>
        private bool SetNewEstimationPeriod()
        {
            string labelCs = txtLabel.Text.Trim();
            string descriptionCs = txtDescription.Text.Trim();
            string labelEn = txtLabelEn.Text.Trim();
            string descriptionEn = txtDescriptionEn.Text.Trim();
            DateTime estimateDateBegin = dtpFrom.Value;
            DateTime estimateDateEnd = dtpTo.Value;
            bool defaultInOlap = rdoTrue.Checked;

            EstimationPeriodList estimationPeriodList = NfiEstaFunctions.FnApiGetListOfEstimationPeriods.Execute(Database);

            if (estimationPeriodList.Items
                .Where(a => a.EstimateDateBegin.Date == estimateDateBegin.Date)
                .Where(a => a.EstimateDateEnd.Date == estimateDateEnd.Date)
                .Any())
            {
                // Období určené počátkem a koncem již v databázi existuje
                // Period set by begin and end already exists in the database
                MessageBox.Show(
                    text: msgEstimationPeriodExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(value: labelCs))
            {
                // LabelCs - prázdné
                // LabelCs - empty
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                // DescriptionCs - prázdné
                // DescriptionCs - empty
                MessageBox.Show(
                     text: msgDescriptionCsIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                // LabelEn - prázdné
                // LabelEn - empty
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                // DescriptionEn - prázdné
                // DescriptionEn - empty
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList results =
                NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfEstimationPeriodExist
                .Execute(Database, labelCs, labelEn, descriptionCs, descriptionEn);

            TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist result =
                results.Items[0];

            if (result.LabelCsFound ?? false)
            {
                // LabelCs - duplicitní
                // LabelCs - duplicate
                MessageBox.Show(
                    text: msgLabelCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (result.LabelEnFound ?? false)
            {
                // LabelEn - duplicitní
                // LabelEn - duplicate
                MessageBox.Show(
                    text: msgLabelEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (result.DescriptionCsFound ?? false)
            {
                // DescriptionCs - duplicitní
                // DescriptionCs - duplicate
                MessageBox.Show(
                    text: msgDescriptionCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (result.DescriptionEnFound ?? false)
            {
                // DescriptionEn - duplicitní
                // DescriptionEn - duplicate
                MessageBox.Show(
                    text: msgDescriptionEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // OK - nové výpočetní období je možné zapsat do databáze
            // OK - It is possible to write a new estimation period in the database
            NfiEstaFunctions.FnApiSaveEstimationPeriod.Execute(
                Database,
                estimateDateBegin,
                estimateDateEnd,
                labelCs,
                descriptionCs,
                labelEn,
                descriptionEn,
                defaultInOlap);

            return true;
        }

        #endregion Methods

    }

}