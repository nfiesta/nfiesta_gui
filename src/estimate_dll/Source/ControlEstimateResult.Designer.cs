﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlEstimateResult
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splEstimateResult = new System.Windows.Forms.SplitContainer();
            this.pnlDimensions = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlPrevious = new System.Windows.Forms.Panel();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splCaption = new System.Windows.Forms.SplitContainer();
            this.pnlSelectedTargetVariable = new System.Windows.Forms.Panel();
            this.grpSelectedTargetVariable = new System.Windows.Forms.GroupBox();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.splViewOLAP = new System.Windows.Forms.SplitContainer();
            this.pnlViewOLAP = new System.Windows.Forms.Panel();
            this.splViewPanels = new System.Windows.Forms.SplitContainer();
            this.pnlViewHistory = new System.Windows.Forms.Panel();
            this.pnlViewPanels = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splEstimateResult)).BeginInit();
            this.splEstimateResult.Panel1.SuspendLayout();
            this.splEstimateResult.Panel2.SuspendLayout();
            this.splEstimateResult.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlPrevious.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).BeginInit();
            this.splCaption.Panel1.SuspendLayout();
            this.splCaption.Panel2.SuspendLayout();
            this.splCaption.SuspendLayout();
            this.pnlSelectedTargetVariable.SuspendLayout();
            this.grpSelectedTargetVariable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splViewOLAP)).BeginInit();
            this.splViewOLAP.Panel1.SuspendLayout();
            this.splViewOLAP.Panel2.SuspendLayout();
            this.splViewOLAP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splViewPanels)).BeginInit();
            this.splViewPanels.Panel1.SuspendLayout();
            this.splViewPanels.Panel2.SuspendLayout();
            this.splViewPanels.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlMain.Controls.Add(this.splEstimateResult);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 0;
            // 
            // splEstimateResult
            // 
            this.splEstimateResult.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splEstimateResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splEstimateResult.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splEstimateResult.Location = new System.Drawing.Point(3, 3);
            this.splEstimateResult.Margin = new System.Windows.Forms.Padding(0);
            this.splEstimateResult.Name = "splEstimateResult";
            // 
            // splEstimateResult.Panel1
            // 
            this.splEstimateResult.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splEstimateResult.Panel1.Controls.Add(this.pnlDimensions);
            this.splEstimateResult.Panel1Collapsed = true;
            this.splEstimateResult.Panel1MinSize = 0;
            // 
            // splEstimateResult.Panel2
            // 
            this.splEstimateResult.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splEstimateResult.Panel2.Controls.Add(this.tlpMain);
            this.splEstimateResult.Panel2MinSize = 0;
            this.splEstimateResult.Size = new System.Drawing.Size(954, 534);
            this.splEstimateResult.SplitterDistance = 350;
            this.splEstimateResult.SplitterWidth = 1;
            this.splEstimateResult.TabIndex = 3;
            // 
            // pnlDimensions
            // 
            this.pnlDimensions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDimensions.Location = new System.Drawing.Point(0, 0);
            this.pnlDimensions.Name = "pnlDimensions";
            this.pnlDimensions.Size = new System.Drawing.Size(350, 100);
            this.pnlDimensions.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 1);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(954, 534);
            this.tlpMain.TabIndex = 0;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.Controls.Add(this.pnlPrevious, 2, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 494);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(954, 40);
            this.tlpButtons.TabIndex = 5;
            // 
            // pnlPrevious
            // 
            this.pnlPrevious.Controls.Add(this.btnPrevious);
            this.pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrevious.Location = new System.Drawing.Point(794, 0);
            this.pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPrevious.Name = "pnlPrevious";
            this.pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPrevious.Size = new System.Drawing.Size(160, 40);
            this.pnlPrevious.TabIndex = 16;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrevious.Location = new System.Drawing.Point(5, 5);
            this.btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(150, 30);
            this.btnPrevious.TabIndex = 15;
            this.btnPrevious.Text = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlWorkSpace.Controls.Add(this.splCaption);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 494);
            this.pnlWorkSpace.TabIndex = 0;
            // 
            // splCaption
            // 
            this.splCaption.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCaption.Location = new System.Drawing.Point(0, 0);
            this.splCaption.Margin = new System.Windows.Forms.Padding(0);
            this.splCaption.Name = "splCaption";
            this.splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            this.splCaption.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splCaption.Panel1.Controls.Add(this.pnlSelectedTargetVariable);
            this.splCaption.Panel1MinSize = 0;
            // 
            // splCaption.Panel2
            // 
            this.splCaption.Panel2.Controls.Add(this.splViewOLAP);
            this.splCaption.Panel2MinSize = 0;
            this.splCaption.Size = new System.Drawing.Size(954, 491);
            this.splCaption.SplitterDistance = 200;
            this.splCaption.SplitterWidth = 1;
            this.splCaption.TabIndex = 14;
            // 
            // pnlSelectedTargetVariable
            // 
            this.pnlSelectedTargetVariable.BackColor = System.Drawing.SystemColors.Control;
            this.pnlSelectedTargetVariable.Controls.Add(this.grpSelectedTargetVariable);
            this.pnlSelectedTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSelectedTargetVariable.Location = new System.Drawing.Point(0, 0);
            this.pnlSelectedTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSelectedTargetVariable.Name = "pnlSelectedTargetVariable";
            this.pnlSelectedTargetVariable.Padding = new System.Windows.Forms.Padding(5);
            this.pnlSelectedTargetVariable.Size = new System.Drawing.Size(954, 200);
            this.pnlSelectedTargetVariable.TabIndex = 13;
            // 
            // grpSelectedTargetVariable
            // 
            this.grpSelectedTargetVariable.BackColor = System.Drawing.SystemColors.Control;
            this.grpSelectedTargetVariable.Controls.Add(this.pnlCaption);
            this.grpSelectedTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSelectedTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpSelectedTargetVariable.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpSelectedTargetVariable.Location = new System.Drawing.Point(5, 5);
            this.grpSelectedTargetVariable.Margin = new System.Windows.Forms.Padding(5);
            this.grpSelectedTargetVariable.Name = "grpSelectedTargetVariable";
            this.grpSelectedTargetVariable.Padding = new System.Windows.Forms.Padding(5);
            this.grpSelectedTargetVariable.Size = new System.Drawing.Size(944, 190);
            this.grpSelectedTargetVariable.TabIndex = 6;
            this.grpSelectedTargetVariable.TabStop = false;
            this.grpSelectedTargetVariable.Text = "grpSelectedTargetVariable";
            // 
            // pnlCaption
            // 
            this.pnlCaption.BackColor = System.Drawing.SystemColors.Control;
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlCaption.Location = new System.Drawing.Point(5, 20);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Size = new System.Drawing.Size(934, 165);
            this.pnlCaption.TabIndex = 0;
            // 
            // splViewOLAP
            // 
            this.splViewOLAP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splViewOLAP.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splViewOLAP.Location = new System.Drawing.Point(0, 0);
            this.splViewOLAP.Margin = new System.Windows.Forms.Padding(0);
            this.splViewOLAP.Name = "splViewOLAP";
            this.splViewOLAP.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splViewOLAP.Panel1
            // 
            this.splViewOLAP.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splViewOLAP.Panel1.Controls.Add(this.pnlViewOLAP);
            this.splViewOLAP.Panel1MinSize = 0;
            // 
            // splViewOLAP.Panel2
            // 
            this.splViewOLAP.Panel2.Controls.Add(this.splViewPanels);
            this.splViewOLAP.Panel2Collapsed = true;
            this.splViewOLAP.Panel2MinSize = 0;
            this.splViewOLAP.Size = new System.Drawing.Size(954, 290);
            this.splViewOLAP.SplitterDistance = 100;
            this.splViewOLAP.SplitterWidth = 1;
            this.splViewOLAP.TabIndex = 15;
            // 
            // pnlViewOLAP
            // 
            this.pnlViewOLAP.BackColor = System.Drawing.SystemColors.Control;
            this.pnlViewOLAP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlViewOLAP.Location = new System.Drawing.Point(0, 0);
            this.pnlViewOLAP.Margin = new System.Windows.Forms.Padding(0);
            this.pnlViewOLAP.Name = "pnlViewOLAP";
            this.pnlViewOLAP.Padding = new System.Windows.Forms.Padding(5);
            this.pnlViewOLAP.Size = new System.Drawing.Size(954, 290);
            this.pnlViewOLAP.TabIndex = 15;
            // 
            // splViewPanels
            // 
            this.splViewPanels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splViewPanels.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splViewPanels.Location = new System.Drawing.Point(0, 0);
            this.splViewPanels.Margin = new System.Windows.Forms.Padding(0);
            this.splViewPanels.Name = "splViewPanels";
            this.splViewPanels.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splViewPanels.Panel1
            // 
            this.splViewPanels.Panel1.Controls.Add(this.pnlViewHistory);
            this.splViewPanels.Panel1MinSize = 0;
            // 
            // splViewPanels.Panel2
            // 
            this.splViewPanels.Panel2.Controls.Add(this.pnlViewPanels);
            this.splViewPanels.Panel2Collapsed = true;
            this.splViewPanels.Panel2MinSize = 0;
            this.splViewPanels.Size = new System.Drawing.Size(150, 46);
            this.splViewPanels.SplitterDistance = 25;
            this.splViewPanels.SplitterWidth = 1;
            this.splViewPanels.TabIndex = 16;
            // 
            // pnlViewHistory
            // 
            this.pnlViewHistory.BackColor = System.Drawing.SystemColors.Control;
            this.pnlViewHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlViewHistory.Location = new System.Drawing.Point(0, 0);
            this.pnlViewHistory.Margin = new System.Windows.Forms.Padding(0);
            this.pnlViewHistory.Name = "pnlViewHistory";
            this.pnlViewHistory.Padding = new System.Windows.Forms.Padding(5);
            this.pnlViewHistory.Size = new System.Drawing.Size(150, 46);
            this.pnlViewHistory.TabIndex = 15;
            // 
            // pnlViewPanels
            // 
            this.pnlViewPanels.BackColor = System.Drawing.SystemColors.Control;
            this.pnlViewPanels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlViewPanels.Location = new System.Drawing.Point(0, 0);
            this.pnlViewPanels.Margin = new System.Windows.Forms.Padding(0);
            this.pnlViewPanels.Name = "pnlViewPanels";
            this.pnlViewPanels.Padding = new System.Windows.Forms.Padding(5);
            this.pnlViewPanels.Size = new System.Drawing.Size(150, 46);
            this.pnlViewPanels.TabIndex = 16;
            // 
            // ControlEstimateResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Name = "ControlEstimateResult";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.splEstimateResult.Panel1.ResumeLayout(false);
            this.splEstimateResult.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splEstimateResult)).EndInit();
            this.splEstimateResult.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlPrevious.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splCaption.Panel1.ResumeLayout(false);
            this.splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).EndInit();
            this.splCaption.ResumeLayout(false);
            this.pnlSelectedTargetVariable.ResumeLayout(false);
            this.grpSelectedTargetVariable.ResumeLayout(false);
            this.splViewOLAP.Panel1.ResumeLayout(false);
            this.splViewOLAP.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splViewOLAP)).EndInit();
            this.splViewOLAP.ResumeLayout(false);
            this.splViewPanels.Panel1.ResumeLayout(false);
            this.splViewPanels.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splViewPanels)).EndInit();
            this.splViewPanels.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splEstimateResult;
        private System.Windows.Forms.Panel pnlDimensions;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.Panel pnlSelectedTargetVariable;
        private System.Windows.Forms.GroupBox grpSelectedTargetVariable;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.SplitContainer splViewOLAP;
        private System.Windows.Forms.Panel pnlViewOLAP;
        private System.Windows.Forms.SplitContainer splViewPanels;
        private System.Windows.Forms.Panel pnlViewHistory;
        private System.Windows.Forms.Panel pnlViewPanels;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
    }

}
