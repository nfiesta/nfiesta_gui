﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">
    /// Formulář pro konfiguraci regresních odhadů:
    /// Nová skupina panelů a referenčních období
    /// </para>
    /// <para lang="en">
    /// Form for configuring regression estimates:
    /// New panel reference year set group
    /// </para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormGRegPanelRefYearSetGroupNew
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        private string msgNoRecordReturned = String.Empty;
        private string msgLabelCsIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormGRegPanelRefYearSetGroupNew(
            Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Popisek skupiny panelů a referenčních období v národním jazyce</para>
        /// <para lang="en">Panel reference year set group label in national language</para>
        /// </summary>
        public string LabelCs
        {
            get
            {
                return txtLabelCsValue.Text.Trim();
            }
        }

        /// <summary>
        /// <para lang="cs">Popisek skupiny panelů a referenčních období v angličtině</para>
        /// <para lang="en">Panel reference year set group label in English</para>
        /// </summary>
        public string LabelEn
        {
            get
            {
                return txtLabelEnValue.Text.Trim();
            }
        }

        /// <summary>
        /// <para lang="cs">Popis skupiny panelů a referenčních období v národním jazyce</para>
        /// <para lang="en">Panel reference year set group description in national language</para>
        /// </summary>
        public string DescriptionCs
        {
            get
            {
                return txtDescriptionCsValue.Text.Trim();
            }
        }

        /// <summary>
        /// <para lang="cs">Popis skupiny panelů a referenčních období v angličtině</para>
        /// <para lang="en">Panel reference year set group description in English</para>
        /// </summary>
        public string DescriptionEn
        {
            get
            {
                return txtDescriptionEnValue.Text.Trim();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegPanelRefYearSetGroupNew),  "Nová skupina panelů a referenčních období" },
                        { nameof(lblLabelCsCaption),                "Zkratka (národní):" },
                        { nameof(lblDescriptionCsCaption),          "Popis (národní):" },
                        { nameof(lblLabelEnCaption),                "Zkratka (anglická):" },
                        { nameof(lblDescriptionEnCaption),          "Popis (anglický):" },
                        { nameof(btnCancel),                        "Zrušit" },
                        { nameof(btnOK),                            "Uložit" },
                        { nameof(msgNoRecordReturned),              "Uložená procedura $1 nevrací žádný záznam." },
                        { nameof(msgLabelCsIsEmpty),                "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgLabelCsExists),                 "Použitá zkratka (národní) již existuje pro jinou skupinu panelů a referenčních období." },
                        { nameof(msgDescriptionCsIsEmpty),          "Popis (národní) musí být vyplněn." },
                        { nameof(msgDescriptionCsExists),           "Použitý popis (národní) již existuje pro jinou skupinu panelů a referenčních období." },
                        { nameof(msgLabelEnIsEmpty),                "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgLabelEnExists),                 "Použitá zkratka (anglická) již existuje pro jinou skupinu panelů a referenčních období." },
                        { nameof(msgDescriptionEnIsEmpty),          "Popis (anglický) musí být vyplněn." },
                        { nameof(msgDescriptionEnExists),           "Použitý popis (anglický) již existuje pro jinou skupinu panelů a referenčních období." },
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormGRegPanelRefYearSetGroupNew),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegPanelRefYearSetGroupNew),  "New panel reference year set group" },
                        { nameof(lblLabelCsCaption),                "Label (national):" },
                        { nameof(lblDescriptionCsCaption),          "Description (national):" },
                        { nameof(lblLabelEnCaption),                "Label (English):" },
                        { nameof(lblDescriptionEnCaption),          "Description (English):" },
                        { nameof(btnCancel),                        "Cancel" },
                        { nameof(btnOK),                            "Save" },
                        { nameof(msgNoRecordReturned),              "Stored procedure $1 returns null record." },
                        { nameof(msgLabelCsIsEmpty),                "Label (national) cannot be empty." },
                        { nameof(msgLabelCsExists),                 "This label (national) already exists for another panel reference year set group." },
                        { nameof(msgDescriptionCsIsEmpty),          "Description (national) cannot be empty." },
                        { nameof(msgDescriptionCsExists),           "This description (national) already exists for another panel reference year set group." },
                        { nameof(msgLabelEnIsEmpty),                "Label (English) cannot be empty." },
                        { nameof(msgLabelEnExists),                 "This label (English) already exists for another panel reference year set group." },
                        { nameof(msgDescriptionEnIsEmpty),          "Description (English) cannot be empty." },
                        { nameof(msgDescriptionEnExists),           "This description (English) already exists for another panel reference year set group." },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormGRegPanelRefYearSetGroupNew),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            SetStyle();

            InitializeLabels();

            LoadContent();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !CheckPanelReferenceYearSetGroupLabels();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;

            lblLabelCsCaption.Font = Setting.LabelFont;
            lblLabelCsCaption.ForeColor = Setting.LabelForeColor;

            lblLabelEnCaption.Font = Setting.LabelFont;
            lblLabelEnCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionCsCaption.Font = Setting.LabelFont;
            lblDescriptionCsCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionEnCaption.Font = Setting.LabelFont;
            lblDescriptionEnCaption.ForeColor = Setting.LabelForeColor;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormGRegPanelRefYearSetGroupNew),
                    out string frmGRegPanelRefYearSetGroupNewText)
                        ? frmGRegPanelRefYearSetGroupNewText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            lblLabelCsCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCsCaption),
                    out string lblLabelCsCaptionText)
                        ? lblLabelCsCaptionText
                        : String.Empty;

            lblDescriptionCsCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionCsCaption),
                    out string lblDescriptionCsCaptionText)
                        ? lblDescriptionCsCaptionText
                        : String.Empty;

            lblLabelEnCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelEnCaption),
                    out string lblLabelEnCaptionText)
                        ? lblLabelEnCaptionText
                        : String.Empty;

            lblDescriptionEnCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionEnCaption),
                    out string lblDescriptionEnCaptionText)
                        ? lblDescriptionEnCaptionText
                        : String.Empty;

            msgNoRecordReturned =
                labels.TryGetValue(key: nameof(msgNoRecordReturned),
                    out msgNoRecordReturned)
                        ? msgNoRecordReturned
                        : String.Empty;

            msgLabelCsIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                    out msgLabelCsIsEmpty)
                        ? msgLabelCsIsEmpty
                        : String.Empty;

            msgLabelCsExists =
                labels.TryGetValue(key: nameof(msgLabelCsExists),
                    out msgLabelCsExists)
                        ? msgLabelCsExists
                        : String.Empty;

            msgDescriptionCsIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                    out msgDescriptionCsIsEmpty)
                        ? msgDescriptionCsIsEmpty
                        : String.Empty;

            msgDescriptionCsExists =
                labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                    out msgDescriptionCsExists)
                        ? msgDescriptionCsExists
                        : String.Empty;

            msgLabelEnIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                    out msgLabelEnIsEmpty)
                        ? msgLabelEnIsEmpty
                        : String.Empty;

            msgLabelEnExists =
                labels.TryGetValue(key: nameof(msgLabelEnExists),
                    out msgLabelEnExists)
                        ? msgLabelEnExists
                        : String.Empty;

            msgDescriptionEnIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                    out msgDescriptionEnIsEmpty)
                        ? msgDescriptionEnIsEmpty
                        : String.Empty;

            msgDescriptionEnExists =
                labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                    out msgDescriptionEnExists)
                        ? msgDescriptionEnExists
                        : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Provede kontrolu popisků skupiny panelů a referenčních období</para>
        /// <para lang="en">Checks labels and descritpions for group of panels and reference year sets</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckPanelReferenceYearSetGroupLabels()
        {
            if (String.IsNullOrEmpty(value: LabelCs))
            {
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: DescriptionCs))
            {
                MessageBox.Show(
                    text: msgDescriptionCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: LabelEn))
            {
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: DescriptionEn))
            {
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList list =
                NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist
                .Execute(
                    database: Database,
                    labelCs: LabelCs,
                    labelEn: LabelEn,
                    descriptionCs: DescriptionCs,
                    descriptionEn: DescriptionEn);

            if ((list?.Items == null) ||
                 (list?.Items.Count < 1) ||
                 (list?.Items.Count > 1))
            {
                MessageBox.Show(
                    text: msgNoRecordReturned
                        .Replace(
                            oldValue: "$1",
                            newValue: NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist.Name),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist item =
                list.Items.FirstOrDefault<TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist>();

            if (item == null)
            {
                MessageBox.Show(
                    text: msgNoRecordReturned
                        .Replace(
                            oldValue: "$1",
                            newValue: NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist.Name),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (item.LabelCsFound ?? true)
            {
                MessageBox.Show(
                    text: msgLabelCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (item.DescriptionCsFound ?? true)
            {
                MessageBox.Show(
                    text: msgDescriptionCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (item.LabelEnFound ?? true)
            {
                MessageBox.Show(
                    text: msgLabelEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (item.DescriptionEnFound ?? true)
            {
                MessageBox.Show(
                    text: msgDescriptionEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            return true;
        }

        #endregion Methods

    }

}