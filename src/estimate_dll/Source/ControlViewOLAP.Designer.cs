﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{
    partial class ControlViewOLAP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlViewOLAP));
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpData = new System.Windows.Forms.GroupBox();
            pnlData = new System.Windows.Forms.Panel();
            dgvData = new System.Windows.Forms.DataGridView();
            pnlTools = new System.Windows.Forms.Panel();
            tsrTools = new System.Windows.Forms.ToolStrip();
            btnFilterColumns = new System.Windows.Forms.ToolStripButton();
            btnConfigureEstimate = new System.Windows.Forms.ToolStripButton();
            btnCalculateEstimate = new System.Windows.Forms.ToolStripButton();
            btnExportResults = new System.Windows.Forms.ToolStripButton();
            btnClose = new System.Windows.Forms.ToolStripButton();
            btnDisplayHistory = new System.Windows.Forms.ToolStripButton();
            btnDisplayPanels = new System.Windows.Forms.ToolStripButton();
            btnEditEstimationPeriod = new System.Windows.Forms.ToolStripButton();
            btnEditPanelRefYearSetGroup = new System.Windows.Forms.ToolStripButton();
            btnLoadEstimationCellCollection = new System.Windows.Forms.ToolStripButton();
            btnLoadEstimationPeriod = new System.Windows.Forms.ToolStripButton();
            btnLoadPhaseEstimateType = new System.Windows.Forms.ToolStripButton();
            btnCancelRowFilter = new System.Windows.Forms.ToolStripButton();
            pnlRowsCount = new System.Windows.Forms.Panel();
            lblRowsCount = new System.Windows.Forms.Label();
            pnlFilterCondition = new System.Windows.Forms.Panel();
            lblFilterCondition = new System.Windows.Forms.Label();
            splProgress = new System.Windows.Forms.SplitContainer();
            prgCalculation = new System.Windows.Forms.ProgressBar();
            lblCalculationStatus = new System.Windows.Forms.Label();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            grpData.SuspendLayout();
            pnlData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvData).BeginInit();
            pnlTools.SuspendLayout();
            tsrTools.SuspendLayout();
            pnlRowsCount.SuspendLayout();
            pnlFilterCondition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splProgress).BeginInit();
            splProgress.Panel1.SuspendLayout();
            splProgress.Panel2.SuspendLayout();
            splProgress.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.SystemColors.Control;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(1, 1);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(1118, 621);
            pnlMain.TabIndex = 0;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpData, 0, 1);
            tlpMain.Controls.Add(pnlTools, 0, 0);
            tlpMain.Controls.Add(pnlRowsCount, 0, 4);
            tlpMain.Controls.Add(pnlFilterCondition, 0, 3);
            tlpMain.Controls.Add(splProgress, 0, 2);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 5;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.Size = new System.Drawing.Size(1118, 621);
            tlpMain.TabIndex = 0;
            // 
            // grpData
            // 
            grpData.Controls.Add(pnlData);
            grpData.Dock = System.Windows.Forms.DockStyle.Fill;
            grpData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpData.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpData.Location = new System.Drawing.Point(6, 41);
            grpData.Margin = new System.Windows.Forms.Padding(6);
            grpData.Name = "grpData";
            grpData.Padding = new System.Windows.Forms.Padding(6);
            grpData.Size = new System.Drawing.Size(1106, 505);
            grpData.TabIndex = 11;
            grpData.TabStop = false;
            grpData.Text = "grpData";
            // 
            // pnlData
            // 
            pnlData.Controls.Add(dgvData);
            pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlData.Location = new System.Drawing.Point(6, 21);
            pnlData.Margin = new System.Windows.Forms.Padding(0);
            pnlData.Name = "pnlData";
            pnlData.Size = new System.Drawing.Size(1094, 478);
            pnlData.TabIndex = 1;
            // 
            // dgvData
            // 
            dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvData.Location = new System.Drawing.Point(0, 0);
            dgvData.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvData.Name = "dgvData";
            dgvData.Size = new System.Drawing.Size(1094, 478);
            dgvData.TabIndex = 0;
            // 
            // pnlTools
            // 
            pnlTools.Controls.Add(tsrTools);
            pnlTools.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTools.Location = new System.Drawing.Point(0, 0);
            pnlTools.Margin = new System.Windows.Forms.Padding(0);
            pnlTools.Name = "pnlTools";
            pnlTools.Size = new System.Drawing.Size(1118, 35);
            pnlTools.TabIndex = 2;
            // 
            // tsrTools
            // 
            tsrTools.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnFilterColumns, btnConfigureEstimate, btnCalculateEstimate, btnExportResults, btnClose, btnDisplayHistory, btnDisplayPanels, btnEditEstimationPeriod, btnEditPanelRefYearSetGroup, btnLoadEstimationCellCollection, btnLoadEstimationPeriod, btnLoadPhaseEstimateType, btnCancelRowFilter });
            tsrTools.Location = new System.Drawing.Point(0, 0);
            tsrTools.Name = "tsrTools";
            tsrTools.Padding = new System.Windows.Forms.Padding(0);
            tsrTools.Size = new System.Drawing.Size(1118, 35);
            tsrTools.TabIndex = 0;
            tsrTools.Text = "tsrTools";
            // 
            // btnFilterColumns
            // 
            btnFilterColumns.Image = (System.Drawing.Image)resources.GetObject("btnFilterColumns.Image");
            btnFilterColumns.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnFilterColumns.Margin = new System.Windows.Forms.Padding(0);
            btnFilterColumns.Name = "btnFilterColumns";
            btnFilterColumns.Size = new System.Drawing.Size(119, 35);
            btnFilterColumns.Text = "btnFilterColumns";
            // 
            // btnConfigureEstimate
            // 
            btnConfigureEstimate.Enabled = false;
            btnConfigureEstimate.Image = (System.Drawing.Image)resources.GetObject("btnConfigureEstimate.Image");
            btnConfigureEstimate.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnConfigureEstimate.Margin = new System.Windows.Forms.Padding(0);
            btnConfigureEstimate.Name = "btnConfigureEstimate";
            btnConfigureEstimate.Size = new System.Drawing.Size(143, 35);
            btnConfigureEstimate.Text = "btnConfigureEstimate";
            // 
            // btnCalculateEstimate
            // 
            btnCalculateEstimate.Enabled = false;
            btnCalculateEstimate.Image = (System.Drawing.Image)resources.GetObject("btnCalculateEstimate.Image");
            btnCalculateEstimate.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnCalculateEstimate.Margin = new System.Windows.Forms.Padding(0);
            btnCalculateEstimate.Name = "btnCalculateEstimate";
            btnCalculateEstimate.Size = new System.Drawing.Size(139, 35);
            btnCalculateEstimate.Text = "btnCalculateEstimate";
            // 
            // btnExportResults
            // 
            btnExportResults.Enabled = false;
            btnExportResults.Image = (System.Drawing.Image)resources.GetObject("btnExportResults.Image");
            btnExportResults.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnExportResults.Margin = new System.Windows.Forms.Padding(0);
            btnExportResults.Name = "btnExportResults";
            btnExportResults.Size = new System.Drawing.Size(116, 35);
            btnExportResults.Text = "btnExportResults";
            // 
            // btnClose
            // 
            btnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            btnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnClose.Image = (System.Drawing.Image)resources.GetObject("btnClose.Image");
            btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(23, 35);
            btnClose.Text = "btnClose";
            // 
            // btnDisplayHistory
            // 
            btnDisplayHistory.Image = (System.Drawing.Image)resources.GetObject("btnDisplayHistory.Image");
            btnDisplayHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnDisplayHistory.Margin = new System.Windows.Forms.Padding(0);
            btnDisplayHistory.Name = "btnDisplayHistory";
            btnDisplayHistory.Size = new System.Drawing.Size(121, 35);
            btnDisplayHistory.Text = "btnDisplayHistory";
            // 
            // btnDisplayPanels
            // 
            btnDisplayPanels.Enabled = false;
            btnDisplayPanels.Image = (System.Drawing.Image)resources.GetObject("btnDisplayPanels.Image");
            btnDisplayPanels.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnDisplayPanels.Margin = new System.Windows.Forms.Padding(0);
            btnDisplayPanels.Name = "btnDisplayPanels";
            btnDisplayPanels.Size = new System.Drawing.Size(117, 35);
            btnDisplayPanels.Text = "btnDisplayPanels";
            // 
            // btnEditEstimationPeriod
            // 
            btnEditEstimationPeriod.Image = (System.Drawing.Image)resources.GetObject("btnEditEstimationPeriod.Image");
            btnEditEstimationPeriod.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnEditEstimationPeriod.Name = "btnEditEstimationPeriod";
            btnEditEstimationPeriod.Size = new System.Drawing.Size(155, 32);
            btnEditEstimationPeriod.Text = "btnEditEstimationPeriod";
            // 
            // btnEditPanelRefYearSetGroup
            // 
            btnEditPanelRefYearSetGroup.Image = (System.Drawing.Image)resources.GetObject("btnEditPanelRefYearSetGroup.Image");
            btnEditPanelRefYearSetGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnEditPanelRefYearSetGroup.Name = "btnEditPanelRefYearSetGroup";
            btnEditPanelRefYearSetGroup.Size = new System.Drawing.Size(182, 20);
            btnEditPanelRefYearSetGroup.Text = "btnEditPanelRefYearSetGroup";
            // 
            // btnLoadEstimationCellCollection
            // 
            btnLoadEstimationCellCollection.Image = (System.Drawing.Image)resources.GetObject("btnLoadEstimationCellCollection.Image");
            btnLoadEstimationCellCollection.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLoadEstimationCellCollection.Name = "btnLoadEstimationCellCollection";
            btnLoadEstimationCellCollection.Size = new System.Drawing.Size(201, 20);
            btnLoadEstimationCellCollection.Text = "btnLoadEstimationCellCollection";
            // 
            // btnLoadEstimationPeriod
            // 
            btnLoadEstimationPeriod.Image = (System.Drawing.Image)resources.GetObject("btnLoadEstimationPeriod.Image");
            btnLoadEstimationPeriod.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLoadEstimationPeriod.Name = "btnLoadEstimationPeriod";
            btnLoadEstimationPeriod.Size = new System.Drawing.Size(161, 20);
            btnLoadEstimationPeriod.Text = "btnLoadEstimationPeriod";
            // 
            // btnLoadPhaseEstimateType
            // 
            btnLoadPhaseEstimateType.Image = (System.Drawing.Image)resources.GetObject("btnLoadPhaseEstimateType.Image");
            btnLoadPhaseEstimateType.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLoadPhaseEstimateType.Name = "btnLoadPhaseEstimateType";
            btnLoadPhaseEstimateType.Size = new System.Drawing.Size(171, 20);
            btnLoadPhaseEstimateType.Text = "btnLoadPhaseEstimateType";
            // 
            // btnCancelRowFilter
            // 
            btnCancelRowFilter.Image = (System.Drawing.Image)resources.GetObject("btnCancelRowFilter.Image");
            btnCancelRowFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnCancelRowFilter.Name = "btnCancelRowFilter";
            btnCancelRowFilter.Size = new System.Drawing.Size(130, 20);
            btnCancelRowFilter.Text = "btnCancelRowFilter";
            // 
            // pnlRowsCount
            // 
            pnlRowsCount.Controls.Add(lblRowsCount);
            pnlRowsCount.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlRowsCount.Location = new System.Drawing.Point(0, 598);
            pnlRowsCount.Margin = new System.Windows.Forms.Padding(0);
            pnlRowsCount.Name = "pnlRowsCount";
            pnlRowsCount.Size = new System.Drawing.Size(1118, 23);
            pnlRowsCount.TabIndex = 14;
            // 
            // lblRowsCount
            // 
            lblRowsCount.BackColor = System.Drawing.SystemColors.Control;
            lblRowsCount.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowsCount.Location = new System.Drawing.Point(0, 0);
            lblRowsCount.Margin = new System.Windows.Forms.Padding(0);
            lblRowsCount.Name = "lblRowsCount";
            lblRowsCount.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblRowsCount.Size = new System.Drawing.Size(1118, 23);
            lblRowsCount.TabIndex = 7;
            lblRowsCount.Text = "lblRowsCount";
            lblRowsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlFilterCondition
            // 
            pnlFilterCondition.Controls.Add(lblFilterCondition);
            pnlFilterCondition.Dock = System.Windows.Forms.DockStyle.Bottom;
            pnlFilterCondition.Location = new System.Drawing.Point(0, 575);
            pnlFilterCondition.Margin = new System.Windows.Forms.Padding(0);
            pnlFilterCondition.Name = "pnlFilterCondition";
            pnlFilterCondition.Size = new System.Drawing.Size(1118, 23);
            pnlFilterCondition.TabIndex = 13;
            // 
            // lblFilterCondition
            // 
            lblFilterCondition.BackColor = System.Drawing.SystemColors.Control;
            lblFilterCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            lblFilterCondition.Location = new System.Drawing.Point(0, 0);
            lblFilterCondition.Margin = new System.Windows.Forms.Padding(0);
            lblFilterCondition.Name = "lblFilterCondition";
            lblFilterCondition.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblFilterCondition.Size = new System.Drawing.Size(1118, 23);
            lblFilterCondition.TabIndex = 7;
            lblFilterCondition.Text = "lblFilterCondition";
            lblFilterCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splProgress
            // 
            splProgress.Dock = System.Windows.Forms.DockStyle.Fill;
            splProgress.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splProgress.IsSplitterFixed = true;
            splProgress.Location = new System.Drawing.Point(0, 552);
            splProgress.Margin = new System.Windows.Forms.Padding(0);
            splProgress.Name = "splProgress";
            // 
            // splProgress.Panel1
            // 
            splProgress.Panel1.Controls.Add(prgCalculation);
            splProgress.Panel1Collapsed = true;
            // 
            // splProgress.Panel2
            // 
            splProgress.Panel2.Controls.Add(lblCalculationStatus);
            splProgress.Size = new System.Drawing.Size(1118, 23);
            splProgress.SplitterDistance = 350;
            splProgress.SplitterWidth = 1;
            splProgress.TabIndex = 15;
            // 
            // prgCalculation
            // 
            prgCalculation.Dock = System.Windows.Forms.DockStyle.Fill;
            prgCalculation.Location = new System.Drawing.Point(0, 0);
            prgCalculation.Margin = new System.Windows.Forms.Padding(0);
            prgCalculation.Name = "prgCalculation";
            prgCalculation.Size = new System.Drawing.Size(350, 100);
            prgCalculation.TabIndex = 1;
            // 
            // lblCalculationStatus
            // 
            lblCalculationStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCalculationStatus.Location = new System.Drawing.Point(0, 0);
            lblCalculationStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblCalculationStatus.Name = "lblCalculationStatus";
            lblCalculationStatus.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblCalculationStatus.Size = new System.Drawing.Size(1118, 23);
            lblCalculationStatus.TabIndex = 0;
            lblCalculationStatus.Text = "lblCalculationStatus";
            lblCalculationStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlViewOLAP
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.SystemColors.Control;
            Controls.Add(pnlMain);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlViewOLAP";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(1120, 623);
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            grpData.ResumeLayout(false);
            pnlData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvData).EndInit();
            pnlTools.ResumeLayout(false);
            pnlTools.PerformLayout();
            tsrTools.ResumeLayout(false);
            tsrTools.PerformLayout();
            pnlRowsCount.ResumeLayout(false);
            pnlFilterCondition.ResumeLayout(false);
            splProgress.Panel1.ResumeLayout(false);
            splProgress.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splProgress).EndInit();
            splProgress.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlTools;
        private System.Windows.Forms.ToolStrip tsrTools;
        private System.Windows.Forms.ToolStripButton btnFilterColumns;
        private System.Windows.Forms.ToolStripButton btnConfigureEstimate;
        private System.Windows.Forms.ToolStripButton btnCalculateEstimate;
        private System.Windows.Forms.ToolStripButton btnDisplayPanels;
        private System.Windows.Forms.ToolStripButton btnDisplayHistory;
        private System.Windows.Forms.ToolStripButton btnExportResults;
        private System.Windows.Forms.GroupBox grpData;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.ToolStripButton btnClose;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.ToolStripButton btnEditEstimationPeriod;
        private System.Windows.Forms.Panel pnlRowsCount;
        private System.Windows.Forms.Label lblRowsCount;
        private System.Windows.Forms.Panel pnlFilterCondition;
        private System.Windows.Forms.Label lblFilterCondition;
        private System.Windows.Forms.ToolStripButton btnEditPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripButton btnLoadEstimationCellCollection;
        private System.Windows.Forms.ToolStripButton btnCancelRowFilter;
        private System.Windows.Forms.SplitContainer splProgress;
        private System.Windows.Forms.ProgressBar prgCalculation;
        private System.Windows.Forms.Label lblCalculationStatus;
        private System.Windows.Forms.ToolStripButton btnLoadPhaseEstimateType;
        private System.Windows.Forms.ToolStripButton btnLoadEstimationPeriod;
    }

}
