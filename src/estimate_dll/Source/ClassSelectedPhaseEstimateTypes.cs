﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using ZaJi.NfiEstaPg.Core;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Vybrané seznamy fází odhadů pro čitatel a jmenovatel</para>
        /// <para lang="en">Selected phase estimate type lists for numerator and denominator</para>
        /// </summary>
        internal class SelectedPhaseEstimateTypes
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vybraný seznam fází odhadů pro čitatel</para>
            /// <para lang="en">Selected phase estimate type list for numerator</para>
            /// </summary>
            private PhaseEstimateTypeList numerator;

            /// <summary>
            /// <para lang="cs">Vybraný seznam fází odhadů pro jmenovatel</para>
            /// <para lang="en">Selected phase estimate type list for denominator</para>
            /// </summary>
            private PhaseEstimateTypeList denominator;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// <para lang="cs">Vybraný seznam fází odhadů pro čitatel</para>
            /// <para lang="en">Selected phase estimate type list for numerator</para>
            /// </summary>
            public PhaseEstimateTypeList Numerator
            {
                get
                {
                    return numerator;
                }
                set
                {
                    numerator = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vybraný seznam fází odhadů pro jmenovatel</para>
            /// <para lang="en">Selected phase estimate type list for denominator</para>
            /// </summary>
            public PhaseEstimateTypeList Denominator
            {
                get
                {
                    return denominator;
                }
                set
                {
                    denominator = value;
                }
            }

            #endregion Properties

        }

    }
}
