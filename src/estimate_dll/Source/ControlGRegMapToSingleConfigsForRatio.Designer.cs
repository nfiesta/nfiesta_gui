﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlGRegMapToSingleConfigsForRatio
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpData = new System.Windows.Forms.TableLayoutPanel();
            lblNumeratorForceSyntheticValue = new System.Windows.Forms.Label();
            lblNumeratorForceSyntheticCaption = new System.Windows.Forms.Label();
            lblNumeratorModelSigmaValue = new System.Windows.Forms.Label();
            lblNumeratorModelSigmaCaption = new System.Windows.Forms.Label();
            lblNumeratorParamAreaTypeValue = new System.Windows.Forms.Label();
            lblNumeratorParamAreaTypeCaption = new System.Windows.Forms.Label();
            lblNumeratorModelValue = new System.Windows.Forms.Label();
            lblNumeratorModelCaption = new System.Windows.Forms.Label();
            lblPanelRefYearSetGroupValue = new System.Windows.Forms.Label();
            lblPanelRefYearSetGroupCaption = new System.Windows.Forms.Label();
            rdoSelected = new System.Windows.Forms.RadioButton();
            lblAllEstimatesConfigurableCaption = new System.Windows.Forms.Label();
            lblAllEstimatesConfigurableValue = new System.Windows.Forms.Label();
            tlpMain.SuspendLayout();
            tlpData.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.WhiteSmoke;
            tlpMain.ColumnCount = 2;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpData, 1, 0);
            tlpMain.Controls.Add(rdoSelected, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(5, 5);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 1;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Size = new System.Drawing.Size(950, 60);
            tlpMain.TabIndex = 2;
            // 
            // tlpData
            // 
            tlpData.ColumnCount = 6;
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpData.Controls.Add(lblNumeratorForceSyntheticValue, 1, 2);
            tlpData.Controls.Add(lblNumeratorForceSyntheticCaption, 0, 2);
            tlpData.Controls.Add(lblNumeratorModelSigmaValue, 4, 2);
            tlpData.Controls.Add(lblNumeratorModelSigmaCaption, 3, 2);
            tlpData.Controls.Add(lblNumeratorModelValue, 4, 1);
            tlpData.Controls.Add(lblNumeratorModelCaption, 3, 1);
            tlpData.Controls.Add(lblNumeratorParamAreaTypeValue, 4, 0);
            tlpData.Controls.Add(lblNumeratorParamAreaTypeCaption, 3, 0);
            tlpData.Controls.Add(lblPanelRefYearSetGroupCaption, 0, 1);
            tlpData.Controls.Add(lblPanelRefYearSetGroupValue, 1, 1);
            tlpData.Controls.Add(lblAllEstimatesConfigurableCaption, 0, 0);
            tlpData.Controls.Add(lblAllEstimatesConfigurableValue, 1, 0);
            tlpData.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpData.Location = new System.Drawing.Point(40, 0);
            tlpData.Margin = new System.Windows.Forms.Padding(0);
            tlpData.Name = "tlpData";
            tlpData.RowCount = 4;
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpData.Size = new System.Drawing.Size(910, 60);
            tlpData.TabIndex = 1;
            // 
            // lblNumeratorForceSyntheticValue
            // 
            lblNumeratorForceSyntheticValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorForceSyntheticValue.Location = new System.Drawing.Point(200, 40);
            lblNumeratorForceSyntheticValue.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorForceSyntheticValue.Name = "lblNumeratorForceSyntheticValue";
            lblNumeratorForceSyntheticValue.Size = new System.Drawing.Size(250, 20);
            lblNumeratorForceSyntheticValue.TabIndex = 13;
            lblNumeratorForceSyntheticValue.Text = "lblNumeratorForceSyntheticValue";
            lblNumeratorForceSyntheticValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorForceSyntheticCaption
            // 
            lblNumeratorForceSyntheticCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorForceSyntheticCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblNumeratorForceSyntheticCaption.Location = new System.Drawing.Point(0, 40);
            lblNumeratorForceSyntheticCaption.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorForceSyntheticCaption.Name = "lblNumeratorForceSyntheticCaption";
            lblNumeratorForceSyntheticCaption.Size = new System.Drawing.Size(200, 20);
            lblNumeratorForceSyntheticCaption.TabIndex = 12;
            lblNumeratorForceSyntheticCaption.Text = "lblNumeratorForceSyntheticCaption";
            lblNumeratorForceSyntheticCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelSigmaValue
            // 
            lblNumeratorModelSigmaValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelSigmaValue.Location = new System.Drawing.Point(660, 40);
            lblNumeratorModelSigmaValue.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelSigmaValue.Name = "lblNumeratorModelSigmaValue";
            lblNumeratorModelSigmaValue.Size = new System.Drawing.Size(250, 20);
            lblNumeratorModelSigmaValue.TabIndex = 10;
            lblNumeratorModelSigmaValue.Text = "lblNumeratorModelSigmaValue";
            lblNumeratorModelSigmaValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelSigmaCaption
            // 
            lblNumeratorModelSigmaCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelSigmaCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblNumeratorModelSigmaCaption.Location = new System.Drawing.Point(460, 40);
            lblNumeratorModelSigmaCaption.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelSigmaCaption.Name = "lblNumeratorModelSigmaCaption";
            lblNumeratorModelSigmaCaption.Size = new System.Drawing.Size(200, 20);
            lblNumeratorModelSigmaCaption.TabIndex = 9;
            lblNumeratorModelSigmaCaption.Text = "lblNumeratorModelSigmaCaption";
            lblNumeratorModelSigmaCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorParamAreaTypeValue
            // 
            lblNumeratorParamAreaTypeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorParamAreaTypeValue.Location = new System.Drawing.Point(660, 0);
            lblNumeratorParamAreaTypeValue.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorParamAreaTypeValue.Name = "lblNumeratorParamAreaTypeValue";
            lblNumeratorParamAreaTypeValue.Size = new System.Drawing.Size(250, 20);
            lblNumeratorParamAreaTypeValue.TabIndex = 7;
            lblNumeratorParamAreaTypeValue.Text = "lblNumeratorParamAreaTypeValue";
            lblNumeratorParamAreaTypeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorParamAreaTypeCaption
            // 
            lblNumeratorParamAreaTypeCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorParamAreaTypeCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblNumeratorParamAreaTypeCaption.Location = new System.Drawing.Point(460, 0);
            lblNumeratorParamAreaTypeCaption.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorParamAreaTypeCaption.Name = "lblNumeratorParamAreaTypeCaption";
            lblNumeratorParamAreaTypeCaption.Size = new System.Drawing.Size(200, 20);
            lblNumeratorParamAreaTypeCaption.TabIndex = 6;
            lblNumeratorParamAreaTypeCaption.Text = "lblNumeratorParamAreaTypeCaption";
            lblNumeratorParamAreaTypeCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelValue
            // 
            lblNumeratorModelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelValue.Location = new System.Drawing.Point(660, 20);
            lblNumeratorModelValue.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelValue.Name = "lblNumeratorModelValue";
            lblNumeratorModelValue.Size = new System.Drawing.Size(250, 20);
            lblNumeratorModelValue.TabIndex = 4;
            lblNumeratorModelValue.Text = "lblNumeratorModelValue";
            lblNumeratorModelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelCaption
            // 
            lblNumeratorModelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblNumeratorModelCaption.Location = new System.Drawing.Point(460, 20);
            lblNumeratorModelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelCaption.Name = "lblNumeratorModelCaption";
            lblNumeratorModelCaption.Size = new System.Drawing.Size(200, 20);
            lblNumeratorModelCaption.TabIndex = 3;
            lblNumeratorModelCaption.Text = "lblNumeratorModelCaption";
            lblNumeratorModelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupValue
            // 
            lblPanelRefYearSetGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelRefYearSetGroupValue.Location = new System.Drawing.Point(200, 20);
            lblPanelRefYearSetGroupValue.Margin = new System.Windows.Forms.Padding(0);
            lblPanelRefYearSetGroupValue.Name = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.Size = new System.Drawing.Size(250, 20);
            lblPanelRefYearSetGroupValue.TabIndex = 1;
            lblPanelRefYearSetGroupValue.Text = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupCaption
            // 
            lblPanelRefYearSetGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelRefYearSetGroupCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblPanelRefYearSetGroupCaption.Location = new System.Drawing.Point(0, 20);
            lblPanelRefYearSetGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            lblPanelRefYearSetGroupCaption.Name = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.Size = new System.Drawing.Size(200, 20);
            lblPanelRefYearSetGroupCaption.TabIndex = 0;
            lblPanelRefYearSetGroupCaption.Text = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoSelected
            // 
            rdoSelected.AutoSize = true;
            rdoSelected.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            rdoSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            rdoSelected.Location = new System.Drawing.Point(0, 0);
            rdoSelected.Margin = new System.Windows.Forms.Padding(0);
            rdoSelected.Name = "rdoSelected";
            rdoSelected.Size = new System.Drawing.Size(40, 60);
            rdoSelected.TabIndex = 0;
            rdoSelected.TabStop = true;
            rdoSelected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            rdoSelected.UseVisualStyleBackColor = true;
            // 
            // lblAllEstimatesConfigurableCaption
            // 
            lblAllEstimatesConfigurableCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAllEstimatesConfigurableCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAllEstimatesConfigurableCaption.Location = new System.Drawing.Point(0, 0);
            lblAllEstimatesConfigurableCaption.Margin = new System.Windows.Forms.Padding(0);
            lblAllEstimatesConfigurableCaption.Name = "lblAllEstimatesConfigurableCaption";
            lblAllEstimatesConfigurableCaption.Size = new System.Drawing.Size(200, 20);
            lblAllEstimatesConfigurableCaption.TabIndex = 14;
            lblAllEstimatesConfigurableCaption.Text = "lblAllEstimatesConfigurableCaption";
            lblAllEstimatesConfigurableCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAllEstimatesConfigurableValue
            // 
            lblAllEstimatesConfigurableValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAllEstimatesConfigurableValue.Location = new System.Drawing.Point(200, 0);
            lblAllEstimatesConfigurableValue.Margin = new System.Windows.Forms.Padding(0);
            lblAllEstimatesConfigurableValue.Name = "lblAllEstimatesConfigurableValue";
            lblAllEstimatesConfigurableValue.Size = new System.Drawing.Size(250, 20);
            lblAllEstimatesConfigurableValue.TabIndex = 15;
            lblAllEstimatesConfigurableValue.Text = "lblAllEstimatesConfigurableValue";
            lblAllEstimatesConfigurableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlGRegMapToSingleConfigsForRatio
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Silver;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlGRegMapToSingleConfigsForRatio";
            Padding = new System.Windows.Forms.Padding(5);
            Size = new System.Drawing.Size(960, 70);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            tlpData.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpData;
        private System.Windows.Forms.Label lblNumeratorForceSyntheticValue;
        private System.Windows.Forms.Label lblNumeratorForceSyntheticCaption;
        private System.Windows.Forms.Label lblNumeratorModelSigmaValue;
        private System.Windows.Forms.Label lblNumeratorModelSigmaCaption;
        private System.Windows.Forms.Label lblNumeratorParamAreaTypeValue;
        private System.Windows.Forms.Label lblNumeratorParamAreaTypeCaption;
        private System.Windows.Forms.Label lblNumeratorModelValue;
        private System.Windows.Forms.Label lblNumeratorModelCaption;
        private System.Windows.Forms.Label lblPanelRefYearSetGroupValue;
        private System.Windows.Forms.Label lblPanelRefYearSetGroupCaption;
        private System.Windows.Forms.RadioButton rdoSelected;
        private System.Windows.Forms.Label lblAllEstimatesConfigurableCaption;
        private System.Windows.Forms.Label lblAllEstimatesConfigurableValue;
    }

}