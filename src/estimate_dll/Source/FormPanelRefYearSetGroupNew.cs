﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nové skupiny panelů a roků měření</para>
    /// <para lang="en">Form to create a new group of panels and reference year sets</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormPanelRefYearSetGroupNew
        : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Nová skupina panelů a roků měření</para>
        /// <para lang="en">New group of panels and reference year sets</para>
        /// </summary>
        private PanelRefYearSetGroup panelReferenceYearSetGroup;

        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;
        private string msgLabelCsTooLong = String.Empty;
        private string msgLabelEnTooLong = String.Empty;
        private string msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone = String.Empty;
        private string msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormPanelRefYearSetGroupNew(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Nová skupina panelů a roků měření</para>
        /// <para lang="en">New group of panels and reference year sets</para>
        /// </summary>
        public PanelRefYearSetGroup PanelReferenceYearSetGroup
        {
            get
            {
                return
                    panelReferenceYearSetGroup ??
                        new PanelRefYearSetGroup(
                            data: PanelRefYearSetGroupList.EmptyDataTable().NewRow());
            }
            set
            {
                panelReferenceYearSetGroup =
                    value ??
                        new PanelRefYearSetGroup(
                            data: PanelRefYearSetGroupList.EmptyDataTable().NewRow());
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPanelRefYearSetGroupNew),  "Nová skupina panelů a referenčních období" },
                        { nameof(btnCancel),                    "Zrušit" },
                        { nameof(btnOK),                        "Vytvořit" },
                        { nameof(grpNewPanelRefYearSetGroup),   "Nová skupina panelů a referenčních období:" },
                        { nameof(lblLabelCsCaption),            "Zkratka (národní):" },
                        { nameof(lblDescriptionCsCaption),      "Popis (národní):" },
                        { nameof(lblLabelEnCaption),            "Zkratka (anglická):" },
                        { nameof(lblDescriptionEnCaption),      "Popis (anglický):"  },
                        { nameof(msgLabelCsIsEmpty),            "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),      "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),            "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),      "Popis (anglický) musí být vyplněn." },
                        { nameof(msgLabelCsExists),             "Použitá zkratka (národní) již existuje pro jinou skupinu panelů a referenčních období." },
                        { nameof(msgDescriptionCsExists),       "Použitý popis (národní) již existuje pro jinou skupinu panelů a referenčních období." },
                        { nameof(msgLabelEnExists),             "Použitá zkratka (anglická) již existuje pro jinou skupinu panelů a referenčních období." },
                        { nameof(msgDescriptionEnExists),       "Použitý popis (anglický) již existuje pro jinou skupinu panelů a referenčních období." },
                        { nameof(msgLabelCsTooLong),            "Zkratka (národní) je příliš dlouhá." },
                        { nameof(msgLabelEnTooLong),            "Zkratka (anglická) je příliš dlouhá." },
                        { nameof(msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone),
                                                                "Uložená procedura $1 nevrátila žádný záznam. (očekává se jeden záznam)" },
                        { nameof(msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany),
                                                                "Uložená procedura $1 vrátila mnoho záznamů. (očekává se jeden záznam)" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormPanelRefYearSetGroupNew),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPanelRefYearSetGroupNew),  "New group of panels and reference year sets" },
                        { nameof(btnCancel),                    "Cancel" },
                        { nameof(btnOK),                        "Insert" },
                        { nameof(grpNewPanelRefYearSetGroup),   "New group of panels and reference year sets:" },
                        { nameof(lblLabelCsCaption),            "Label (national):" },
                        { nameof(lblDescriptionCsCaption),      "Description (national):" },
                        { nameof(lblLabelEnCaption),            "Label (English):" },
                        { nameof(lblDescriptionEnCaption),      "Description (English):" },
                        { nameof(msgLabelCsIsEmpty),            "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),      "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),            "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),      "Description (English) cannot be empty." },
                        { nameof(msgLabelCsExists),             "This label (national) already exists for another group of panels and reference year sets." },
                        { nameof(msgDescriptionCsExists),       "This description (national) already exists for another group of panels and reference year sets." },
                        { nameof(msgLabelEnExists),             "This label (English) already exists for another group of panels and reference year sets." },
                        { nameof(msgDescriptionEnExists),       "This description (English) already exists for another group of panels and reference year sets." },
                        { nameof(msgLabelCsTooLong),            "Label (national) is too long." },
                        { nameof(msgLabelEnTooLong),            "Label (English) is too long." },
                        { nameof(msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone),
                                                                "Stored procedure $1 doesn't return any row. (one row expected)" },
                        { nameof(msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany),
                                                                "Stored procedure $1 returns too many rows. (one row expected)"  }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormPanelRefYearSetGroupNew),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            PanelReferenceYearSetGroup =
                new PanelRefYearSetGroup(
                            data: PanelRefYearSetGroupList.EmptyDataTable().NewRow());

            InitializeLabels();

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !Save();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormPanelRefYearSetGroupNew),
                    out string frmPanelRefYearSetGroupNewText)
                        ? frmPanelRefYearSetGroupNewText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                    out string btnInsertText)
                        ? btnInsertText
                        : String.Empty;

            grpNewPanelRefYearSetGroup.Text =
                labels.TryGetValue(key: nameof(grpNewPanelRefYearSetGroup),
                    out string grpNewPanelRefYearSetGroupText)
                        ? grpNewPanelRefYearSetGroupText
                        : String.Empty;

            lblLabelCsCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCsCaption),
                    out string lblLabelCsCaptionText)
                        ? lblLabelCsCaptionText
                        : String.Empty;

            lblDescriptionCsCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionCsCaption),
                    out string lblDescriptionCsCaptionText)
                        ? lblDescriptionCsCaptionText
                        : String.Empty;

            lblLabelEnCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelEnCaption),
                    out string lblLabelEnCaptionText)
                        ? lblLabelEnCaptionText
                        : String.Empty;

            lblDescriptionEnCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionEnCaption),
                    out string lblDescriptionEnCaptionText)
                        ? lblDescriptionEnCaptionText
                        : String.Empty;

            msgLabelCsIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                    out msgLabelCsIsEmpty)
                        ? msgLabelCsIsEmpty
                        : String.Empty;

            msgDescriptionCsIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                    out msgDescriptionCsIsEmpty)
                        ? msgDescriptionCsIsEmpty
                        : String.Empty;

            msgLabelEnIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                    out msgLabelEnIsEmpty)
                        ? msgLabelEnIsEmpty
                        : String.Empty;

            msgDescriptionEnIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                    out msgDescriptionEnIsEmpty)
                        ? msgDescriptionEnIsEmpty
                        : String.Empty;

            msgLabelCsExists =
                labels.TryGetValue(key: nameof(msgLabelCsExists),
                    out msgLabelCsExists)
                        ? msgLabelCsExists
                        : String.Empty;

            msgDescriptionCsExists =
                labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                    out msgDescriptionCsExists)
                        ? msgDescriptionCsExists
                        : String.Empty;

            msgLabelEnExists =
                labels.TryGetValue(key: nameof(msgLabelEnExists),
                    out msgLabelEnExists)
                        ? msgLabelEnExists
                        : String.Empty;

            msgDescriptionEnExists =
                labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                    out msgDescriptionEnExists)
                        ? msgDescriptionEnExists
                        : String.Empty;

            msgLabelCsTooLong =
                labels.TryGetValue(key: nameof(msgLabelCsTooLong),
                    out msgLabelCsTooLong)
                        ? msgLabelCsTooLong
                        : String.Empty;

            msgLabelEnTooLong =
                labels.TryGetValue(key: nameof(msgLabelEnTooLong),
                    out msgLabelEnTooLong)
                        ? msgLabelEnTooLong
                        : String.Empty;

            msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone =
                labels.TryGetValue(key: nameof(msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone),
                    out msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone)
                        ? msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone
                        : String.Empty;

            msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany =
               labels.TryGetValue(key: nameof(msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany),
                   out msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany)
                       ? msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany
                       : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nastaví vlastnosti objektu nové skupiny panelů a roků měření</para>
        /// <para lang="en">Sets the object properties of the new group of panels and reference year sets</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud je možné uložit objekt nové skupiny panelů a roků měření do databázové tabulky, jinak false</para>
        /// <para lang="en">It returns true if it is possible to save the new group of panels and reference year sets object into database table, else false</para>
        /// </returns>
        private bool Save()
        {
            PanelReferenceYearSetGroup = new(
                    data: PanelRefYearSetGroupList.EmptyDataTable().NewRow())
            {
                LabelCs = txtLabelCsValue.Text.Trim(),
                LabelEn = txtLabelEnValue.Text.Trim(),
                DescriptionCs = txtDescriptionCsValue.Text.Trim(),
                DescriptionEn = txtDescriptionEnValue.Text.Trim()
            };

            if (String.IsNullOrEmpty(value: PanelReferenceYearSetGroup.LabelCs))
            {
                // LabelCs - prázdné
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(value: PanelReferenceYearSetGroup.LabelEn))
            {
                // LabelEn - prázdné
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(value: PanelReferenceYearSetGroup.DescriptionCs))
            {
                // DescriptionCs - prázdné
                MessageBox.Show(
                    text: msgDescriptionCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(value: PanelReferenceYearSetGroup.DescriptionEn))
            {
                // DescriptionEn - prázdné
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (PanelReferenceYearSetGroup.LabelCs.Length > PanelRefYearSetGroup.LabelCsMaxLength)
            {
                // LabelCs - moc dlouhé
                MessageBox.Show(
                    text: msgLabelCsTooLong,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (PanelReferenceYearSetGroup.LabelEn.Length > PanelRefYearSetGroup.LabelEnMaxLength)
            {
                // LabelEn - moc dlouhé
                MessageBox.Show(
                    text: msgLabelEnTooLong,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            Database.Postgres.ExceptionFlag = false;
            TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList uniqueChecks =
                NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist.Execute(
                database: Database,
                labelCs: PanelReferenceYearSetGroup.LabelCs,
                labelEn: PanelReferenceYearSetGroup.LabelEn,
                descriptionCs: PanelReferenceYearSetGroup.DescriptionCs,
                descriptionEn: PanelReferenceYearSetGroup.DescriptionEn);

            if (Database.Postgres.ExceptionFlag)
            {
                return false;
            }

            if ((uniqueChecks == null) || (uniqueChecks.Items.Count == 0))
            {
                MessageBox.Show(
                   text: msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistNone,
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);
                return false;
            }

            if (uniqueChecks.Items.Count != 1)
            {
                MessageBox.Show(
                   text: msgFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistMany,
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);

                return false;
            }

            TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist uniqueCheck =
                uniqueChecks.Items[0];

            if (uniqueCheck.LabelCsFound ?? false)
            {
                // LabelCs - duplicitní
                MessageBox.Show(
                    text: msgLabelCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (uniqueCheck.LabelEnFound ?? false)
            {
                // LabelEn - duplicitní
                MessageBox.Show(
                    text: msgLabelEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (uniqueCheck.DescriptionCsFound ?? false)
            {
                // DescriptionCs - duplicitní
                MessageBox.Show(
                    text: msgDescriptionCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (uniqueCheck.DescriptionEnFound ?? false)
            {
                // DescriptionEn - duplicitní
                MessageBox.Show(
                    text: msgDescriptionEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            return true;
        }

        #endregion Methods

    }

}