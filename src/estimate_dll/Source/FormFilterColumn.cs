﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro nastavení filtru sloupců</para>
    /// <para lang="en">Form for setting column filter</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormFilterColumn
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormFilterColumn(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlViewOLAP)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlViewOLAP)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlViewOLAP)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlViewOLAP)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)</para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)</para>
        /// </summary>
        public DisplayType DisplayType
        {
            get
            {
                return ((ControlViewOLAP)ControlOwner).DisplayType;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazené sloupce (read-only)</para>
        /// <para lang="en">Displayed columns (read-only)</para>
        /// </summary>
        public string[] VisibleColumns
        {
            get
            {
                return
                    pnlColumnList.Controls.OfType<ControlColumnMetadata>()
                        .Where(a => a.Checked)
                        .OrderBy(a => a.Top)
                        .Select(a => a.Metadata.Name)
                        .ToArray<string>();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormFilterColumn),     "Výběr sloupců tabulky" },
                        { nameof(btnCancel),            "Zrušit" },
                        { nameof(btnOK),                "OK" },
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormFilterColumn),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormFilterColumn),     "Table columns selection" },
                        { nameof(btnCancel),            "Cancel" },
                        { nameof(btnOK),                "OK" },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormFilterColumn),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            InitializeLabels();

            LoadContent();

            InitializeList();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormFilterColumn),
                out string frmFilterColumnText)
                    ? frmFilterColumnText
                    : String.Empty;

            btnOK.Text =
               labels.TryGetValue(key: nameof(btnOK),
               out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
               labels.TryGetValue(key: nameof(btnCancel),
               out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            foreach (ControlColumnMetadata control in
                pnlColumnList.Controls.OfType<ControlColumnMetadata>())
            {
                control.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace zaškrtávacího seznamu jmen sloupců</para>
        /// <para lang="en">
        /// Initializing checked list box with column names</para>
        /// </summary>
        private void InitializeList()
        {
            bool dragging = false;
            int xOffset = 0;
            int yOffset = 0;

            pnlColumnList.Controls.Clear();
            List<ColumnMetadata> columns = DisplayType switch
            {
                DisplayType.Total =>
                    [.. OLAPTotalEstimateList.Cols.Values],
                DisplayType.Ratio =>
                    [.. OLAPRatioEstimateList.Cols.Values],
                _ =>
                    throw new ArgumentException(
                        message: $"Argument {nameof(DisplayType)} unknown value.",
                        paramName: nameof(DisplayType)),
            };
            int i = -1;
            foreach (ColumnMetadata column in columns
                .OrderByDescending(a => a.Visible)
                .ThenBy(a => a.DisplayIndex))
            {
                i++;

                ControlColumnMetadata control = new(
                    controlOwner: this,
                    metadata: column)
                {
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    Height = 20,
                    Left = 25,
                    Top = (i * 25) + 10,
                    EnableEditLabels = true
                };

                control.ControlMouseDown += new MouseEventHandler(
                    (sender, e) =>
                    {
                        if (e.Button == MouseButtons.Right)
                        {
                            dragging = true;
                            ControlColumnMetadata ctrSender = (ControlColumnMetadata)sender;
                            pnlColumnList.Controls.SetChildIndex(
                                child: ctrSender,
                                newIndex: 0);
                            xOffset = e.X;
                            yOffset = e.Y;
                            ctrSender.Selected = true;
                        }
                    });

                control.ControlMouseMove += new MouseEventHandler(
                    (sender, e) =>
                    {
                        if (dragging)
                        {
                            ControlColumnMetadata ctrSender = (ControlColumnMetadata)sender;

                            int xMoved = e.Location.X - xOffset;
                            int yMoved = e.Location.Y - yOffset;

                            int newPosX = ctrSender.Location.X + xMoved;
                            int newPosY = ctrSender.Location.Y + yMoved;

                            ctrSender.Location = new Point(x: newPosX, y: newPosY);
                        }
                    });

                control.ControlMouseUp += new MouseEventHandler(
                    (sender, e) =>
                    {
                        if (dragging)
                        {
                            dragging = false;
                            ControlColumnMetadata ctrSender = (ControlColumnMetadata)sender;

                            List<ControlColumnMetadata> controls =
                                [.. pnlColumnList.Controls
                                    .OfType<ControlColumnMetadata>()
                                    .OrderBy(a => a.Top)];

                            pnlColumnList.AutoScroll = false;
                            int j = -1;
                            foreach (ControlColumnMetadata ctrControl in controls)
                            {
                                j++;
                                ctrControl.Location = new Point(x: 25, y: (j * 25) + 10);
                            }
                            pnlColumnList.AutoScroll = true;

                            ctrSender.Selected = false;
                        }
                    });

                control.CreateControl();
                pnlColumnList.Controls.Add(value: control);
            };

            InitializeLabels();
        }

        #endregion Methods

    }

}