﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace ModuleEstimate
    {

        partial class ControlVariable
        {
            /// <summary> 
            /// Vyžaduje se proměnná návrháře.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Uvolněte všechny používané prostředky.
            /// </summary>
            /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Kód vygenerovaný pomocí Návrháře komponent

            /// <summary> 
            /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
            /// obsah této metody v editoru kódu.
            /// </summary>
            private void InitializeComponent()
            {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlPrevious = new System.Windows.Forms.Panel();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.pnlNext = new System.Windows.Forms.Panel();
            this.btnNext = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splCaption = new System.Windows.Forms.SplitContainer();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.grpIndicator = new System.Windows.Forms.GroupBox();
            this.pnlIndicator = new System.Windows.Forms.Panel();
            this.splVariable = new System.Windows.Forms.SplitContainer();
            this.tlpControls = new System.Windows.Forms.TableLayoutPanel();
            this.tlpSubPopulation = new System.Windows.Forms.TableLayoutPanel();
            this.pnlSubPopulationCategories = new System.Windows.Forms.Panel();
            this.grpSubPopulation = new System.Windows.Forms.GroupBox();
            this.cboSubPopulation = new System.Windows.Forms.ComboBox();
            this.tlpAreaDomain = new System.Windows.Forms.TableLayoutPanel();
            this.grpAreaDomain = new System.Windows.Forms.GroupBox();
            this.cboAreaDomain = new System.Windows.Forms.ComboBox();
            this.pnlAreaDomainCategories = new System.Windows.Forms.Panel();
            this.pnlAttributeCategories = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlPrevious.SuspendLayout();
            this.pnlNext.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).BeginInit();
            this.splCaption.Panel1.SuspendLayout();
            this.splCaption.Panel2.SuspendLayout();
            this.splCaption.SuspendLayout();
            this.pnlCaption.SuspendLayout();
            this.grpIndicator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splVariable)).BeginInit();
            this.splVariable.Panel1.SuspendLayout();
            this.splVariable.Panel2.SuspendLayout();
            this.splVariable.SuspendLayout();
            this.tlpControls.SuspendLayout();
            this.tlpSubPopulation.SuspendLayout();
            this.grpSubPopulation.SuspendLayout();
            this.tlpAreaDomain.SuspendLayout();
            this.grpAreaDomain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.SystemColors.Control;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 1);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(3, 3);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(954, 534);
            this.tlpMain.TabIndex = 0;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.Controls.Add(this.pnlPrevious, 1, 0);
            this.tlpButtons.Controls.Add(this.pnlNext, 2, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 494);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(954, 40);
            this.tlpButtons.TabIndex = 12;
            // 
            // pnlPrevious
            // 
            this.pnlPrevious.Controls.Add(this.btnPrevious);
            this.pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrevious.Location = new System.Drawing.Point(634, 0);
            this.pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPrevious.Name = "pnlPrevious";
            this.pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPrevious.Size = new System.Drawing.Size(160, 40);
            this.pnlPrevious.TabIndex = 17;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrevious.Location = new System.Drawing.Point(5, 5);
            this.btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(150, 30);
            this.btnPrevious.TabIndex = 15;
            this.btnPrevious.Text = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlNext
            // 
            this.pnlNext.Controls.Add(this.btnNext);
            this.pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNext.Location = new System.Drawing.Point(794, 0);
            this.pnlNext.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNext.Name = "pnlNext";
            this.pnlNext.Padding = new System.Windows.Forms.Padding(5);
            this.pnlNext.Size = new System.Drawing.Size(160, 40);
            this.pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnNext.Location = new System.Drawing.Point(5, 5);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 15;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlWorkSpace.Controls.Add(this.splCaption);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 494);
            this.pnlWorkSpace.TabIndex = 0;
            // 
            // splCaption
            // 
            this.splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCaption.Location = new System.Drawing.Point(0, 0);
            this.splCaption.Margin = new System.Windows.Forms.Padding(0);
            this.splCaption.Name = "splCaption";
            this.splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            this.splCaption.Panel1.Controls.Add(this.pnlCaption);
            this.splCaption.Panel1MinSize = 0;
            // 
            // splCaption.Panel2
            // 
            this.splCaption.Panel2.Controls.Add(this.splVariable);
            this.splCaption.Panel2MinSize = 0;
            this.splCaption.Size = new System.Drawing.Size(954, 491);
            this.splCaption.SplitterDistance = 150;
            this.splCaption.SplitterWidth = 1;
            this.splCaption.TabIndex = 0;
            // 
            // pnlCaption
            // 
            this.pnlCaption.BackColor = System.Drawing.SystemColors.Control;
            this.pnlCaption.Controls.Add(this.grpIndicator);
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Padding = new System.Windows.Forms.Padding(5);
            this.pnlCaption.Size = new System.Drawing.Size(954, 150);
            this.pnlCaption.TabIndex = 7;
            // 
            // grpIndicator
            // 
            this.grpIndicator.Controls.Add(this.pnlIndicator);
            this.grpIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpIndicator.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpIndicator.Location = new System.Drawing.Point(5, 5);
            this.grpIndicator.Margin = new System.Windows.Forms.Padding(0);
            this.grpIndicator.Name = "grpIndicator";
            this.grpIndicator.Padding = new System.Windows.Forms.Padding(5);
            this.grpIndicator.Size = new System.Drawing.Size(944, 140);
            this.grpIndicator.TabIndex = 7;
            this.grpIndicator.TabStop = false;
            this.grpIndicator.Text = "grpIndicator";
            // 
            // pnlIndicator
            // 
            this.pnlIndicator.BackColor = System.Drawing.SystemColors.Control;
            this.pnlIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlIndicator.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlIndicator.Location = new System.Drawing.Point(5, 20);
            this.pnlIndicator.Margin = new System.Windows.Forms.Padding(0);
            this.pnlIndicator.Name = "pnlIndicator";
            this.pnlIndicator.Size = new System.Drawing.Size(934, 115);
            this.pnlIndicator.TabIndex = 7;
            // 
            // splVariable
            // 
            this.splVariable.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splVariable.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splVariable.Location = new System.Drawing.Point(0, 0);
            this.splVariable.Margin = new System.Windows.Forms.Padding(0);
            this.splVariable.Name = "splVariable";
            this.splVariable.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splVariable.Panel1
            // 
            this.splVariable.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splVariable.Panel1.Controls.Add(this.tlpControls);
            this.splVariable.Panel1MinSize = 0;
            // 
            // splVariable.Panel2
            // 
            this.splVariable.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splVariable.Panel2.Controls.Add(this.pnlAttributeCategories);
            this.splVariable.Panel2MinSize = 0;
            this.splVariable.Size = new System.Drawing.Size(954, 340);
            this.splVariable.SplitterDistance = 170;
            this.splVariable.SplitterWidth = 1;
            this.splVariable.TabIndex = 7;
            // 
            // tlpControls
            // 
            this.tlpControls.ColumnCount = 2;
            this.tlpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpControls.Controls.Add(this.tlpSubPopulation, 0, 0);
            this.tlpControls.Controls.Add(this.tlpAreaDomain, 0, 0);
            this.tlpControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpControls.Location = new System.Drawing.Point(0, 0);
            this.tlpControls.Margin = new System.Windows.Forms.Padding(0);
            this.tlpControls.Name = "tlpControls";
            this.tlpControls.RowCount = 1;
            this.tlpControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpControls.Size = new System.Drawing.Size(954, 170);
            this.tlpControls.TabIndex = 7;
            // 
            // tlpSubPopulation
            // 
            this.tlpSubPopulation.ColumnCount = 1;
            this.tlpSubPopulation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSubPopulation.Controls.Add(this.pnlSubPopulationCategories, 0, 1);
            this.tlpSubPopulation.Controls.Add(this.grpSubPopulation, 0, 0);
            this.tlpSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSubPopulation.Location = new System.Drawing.Point(477, 0);
            this.tlpSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            this.tlpSubPopulation.Name = "tlpSubPopulation";
            this.tlpSubPopulation.RowCount = 2;
            this.tlpSubPopulation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpSubPopulation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSubPopulation.Size = new System.Drawing.Size(477, 170);
            this.tlpSubPopulation.TabIndex = 2;
            // 
            // pnlSubPopulationCategories
            // 
            this.pnlSubPopulationCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSubPopulationCategories.Location = new System.Drawing.Point(0, 60);
            this.pnlSubPopulationCategories.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSubPopulationCategories.Name = "pnlSubPopulationCategories";
            this.pnlSubPopulationCategories.Size = new System.Drawing.Size(477, 110);
            this.pnlSubPopulationCategories.TabIndex = 2;
            // 
            // grpSubPopulation
            // 
            this.grpSubPopulation.Controls.Add(this.cboSubPopulation);
            this.grpSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSubPopulation.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpSubPopulation.Location = new System.Drawing.Point(5, 5);
            this.grpSubPopulation.Margin = new System.Windows.Forms.Padding(5);
            this.grpSubPopulation.Name = "grpSubPopulation";
            this.grpSubPopulation.Padding = new System.Windows.Forms.Padding(5);
            this.grpSubPopulation.Size = new System.Drawing.Size(467, 50);
            this.grpSubPopulation.TabIndex = 1;
            this.grpSubPopulation.TabStop = false;
            this.grpSubPopulation.Text = "grpSubPopulation";
            // 
            // cboSubPopulation
            // 
            this.cboSubPopulation.Dock = System.Windows.Forms.DockStyle.Top;
            this.cboSubPopulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cboSubPopulation.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboSubPopulation.FormattingEnabled = true;
            this.cboSubPopulation.Location = new System.Drawing.Point(5, 20);
            this.cboSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubPopulation.Name = "cboSubPopulation";
            this.cboSubPopulation.Size = new System.Drawing.Size(457, 24);
            this.cboSubPopulation.TabIndex = 1;
            // 
            // tlpAreaDomain
            // 
            this.tlpAreaDomain.ColumnCount = 1;
            this.tlpAreaDomain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAreaDomain.Controls.Add(this.grpAreaDomain, 0, 0);
            this.tlpAreaDomain.Controls.Add(this.pnlAreaDomainCategories, 0, 1);
            this.tlpAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAreaDomain.Location = new System.Drawing.Point(0, 0);
            this.tlpAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAreaDomain.Name = "tlpAreaDomain";
            this.tlpAreaDomain.RowCount = 2;
            this.tlpAreaDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpAreaDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAreaDomain.Size = new System.Drawing.Size(477, 170);
            this.tlpAreaDomain.TabIndex = 0;
            // 
            // grpAreaDomain
            // 
            this.grpAreaDomain.Controls.Add(this.cboAreaDomain);
            this.grpAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpAreaDomain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpAreaDomain.Location = new System.Drawing.Point(5, 5);
            this.grpAreaDomain.Margin = new System.Windows.Forms.Padding(5);
            this.grpAreaDomain.Name = "grpAreaDomain";
            this.grpAreaDomain.Padding = new System.Windows.Forms.Padding(5);
            this.grpAreaDomain.Size = new System.Drawing.Size(467, 50);
            this.grpAreaDomain.TabIndex = 0;
            this.grpAreaDomain.TabStop = false;
            this.grpAreaDomain.Text = "grpAreaDomain";
            // 
            // cboAreaDomain
            // 
            this.cboAreaDomain.Dock = System.Windows.Forms.DockStyle.Top;
            this.cboAreaDomain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cboAreaDomain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboAreaDomain.FormattingEnabled = true;
            this.cboAreaDomain.Location = new System.Drawing.Point(5, 20);
            this.cboAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            this.cboAreaDomain.Name = "cboAreaDomain";
            this.cboAreaDomain.Size = new System.Drawing.Size(457, 24);
            this.cboAreaDomain.TabIndex = 0;
            // 
            // pnlAreaDomainCategories
            // 
            this.pnlAreaDomainCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAreaDomainCategories.Location = new System.Drawing.Point(0, 60);
            this.pnlAreaDomainCategories.Margin = new System.Windows.Forms.Padding(0);
            this.pnlAreaDomainCategories.Name = "pnlAreaDomainCategories";
            this.pnlAreaDomainCategories.Size = new System.Drawing.Size(477, 110);
            this.pnlAreaDomainCategories.TabIndex = 1;
            // 
            // pnlAttributeCategories
            // 
            this.pnlAttributeCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAttributeCategories.Location = new System.Drawing.Point(0, 0);
            this.pnlAttributeCategories.Margin = new System.Windows.Forms.Padding(0);
            this.pnlAttributeCategories.Name = "pnlAttributeCategories";
            this.pnlAttributeCategories.Size = new System.Drawing.Size(954, 169);
            this.pnlAttributeCategories.TabIndex = 3;
            // 
            // ControlVariable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlVariable";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlPrevious.ResumeLayout(false);
            this.pnlNext.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splCaption.Panel1.ResumeLayout(false);
            this.splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).EndInit();
            this.splCaption.ResumeLayout(false);
            this.pnlCaption.ResumeLayout(false);
            this.grpIndicator.ResumeLayout(false);
            this.splVariable.Panel1.ResumeLayout(false);
            this.splVariable.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splVariable)).EndInit();
            this.splVariable.ResumeLayout(false);
            this.tlpControls.ResumeLayout(false);
            this.tlpSubPopulation.ResumeLayout(false);
            this.grpSubPopulation.ResumeLayout(false);
            this.tlpAreaDomain.ResumeLayout(false);
            this.grpAreaDomain.ResumeLayout(false);
            this.ResumeLayout(false);

            }

            #endregion

            private System.Windows.Forms.Panel pnlMain;
            private System.Windows.Forms.TableLayoutPanel tlpMain;
            private System.Windows.Forms.Panel pnlWorkSpace;
            private System.Windows.Forms.SplitContainer splCaption;
            private System.Windows.Forms.Panel pnlCaption;
            private System.Windows.Forms.GroupBox grpIndicator;
            private System.Windows.Forms.Panel pnlIndicator;
            private System.Windows.Forms.SplitContainer splVariable;
            private System.Windows.Forms.Panel pnlAttributeCategories;
            private System.Windows.Forms.TableLayoutPanel tlpControls;
            private System.Windows.Forms.TableLayoutPanel tlpSubPopulation;
            private System.Windows.Forms.Panel pnlSubPopulationCategories;
            private System.Windows.Forms.GroupBox grpSubPopulation;
            private System.Windows.Forms.ComboBox cboSubPopulation;
            private System.Windows.Forms.TableLayoutPanel tlpAreaDomain;
            private System.Windows.Forms.GroupBox grpAreaDomain;
            private System.Windows.Forms.ComboBox cboAreaDomain;
            private System.Windows.Forms.Panel pnlAreaDomainCategories;
            private System.Windows.Forms.TableLayoutPanel tlpButtons;
            private System.Windows.Forms.Panel pnlPrevious;
            private System.Windows.Forms.Button btnPrevious;
            private System.Windows.Forms.Panel pnlNext;
            private System.Windows.Forms.Button btnNext;
        }

    }
}
