﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlColumnMetadata
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlColumnMetadata));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.chkVisible = new System.Windows.Forms.CheckBox();
            this.lblDisplayIndex = new System.Windows.Forms.Label();
            this.lblColumnName = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.Color.White;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(548, 28);
            this.pnlMain.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.Color.White;
            this.tlpMain.ColumnCount = 4;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.Controls.Add(this.chkVisible, 0, 0);
            this.tlpMain.Controls.Add(this.lblDisplayIndex, 0, 0);
            this.tlpMain.Controls.Add(this.lblColumnName, 2, 0);
            this.tlpMain.Controls.Add(this.btnEdit, 3, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 1;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Size = new System.Drawing.Size(548, 28);
            this.tlpMain.TabIndex = 2;
            // 
            // chkVisible
            // 
            this.chkVisible.BackColor = System.Drawing.Color.White;
            this.chkVisible.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkVisible.Location = new System.Drawing.Point(30, 0);
            this.chkVisible.Margin = new System.Windows.Forms.Padding(0);
            this.chkVisible.Name = "chkVisible";
            this.chkVisible.Padding = new System.Windows.Forms.Padding(10, 2, 0, 0);
            this.chkVisible.Size = new System.Drawing.Size(30, 28);
            this.chkVisible.TabIndex = 7;
            this.chkVisible.UseVisualStyleBackColor = false;
            // 
            // lblDisplayIndex
            // 
            this.lblDisplayIndex.BackColor = System.Drawing.Color.White;
            this.lblDisplayIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDisplayIndex.Location = new System.Drawing.Point(0, 0);
            this.lblDisplayIndex.Margin = new System.Windows.Forms.Padding(0);
            this.lblDisplayIndex.Name = "lblDisplayIndex";
            this.lblDisplayIndex.Size = new System.Drawing.Size(30, 28);
            this.lblDisplayIndex.TabIndex = 0;
            this.lblDisplayIndex.Text = "1.";
            this.lblDisplayIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumnName
            // 
            this.lblColumnName.BackColor = System.Drawing.Color.White;
            this.lblColumnName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblColumnName.Location = new System.Drawing.Point(60, 0);
            this.lblColumnName.Margin = new System.Windows.Forms.Padding(0);
            this.lblColumnName.Name = "lblColumnName";
            this.lblColumnName.Size = new System.Drawing.Size(458, 28);
            this.lblColumnName.TabIndex = 6;
            this.lblColumnName.Text = "lblColumnName";
            this.lblColumnName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEdit.BackgroundImage")));
            this.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEdit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Location = new System.Drawing.Point(518, 0);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(0);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(30, 28);
            this.btnEdit.TabIndex = 8;
            this.btnEdit.UseVisualStyleBackColor = false;
            // 
            // ControlColumnMetadata
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkBlue;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlColumnMetadata";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(550, 30);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.CheckBox chkVisible;
        private System.Windows.Forms.Label lblDisplayIndex;
        private System.Windows.Forms.Label lblColumnName;
        private System.Windows.Forms.Button btnEdit;
    }

}