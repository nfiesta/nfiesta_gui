﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro nastavení metadat sloupce tabulky</para>
    /// <para lang="en">Form for setting table column metadata</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormColumnEdit
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormColumnEdit(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlColumnMetadata)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlColumnMetadata)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlColumnMetadata)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlColumnMetadata)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů (read-only)</para>
        /// <para lang="en">Module for configuration and calculation estimates setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Metadata sloupce tabulky (read-only)</para>
        /// <para lang="en">Table column metadata (read-only)</para>
        /// </summary>
        public ColumnMetadata Metadata
        {
            get
            {
                return ((ControlColumnMetadata)ControlOwner).Metadata;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormColumnEdit),                 "Popisek sloupce tabulky" },
                        { nameof(lblHeaderTextCs),                "Popisek sloupce [národní]:" },
                        { nameof(lblToolTipTextCs),               "Tip pro sloupec [národní]:" },
                        { nameof(lblHeaderTextEn),                "Popisek sloupce [anglický]:" },
                        { nameof(lblToolTipTextEn),               "Tip pro sloupec [anglický]:" },
                        { nameof(btnCancel),                      "Zrušit" },
                        { nameof(btnOK),                          "OK" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormColumnEdit),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormColumnEdit),                 "Column caption" },
                        { nameof(lblHeaderTextCs),                "Column header [national]:" },
                        { nameof(lblToolTipTextCs),               "Column tip [national]:" },
                        { nameof(lblHeaderTextEn),                "Column header [English]:" },
                        { nameof(lblToolTipTextEn),               "Column header  [English]:" },
                        { nameof(btnCancel),                      "Cancel" },
                        { nameof(btnOK),                          "OK" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormColumnEdit),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            InitializeLabels();

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    Metadata.HeaderTextCs = txtHeaderTextCs.Text;
                    Metadata.ToolTipTextCs = txtToolTipTextCs.Text;

                    Metadata.HeaderTextEn = txtHeaderTextEn.Text;
                    Metadata.ToolTipTextEn = txtToolTipTextEn.Text;

                    DialogResult = DialogResult.OK;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            // Popisky:

            Text =
                labels.TryGetValue(key: nameof(FormColumnEdit),
                out string frmColumnEditText)
                    ? frmColumnEditText
                    : String.Empty;

            lblHeaderTextCs.Text =
                labels.TryGetValue(key: nameof(lblHeaderTextCs),
                out string lblHeaderTextCsText)
                    ? lblHeaderTextCsText
                    : String.Empty;

            lblToolTipTextCs.Text =
                labels.TryGetValue(key: nameof(lblToolTipTextCs),
                out string lblToolTipTextCsText)
                    ? lblToolTipTextCsText
                    : String.Empty;

            lblHeaderTextEn.Text =
                labels.TryGetValue(key: nameof(lblHeaderTextEn),
                out string lblHeaderTextEnText)
                    ? lblHeaderTextEnText
                    : String.Empty;

            lblToolTipTextEn.Text =
                labels.TryGetValue(key: nameof(lblToolTipTextEn),
                out string lblToolTipTextEnText)
                    ? lblToolTipTextEnText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            // Hodnoty:

            txtHeaderTextCs.Text =
                Metadata.HeaderTextCs;

            txtToolTipTextCs.Text =
                Metadata.ToolTipTextCs;

            txtHeaderTextEn.Text =
                Metadata.HeaderTextEn;

            txtToolTipTextEn.Text =
                Metadata.ToolTipTextEn;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}