﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro výběr fáze odhadu</para>
    /// <para lang="en">Form for selection of the estimate phase</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormPhaseEstimateType
        : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)</para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)</para>
        /// </summary>
        private DisplayType displayType;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormPhaseEstimateType(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)</para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)</para>
        /// </summary>
        public DisplayType DisplayType
        {
            get
            {
                return displayType;
            }
            set
            {
                displayType = value;

                tlpWorkSpace.ColumnStyles[0].SizeType = SizeType.Percent;
                tlpWorkSpace.ColumnStyles[1].SizeType = SizeType.Percent;

                if (DisplayType == DisplayType.Ratio)
                {
                    grpNumerator.Visible = true;
                    grpDenominator.Visible = true;

                    tlpWorkSpace.ColumnStyles[0].Width = 50;
                    tlpWorkSpace.ColumnStyles[1].Width = 50;
                }
                else
                {
                    grpNumerator.Visible = true;
                    grpDenominator.Visible = false;

                    tlpWorkSpace.ColumnStyles[0].Width = 100;
                    tlpWorkSpace.ColumnStyles[1].Width = 0;
                }

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané seznamy fází odhadů pro čitatel a jmenovatel</para>
        /// <para lang="en">Selected phase estimate type lists for numerator and denominator</para>
        /// </summary>
        public SelectedPhaseEstimateTypes SelectedPhaseEstimateTypes
        {
            get
            {
                return
                    new SelectedPhaseEstimateTypes()
                    {
                        Numerator =
                            new PhaseEstimateTypeList(
                                database: Database,
                                rows: Database.SNfiEsta.CPhaseEstimateType.Data.AsEnumerable()
                                    .Where(type =>
                                        pnlNumerator.Controls
                                            .OfType<CheckBox>()
                                            .Where(box => box.Checked && box.Enabled)
                                            .Where(box => box.Tag != null)
                                            .Where(box => box.Tag is PhaseEstimateType)
                                            .Select(box => ((PhaseEstimateType)box.Tag).Id)
                                            .Contains(value: type.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name)))),
                        Denominator =
                            (DisplayType == DisplayType.Ratio)
                                ? new PhaseEstimateTypeList(
                                    database: Database,
                                    rows: Database.SNfiEsta.CPhaseEstimateType.Data.AsEnumerable()
                                        .Where(type =>
                                            pnlDenominator.Controls
                                                .OfType<CheckBox>()
                                                .Where(box => box.Checked && box.Enabled)
                                                .Where(box => box.Tag != null)
                                                .Where(box => box.Tag is PhaseEstimateType)
                                                .Select(box => ((PhaseEstimateType)box.Tag).Id)
                                                .Contains(value: type.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name))))
                                : null
                    };
            }
            set
            {
                foreach (CheckBox control in pnlNumerator.Controls.OfType<CheckBox>())
                {
                    control.Checked = false;
                }

                foreach (CheckBox control in pnlDenominator.Controls.OfType<CheckBox>())
                {
                    control.Checked = false;
                }

                if (value == null)
                {
                    return;
                }

                if (value.Numerator != null)
                {
                    foreach (CheckBox control in pnlNumerator.Controls
                        .OfType<CheckBox>()
                        .Where(box => box.Enabled)
                        .Where(box => box.Tag != null)
                        .Where(box => box.Tag is PhaseEstimateType)
                        .Where(box => value.Numerator.Items
                            .Select(type => type.Id).ToList<int>()
                            .Contains(item: ((PhaseEstimateType)box.Tag).Id)))
                    {
                        control.Checked = true;
                    }
                }

                if (value.Denominator != null)
                {
                    if (DisplayType == DisplayType.Ratio)
                    {
                        foreach (CheckBox control in pnlDenominator.Controls
                                .OfType<CheckBox>()
                                .Where(box => box.Enabled)
                                .Where(box => box.Tag != null)
                                .Where(box => box.Tag is PhaseEstimateType)
                                .Where(box => value.Denominator.Items
                                    .Select(type => type.Id).ToList<int>()
                                    .Contains(item: ((PhaseEstimateType)box.Tag).Id)))
                        {
                            control.Checked = true;
                        }
                    }
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPhaseEstimateType),    "Typy odhadů (fáze)" },
                        { nameof(grpNumerator),             "Čitatel" },
                        { nameof(grpDenominator),           "Jmenovatel" },
                        { nameof(btnCancel),                "Zrušit" },
                        { nameof(btnOK),                    "OK" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormPhaseEstimateType),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPhaseEstimateType),    "Estimate types (phase)" },
                        { nameof(grpNumerator),             "Numerator" },
                        { nameof(grpDenominator),           "Denominator" },
                        { nameof(btnCancel),                "Cancel" },
                        { nameof(btnOK),                    "OK" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormPhaseEstimateType),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            DisplayType = DisplayType.Total;

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            LoadContent();

            InitializeList();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace zaškrtávacího seznamu unikátních hodnot
        /// vyskytujících se ve zvoleném datovém sloupci (read-only)</para>
        /// <para lang="en">
        /// Initializing checked list box with unique values
        /// occurring in the selected data column (read-only)</para>
        /// </summary>
        private void InitializeList()
        {
            int[] allowedPhase = [
                    (int) PhaseEstimateTypeEnum.SinglePhase,
                    (int) PhaseEstimateTypeEnum.GRegMap ];

            pnlNumerator.Controls.Clear();
            pnlDenominator.Controls.Clear();

            int i = -1;
            foreach (PhaseEstimateType type in
                Database.SNfiEsta.CPhaseEstimateType.Items
                .OrderBy(a => a.Id))
            {
                i++;

                CheckBox numeratorControl = new()
                {
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    BackColor = pnlNumerator.BackColor,
                    Font = Setting.CheckBoxFont,
                    ForeColor = Setting.CheckBoxForeColor,
                    Checked = false,
                    Enabled = allowedPhase.Contains(value: type.Id),
                    Height = 20,
                    Left = 25,
                    Tag = type,
                    Text = String.Empty,
                    Top = (i * 25) + 10,
                    Width = pnlNumerator.Width - 50
                };
                numeratorControl.CreateControl();
                pnlNumerator.Controls.Add(value: numeratorControl);

                CheckBox denominatorControl = new()
                {
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    BackColor = pnlNumerator.BackColor,
                    Font = Setting.CheckBoxFont,
                    ForeColor = Setting.CheckBoxForeColor,
                    Checked = false,
                    Enabled = allowedPhase.Contains(value: type.Id),
                    Height = 20,
                    Left = 25,
                    Tag = type,
                    Text = String.Empty,
                    Top = (i * 25) + 10,
                    Width = pnlNumerator.Width - 50
                };
                denominatorControl.CreateControl();
                pnlDenominator.Controls.Add(value: denominatorControl);
            };

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormPhaseEstimateType),
                out string frmCalendarText)
                    ? frmCalendarText
                    : String.Empty;

            grpNumerator.Text =
                (DisplayType == DisplayType.Ratio)
                    ? labels.TryGetValue(key: nameof(grpNumerator),
                    out string grpNumeratorText)
                        ? grpNumeratorText
                        : String.Empty
                    : String.Empty;

            grpDenominator.Text =
                (DisplayType == DisplayType.Ratio)
                    ? labels.TryGetValue(key: nameof(grpDenominator),
                    out string grpDenominatorText)
                        ? grpDenominatorText
                        : String.Empty
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            foreach (CheckBox control in
                pnlNumerator.Controls.OfType<CheckBox>())
            {
                PhaseEstimateType type = (PhaseEstimateType)control.Tag;
                control.Text =
                        (type != null)
                        ? (LanguageVersion == LanguageVersion.National)
                            ? type.LabelCs
                            : (LanguageVersion == LanguageVersion.International)
                                ? type.LabelEn
                                : String.Empty
                        : String.Empty;
            }

            foreach (CheckBox control in
                pnlDenominator.Controls.OfType<CheckBox>())
            {
                PhaseEstimateType type = (PhaseEstimateType)control.Tag;
                control.Text =
                        (type != null)
                        ? (LanguageVersion == LanguageVersion.National)
                            ? type.LabelCs
                            : (LanguageVersion == LanguageVersion.International)
                                ? type.LabelEn
                                : String.Empty
                        : String.Empty;
            }
        }

        #endregion Methods

    }

}