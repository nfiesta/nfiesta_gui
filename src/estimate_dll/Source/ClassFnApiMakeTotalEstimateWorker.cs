﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Pracovní vlákno pro výpočet odhadů úhrnu</para>
        /// <para lang="en">Worker thread for calculation of total estimates</para>
        /// </summary>
        internal class FnApiMakeTotalEstimateWorker
            : WorkerThread<FnApiMakeTotalEstimateTask, OLAPTotalEstimate>
        {

            #region Methods

            /// <summary>
            /// <para lang="cs">Spuštění výpočtu odhadů</para>
            /// <para lang="en">Starting the calculation of estimates</para>
            /// </summary>
            public override object Execute(ref ThreadEventArgs e)
            {
                if (ThreadConnection == null) { return null; }
                if (Task == null) { return null; }

                Task.SQL = NfiEstaFunctions.FnMakeEstimate
                    .GetCommandText(
                        estimateConfId: Task.Element?.EstimateConfId);

                Task.Result = Task.Element?.EstimateConfId;
                NfiEstaFunctions.FnMakeEstimate.Execute(
                    connection: ThreadConnection,
                    estimateConfId: Task.Element?.EstimateConfId);

                return Task.Result;
            }

            #endregion Methods

        }

    }
}
