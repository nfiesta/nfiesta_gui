﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">
    /// Formulář pro konfiguraci regresních odhadů:
    /// Přehled konfigurace regresního odhadu a zápis do databáze
    /// </para>
    /// <para lang="en">
    /// Form for configuring regression estimates:
    /// regression estimate configuration summary and writing to the database
    /// </para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormGRegConfigurationSummary
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Vybraná skupina panelů a roků měření</para>
        /// <para lang="en">Selected group of panels and reference year set</para>
        /// </summary>
        private TFnApiGetGroupForPanelRefYearSetCombinations selectedPanelReferenceYearSetGroup;

        /// <summary>
        /// <para lang="cs">Vybraná kombinace modelu, sigma a typu parametrizační oblasti</para>
        /// <para lang="en">Selected combination of working model, sigma and parametrization area type</para>
        /// </summary>
        private TFnApiGetWModelsSigmaParamTypes selectedModelSigmaParamType;

        /// <summary>
        /// <para lang="cs">Vynucení syntetického odhadu</para>
        /// <para lang="en">Force synthetic estimate</para>
        /// </summary>
        private bool forceSynthetic;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormGRegConfigurationSummary(
            Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vybraná skupina panelů a roků měření</para>
        /// <para lang="en">Selected group of panels and reference year set</para>
        /// </summary>
        public TFnApiGetGroupForPanelRefYearSetCombinations SelectedPanelReferenceYearSetGroup
        {
            get
            {
                return selectedPanelReferenceYearSetGroup;
            }
            set
            {
                selectedPanelReferenceYearSetGroup = value;

                lblPanelRefYearSetGroupValue.Text =
                    (SelectedPanelReferenceYearSetGroup == null)
                    ? String.Empty
                    : (LanguageVersion == LanguageVersion.International)
                        ? SelectedPanelReferenceYearSetGroup.LabelEn ?? String.Empty
                        : (LanguageVersion == LanguageVersion.National)
                            ? SelectedPanelReferenceYearSetGroup.LabelCs ?? String.Empty
                            : SelectedPanelReferenceYearSetGroup.LabelEn ?? String.Empty;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná kombinace modelu, sigma a typu parametrizační oblasti</para>
        /// <para lang="en">Selected combination of working model, sigma and parametrization area type</para>
        /// </summary>
        public TFnApiGetWModelsSigmaParamTypes SelectedModelSigmaParamType
        {
            get
            {
                return selectedModelSigmaParamType;
            }
            set
            {
                selectedModelSigmaParamType = value;

                lblWorkingModelValue.Text =
                    (SelectedModelSigmaParamType == null)
                    ? String.Empty
                    : (LanguageVersion == LanguageVersion.International)
                        ? SelectedModelSigmaParamType.WorkingModelLabelEn ?? String.Empty
                        : (LanguageVersion == LanguageVersion.National)
                            ? SelectedModelSigmaParamType.WorkingModelLabelCs ?? String.Empty
                            : SelectedModelSigmaParamType.WorkingModelLabelEn ?? String.Empty;

                lblSigmaValue.Text =
                    (SelectedModelSigmaParamType?.Sigma == null)
                    ? String.Empty
                    : SelectedModelSigmaParamType.Sigma.ToString();

                lblParamAreaTypeValue.Text =
                    (SelectedModelSigmaParamType == null)
                    ? String.Empty
                    : (LanguageVersion == LanguageVersion.International)
                        ? SelectedModelSigmaParamType.ParamAreaTypeLabelEn ?? String.Empty
                        : (LanguageVersion == LanguageVersion.National)
                            ? SelectedModelSigmaParamType.ParamAreaTypeLabelCs ?? String.Empty
                            : SelectedModelSigmaParamType.ParamAreaTypeLabelEn ?? String.Empty;
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikační číslo vybrané skupiny panelů a roků měření</para>
        /// <para lang="en">Panel reference year set group identifier</para>
        /// </summary>
        public Nullable<int> PanelReferenceYearSetGroupId
        {
            get
            {
                return
                    SelectedPanelReferenceYearSetGroup?.Id;
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikační číslo modelu</para>
        /// <para lang="en">Model identifier</para>
        /// </summary>
        public Nullable<int> WorkingModelId
        {
            get
            {
                return
                    SelectedModelSigmaParamType?.WorkingModelId;
            }
        }

        /// <summary>
        /// <para lang="cs">Sigma</para>
        /// <para lang="en">Sigma</para>
        /// </summary>
        public Nullable<bool> Sigma
        {
            get
            {
                return
                    SelectedModelSigmaParamType?.Sigma;
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikační číslo typu parametrizační oblasti</para>
        /// <para lang="en">Paramtrization area type identifier</para>
        /// </summary>
        public Nullable<int> ParamAreaTypeId
        {
            get
            {
                return
                    SelectedModelSigmaParamType?.ParamAreaTypeId;
            }
        }

        /// <summary>
        /// <para lang="cs">Vynucení syntetického odhadu</para>
        /// <para lang="en">Force synthetic estimate</para>
        /// </summary>
        public bool ForceSynthetic
        {
            get
            {
                return forceSynthetic;
            }
            set
            {
                forceSynthetic = value;

                lblForceSyntheticValue.Text = ForceSynthetic.ToString();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegConfigurationSummary),     "Přehled konfigurace regresního odhadu" },
                        { nameof(btnCancel),                        "Zrušit" },
                        { nameof(btnOK),                            "Uložit" },
                        { nameof(lblPanelRefYearSetGroupCaption),   "Skupina panelů a referenčních období:" },
                        { nameof(lblWorkingModelCaption),           "Model:" },
                        { nameof(lblSigmaCaption),                  "Sigma:" },
                        { nameof(lblParamAreaTypeCaption),          "Typ parametrizační oblasti:" },
                        { nameof(lblForceSyntheticCaption),         "Vynutit syntetický odhad:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                            key: nameof(FormGRegConfigurationSummary),
                            out Dictionary<string, string> dictNational)
                                ? dictNational
                                : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegConfigurationSummary),     "Regression estimate configuration summary" },
                        { nameof(btnCancel),                        "Cancel" },
                        { nameof(btnOK),                            "Save" },
                        { nameof(lblPanelRefYearSetGroupCaption),   "Panel reference year set group:" },
                        { nameof(lblWorkingModelCaption),           "Working model:" },
                        { nameof(lblSigmaCaption),                  "Sigma:" },
                        { nameof(lblParamAreaTypeCaption),          "Parametrization area type:" },
                        { nameof(lblForceSyntheticCaption),         "Force synthetic estimate:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                            key: nameof(FormGRegConfigurationSummary),
                            out Dictionary<string, string> dictInternational)
                                ? dictInternational
                                : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            SelectedPanelReferenceYearSetGroup = null;
            SelectedModelSigmaParamType = null;
            ForceSynthetic = false;

            SetStyle();

            InitializeLabels();

            LoadContent();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;

            lblPanelRefYearSetGroupCaption.Font = Setting.LabelFont;
            lblPanelRefYearSetGroupCaption.ForeColor = Setting.LabelForeColor;

            lblWorkingModelCaption.Font = Setting.LabelFont;
            lblWorkingModelCaption.ForeColor = Setting.LabelForeColor;

            lblSigmaCaption.Font = Setting.LabelFont;
            lblSigmaCaption.ForeColor = Setting.LabelForeColor;

            lblParamAreaTypeCaption.Font = Setting.LabelFont;
            lblParamAreaTypeCaption.ForeColor = Setting.LabelForeColor;

            lblForceSyntheticCaption.Font = Setting.LabelFont;
            lblForceSyntheticCaption.ForeColor = Setting.LabelForeColor;

            lblPanelRefYearSetGroupValue.Font = Setting.LabelValueFont;
            lblPanelRefYearSetGroupValue.ForeColor = Setting.LabelValueForeColor;

            lblWorkingModelValue.Font = Setting.LabelValueFont;
            lblWorkingModelValue.ForeColor = Setting.LabelValueForeColor;

            lblSigmaValue.Font = Setting.LabelValueFont;
            lblSigmaValue.ForeColor = Setting.LabelValueForeColor;

            lblParamAreaTypeValue.Font = Setting.LabelValueFont;
            lblParamAreaTypeValue.ForeColor = Setting.LabelValueForeColor;

            lblForceSyntheticValue.Font = Setting.LabelValueFont;
            lblForceSyntheticValue.ForeColor = Setting.LabelValueForeColor;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormGRegConfigurationSummary),
                    out string frmGRegConfigurationSummaryText)
                        ? frmGRegConfigurationSummaryText
                        : String.Empty;

            btnOK.Text =
               labels.TryGetValue(key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            lblPanelRefYearSetGroupCaption.Text =
                labels.TryGetValue(key: nameof(lblPanelRefYearSetGroupCaption),
                    out string lblPanelRefYearSetGroupCaptionText)
                        ? lblPanelRefYearSetGroupCaptionText
                        : String.Empty;

            lblWorkingModelCaption.Text =
                labels.TryGetValue(key: nameof(lblWorkingModelCaption),
                    out string lblWorkingModelCaptionText)
                        ? lblWorkingModelCaptionText
                        : String.Empty;

            lblSigmaCaption.Text =
                labels.TryGetValue(key: nameof(lblSigmaCaption),
                    out string lblSigmaCaptionText)
                        ? lblSigmaCaptionText
                        : String.Empty;

            lblParamAreaTypeCaption.Text =
                labels.TryGetValue(key: nameof(lblParamAreaTypeCaption),
                    out string lblParamAreaTypeCaptionText)
                        ? lblParamAreaTypeCaptionText
                        : String.Empty;

            lblForceSyntheticCaption.Text =
                labels.TryGetValue(key: nameof(lblForceSyntheticCaption),
                    out string lblForceSyntheticCaptionText)
                        ? lblForceSyntheticCaptionText
                        : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}