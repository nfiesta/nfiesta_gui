﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlPanelReferenceYearSet
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpData = new System.Windows.Forms.TableLayoutPanel();
            lblPlaceHolder4 = new System.Windows.Forms.Label();
            lblReferenceDateEndValue = new System.Windows.Forms.Label();
            lblReferenceDateEndCaption = new System.Windows.Forms.Label();
            lblPlaceHolder3 = new System.Windows.Forms.Label();
            lblPlaceHolder2 = new System.Windows.Forms.Label();
            lblPlaceHolder1 = new System.Windows.Forms.Label();
            lblCollapsedReferenceYearSetValue = new System.Windows.Forms.Label();
            lblCollapsedReferenceYearSetCaption = new System.Windows.Forms.Label();
            lblCollapsedPanelValue = new System.Windows.Forms.Label();
            lblCollapsedPanelCaption = new System.Windows.Forms.Label();
            pnlButtonCollapse = new System.Windows.Forms.Panel();
            lblCollapse = new System.Windows.Forms.Label();
            lblPanelCaption = new System.Windows.Forms.Label();
            lblReferenceYearSetCaption = new System.Windows.Forms.Label();
            lblReferenceDateBeginCaption = new System.Windows.Forms.Label();
            lblPanelValue = new System.Windows.Forms.Label();
            lblReferenceYearSetValue = new System.Windows.Forms.Label();
            lblReferenceDateBeginValue = new System.Windows.Forms.Label();
            lblRefYearSetFullyWithinEstimationPeriodCaption = new System.Windows.Forms.Label();
            lblRefYearSetFullyWithinEstimationPeriodValue = new System.Windows.Forms.Label();
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption = new System.Windows.Forms.Label();
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue = new System.Windows.Forms.Label();
            lblSSizeCaption = new System.Windows.Forms.Label();
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption = new System.Windows.Forms.Label();
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption = new System.Windows.Forms.Label();
            lblSSizeValue = new System.Windows.Forms.Label();
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue = new System.Windows.Forms.Label();
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue = new System.Windows.Forms.Label();
            lblExpand = new System.Windows.Forms.Label();
            lblPlaceHolder5 = new System.Windows.Forms.Label();
            lblPlaceHolder6 = new System.Windows.Forms.Label();
            chkSelection = new System.Windows.Forms.CheckBox();
            tlpMain.SuspendLayout();
            tlpData.SuspendLayout();
            pnlButtonCollapse.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.Transparent;
            tlpMain.ColumnCount = 2;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpData, 1, 0);
            tlpMain.Controls.Add(chkSelection, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(1, 1);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 1;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Size = new System.Drawing.Size(958, 148);
            tlpMain.TabIndex = 0;
            // 
            // tlpData
            // 
            tlpData.BackColor = System.Drawing.Color.Transparent;
            tlpData.ColumnCount = 5;
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            tlpData.Controls.Add(lblPlaceHolder4, 0, 5);
            tlpData.Controls.Add(lblReferenceDateEndValue, 2, 4);
            tlpData.Controls.Add(lblReferenceDateEndCaption, 1, 4);
            tlpData.Controls.Add(lblPlaceHolder3, 0, 4);
            tlpData.Controls.Add(lblPlaceHolder2, 0, 3);
            tlpData.Controls.Add(lblPlaceHolder1, 0, 2);
            tlpData.Controls.Add(lblCollapsedReferenceYearSetValue, 4, 0);
            tlpData.Controls.Add(lblCollapsedReferenceYearSetCaption, 3, 0);
            tlpData.Controls.Add(lblCollapsedPanelValue, 2, 0);
            tlpData.Controls.Add(lblCollapsedPanelCaption, 1, 0);
            tlpData.Controls.Add(pnlButtonCollapse, 0, 1);
            tlpData.Controls.Add(lblPanelCaption, 1, 1);
            tlpData.Controls.Add(lblReferenceYearSetCaption, 1, 2);
            tlpData.Controls.Add(lblReferenceDateBeginCaption, 1, 3);
            tlpData.Controls.Add(lblPanelValue, 2, 1);
            tlpData.Controls.Add(lblReferenceYearSetValue, 2, 2);
            tlpData.Controls.Add(lblReferenceDateBeginValue, 2, 3);
            tlpData.Controls.Add(lblRefYearSetFullyWithinEstimationPeriodCaption, 1, 5);
            tlpData.Controls.Add(lblRefYearSetFullyWithinEstimationPeriodValue, 2, 5);
            tlpData.Controls.Add(lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption, 3, 5);
            tlpData.Controls.Add(lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue, 4, 5);
            tlpData.Controls.Add(lblSSizeCaption, 3, 4);
            tlpData.Controls.Add(lblShareOfEstimationPeriodIntersectedByRefYearSetCaption, 3, 3);
            tlpData.Controls.Add(lblShareOfRefYearSetIntersectedByEstimationPeriodCaption, 3, 2);
            tlpData.Controls.Add(lblSSizeValue, 4, 4);
            tlpData.Controls.Add(lblShareOfEstimationPeriodIntersectedByRefYearSetValue, 4, 3);
            tlpData.Controls.Add(lblShareOfRefYearSetIntersectedByEstimationPeriodValue, 4, 2);
            tlpData.Controls.Add(lblExpand, 0, 0);
            tlpData.Controls.Add(lblPlaceHolder5, 3, 1);
            tlpData.Controls.Add(lblPlaceHolder6, 4, 1);
            tlpData.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpData.Location = new System.Drawing.Point(25, 0);
            tlpData.Margin = new System.Windows.Forms.Padding(0);
            tlpData.Name = "tlpData";
            tlpData.RowCount = 7;
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpData.Size = new System.Drawing.Size(933, 148);
            tlpData.TabIndex = 6;
            // 
            // lblPlaceHolder4
            // 
            lblPlaceHolder4.AutoEllipsis = true;
            lblPlaceHolder4.BackColor = System.Drawing.Color.Transparent;
            lblPlaceHolder4.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPlaceHolder4.Location = new System.Drawing.Point(0, 125);
            lblPlaceHolder4.Margin = new System.Windows.Forms.Padding(0);
            lblPlaceHolder4.Name = "lblPlaceHolder4";
            lblPlaceHolder4.Size = new System.Drawing.Size(25, 25);
            lblPlaceHolder4.TabIndex = 50;
            lblPlaceHolder4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReferenceDateEndValue
            // 
            lblReferenceDateEndValue.AutoEllipsis = true;
            lblReferenceDateEndValue.BackColor = System.Drawing.Color.Transparent;
            lblReferenceDateEndValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReferenceDateEndValue.Location = new System.Drawing.Point(297, 100);
            lblReferenceDateEndValue.Margin = new System.Windows.Forms.Padding(0);
            lblReferenceDateEndValue.Name = "lblReferenceDateEndValue";
            lblReferenceDateEndValue.Size = new System.Drawing.Size(181, 25);
            lblReferenceDateEndValue.TabIndex = 49;
            lblReferenceDateEndValue.Text = "lblReferenceDateEndValue";
            lblReferenceDateEndValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReferenceDateEndCaption
            // 
            lblReferenceDateEndCaption.AutoEllipsis = true;
            lblReferenceDateEndCaption.BackColor = System.Drawing.Color.Transparent;
            lblReferenceDateEndCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReferenceDateEndCaption.Location = new System.Drawing.Point(25, 100);
            lblReferenceDateEndCaption.Margin = new System.Windows.Forms.Padding(0);
            lblReferenceDateEndCaption.Name = "lblReferenceDateEndCaption";
            lblReferenceDateEndCaption.Size = new System.Drawing.Size(272, 25);
            lblReferenceDateEndCaption.TabIndex = 48;
            lblReferenceDateEndCaption.Text = "lblReferenceDateEndCaption";
            lblReferenceDateEndCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPlaceHolder3
            // 
            lblPlaceHolder3.AutoEllipsis = true;
            lblPlaceHolder3.BackColor = System.Drawing.Color.Transparent;
            lblPlaceHolder3.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPlaceHolder3.Location = new System.Drawing.Point(0, 100);
            lblPlaceHolder3.Margin = new System.Windows.Forms.Padding(0);
            lblPlaceHolder3.Name = "lblPlaceHolder3";
            lblPlaceHolder3.Size = new System.Drawing.Size(25, 25);
            lblPlaceHolder3.TabIndex = 47;
            lblPlaceHolder3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPlaceHolder2
            // 
            lblPlaceHolder2.AutoEllipsis = true;
            lblPlaceHolder2.BackColor = System.Drawing.Color.Transparent;
            lblPlaceHolder2.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPlaceHolder2.Location = new System.Drawing.Point(0, 75);
            lblPlaceHolder2.Margin = new System.Windows.Forms.Padding(0);
            lblPlaceHolder2.Name = "lblPlaceHolder2";
            lblPlaceHolder2.Size = new System.Drawing.Size(25, 25);
            lblPlaceHolder2.TabIndex = 46;
            lblPlaceHolder2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPlaceHolder1
            // 
            lblPlaceHolder1.AutoEllipsis = true;
            lblPlaceHolder1.BackColor = System.Drawing.Color.Transparent;
            lblPlaceHolder1.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPlaceHolder1.Location = new System.Drawing.Point(0, 50);
            lblPlaceHolder1.Margin = new System.Windows.Forms.Padding(0);
            lblPlaceHolder1.Name = "lblPlaceHolder1";
            lblPlaceHolder1.Size = new System.Drawing.Size(25, 25);
            lblPlaceHolder1.TabIndex = 45;
            lblPlaceHolder1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCollapsedReferenceYearSetValue
            // 
            lblCollapsedReferenceYearSetValue.AutoEllipsis = true;
            lblCollapsedReferenceYearSetValue.BackColor = System.Drawing.Color.Transparent;
            lblCollapsedReferenceYearSetValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCollapsedReferenceYearSetValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            lblCollapsedReferenceYearSetValue.Location = new System.Drawing.Point(750, 0);
            lblCollapsedReferenceYearSetValue.Margin = new System.Windows.Forms.Padding(0);
            lblCollapsedReferenceYearSetValue.Name = "lblCollapsedReferenceYearSetValue";
            lblCollapsedReferenceYearSetValue.Size = new System.Drawing.Size(183, 25);
            lblCollapsedReferenceYearSetValue.TabIndex = 36;
            lblCollapsedReferenceYearSetValue.Text = "lblCollapsedReferenceYearSetValue";
            lblCollapsedReferenceYearSetValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCollapsedReferenceYearSetCaption
            // 
            lblCollapsedReferenceYearSetCaption.AutoEllipsis = true;
            lblCollapsedReferenceYearSetCaption.BackColor = System.Drawing.Color.Transparent;
            lblCollapsedReferenceYearSetCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCollapsedReferenceYearSetCaption.Location = new System.Drawing.Point(478, 0);
            lblCollapsedReferenceYearSetCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCollapsedReferenceYearSetCaption.Name = "lblCollapsedReferenceYearSetCaption";
            lblCollapsedReferenceYearSetCaption.Size = new System.Drawing.Size(272, 25);
            lblCollapsedReferenceYearSetCaption.TabIndex = 35;
            lblCollapsedReferenceYearSetCaption.Text = "lblCollapsedReferenceYearSetCaption";
            lblCollapsedReferenceYearSetCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCollapsedPanelValue
            // 
            lblCollapsedPanelValue.AutoEllipsis = true;
            lblCollapsedPanelValue.BackColor = System.Drawing.Color.Transparent;
            lblCollapsedPanelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCollapsedPanelValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            lblCollapsedPanelValue.Location = new System.Drawing.Point(297, 0);
            lblCollapsedPanelValue.Margin = new System.Windows.Forms.Padding(0);
            lblCollapsedPanelValue.Name = "lblCollapsedPanelValue";
            lblCollapsedPanelValue.Size = new System.Drawing.Size(181, 25);
            lblCollapsedPanelValue.TabIndex = 30;
            lblCollapsedPanelValue.Text = "lblCollapsedPanelValue";
            lblCollapsedPanelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCollapsedPanelCaption
            // 
            lblCollapsedPanelCaption.AutoEllipsis = true;
            lblCollapsedPanelCaption.BackColor = System.Drawing.Color.Transparent;
            lblCollapsedPanelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCollapsedPanelCaption.Location = new System.Drawing.Point(25, 0);
            lblCollapsedPanelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCollapsedPanelCaption.Name = "lblCollapsedPanelCaption";
            lblCollapsedPanelCaption.Size = new System.Drawing.Size(272, 25);
            lblCollapsedPanelCaption.TabIndex = 25;
            lblCollapsedPanelCaption.Text = "lblCollapsedPanelCaption";
            lblCollapsedPanelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlButtonCollapse
            // 
            pnlButtonCollapse.Controls.Add(lblCollapse);
            pnlButtonCollapse.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtonCollapse.Location = new System.Drawing.Point(0, 25);
            pnlButtonCollapse.Margin = new System.Windows.Forms.Padding(0);
            pnlButtonCollapse.Name = "pnlButtonCollapse";
            pnlButtonCollapse.Size = new System.Drawing.Size(25, 25);
            pnlButtonCollapse.TabIndex = 24;
            // 
            // lblCollapse
            // 
            lblCollapse.AutoEllipsis = true;
            lblCollapse.BackColor = System.Drawing.Color.Transparent;
            lblCollapse.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCollapse.Font = new System.Drawing.Font("Wide Latin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblCollapse.Location = new System.Drawing.Point(0, 0);
            lblCollapse.Margin = new System.Windows.Forms.Padding(0);
            lblCollapse.Name = "lblCollapse";
            lblCollapse.Size = new System.Drawing.Size(25, 25);
            lblCollapse.TabIndex = 54;
            lblCollapse.Text = "-";
            lblCollapse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPanelCaption
            // 
            lblPanelCaption.AutoEllipsis = true;
            lblPanelCaption.BackColor = System.Drawing.Color.Transparent;
            lblPanelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelCaption.Location = new System.Drawing.Point(25, 25);
            lblPanelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblPanelCaption.Name = "lblPanelCaption";
            lblPanelCaption.Size = new System.Drawing.Size(272, 25);
            lblPanelCaption.TabIndex = 26;
            lblPanelCaption.Text = "lblPanelCaption";
            lblPanelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReferenceYearSetCaption
            // 
            lblReferenceYearSetCaption.AutoEllipsis = true;
            lblReferenceYearSetCaption.BackColor = System.Drawing.Color.Transparent;
            lblReferenceYearSetCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReferenceYearSetCaption.Location = new System.Drawing.Point(25, 50);
            lblReferenceYearSetCaption.Margin = new System.Windows.Forms.Padding(0);
            lblReferenceYearSetCaption.Name = "lblReferenceYearSetCaption";
            lblReferenceYearSetCaption.Size = new System.Drawing.Size(272, 25);
            lblReferenceYearSetCaption.TabIndex = 27;
            lblReferenceYearSetCaption.Text = "lblReferenceYearSetCaption";
            lblReferenceYearSetCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReferenceDateBeginCaption
            // 
            lblReferenceDateBeginCaption.AutoEllipsis = true;
            lblReferenceDateBeginCaption.BackColor = System.Drawing.Color.Transparent;
            lblReferenceDateBeginCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReferenceDateBeginCaption.Location = new System.Drawing.Point(25, 75);
            lblReferenceDateBeginCaption.Margin = new System.Windows.Forms.Padding(0);
            lblReferenceDateBeginCaption.Name = "lblReferenceDateBeginCaption";
            lblReferenceDateBeginCaption.Size = new System.Drawing.Size(272, 25);
            lblReferenceDateBeginCaption.TabIndex = 28;
            lblReferenceDateBeginCaption.Text = "lblReferenceDateBeginCaption";
            lblReferenceDateBeginCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelValue
            // 
            lblPanelValue.AutoEllipsis = true;
            lblPanelValue.BackColor = System.Drawing.Color.Transparent;
            lblPanelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            lblPanelValue.Location = new System.Drawing.Point(297, 25);
            lblPanelValue.Margin = new System.Windows.Forms.Padding(0);
            lblPanelValue.Name = "lblPanelValue";
            lblPanelValue.Size = new System.Drawing.Size(181, 25);
            lblPanelValue.TabIndex = 31;
            lblPanelValue.Text = "lblPanelValue";
            lblPanelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReferenceYearSetValue
            // 
            lblReferenceYearSetValue.AutoEllipsis = true;
            lblReferenceYearSetValue.BackColor = System.Drawing.Color.Transparent;
            lblReferenceYearSetValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReferenceYearSetValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            lblReferenceYearSetValue.Location = new System.Drawing.Point(297, 50);
            lblReferenceYearSetValue.Margin = new System.Windows.Forms.Padding(0);
            lblReferenceYearSetValue.Name = "lblReferenceYearSetValue";
            lblReferenceYearSetValue.Size = new System.Drawing.Size(181, 25);
            lblReferenceYearSetValue.TabIndex = 32;
            lblReferenceYearSetValue.Text = "lblReferenceYearSetValue";
            lblReferenceYearSetValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReferenceDateBeginValue
            // 
            lblReferenceDateBeginValue.AutoEllipsis = true;
            lblReferenceDateBeginValue.BackColor = System.Drawing.Color.Transparent;
            lblReferenceDateBeginValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReferenceDateBeginValue.Location = new System.Drawing.Point(297, 75);
            lblReferenceDateBeginValue.Margin = new System.Windows.Forms.Padding(0);
            lblReferenceDateBeginValue.Name = "lblReferenceDateBeginValue";
            lblReferenceDateBeginValue.Size = new System.Drawing.Size(181, 25);
            lblReferenceDateBeginValue.TabIndex = 33;
            lblReferenceDateBeginValue.Text = "lblReferenceDateBeginValue";
            lblReferenceDateBeginValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRefYearSetFullyWithinEstimationPeriodCaption
            // 
            lblRefYearSetFullyWithinEstimationPeriodCaption.AutoEllipsis = true;
            lblRefYearSetFullyWithinEstimationPeriodCaption.BackColor = System.Drawing.Color.Transparent;
            lblRefYearSetFullyWithinEstimationPeriodCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRefYearSetFullyWithinEstimationPeriodCaption.Location = new System.Drawing.Point(25, 125);
            lblRefYearSetFullyWithinEstimationPeriodCaption.Margin = new System.Windows.Forms.Padding(0);
            lblRefYearSetFullyWithinEstimationPeriodCaption.Name = "lblRefYearSetFullyWithinEstimationPeriodCaption";
            lblRefYearSetFullyWithinEstimationPeriodCaption.Size = new System.Drawing.Size(272, 25);
            lblRefYearSetFullyWithinEstimationPeriodCaption.TabIndex = 37;
            lblRefYearSetFullyWithinEstimationPeriodCaption.Text = "lblRefYearSetFullyWithinEstimationPeriodCaption";
            lblRefYearSetFullyWithinEstimationPeriodCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRefYearSetFullyWithinEstimationPeriodValue
            // 
            lblRefYearSetFullyWithinEstimationPeriodValue.AutoEllipsis = true;
            lblRefYearSetFullyWithinEstimationPeriodValue.BackColor = System.Drawing.Color.Transparent;
            lblRefYearSetFullyWithinEstimationPeriodValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRefYearSetFullyWithinEstimationPeriodValue.Location = new System.Drawing.Point(297, 125);
            lblRefYearSetFullyWithinEstimationPeriodValue.Margin = new System.Windows.Forms.Padding(0);
            lblRefYearSetFullyWithinEstimationPeriodValue.Name = "lblRefYearSetFullyWithinEstimationPeriodValue";
            lblRefYearSetFullyWithinEstimationPeriodValue.Size = new System.Drawing.Size(181, 25);
            lblRefYearSetFullyWithinEstimationPeriodValue.TabIndex = 38;
            lblRefYearSetFullyWithinEstimationPeriodValue.Text = "lblRefYearSetFullyWithinEstimationPeriodValue";
            lblRefYearSetFullyWithinEstimationPeriodValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption
            // 
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.AutoEllipsis = true;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.BackColor = System.Drawing.Color.Transparent;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.Location = new System.Drawing.Point(478, 125);
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.Margin = new System.Windows.Forms.Padding(0);
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.Name = "lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption";
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.Size = new System.Drawing.Size(272, 25);
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.TabIndex = 51;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.Text = "lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption";
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue
            // 
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.AutoEllipsis = true;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.BackColor = System.Drawing.Color.Transparent;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.Location = new System.Drawing.Point(750, 125);
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.Margin = new System.Windows.Forms.Padding(0);
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.Name = "lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue";
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.Size = new System.Drawing.Size(183, 25);
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.TabIndex = 52;
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.Text = "lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue";
            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSSizeCaption
            // 
            lblSSizeCaption.AutoEllipsis = true;
            lblSSizeCaption.BackColor = System.Drawing.Color.Transparent;
            lblSSizeCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSSizeCaption.Location = new System.Drawing.Point(478, 100);
            lblSSizeCaption.Margin = new System.Windows.Forms.Padding(0);
            lblSSizeCaption.Name = "lblSSizeCaption";
            lblSSizeCaption.Size = new System.Drawing.Size(272, 25);
            lblSSizeCaption.TabIndex = 43;
            lblSSizeCaption.Text = "lblSSizeCaption";
            lblSSizeCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShareOfEstimationPeriodIntersectedByRefYearSetCaption
            // 
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.AutoEllipsis = true;
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.BackColor = System.Drawing.Color.Transparent;
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.Location = new System.Drawing.Point(478, 75);
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.Margin = new System.Windows.Forms.Padding(0);
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.Name = "lblShareOfEstimationPeriodIntersectedByRefYearSetCaption";
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.Size = new System.Drawing.Size(272, 25);
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.TabIndex = 41;
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.Text = "lblShareOfEstimationPeriodIntersectedByRefYearSetCaption";
            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShareOfRefYearSetIntersectedByEstimationPeriodCaption
            // 
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.AutoEllipsis = true;
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.BackColor = System.Drawing.Color.Transparent;
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.Location = new System.Drawing.Point(478, 50);
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.Margin = new System.Windows.Forms.Padding(0);
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.Name = "lblShareOfRefYearSetIntersectedByEstimationPeriodCaption";
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.Size = new System.Drawing.Size(272, 25);
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.TabIndex = 39;
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.Text = "lblShareOfRefYearSetIntersectedByEstimationPeriodCaption";
            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSSizeValue
            // 
            lblSSizeValue.AutoEllipsis = true;
            lblSSizeValue.BackColor = System.Drawing.Color.Transparent;
            lblSSizeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSSizeValue.Location = new System.Drawing.Point(750, 100);
            lblSSizeValue.Margin = new System.Windows.Forms.Padding(0);
            lblSSizeValue.Name = "lblSSizeValue";
            lblSSizeValue.Size = new System.Drawing.Size(183, 25);
            lblSSizeValue.TabIndex = 44;
            lblSSizeValue.Text = "lblSSizeValue";
            lblSSizeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShareOfEstimationPeriodIntersectedByRefYearSetValue
            // 
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.AutoEllipsis = true;
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.BackColor = System.Drawing.Color.Transparent;
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.Location = new System.Drawing.Point(750, 75);
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.Margin = new System.Windows.Forms.Padding(0);
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.Name = "lblShareOfEstimationPeriodIntersectedByRefYearSetValue";
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.Size = new System.Drawing.Size(183, 25);
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.TabIndex = 42;
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.Text = "lblShareOfEstimationPeriodIntersectedByRefYearSetValue";
            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShareOfRefYearSetIntersectedByEstimationPeriodValue
            // 
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.AutoEllipsis = true;
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.BackColor = System.Drawing.Color.Transparent;
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.Location = new System.Drawing.Point(750, 50);
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.Margin = new System.Windows.Forms.Padding(0);
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.Name = "lblShareOfRefYearSetIntersectedByEstimationPeriodValue";
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.Size = new System.Drawing.Size(183, 25);
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.TabIndex = 40;
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.Text = "lblShareOfRefYearSetIntersectedByEstimationPeriodValue";
            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExpand
            // 
            lblExpand.AutoEllipsis = true;
            lblExpand.BackColor = System.Drawing.Color.Transparent;
            lblExpand.Dock = System.Windows.Forms.DockStyle.Fill;
            lblExpand.Font = new System.Drawing.Font("Wide Latin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblExpand.Location = new System.Drawing.Point(0, 0);
            lblExpand.Margin = new System.Windows.Forms.Padding(0);
            lblExpand.Name = "lblExpand";
            lblExpand.Size = new System.Drawing.Size(25, 25);
            lblExpand.TabIndex = 53;
            lblExpand.Text = "+";
            lblExpand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlaceHolder5
            // 
            lblPlaceHolder5.AutoEllipsis = true;
            lblPlaceHolder5.BackColor = System.Drawing.Color.Transparent;
            lblPlaceHolder5.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPlaceHolder5.Location = new System.Drawing.Point(478, 25);
            lblPlaceHolder5.Margin = new System.Windows.Forms.Padding(0);
            lblPlaceHolder5.Name = "lblPlaceHolder5";
            lblPlaceHolder5.Size = new System.Drawing.Size(272, 25);
            lblPlaceHolder5.TabIndex = 56;
            lblPlaceHolder5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlaceHolder6
            // 
            lblPlaceHolder6.AutoEllipsis = true;
            lblPlaceHolder6.BackColor = System.Drawing.Color.Transparent;
            lblPlaceHolder6.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPlaceHolder6.Location = new System.Drawing.Point(750, 25);
            lblPlaceHolder6.Margin = new System.Windows.Forms.Padding(0);
            lblPlaceHolder6.Name = "lblPlaceHolder6";
            lblPlaceHolder6.Size = new System.Drawing.Size(183, 25);
            lblPlaceHolder6.TabIndex = 55;
            lblPlaceHolder6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkSelection
            // 
            chkSelection.AutoSize = true;
            chkSelection.BackColor = System.Drawing.Color.Transparent;
            chkSelection.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            chkSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            chkSelection.Location = new System.Drawing.Point(0, 0);
            chkSelection.Margin = new System.Windows.Forms.Padding(0);
            chkSelection.Name = "chkSelection";
            chkSelection.Size = new System.Drawing.Size(25, 148);
            chkSelection.TabIndex = 3;
            chkSelection.UseVisualStyleBackColor = false;
            // 
            // ControlPanelReferenceYearSet
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.PaleGreen;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlPanelReferenceYearSet";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(960, 150);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            tlpData.ResumeLayout(false);
            pnlButtonCollapse.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.CheckBox chkSelection;
        private System.Windows.Forms.TableLayoutPanel tlpData;
        private System.Windows.Forms.Panel pnlButtonCollapse;
        private System.Windows.Forms.Label lblCollapsedPanelCaption;
        private System.Windows.Forms.Label lblRefYearSetFullyWithinEstimationPeriodValue;
        private System.Windows.Forms.Label lblRefYearSetFullyWithinEstimationPeriodCaption;
        private System.Windows.Forms.Label lblCollapsedReferenceYearSetValue;
        private System.Windows.Forms.Label lblCollapsedReferenceYearSetCaption;
        private System.Windows.Forms.Label lblCollapsedPanelValue;
        private System.Windows.Forms.Label lblPanelCaption;
        private System.Windows.Forms.Label lblReferenceYearSetCaption;
        private System.Windows.Forms.Label lblReferenceDateBeginCaption;
        private System.Windows.Forms.Label lblPanelValue;
        private System.Windows.Forms.Label lblReferenceYearSetValue;
        private System.Windows.Forms.Label lblReferenceDateBeginValue;
        private System.Windows.Forms.Label lblShareOfRefYearSetIntersectedByEstimationPeriodCaption;
        private System.Windows.Forms.Label lblShareOfRefYearSetIntersectedByEstimationPeriodValue;
        private System.Windows.Forms.Label lblShareOfEstimationPeriodIntersectedByRefYearSetCaption;
        private System.Windows.Forms.Label lblShareOfEstimationPeriodIntersectedByRefYearSetValue;
        private System.Windows.Forms.Label lblSSizeCaption;
        private System.Windows.Forms.Label lblSSizeValue;
        private System.Windows.Forms.Label lblReferenceDateEndValue;
        private System.Windows.Forms.Label lblReferenceDateEndCaption;
        private System.Windows.Forms.Label lblPlaceHolder3;
        private System.Windows.Forms.Label lblPlaceHolder2;
        private System.Windows.Forms.Label lblPlaceHolder1;
        private System.Windows.Forms.Label lblPlaceHolder4;
        private System.Windows.Forms.Label lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue;
        private System.Windows.Forms.Label lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption;
        private System.Windows.Forms.Label lblCollapse;
        private System.Windows.Forms.Label lblExpand;
        private System.Windows.Forms.Label lblPlaceHolder5;
        private System.Windows.Forms.Label lblPlaceHolder6;
    }
}
