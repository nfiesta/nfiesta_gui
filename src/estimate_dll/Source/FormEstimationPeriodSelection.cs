﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro výběr výpočetních období</para>
    /// <para lang="en">Form for selection of the estimation periods</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormEstimationPeriodSelection
        : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormEstimationPeriodSelection(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vybraná skupina výpočetních období</para>
        /// <para lang="en">Selected estimation period collection</para>
        /// </summary>
        public EstimationPeriodList SelectedEstimationPeriods
        {
            get
            {
                return
                    new EstimationPeriodList(
                        database: Database,
                        rows: Database.SNfiEsta.CEstimationPeriod.Data.AsEnumerable()
                        .Where(estimationPeriod =>
                            pnlEstimationPeriods.Controls
                                .OfType<CheckBox>()
                                .Where(box => box.Checked && box.Enabled)
                                .Where(box => box.Tag != null)
                                .Where(box => box.Tag is EstimationPeriod)
                                .Select(box => ((EstimationPeriod)box.Tag).Id)
                                .Contains(value: estimationPeriod.Field<int>(columnName: EstimationPeriodList.ColId.Name))));
            }
            set
            {
                foreach (CheckBox control in pnlEstimationPeriods.Controls.OfType<CheckBox>())
                {
                    control.Checked = false;
                }

                if (value != null)
                {
                    foreach (CheckBox control in pnlEstimationPeriods.Controls
                            .OfType<CheckBox>()
                            .Where(box => box.Enabled)
                            .Where(box => box.Tag != null)
                            .Where(box => box.Tag is EstimationPeriod)
                            .Where(box => value.Items
                                .Select(estimationPeriod => estimationPeriod.Id).ToList<int>()
                                .Contains(item: ((EstimationPeriod)box.Tag).Id)))
                    {
                        control.Checked = true;
                    }
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimationPeriodSelection),        "Výpočetní období" },
                        { nameof(btnCancel),                            "Zrušit" },
                        { nameof(btnOK),                                "OK" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimationPeriodSelection),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimationPeriodSelection),        "Estimation periods" },
                        { nameof(btnCancel),                            "Cancel" },
                        { nameof(btnOK),                                "OK" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimationPeriodSelection),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            InitializeLabels();

            LoadContent();

            InitializeList();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormEstimationPeriodSelection),
                out string frmEstimationCellCollectionText)
                    ? frmEstimationCellCollectionText
                    : String.Empty;

            btnOK.Text =
               labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
               labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    foreach (CheckBox control in pnlEstimationPeriods.Controls.OfType<CheckBox>())
                    {
                        control.Text = ((control.Tag != null) && (control.Tag is EstimationPeriod period))
                            ? period.LabelCs
                            : String.Empty;
                    }
                    break;

                case LanguageVersion.International:
                    foreach (CheckBox control in pnlEstimationPeriods.Controls.OfType<CheckBox>())
                    {
                        control.Text = ((control.Tag != null) && (control.Tag is EstimationPeriod period))
                            ? period.LabelEn
                            : String.Empty;
                    }
                    break;

                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(LanguageVersion)} unknown value.",
                        paramName: nameof(LanguageVersion));
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace zaškrtávacího seznamu unikátních hodnot
        /// vyskytujících se ve zvoleném datovém sloupci (read-only)</para>
        /// <para lang="en">
        /// Initializing checked list box with unique values
        /// occurring in the selected data column (read-only)</para>
        /// </summary>
        private void InitializeList()
        {
            pnlEstimationPeriods.Controls.Clear();

            int i = -1;
            foreach (EstimationPeriod period in
                Database.SNfiEsta.CEstimationPeriod.Items
                .OrderBy(a => a.Id))
            {
                i++;
                CheckBox control = new()
                {
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    BackColor = pnlEstimationPeriods.BackColor,
                    Enabled = true,
                    ForeColor = Color.Black,
                    Checked = false,
                    Height = 20,
                    Left = 25,
                    Tag = period,
                    Text = String.Empty,
                    Top = (i * 25) + 10,
                    Width = pnlEstimationPeriods.Width - 50
                };
                control.CreateControl();
                pnlEstimationPeriods.Controls.Add(value: control);
            };

            InitializeLabels();
        }

        #endregion Methods

    }

}