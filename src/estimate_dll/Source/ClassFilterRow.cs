﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Podmínka pro výběr řádků v DataGridView</para>
        /// <para lang="en">Condition for rows selection in DataGridView</para>
        /// </summary>
        internal class FilterRow
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Seznam částí podmínky pro výběr řádků v DataGridView pro vybrané sloupce</para>
            /// <para lang="en">List of condition parts for rows selection in DataGridView for selected columns</para>
            /// </summary>
            private List<IFilterRowItem> items;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu</para>
            /// <para lang="en">Object constructor</para>
            /// </summary>
            public FilterRow()
            {
                Items = [];
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Seznam částí podmínky pro výběr řádků v DataGridView pro vybrané sloupce</para>
            /// <para lang="en">List of condition parts for rows selection in DataGridView for selected columns</para>
            /// </summary>
            public List<IFilterRowItem> Items
            {
                get
                {
                    return items ?? [];
                }
                set
                {
                    items = value ?? [];
                }
            }

            /// <summary>
            /// <para lang="cs">Prázdný filtr</para>
            /// <para lang="en">Empty filter</para>
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    if (Items == null)
                    {
                        return true;
                    }

                    if (Items.Count == 0)
                    {
                        return true;
                    }

                    return false;
                }
            }

            /// <summary>
            /// <para lang="cs">Text podmínky pro výběr řádků tabulky (read-only)</para>
            /// <para lang="en">Condition text for table rows selection (read-only)</para>
            /// </summary>
            public string Condition
            {
                get
                {
                    if (IsEmpty)
                    {
                        return Functions.StrTrue;
                    }

                    else if (Items.Count == 1)
                    {
                        return
                            Items
                            .Select(a => a.Condition)
                            .FirstOrDefault<string>();
                    }

                    else
                    {
                        return
                            Items
                            .Select(a => a.Condition)
                            .Aggregate((a, b) => $"{a} AND {b}");
                    }
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Část podmínky pro výběr řádků v DataGridView pro vybraný sloupec</para>
            /// <para lang="en">Condition part for rows selection in DataGridView for selected column</para>
            /// </summary>
            /// <param name="column">
            /// <para lang="cs">Popis vybraného sloupce tabulky</para>
            /// <para lang="en">Description of the selected table column</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací část podmínky pro výběr řádků v DataGridView pro vybraný sloupec</para>
            /// <para lang="en">Returns part of the condition for rows selection in DataGridView for selected column</para>
            /// </returns>
            public IFilterRowItem Get(ColumnMetadata column)
            {
                if (column == null)
                {
                    return null;
                }

                return
                    Items
                    .Where(a => a.Column.Name == column.Name)
                    .FirstOrDefault<IFilterRowItem>();
            }

            /// <summary>
            /// <para lang="cs">Odstranění části podmínky pro výběr řádků v DataGridView pro vybraný sloupec</para>
            /// <para lang="en">Removing part of the condition for rows selection in DataGridView for selected column</para>
            /// </summary>
            /// <param name="column">
            /// <para lang="cs">Popis vybraného sloupce tabulky</para>
            /// <para lang="en">Description of the selected table column</para>
            /// </param>
            public void Remove(ColumnMetadata column)
            {
                IFilterRowItem item = Get(column: column);

                if (item != null)
                {
                    Items.Remove(item: item);
                }
            }

            /// <summary>
            /// <para lang="cs">Přidání části podmínky pro výběr řádků v DataGridView pro vybraný sloupec</para>
            /// <para lang="en">Adding part of the condition for rows selection in DataGridView for selected column</para>
            /// </summary>
            /// <param name="column">
            /// <para lang="cs">Popis vybraného sloupce tabulky</para>
            /// <para lang="en">Description of the selected table column</para>
            /// </param>
            /// <param name="selectedValues">
            /// <para lang="cs">Vybrané hodnoty ve sloupci</para>
            /// <para lang="en">Selected values in the column</para>
            /// </param>
            public void Add(ColumnMetadata column, List<string> selectedValues)
            {
                if (column == null)
                {
                    return;
                }

                Remove(column: column);

                if (selectedValues == null)
                {
                    return;
                }

                if (selectedValues.Count == 0)
                {
                    return;
                }

                items.Add(
                    new FilterRowItemEnumeration(
                        column: column,
                        selectedValues: selectedValues));
            }

            /// <summary>
            /// <para lang="cs">Přidání části podmínky pro výběr řádků v DataGridView pro vybraný sloupec</para>
            /// <para lang="en">Adding part of the condition for rows selection in DataGridView for selected column</para>
            /// </summary>
            /// <param name="column">
            /// <para lang="cs">Popis vybraného sloupce tabulky</para>
            /// <para lang="en">Description of the selected table column</para>
            /// </param>
            /// <param name="condition">
            /// <para lang="cs">Text podmínky pro výběr řádků tabulky</para>
            /// <para lang="en">Condition text for table rows selection</para>
            /// </param>
            public void Add(ColumnMetadata column, string condition)
            {
                if (column == null)
                {
                    return;
                }

                Remove(column: column);

                if (String.IsNullOrEmpty(value: condition))
                {
                    return;
                }

                items.Add(
                    new FilterRowItem(
                        column: column,
                        condition: condition));
            }

            /// <summary>
            /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
            /// <para lang="en">Determines whether specified object is equal to the current object</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Zadaný objekt</para>
            /// <para lang="en">Speicified object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not FilterRow Type, return False
                if (obj is not FilterRow)
                {
                    return false;
                }

                return
                    Condition == ((FilterRow)obj).Condition;
            }

            /// <summary>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </returns>
            public override int GetHashCode()
            {
                return
                    Condition.GetHashCode();
            }

            /// <summary>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </returns>
            public override string ToString()
            {
                return Condition;
            }

            #endregion Methods


            #region Internal Class

            /// <summary>
            /// <para lang="cs">Rozhraní pro část podmínky pro výběr řádků v DataGridView pro jeden sloupec</para>
            /// <para lang="en">Interface for condition part for rows selection in DataGridView for one column</para>
            /// </summary>
            internal interface IFilterRowItem
            {

                #region Properties

                /// <summary>
                /// <para lang="cs">Popis sloupce tabulky</para>
                /// <para lang="en">Description of the table column</para>
                /// </summary>
                ColumnMetadata Column { get; }

                /// <summary>
                /// <para lang="cs">Text podmínky pro výběr řádků tabulky (read-only)</para>
                /// <para lang="en">Condition text for table rows selection (read-only)</para>
                /// </summary>
                string Condition { get; }

                #endregion Properties

            }

            /// <summary>
            /// <para lang="cs">Část podmínky pro výběr řádků v DataGridView pro jeden sloupec</para>
            /// <para lang="en">Condition part for rows selection in DataGridView for one column</para>
            /// </summary>
            internal class FilterRowItemEnumeration : IFilterRowItem
            {

                #region Private Fields

                /// <summary>
                /// <para lang="cs">Popis sloupce tabulky</para>
                /// <para lang="en">Description of the table column</para>
                /// </summary>
                private ColumnMetadata column;

                /// <summary>
                /// <para lang="cs">Vybrané hodnoty ve sloupci</para>
                /// <para lang="en">Selected values in the column</para>
                /// </summary>
                private List<string> selectedValues;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// <para lang="cs">Konstruktor objektu</para>
                /// <para lang="en">Object constructor</para>
                /// </summary>
                /// <param name="column">
                /// <para lang="cs">Popis sloupce tabulky</para>
                /// <para lang="en">Description of the table column</para>
                /// </param>
                /// <param name="selectedValues">
                /// <para lang="cs">Vybrané hodnoty ve sloupci</para>
                /// <para lang="en">Selected values in the column</para>
                /// </param>
                public FilterRowItemEnumeration(
                    ColumnMetadata column,
                    List<string> selectedValues)
                {
                    Column = column;
                    SelectedValues = selectedValues;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// <para lang="cs">Popis sloupce tabulky</para>
                /// <para lang="en">Description of the table column</para>
                /// </summary>
                public ColumnMetadata Column
                {
                    get
                    {
                        return column ??
                            throw new ArgumentNullException(
                            message: $"Argument {nameof(Column)} must not be null.",
                            paramName: nameof(Column));
                    }
                    private set
                    {
                        column = value ??
                            throw new ArgumentNullException(
                            message: $"Argument {nameof(Column)} must not be null.",
                            paramName: nameof(Column));
                    }
                }

                /// <summary>
                /// <para lang="cs">Vybrané hodnoty ve sloupci</para>
                /// <para lang="en">Selected values in the column</para>
                /// </summary>
                public List<string> SelectedValues
                {
                    get
                    {
                        return selectedValues ??
                            throw new ArgumentNullException(
                            message: $"Argument {nameof(SelectedValues)} must not be null.",
                            paramName: nameof(SelectedValues));
                    }
                    private set
                    {
                        selectedValues = value ??
                            throw new ArgumentNullException(
                            message: $"Argument {nameof(SelectedValues)} must not be null.",
                            paramName: nameof(SelectedValues));
                    }
                }

                /// <summary>
                /// <para lang="cs">Text podmínky pro výběr řádků tabulky (read-only)</para>
                /// <para lang="en">Condition text for table rows selection (read-only)</para>
                /// </summary>
                public string Condition
                {
                    get
                    {
                        if (SelectedValues.Count == 0)
                        {
                            return $"(true)";
                        }

                        string strNullCondition =
                            SelectedValues.Contains(item: Functions.StrNull) ?
                            $"({Column.Name} IS NULL)" :
                            String.Empty;

                        List<string> values = SelectedValues
                            .Where(a => a != Functions.StrNull)
                            .ToList<string>();

                        string strValCondition;
                        if (values.Count == 0)
                        {
                            strValCondition = String.Empty;
                        }
                        else if (values.Count == 1)
                        {
                            string strItems = $"\'{values.FirstOrDefault<string>()}\'";
                            strValCondition = $"({Column.Name} IN ({strItems}))";
                        }
                        else
                        {
                            string strItems = values
                                .Select(a => a.ToString())
                                .Select(a => $"\'{a}\'")
                                .Aggregate((a, b) => $"{a}, {b}");
                            strValCondition = $"({Column.Name} IN ({strItems}))";
                        }

                        if (String.IsNullOrEmpty(value: strNullCondition))
                        {
                            if (String.IsNullOrEmpty(value: strValCondition))
                            {
                                return String.Empty;
                            }
                            else
                            {
                                return strValCondition;
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(value: strValCondition))
                            {
                                return strNullCondition;
                            }
                            else
                            {
                                return $"({strNullCondition} OR {strValCondition})";
                            }
                        }
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
                /// <para lang="en">Determines whether specified object is equal to the current object</para>
                /// </summary>
                /// <param name="obj">
                /// <para lang="cs">Zadaný objekt</para>
                /// <para lang="en">Speicified object</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">ano/ne</para>
                /// <para lang="en">true/false</para>
                /// </returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not FilterRowItemEnumeration Type, return False
                    if (obj is not FilterRowItemEnumeration)
                    {
                        return false;
                    }

                    return
                        Condition == ((FilterRowItemEnumeration)obj).Condition;
                }

                /// <summary>
                /// <para lang="cs">Vrací hash code</para>
                /// <para lang="en">Returns the hash code</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Vrací hash code</para>
                /// <para lang="en">Returns the hash code</para>
                /// </returns>
                public override int GetHashCode()
                {
                    return
                        Condition.GetHashCode();
                }

                /// <summary>
                /// <para lang="cs">Vrací text popisující objekt</para>
                /// <para lang="en">Returns a string that represents the current object</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Vrací text popisující objekt</para>
                /// <para lang="en">Returns a string that represents the current object</para>
                /// </returns>
                public override string ToString()
                {
                    return Condition;
                }

                #endregion Methods

            }

            /// <summary>
            /// <para lang="cs">Část podmínky pro výběr řádků v DataGridView pro jeden sloupec</para>
            /// <para lang="en">Condition part for rows selection in DataGridView for one column</para>
            /// </summary>
            internal class FilterRowItem : IFilterRowItem
            {

                #region Private Fields

                /// <summary>
                /// <para lang="cs">Popis sloupce tabulky</para>
                /// <para lang="en">Description of the table column</para>
                /// </summary>
                private ColumnMetadata column;

                /// <summary>
                /// <para lang="cs">Text podmínky pro výběr řádků tabulky</para>
                /// <para lang="en">Condition text for table rows selection</para>
                /// </summary>
                private string condition;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// <para lang="cs">Konstruktor objektu</para>
                /// <para lang="en">Object constructor</para>
                /// </summary>
                /// <param name="column">
                /// <para lang="cs">Popis sloupce tabulky</para>
                /// <para lang="en">Description of the table column</para>
                /// </param>
                /// <param name="condition">
                /// <para lang="cs">Text podmínky pro výběr řádků tabulky</para>
                /// <para lang="en">Condition text for table rows selection</para>
                /// </param>
                public FilterRowItem(
                    ColumnMetadata column,
                    string condition)
                {
                    Column = column;
                    Condition = condition;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// <para lang="cs">Popis sloupce tabulky</para>
                /// <para lang="en">Description of the table column</para>
                /// </summary>
                public ColumnMetadata Column
                {
                    get
                    {
                        return column ??
                            throw new ArgumentNullException(
                            message: $"Argument {nameof(Column)} must not be null.",
                            paramName: nameof(Column));
                    }
                    private set
                    {
                        column = value ??
                            throw new ArgumentNullException(
                            message: $"Argument {nameof(Column)} must not be null.",
                            paramName: nameof(Column));
                    }
                }

                /// <summary>
                /// <para lang="cs">Text podmínky pro výběr řádků tabulky (read-only)</para>
                /// <para lang="en">Condition text for table rows selection (read-only)</para>
                /// </summary>
                public string Condition
                {
                    get
                    {
                        return condition;
                    }
                    private set
                    {
                        condition = value;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
                /// <para lang="en">Determines whether specified object is equal to the current object</para>
                /// </summary>
                /// <param name="obj">
                /// <para lang="cs">Zadaný objekt</para>
                /// <para lang="en">Speicified object</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">ano/ne</para>
                /// <para lang="en">true/false</para>
                /// </returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not FilterRowItem Type, return False
                    if (obj is not FilterRowItem)
                    {
                        return false;
                    }

                    return
                        Condition == ((FilterRowItem)obj).Condition;
                }

                /// <summary>
                /// <para lang="cs">Vrací hash code</para>
                /// <para lang="en">Returns the hash code</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Vrací hash code</para>
                /// <para lang="en">Returns the hash code</para>
                /// </returns>
                public override int GetHashCode()
                {
                    return
                        Condition.GetHashCode();
                }

                /// <summary>
                /// <para lang="cs">Vrací text popisující objekt</para>
                /// <para lang="en">Returns a string that represents the current object</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Vrací text popisující objekt</para>
                /// <para lang="en">Returns a string that represents the current object</para>
                /// </returns>
                public override string ToString()
                {
                    return Condition;
                }

                #endregion Methods

            }

            #endregion Internal Class

        }

    }
}