﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba atributových kategorií pro odhad úhrnu"</para>
    /// <para lang="en">Control "Attribute categories selection for total estimate"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlVariable
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení nadpisu</para>
        /// <para lang="en">Control for display caption</para>
        /// </summary>
        private ControlCaption ctrCaption;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení kategorií plošných domén</para>
        /// <para lang="en">Control for display area domain categories</para>
        /// </summary>
        private ControlCategoryList<AreaDomainCategory> ctrAreaDomainCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení kategorií subpopulace</para>
        /// <para lang="en">Control for display subpopulation categories</para>
        /// </summary>
        private ControlCategoryList<SubPopulationCategory> ctrSubPopulationCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení atributových kategorií</para>
        /// <para lang="en">Control for display attribute categories</para>
        /// </summary>
        private ControlCategoryList<VariablePair> ctrAttributeCategories;

        #endregion Controls;


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlVariable(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraný indikátor (read-only)</para>
        /// <para lang="en">Selected indicator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedTargetVariable
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrTargetVariable
                    .SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru (read-only)</para>
        /// <para lang="en">Selected indicator metadata (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedTargetVariableMetadata
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrTargetVariable
                    .SelectedTargetVariableMetadata;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná jednotka (read-only)</para>
        /// <para lang="en">Selected unit of measure (read-only)</para>
        /// </summary>
        public Unit SelectedUnitOfMeasure
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrUnit
                    .SelectedUnitOfMeasure;
            }
        }

        // Výsledkem výběru v této komponentě jsou vybrané atributové kategorie:
        // The result of the selection in this component are selected attribute categories:

        /// <summary>
        /// <para lang="cs">Vybrané atributové kategorie</para>
        /// <para lang="en">Selected attribute categories</para>
        /// </summary>
        public VariablePairList SelectedVariables
        {
            get
            {
                return
                    ctrAttributeCategories.AttributeCategories;
            }
            set
            {
                ctrAttributeCategories.AttributeCategories = value;
                EnableButtonNext();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),        "Předchozí" },
                        { nameof(btnNext),            "Další" },
                        { nameof(cboAreaDomain),      "ExtendedLabelCs" },
                        { nameof(cboSubPopulation),   "ExtendedLabelCs" },
                        { nameof(grpAreaDomain),      "Plošná doména:" },
                        { nameof(grpIndicator),       "Indikátor:" },
                        { nameof(grpSubPopulation) ,  "Subpopulace:" }
                     }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlVariable),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),        "Previous" },
                        { nameof(btnNext),            "Next" },
                        { nameof(cboAreaDomain),      "ExtendedLabelEn" },
                        { nameof(cboSubPopulation),   "ExtendedLabelEn" },
                        { nameof(grpAreaDomain),      "Area domain:" },
                        { nameof(grpIndicator),       "Indicator:" },
                        { nameof(grpSubPopulation),   "Subpopulation:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlVariable),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            InitializeCaption();

            InitializeCategoryLists();

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            EnableButtonNext();

            onEdit = true;

            pnlMain.BackColor = Setting.BorderColor;
            pnlWorkSpace.BackColor = Setting.BorderColor;

            splCaption.BackColor = Setting.SpliterColor;
            splCaption.SplitterDistance = Setting.SplVariableCaptionDistance;

            splVariable.BackColor = Setting.SpliterColor;
            splVariable.SplitterDistance = Setting.SplVariableDistance;

            onEdit = false;

            btnPrevious.Click += new EventHandler(
              (sender, e) =>
              {
                  PreviousClick?.Invoke(
                       sender: this,
                       e: new EventArgs());
              });

            btnNext.Click += new EventHandler(
                (sender, e) =>
                {
                    NextClick?.Invoke(
                       sender: this,
                       e: new EventArgs());
                });

            cboAreaDomain.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SetAttributeCategories();
                    }
                });

            cboSubPopulation.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SetAttributeCategories();
                    }
                });

            splCaption.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplVariableCaptionDistance =
                            splCaption.SplitterDistance;
                    }
                });

            splVariable.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplVariableDistance =
                            splVariable.SplitterDistance;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku nadpisu</para>
        /// <para lang="en">Initialization of the control for display caption</para>
        /// </summary>
        private void InitializeCaption()
        {
            pnlIndicator.Controls.Clear();
            ctrCaption = new ControlCaption(
                controlOwner: this,
                numeratorTargetVariableMetadata: SelectedTargetVariableMetadata,
                denominatorTargetVariableMetadata: null,
                variablePairs: null,
                captionVersion: CaptionVersion.Extended,
                withAttributeCategories: false,
                withIdentifiers: true)
            {
                Dock = DockStyle.Fill
            };
            pnlIndicator.Controls.Add(value: ctrCaption);
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamů atributových kategorií</para>
        /// <para lang="en">Initialization of attribute category lists</para>
        /// </summary>
        private void InitializeCategoryLists()
        {
            pnlAreaDomainCategories.Controls.Clear();
            ctrAreaDomainCategories =
                new ControlCategoryList<AreaDomainCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            pnlAreaDomainCategories.Controls.Add(
                value: ctrAreaDomainCategories);

            pnlSubPopulationCategories.Controls.Clear();
            ctrSubPopulationCategories =
                new ControlCategoryList<SubPopulationCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            pnlSubPopulationCategories.Controls.Add(
                value: ctrSubPopulationCategories);

            pnlAttributeCategories.Controls.Clear();
            ctrAttributeCategories =
                new ControlCategoryList<VariablePair>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            pnlAttributeCategories.Controls.Add(
                value: ctrAttributeCategories);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            onEdit = true;
            cboAreaDomain.DisplayMember =
                labels.TryGetValue(key: nameof(cboAreaDomain),
                out string cboAreaDomainText)
                    ? cboAreaDomainText
                    : String.Empty;
            onEdit = false;

            onEdit = true;
            cboSubPopulation.DisplayMember =
                labels.TryGetValue(key: nameof(cboSubPopulation),
                out string cboSubPopulationText)
                    ? cboSubPopulationText
                    : String.Empty;
            onEdit = false;

            grpAreaDomain.Text =
                labels.TryGetValue(key: nameof(grpAreaDomain),
                out string grpAreaDomainText)
                    ? grpAreaDomainText
                    : String.Empty;


            grpIndicator.Text =
                labels.TryGetValue(key: nameof(grpIndicator),
                out string grpIndicatorText)
                    ? grpIndicatorText
                    : String.Empty;

            grpSubPopulation.Text =
                labels.TryGetValue(key: nameof(grpSubPopulation),
                out string grpSubPopulationText)
                    ? grpSubPopulationText
                    : String.Empty;

            ctrCaption?.InitializeLabels();

            ctrAreaDomainCategories?.InitializeLabels();

            ctrSubPopulationCategories?.InitializeLabels();

            ctrAttributeCategories?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">
        /// Povolí nebo zakáže přístup na další komponentu,
        /// podle toho, zda je nebo není vybraný indikátor a jeho metadata,
        /// jednotka a kategorie atributového členění
        /// </para>
        /// <para lang="en">
        /// Enables or disables access to the next component
        /// depending on whether or not the indicator and its metadata are selected,
        /// the unit of measure and attribute categories are selected
        /// </para>
        /// </summary>
        private void EnableButtonNext()
        {
            btnNext.Enabled =
                (SelectedTargetVariable != null) &&
                (SelectedTargetVariableMetadata != null) &&
                (SelectedUnitOfMeasure != null) &&
                (SelectedVariables != null) &&
                (SelectedVariables.Count > 0);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            SetComboAreaDomain();
            SetComboSubPopulation();

            SetAttributeCategories();
        }

        /// <summary>
        /// <para lang="cs">Nastaví položky v ComboBox AreaDomain</para>
        /// <para lang="en">Method sets items in ComboBox AreaDomain</para>
        /// </summary>
        private void SetComboAreaDomain()
        {
            List<AreaDomain> areaDomainItems
                = NfiEstaFunctions.FnGetAreaDomainsForTargetVariable
                    .Execute(
                        database: Database,
                        targetVariableId: SelectedTargetVariable.Id,
                        numeratorAreaDomainId: null)
                    .Items;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            onEdit = true;
            cboAreaDomain.DataSource = areaDomainItems;
            cboAreaDomain.DisplayMember =
                labels.TryGetValue(key: nameof(cboAreaDomain),
                out string cboAreaDomainText)
                    ? cboAreaDomainText
                    : String.Empty;
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Nastaví položky v ComboBox SubPopulation</para>
        /// <para lang="en">Method sets items in ComboBox SubPopulation</para>
        /// </summary>
        private void SetComboSubPopulation()
        {
            List<SubPopulation> subPopulationItems
                = NfiEstaFunctions.FnGetSubPopulationsForTargetVariable
                    .Execute(
                        database: Database,
                        targetVariableId: SelectedTargetVariable.Id,
                        numeratorSubPopulationId: null)
                    .Items;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            onEdit = true;
            cboSubPopulation.DataSource = subPopulationItems;
            cboSubPopulation.DisplayMember =
                labels.TryGetValue(key: nameof(cboSubPopulation),
                out string cboSubPopulationText)
                    ? cboSubPopulationText
                    : String.Empty;
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Výběr a zobrazení atributových kategorií</para>
        /// <para lang="en">Select and display attribute categories</para>
        /// </summary>
        private void SetAttributeCategories()
        {
            if ((cboAreaDomain.SelectedItem == null) ||
                (cboSubPopulation.SelectedItem == null))
            {
                if (cboAreaDomain.SelectedItem == null)
                {
                    ctrAreaDomainCategories.Clear();
                }
                if (cboSubPopulation.SelectedItem == null)
                {
                    ctrSubPopulationCategories.Clear();
                }
                ctrAttributeCategories.Clear();
                return;
            }

            AreaDomain selectedAreaDomain = (AreaDomain)cboAreaDomain.SelectedItem;

            SubPopulation selectedSubPopulation = (SubPopulation)cboSubPopulation.SelectedItem;

            ctrAreaDomainCategories.AreaDomainCategories =
                NfiEstaFunctions.FnGetAreaSubPopulationCategories.AreaDomainCategories(
                    database: Database,
                    cAreaDomainCategory: Database.SNfiEsta.CAreaDomainCategory,
                    targetVariable: SelectedTargetVariable,
                    areaDomain: selectedAreaDomain);

            ctrSubPopulationCategories.SubPopulationCategories =
                NfiEstaFunctions.FnGetAreaSubPopulationCategories.SubPopulationCategories(
                    database: Database,
                    cSubPopulationCategory: Database.SNfiEsta.CSubPopulationCategory,
                    targetVariable: SelectedTargetVariable,
                    subPopulation: selectedSubPopulation);

            SelectedVariables =
                NfiEstaFunctions.FnGetAttributeCategoriesForTargetVariable.Execute(
                    database: Database,
                    vwVariable: Database.SNfiEsta.VVariable,
                    numeratorTargetVariableId: SelectedTargetVariable.Id,
                    numeratorAreaDomainId: selectedAreaDomain.Id,
                    numeratorSubPopulationId: selectedSubPopulation.Id,
                    denominatorTargetVariableId: null,
                    denominatorAreaDomainId: null,
                    denominatorSubPopulationId: null);
        }

        #endregion Methods

    }

}
