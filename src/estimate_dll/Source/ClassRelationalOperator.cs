﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Třída: položka relačního operátoru</para>
        /// <para lang="en">Class: relational operator item</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        internal class RelationalOperator
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Identifikátor</para>
            /// <para lang="en">Identifier</para>
            /// </summary>
            private int id;

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            private LanguageFile languageFile;

            #endregion Private Fields


            #region Private Strings

            private string strIsEqual = String.Empty;
            private string strIsNotEqual = String.Empty;
            private string strIsGreater = String.Empty;
            private string strIsGreaterOrEqual = String.Empty;
            private string strIsLess = String.Empty;
            private string strIsLessOrEqual = String.Empty;

            #endregion Private Strings


            #region Properties

            /// <summary>
            /// <para lang="cs">Identifikátor</para>
            /// <para lang="en">Identifier</para>
            /// </summary>
            public int Id
            {
                get
                {
                    return id;
                }
                private set
                {
                    id =
                       new List<int>() { 100, 200, 300, 400, 500, 600 }.Contains(item: value) ?
                       value :
                       0;
                }
            }

            /// <summary>
            /// <para lang="cs">Operátor</para>
            /// <para lang="en">Operator</para>
            /// </summary>
            public string Operator
            {
                get
                {
                    return Id switch
                    {
                        100 => "=",
                        200 => "!=",
                        300 => ">",
                        400 => ">=",
                        500 => "<",
                        600 => "<=",
                        _ => String.Empty,
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Popisek národní</para>
            /// <para lang="en">Label national</para>
            /// </summary>
            public string LabelCs
            {
                get
                {
                    Dictionary<string, string> labels = Dictionary(
                        languageVersion: LanguageVersion.National,
                        languageFile: LanguageFile);

                    return Id switch
                    {
                        100 =>
                            labels.TryGetValue(key: nameof(strIsEqual), out strIsEqual)
                                ? strIsEqual
                                : String.Empty,

                        200 =>
                            labels.TryGetValue(key: nameof(strIsNotEqual), out strIsNotEqual)
                                ? strIsNotEqual
                                : String.Empty,

                        300 =>
                            labels.TryGetValue(key: nameof(strIsGreater), out strIsGreater)
                                ? strIsGreater
                                : String.Empty,

                        400 =>
                            labels.TryGetValue(key: nameof(strIsGreaterOrEqual), out strIsGreaterOrEqual)
                                ? strIsGreaterOrEqual
                                : String.Empty,

                        500 =>
                            labels.TryGetValue(key: nameof(strIsLess), out strIsLess)
                                ? strIsLess
                                : String.Empty,

                        600 =>
                            labels.TryGetValue(key: nameof(strIsLessOrEqual), out strIsLessOrEqual)
                                ? strIsLessOrEqual
                                : String.Empty,

                        _ => String.Empty,
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Popisek mezinárodní</para>
            /// <para lang="en">Label international</para>
            /// </summary>
            public string LabelEn
            {
                get
                {
                    Dictionary<string, string> labels = Dictionary(
                       languageVersion: LanguageVersion.International,
                       languageFile: LanguageFile);

                    return Id switch
                    {
                        100 =>
                            labels.TryGetValue(key: nameof(strIsEqual), out strIsEqual)
                                ? strIsEqual
                                : String.Empty,

                        200 =>
                            labels.TryGetValue(key: nameof(strIsNotEqual), out strIsNotEqual)
                                ? strIsNotEqual
                                : String.Empty,

                        300 =>
                            labels.TryGetValue(key: nameof(strIsGreater), out strIsGreater)
                                ? strIsGreater
                                : String.Empty,

                        400 =>
                            labels.TryGetValue(key: nameof(strIsGreaterOrEqual), out strIsGreaterOrEqual)
                                ? strIsGreaterOrEqual
                                : String.Empty,

                        500 =>
                            labels.TryGetValue(key: nameof(strIsLess), out strIsLess)
                                ? strIsLess
                                : String.Empty,

                        600 =>
                            labels.TryGetValue(key: nameof(strIsLessOrEqual), out strIsLessOrEqual)
                                ? strIsLessOrEqual
                                : String.Empty,

                        _ => String.Empty,
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Operátor není vybraný</para>
            /// <para lang="en">Operator is not selected</para>
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    return Id == 0;
                }
            }

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            private LanguageFile LanguageFile
            {
                get
                {
                    return languageFile;
                }
                set
                {
                    languageFile = value;
                }
            }

            #endregion Properties


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="languageFile"></param>
            public RelationalOperator(LanguageFile languageFile = null)
            {
                Id = 0;
                LanguageFile = languageFile;
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="val">
            /// <para lang="cs">Operátor</para>
            /// <para lang="en">Operator</para>
            /// </param>
            /// <param name="languageFile"></param>
            public RelationalOperator(string val, LanguageFile languageFile = null)
            {
                Id = val switch
                {
                    "=" => 100,
                    "!=" => 200,
                    ">" => 300,
                    ">=" => 400,
                    "<" => 500,
                    "<=" => 600,
                    _ => 0,
                };
                LanguageFile = languageFile;
            }

            #endregion Constructor


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Seznam relačních operátorů</para>
            /// <para lang="en">List of relational operators</para>
            /// </summary>
            /// <param name="languageFile"></param>
            /// <returns></returns>
            public static List<RelationalOperator> RelationalOperators(LanguageFile languageFile = null)
            {
                return
                    [.. new List<RelationalOperator>()
                    {
                            new(languageFile: languageFile),
                            new(val: ">", languageFile: languageFile),
                            new(val: "<", languageFile: languageFile),
                    }
                    .OrderBy(a => a.Id)];
            }

            /// <summary>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Zvolený jazyk</para>
            /// <para lang="en">Selected language</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">File with control labels for national and international version</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts in selected language</para>
            /// </returns>
            public static Dictionary<string, string> Dictionary(
                LanguageVersion languageVersion,
                LanguageFile languageFile)
            {
                return languageVersion switch
                {
                    LanguageVersion.National => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(strIsEqual),                   "Je rovno" },
                            { nameof(strIsNotEqual),                "Není rovno" },
                            { nameof(strIsGreater),                 "Je větší než" },
                            { nameof(strIsGreaterOrEqual),          "Je větší než nebo rovno" },
                            { nameof(strIsLess),                    "Je menší než" },
                            { nameof(strIsLessOrEqual),             "Je menší než nebo rovno" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: nameof(RelationalOperator),
                            out Dictionary<string, string> dictNational)
                                ? dictNational
                                : [],

                    LanguageVersion.International => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(strIsEqual),                   "Is equal to" },
                            { nameof(strIsNotEqual),                "Is not equal to" },
                            { nameof(strIsGreater),                 "Is greater than" },
                            { nameof(strIsGreaterOrEqual),          "Is greater than or equal to" },
                            { nameof(strIsLess),                    "Is less than" },
                            { nameof(strIsLessOrEqual),             "Is less than or equal to" }
                         }
                         : languageFile.InternationalVersion.Data.TryGetValue(
                            key: nameof(RelationalOperator),
                            out Dictionary<string, string> dictInternational)
                                ? dictInternational
                                : [],

                    _ => [],
                };
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true/false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not RelationalOperatorItem Type, return False
                if (obj is not RelationalOperator)
                {
                    return false;
                }

                return
                    Id == ((RelationalOperator)obj).Id;
            }

            /// <summary>
            /// Returns the hash code
            /// </summary>
            /// <returns>Hash code</returns>
            public override int GetHashCode()
            {
                return
                    Id.GetHashCode();
            }

            /// <summary>
            /// Returns the string that represents current object
            /// </summary>
            /// <returns>Returns the string that represents current object</returns>
            public override string ToString()
            {
                return Operator;
            }

            #endregion Methods

        }

    }
}
