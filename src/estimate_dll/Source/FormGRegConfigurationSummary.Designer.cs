﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormGRegConfigurationSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            pnlWorkSpace = new System.Windows.Forms.Panel();
            tlpSummary = new System.Windows.Forms.TableLayoutPanel();
            lblForceSyntheticValue = new System.Windows.Forms.Label();
            lblForceSyntheticCaption = new System.Windows.Forms.Label();
            lblParamAreaTypeValue = new System.Windows.Forms.Label();
            lblParamAreaTypeCaption = new System.Windows.Forms.Label();
            lblSigmaValue = new System.Windows.Forms.Label();
            lblSigmaCaption = new System.Windows.Forms.Label();
            lblWorkingModelValue = new System.Windows.Forms.Label();
            lblWorkingModelCaption = new System.Windows.Forms.Label();
            lblPanelRefYearSetGroupValue = new System.Windows.Forms.Label();
            lblPanelRefYearSetGroupCaption = new System.Windows.Forms.Label();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlOK.SuspendLayout();
            pnlCancel.SuspendLayout();
            tlpWorkSpace.SuspendLayout();
            pnlWorkSpace.SuspendLayout();
            tlpSummary.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.SystemColors.Control;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(944, 501);
            pnlMain.TabIndex = 8;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.SystemColors.Control;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Controls.Add(tlpWorkSpace, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            pnlButtons.BackColor = System.Drawing.SystemColors.Control;
            pnlButtons.Controls.Add(tlpButtons);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 461);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(944, 40);
            pnlButtons.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 2;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 15;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 16;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 12;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // tlpWorkSpace
            // 
            tlpWorkSpace.ColumnCount = 1;
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Controls.Add(pnlWorkSpace, 0, 0);
            tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpWorkSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tlpWorkSpace.Location = new System.Drawing.Point(0, 0);
            tlpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            tlpWorkSpace.Name = "tlpWorkSpace";
            tlpWorkSpace.RowCount = 1;
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 401F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 401F));
            tlpWorkSpace.Size = new System.Drawing.Size(944, 461);
            tlpWorkSpace.TabIndex = 4;
            // 
            // pnlWorkSpace
            // 
            pnlWorkSpace.AutoScroll = true;
            pnlWorkSpace.BackColor = System.Drawing.Color.White;
            pnlWorkSpace.Controls.Add(tlpSummary);
            pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlWorkSpace.ForeColor = System.Drawing.Color.Black;
            pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            pnlWorkSpace.Name = "pnlWorkSpace";
            pnlWorkSpace.Padding = new System.Windows.Forms.Padding(10);
            pnlWorkSpace.Size = new System.Drawing.Size(944, 461);
            pnlWorkSpace.TabIndex = 0;
            // 
            // tlpSummary
            // 
            tlpSummary.ColumnCount = 2;
            tlpSummary.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            tlpSummary.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSummary.Controls.Add(lblForceSyntheticValue, 1, 4);
            tlpSummary.Controls.Add(lblForceSyntheticCaption, 0, 4);
            tlpSummary.Controls.Add(lblParamAreaTypeValue, 1, 3);
            tlpSummary.Controls.Add(lblParamAreaTypeCaption, 0, 3);
            tlpSummary.Controls.Add(lblSigmaValue, 1, 2);
            tlpSummary.Controls.Add(lblSigmaCaption, 0, 2);
            tlpSummary.Controls.Add(lblWorkingModelValue, 1, 1);
            tlpSummary.Controls.Add(lblWorkingModelCaption, 0, 1);
            tlpSummary.Controls.Add(lblPanelRefYearSetGroupValue, 1, 0);
            tlpSummary.Controls.Add(lblPanelRefYearSetGroupCaption, 0, 0);
            tlpSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSummary.Location = new System.Drawing.Point(10, 10);
            tlpSummary.Margin = new System.Windows.Forms.Padding(0);
            tlpSummary.Name = "tlpSummary";
            tlpSummary.RowCount = 6;
            tlpSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpSummary.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSummary.Size = new System.Drawing.Size(924, 441);
            tlpSummary.TabIndex = 1;
            // 
            // lblForceSyntheticValue
            // 
            lblForceSyntheticValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblForceSyntheticValue.Location = new System.Drawing.Point(253, 100);
            lblForceSyntheticValue.Name = "lblForceSyntheticValue";
            lblForceSyntheticValue.Size = new System.Drawing.Size(668, 25);
            lblForceSyntheticValue.TabIndex = 9;
            lblForceSyntheticValue.Text = "lblForceSyntheticValue";
            lblForceSyntheticValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblForceSyntheticCaption
            // 
            lblForceSyntheticCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblForceSyntheticCaption.Location = new System.Drawing.Point(3, 100);
            lblForceSyntheticCaption.Name = "lblForceSyntheticCaption";
            lblForceSyntheticCaption.Size = new System.Drawing.Size(244, 25);
            lblForceSyntheticCaption.TabIndex = 8;
            lblForceSyntheticCaption.Text = "lblForceSyntheticCaption";
            lblForceSyntheticCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblParamAreaTypeValue
            // 
            lblParamAreaTypeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblParamAreaTypeValue.Location = new System.Drawing.Point(253, 75);
            lblParamAreaTypeValue.Name = "lblParamAreaTypeValue";
            lblParamAreaTypeValue.Size = new System.Drawing.Size(668, 25);
            lblParamAreaTypeValue.TabIndex = 7;
            lblParamAreaTypeValue.Text = "lblParamAreaTypeValue";
            lblParamAreaTypeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblParamAreaTypeCaption
            // 
            lblParamAreaTypeCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblParamAreaTypeCaption.Location = new System.Drawing.Point(3, 75);
            lblParamAreaTypeCaption.Name = "lblParamAreaTypeCaption";
            lblParamAreaTypeCaption.Size = new System.Drawing.Size(244, 25);
            lblParamAreaTypeCaption.TabIndex = 6;
            lblParamAreaTypeCaption.Text = "lblParamAreaTypeCaption";
            lblParamAreaTypeCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSigmaValue
            // 
            lblSigmaValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSigmaValue.Location = new System.Drawing.Point(253, 50);
            lblSigmaValue.Name = "lblSigmaValue";
            lblSigmaValue.Size = new System.Drawing.Size(668, 25);
            lblSigmaValue.TabIndex = 5;
            lblSigmaValue.Text = "lblSigmaValue";
            lblSigmaValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSigmaCaption
            // 
            lblSigmaCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSigmaCaption.Location = new System.Drawing.Point(3, 50);
            lblSigmaCaption.Name = "lblSigmaCaption";
            lblSigmaCaption.Size = new System.Drawing.Size(244, 25);
            lblSigmaCaption.TabIndex = 4;
            lblSigmaCaption.Text = "lblSigmaCaption";
            lblSigmaCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWorkingModelValue
            // 
            lblWorkingModelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblWorkingModelValue.Location = new System.Drawing.Point(253, 25);
            lblWorkingModelValue.Name = "lblWorkingModelValue";
            lblWorkingModelValue.Size = new System.Drawing.Size(668, 25);
            lblWorkingModelValue.TabIndex = 3;
            lblWorkingModelValue.Text = "lblWorkingModelValue";
            lblWorkingModelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWorkingModelCaption
            // 
            lblWorkingModelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblWorkingModelCaption.Location = new System.Drawing.Point(3, 25);
            lblWorkingModelCaption.Name = "lblWorkingModelCaption";
            lblWorkingModelCaption.Size = new System.Drawing.Size(244, 25);
            lblWorkingModelCaption.TabIndex = 2;
            lblWorkingModelCaption.Text = "lblWorkingModelCaption";
            lblWorkingModelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupValue
            // 
            lblPanelRefYearSetGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelRefYearSetGroupValue.Location = new System.Drawing.Point(253, 0);
            lblPanelRefYearSetGroupValue.Name = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.Size = new System.Drawing.Size(668, 25);
            lblPanelRefYearSetGroupValue.TabIndex = 1;
            lblPanelRefYearSetGroupValue.Text = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupCaption
            // 
            lblPanelRefYearSetGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelRefYearSetGroupCaption.Location = new System.Drawing.Point(3, 0);
            lblPanelRefYearSetGroupCaption.Name = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.Size = new System.Drawing.Size(244, 25);
            lblPanelRefYearSetGroupCaption.TabIndex = 0;
            lblPanelRefYearSetGroupCaption.Text = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormGRegConfigurationSummary
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormGRegConfigurationSummary";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormGRegConfigurationSummary";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            tlpWorkSpace.ResumeLayout(false);
            pnlWorkSpace.ResumeLayout(false);
            tlpSummary.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.TableLayoutPanel tlpSummary;
        private System.Windows.Forms.Label lblForceSyntheticValue;
        private System.Windows.Forms.Label lblForceSyntheticCaption;
        private System.Windows.Forms.Label lblParamAreaTypeValue;
        private System.Windows.Forms.Label lblParamAreaTypeCaption;
        private System.Windows.Forms.Label lblSigmaValue;
        private System.Windows.Forms.Label lblSigmaCaption;
        private System.Windows.Forms.Label lblWorkingModelValue;
        private System.Windows.Forms.Label lblWorkingModelCaption;
        private System.Windows.Forms.Label lblPanelRefYearSetGroupValue;
        private System.Windows.Forms.Label lblPanelRefYearSetGroupCaption;
    }

}