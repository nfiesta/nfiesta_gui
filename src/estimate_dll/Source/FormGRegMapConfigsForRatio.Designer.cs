﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{
    partial class FormGRegMapConfigsForRatio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            pnlWorkSpace = new System.Windows.Forms.Panel();
            pnlMessages = new System.Windows.Forms.Panel();
            lblDifferentListLength = new System.Windows.Forms.Label();
            lblNoDenominatorConfigurations = new System.Windows.Forms.Label();
            lblNoNumeratorConfigurations = new System.Windows.Forms.Label();
            lblNoSelectedCombination = new System.Windows.Forms.Label();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlOK.SuspendLayout();
            pnlCancel.SuspendLayout();
            tlpWorkSpace.SuspendLayout();
            pnlMessages.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.SystemColors.Control;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Controls.Add(tlpWorkSpace, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 3;
            // 
            // pnlButtons
            // 
            pnlButtons.BackColor = System.Drawing.SystemColors.Control;
            pnlButtons.Controls.Add(tlpButtons);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 461);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(944, 40);
            pnlButtons.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 2;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 15;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 16;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 12;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // tlpWorkSpace
            // 
            tlpWorkSpace.ColumnCount = 1;
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Controls.Add(pnlWorkSpace, 0, 0);
            tlpWorkSpace.Controls.Add(pnlMessages, 0, 1);
            tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpWorkSpace.Location = new System.Drawing.Point(0, 0);
            tlpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            tlpWorkSpace.Name = "tlpWorkSpace";
            tlpWorkSpace.RowCount = 2;
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpWorkSpace.Size = new System.Drawing.Size(944, 461);
            tlpWorkSpace.TabIndex = 6;
            // 
            // pnlWorkSpace
            // 
            pnlWorkSpace.AutoScroll = true;
            pnlWorkSpace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            pnlWorkSpace.Name = "pnlWorkSpace";
            pnlWorkSpace.Size = new System.Drawing.Size(944, 421);
            pnlWorkSpace.TabIndex = 5;
            // 
            // pnlMessages
            // 
            pnlMessages.Controls.Add(lblDifferentListLength);
            pnlMessages.Controls.Add(lblNoDenominatorConfigurations);
            pnlMessages.Controls.Add(lblNoNumeratorConfigurations);
            pnlMessages.Controls.Add(lblNoSelectedCombination);
            pnlMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMessages.Location = new System.Drawing.Point(0, 421);
            pnlMessages.Margin = new System.Windows.Forms.Padding(0);
            pnlMessages.Name = "pnlMessages";
            pnlMessages.Size = new System.Drawing.Size(944, 40);
            pnlMessages.TabIndex = 5;
            // 
            // lblDifferentListLength
            // 
            lblDifferentListLength.ForeColor = System.Drawing.Color.Red;
            lblDifferentListLength.Location = new System.Drawing.Point(570, 12);
            lblDifferentListLength.Margin = new System.Windows.Forms.Padding(0);
            lblDifferentListLength.Name = "lblDifferentListLength";
            lblDifferentListLength.Size = new System.Drawing.Size(121, 15);
            lblDifferentListLength.TabIndex = 14;
            lblDifferentListLength.Text = "lblDifferentListLength";
            lblDifferentListLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblDifferentListLength.Visible = false;
            // 
            // lblNoDenominatorConfigurations
            // 
            lblNoDenominatorConfigurations.ForeColor = System.Drawing.Color.Red;
            lblNoDenominatorConfigurations.Location = new System.Drawing.Point(366, 12);
            lblNoDenominatorConfigurations.Margin = new System.Windows.Forms.Padding(0);
            lblNoDenominatorConfigurations.Name = "lblNoDenominatorConfigurations";
            lblNoDenominatorConfigurations.Size = new System.Drawing.Size(185, 15);
            lblNoDenominatorConfigurations.TabIndex = 13;
            lblNoDenominatorConfigurations.Text = "lblNoDenominatorConfigurations";
            lblNoDenominatorConfigurations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblNoDenominatorConfigurations.Visible = false;
            // 
            // lblNoNumeratorConfigurations
            // 
            lblNoNumeratorConfigurations.ForeColor = System.Drawing.Color.Red;
            lblNoNumeratorConfigurations.Location = new System.Drawing.Point(182, 12);
            lblNoNumeratorConfigurations.Margin = new System.Windows.Forms.Padding(0);
            lblNoNumeratorConfigurations.Name = "lblNoNumeratorConfigurations";
            lblNoNumeratorConfigurations.Size = new System.Drawing.Size(173, 15);
            lblNoNumeratorConfigurations.TabIndex = 12;
            lblNoNumeratorConfigurations.Text = "lblNoNumeratorConfigurations";
            lblNoNumeratorConfigurations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblNoNumeratorConfigurations.Visible = false;
            // 
            // lblNoSelectedCombination
            // 
            lblNoSelectedCombination.ForeColor = System.Drawing.Color.Red;
            lblNoSelectedCombination.Location = new System.Drawing.Point(9, 12);
            lblNoSelectedCombination.Margin = new System.Windows.Forms.Padding(0);
            lblNoSelectedCombination.Name = "lblNoSelectedCombination";
            lblNoSelectedCombination.Size = new System.Drawing.Size(150, 15);
            lblNoSelectedCombination.TabIndex = 11;
            lblNoSelectedCombination.Text = "lblNoSelectedCombination";
            lblNoSelectedCombination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblNoSelectedCombination.Visible = false;
            // 
            // FormGRegMapConfigsForRatio
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormGRegMapConfigsForRatio";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormGRegMapConfigsForRatio";
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            tlpWorkSpace.ResumeLayout(false);
            pnlMessages.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlMessages;
        private System.Windows.Forms.Label lblNoSelectedCombination;
        private System.Windows.Forms.Label lblDifferentListLength;
        private System.Windows.Forms.Label lblNoDenominatorConfigurations;
        private System.Windows.Forms.Label lblNoNumeratorConfigurations;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.Panel pnlWorkSpace;
    }
}