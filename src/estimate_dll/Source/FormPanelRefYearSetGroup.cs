﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro editaci skupin panelů a roků měření</para>
    /// <para lang="en">Form for editing panel and reference year set groups</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormPanelRefYearSetGroup
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Editace skupin panelů a roků měření není povolena</para>
        /// <para lang="en">Editing of panel and reference year set groups is not allowed</para>
        /// </summary>
        private bool readOnlyMode;

        /// <summary>
        /// <para lang="cs">Indikátor změny v databázi</para>
        /// <para lang="en">Database changed indicator</para>
        /// </summary>
        private bool dbChanged;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        private string msgNoneSelectedPanelRefYearSetGroup = String.Empty;
        private string msgSelectedPanelRefYearSetGroupNotExists = String.Empty;
        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        private string strRowsCountNone = String.Empty;
        private string strRowsCountOne = String.Empty;
        private string strRowsCountSome = String.Empty;
        private string strRowsCountMany = String.Empty;

        /// <summary>
        /// <para lang="cs">Seznam skupin panelů a roků měření, který vrací funkce fn_api_get_list_of_panel_refyearset_groups</para>
        /// <para lang="en">List of groups of panels and reference-year sets returned by fn_api_get_list_of_panel_refyearset_groups function</para>
        /// </summary>
        private PanelRefYearSetGroupList panelRefYearSetGroupList;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormPanelRefYearSetGroup(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Editace skupin panelů a roků měření není povolena</para>
        /// <para lang="en">Editing of panel and reference year set groups is not allowed</para>
        /// </summary>
        public bool ReadOnlyMode
        {
            get
            {
                return readOnlyMode;
            }
            set
            {
                readOnlyMode = value;

                txtLabelCsValue.Enabled = !ReadOnlyMode;
                txtLabelEnValue.Enabled = !ReadOnlyMode;
                txtDescriptionCsValue.Enabled = !ReadOnlyMode;
                txtDescriptionEnValue.Enabled = !ReadOnlyMode;

                btnDelete.Visible = !ReadOnlyMode;
                btnUpdate.Visible = !ReadOnlyMode;
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor změny v databázi</para>
        /// <para lang="en">Database changed indicator</para>
        /// </summary>
        public bool DbChanged
        {
            get
            {
                return dbChanged;
            }
            set
            {
                dbChanged = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPanelRefYearSetGroup),                   "Skupiny panelů s odpovídajícími roky měření" },
                        { nameof(btnClose),                                   "Zavřít" },
                        { nameof(btnDelete),                                  "Smazat" },
                        { nameof(btnUpdate),                                  "Zapsat do databáze" },
                        { nameof(grpPanelRefYearSetGroups),                   "Skupiny panelů:" },
                        { nameof(grpSelectedPanelRefYearSetGroup),            "Vybraná skupina panelů:" },
                        { nameof(grpPanels),                                  "Skupina panelů:" },
                        { nameof(lblIdCaption),                               "Identifikační číslo:" },
                        { nameof(lblLabelCsCaption),                          "Zkratka (národní):" },
                        { nameof(lblDescriptionCsCaption),                    "Popis (národní):" },
                        { nameof(lblLabelEnCaption),                          "Zkratka (anglická):" },
                        { nameof(lblDescriptionEnCaption),                    "Popis (anglický):" },
                        { nameof(lstPanelRefYearSetGroups),                   "LabelCs" },
                        { nameof(msgNoneSelectedPanelRefYearSetGroup),        "Není vybrána žádná skupina panelů."},
                        { nameof(msgSelectedPanelRefYearSetGroupNotExists),   "Vybraná skupina panelů neexistuje."},
                        { nameof(msgLabelCsIsEmpty),                          "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),                    "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),                          "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),                    "Popis (anglický) musí být vyplněn." },
                        { nameof(msgLabelCsExists),                           "Použitá zkratka (národní) již existuje pro jinou skupinu panelů." },
                        { nameof(msgDescriptionCsExists),                     "Použitý popis (národní) již existuje pro jinou skupinu panelů." },
                        { nameof(msgLabelEnExists),                           "Použitá zkratka (anglická) již existuje pro jinou skupinu panelů." },
                        { nameof(msgDescriptionEnExists),                     "Použitý popis (anglický) již existuje pro jinou skupinu panelů." },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Name}",
                                "Popisek panelu" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Name}",
                                "Popisek panelu" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Name}",
                                "Popisek roků měření" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Name}",
                                "Popisek roků měření" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.Name}",
                                "Celkový počet klastrů v panelu" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.Name}",
                                "Celkový počet inventarizačních ploch v panelu" },
                        { nameof(strRowsCountNone),                         "Není vybrán žádný panel." },
                        { nameof(strRowsCountOne),                          "Je vybrán $1 panel." },
                        { nameof(strRowsCountSome),                         "Jsou vybrány $1 panely." },
                        { nameof(strRowsCountMany),                         "Je vybráno $1 panelů." },
                    }
                    : languageFile.NationalVersion.Data.TryGetValue
                        (key: nameof(FormPanelRefYearSetGroup),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPanelRefYearSetGroup),                   "Groups of panels with reference year set" },
                        { nameof(btnClose),                                   "Close" },
                        { nameof(btnDelete),                                  "Delete" },
                        { nameof(btnUpdate),                                  "Write to database" },
                        { nameof(grpPanelRefYearSetGroups),                   "Groups of panels:" },
                        { nameof(grpSelectedPanelRefYearSetGroup),            "Selected group of panels:" },
                        { nameof(grpPanels),                                  "Panels:" },
                        { nameof(lblIdCaption),                               "Identifier:" },
                        { nameof(lblLabelCsCaption),                          "Label (national):" },
                        { nameof(lblDescriptionCsCaption),                    "Description (national):" },
                        { nameof(lblLabelEnCaption),                          "Label (English):" },
                        { nameof(lblDescriptionEnCaption),                    "Description (English):" },
                        { nameof(lstPanelRefYearSetGroups),                   "LabelEn" },
                        { nameof(msgNoneSelectedPanelRefYearSetGroup),        "There is no group of panels selected."},
                        { nameof(msgSelectedPanelRefYearSetGroupNotExists),   "Selected group of panels does not exist."},
                        { nameof(msgLabelCsIsEmpty),                          "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),                    "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),                          "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),                    "Description (English) cannot be empty." },
                        { nameof(msgLabelCsExists),                           "This label (national) already exists for another group of panels." },
                        { nameof(msgDescriptionCsExists),                     "This description (national) already exists for another group of panels." },
                        { nameof(msgLabelEnExists),                           "This label (English) already exists for another group of panels." },
                        { nameof(msgDescriptionEnExists),                     "This description (English) already exists for another group of panels." },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Name}",
                                "Sampling panel label" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Name}",
                                "Sampling panel label" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Name}",
                                "Reference year set label" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Name}",
                                "Reference year set label" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.Name}",
                                "Total number of clusters on panel" },
                        { $"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.Name}",
                                "Total number of plots on panel" },
                        { nameof(strRowsCountNone),                         "No panel is selected." },
                        { nameof(strRowsCountOne),                          "$1 panel is selected." },
                        { nameof(strRowsCountSome),                         "$1 panels are selected." },
                        { nameof(strRowsCountMany),                         "$1 panels are selected." },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormPanelRefYearSetGroup),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            ReadOnlyMode = false;

            DbChanged = false;

            onEdit = false;

            LoadContent();

            InitializeLabels();

            PanelRefYearSetGroupSelected();

            btnDelete.Click += new EventHandler(
                (sender, e) =>
                {
                    DeleteFromDb();
                });

            btnUpdate.Click += new EventHandler(
                (sender, e) =>
                {
                    UpdateDb();
                });

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            lstPanelRefYearSetGroups.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        PanelRefYearSetGroupSelected();
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormPanelRefYearSetGroup),
                out string frmPanelRefYearSetGroupText)
                    ? frmPanelRefYearSetGroupText
                    : String.Empty;

            btnClose.Text =
                labels.TryGetValue(key: nameof(btnClose),
                out string btnCloseText)
                    ? btnCloseText
                    : String.Empty;

            btnDelete.Text =
                labels.TryGetValue(key: nameof(btnDelete),
                out string btnDeleteText)
                    ? btnDeleteText
                    : String.Empty;

            btnUpdate.Text =
                labels.TryGetValue(key: nameof(btnUpdate),
                out string btnUpdateText)
                    ? btnUpdateText
                    : String.Empty;

            grpPanelRefYearSetGroups.Text =
                labels.TryGetValue(key: nameof(grpPanelRefYearSetGroups),
                out string grpPanelRefYearSetGroupsText)
                    ? grpPanelRefYearSetGroupsText
                    : String.Empty;

            grpSelectedPanelRefYearSetGroup.Text =
                labels.TryGetValue(key: nameof(grpSelectedPanelRefYearSetGroup),
                out string grpSelectedPanelRefYearSetGroupText)
                    ? grpSelectedPanelRefYearSetGroupText
                    : String.Empty;

            grpPanels.Text =
                labels.TryGetValue(key: nameof(grpPanels),
                out string grpPanelsText)
                    ? grpPanelsText
                    : String.Empty;

            lblIdCaption.Text =
                labels.TryGetValue(key: nameof(lblIdCaption),
                out string lblIdCaptionText)
                    ? lblIdCaptionText
                    : String.Empty;

            lblLabelCsCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCsCaption),
                out string lblLabelCsCaptionText)
                    ? lblLabelCsCaptionText
                    : String.Empty;

            lblDescriptionCsCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionCsCaption),
                out string lblDescriptionCsCaptionText)
                    ? lblDescriptionCsCaptionText
                    : String.Empty;

            lblLabelEnCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelEnCaption),
                out string lblLabelEnCaptionText)
                    ? lblLabelEnCaptionText
                    : String.Empty;

            lblDescriptionEnCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionEnCaption),
                out string lblDescriptionEnCaptionText)
                    ? lblDescriptionEnCaptionText
                    : String.Empty;

            onEdit = true;
            lstPanelRefYearSetGroups.DisplayMember =
                labels.TryGetValue(key: nameof(lstPanelRefYearSetGroups),
                out string lstPanelRefYearSetGroupsText)
                    ? lstPanelRefYearSetGroupsText
                    : String.Empty;
            onEdit = false;

            msgNoneSelectedPanelRefYearSetGroup =
                labels.TryGetValue(key: nameof(msgNoneSelectedPanelRefYearSetGroup),
                out msgNoneSelectedPanelRefYearSetGroup)
                    ? msgNoneSelectedPanelRefYearSetGroup
                    : String.Empty;

            msgSelectedPanelRefYearSetGroupNotExists =
                labels.TryGetValue(key: nameof(msgSelectedPanelRefYearSetGroupNotExists),
                out msgSelectedPanelRefYearSetGroupNotExists)
                    ? msgSelectedPanelRefYearSetGroupNotExists
                    : String.Empty;

            msgLabelCsIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                out msgLabelCsIsEmpty)
                    ? msgLabelCsIsEmpty
                    : String.Empty;

            msgDescriptionCsIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                out msgDescriptionCsIsEmpty)
                    ? msgDescriptionCsIsEmpty
                    : String.Empty;

            msgLabelEnIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                out msgLabelEnIsEmpty)
                    ? msgLabelEnIsEmpty
                    : String.Empty;

            msgDescriptionEnIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                out msgDescriptionEnIsEmpty)
                    ? msgDescriptionEnIsEmpty
                    : String.Empty;

            msgLabelCsExists =
               labels.TryGetValue(key: nameof(msgLabelCsExists),
               out msgLabelCsExists)
                   ? msgLabelCsExists
                   : String.Empty;

            msgDescriptionCsExists =
               labels.TryGetValue(key: nameof(msgDescriptionCsExists),
               out msgDescriptionCsExists)
                   ? msgDescriptionCsExists
                   : String.Empty;

            msgLabelEnExists =
               labels.TryGetValue(key: nameof(msgLabelEnExists),
               out msgLabelEnExists)
                   ? msgLabelEnExists
                   : String.Empty;

            msgDescriptionEnExists =
               labels.TryGetValue(key: nameof(msgDescriptionEnExists),
               out msgDescriptionEnExists)
                   ? msgDescriptionEnExists
                   : String.Empty;

            TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnHeader(
               columnName: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Name,
               headerTextCs: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Name}"],
               headerTextEn: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Name}"]
            );

            TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnHeader(
               columnName: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Name,
               headerTextCs: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Name}"],
               headerTextEn: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Name}"]
            );

            TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnHeader(
               columnName: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Name,
               headerTextCs: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Name}"],
               headerTextEn: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Name}"]
            );

            TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnHeader(
               columnName: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Name,
               headerTextCs: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Name}"],
               headerTextEn: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Name}"]
            );

            TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnHeader(
               columnName: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.Name,
               headerTextCs: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.Name}"],
               headerTextEn: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.Name}"]
            );

            TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnHeader(
               columnName: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.Name,
               headerTextCs: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.Name}"],
               headerTextEn: labels[$"col-{TFnApiGetPanelRefYearSetCombinationsForGroupsList.Name}.{TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.Name}"]
            );
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent()
        {
            onEdit = true;

            panelRefYearSetGroupList = NfiEstaFunctions.FnApiGetListOfPanelRefYearSetGroups.Execute(Database);

            lstPanelRefYearSetGroups.DataSource = panelRefYearSetGroupList.Items;
            onEdit = false;

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Vybrání skupiny panelů a roků měření</para>
        /// <para lang="en">Selecting the group of panels and reference year sets</para>
        /// </summary>
        private void PanelRefYearSetGroupSelected()
        {
            PanelRefYearSetGroup selectedPanelRefYearSetGroup =
                (lstPanelRefYearSetGroups.SelectedItem == null) ? null :
                (PanelRefYearSetGroup)lstPanelRefYearSetGroups.SelectedItem;

            if (selectedPanelRefYearSetGroup != null)
            {
                List<int?> selectedGroups = [selectedPanelRefYearSetGroup.Id];

                grpPanels.Controls.Clear();
                DataGridView dgvPanelRefYearSetCombinationsForGroup = new() { Dock = DockStyle.Fill };
                grpPanels.Controls.Add(dgvPanelRefYearSetCombinationsForGroup);

                TFnApiGetPanelRefYearSetCombinationsForGroupsList listOfPanelRefYearSetCombinationsForGroups = NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups.Execute(Database, selectedGroups);
                TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnsVisibility();

                TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColId.Visible = false;
                TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelId.Visible = false;
                TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionCs.Visible = false;
                TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionEn.Visible = false;
                TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetId.Visible = false;
                TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionCs.Visible = false;
                TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionEn.Visible = false;

                switch (LanguageVersion)
                {
                    case LanguageVersion.National:
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Visible = true;
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Visible = false;
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Visible = true;
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Visible = false;
                        break;

                    case LanguageVersion.International:
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Visible = false;
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Visible = true;
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Visible = false;
                        TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Visible = true;
                        break;
                }

                dgvPanelRefYearSetCombinationsForGroup.DataSource = listOfPanelRefYearSetCombinationsForGroups.Data;
                listOfPanelRefYearSetCombinationsForGroups.FormatDataGridView(dgvPanelRefYearSetCombinationsForGroup);

                lblIdValue.Text = selectedPanelRefYearSetGroup.Id.ToString();
                txtLabelCsValue.Text = selectedPanelRefYearSetGroup.LabelCs;
                txtDescriptionCsValue.Text = selectedPanelRefYearSetGroup.DescriptionCs;
                txtLabelEnValue.Text = selectedPanelRefYearSetGroup.LabelEn;
                txtDescriptionEnValue.Text = selectedPanelRefYearSetGroup.DescriptionEn;

                SetLabelRowsCountValue(count: dgvPanelRefYearSetCombinationsForGroup.Rows.Count);
            }

            EnableButtonDelete();

            EnableButtonUpdate();
        }

        /// <summary>
        /// <para lang="cs">Nastavení popisku s počtem vybraných řádků</para>
        /// <para lang="en">Setting the label with the number of selected rows</para>
        /// </summary>
        /// <param name="count">
        /// <para lang="cs">Počet vybraných řádků</para>
        /// <para lang="en">Number of selected rows</para>
        /// </param>
        private void SetLabelRowsCountValue(int count)
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            switch (count)
            {
                case 0:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountNone),
                            out strRowsCountNone)
                                ? strRowsCountNone
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 1:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountOne),
                            out strRowsCountOne)
                                ? strRowsCountOne
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 2:
                case 3:
                case 4:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountSome),
                            out strRowsCountSome)
                                ? strRowsCountSome
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                default:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountMany),
                            out strRowsCountMany)
                                ? strRowsCountMany
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Zpřístupnění tlačítka Delete</para>
        /// <para lang="en">Enable button Delete</para>
        /// </summary>
        private void EnableButtonDelete()
        {
            // Vybraná skupina panelů
            // Selected group of panels
            PanelRefYearSetGroup selectedPanelRefYearSetGroup =
                (lstPanelRefYearSetGroups.SelectedItem == null) ? null :
                (PanelRefYearSetGroup)lstPanelRefYearSetGroups.SelectedItem;

            if (selectedPanelRefYearSetGroup == null)
            {
                // Žádná skupina není vybraná
                // No group is selected
                btnDelete.Enabled = false;
                return;
            }

            if (!panelRefYearSetGroupList.Items
                .Where(a => a.Id == selectedPanelRefYearSetGroup.Id)
                .Any())
            {
                // Vybraná skupina není v databázové tabulce (nemůže nastat)
                // Selected group is not in the database table (this cannot happen)
                btnDelete.Enabled = false;
                return;
            }

            TFnApiBeforeDeletePanelRefYearSetGroupList result = NfiEstaFunctions.FnApiBeforeDeletePanelRefYearSetGroup.Execute(Database, selectedPanelRefYearSetGroup.Id);

            bool canDelete = !result.Items.Any(item => item.AuxConfExist == true || item.TotalEstimateConfExist == true);
            btnDelete.Enabled = canDelete;
        }

        /// <summary>
        /// <para lang="cs">Zpřístupnění tlačítka Update</para>
        /// <para lang="en">Enable button Update</para>
        /// </summary>
        private void EnableButtonUpdate()
        {
            // Vybraná skupina panelů
            // Selected group of panels
            PanelRefYearSetGroup selectedPanelRefYearSetGroup =
                (lstPanelRefYearSetGroups.SelectedItem == null) ? null :
                (PanelRefYearSetGroup)lstPanelRefYearSetGroups.SelectedItem;

            if (selectedPanelRefYearSetGroup == null)
            {
                // Žádná skupina není vybraná
                // No group is selected
                btnUpdate.Enabled = false;
                return;
            }

            if (!panelRefYearSetGroupList.Items
                .Where(a => a.Id == selectedPanelRefYearSetGroup.Id)
                .Any())
            {
                // Vybraná skupina není v databázové tabulce (nemůže nastat)
                // Selected group is not in the database table (this cannot happen)
                btnUpdate.Enabled = false;
                return;
            }

            btnUpdate.Enabled = true;
        }

        /// <summary>
        /// <para lang="cs">Smazání skupiny panelů a roků měření z databáze</para>
        /// <para lang="en">Delete group of panels and reference year sets from database</para>
        /// </summary>
        private void DeleteFromDb()
        {
            // Vybraná skupina panelů
            // Selected group of panels
            PanelRefYearSetGroup selectedPanelRefYearSetGroup =
                (lstPanelRefYearSetGroups.SelectedItem == null) ? null :
                (PanelRefYearSetGroup)lstPanelRefYearSetGroups.SelectedItem;

            if (selectedPanelRefYearSetGroup == null)
            {
                // Žádná skupina není vybraná
                // No group is selected
                MessageBox.Show(
                    text: msgNoneSelectedPanelRefYearSetGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (!panelRefYearSetGroupList.Items
                .Where(a => a.Id == selectedPanelRefYearSetGroup.Id)
                .Any())
            {
                // Vybraná skupina není v databázové tabulce (nemůže nastat)
                // Selected group is not in the database table (this cannot happen)
                MessageBox.Show(
                    text: msgSelectedPanelRefYearSetGroupNotExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            NfiEstaFunctions.FnApiDeletePanelRefYearSetGroup.Execute(Database, selectedPanelRefYearSetGroup.Id);

            DbChanged = true;

            LoadContent();

            // Vybere první položku v seznamu
            // Selects the first item in the list
            if (lstPanelRefYearSetGroups.Items.Count > 0)
            {
                lstPanelRefYearSetGroups.SelectedIndex = 0;
                PanelRefYearSetGroupSelected();
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis změn do databáze</para>
        /// <para lang="en">Writing changes to the database</para>
        /// </summary>
        private void UpdateDb()
        {
            // Vybraná skupina panelů
            // Selected group of panels
            PanelRefYearSetGroup selectedPanelRefYearSetGroup =
                (lstPanelRefYearSetGroups.SelectedItem == null) ? null :
                (PanelRefYearSetGroup)lstPanelRefYearSetGroups.SelectedItem;

            string labelCs = txtLabelCsValue.Text.Trim();
            string descriptionCs = txtDescriptionCsValue.Text.Trim();
            string labelEn = txtLabelEnValue.Text.Trim();
            string descriptionEn = txtDescriptionEnValue.Text.Trim();

            if (selectedPanelRefYearSetGroup == null)
            {
                // Žádná skupina není vybraná
                // No group is selected
                MessageBox.Show(
                    text: msgNoneSelectedPanelRefYearSetGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (!panelRefYearSetGroupList.Items
                .Where(a => a.Id == selectedPanelRefYearSetGroup.Id)
                .Any())
            {
                // Vybraná skupina není v databázové tabulce (nemůže nastat)
                // Selected group is not in the database table (this cannot happen)
                MessageBox.Show(
                    text: msgNoneSelectedPanelRefYearSetGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (String.IsNullOrEmpty(value: labelCs))
            {
                // LabelCs - prázdné
                // LabelCs - empty
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                // DescriptionCs - prázdné
                // DescriptionCs - empty
                MessageBox.Show(
                    text: msgDescriptionCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                // LabelEn - prázdné
                // LabelEn - empty
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                // DescriptionEn - prázdné
                // DescriptionEn - empty
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if ((selectedPanelRefYearSetGroup.LabelCs != labelCs) &&
                 panelRefYearSetGroupList.Items
                    .Where(a => a.LabelCs == labelCs)
                    .Any())
            {
                // LabelCs - duplicitní
                // LabelCs - duplicate
                MessageBox.Show(
                    text: msgLabelCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if ((selectedPanelRefYearSetGroup.DescriptionCs != descriptionCs) &&
                 panelRefYearSetGroupList.Items
                    .Where(a => a.DescriptionCs == descriptionCs)
                    .Any())
            {
                // DescriptionCs - duplicitní
                // DescriptionCs - duplicate
                MessageBox.Show(
                    text: msgDescriptionCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if ((selectedPanelRefYearSetGroup.LabelEn != labelEn) &&
                panelRefYearSetGroupList.Items
                    .Where(a => a.LabelEn == labelEn)
                    .Any())
            {
                // LabelEn - duplicitní
                // LabelEn - duplicate
                MessageBox.Show(
                    text: msgLabelEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if ((selectedPanelRefYearSetGroup.DescriptionEn != descriptionEn) &&
                 panelRefYearSetGroupList.Items
                    .Where(a => a.DescriptionEn == descriptionEn)
                    .Any())
            {
                // DescriptionEn - duplicitní
                // DescriptionEn - duplicate
                MessageBox.Show(
                    text: msgDescriptionEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            selectedPanelRefYearSetGroup.LabelCs = labelCs;
            selectedPanelRefYearSetGroup.DescriptionCs = descriptionCs;
            selectedPanelRefYearSetGroup.LabelEn = labelEn;
            selectedPanelRefYearSetGroup.DescriptionEn = descriptionEn;

            NfiEstaFunctions.FnApiUpdatePanelRefYearSetGroup.Execute(
                Database,
                selectedPanelRefYearSetGroup.Id,
                labelCs,
                descriptionCs,
                labelEn,
                descriptionEn);

            DbChanged = true;

            LoadContent();

            // Výběr editované položky, po aktualizaci databáze
            // Selection of the edited item after database update
            if (panelRefYearSetGroupList.Items
                    .Where(a => a.Id == selectedPanelRefYearSetGroup.Id)
                    .Any())
            {
                lstPanelRefYearSetGroups.SelectedItem =
                    panelRefYearSetGroupList.Items
                    .Where(a => a.Id == selectedPanelRefYearSetGroup.Id)
                    .First();
            }
        }

        #endregion Methods
    }

}