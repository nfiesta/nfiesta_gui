﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení atributové kategorie - třída pro Designer</para>
    /// <para lang="en">Control for display attribute category - class for Designer</para>
    /// </summary>
    internal partial class ControlCategoryItemDesign
        : UserControl
    {
        protected ControlCategoryItemDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Label LblName => lblName;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení atributové kategorie</para>
    /// <para lang="en">Control for display attribute category</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal class ControlCategoryItem<TCategory>
            : ControlCategoryItemDesign, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Položka seznamu atributových kategorií</para>
        /// <para lang="en">Item of the attribute categories list</para>
        /// </summary>
        private TCategory item;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="item">
        /// <para lang="cs">Položka seznamu atributových kategorií</para>
        /// <para lang="en">Item of the attribute categories list</para>
        /// </param>
        public ControlCategoryItem(
            Control controlOwner,
            TCategory item) : base()
        {
            Initialize(
                controlOwner: controlOwner,
                item: item);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ atributové kategorie (read-only)</para>
        /// <para lang="en">Attribute category type (read-only)</para>
        /// </summary>
        public static CategoryType CategoryType
        {
            get
            {
                return typeof(TCategory).FullName switch
                {
                    "ZaJi.NfiEstaPg.Core.AreaDomainCategory"
                        => CategoryType.AreaDomainCategory,
                    "ZaJi.NfiEstaPg.Core.SubPopulationCategory"
                        => CategoryType.SubPopulationCategory,
                    "ZaJi.NfiEstaPg.VariablePair"
                        => CategoryType.VariablePair,
                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(Item)} must be type ",
                                $"of {nameof(AreaDomainCategory)} ",
                                $"or {nameof(SubPopulationCategory)} ",
                                $"or {nameof(VariablePair)} "),
                            paramName: nameof(Item)),
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Položka seznamu atributových kategorií (read-only)</para>
        /// <para lang="en">Item of the attribute categories list (read-only)</para>
        /// </summary>
        public TCategory Item
        {
            get
            {
                if (item == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Item)} must not be null.",
                        paramName: nameof(Item));
                }
                return item;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Item)} must not be null.",
                        paramName: nameof(Item));
                }
                item = value;
                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Kategorie plošné domény (read-only)</para>
        /// <para lang="en">Area domain category (read-only)</para>
        /// </summary>
        public AreaDomainCategory AreaDomainCategory
        {
            get
            {
                return
                    (CategoryType == CategoryType.AreaDomainCategory) ?
                    (AreaDomainCategory)Convert.ChangeType(
                                value: Item,
                                conversionType: typeof(AreaDomainCategory)) : null;
            }
        }

        /// <summary>
        /// <para lang="cs">Kategorie subpopulace (read-only)</para>
        /// <para lang="en">Subpopulation category (read-only)</para>
        /// </summary>
        public SubPopulationCategory SubPopulationCategory
        {
            get
            {
                return
                    (CategoryType == CategoryType.SubPopulationCategory) ?
                    (SubPopulationCategory)Convert.ChangeType(
                                value: Item,
                                conversionType: typeof(SubPopulationCategory)) : null;
            }
        }

        /// <summary>
        /// <para lang="cs">Atributová kategorie (read-only)</para>
        /// <para lang="en">Attribute category (read-only)</para>
        /// </summary>
        public VariablePair AttributeCategory
        {
            get
            {
                return
                    (CategoryType == CategoryType.VariablePair) ?
                    (VariablePair)Convert.ChangeType(
                                value: Item,
                                conversionType: typeof(VariablePair)) : null;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => CategoryType switch
                {
                    CategoryType.AreaDomainCategory => (languageFile == null)
                        ? []
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryItem<AreaDomainCategory>)}{nameof(AreaDomainCategory)}",
                            out Dictionary<string, string> dictNationalAreaDomainCategory)
                                ? dictNationalAreaDomainCategory
                                : [],

                    CategoryType.SubPopulationCategory => (languageFile == null)
                        ? []
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryItem<SubPopulationCategory>)}{nameof(SubPopulationCategory)}",
                            out Dictionary<string, string> dictNationalSubPopulationCategory)
                                ? dictNationalSubPopulationCategory
                                : [],

                    CategoryType.VariablePair => (languageFile == null)
                        ? []
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryItem<VariablePair>)}{nameof(VariablePair)}",
                            out Dictionary<string, string> dictNationalVariablePair)
                                ? dictNationalVariablePair
                                : [],

                    _ => [],
                },

                LanguageVersion.International => CategoryType switch
                {
                    CategoryType.AreaDomainCategory => (languageFile == null)
                        ? []
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryItem<AreaDomainCategory>)}{nameof(AreaDomainCategory)}",
                            out Dictionary<string, string> dictInternationalAreaDomainCategory)
                                ? dictInternationalAreaDomainCategory
                                : [],

                    CategoryType.SubPopulationCategory => (languageFile == null)
                        ? []
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryItem<SubPopulationCategory>)}{nameof(SubPopulationCategory)}",
                            out Dictionary<string, string> dictInternationalSubPopulationCategory)
                                ? dictInternationalSubPopulationCategory
                                : [],

                    CategoryType.VariablePair => (languageFile == null)
                        ? []
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryItem<VariablePair>)}{nameof(VariablePair)}",
                            out Dictionary<string, string> dictInternationalVariablePair)
                                ? dictInternationalVariablePair
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="item">
        /// <para lang="cs">Položka seznamu atributových kategorií</para>
        /// <para lang="en">Item of the attribute categories list</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            TCategory item)
        {
            ControlOwner = controlOwner;
            Item = item;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    switch (CategoryType)
                    {
                        case CategoryType.AreaDomainCategory:
                            LblName.Text = AreaDomainCategory.LabelCs;
                            return;
                        case CategoryType.SubPopulationCategory:
                            LblName.Text = SubPopulationCategory.LabelCs;
                            return;
                        case CategoryType.VariablePair:
                            LblName.Text = AttributeCategory.LabelCs;
                            return;
                        default:
                            LblName.Text = String.Empty;
                            return;
                    }

                case LanguageVersion.International:
                    switch (CategoryType)
                    {
                        case CategoryType.AreaDomainCategory:
                            LblName.Text = AreaDomainCategory.LabelEn;
                            return;
                        case CategoryType.SubPopulationCategory:
                            LblName.Text = SubPopulationCategory.LabelEn;
                            return;
                        case CategoryType.VariablePair:
                            LblName.Text = AttributeCategory.LabelEn;
                            return;
                        default:
                            LblName.Text = String.Empty;
                            return;
                    }

                default:
                    LblName.Text = String.Empty;
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion

    }

    #region enum CategoryType

    /// <summary>
    /// <para lang="cs">Typ atributové kategorie</para>
    /// <para lang="en">Attribute category type</para>
    /// </summary>
    internal enum CategoryType
    {
        /// <summary>
        /// <para lang="cs">Kategorie plošné domény</para>
        /// <para lang="en">Area domain category</para>
        /// </summary>
        AreaDomainCategory = 100,

        /// <summary>
        /// <para lang="cs">Kategorie subpopulace</para>
        /// <para lang="en">Subpopulation category</para>
        /// </summary>
        SubPopulationCategory = 200,

        /// <summary>
        /// <para lang="cs">Atributová kategorie</para>
        /// <para lang="en">Attribute category</para>
        /// </summary>
        VariablePair = 300,

        /// <summary>
        /// <para lang="cs">Neznámá kategorie</para>
        /// <para lang="en">Unknown category</para>
        /// </summary>
        Unknown = 0
    }

    #endregion enum CategoryType

}