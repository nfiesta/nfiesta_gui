﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Linq;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Pracovní vlákno pro konfiguraci jednofázových odhadů úhrnů</para>
        /// <para lang="en">Worker thread for configuration one-phase estimates of totals</para>
        /// </summary>
        internal class FnApiSaveSinglePhaseTotalConfigWorker
            : WorkerThread<FnApiSaveSinglePhaseTotalConfigTask, OLAPTotalEstimate>
        {

            #region Methods

            /// <summary>
            /// <para lang="cs">Spuštění konfigurace odhadů</para>
            /// <para lang="en">Starting the configuration of estimates</para>
            /// </summary>
            public override object Execute(ref ThreadEventArgs e)
            {
                if (ThreadConnection == null) { return null; }
                if (Task == null) { return null; }

                Task.SQL = NfiEstaFunctions.FnApiSaveSinglePhaseTotalConfig
                    .GetCommandText(
                        estimationPeriodId: Task.Element?.EstimationPeriodId,
                        estimationCellId: Task.Element?.EstimationCellId,
                        variableId: Task.Element?.VariableId,
                        aggregatePanelRefYearSetGroup:
                            Task.AggregatedEstimationCellGroup?.GlobalPanelReferenceYearSetGroup?.Id,
                        panelRefYearSetGroupsByStrata:
                            Task.AggregatedEstimationCellGroup?.PartialPanelReferenceYearSetGroups?
                                .Select(a => (Nullable<int>)a?.PanelRefYearSetGroup?.Id)
                                .ToList<Nullable<int>>());

                Task.Result = NfiEstaFunctions.FnApiSaveSinglePhaseTotalConfig
                    .Execute(
                        connection: ThreadConnection,
                        estimationPeriodId: Task.Element?.EstimationPeriodId,
                        estimationCellId: Task.Element?.EstimationCellId,
                        variableId: Task.Element?.VariableId,
                        aggregatePanelRefYearSetGroup:
                            Task.AggregatedEstimationCellGroup?.GlobalPanelReferenceYearSetGroup?.Id,
                        panelRefYearSetGroupsByStrata:
                            Task.AggregatedEstimationCellGroup?.PartialPanelReferenceYearSetGroups?
                                .Select(a => (Nullable<int>)a?.PanelRefYearSetGroup?.Id)
                                .ToList<Nullable<int>>());

                return Task.Result;
            }

            #endregion Methods

        }

    }
}