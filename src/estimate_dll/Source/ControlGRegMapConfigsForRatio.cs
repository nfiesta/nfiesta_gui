﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">
    /// Ovládací prvek:
    /// Kombinace skupin panelů a referenčních období, typu parametrizační oblasti
    /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů
    /// </para>
    /// <para lang="en">
    /// Control:
    /// Combination of panel-refyearset group, param. area type,
    /// force synthetic, model and sigma of already configured GREG-map totals
    /// </para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class ControlGRegMapConfigsForRatio
        : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Identifikátor</para>
        /// <para lang="en">Identifier</para>
        /// </summary>
        private int id;

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">
        /// Kombinace skupin panelů a referenčních období, typu parametrizační oblasti,
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů.
        /// (návratový typ uložené procedury fn_api_get_gregmap_configs4ratio)
        /// </para>
        /// <para lang="en">
        /// Combination of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals.
        /// (return type of the stored procedure fn_api_get_gregmap_configs4ratio)
        /// </para>
        /// </summary>
        private TFnApiGetGRegMapConfigsForRatio combination;

        private string strTrue = "true";
        private string strFalse = "false";
        private string strNull = "null";

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">
        /// Událost výběru kombinace skupin panelů a referenčních období, typu parametrizační oblasti
        /// syntetického odhadu, modelu a sigma</para>
        /// <para lang="en">
        /// Combination of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma selection event</para>
        /// </summary>
        public event EventHandler OnSelect;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlGRegMapConfigsForRatio(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Identifikátor</para>
        /// <para lang="en">Identifier</para>
        /// </summary>
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Kombinace skupin panelů a referenčních období, typu parametrizační oblasti,
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů.
        /// (návratový typ uložené procedury fn_api_get_gregmap_configs4ratio)
        /// </para>
        /// <para lang="en">
        /// Combination of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals.
        /// (return type of the stored procedure fn_api_get_gregmap_configs4ratio)
        /// </para>
        /// </summary>
        public TFnApiGetGRegMapConfigsForRatio Combination
        {
            get
            {
                return combination;
            }
            set
            {
                combination = value;

                Checked = false;
                Enabled = Combination?.AllEstimatesConfigurable ?? false;

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Výběr kombinace skupin panelů a referenčních období,
        /// typu parametrizační oblasti syntetického odhadu, modelu a sigma
        /// </para>
        /// <para lang="en">
        /// Select combination of panel-refyearset group,
        /// param. area type, force synthetic, model and sigma"
        /// </para>
        /// </summary>
        public bool Checked
        {
            get
            {
                return rdoSelected.Checked;
            }
            set
            {
                rdoSelected.Checked = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(lblAllEstimatesConfigurableCaption),   "Všechny odhady konfigurovatelné:" },
                        { nameof(lblPanelRefYearSetGroupCaption),       "Skupina panelů a období měření:" },
                        { nameof(lblForceSyntheticCaption),             "Syntetický odhad:" },
                        { nameof(lblParamAreaTypeCaption),              "Typ parametrizační oblasti:" },
                        { nameof(lblNumeratorModelCaption),             "Model pro čitatel:" },
                        { nameof(lblNumeratorModelSigmaCaption),        "Sigma modelu pro čitatel:" },
                        { nameof(lblDenominatorModelCaption),           "Model pro jmenovatel:" },
                        { nameof(lblDenominatorModelSigmaCaption),      "Sigma modelu pro jmenovatel:" },
                        { nameof(strTrue),                              "ano" },
                        { nameof(strFalse),                             "ne" },
                        { nameof(strNull),                              "null" },
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlGRegMapConfigsForRatio),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(lblAllEstimatesConfigurableCaption),   "All estimates configurable:" },
                        { nameof(lblPanelRefYearSetGroupCaption),       "Panel reference year set group:" },
                        { nameof(lblForceSyntheticCaption),             "Force synthetic:" },
                        { nameof(lblParamAreaTypeCaption),              "Parametrization area type:" },
                        { nameof(lblNumeratorModelCaption),             "Numerator model:" },
                        { nameof(lblNumeratorModelSigmaCaption),        "Numerator model sigma:" },
                        { nameof(lblDenominatorModelCaption),           "Denominator model:" },
                        { nameof(lblDenominatorModelSigmaCaption),      "Denominator model sigma:" },
                        { nameof(strTrue),                              "yes" },
                        { nameof(strFalse),                             "no" },
                        { nameof(strNull),                              "null" },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlGRegMapConfigsForRatio),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Id = 0;
            Combination = null;

            rdoSelected.CheckedChanged += new EventHandler(
                (sender, e) =>
                {
                    if (rdoSelected.Checked)
                    {
                        OnSelect?.Invoke(sender: this, e: e);
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            strTrue =
                labels.TryGetValue(key: nameof(strTrue),
                out string strTrueText)
                    ? strTrueText
                    : "true";

            strFalse =
                labels.TryGetValue(key: nameof(strFalse),
                out string strFalseText)
                    ? strFalseText
                    : "false";

            strNull =
                labels.TryGetValue(key: nameof(strNull),
                out string strNullText)
                    ? strNullText
                    : "null";

            // AllEstimatesConfigurable
            lblAllEstimatesConfigurableCaption.Text =
                labels.TryGetValue(key: nameof(lblAllEstimatesConfigurableCaption),
                out string lblAllEstimatesConfigurableCaptionText)
                    ? lblAllEstimatesConfigurableCaptionText
                    : String.Empty;

            lblAllEstimatesConfigurableValue.Text =
                (Combination?.AllEstimatesConfigurable == null)
                    ? strNull
                    : (bool)Combination.AllEstimatesConfigurable
                        ? strTrue
                        : strFalse;

            // PanelRefYearSetGroup
            lblPanelRefYearSetGroupCaption.Text =
                labels.TryGetValue(key: nameof(lblPanelRefYearSetGroupCaption),
                out string lblPanelRefYearSetGroupCaptionText)
                    ? lblPanelRefYearSetGroupCaptionText
                    : String.Empty;

            lblPanelRefYearSetGroupValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? String.IsNullOrEmpty(Combination?.PanelRefYearSetGroupLabelEn)
                        ? strNull
                        : Combination.PanelRefYearSetGroupLabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? String.IsNullOrEmpty(Combination?.PanelRefYearSetGroupLabelCs)
                            ? strNull
                            : Combination.PanelRefYearSetGroupLabelCs
                        : String.IsNullOrEmpty(Combination?.PanelRefYearSetGroupLabelEn)
                            ? strNull
                            : Combination.PanelRefYearSetGroupLabelEn;

            // ForceSynthetic
            lblForceSyntheticCaption.Text =
                labels.TryGetValue(key: nameof(lblForceSyntheticCaption),
                out string lblForceSyntheticCaptionText)
                    ? lblForceSyntheticCaptionText
                    : String.Empty;

            lblForceSyntheticValue.Text =
                (Combination?.ForceSynthetic == null)
                    ? strNull
                    : (bool)Combination.ForceSynthetic
                        ? strTrue
                        : strFalse;

            // ParamAreaType
            lblParamAreaTypeCaption.Text =
                labels.TryGetValue(key: nameof(lblParamAreaTypeCaption),
                out string lblParamAreaTypeCaptionText)
                    ? lblParamAreaTypeCaptionText
                    : String.Empty;

            lblParamAreaTypeValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? String.IsNullOrEmpty(Combination?.ParamAreaTypeLabelEn)
                        ? strNull
                        : Combination.ParamAreaTypeLabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? String.IsNullOrEmpty(Combination?.ParamAreaTypeLabelCs)
                            ? strNull
                            : Combination.ParamAreaTypeLabelCs
                        : String.IsNullOrEmpty(Combination?.ParamAreaTypeLabelEn)
                            ? strNull
                            : Combination.ParamAreaTypeLabelEn;

            // NumeratorModel
            lblNumeratorModelCaption.Text =
                labels.TryGetValue(key: nameof(lblNumeratorModelCaption),
                out string lblNumeratorModelCaptionText)
                    ? lblNumeratorModelCaptionText
                    : String.Empty;

            lblNumeratorModelValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? String.IsNullOrEmpty(Combination?.NumeratorModelLabelEn)
                        ? strNull
                        : Combination.NumeratorModelLabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? String.IsNullOrEmpty(Combination?.NumeratorModelLabelCs)
                            ? strNull
                            : Combination.NumeratorModelLabelCs
                        : String.IsNullOrEmpty(Combination?.NumeratorModelLabelEn)
                            ? strNull
                            : Combination.NumeratorModelLabelEn;

            // NumeratorModelSigma
            lblNumeratorModelSigmaCaption.Text =
                labels.TryGetValue(key: nameof(lblNumeratorModelSigmaCaption),
                out string lblNumeratorModelSigmaCaptionText)
                    ? lblNumeratorModelSigmaCaptionText
                    : String.Empty;

            lblNumeratorModelSigmaValue.Text =
                (Combination?.NumeratorModelSigma == null)
                    ? strNull
                    : (bool)Combination.NumeratorModelSigma
                        ? strTrue
                        : strFalse;

            // DenominatorModel
            lblDenominatorModelCaption.Text =
               labels.TryGetValue(key: nameof(lblDenominatorModelCaption),
               out string lblDenominatorModelCaptionText)
                   ? lblDenominatorModelCaptionText
                   : String.Empty;

            lblDenominatorModelValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? String.IsNullOrEmpty(Combination?.DenominatorModelLabelEn)
                        ? strNull
                        : Combination.DenominatorModelLabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? String.IsNullOrEmpty(Combination?.DenominatorModelLabelCs)
                            ? strNull
                            : Combination.DenominatorModelLabelCs
                        : String.IsNullOrEmpty(Combination?.DenominatorModelLabelEn)
                            ? strNull
                            : Combination.DenominatorModelLabelEn;

            // DenominatorModelSigma
            lblDenominatorModelSigmaCaption.Text =
                labels.TryGetValue(key: nameof(lblDenominatorModelSigmaCaption),
                out string lblDenominatorModelSigmaCaptionText)
                    ? lblDenominatorModelSigmaCaptionText
                    : String.Empty;

            lblDenominatorModelSigmaValue.Text =
                (Combination?.DenominatorModelSigma == null)
                    ? strNull
                    : (bool)Combination.DenominatorModelSigma
                        ? strTrue
                        : strFalse;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}