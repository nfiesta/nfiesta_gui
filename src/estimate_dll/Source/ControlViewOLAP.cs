﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

// #define TEST

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Zobrazení výsledků odhadů"</para>
    /// <para lang="en">Control "Display estimate results"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlViewOLAP
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Barva textu pro zvýraznění neaditivního odhadu</para>
        /// <para lang="en">Text color to highlight a non-additive estimate</para>
        /// </summary>
        private static readonly Color NonAdditiveEstimateColor = Color.Red;

        /// <summary>
        /// <para lang="cs">Barva textu pro zvýraznění attribotově neaditivního odhadu</para>
        /// <para lang="en">Text color to highlight non-additive estimates in attribute categories</para>
        /// </summary>
        private static readonly Color NonAttrAdditiveEstimateColor = Color.IndianRed;

        /// <summary>
        /// <para lang="cs">Barva textu pro zvýraznění geograficky neaditivního odhadu</para>
        /// <para lang="en">Text color to highlight geographically non-additive estimates</para>
        /// </summary>
        private static readonly Color NonGeoAdditiveEstimateColor = Color.DarkOrange;

        /// <summary>
        /// <para lang="cs">Barva textu pro standardní odhad</para>
        /// <para lang="en">Text color to highlight standard estimates</para>
        /// </summary>
        private static readonly Color RegularEstimateRowColor = Color.Black;

        /// <summary>
        /// <para lang="cs">Barva textu pro historický odhad</para>
        /// <para lang="en">Text color to highlight historical estimates</para>
        /// </summary>
        private static readonly Color HistoryEstimateRowColor = Color.DarkGray;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Odhady úhrnu</para>
        /// <para lang="en">Total estimates</para>
        /// </summary>
        private OLAPTotalEstimateList dataSourceTotal;

        /// <summary>
        /// <para lang="cs">Odhady podílu</para>
        /// <para lang="en">Ratio estimates</para>
        /// </summary>
        private OLAPRatioEstimateList dataSourceRatio;

        /// <summary>
        /// <para lang="cs">Podmínka pro výběr řádků v DataGridView</para>
        /// <para lang="en">Condition for rows selection in DataGridView</para>
        /// </summary>
        private FilterRow rowFilter;

        /// <summary>
        /// <para lang="cs">Je zobrazena historie odhadů?</para>
        /// <para lang="en">Is history of the estimates displayed?</para>
        /// </summary>
        private bool historyDisplayed;

        /// <summary>
        /// <para lang="cs">Řídící vlákno</para>
        /// <para lang="en">Control thread</para>
        /// </summary>
        private IControlThread controlThread;

        private string strDisplayHistory = String.Empty;
        private string strHideHistory = String.Empty;
        private string strRowsCountNone = String.Empty;
        private string strRowsCountOne = String.Empty;
        private string strRowsCountSome = String.Empty;
        private string strRowsCountMany = String.Empty;
        private string strConfigurationComplete = String.Empty;
        private string strCalculationComplete = String.Empty;
        private string msgNewConfigurationExists = String.Empty;
        private string msgNewConfigurationIsNotOnePhase = String.Empty;
        private string msgNewConfigurationIsNotRegPhase = String.Empty;
        private string msgNewConfigurationIsNotMixedPhase = String.Empty;
        private string msgNewConfigurationIsNotSingleTypePhase = String.Empty;
        private string msgNewConfigurationIsNotOnePeriod = String.Empty;
        private string msgNewConfigurationIsNotOnePeriodGREG = String.Empty;
        private string msgNewConfigurationNoneCommonPanels = String.Empty;
        private string msgNewConfigurationNoneSelected = String.Empty;
        private string msgNewConfigurationIsNotOneEstCellCollection = String.Empty;
        private string msgIncompleteGroupForEachStratum = String.Empty;
        private string msgNoSuitableModelSigmaParam = String.Empty;
        private string msgNotExistMatchingGRegMapTotals = String.Empty;
        private string msgNotExistMatchingGRegMapToSingleTotals = String.Empty;
        private string msgNotExistMatchingSingleToGRegMapTotals = String.Empty;
        private string msgCalcEstimateWithConfiguration = String.Empty;
        private string msgCalcEstimateNotOnePhase = String.Empty;
        private string msgCalcEstimateNoneSelected = String.Empty;
        private string msgNoSelectedPanelRefYearSetGroup = String.Empty;
        private string msgNoEstimationPeriod = String.Empty;
        private string msgFnSystemUtilizationIsNull = String.Empty;
        private string msgFnSystemUtilizationLoadMinIsNull = String.Empty;
        private string msgFnSystemUtilizationCPUCountIsNull = String.Empty;
        private string msgFnSystemUtilizationHighLoad = String.Empty;
        private string msgCheckSystemResourcesAvailabilityForConfiguration = String.Empty;
        private string msgCheckSystemResourcesAvailabilityForCalculation = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">DataGridView pro zobrazení odhadů úhrnu nebo podílu</para>
        /// <para lang="en">DataGridView for display total or ratio estimates</para>
        /// </summary>
        private DataGridView dgvResults;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna vybraného odhadu"</para>
        /// <para lang="en">Event
        /// "Selected estimate changed"</para>
        /// </summary>
        public event EventHandler SelectedEstimateChanged;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna filtru řádků"</para>
        /// <para lang="en">Event
        /// "Row filter changed"</para>
        /// </summary>
        public event EventHandler RowFilterChanged;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Dokončení konfigurace odhadů"</para>
        /// <para lang="en">Event
        /// "Completing the configuration of estimates"</para>
        /// </summary>
        public event EventHandler EstimatesConfigured;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Dokončení výpočtu odhadů"</para>
        /// <para lang="en">Event
        /// "Completing the calculation of estimates"</para>
        /// </summary>
        public event EventHandler EstimatesCalculated;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Zobrazit historii odhadu"</para>
        /// <para lang="en">Event
        /// "Display estimate history"</para>
        /// </summary>
        public event EventHandler DisplayHistoryClick;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Zobrazit panely"</para>
        /// <para lang="en">Event
        /// "Display panels"</para>
        /// </summary>
        public event EventHandler DisplayPanelsClick;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna v seznamu výpočetních období"</para>
        /// <para lang="en">Event
        /// "List of estimation periods changed"</para>
        /// </summary>
        public event EventHandler EstimationPeriodChanged;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna v seznamu panelů"</para>
        /// <para lang="en">Event
        /// "List of panels changed"</para>
        /// </summary>
        public event EventHandler PanelReferenceYearSetGroupChanged;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna v seznamu výpočetních buněk"</para>
        /// <para lang="en">Event
        /// "List of estimation cells changed"</para>
        /// </summary>
        public event EventHandler EstimationCellCollectionChanged;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Zavřít zobrazení OLAP"</para>
        /// <para lang="en">Event
        /// "Close the display of OLAP"</para>
        /// </summary>
        public event EventHandler Closed;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlViewOLAP(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlEstimateResult)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimateResult)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlEstimateResult)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimateResult)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro konfiguraci a výpočet odhadů" (read-only)</para>
        /// <para lang="en">Control "Module for configuration and calculation estimates" (read-only)</para>
        /// </summary>
        public ControlEstimate CtrEstimate
        {
            get
            {
                return ((ControlEstimate)
                    ((ControlEstimateResult)ControlOwner)
                    .ControlOwner);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)</para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)</para>
        /// </summary>
        public DisplayType DisplayType
        {
            get
            {
                return ((ControlEstimateResult)ControlOwner).DisplayType;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané kolekce výpočetních buněk</para>
        /// <para lang="en">Selected estimation cell collections</para>
        /// </summary>
        public EstimationCellCollectionList SelectedEstimationCellCollections
        {
            get
            {
                return ((ControlEstimateResult)ControlOwner).SelectedEstimationCellCollections;
            }
            set
            {
                ((ControlEstimateResult)ControlOwner).SelectedEstimationCellCollections = value;
                EstimationCellCollectionChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina výpočetních období</para>
        /// <para lang="en">Selected estimation periods</para>
        /// </summary>
        public EstimationPeriodList SelectedEstimationPeriods
        {
            get
            {
                return ((ControlEstimateResult)ControlOwner).SelectedEstimationPeriods;
            }
            set
            {
                ((ControlEstimateResult)ControlOwner).SelectedEstimationPeriods = value;
                EstimationPeriodChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané seznamy fází odhadů pro čitatel a jmenovatel</para>
        /// <para lang="en">Selected phase estimate type lists for numerator and denominator</para>
        /// </summary>
        public SelectedPhaseEstimateTypes SelectedPhaseEstimateTypes
        {
            get
            {
                return ((ControlEstimateResult)ControlOwner).SelectedPhaseEstimateTypes;
            }
            set
            {
                ((ControlEstimateResult)ControlOwner).SelectedPhaseEstimateTypes = value;
                EstimationCellCollectionChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Odhady úhrnu</para>
        /// <para lang="en">Total estimates</para>
        /// </summary>
        public OLAPTotalEstimateList DataSourceTotal
        {
            get
            {
                return DisplayType switch
                {
                    DisplayType.Total =>
                        dataSourceTotal ??
                            new OLAPTotalEstimateList(database: Database),

                    DisplayType.Ratio =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(DataSourceTotal)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Ratio)}"),
                            paramName: nameof(DataSourceTotal)),

                    _ =>
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value."),
                };
            }
            set
            {
                switch (DisplayType)
                {
                    case DisplayType.Total:
                        dataSourceTotal = value ??
                            new OLAPTotalEstimateList(database: Database);
                        InitializeDataGridView();
                        return;

                    case DisplayType.Ratio:
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(DataSourceTotal)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Ratio)}"),
                            paramName: nameof(DataSourceTotal));

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Odhady podílu</para>
        /// <para lang="en">Ratio estimates</para>
        /// </summary>
        public OLAPRatioEstimateList DataSourceRatio
        {
            get
            {
                return DisplayType switch
                {
                    DisplayType.Total =>
                       throw new Exception(
                            message: String.Concat(
                                $"Argument {nameof(DataSourceRatio)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Total)}")),

                    DisplayType.Ratio =>
                        dataSourceRatio ?? new OLAPRatioEstimateList(database: Database),

                    _ =>
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value."),
                };
            }
            set
            {
                switch (DisplayType)
                {
                    case DisplayType.Total:
                        throw new Exception(
                            message: String.Concat(
                                $"Argument {nameof(DataSourceRatio)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Total)}"));

                    case DisplayType.Ratio:
                        dataSourceRatio = value ??
                           new OLAPRatioEstimateList(database: Database);
                        InitializeDataGridView();
                        return;

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Podmínka pro výběr řádků v DataGridView</para>
        /// <para lang="en">Condition for rows selection in DataGridView</para>
        /// </summary>
        public FilterRow RowFilter
        {
            get
            {
                return rowFilter ??
                    new FilterRow();
            }
            set
            {
                SetRowFilter(filter: value);
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví podmínku pro výběr řádků v DataGridView</para>
        /// <para lang="en">Sets condition for rows selection in DataGridView</para>
        /// </summary>
        private void SetRowFilter(FilterRow filter)
        {
            rowFilter = filter ??
                    new FilterRow();

            switch (DisplayType)
            {
                case DisplayType.Total:
                    DataSourceTotal.RowFilter = rowFilter.ToString();
                    break;

                case DisplayType.Ratio:
                    DataSourceRatio.RowFilter = rowFilter.ToString();
                    break;

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }

            SetLabelRowsCountValue(count: dgvResults.Rows.Count);

            DisplayFilterCondition();

            SetButtonsAccessibility();

            HighlightDataGridViewRows();

            btnCancelRowFilter.Visible = !RowFilter.IsEmpty;

            RowFilterChanged?.Invoke(sender: this, e: new EventArgs());
        }


        /// <summary>
        /// <para lang="cs">Vybraný odhad úhrnu</para>
        /// <para lang="en">Selected total estimate</para>
        /// </summary>
        public OLAPTotalEstimate SelectedEstimateTotal
        {
            get
            {
                if (dgvResults.SelectedRows.Count != 1)
                {
                    return null;
                }

                if (dgvResults.SelectedRows[0].Cells[OLAPTotalEstimateList.ColEstimateConfId.Name].Value == null)
                {
                    return null;
                }

                switch (DisplayType)
                {
                    case DisplayType.Total:
                        if (Int32.TryParse(
                            s: dgvResults.SelectedRows[0].Cells[OLAPTotalEstimateList.ColEstimateConfId.Name].Value.ToString(),
                            out int estimateConfId))
                        {
                            return
                                DataSourceTotal.Items
                                    .Where(a => a.EstimateConfId == estimateConfId)
                                    .FirstOrDefault<OLAPTotalEstimate>();
                        }
                        else
                        {
                            return null;
                        }

                    case DisplayType.Ratio:
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(SelectedEstimateTotal)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Ratio)}"),
                            paramName: nameof(SelectedEstimateTotal));

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný odhad podílu</para>
        /// <para lang="en">Selected ratio estimate</para>
        /// </summary>
        public OLAPRatioEstimate SelectedEstimateRatio
        {
            get
            {
                if (dgvResults.SelectedRows.Count != 1)
                {
                    return null;
                }

                if (dgvResults.SelectedRows[0].Cells[OLAPRatioEstimateList.ColEstimateConfId.Name].Value == null)
                {
                    return null;
                }

                switch (DisplayType)
                {
                    case DisplayType.Total:
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(DataSourceRatio)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Total)}"),
                            paramName: nameof(DataSourceRatio));

                    case DisplayType.Ratio:
                        if (Int32.TryParse(
                             s: dgvResults.SelectedRows[0].Cells[OLAPRatioEstimateList.ColEstimateConfId.Name].Value.ToString(),
                             out int estimateConfId))
                        {
                            return
                                DataSourceRatio.Items
                                    .Where(a => a.EstimateConfId == estimateConfId)
                                    .FirstOrDefault<OLAPRatioEstimate>();
                        }
                        else
                        {
                            return null;
                        }

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazené sloupce odhadů úhrnu</para>
        /// <para lang="en">Displayed columns in total estimates</para>
        /// </summary>
        public string[] VisibleColumnsTotal
        {
            get
            {
                return DisplayType switch
                {
                    DisplayType.Total =>
                        Setting.ViewOLAPTotalsVisibleColumns,

                    DisplayType.Ratio =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(VisibleColumnsTotal)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Ratio)}"),
                            paramName: nameof(VisibleColumnsTotal)),

                    _ =>
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value."),
                };
            }
            set
            {
                switch (DisplayType)
                {
                    case DisplayType.Total:
                        Setting.ViewOLAPTotalsVisibleColumns = value;
                        OLAPTotalEstimateList.SetColumnsVisibility(
                            columnNames: Setting.ViewOLAPTotalsVisibleColumns);
                        InitializeDataGridView();
                        return;

                    case DisplayType.Ratio:
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(VisibleColumnsTotal)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Ratio)}"),
                            paramName: nameof(VisibleColumnsTotal));

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazené sloupce odhadů podílu</para>
        /// <para lang="en">Displayed columns in ratio estimates</para>
        /// </summary>
        public string[] VisibleColumnsRatio
        {
            get
            {
                return DisplayType switch
                {
                    DisplayType.Total =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(VisibleColumnsRatio)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Total)}"),
                            paramName: nameof(VisibleColumnsRatio)),

                    DisplayType.Ratio =>
                        Setting.ViewOLAPRatiosVisibleColumns,

                    _ =>
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value."),
                };
            }
            set
            {
                switch (DisplayType)
                {
                    case DisplayType.Total:
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(VisibleColumnsRatio)} is not accessible ",
                                $"when {nameof(DisplayType)} is {nameof(DisplayType.Total)}"),
                            paramName: nameof(VisibleColumnsRatio));

                    case DisplayType.Ratio:
                        Setting.ViewOLAPRatiosVisibleColumns = value;
                        OLAPRatioEstimateList.SetColumnsVisibility(
                            columnNames: Setting.ViewOLAPRatiosVisibleColumns);
                        InitializeDataGridView();
                        return;

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Je zobrazena historie odhadů? (read-only)</para>
        /// <para lang="en">Is history of the estimates displayed? (read-only)</para>
        /// </summary>
        public bool HistoryDisplayed
        {
            get
            {
                return historyDisplayed;
            }
            private set
            {
                historyDisplayed = value;

                btnDisplayHistory.Text = HistoryDisplayed
                    ? strHideHistory
                    : strDisplayHistory;

                SetButtonsAccessibility();
            }
        }

        /// <summary>
        /// <para lang="cs">Řídící vlákno</para>
        /// <para lang="en">Control thread</para>
        /// </summary>
        public IControlThread ControlThread
        {
            get
            {
                return controlThread;
            }
            private set
            {
                controlThread = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazit tlačítko pro výběr sloupců tabulky?</para>
        /// <para lang="en">Show button for selecting table columns?</para>
        /// </summary>
        public bool DisplayButtonFilterColumns
        {
            get
            {
                return btnFilterColumns.Visible;
            }
            set
            {
                btnFilterColumns.Visible = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazit tlačítko pro zavření ovládacího prvku?</para>
        /// <para lang="en">Show button to close the control?</para>
        /// </summary>
        public bool DisplayButtonClose
        {
            get
            {
                return btnClose.Visible;
            }
            set
            {
                btnClose.Visible = value;
            }
        }


        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraný indikátor (read-only)</para>
        /// <para lang="en">Selected indicator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedTargetVariable
        {
            get
            {
                return
                    ((ControlEstimateResult)ControlOwner)
                        .SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru (read-only)</para>
        /// <para lang="en">Selected indicator metadata (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedTargetVariableMetadata
        {
            get
            {
                return
                    ((ControlEstimateResult)ControlOwner)
                        .SelectedTargetVariableMetadata;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                        { nameof(btnFilterColumns),                         "Výběr sloupců" },
                        { nameof(btnConfigureEstimate),                     "Konfigurace odhadů" },
                        { nameof(btnCalculateEstimate),                     "Výpočet odhadů" },
                        { nameof(btnExportResults),                         "Export dat" },
                        { nameof(btnDisplayPanels),                         "Zobrazit panely" },
                        { nameof(btnEditEstimationPeriod),                  "Výpočetní období" },
                        { nameof(btnEditPanelRefYearSetGroup),              "Skupiny panelů" },
                        { nameof(btnLoadEstimationCellCollection),          "Geografická členění" },
                        { nameof(btnLoadEstimationPeriod),                  "Výpočetní období" },
                        { nameof(btnLoadPhaseEstimateType),                 "Typy odhadů (fáze)" },
                        { nameof(btnCancelRowFilter),                       "Zrušit výběr řádků" },
                        { nameof(grpData),                                  "Odhady:" },
                        { nameof(tsrTools),                                 String.Empty },
                        { nameof(strDisplayHistory),                        "Zobrazit historii" },
                        { nameof(strHideHistory),                           "Skrýt historii" },
                        { nameof(strRowsCountNone),                         "Není vybrán žádný odhad." },
                        { nameof(strRowsCountOne),                          "Je vybrán $1 odhad." },
                        { nameof(strRowsCountSome),                         "Jsou vybrány $1 odhady." },
                        { nameof(strRowsCountMany),                         "Je vybráno $1 odhadů." },
                        { nameof(strConfigurationComplete),                 "Konfigurace byla dokončena. Spotřebovaný čas: $1 s." },
                        { nameof(strCalculationComplete),                   "Výpočet byl dokončen. Spotřebovaný čas: $1 s." },
                        { nameof(msgNewConfigurationExists),                "Konfigurovat je možné pouze odhady, které ještě žádnou konfiguraci nemají." },
                        { nameof(msgNewConfigurationIsNotOnePhase),         "Pro konfiguraci vyberte pouze jeden typ odhadu!" },
                        { nameof(msgNewConfigurationIsNotRegPhase),         "Pro konfiguraci odhadů vyberte pouze jeden typ odhadu." },
                        { nameof(msgNewConfigurationIsNotMixedPhase),       "Není vybraný smíšený typ odhadu." },
                        { nameof(msgNewConfigurationIsNotSingleTypePhase),  "Pro konfiguraci odhadů omezte výběr pouze na jeden typ odhadů." },
                        { nameof(msgNewConfigurationIsNotOnePeriod),        "Pro konfiguraci odhadů omezte výběr pouze na jedno období." },
                        { nameof(msgNewConfigurationIsNotOnePeriodGREG),    "Pro konfiguraci GREG-map odhadů omezte výběr pouze na jedno období." },
                        { nameof(msgNewConfigurationNoneCommonPanels),      "Čitatel a jmenovatel nemají žádné společné panely." },
                        { nameof(msgNewConfigurationNoneSelected),          "Není zvolen žádný odhad pro konfiguraci."},
                        { nameof(msgNewConfigurationIsNotOneEstCellCollection),
                                                                            "Konfiguraci GREG-map odhadů je možné provést pouze na jedné kolekci výpočetních buněk."},
                        { nameof(msgIncompleteGroupForEachStratum) ,        String.Concat(
                                                                            "Pro žádnou kombinaci strat nebyla nalezena odpovídající konfigurace jednofázového odhadu. $0",
                                                                            "Nejprve prosím nakonfigurujte jednofázové odhady odpovídající požadovaným GREG-map odhadům. $0",
                                                                            "Proces konfigurace bude ukončen. $0"
                                                                            )},
                        { nameof(msgNoSuitableModelSigmaParam),             String.Concat(
                                                                            "Neexistuje žádná vhodná kombinace modelu, sigma a typu parametrizační oblasti. $0",
                                                                            "Nejprve prosím definujte vhodné pracovní modely a parametrizační oblasti. $0",
                                                                            "Proces konfigurace bude ukončen. $0"
                                                                            )},
                        { nameof(msgNotExistMatchingGRegMapTotals),         String.Concat(
                                                                            "Pro čitatele a jmenovatele nebyla nalezena žádná odpovídající konfigurace regresních odhadů. $0",
                                                                            "Nejprve prosím nakonfigurujte všechny odpovídající regresní odhady úhrnu. $0"
                                                                            )},
                        { nameof(msgNotExistMatchingGRegMapToSingleTotals), String.Concat(
                                                                            "Pro čitatele a jmenovatele nebyla nalezena žádná odpovídající konfigurace regresního a jednofázového odhadu. $0",
                                                                            "Nejprve prosím nakonfigurujte všechny odpovídající regresní a jednofázové odhady úhrnu. $0"
                                                                            )},
                        { nameof(msgNotExistMatchingSingleToGRegMapTotals), String.Concat(
                                                                            "Pro čitatele a jmenovatele nebyla nalezena žádná odpovídající konfigurace jednofázového a regresního odhadu. $0",
                                                                            "Nejprve prosím nakonfigurujte všechny odpovídající jednofázové a regresní odhady úhrnu. $0"
                                                                            )},
                        { nameof(msgCalcEstimateWithConfiguration),         "Vypočítat je možné pouze odhady, které mají provedenou konfiguraci." },
                        { nameof(msgCalcEstimateNotOnePhase),               "Vypočítat je nyní možné pouze jednofázové odhady." },
                        { nameof(msgCalcEstimateNoneSelected),              "Není zvolen žádný odhad pro výpočet." },
                        { nameof(msgNoSelectedPanelRefYearSetGroup),        "Nebyla vybraná žádná skupina panelů a referečních období." },
                        { nameof(msgNoEstimationPeriod),                    "Záznam (id = $1) neexistuje v tabulce c_estimation_period." },
                        { nameof(msgFnSystemUtilizationIsNull),             "Uložená procedura fn_system_utilization vrátila NULL." },
                        { nameof(msgFnSystemUtilizationLoadMinIsNull),      "Hodnotu load_1_min se nepodařilo získat z výsledku uložené procedury fn_system_utilization." },
                        { nameof(msgFnSystemUtilizationCPUCountIsNull),     "Hodnotu cpu_count se nepodařilo získat z výsledku uložené procedury fn_system_utilization." },
                        { nameof(msgFnSystemUtilizationHighLoad),           "Vysoké zatížení systému." },
                        { nameof(msgCheckSystemResourcesAvailabilityForConfiguration),
                                                                            String.Concat(
                                                                            "Požadovaný počet připojení pro konfiguraci odhadů $1 ",
                                                                            "přesahuje počet dostupných připojení. $0",
                                                                            "Konfigurace bude spuštěna na dostupných $2 připojeních.") },
                        { nameof(msgCheckSystemResourcesAvailabilityForCalculation),
                                                                            String.Concat(
                                                                            "Požadovaný počet připojení pro výpočet odhadů $1 ",
                                                                            "přesahuje počet dostupných připojení. $0",
                                                                            "Výpočet odhadů bude spuštěn na dostupných $2 připojeních.") },

                        #region DataGridView
                        // DataGridView - Total Estimate
                        // 0
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColId.Name}",
                            "Identifikátor záznamu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColId.Name}",
                            "Identifikátor záznamu" },
                        // 1
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu (fáze)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu (fáze)" },
                        // 2
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name}",
                            "Typ odhadu (fáze)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name}",
                            "Typ odhadu (fáze) [cs]" },
                        // 3
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name}",
                            "Typ odhadu (fáze)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name}",
                            "Typ odhadu (fáze) [en]" },
                        // 4
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodId.Name}",
                            "Identifikační číslo období" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodId.Name}",
                            "Identifikační číslo období" },
                        // 5
                            { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateBegin.Name}",
                            "Začátek období" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateBegin.Name}",
                            "Začátek období" },
                        // 6
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateEnd.Name}",
                            "Konec období" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateEnd.Name}",
                            "Konec období" },
                        // 7
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Období" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Období [cs]" },
                        // 8
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Období" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Období [en]" },
                        // 9
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableId.Name}",
                            "Identifikační číslo atributového členění" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableId.Name}",
                            "Identifikační číslo atributového členění" },
                        // 10
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelCs.Name}",
                            "Atributové členění" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelCs.Name}",
                            "Atributové členění [cs]" },
                        // 11
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelEn.Name}",
                            "Atributové členění" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelEn.Name}",
                            "Atributové členění [en]" },
                        // 12
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellId.Name}",
                            "Identifikační číslo oblasti odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellId.Name}",
                            "Identifikační číslo oblasti odhadu" },
                        // 13
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Oblast odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Oblast odhadu [cs]" },
                        // 14
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Oblast odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Oblast odhadu [en]" },
                        // 15
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionId.Name}",
                            "Identifikační číslo geografického členění" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionId.Name}",
                            "Identifikační číslo geografického členění" },
                        // 16
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Geografické členění" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Geografické členění [cs]" },
                        // 17
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Geografické členění" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Geografické členění [en]" },
                        // 18
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Identifikační číslo skupiny panelů" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Identifikační číslo skupiny panelů" },
                        // 19
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Skupina panelů" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Skupina panelů [cs]" },
                        // 20
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Skupina panelů" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Skupina panelů [en]" },
                        // 21
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu" },
                        // 22
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu (úhrn nebo podíl)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu (úhrn nebo podíl)" },
                        // 23
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu úhrnu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu úhrnu" },
                        // 24
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateExecutor.Name}",
                            "Uživatel, který provedl konfiguraci odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateExecutor.Name}",
                            "Uživatel, který provedl konfiguraci odhadu" },
                        // 25
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfName.Name}",
                            "Popisek konfigurace odhadu úhrnu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfName.Name}",
                            "Popisek konfigurace odhadu úhrnu" },
                        // 26
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultId.Name}",
                            "Identifikační číslo výsledku odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultId.Name}",
                            "Identifikační číslo výsledku odhadu" },
                        // 27
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPointEstimate.Name}",
                            "Bodový odhad" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPointEstimate.Name}",
                            "Bodový odhad" },
                        // 28
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVarianceEstimate.Name}",
                            "Rozptyl" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVarianceEstimate.Name}",
                            "Rozptyl" },
                        // 29
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcStarted.Name}",
                            "Zahájení výpočtu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcStarted.Name}",
                            "Zahájení výpočtu" },
                        // 30
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColExtensionVersion.Name}",
                            "Verze extenze" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColExtensionVersion.Name}",
                            "Verze extenze" },
                        // 31
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcDuration.Name}",
                            "Doba výpočtu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcDuration.Name}",
                            "Doba výpočtu" },
                        // 32
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColMinSSize.Name}",
                            "Minimální velikost výběru" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColMinSSize.Name}",
                            "Minimální velikost výběru" },
                        // 33
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColActSSize.Name}",
                            "Skutečná velikost výběru" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColActSSize.Name}",
                            "Skutečná velikost výběru" },
                        // 34
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsLatest.Name}",
                            "Aktuální verze odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsLatest.Name}",
                            "Aktuální verze odhadu" },
                        // 35
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultExecutor.Name}",
                            "Uživatel, který provedl výpočet odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultExecutor.Name}",
                            "Uživatel, který provedl výpočet odhadu" },
                        // 36
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColStandardError.Name}",
                            "Směrodatná chyba" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColStandardError.Name}",
                            "Směrodatná chyba" },
                        // 37
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColConfidenceInterval.Name}",
                            "Intervalový odhad" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColConfidenceInterval.Name}",
                            "Intervalový odhad" },
                        // 38
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColLowerBound.Name}",
                            "Intervalový odhad - dolní mez" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColLowerBound.Name}",
                            "Intervalový odhad - dolní mez" },
                        // 39
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColUpperBound.Name}",
                            "Intervalový odhad - horní mez" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColUpperBound.Name}",
                            "Intervalový odhad - horní mez" },
                        // 40
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffDown.Name}",
                            "Kontrola atributové aditivity - rozdíl v součtu podřízených kategorií" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffDown.Name}",
                            "Kontrola atributové aditivity - rozdíl v součtu podřízených kategorií" },
                        // 41
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffUp.Name}",
                            "Kontrola atributové aditivity - rozdíl v hodnotě nadřazené kategorie" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffUp.Name}",
                            "Kontrola atributové aditivity - rozdíl v hodnotě nadřazené kategorie" },
                        // 42
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffDown.Name}",
                            "Kontrola geografické aditivity - rozdíl v součtu podřízených kategorií" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffDown.Name}",
                            "Kontrola geografické aditivity - rozdíl v součtu podřízených kategorií" },
                        // 43
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffUp.Name}",
                            "Kontrola geografické aditivity - rozdíl v hodnotě nadřazené kategorie" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffUp.Name}",
                            "Kontrola geografické aditivity - rozdíl v hodnotě nadřazené kategorie" },
                        // 44
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAttrAdditive.Name}",
                            "Aditivita odhadu (atributové kategorie)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAttrAdditive.Name}",
                            "Aditivita odhadu (atributové kategorie)" },
                        // 45
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsGeoAdditive.Name}",
                            "Aditivita odhadu (geografické kategorie)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsGeoAdditive.Name}",
                            "Aditivita odhadu (geografické kategorie)" },
                        // 46
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAdditive.Name}",
                            "Aditivita odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAdditive.Name}",
                            "Aditivita odhadu" },

                        // 47
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelId.Name}",
                            "Identifikační číslo modelu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelId.Name}",
                            "Identifikační číslo modelu" },
                        // 48
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelCs.Name}",
                            "Model" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelCs.Name}",
                            "Model [cs]" },
                        // 49
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionCs.Name}",
                            "Model - popis" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionCs.Name}",
                            "Model - popis [cs]" },
                        // 50
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelEn.Name}",
                            "Model" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelEn.Name}",
                            "Model [en]" },
                        // 51
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionEn.Name}",
                            "Model - popis" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionEn.Name}",
                            "Model - popis [en]" },
                        // 52
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColSigma.Name}",
                            "Sigma" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColSigma.Name}",
                            "Sigma" },

                        // 53
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaId.Name}",
                            "Identifikační číslo parametrizační oblasti" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaId.Name}",
                            "Identifikační číslo parametrizační oblasti" },
                        // 54
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelCs.Name}",
                            "Parametrizační oblast" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelCs.Name}",
                            "Parametrizační oblast [cs]" },
                        // 55
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name}",
                            "Parametrizační oblast - popis" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name}",
                            "Parametrizační oblast - popis [cs]" },
                        // 56
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelEn.Name}",
                            "Parametrizační oblast" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelEn.Name}",
                            "Parametrizační oblast [en]" },
                        // 57
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name}",
                            "Parametrizační oblast - popis" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name}",
                            "Parametrizační oblast - popis [en]" },

                        // 58
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeId.Name}",
                            "Identifikační číslo typu parametrizační oblasti" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeId.Name}",
                            "Identifikační číslo typu parametrizační oblasti" },
                        // 59
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name}",
                            "Typ parametrizační oblasti" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name}",
                            "Typ parametrizační oblasti [cs]" },
                        // 60
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name}",
                            "Typ parametrizační oblasti - popis" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name}",
                            "Typ parametrizační oblasti - popis [cs]" },
                        // 61
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name}",
                            "Typ parametrizační oblasti" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name}",
                            "Typ parametrizační oblasti [en]" },
                        // 62
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name}",
                            "Typ parametrizační oblasti - popis" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name}",
                            "Typ parametrizační oblasti - popis [en]" },

                        // 63
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusId.Name}",
                            "Identifikační číslo stavu odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusId.Name}",
                            "Identifikační číslo stavu odhadu" },
                        // 64
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Stav odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Stav odhadu [cs]" },
                        // 65
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Stav odhadu" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Stav odhadu [en]" },

                        // DataGridView - Ratio Estimate
                        // 0
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColId.Name}",
                            "Identifikátor záznamu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColId.Name}",
                            "Identifikátor záznamu" },
                        // 1
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorId.Name}",
                            "Identifikátor záznamu odhadu úhrnu v čitateli" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorId.Name}",
                            "Identifikátor záznamu odhadu úhrnu v čitateli" },
                        // 2
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorId.Name}",
                            "Identifikátor záznamu odhadu úhrnu ve jmenovateli" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorId.Name}",
                            "Identifikátor záznamu odhadu úhrnu ve jmenovateli" },
                        // 3
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu čitatele (fáze)" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu čitatele (fáze)" },
                        // 4
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name}",
                            "Typ odhadu čitatele (fáze)" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name}",
                            "Typ odhadu čitatele (fáze) [cs]" },
                        // 5
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name}",
                            "Typ odhadu čitatele (fáze)" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name}",
                            "Typ odhadu čitatele (fáze) [en]" },
                        // 6
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu jmenovatele (fáze)" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu jmenovatele (fáze)" },
                        // 7
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name}",
                            "Typ odhadu jmenovatele (fáze)" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name}",
                            "Typ odhadu jmenovatele (fáze) [cs]" },
                        // 8
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name}",
                            "Typ odhadu jmenovatele (fáze)" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name}",
                            "Typ odhadu jmenovatele (fáze) [en]" },
                        // 9
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodId.Name}",
                            "Identifikační číslo období" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodId.Name}",
                            "Identifikační číslo období" },
                        // 10
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateBegin.Name}",
                            "Začátek období" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateBegin.Name}",
                            "Začátek období" },
                        // 11
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateEnd.Name}",
                            "Konec období" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateEnd.Name}",
                            "Konec období" },
                        // 12
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Období" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Období [cs]" },
                        // 13
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Období" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Období [en]" },
                        // 14
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableId.Name}",
                            "Identifikační číslo atributového členění čitatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableId.Name}",
                            "Identifikační číslo atributového členění čitatele" },
                        // 15
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name}",
                            "Atributové členění čitatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name}",
                            "Atributové členění čitatele [cs]" },
                        // 16
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name}",
                            "Atributové členění čitatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name}",
                            "Atributové členění čitatele [en]" },
                        // 17
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableId.Name}",
                            "Identifikační číslo atributového členění jmenovatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableId.Name}",
                            "Identifikační číslo atributového členění jmenovatele" },
                        // 18
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name}",
                            "Atributové členění jmenovatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name}",
                            "Atributové členění jmenovatele [cs]" },
                        // 19
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name}",
                            "Atributové členění jmenovatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name}",
                            "Atributové členění jmenovatele [en]" },
                        // 20
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellId.Name}",
                            "Identifikační číslo oblasti odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellId.Name}",
                            "Identifikační číslo oblasti odhadu" },
                        // 21
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Oblast odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Oblast odhadu [cs]" },
                        // 22
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Oblast odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Oblast odhadu [en]" },
                        // 23
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionId.Name}",
                            "Identifikační číslo geografického členění" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionId.Name}",
                            "Identifikační číslo geografického členění" },
                        // 24
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Geografické členění" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Geografické členění [cs]" },
                        // 25
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Geografické členění" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Geografické členění [en]" },
                        // 26
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Identifikační číslo skupiny panelů" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Identifikační číslo skupiny panelů" },
                        // 27
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Skupina panelů"},
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Skupina panelů [cs]" },
                        // 28
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Skupina panelů" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Skupina panelů [en]" },
                        // 29
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu" },
                        // 30
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu (úhrn nebo podíl)" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateTypeId.Name}",
                            "Identifikační číslo typu odhadu (úhrn nebo podíl)" },
                        // 31
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu úhrnu čitatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu úhrnu čitatele" },
                        // 32
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu úhrnu jmenovatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.Name}",
                            "Identifikační číslo konfigurace odhadu úhrnu jmenovatele" },
                        // 33
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateExecutor.Name}",
                            "Uživatel, který provedl konfiguraci odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateExecutor.Name}",
                            "Uživatel, který provedl konfiguraci odhadu" },
                        // 34
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfName.Name}",
                            "Popisek konfigurace odhadu úhrnu čitatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfName.Name}",
                            "Popisek konfigurace odhadu úhrnu čitatele" },
                        // 35
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfName.Name}",
                            "Popisek konfigurace odhadu úhrnu jmenovatele" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfName.Name}",
                            "Popisek konfigurace odhadu úhrnu jmenovatele" },
                        // 36
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultId.Name}",
                            "Identifikační číslo výsledku odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultId.Name}",
                            "Identifikační číslo výsledku odhadu" },
                        // 37
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPointEstimate.Name}",
                            "Bodový odhad" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPointEstimate.Name}",
                            "Bodový odhad" },
                        // 38
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColVarianceEstimate.Name}",
                            "Rozptyl" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColVarianceEstimate.Name}",
                            "Rozptyl" },
                        // 39
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcStarted.Name}",
                            "Zahájení výpočtu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcStarted.Name}",
                            "Zahájení výpočtu" },
                        // 40
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColExtensionVersion.Name}",
                            "Verze extenze pro výpočet" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColExtensionVersion.Name}",
                            "Verze extenze pro výpočet" },
                        // 41
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcDuration.Name}",
                            "Doba výpočtu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcDuration.Name}",
                            "Doba výpočtu" },
                        // 42
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColMinSSize.Name}",
                            "Minimální velikost výběru pro odhad" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColMinSSize.Name}",
                            "Minimální velikost výběru pro odhad" },
                        // 43
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColActSSize.Name}",
                            "Skutečná velikost výběru pro odhad" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColActSSize.Name}",
                            "Skutečná velikost výběru pro odhad" },
                        // 44
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsLatest.Name}",
                            "Aktuální verze odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsLatest.Name}",
                            "Aktuální verze odhadu" },
                        // 45
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultExecutor.Name}",
                            "Uživatel, který provedl výpočet odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultExecutor.Name}",
                            "Uživatel, který provedl výpočet odhadu" },
                        // 46
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColStandardError.Name}",
                            "Směrodatná chyba" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColStandardError.Name}",
                            "Směrodatná chyba" },
                        // 47
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColConfidenceInterval.Name}",
                            "Intervalový odhad" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColConfidenceInterval.Name}",
                            "Intervalový odhad" },
                        // 48
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColLowerBound.Name}",
                            "Intervalový odhad - dolní mez" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColLowerBound.Name}",
                            "Intervalový odhad - dolní mez" },
                        // 49
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColUpperBound.Name}",
                            "Intervalový odhad - horní mez" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColUpperBound.Name}",
                            "Intervalový odhad - horní mez" },
                        // 50
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffDown.Name}",
                            "Kontrola atributové aditivity - rozdíl v součtu podřízených kategorií" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffDown.Name}",
                            "Kontrola atributové aditivity - rozdíl v součtu podřízených kategorií" },
                        // 51
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffUp.Name}",
                            "Kontrola atributové aditivity - rozdíl v hodnotě nadřazené kategorie" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffUp.Name}",
                            "Kontrola atributové aditivity - rozdíl v hodnotě nadřazené kategorie" },
                        // 52
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsAdditive.Name}",
                            "Aditivita odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsAdditive.Name}",
                            "Aditivita odhadu"  },
                        // 53
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusId.Name}",
                            "Identifikační číslo stavu odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusId.Name}",
                            "Identifikační číslo stavu odhadu" },
                        // 54
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Stav odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Stav odhadu [cs]" },
                        // 55
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Stav odhadu" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Stav odhadu [en]" }

                        #endregion DataGridView
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(key: nameof(ControlViewOLAP),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnFilterColumns),                         "Columns selection" },
                        { nameof(btnConfigureEstimate),                     "Estimate configuration" },
                        { nameof(btnCalculateEstimate),                     "Estimate calculation" },
                        { nameof(btnExportResults),                         "Export data" },
                        { nameof(btnDisplayPanels),                         "Display panels" },
                        { nameof(btnEditEstimationPeriod),                  "Estimation periods" },
                        { nameof(btnEditPanelRefYearSetGroup),              "Panel groups" },
                        { nameof(btnLoadEstimationCellCollection),          "Cell collections" },
                        { nameof(btnLoadEstimationPeriod),                  "Estimation periods" },
                        { nameof(btnLoadPhaseEstimateType),                 "Estimate type (phase)" },
                        { nameof(btnCancelRowFilter),                       "Cancel rows selection" },
                        { nameof(grpData),                                  "Estimates:" },
                        { nameof(tsrTools),                                 String.Empty },
                        { nameof(strDisplayHistory),                        "Display history" },
                        { nameof(strHideHistory),                           "Hide history" },
                        { nameof(strRowsCountNone),                         "No estimate is selected." },
                        { nameof(strRowsCountOne),                          "$1 estimate is selected." },
                        { nameof(strRowsCountSome),                         "$1 estimates are selected." },
                        { nameof(strRowsCountMany),                         "$1 estimates are selected." },
                        { nameof(strConfigurationComplete),                 "The calculation was completed. Elapsed time: $1 s." },
                        { nameof(strCalculationComplete),                   "The calculation was completed. Elapsed time: $1 s." },
                        { nameof(msgNewConfigurationExists),                "Creating new configuration is possible only for estimates without any configuration."  },
                        { nameof(msgNewConfigurationIsNotOnePhase),         "Choose only one type of estimator for configuration!" },
                        { nameof(msgNewConfigurationIsNotRegPhase),         "To configure estimates filter one type of estimator only." },
                        { nameof(msgNewConfigurationIsNotMixedPhase),       "Mixed estimate type is not selected." },
                        { nameof(msgNewConfigurationIsNotSingleTypePhase),  "To configure estimates filter to only one type of estimates." },
                        { nameof(msgNewConfigurationIsNotOnePeriod),        "To configure any estimates filter one estimation period only." },
                        { nameof(msgNewConfigurationIsNotOnePeriodGREG),    "To configure GREG-map estimates filter one estimation period only." },
                        { nameof(msgNewConfigurationNoneCommonPanels),      "The numerator and denominator have no common panels." },
                        { nameof(msgNewConfigurationNoneSelected),          "No estimate is selected for the configuration." },
                        { nameof(msgNewConfigurationIsNotOneEstCellCollection),
                                                                            "Configuration of GREG-map estimates can be done for one cell collection only."},
                        { nameof(msgIncompleteGroupForEachStratum),         String.Concat(
                                                                            "No matching single-phase configuration found for any strata combination. $0",
                                                                            "Please configure single-phase estimates corresponding to desired GREG-map estimates first. $0",
                                                                            "The configuration process will be terminated. $0"
                                                                            )},
                        { nameof(msgNoSuitableModelSigmaParam),             String.Concat(
                                                                            "No suitable combination of model, sigma and parametrisation area type exists. $0",
                                                                            "Please define suitable working models and parametrisation areas first. $0",
                                                                            "The configuration process will be terminated. $0"
                                                                            )},
                        { nameof(msgNotExistMatchingGRegMapTotals),         String.Concat(
                                                                            "No matching GREG-map configuration found for numerator and denominator. $0",
                                                                            "Please configure all corresponding GREG-map total estimates first. $0"
                                                                            )},
                        { nameof(msgNotExistMatchingGRegMapToSingleTotals), String.Concat(
                                                                            "No matching GREG-map and single-phase config found for nominator and denominator. $0",
                                                                            "Please configure all corresponding total estimates first. $0"
                                                                            )},
                        { nameof(msgNotExistMatchingSingleToGRegMapTotals), String.Concat(
                                                                            "No matching GREG-map and single-phase config found for nominator and denominator. $0",
                                                                            "Please configure all corresponding total estimates first. $0"
                                                                            )},
                        { nameof(msgCalcEstimateWithConfiguration),         "Calculation is possible only for estimates with configuration." },
                        { nameof(msgCalcEstimateNotOnePhase),               "Calculation is now possible only for one phase estimates." },
                        { nameof(msgCalcEstimateNoneSelected),              "No estimate is selected for calculation." },
                        { nameof(msgNoSelectedPanelRefYearSetGroup),        "No group of panels and reference year set was selected." },
                        { nameof(msgNoEstimationPeriod),                    "Record (id = $1) does not exist in database table c_estimation_period." },
                        { nameof(msgFnSystemUtilizationIsNull),             "Stored procedure fn_system_utilization returned NULL." },
                        { nameof(msgFnSystemUtilizationLoadMinIsNull),      "The load_1_min value could not be obtained from the result of the fn_system_utilization stored procedure." },
                        { nameof(msgFnSystemUtilizationCPUCountIsNull),     "The cpu_count value could not be obtained from the result of the fn_system_utilization stored procedure." },
                        { nameof(msgFnSystemUtilizationHighLoad),           "High system load." },
                        { nameof(msgCheckSystemResourcesAvailabilityForConfiguration),
                                                                            String.Concat(
                                                                            "Required number of connections for estimate configurations $1 ",
                                                                            "exceeds the number of available connections. $0",
                                                                            "The estimate configurations will be run on the available $2 connections.") },
                        { nameof(msgCheckSystemResourcesAvailabilityForCalculation),
                                                                            String.Concat(
                                                                            "Required number of connections for estimate calculations $1 ",
                                                                            "exceeds the number of available connections. $0",
                                                                            "The estimate calculations will be run on the available $2 connections.") },

                        #region DataGridView
                        // DataGridView - Total Estimate
                        // 0
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColId.Name}",
                            "Record - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColId.Name}",
                            "Record - identifier" },
                        // 1
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name}",
                            "Estimate type (phase) - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name}",
                            "Estimate type (phase) - identifier" },
                        // 2
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name}",
                            "Estimate type (phase)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name}",
                            "Estimate type (phase) [cs]" },
                        // 3
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name}",
                            "Estimate type (phase)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name}",
                            "Estimate type (phase) [en]" },
                        // 4
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodId.Name}",
                            "Period - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodId.Name}",
                            "Period - identifier" },
                        // 5
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateBegin.Name}",
                            "Estimate date begin" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateBegin.Name}",
                            "Estimate date begin" },
                        // 6
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateEnd.Name}",
                            "Estimate date end" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateDateEnd.Name}",
                            "Estimate date end" },
                        // 7
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Period" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Period [cs]" },
                        // 8
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Period" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Period [en]" },
                        // 9
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableId.Name}",
                            "Attribute category - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableId.Name}",
                            "Attribute category - identifier" },
                        // 10
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelCs.Name}",
                            "Attribute category" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelCs.Name}",
                            "Attribute category [cs]" },
                        // 11
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelEn.Name}",
                            "Attribute category" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVariableLabelEn.Name}",
                            "Attribute category [en]" },
                        // 12
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellId.Name}",
                            "Estimation cell - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellId.Name}",
                            "Estimation cell - identifier" },
                        // 13
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Estimation cell" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Estimation cell [cs]" },
                        // 14
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Estimation cell" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Estimation cell [en]" },
                        // 15
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionId.Name}",
                            "Cell collection - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionId.Name}",
                            "Cell collection - identifier" },
                        // 16
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Cell collection" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Cell collection [cs]" },
                        // 17
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Cell collection" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Cell collection [en]" },
                        // 18
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Group of panels - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Group of panels - identifier" },
                        // 19
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Group of panels" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Group of panels [cs]" },
                        // 20
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Group of panels" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Group of panels [en]" },
                        // 21
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfId.Name}",
                            "Estimate configuration - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfId.Name}",
                            "Estimate configuration - identifier" },
                        // 22
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateTypeId.Name}",
                            "Estimate type (total or ratio) - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateTypeId.Name}",
                            "Estimate type (total or ratio) - identifier" },
                        // 23
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfId.Name}",
                            "Total estimate configuration - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfId.Name}",
                            "Total estimate configuration - identifier" },
                        // 24
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateExecutor.Name}",
                            "User who configurated estimate" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateExecutor.Name}",
                            "User who configurated estimate" },
                        // 25
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfName.Name}",
                            "Total estimate configuration - label" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColTotalEstimateConfName.Name}",
                            "Total estimate configuration - label" },
                        // 26
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultId.Name}",
                            "Estimate result - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultId.Name}",
                            "Estimate result - identifier" },
                        // 27
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPointEstimate.Name}",
                            "Point estimate" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColPointEstimate.Name}",
                            "Point estimate" },
                        // 28
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVarianceEstimate.Name}",
                            "Variance" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColVarianceEstimate.Name}",
                            "Variance" },
                        // 29
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcStarted.Name}",
                            "Calculation started" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcStarted.Name}",
                            "Calculation started" },
                        // 30
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColExtensionVersion.Name}",
                            "Extension version" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColExtensionVersion.Name}",
                            "Extension version" },
                        // 31
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcDuration.Name}",
                            "Calculation duration" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColCalcDuration.Name}",
                            "Calculation duration" },
                        // 32
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColMinSSize.Name}",
                            "Minimal sample size" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColMinSSize.Name}",
                            "Minimal sample size" },
                        // 33
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColActSSize.Name}",
                            "Actual sample size" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColActSSize.Name}",
                            "Actual sample size" },
                        // 34
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsLatest.Name}",
                            "Latest estimate version" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsLatest.Name}",
                            "Latest estimate version" },
                        // 35
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultExecutor.Name}",
                            "User who calculated estimate result" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColResultExecutor.Name}",
                            "User who calculated estimate result" },
                        // 36
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColStandardError.Name}",
                            "Standard error" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColStandardError.Name}",
                            "Standard error" },
                        // 37
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColConfidenceInterval.Name}",
                            "Confidence interval" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColConfidenceInterval.Name}",
                            "Confidence interval" },
                        // 38
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColLowerBound.Name}",
                            "Confidence interval - lower bound" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColLowerBound.Name}",
                            "Confidence interval - lower bound" },
                        // 39
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColUpperBound.Name}",
                            "Confidence interval - upper bound" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColUpperBound.Name}",
                            "Confidence interval - upper bound" },
                        // 40
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffDown.Name}",
                            "Attribute additivity difference in sum of lower categories" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffDown.Name}",
                            "Attribute additivity difference in sum of lower categories" },
                        // 41
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffUp.Name}",
                            "Attribute additivity difference in value of superior category" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColAttrDiffUp.Name}",
                            "Attribute additivity difference in value of superior category" },
                        // 42
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffDown.Name}",
                            "Geographical additivity difference in sum of lower categories" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffDown.Name}",
                            "Geographical additivity difference in sum of lower categories" },
                        // 43
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffUp.Name}",
                            "Geographical additivity difference in value of superior category" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColGeoDiffUp.Name}",
                            "Geographical additivity difference in value of superior category" },
                        // 44
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAttrAdditive.Name}",
                            "Estimate is additive (attribute categories)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAttrAdditive.Name}",
                            "Estimate is additive (attribute categories)" },
                        // 45
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsGeoAdditive.Name}",
                            "Estimate is additive (estimation cells)" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsGeoAdditive.Name}",
                            "Estimate is additive (estimation cells)" },
                        // 46
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAdditive.Name}",
                            "Estimate is additive" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColIsAdditive.Name}",
                            "Estimate is additive" },
                        // 47
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelId.Name}",
                            "Model - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelId.Name}",
                            "Model - identifier" },
                        // 48
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelCs.Name}",
                            "Model" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelCs.Name}",
                            "Model [cs]" },
                        // 49
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionCs.Name}",
                            "Model - description" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionCs.Name}",
                            "Model - description [cs]" },
                        // 50
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelEn.Name}",
                            "Model" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelLabelEn.Name}",
                            "Model [en]" },
                        // 51
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionEn.Name}",
                            "Model - description" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColModelDescriptionEn.Name}",
                            "Model - description [en]" },
                        // 52
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColSigma.Name}",
                            "Sigma" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColSigma.Name}",
                            "Sigma" },
                        // 53
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaId.Name}",
                            "Parametrization area - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaId.Name}",
                            "Parametrization area - identifier" },
                        // 54
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelCs.Name}",
                            "Parametrization area" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelCs.Name}",
                            "Parametrization area [cs]" },
                        // 55
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name}",
                            "Parametrization area - description" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name}",
                            "Parametrization area - description [cs]" },
                        // 56
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelEn.Name}",
                            "Parametrization area" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaLabelEn.Name}",
                            "Parametrization area [en]" },
                        // 57
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name}",
                            "Parametrization area - description" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name}",
                            "Parametrization area - description [en]" },
                        // 58
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeId.Name}",
                            "Parametrization area type - identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeId.Name}",
                            "Parametrization area type - identifier" },
                        // 59
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name}",
                            "Parametrization area type" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name}",
                            "Parametrization area type [cs]" },
                        // 60
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name}",
                            "Parametrization area type - description" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name}",
                            "Parametrization area type - description [cs]" },
                        // 61
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name}",
                            "Parametrization area type" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name}",
                            "Parametrization area type [en]" },
                        // 62
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name}",
                            "Parametrization area type - description" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name}",
                            "Parametrization area type - description [en]" },

                        // 63
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusId.Name}",
                            "Estimate status identifier" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusId.Name}",
                            "Estimate status identifier" },
                        // 64
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Estimate status" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Estimate status [cs]" },
                        // 65
                        { $"col-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Estimate status" },
                        { $"tip-{OLAPTotalEstimateList.Name}.{OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Estimate status [en]" },

                        // DataGridView - Ratio Estimate
                        // 0
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColId.Name}",
                            "Record identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColId.Name}",
                            "Record identifier" },
                        // 1
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorId.Name}",
                            "Record identifier for estimate of total in numerator" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorId.Name}",
                            "Record identifier for estimate of total in numerator" },
                        // 2
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorId.Name}",
                            "Record identifier for estimate of total in denominator" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorId.Name}",
                            "Record identifier for estimate of total in denominator" },
                        // 3
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name}",
                            "Numerator phase estimate type identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name}",
                            "Numerator phase estimate type identifier" },
                        // 4
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name}",
                            "Numerator phase estimate" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name}",
                            "Numerator phase estimate [cs]" },
                        // 5
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name}",
                            "Numerator phase estimate" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name}",
                            "Numerator phase estimate [en]" },
                        // 6
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name}",
                            "Denominator phase estimate type identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name}",
                            "Denominator phase estimate type identifier" },
                        // 7
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name}",
                            "Denominator phase estimate" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name}",
                            "Denominator phase estimate [cs]" },
                        // 8
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name}",
                            "Denominator phase estimate" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name}",
                            "Denominator phase estimate [en]" },
                        // 9
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodId.Name}",
                            "Period identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodId.Name}",
                            "Period identifier" },
                        // 10
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateBegin.Name}",
                            "Estimate date begin" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateBegin.Name}",
                            "Estimate date begin" },
                        // 11
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateEnd.Name}",
                            "Estimate date end" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateDateEnd.Name}",
                            "Estimate date end" },
                        // 12
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Period" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name}",
                            "Period [cs]" },
                        // 13
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Period" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name}",
                            "Period [en]" },
                        // 14
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableId.Name}",
                            "Numerator category identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableId.Name}",
                            "Numerator category identifier" },
                        // 15
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name}",
                            "Numerator category" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name}",
                            "Numerator category [cs]" },
                        // 16
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name}",
                            "Numerator category" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name}",
                            "Numerator category [en]" },
                        // 17
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableId.Name}",
                            "Denominator category identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableId.Name}",
                            "Denominator category identifier" },
                        // 18
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name}",
                            "Denominator category" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name}",
                            "Denominator category [cs]" },
                        // 19
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name}",
                            "Denominator category" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name}",
                            "Denominator category [en]" },
                        // 20
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellId.Name}",
                            "Estimation cell identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellId.Name}",
                            "Estimation cell identifier" },
                        // 21
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Estimation cell" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name}",
                            "Estimation cell [cs]" },
                        // 22
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Estimation cell" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name}",
                            "Estimation cell [en]" },
                        // 23
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionId.Name}",
                            "Geographical division identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionId.Name}",
                            "Geographical division identifier" },
                        // 24
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Geographical division" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name}",
                            "Geographical division [cs]" },
                        // 25
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Geographical division" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name}",
                            "Geographical division [en]" },
                        // 26
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Group of panels identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupId.Name}",
                            "Group of panels identifier" },
                        // 27
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Group of panels"},
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name}",
                            "Group of panels [cs]" },
                        // 28
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Group of panels" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name}",
                            "Group of panels [en]" },
                        // 29
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfId.Name}",
                            "Estimate configuration identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfId.Name}",
                            "Estimate configuration identifier" },
                        // 30
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateTypeId.Name}",
                            "Estimate type (total or ratio) identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateTypeId.Name}",
                            "Estimate type (total or ratio) identifier" },
                        // 31
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.Name}",
                            "Numerator total estimate configuration identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.Name}",
                            "Numerator total estimate configuration identifier" },
                        // 32
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.Name}",
                            "Denominator total estimate configuration identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.Name}",
                            "Denominator total estimate configuration identifier" },
                        // 33
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateExecutor.Name}",
                            "User who configurated estimate" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateExecutor.Name}",
                            "User who configurated estimate" },
                        // 34
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfName.Name}",
                            "Numerator total estimate configuration label" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColNumeratorTotalEstimateConfName.Name}",
                            "Numerator total estimate configuration label" },
                        // 35
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfName.Name}",
                            "Denominator total estimate configuration label" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColDenominatorTotalEstimateConfName.Name}",
                            "Denominator total estimate configuration label" },
                        // 36
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultId.Name}",
                            "Estimate result identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultId.Name}",
                            "Estimate result identifier" },
                        // 37
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPointEstimate.Name}",
                            "Point estimate" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColPointEstimate.Name}",
                            "Point estimate" },
                        // 38
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColVarianceEstimate.Name}",
                            "Variance" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColVarianceEstimate.Name}",
                            "Variance" },
                        // 39
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcStarted.Name}",
                            "Calculation started" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcStarted.Name}",
                            "Calculation started"  },
                        // 40
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColExtensionVersion.Name}",
                            "Extension version" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColExtensionVersion.Name}",
                            "Extension version" },
                        // 41
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcDuration.Name}",
                            "Calculation duration"  },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColCalcDuration.Name}",
                            "Calculation duration" },
                        // 42
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColMinSSize.Name}",
                            "Minimal sample size" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColMinSSize.Name}",
                            "Minimal sample size" },
                        // 43
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColActSSize.Name}",
                            "Actual sample size" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColActSSize.Name}",
                            "Actual sample size" },
                        // 44
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsLatest.Name}",
                            "Latest estimate version"  },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsLatest.Name}",
                            "Latest estimate version"  },
                        // 45
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultExecutor.Name}",
                            "User who calculated estimate result" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColResultExecutor.Name}",
                            "User who calculated estimate result" },
                        // 46
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColStandardError.Name}",
                            "Standard error" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColStandardError.Name}",
                            "Standard error" },
                        // 47
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColConfidenceInterval.Name}",
                            "Confidence interval" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColConfidenceInterval.Name}",
                            "Confidence interval" },
                        // 48
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColLowerBound.Name}",
                            "Confidence interval - lower bound" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColLowerBound.Name}",
                            "Confidence interval - lower bound" },
                        // 49
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColUpperBound.Name}",
                            "Confidence interval - upper bound" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColUpperBound.Name}",
                            "Confidence interval - upper bound" },
                        // 50
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffDown.Name}",
                            "Attribute additivity difference in sum of lower categories" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffDown.Name}",
                            "Attribute additivity difference in sum of lower categories" },
                        // 51
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffUp.Name}",
                            "Attribute additivity difference in value of superior category" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColAttrDiffUp.Name}",
                            "Attribute additivity difference in value of superior category" },
                        // 52
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsAdditive.Name}",
                            "Estimate is additive" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColIsAdditive.Name}",
                            "Estimate is additive" },
                        // 53
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusId.Name}",
                            "Estimate status identifier" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusId.Name}",
                            "Estimate status identifier" },
                        // 54
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Estimate status" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name}",
                            "Estimate status [cs]" },
                        // 55
                        { $"col-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Estimate status" },
                        { $"tip-{OLAPRatioEstimateList.Name}.{OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name}",
                            "Estimate status [en]" }

                        #endregion DataGridView
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(key: nameof(ControlViewOLAP),
                        out Dictionary<string, string> dictInternational)
                        ? dictInternational
                        : [],

                _ => [],
            };
        }

        /// <summary>
        /// <para lang="cs">Vrací metadata sloupce tabulky</para>
        /// <para lang="en">Returns metadata of the table column</para>
        /// </summary>
        /// <param name="displayType">
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)
        /// </para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)
        /// </para>
        /// </param>
        /// <param name="dgvData">
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </param>
        /// <param name="dgvColumn">
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací metadata sloupce tabulky</para>
        /// <para lang="en">Returns metadata of the table column</para>
        /// </returns>
        public static ColumnMetadata GetColumnMetadata(
            DisplayType displayType,
            DataGridView dgvData,
            DataGridViewColumn dgvColumn)
        {
            if (dgvData == null)
            {
                return null;
            }

            if (dgvData.Tag == null)
            {
                return null;
            }

            if (dgvColumn == null)
            {
                return null;
            }

            switch (displayType)
            {
                case DisplayType.Total:
                    if (dgvData.Tag is OLAPTotalEstimateList olapTotal)
                    {
                        return
                            olapTotal.Columns.Values
                            .Where(a => a.Name == dgvColumn.DataPropertyName)
                            .FirstOrDefault<ColumnMetadata>();
                    }
                    else
                    {
                        return null;
                    }

                case DisplayType.Ratio:
                    if (dgvData.Tag is OLAPRatioEstimateList olapRatio)
                    {
                        return
                            olapRatio.Columns.Values
                            .Where(a => a.Name == dgvColumn.DataPropertyName)
                            .FirstOrDefault<ColumnMetadata>();
                    }
                    else
                    {
                        return null;
                    }

                default:
                    return null;
            }
        }

        #endregion Static Methods


        #region Initialization methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            switch (DisplayType)
            {
                case DisplayType.Total:
                    DataSourceTotal = new OLAPTotalEstimateList(
                        database: Database);
                    break;

                case DisplayType.Ratio:
                    DataSourceRatio = new OLAPRatioEstimateList(
                        database: Database);
                    break;

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }

            RowFilter = new FilterRow();

            HistoryDisplayed = false;

            ControlThread = null;

            InitializeLabels();

            LoadContent();

            SetButtonsAccessibility();

            Setting.LoadViewOLAPTotalsColumnsWidth();
            Setting.LoadViewOLAPRatiosColumnsWidth();

            btnFilterColumns.Click += new EventHandler(
                (sender, e) => { FilterColumns(); });

            btnConfigureEstimate.Click += new EventHandler(
                (sender, e) => { AddNewEstimateConfiguration(); });

            btnCalculateEstimate.Click += new EventHandler(
                (sender, e) => { CalculateEstimates(); });

            btnExportResults.Click += new EventHandler(
                (sender, e) => { ExportData(); });

            btnDisplayHistory.Click += new EventHandler(
                (sender, e) => { DisplayHistory(); });

            btnDisplayPanels.Click += new EventHandler(
                (sender, e) => { DisplayPanels(); });

            btnEditEstimationPeriod.Click += new EventHandler(
                (sender, e) => { EditEstimationPeriod(); });

            btnEditPanelRefYearSetGroup.Click += new EventHandler(
                (sender, e) => { EditPanelReferenceYearSetGroup(); });

            btnLoadEstimationCellCollection.Click += new EventHandler(
                (sender, e) => { LoadEstimationCellCollection(); });

            btnLoadEstimationPeriod.Click += new EventHandler(
                (sender, e) => { LoadEstimationPeriod(); });

            btnLoadPhaseEstimateType.Click += new EventHandler(
               (sender, e) => { LoadPhaseEstimateType(); });

            btnCancelRowFilter.Click += new EventHandler(
                (sender, e) => { CancelRowFilter(); });

            btnClose.Click += new EventHandler(
                (sender, e) => { Closed?.Invoke(sender: this, e: new EventArgs()); });

        }

        /// <summary>
        /// <para lang="cs">Inicializace DataGridView pro zobrazení odhadů úhrnu nebo podílu</para>
        /// <para lang="en">Initialization of the DataGridView for display total or ratio estimates</para>
        /// </summary>
        private void InitializeDataGridView()
        {
            pnlData.Controls.Clear();

            dgvResults = new DataGridView()
            {
                Dock = DockStyle.Fill
            };

            dgvResults.Sorted += (sender, e) =>
            {
                HighlightDataGridViewRows();
            };

            dgvResults.SelectionChanged += (sender, e) =>
            {
                RaiseSelectedEstimateChanged();
            };

            dgvResults.ColumnHeaderMouseClick += (sender, e) =>
            {
                if (e.Button == MouseButtons.Right)
                {
                    ColumnMetadata metadata = GetColumnMetadata(
                        displayType: DisplayType,
                        dgvData: (DataGridView)sender,
                        dgvColumn: ((DataGridView)sender).Columns[e.ColumnIndex]);

                    if (metadata == null)
                    {
                        return;
                    }

                    if (String.IsNullOrEmpty(value: metadata.DataType))
                    {
                        return;
                    }

                    if (metadata.DataType == "System.SByte" ||
                        metadata.DataType == "System.Int16" ||
                        metadata.DataType == "System.Int32" ||
                        metadata.DataType == "System.Int64" ||
                        metadata.DataType == "System.Byte" ||
                        metadata.DataType == "System.UInt16" ||
                        metadata.DataType == "System.UInt32" ||
                        metadata.DataType == "System.UInt64" ||
                        metadata.DataType == "System.Boolean" ||
                        metadata.DataType == "System.Char" ||
                        metadata.DataType == "System.String")
                    {
                        FormFilterRow form = new(
                           controlOwner: this,
                           dgvData: (DataGridView)sender,
                           dgvColumn: ((DataGridView)sender).Columns[e.ColumnIndex]);

                        if (form.ShowDialog() == DialogResult.OK) { }
                        return;
                    }

                    if (metadata.DataType == "System.Single" ||
                        metadata.DataType == "System.Double")
                    {
                        // TODO

                        //FormFilterRowNumeric frmFilterNumbers = new FormFilterRowNumeric(
                        //    controlOwner: this,
                        //    dgvData: (DataGridView)sender,
                        //    dgvColumn: ((DataGridView)sender).Columns[e.ColumnIndex]);

                        //if (frmFilterNumbers.ShowDialog() == DialogResult.OK) { }
                        //return;
                    }

                    if (metadata.DataType == "System.DateTime")
                    {
                        FormFilterRowDate frmFilterDates = new(
                            controlOwner: this,
                            dgvData: (DataGridView)sender,
                            dgvColumn: ((DataGridView)sender).Columns[e.ColumnIndex]);

                        if (frmFilterDates.ShowDialog() == DialogResult.OK) { }
                        return;
                    }

                    if (metadata.DataType == "System.TimeSpan")
                    {
                        // TODO
                    }

                }
            };

            dgvResults.ColumnWidthChanged += (sender, e) =>
            {
                switch (DisplayType)
                {
                    case DisplayType.Total:
                        ColumnMetadata colTotal =
                            OLAPTotalEstimateList.Cols.Values
                                .Where(a => a.Name == e.Column.DataPropertyName)
                                .FirstOrDefault<ColumnMetadata>();
                        if (colTotal != null)
                        {
                            colTotal.Width = e.Column.Width;
                            Setting.SaveViewOLAPTotalsColumnsWidth();
                        }
                        break;

                    case DisplayType.Ratio:
                        ColumnMetadata colRatio =
                            OLAPRatioEstimateList.Cols.Values
                                .Where(a => a.Name == e.Column.DataPropertyName)
                                .FirstOrDefault<ColumnMetadata>();
                        if (colRatio != null)
                        {
                            colRatio.Width = e.Column.Width;
                            Setting.SaveViewOLAPRatiosColumnsWidth();
                        }
                        break;

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }

            };

            dgvResults.CreateControl();

            pnlData.Controls.Add(value: dgvResults);

            switch (DisplayType)
            {
                case DisplayType.Total:
                    DataSourceTotal.SetDataGridViewDataSource(
                        dataGridView: dgvResults);
                    DataSourceTotal.FormatDataGridView(
                         dataGridView: dgvResults);
                    break;

                case DisplayType.Ratio:
                    DataSourceRatio.SetDataGridViewDataSource(
                          dataGridView: dgvResults);
                    DataSourceRatio.FormatDataGridView(
                         dataGridView: dgvResults);
                    break;

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }

            foreach (DataGridViewColumn col in dgvResults.Columns)
            {
                if (col is DataGridViewCheckBoxColumn cboxCol)
                {
                    cboxCol.ThreeState = true;
                }
            }

            SetRowFilter(filter: RowFilter);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            // Skryje datavou tabulku, aby zbytečně neproblikávala,
            // když se přenačítají její sloupečky do jiného jazyka
            grpData.Visible = false;

            #region Dictionary

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Dictionary<string, string> labelsCs = Dictionary(
                languageVersion: LanguageVersion.National,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsEn = Dictionary(
                languageVersion: LanguageVersion.International,
                languageFile: LanguageFile);

            #endregion Dictionary

            #region Controls

            btnFilterColumns.Text =
               labels.TryGetValue(key: nameof(btnFilterColumns),
                    out string btnFilterColumnsText)
                        ? btnFilterColumnsText
                        : String.Empty;

            btnConfigureEstimate.Text =
                labels.TryGetValue(key: nameof(btnConfigureEstimate),
                    out string btnConfigureEstimateText)
                        ? btnConfigureEstimateText
                        : String.Empty;

            btnCalculateEstimate.Text =
                labels.TryGetValue(key: nameof(btnCalculateEstimate),
                    out string btnCalculateEstimateText)
                        ? btnCalculateEstimateText
                        : String.Empty;

            btnExportResults.Text =
                labels.TryGetValue(key: nameof(btnExportResults),
                    out string btnExportResultsText)
                        ? btnExportResultsText
                        : String.Empty;

            strDisplayHistory =
                labels.TryGetValue(key: nameof(strDisplayHistory),
                    out strDisplayHistory)
                        ? strDisplayHistory
                        : String.Empty;

            strHideHistory =
                labels.TryGetValue(key: nameof(strHideHistory),
                    out strHideHistory)
                        ? strHideHistory
                        : String.Empty;

            btnDisplayHistory.Text = HistoryDisplayed
                ? strHideHistory
                : strDisplayHistory;

            btnDisplayPanels.Text =
                labels.TryGetValue(key: nameof(btnDisplayPanels),
                    out string btnDisplayPanelsText)
                        ? btnDisplayPanelsText
                        : String.Empty;

            btnEditEstimationPeriod.Text =
                labels.TryGetValue(key: nameof(btnEditEstimationPeriod),
                    out string btnEditEstimationPeriodText)
                        ? btnEditEstimationPeriodText
                        : String.Empty;

            btnEditPanelRefYearSetGroup.Text =
                labels.TryGetValue(key: nameof(btnEditPanelRefYearSetGroup),
                    out string btnEditPanelRefYearSetGroupText)
                        ? btnEditPanelRefYearSetGroupText
                        : String.Empty;

            btnLoadEstimationCellCollection.Text =
                labels.TryGetValue(key: nameof(btnLoadEstimationCellCollection),
                    out string btnLoadEstimationCellCollectionText)
                        ? btnLoadEstimationCellCollectionText
                        : String.Empty;

            btnLoadEstimationPeriod.Text =
                labels.TryGetValue(key: nameof(btnLoadEstimationPeriod),
                    out string btnLoadEstimationPeriodText)
                        ? btnLoadEstimationPeriodText
                        : String.Empty;

            btnLoadPhaseEstimateType.Text =
                labels.TryGetValue(key: nameof(btnLoadPhaseEstimateType),
                    out string btnLoadPhaseEstimateTypeText)
                        ? btnLoadPhaseEstimateTypeText
                        : String.Empty;

            btnCancelRowFilter.Text =
                labels.TryGetValue(key: nameof(btnCancelRowFilter),
                    out string btnCancelRowFilterText)
                        ? btnCancelRowFilterText
                        : String.Empty;

            grpData.Text =
                labels.TryGetValue(key: nameof(grpData),
                    out string grpDataText)
                        ? grpDataText
                        : String.Empty;

            tsrTools.Text =
                labels.TryGetValue(key: nameof(tsrTools),
                    out string tsrToolsText)
                        ? tsrToolsText
                        : String.Empty;

            lblCalculationStatus.Text = String.Empty;

            strConfigurationComplete =
                labels.TryGetValue(key: nameof(strConfigurationComplete),
                    out strConfigurationComplete)
                        ? strConfigurationComplete
                        : String.Empty;

            strCalculationComplete =
                labels.TryGetValue(key: nameof(strCalculationComplete),
                    out strCalculationComplete)
                        ? strCalculationComplete
                        : String.Empty;

            strRowsCountNone =
                labels.TryGetValue(key: nameof(strRowsCountNone),
                    out strRowsCountNone)
                        ? strRowsCountNone
                        : String.Empty;

            strRowsCountOne =
                labels.TryGetValue(key: nameof(strRowsCountOne),
                    out strRowsCountOne)
                        ? strRowsCountOne
                        : String.Empty;

            strRowsCountSome =
                labels.TryGetValue(key: nameof(strRowsCountSome),
                    out strRowsCountSome)
                        ? strRowsCountSome
                        : String.Empty;

            strRowsCountMany =
                labels.TryGetValue(key: nameof(strRowsCountMany),
                    out strRowsCountMany)
                        ? strRowsCountMany
                        : String.Empty;

            msgNewConfigurationExists =
                labels.TryGetValue(key: nameof(msgNewConfigurationExists),
                    out msgNewConfigurationExists)
                        ? msgNewConfigurationExists
                        : String.Empty;

            msgNewConfigurationIsNotOnePhase =
                labels.TryGetValue(key: nameof(msgNewConfigurationIsNotOnePhase),
                    out msgNewConfigurationIsNotOnePhase)
                        ? msgNewConfigurationIsNotOnePhase
                        : String.Empty;

            msgNewConfigurationIsNotRegPhase =
                labels.TryGetValue(key: nameof(msgNewConfigurationIsNotRegPhase),
                    out msgNewConfigurationIsNotRegPhase)
                        ? msgNewConfigurationIsNotRegPhase
                        : String.Empty;

            msgNewConfigurationIsNotMixedPhase =
                labels.TryGetValue(key: nameof(msgNewConfigurationIsNotMixedPhase),
                    out msgNewConfigurationIsNotMixedPhase)
                        ? msgNewConfigurationIsNotMixedPhase
                        : String.Empty;

            msgNewConfigurationIsNotSingleTypePhase =
                labels.TryGetValue(key: nameof(msgNewConfigurationIsNotSingleTypePhase),
                    out msgNewConfigurationIsNotSingleTypePhase)
                        ? msgNewConfigurationIsNotSingleTypePhase
                        : String.Empty;

            msgNewConfigurationIsNotOnePeriod =
                labels.TryGetValue(key: nameof(msgNewConfigurationIsNotOnePeriod),
                    out msgNewConfigurationIsNotOnePeriod)
                        ? msgNewConfigurationIsNotOnePeriod
                        : String.Empty;

            msgNewConfigurationIsNotOnePeriodGREG =
                labels.TryGetValue(key: nameof(msgNewConfigurationIsNotOnePeriodGREG),
                    out msgNewConfigurationIsNotOnePeriodGREG)
                        ? msgNewConfigurationIsNotOnePeriodGREG
                        : String.Empty;

            msgNewConfigurationNoneCommonPanels =
                labels.TryGetValue(key: nameof(msgNewConfigurationNoneCommonPanels),
                    out msgNewConfigurationNoneCommonPanels)
                        ? msgNewConfigurationNoneCommonPanels
                        : String.Empty;

            msgNewConfigurationNoneSelected =
                labels.TryGetValue(key: nameof(msgNewConfigurationNoneSelected),
                    out msgNewConfigurationNoneSelected)
                        ? msgNewConfigurationNoneSelected
                        : String.Empty;

            msgNewConfigurationIsNotOneEstCellCollection =
                labels.TryGetValue(key: nameof(msgNewConfigurationIsNotOneEstCellCollection),
                    out msgNewConfigurationIsNotOneEstCellCollection)
                        ? msgNewConfigurationIsNotOneEstCellCollection
                        : String.Empty;

            msgIncompleteGroupForEachStratum =
                labels.TryGetValue(key: nameof(msgIncompleteGroupForEachStratum),
                    out msgIncompleteGroupForEachStratum)
                        ? msgIncompleteGroupForEachStratum
                        : String.Empty;

            msgNoSuitableModelSigmaParam =
               labels.TryGetValue(key: nameof(msgNoSuitableModelSigmaParam),
                   out msgNoSuitableModelSigmaParam)
                       ? msgNoSuitableModelSigmaParam
                       : String.Empty;

            msgNotExistMatchingGRegMapTotals =
               labels.TryGetValue(key: nameof(msgNotExistMatchingGRegMapTotals),
                   out msgNotExistMatchingGRegMapTotals)
                       ? msgNotExistMatchingGRegMapTotals
                       : String.Empty;

            msgNotExistMatchingGRegMapToSingleTotals =
               labels.TryGetValue(key: nameof(msgNotExistMatchingGRegMapToSingleTotals),
                   out msgNotExistMatchingGRegMapToSingleTotals)
                       ? msgNotExistMatchingGRegMapToSingleTotals
                       : String.Empty;

            msgNotExistMatchingSingleToGRegMapTotals =
               labels.TryGetValue(key: nameof(msgNotExistMatchingSingleToGRegMapTotals),
                   out msgNotExistMatchingSingleToGRegMapTotals)
                       ? msgNotExistMatchingSingleToGRegMapTotals
                       : String.Empty;

            msgCalcEstimateWithConfiguration =
               labels.TryGetValue(key: nameof(msgCalcEstimateWithConfiguration),
                   out msgCalcEstimateWithConfiguration)
                       ? msgCalcEstimateWithConfiguration
                       : String.Empty;

            msgCalcEstimateNotOnePhase =
               labels.TryGetValue(key: nameof(msgCalcEstimateNotOnePhase),
                   out msgCalcEstimateNotOnePhase)
                       ? msgCalcEstimateNotOnePhase
                       : String.Empty;

            msgCalcEstimateNoneSelected =
               labels.TryGetValue(key: nameof(msgCalcEstimateNoneSelected),
                  out msgCalcEstimateNoneSelected)
                      ? msgCalcEstimateNoneSelected
                      : String.Empty;

            msgNoSelectedPanelRefYearSetGroup =
               labels.TryGetValue(key: nameof(msgNoSelectedPanelRefYearSetGroup),
                   out msgNoSelectedPanelRefYearSetGroup)
                       ? msgNoSelectedPanelRefYearSetGroup
                       : String.Empty;

            msgNoEstimationPeriod =
               labels.TryGetValue(key: nameof(msgNoEstimationPeriod),
                   out msgNoEstimationPeriod)
                       ? msgNoEstimationPeriod
                       : String.Empty;

            msgFnSystemUtilizationIsNull =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationIsNull),
                   out msgFnSystemUtilizationIsNull)
                       ? msgFnSystemUtilizationIsNull
                       : String.Empty;

            msgFnSystemUtilizationLoadMinIsNull =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationLoadMinIsNull),
                   out msgFnSystemUtilizationLoadMinIsNull)
                       ? msgFnSystemUtilizationLoadMinIsNull
                       : String.Empty;

            msgFnSystemUtilizationCPUCountIsNull =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationCPUCountIsNull),
                   out msgFnSystemUtilizationCPUCountIsNull)
                       ? msgFnSystemUtilizationCPUCountIsNull
                       : String.Empty;

            msgFnSystemUtilizationHighLoad =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationHighLoad),
                   out msgFnSystemUtilizationHighLoad)
                       ? msgFnSystemUtilizationHighLoad
                       : String.Empty;

            msgCheckSystemResourcesAvailabilityForConfiguration =
               labels.TryGetValue(key: nameof(msgCheckSystemResourcesAvailabilityForConfiguration),
                   out msgCheckSystemResourcesAvailabilityForConfiguration)
                       ? msgCheckSystemResourcesAvailabilityForConfiguration
                       : String.Empty;

            msgCheckSystemResourcesAvailabilityForCalculation =
               labels.TryGetValue(key: nameof(msgCheckSystemResourcesAvailabilityForCalculation),
                   out msgCheckSystemResourcesAvailabilityForCalculation)
                       ? msgCheckSystemResourcesAvailabilityForCalculation
                       : String.Empty;

            #endregion Controls

            #region Visible Columns

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    switch (DisplayType)
                    {
                        case DisplayType.Total:
                            VisibleColumnsTotal =
                                Setting.ViewOLAPTotalsVisibleColumns
                                .Select(a => a
                                    .Replace(OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name, OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name, OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name, OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name, OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColVariableLabelCs.Name, OLAPTotalEstimateList.ColVariableLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColVariableLabelEn.Name, OLAPTotalEstimateList.ColVariableLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name, OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name, OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name, OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name, OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name, OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name, OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelLabelCs.Name, OLAPTotalEstimateList.ColModelLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelLabelEn.Name, OLAPTotalEstimateList.ColModelLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelDescriptionCs.Name, OLAPTotalEstimateList.ColModelDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelDescriptionEn.Name, OLAPTotalEstimateList.ColModelDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaLabelCs.Name, OLAPTotalEstimateList.ColParamAreaLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaLabelEn.Name, OLAPTotalEstimateList.ColParamAreaLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name, OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name, OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name, OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name, OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name, OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name, OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name, OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name, OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name))
                                .Distinct()
                                .ToArray<string>();
                            break;

                        case DisplayType.Ratio:
                            VisibleColumnsRatio =
                                Setting.ViewOLAPRatiosVisibleColumns
                                .Select(a => a
                                    .Replace(OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name, OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name, OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name, OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name, OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name, OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name, OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name, OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name, OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name, OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name, OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name, OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name, OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name, OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name, OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name, OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name, OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name))
                                .Distinct()
                                .ToArray<string>();
                            break;

                        default:
                            throw new ArgumentException(
                                message: $"Argument {nameof(DisplayType)} unknown value.",
                                paramName: nameof(DisplayType));
                    }
                    break;

                case LanguageVersion.International:
                    switch (DisplayType)
                    {
                        case DisplayType.Total:
                            VisibleColumnsTotal =
                                Setting.ViewOLAPTotalsVisibleColumns
                                .Select(a => a
                                    .Replace(OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name, OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name, OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name, OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name, OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColVariableLabelCs.Name, OLAPTotalEstimateList.ColVariableLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColVariableLabelEn.Name, OLAPTotalEstimateList.ColVariableLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name, OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name, OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name, OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name, OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name, OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name, OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelLabelCs.Name, OLAPTotalEstimateList.ColModelLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelLabelEn.Name, OLAPTotalEstimateList.ColModelLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelDescriptionCs.Name, OLAPTotalEstimateList.ColModelDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColModelDescriptionEn.Name, OLAPTotalEstimateList.ColModelDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaLabelCs.Name, OLAPTotalEstimateList.ColParamAreaLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaLabelEn.Name, OLAPTotalEstimateList.ColParamAreaLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name, OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name, OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name, OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name, OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name, OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name, OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name, OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name)
                                    .Replace(OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name, OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name))
                                .Distinct()
                                .ToArray<string>();
                            break;

                        case DisplayType.Ratio:
                            VisibleColumnsRatio =
                                Setting.ViewOLAPRatiosVisibleColumns
                                .Select(a => a
                                    .Replace(OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name, OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name, OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name, OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name, OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name, OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name, OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name, OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name, OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name, OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name, OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name, OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name, OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name, OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name, OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name, OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name, OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name)
                                    .Replace(OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name, OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name))
                                .Distinct()
                                .ToArray<string>();
                            break;

                        default:
                            throw new ArgumentException(
                                message: $"Argument {nameof(DisplayType)} unknown value.",
                                paramName: nameof(DisplayType));
                    }
                    break;

                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(LanguageVersion)} unknown value.",
                        paramName: nameof(LanguageVersion));
            }

            #endregion Visible Columns

            #region OLAPTotalEstimateList

            foreach (ColumnMetadata column in OLAPTotalEstimateList.Cols.Values)
            {
                OLAPTotalEstimateList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{OLAPTotalEstimateList.Name}.{column.Name}")
                        ? labelsCs[$"col-{OLAPTotalEstimateList.Name}.{column.Name}"]
                        : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{OLAPTotalEstimateList.Name}.{column.Name}")
                        ? labelsEn[$"col-{OLAPTotalEstimateList.Name}.{column.Name}"]
                        : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{OLAPTotalEstimateList.Name}.{column.Name}")
                        ? labelsCs[$"tip-{OLAPTotalEstimateList.Name}.{column.Name}"]
                        : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{OLAPTotalEstimateList.Name}.{column.Name}")
                        ? labelsEn[$"tip-{OLAPTotalEstimateList.Name}.{column.Name}"]
                        : String.Empty);
            }

            #endregion OLAPTotalEstimateList

            #region OLAPRatioEstimateList

            foreach (ColumnMetadata column in OLAPRatioEstimateList.Cols.Values)
            {
                OLAPRatioEstimateList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{OLAPRatioEstimateList.Name}.{column.Name}") ?
                    labelsCs[$"col-{OLAPRatioEstimateList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{OLAPRatioEstimateList.Name}.{column.Name}") ?
                    labelsEn[$"col-{OLAPRatioEstimateList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{OLAPRatioEstimateList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{OLAPRatioEstimateList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{OLAPRatioEstimateList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{OLAPRatioEstimateList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion OLAPRatioEstimateList

            // Znovu zobrazí přenačtenou datovou tabulku
            grpData.Visible = true;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
        }

        #endregion Initialization methods


        #region Display methods

        /// <summary>
        /// <para lang="cs">Nastavení popisku s počtem vybraných odhadů</para>
        /// <para lang="en">Setting the label with the number of selected estimates</para>
        /// </summary>
        /// <param name="count">
        /// <para lang="cs">Počet vybraných odhadů</para>
        /// <para lang="en">Number of selected estimates</para>
        /// </param>
        private void SetLabelRowsCountValue(int count)
        {
            switch (count)
            {
                case 0:
                    lblRowsCount.Text = strRowsCountNone
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 1:
                    lblRowsCount.Text = strRowsCountOne
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 2:
                case 3:
                case 4:
                    lblRowsCount.Text = strRowsCountSome
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                default:
                    lblRowsCount.Text = strRowsCountMany
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí podmínku pro výběr řádků</para>
        /// <para lang="en">Display condition for rows selection</para>
        /// </summary>
        private void DisplayFilterCondition()
        {
            if (RowFilter == null)
            {
                lblFilterCondition.Text = String.Empty;
                return;
            }

            if (RowFilter.ToString() == "true")
            {
                lblFilterCondition.Text = String.Empty;
                return;
            }

            lblFilterCondition.Text = RowFilter.ToString();
        }

        /// <summary>
        /// <para lang="cs">Zvýraznění řádků v DataGridView</para>
        /// <para lang="en">Highlight rows in DataGridView</para>
        /// </summary>
        private void HighlightDataGridViewRows()
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    foreach (DataGridViewRow row in dgvResults.Rows)
                    {
                        int status =
                            Functions.GetNIntArg(
                                row: row,
                                name: OLAPTotalEstimateList.ColEstimateConfStatusId.Name,
                                defaultValue: null) ?? 0;

                        bool isLatest =
                            Functions.GetNBoolArg(
                                row: row,
                                name: OLAPTotalEstimateList.ColIsLatest.Name,
                                defaultValue: null) ?? true;

                        bool isAttrAdditive =
                            Functions.GetNBoolArg(
                                row: row,
                                name: OLAPTotalEstimateList.ColIsAttrAdditive.Name,
                                defaultValue: null) ?? false;

                        bool isGeoAdditive =
                            Functions.GetNBoolArg(
                                row: row,
                                name: OLAPTotalEstimateList.ColIsGeoAdditive.Name,
                                defaultValue: null) ?? false;

                        HighlightHistoryDataGridViewRow(
                            row: row,
                            isLatest: isLatest);

                        HighlightNonAditiveDataGridViewRow(
                            row: row,
                            status: status,
                            isAttrAdditive: isAttrAdditive,
                            isGeoAdditive: isGeoAdditive);
                    }
                    break;

                case DisplayType.Ratio:
                    foreach (DataGridViewRow row in dgvResults.Rows)
                    {
                        int status =
                           Functions.GetNIntArg(
                                       row: row,
                                       name: OLAPRatioEstimateList.ColEstimateConfStatusId.Name,
                                       defaultValue: null) ?? 0;

                        bool isLatest =
                            Functions.GetNBoolArg(
                                         row: row,
                                         name: OLAPRatioEstimateList.ColIsLatest.Name,
                                         defaultValue: null) ?? true;

                        bool isAttrAdditive =
                            Functions.GetNBoolArg(
                                         row: row,
                                         name: OLAPRatioEstimateList.ColIsAdditive.Name,
                                         defaultValue: null) ?? false;

                        bool isGeoAdditive =
                            Functions.GetNBoolArg(
                                         row: row,
                                         name: OLAPRatioEstimateList.ColIsAdditive.Name,
                                         defaultValue: null) ?? false;

                        HighlightHistoryDataGridViewRow(
                            row: row,
                            isLatest: isLatest);

                        HighlightNonAditiveDataGridViewRow(
                            row: row,
                            status: status,
                            isAttrAdditive: isAttrAdditive,
                            isGeoAdditive: isGeoAdditive);
                    }
                    break;

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Zvýraznění řádku v DataGridView, který obsahuje historii</para>
        /// <para lang="en">Highlight row in DataGridView, that contains history record</para>
        /// </summary>
        /// <param name="row">
        /// <para lang="cs">Řádek v DataGridView</para>
        /// <para lang="en">Row in DataGridView</para>
        /// </param>
        /// <param name="isLatest">
        /// <para lang="cs">true - aktuální záznam, false - historický záznam</para>
        /// <para lang="en">true - actual record, false - historical record</para>
        /// </param>
        private void HighlightHistoryDataGridViewRow(
            DataGridViewRow row,
            bool isLatest)
        {
            if (HistoryDisplayed)
            {
                if (isLatest)
                {
                    row.DefaultCellStyle.Font =
                    new System.Drawing.Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: System.Drawing.FontStyle.Regular,
                        unit: System.Drawing.GraphicsUnit.Point,
                        gdiCharSet: (byte)238);
                    row.DefaultCellStyle.ForeColor =
                        RegularEstimateRowColor;
                }
                else
                {
                    row.DefaultCellStyle.Font =
                    new System.Drawing.Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 8F,
                        style: System.Drawing.FontStyle.Regular,
                        unit: System.Drawing.GraphicsUnit.Point,
                        gdiCharSet: (byte)238);
                    row.DefaultCellStyle.ForeColor =
                        HistoryEstimateRowColor;
                }
            }
            else
            {
                row.DefaultCellStyle.Font =
                    new System.Drawing.Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: System.Drawing.FontStyle.Regular,
                        unit: System.Drawing.GraphicsUnit.Point,
                        gdiCharSet: (byte)238);
                row.DefaultCellStyle.ForeColor =
                        RegularEstimateRowColor;
            }
        }

        /// <summary>
        /// <para lang="cs">Zvýraznění řádku v DataGridView, který není aditivní</para>
        /// <para lang="en">Highlight row in DataGridView, that is not additive</para>
        /// </summary>
        /// <param name="row">
        /// <para lang="cs">Řádek v DataGridView</para>
        /// <para lang="en">Row in DataGridView</para>
        /// </param>
        /// <param name="status">
        /// <para lang="cs">Stav odhadu</para>
        /// <para lang="en">Estimate state</para>
        /// </param>
        /// <param name="isAttrAdditive">
        /// <para lang="cs">Záznam je aditivní v atributových kategoriích</para>
        /// <para lang="en">Record is additive in attribute categories</para>
        /// </param>
        /// <param name="isGeoAdditive">
        /// <para lang="cs">Záznam je aditivní v geografických regionech</para>
        /// <para lang="en">Record is additive in geographical regions</para>
        /// </param>
        private static void HighlightNonAditiveDataGridViewRow(
            DataGridViewRow row,
            int status,
            bool isAttrAdditive,
            bool isGeoAdditive)
        {
            // Barevné označení řádků s neaditivitou
            if (status == 300)
            {
                if (!isAttrAdditive && !isGeoAdditive)
                {
                    row.DefaultCellStyle.ForeColor =
                        NonAdditiveEstimateColor;
                }
                else if (!isAttrAdditive)
                {
                    row.DefaultCellStyle.ForeColor =
                        NonAttrAdditiveEstimateColor;
                }
                else if (!isGeoAdditive)
                {
                    row.DefaultCellStyle.ForeColor =
                        NonGeoAdditiveEstimateColor;
                }
                else
                {
                    // Aditivní odhady - ponechá se původní barva textu.
                    // Additive estimates - keep the original text color.
                }
            }
            else
            {
                // Kontrola aditivity se provádí pouze u odhadů, které byly vypočteny (tj. status = 300)
                // Pro ostatní odhady se ponechá původní barva textu.
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Nastaví dostupnost tlačítek v panelu nástrojů
        /// podle vybraného stavu odhadu</para>
        /// <para lang="en">
        /// Sets the availability of buttons in the toolstrip
        /// according to the selected estimation status</para>
        /// </summary>
        public void SetButtonsAccessibility()
        {
            if (HistoryDisplayed)
            {
                // Pokud je zobrazena historie, tlačítka jsou zakázaná
                btnConfigureEstimate.Enabled = false;
                btnCalculateEstimate.Enabled = false;
                btnExportResults.Enabled = false;
                return;
            }

            List<int> selectedEstimateConfStatusIds = [];
            Nullable<int> selectedEstimateConfStatusId = null;

            selectedEstimateConfStatusIds = DisplayType switch
            {
                DisplayType.Total =>
                    DataSourceTotal.Data.DefaultView.ToTable().AsEnumerable()
                        .Select(a => a.Field<int>(columnName: OLAPTotalEstimateList.ColEstimateConfStatusId.Name))
                        .Distinct()
                        .ToList<int>(),

                DisplayType.Ratio =>
                    DataSourceRatio.Data.DefaultView.ToTable().AsEnumerable()
                        .Select(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColEstimateConfStatusId.Name))
                        .Distinct()
                        .ToList<int>(),

                _ =>
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value."),
            };

            selectedEstimateConfStatusId =
                (selectedEstimateConfStatusIds.Count != 1) ? (Nullable<int>)null :
                selectedEstimateConfStatusIds.FirstOrDefault<int>();

            if (selectedEstimateConfStatusId == null)
            {
                // POKUD JE ZAŠKRTNUTO VÍCE STAVů ODHADU
                // TLAČÍTKA NEJSOU DOSTUPNÁ,
                // KROMĚ KONFIGURACE ODHADů, KTERÁ JE MOŽNÁ VŽDY
                btnConfigureEstimate.Enabled = true;
                btnCalculateEstimate.Enabled = false;
                btnExportResults.Enabled = false;
                return;
            }

            switch (selectedEstimateConfStatusId)
            {
                // 100 - NENÍ KONFIGURACE
                case 100:
                    // Povolena konfigurace odhadu:
                    btnConfigureEstimate.Enabled = true;

                    // Není povolen výpočet (není nakonfigurováno z čeho počítat):
                    btnCalculateEstimate.Enabled = false;

                    // Není povolen export (není co exportovat)
                    btnExportResults.Enabled = false;

                    return;

                // 200 - JE KONFIGURACE - NENÍ ODHAD
                case 200:
                    // Povolena opakovaná konfigurace odhadu:
                    btnConfigureEstimate.Enabled = true;

                    // Povolen výpočet:
                    btnCalculateEstimate.Enabled = true;

                    // Není povolen export (není co exportovat)
                    btnExportResults.Enabled = false;

                    return;

                // 300 - JE KONFIGURACE - JE ODHAD
                case 300:
                    // Povolena opakovaná konfigurace odhadu:
                    btnConfigureEstimate.Enabled = true;

                    // Povolen přepočet existujícího odhadu:
                    btnCalculateEstimate.Enabled = true;

                    // Povolen export:
                    btnExportResults.Enabled = true;

                    return;

                // NĚKDE JE NĚCO ŠPATNĚ - NEZNÁMÁ HODNOTA STAVU
                default:
                    btnConfigureEstimate.Visible = false;
                    btnConfigureEstimate.Enabled = false;

                    btnCalculateEstimate.Visible = false;
                    btnCalculateEstimate.Enabled = false;

                    btnExportResults.Visible = false;
                    btnExportResults.Enabled = false;

                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Výběr sloupců tabulky</para>
        /// <para lang="en">Table column selection</para>
        /// </summary>
        private void FilterColumns()
        {
            FormFilterColumn form =
                new(controlOwner: this);

            if (form.ShowDialog() == DialogResult.OK)
            {
                switch (DisplayType)
                {
                    case DisplayType.Total:
                        VisibleColumnsTotal = form.VisibleColumns;
                        break;

                    case DisplayType.Ratio:
                        VisibleColumnsRatio = form.VisibleColumns;
                        break;

                    default:
                        throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
                }
            }
        }

        #endregion Display methods


        #region Check methods

        /// <summary>
        /// <para lang="cs">
        /// Provede kontrolu systémových prostředů před spuštěním konfigurace nebo výpočtu
        /// </para>
        /// <para lang="en">
        /// Performs a check of system resources before running configuration or calculation
        /// </para>
        /// </summary>
        /// <param name="configuration">
        /// <para lang="cs">
        /// Kontrola systémových prostředů je pro konfigurace (true) nebo pro výpočty (false)
        /// </para>
        /// <para lang="en">
        /// Check of system resources is for configurations (true) or for calculations (false)
        /// </para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrací true: je možné spustit konfiguraci nebo výpočet,
        /// false: není možné spustit konfiguraci nebo výpočet
        /// </para>
        /// <para lang="en">
        /// Returns true: it is possible to run the configuration or calculation,
        /// false: it is not possible to run the configuration or calculation
        /// </para>
        /// </returns>
        private bool CheckSystemResourcesAvailability(bool configuration)
        {
            FnSystemUtilizationJson systemUtilization =
                NfiEstaFunctions.FnSystemUtilization.Execute(database: Database);

            if (systemUtilization == null)
            {
                Setting.ConfigurationAvailableThreadsNumber =
                    Setting.ConfigurationRequiredThreadsNumber;

                Setting.EstimateAvailableThreadsNumber =
                    Setting.EstimateRequiredThreadsNumber;

                // "Uložená procedura fn_system_utilization vrátila NULL."
                MessageBox.Show(
                    text: msgFnSystemUtilizationIsNull,
                    caption: String.Empty,
                    icon: MessageBoxIcon.Information,
                    buttons: MessageBoxButtons.OK);
            }

            else if (systemUtilization.Load1Min == null)
            {
                Setting.ConfigurationAvailableThreadsNumber =
                    Setting.ConfigurationRequiredThreadsNumber;

                Setting.EstimateAvailableThreadsNumber =
                    Setting.EstimateRequiredThreadsNumber;

                // "Hodnotu load_1_min se nepodařilo získat z výsledku uložené procedury fn_system_utilization."
                //MessageBox.Show(
                //    text: msgFnSystemUtilizationLoadMinIsNull,
                //    caption: String.Empty,
                //    icon: MessageBoxIcon.Information,
                //    buttons: MessageBoxButtons.OK);
            }

            else if (systemUtilization.CPUCount == null)
            {
                Setting.ConfigurationAvailableThreadsNumber =
                    Setting.ConfigurationRequiredThreadsNumber;

                Setting.EstimateAvailableThreadsNumber =
                    Setting.EstimateRequiredThreadsNumber;

                // "Hodnotu cpu_count se nepodařilo získat z výsledku uložené procedury fn_system_utilization."
                MessageBox.Show(
                    text: msgFnSystemUtilizationCPUCountIsNull,
                    caption: String.Empty,
                    icon: MessageBoxIcon.Information,
                    buttons: MessageBoxButtons.OK);
            }

            else
            {
                int limit = (int)((double)systemUtilization.CPUCount - (double)systemUtilization.Load1Min);

                if (limit < 1)
                {
                    limit = 1;

                    // "Vysoké zatížení systému."
                    MessageBox.Show(
                            text: msgFnSystemUtilizationHighLoad,
                            caption: String.Empty,
                            icon: MessageBoxIcon.Information,
                            buttons: MessageBoxButtons.OK);
                }

                Setting.ConfigurationAvailableThreadsNumber =
                    (limit >= Setting.ConfigurationRequiredThreadsNumber)
                        ? Setting.ConfigurationRequiredThreadsNumber
                        : limit;

                Setting.EstimateAvailableThreadsNumber =
                    (limit >= Setting.EstimateRequiredThreadsNumber)
                        ? Setting.EstimateRequiredThreadsNumber
                        : limit;
            }

            if (configuration)
            {
                if (Setting.ConfigurationAvailableThreadsNumber < Setting.ConfigurationRequiredThreadsNumber)
                {
                    // "Požadovaný počet připojení pro konfiguraci odhadů přesahuje počet dostupných připojení"
                    return
                      DialogResult.OK ==
                          MessageBox.Show(
                              text: msgCheckSystemResourcesAvailabilityForConfiguration
                                .Replace("$0", Environment.NewLine)
                                .Replace("$1", Setting.ConfigurationRequiredThreadsNumber.ToString())
                                .Replace("$2", Setting.ConfigurationAvailableThreadsNumber.ToString()),
                              caption: String.Empty,
                              icon: MessageBoxIcon.Information,
                              buttons: MessageBoxButtons.OKCancel);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (Setting.EstimateAvailableThreadsNumber < Setting.EstimateRequiredThreadsNumber)
                {
                    // "Požadovaný počet připojení pro výpočet odhadů přesahuje počet dostupných připojení"
                    return
                      DialogResult.OK ==
                          MessageBox.Show(
                              text: msgCheckSystemResourcesAvailabilityForCalculation
                                .Replace("$0", Environment.NewLine)
                                .Replace("$1", Setting.EstimateRequiredThreadsNumber.ToString())
                                .Replace("$2", Setting.EstimateAvailableThreadsNumber.ToString()),
                              caption: String.Empty,
                              icon: MessageBoxIcon.Information,
                              buttons: MessageBoxButtons.OKCancel);
                }
                else
                {
                    return true;
                }
            }
        }


        /// <summary>
        /// <para lang="cs">Kontrola, zda zobrazená množina odhadů obsahuje pouze odhady s povoleným status pro konfiguraci</para>
        /// <para lang="en">Checks if displayed estimate set contains only estimates with enabled status for configuration</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckEstimateConfStatusForConfiguration(
            IEnumerable<DataRow> dtEstimates,
            bool verbose = true)
        {
            // Povolený status odhadů pro konfiguraci
            // Konfiguraci je možné provést na odhadech status 100:
            //      nemají provedenou konfiguraci - nová konfigurace
            // Konfiguraci je možné provést na odhadech status = 200 nebo 300:
            //      mají provedenou konfiguraci - nová konfigurace pro jinou sadu panelů
            // Enabled estimation status for configuration
            // Configuration can be done on status estimates of 100
            //      not configured - new configuration
            // Configuration can be done on estimates of status = 200 or 300
            //      have configuration done - new configuration for different set of panels
            int[] allowedStatus = [
                    (int) EstimateConfStatusEnum.NoConfiguration,
                    (int) EstimateConfStatusEnum.Configured,
                    (int) EstimateConfStatusEnum.Calculated];

            switch (DisplayType)
            {
                case DisplayType.Total:
                    var totalEstimates = dtEstimates
                        .Select(a => new
                        {
                            EstimateConfStatusId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateList.ColEstimateConfStatusId.Name) ?? 0
                        });

                    if (totalEstimates.All(a => allowedStatus.Contains(value: a.EstimateConfStatusId)))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                text: msgNewConfigurationExists,
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                case DisplayType.Ratio:
                    var ratioEstimates = dtEstimates
                        .Select(a => new
                        {
                            EstimateConfStatusId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColEstimateConfStatusId.Name) ?? 0
                        });

                    if (ratioEstimates.All(a => allowedStatus.Contains(value: a.EstimateConfStatusId)))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                text: msgNewConfigurationExists,
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola, zda zobrazená množina odhadů obsahuje pouze jednofázové odhady</para>
        /// <para lang="en">Checks if displayed estimate set contains only one phase estimates</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckOnlySinglePhaseEstimates(
            IEnumerable<DataRow> dtEstimates,
            bool verbose = true)
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    var totalEstimates = dtEstimates
                        .Select(a => new
                        {
                            PhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name) ?? 0
                        });

                    if (totalEstimates.All(a => a.PhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.SinglePhase))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            // Nové konfigurace pouze pro jedno období
                            // New configurations for one period only
                            MessageBox.Show(
                                text: msgNewConfigurationIsNotOnePhase,
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                case DisplayType.Ratio:
                    var ratioEstimates = dtEstimates
                        .Select(a => new
                        {
                            NumeratorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) ?? 0,
                            DenominatorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) ?? 0
                        });

                    if (ratioEstimates.All(a =>
                        (a.NumeratorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.SinglePhase) &&
                        (a.DenominatorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.SinglePhase)))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            // Nové konfigurace pouze pro jedno období
                            // New configurations for one period only
                            MessageBox.Show(
                                 text: msgNewConfigurationIsNotOnePhase,
                                 caption: String.Empty,
                                 buttons: MessageBoxButtons.OK,
                                 icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola, zda zobrazená množina odhadů obsahuje pouze dvoufázové regresní odhady</para>
        /// <para lang="en">Checks if displayed estimate set contains only two phase regression estimates</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckOnlyGRegMapEstimates(
            IEnumerable<DataRow> dtEstimates,
            bool verbose = true)
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    var totalEstimates = dtEstimates
                        .Select(a => new
                        {
                            PhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name) ?? 0
                        });

                    if (totalEstimates.All(a => a.PhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.GRegMap))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                text: msgNewConfigurationIsNotRegPhase,
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                case DisplayType.Ratio:
                    var ratioEstimates = dtEstimates
                        .Select(a => new
                        {
                            NumeratorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) ?? 0,
                            DenominatorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) ?? 0
                        });

                    if (ratioEstimates.All(a =>
                        (a.NumeratorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.GRegMap) &&
                        (a.DenominatorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.GRegMap)))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                 text: msgNewConfigurationIsNotRegPhase,
                                 caption: String.Empty,
                                 buttons: MessageBoxButtons.OK,
                                 icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola, zda zobrazená množina odhadů obsahuje pouze smíšené odhady - čitatel regresní, jmenovatel jednofázový</para>
        /// <para lang="en">Checks if displayed estimate set contains only mixed estimates - numerator GRegMap estimate and denominator single estimate</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckOnlyGRegMapToSingleEstimates(
            IEnumerable<DataRow> dtEstimates,
            bool verbose = true)
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    if (verbose)
                    {
                        MessageBox.Show(
                            text: msgNewConfigurationIsNotMixedPhase,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                    return false;

                case DisplayType.Ratio:
                    var ratioEstimates = dtEstimates
                        .Select(a => new
                        {
                            NumeratorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) ?? 0,
                            DenominatorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) ?? 0
                        });

                    if (ratioEstimates.All(a =>
                        (a.NumeratorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.GRegMap) &&
                        (a.DenominatorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.SinglePhase)))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                 text: msgNewConfigurationIsNotMixedPhase,
                                 caption: String.Empty,
                                 buttons: MessageBoxButtons.OK,
                                 icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola, zda zobrazená množina odhadů obsahuje pouze smíšené odhady - čitatel jednofázový, jmenovatel regresní</para>
        /// <para lang="en">Checks if displayed estimate set contains only mixed estimates - numerator single estimate and denominator GRegMap estimate</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckOnlySingleToGRegMapEstimates(
            IEnumerable<DataRow> dtEstimates,
            bool verbose = true)
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    if (verbose)
                    {
                        MessageBox.Show(
                            text: msgNewConfigurationIsNotMixedPhase,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                    return false;

                case DisplayType.Ratio:
                    var ratioEstimates = dtEstimates
                        .Select(a => new
                        {
                            NumeratorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) ?? 0,
                            DenominatorPhaseEstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) ?? 0
                        });

                    if (ratioEstimates.All(a =>
                        (a.NumeratorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.SinglePhase) &&
                        (a.DenominatorPhaseEstimateTypeId == (int)PhaseEstimateTypeEnum.GRegMap)))
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                 text: msgNewConfigurationIsNotMixedPhase,
                                 caption: String.Empty,
                                 buttons: MessageBoxButtons.OK,
                                 icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola zda zobrazená množina odhadů obsahuje pouze odhady z jednoho výpočetního období</para>
        /// <para lang="en">Checks if displayed estimate set contains only estimates from one estimation period</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="GREGEstimate">
        /// <para lang="cs">Test je pro GREG-map odhad</para>
        /// <para lang="en">Test is for GREG-map estimate</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckOnlyOneEstimationPeriod(
            IEnumerable<DataRow> dtEstimates,
            bool GREGEstimate = false,
            bool verbose = true)
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    var totalEstimates = dtEstimates
                        .GroupBy(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColEstimationPeriodId.Name) ?? 0,
                                (key, g) => new { Id = key, Vals = g.ToList() });
                    if (totalEstimates.Count() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                    text: GREGEstimate
                                            ? msgNewConfigurationIsNotOnePeriodGREG
                                            : msgNewConfigurationIsNotOnePeriod,
                                    caption: String.Empty,
                                    buttons: MessageBoxButtons.OK,
                                    icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                case DisplayType.Ratio:
                    var ratioEstimates = dtEstimates
                       .GroupBy(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColEstimationPeriodId.Name) ?? 0,
                            (key, g) => new { Id = key, Vals = g.ToList() });
                    if (ratioEstimates.Count() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                    text: GREGEstimate
                                            ? msgNewConfigurationIsNotOnePeriodGREG
                                            : msgNewConfigurationIsNotOnePeriod,
                                    caption: String.Empty,
                                    buttons: MessageBoxButtons.OK,
                                    icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola zda zobrazená množina odhadů obsahuje pouze odhady z jedné kolekce výpočetních buněk</para>
        /// <para lang="en">Checks if displayed estimate set contains only estimates from one estimation cell collection</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckOnlyOneEstimationCellCollection(
            IEnumerable<DataRow> dtEstimates,
            bool verbose = true)
        {
            switch (DisplayType)
            {
                case DisplayType.Total:
                    var totalEstimates = dtEstimates
                        .GroupBy(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColEstimationCellCollectionId.Name) ?? 0,
                            (key, g) => new { Id = key, Vals = g.ToList() });

                    if (totalEstimates.Count() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                                text: msgNewConfigurationIsNotOneEstCellCollection,
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                case DisplayType.Ratio:
                    var ratioEstimates = dtEstimates
                        .GroupBy(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColEstimationCellCollectionId.Name) ?? 0,
                            (key, g) => new { Id = key, Vals = g.ToList() });

                    if (ratioEstimates.Count() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        if (verbose)
                        {
                            MessageBox.Show(
                               text: msgNewConfigurationIsNotOneEstCellCollection,
                               caption: String.Empty,
                               buttons: MessageBoxButtons.OK,
                               icon: MessageBoxIcon.Information);
                        }
                        return false;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola zda existuje výpočetní období</para>
        /// <para lang="en">Checks if estimation period exists</para>
        /// </summary>
        /// <param name="estimationPeriodId">
        /// <para lang="cs">Identifikační číslo výpočetního období</para>
        /// <para lang="en">Estimation period identifier</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckEstimationPeriodExists(
            int estimationPeriodId,
            bool verbose = true)
        {
            EstimationPeriod estimationPeriod =
                Database.SNfiEsta.CEstimationPeriod[estimationPeriodId];

            if (estimationPeriod != null)
            {
                return true;
            }
            else
            {
                if (verbose)
                {
                    // Neexistuje záznam v tabulce c_estimation_period
                    // Record in database table c_estimation_period doesn't exist
                    MessageBox.Show(
                         text: msgNoEstimationPeriod
                            .Replace(oldValue: "$1", newValue: estimationPeriodId.ToString()),
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                }
                return false;
            }
        }

        /// <summary>
        /// <para lang="cs">Kontrola zda zobrazená množina odhadů vůbec něco obsahuje</para>
        /// <para lang="en">Check if the displayed set of estimates contains anything at all</para>
        /// </summary>
        /// <param name="cteEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckIsNotEmptyEstimateList(
            IEnumerable<DataRow> cteEstimates,
            bool verbose = true)
        {
            if (cteEstimates.Any())
            {
                return true;
            }
            else
            {
                if (verbose)
                {
                    MessageBox.Show(
                        text: msgNewConfigurationNoneSelected,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Kontrola, zda pro každé stratum existuje nějaká konfigurace jednofázových odhadů
        /// (existuje nějaká skupina panelů a referenčních období)
        /// </para>
        /// <para lang="en">
        /// Check if there is any configuration of one-phase estimates for each stratum
        /// (there is some group of panels and reference periods)
        /// </para>
        /// </summary>
        /// <param name="onePGroupsForGRegMap">
        /// <para lang="cs">
        /// Seznam panelů a referenčních období (po stratech),
        /// pro které již byly nakonfigurované jednofázové odhady
        /// </para>
        /// <para lang="en">
        /// List of panels and reference periods (by strata)
        /// for which one-phase estimates have already been configured
        /// </para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckCompleteGroupForEachStratum(
            TFnApiGetOnePGroupsForGRegMapList onePGroupsForGRegMap,
            bool verbose = true)
        {
            if ((onePGroupsForGRegMap == null) ||
                (onePGroupsForGRegMap.Items.Count == 0))
            {
                if (verbose)
                {
                    MessageBox.Show(
                        text: msgIncompleteGroupForEachStratum
                                .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }

            // Skupiny panelů a referenčních období, rozdělených po kombinacích strat
            var groupsByStrata =
                onePGroupsForGRegMap.Items
                .GroupBy(a => new
                {
                    a.CountryId,
                    a.StrataSetId,
                    a.StratumId,
                })
                .Select(a => new
                {
                    a.Key.CountryId,
                    a.Key.StrataSetId,
                    a.Key.StratumId,

                    // Nová verze:
                    // Do not consider the values of complete_group
                    Number = a.Count()

                    // Původní verze:
                    // Počet kompletních skupin panelů a referenčních období ve stratu
                    // Number = a.Count(b => b.CompleteGroup ?? false)
                });

            if (groupsByStrata.All(a => a.Number > 0))
            {
                // Pokud, každé stratum má alespoň jednu kompletní skupinu panelů a referenčních období
                return true;
            }
            else
            {
                if (verbose)
                {
                    MessageBox.Show(
                        text: msgIncompleteGroupForEachStratum
                                .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Kontrola zda existuje alespoň jedna kombinace modelu,
        /// sigma a typu parametrizační oblasti
        /// </para>
        /// <para lang="en">
        /// Check if there is at least one combination of model,
        /// sigma and parameterization area type
        /// </para>
        /// </summary>
        /// <param name="dtModelsSigmaParamTypes">
        /// <para lang="cs">
        /// Seznam dostupných kombinací pracovního modelu, sigma a typu parametrizační oblasti
        /// </para>
        /// <para lang="en">
        /// List of alternative combinations of working model, sigma and parametrization area type
        /// </para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckCompleteModelsSigmaParamAreaTypes(
            TFnApiGetWModelsSigmaParamTypesList modelsSigmaParamTypes,
            bool verbose = true)
        {
            if ((modelsSigmaParamTypes == null) ||
                (modelsSigmaParamTypes.Items.Count == 0))
            {
                if (verbose)
                {
                    MessageBox.Show(
                        text:
                            msgNoSuitableModelSigmaParam
                            .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// <para lang="cs">
        /// Kontrola, zda existují nějaké páry konfigurací
        /// odpovídajích regresních odhadů úhrnů pro podíl
        /// </para>
        /// <para lang="en">
        /// Check if there are any matching pair
        /// of GREG-map total configs.
        /// </para>
        /// </summary>
        /// <param name="gRegMapConfigsForRatio">
        /// <para lang="cs">
        /// Seznam platných kombinací skupin panelů a referenčních období, typu parametrizační oblasti
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů
        /// </para>
        /// <para lang="en">
        /// List of valid combinations of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals
        /// </para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckExistMatchingGRegMapTotals(
            TFnApiGetGRegMapConfigsForRatioList gRegMapConfigsForRatio,
            bool verbose = true)
        {
            if ((gRegMapConfigsForRatio == null) ||
                (gRegMapConfigsForRatio.Items.Count == 0))
            {
                if (verbose)
                {
                    // Node no. 40
                    // Zobrazení chybové zprávy
                    MessageBox.Show(
                        text: msgNotExistMatchingGRegMapTotals
                            .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// <para lang="cs">
        /// Kontrola, zda existují nějaké páry konfigurací
        /// odpovídajích regresních a jednofázových odhadů úhrnů pro podíl
        /// </para>
        /// <para lang="en">
        /// Check if there are any matching pair
        /// of GREG-map and single-phase total config.
        /// </para>
        /// </summary>
        /// <param name="gRegMapToSingleConfigsForRatio">
        /// <para lang="cs">
        /// Seznam platných kombinací skupin panelů a referenčních období, typu parametrizační oblasti
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů v čitateli
        /// a odpovídajících jednofázových odhadů úhrnu ve jmenovateli.
        /// </para>
        /// <para lang="en">
        /// List of combinations of panel-refyearset group, type of parametrisation area, force_synthetic
        /// working model and sigma of already configured GREG-map (numerator)
        /// and the corresponding single-phase (denominator) total estimates.
        /// </para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckExistMatchingGRegMapToSingleTotals(
            TFnApiGetGRegMapToSingleConfigsForRatioList gRegMapToSingleConfigsForRatio,
            bool verbose = true)
        {
            if ((gRegMapToSingleConfigsForRatio == null) ||
                (gRegMapToSingleConfigsForRatio.Items.Count == 0))
            {
                if (verbose)
                {
                    // Node no. 46
                    // Zobrazení chybové zprávy
                    MessageBox.Show(
                        text: msgNotExistMatchingGRegMapToSingleTotals
                            .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// <para lang="cs">
        /// Kontrola, zda existují nějaké páry konfigurací
        /// odpovídajích regresních a jednofázových odhadů úhrnů pro podíl
        /// </para>
        /// <para lang="en">
        /// Check if there are any matching pair
        /// of GREG-map and single-phase total config.
        /// </para>
        /// </summary>
        /// <param name="singleToGRegMapConfigsForRatio">
        /// <para lang="cs">
        /// Seznam platných kombinací skupin panelů a referenčních období, typu parametrizační oblasti
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů ve jmenovateli
        /// a odpovídajících jednofázových odhadů úhrnu v čitateli.
        /// </para>
        /// <para lang="en">
        /// List of combinations of panel-refyearset group, type of parametrisation area, force_synthetic
        /// working model and sigma of already configured GREG-map (denominator)
        /// and the corresponding single-phase (numerator) total estimates.
        /// </para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox s výsledkem kontroly</para>
        /// <para lang="en">Display MessageBox with check result</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool CheckExistMatchingSingleToGRegMapTotals(
            TFnApiGetSingleToGRegMapConfigsForRatioList singleToGRegMapConfigsForRatio,
            bool verbose = true)
        {
            if ((singleToGRegMapConfigsForRatio == null) ||
                (singleToGRegMapConfigsForRatio.Items.Count == 0))
            {
                if (verbose)
                {
                    // Node no. 46
                    // Zobrazení chybové zprávy
                    MessageBox.Show(
                        text: msgNotExistMatchingSingleToGRegMapTotals
                            .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return false;
            }

            return true;
        }

        #endregion Check methods


        #region Configuration methods

        /// <summary>
        /// <para lang="cs">Vytvoření nové konfigurace odhadu</para>
        /// <para lang="en">Create a new estimate configuration</para>
        /// </summary>
        private void AddNewEstimateConfiguration()
        {
            // Datová tabulka s odhady vybranými pro konfiguraci
            // Data table with estimates selected for configuration
            IEnumerable<DataRow> dtEstimates;

            switch (DisplayType)
            {
                // Node no.6: Konfigurace úhrnů
                case DisplayType.Total:
                    dtEstimates = DataSourceTotal.Data.DefaultView.ToTable().AsEnumerable();

                    // Konfigurace úhrnů jednofázových odhadů
                    if (CheckOnlySinglePhaseEstimates(
                        dtEstimates: dtEstimates,
                        verbose: false))
                    {
                        AddNewSinglePhaseTotalEstimateConfiguration(
                            dtEstimates: dtEstimates);
                        return;
                    }

                    // Node no.8: Konfigurace úhrnů regresních odhadů
                    else if (CheckOnlyGRegMapEstimates(
                        dtEstimates: dtEstimates,
                        verbose: false))
                    {
                        AddNewGRegMapTotalEstimateConfiguration(
                            dtEstimates: dtEstimates);
                        return;
                    }

                    // Node no.9
                    else
                    {
                        MessageBox.Show(
                            text: msgNewConfigurationIsNotSingleTypePhase,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);

                        return;
                    }

                // Node no.6: Konfigurace podílů
                case DisplayType.Ratio:
                    dtEstimates = DataSourceRatio.Data.DefaultView.ToTable().AsEnumerable();

                    // Konfigurace podílů jednofázových odhadů
                    if (CheckOnlySinglePhaseEstimates(
                        dtEstimates: dtEstimates,
                        verbose: false))
                    {
                        AddNewSinglePhaseRatioEstimateConfiguration(
                            dtEstimates: dtEstimates);
                        return;
                    }

                    // Konfigurace podílů regresních odhadů
                    else if (CheckOnlyGRegMapEstimates(
                        dtEstimates: dtEstimates,
                        verbose: false))
                    {
                        AddNewGRegMapRatioEstimateConfiguration(
                            dtEstimates: dtEstimates);
                        return;
                    }

                    // Konfigurace podílů smíšených odhadů (čitatel regresní, jmenovatel jednoduchý)
                    else if (CheckOnlyGRegMapToSingleEstimates(
                        dtEstimates: dtEstimates,
                        verbose: false))
                    {
                        AddNewGRegMapToSingleRatioEstimateConfiguration(
                            dtEstimates: dtEstimates);
                        return;
                    }

                    // Konfigurace podílů smíšených odhadů (čitatel regresní, jmenovatel jednoduchý)
                    else if (CheckOnlySingleToGRegMapEstimates(
                        dtEstimates: dtEstimates,
                        verbose: false))
                    {
                        AddNewSingleToGRegMapRatioEstimateConfiguration(
                            dtEstimates: dtEstimates);
                        return;
                    }

                    else
                    {
                        MessageBox.Show(
                            text: msgNewConfigurationIsNotOnePeriod,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);

                        return;
                    }

                default:
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value.");
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové konfigurace odhadu úhrnu (jednofázové odhady)</para>
        /// <para lang="en">Create a new total estimate configuration (one-phase estimates)</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        private void AddNewSinglePhaseTotalEstimateConfiguration(
            IEnumerable<DataRow> dtEstimates)
        {
            // Node no.1:
            // dtEstimates obsahuje aktuální množinu odhadů zobrazenou ve výpočetním modulu

            // Kontrola z Node no.2 a případné zobrazení zprávy z Node no.3:
            if (!CheckOnlySinglePhaseEstimates(dtEstimates: dtEstimates, verbose: true)) { return; }

            // Kontrola z Node no.4 a případné zobrazení zprávy z Node no.5:
            if (!CheckOnlyOneEstimationPeriod(dtEstimates: dtEstimates, GREGEstimate: false, verbose: true)) { return; }

            // Kontrola povolený status odhadu
            int[] allowedStatus = [
                    (int) EstimateConfStatusEnum.NoConfiguration,
                    (int) EstimateConfStatusEnum.Configured,
                    (int) EstimateConfStatusEnum.Calculated];

            if (!CheckEstimateConfStatusForConfiguration(dtEstimates: dtEstimates, verbose: true)) { return; }

            IEnumerable<DataRow> cteEstimates = dtEstimates
                .Where(a => allowedStatus.Contains(value: a.Field<int>(OLAPTotalEstimateList.ColEstimateConfStatusId.Name)))
                .Where(a => a.Field<int>(columnName: OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.SinglePhase);

            // Kontrola zobrazený seznam odhadů pro konfiguraci není prázdný
            if (!CheckIsNotEmptyEstimateList(cteEstimates: cteEstimates)) { return; }

            #region Panels for estimation cells

            // Seznam výpočetních buněk se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> estimationCellsIds = cteEstimates
                .Where(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColEstimationCellId.Name) != null)
                .Select(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColEstimationCellId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových kategorií se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> variablesIds = cteEstimates
                .Where(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColVariableId.Name) != null)
                .Select(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Období je stejné pro všechny odhady vybrané pro konfiguraci
            // načte se z prvního řádku:
            OLAPTotalEstimate firstEstimate = new(data: cteEstimates.FirstOrDefault());

            // Počátek a konec období odhadu se získá z prvního odhadu:
            if (!CheckEstimationPeriodExists(estimationPeriodId: firstEstimate.EstimationPeriodId)) { return; }
            EstimationPeriod estimationPeriod =
                Database.SNfiEsta.CEstimationPeriod[firstEstimate.EstimationPeriodId];

            // Node no.6 and Node no.7
            // Zapnout výpis Warning a Notice
            Setting.DBSuppressWarning = false;
            Database.Postgres.ExceptionFlag = false;

            // Node no.5
            // Get list of countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell
            // and for which all required variables are available
            TFnApiGetDataOptionsForSinglePhaseConfigList panelReferenceYearSetPairs =
                NfiEstaFunctions.FnApiGetDataOptionsForSinglePhaseConfig.Execute(
                    database: Database,
                    estimationCellsIds: estimationCellsIds,
                    variablesIds: variablesIds,
                    estimateDateBegin: estimationPeriod?.EstimateDateBegin,
                    estimateDateEnd: estimationPeriod?.EstimateDateEnd,
                    cellCoverageTolerance: Setting.CellCoverageTolerance);

            // Node no.8
            // Any exception from the function fn_api_get_data_options4single_phase_config?
            Setting.DBSuppressWarning = true;
            if (Database.Postgres.ExceptionFlag)
            {
                // Node no.9
                // Terminate the configuration process.
                return;
            }

            #endregion Panels for estimation cells

            // Node no.10 a no 11
            // Display all pairs of panels and refyearsets within each country, strata set and stratum.
            // Let user chose a set of panel and refrence-year set combinations within each country, strata set and stratum.
            FormEstimateConfiguration frmEstimateConfiguration =
                new(controlOwner: this)
                {
                    PanelReferenceYearSetPairs = panelReferenceYearSetPairs,
                    EstimationPeriod = estimationPeriod
                };

            if (frmEstimateConfiguration.ShowDialog() == DialogResult.OK)
            {
                StartConfigurationTotalsThreads(
                    cteEstimates: cteEstimates,
                    aggregatedEstimationCellGroups: frmEstimateConfiguration.AggregatedEstimationCellGroups);
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové konfigurace odhadu podílu (jednofázové odhady)</para>
        /// <para lang="en">Create a new ratio estimate configuration (one-phase estimates)</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        private void AddNewSinglePhaseRatioEstimateConfiguration(
            IEnumerable<DataRow> dtEstimates)
        {
            // Node no.1:
            // dtEstimates obsahuje aktuální množinu odhadů zobrazenou ve výpočetním modulu

            // Kontrola z Node no.2 a případné zobrazení zprávy z Node no.3:
            if (!CheckOnlySinglePhaseEstimates(dtEstimates: dtEstimates, verbose: true)) { return; }

            // Kontrola z Node no.4 a případné zobrazení zprávy z Node no.5:
            if (!CheckOnlyOneEstimationPeriod(dtEstimates: dtEstimates, GREGEstimate: false, verbose: true)) { return; }

            // Kontrola povolený status odhadu
            int[] allowedStatus = [
                    (int) EstimateConfStatusEnum.NoConfiguration,
                    (int) EstimateConfStatusEnum.Configured,
                    (int) EstimateConfStatusEnum.Calculated];

            if (!CheckEstimateConfStatusForConfiguration(dtEstimates: dtEstimates, verbose: true)) { return; }

            IEnumerable<DataRow> cteEstimates = dtEstimates
                 .Where(a => allowedStatus.Contains(value: a.Field<int>(OLAPRatioEstimateList.ColEstimateConfStatusId.Name)))
                 .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.SinglePhase)
                 .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.SinglePhase);

            // Kontrola zobrazený seznam odhadů pro konfiguraci není prázdný
            if (!CheckIsNotEmptyEstimateList(cteEstimates: cteEstimates)) { return; }

            #region Panels for estimation cells

            // Seznam výpočetních buněk se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> estimationCellsIds = cteEstimates
                .Where(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColEstimationCellId.Name) != null)
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColEstimationCellId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových kategorií čitatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> numeratorVariablesIds = cteEstimates
                .Where(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColNumeratorVariableId.Name) != null)
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColNumeratorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových kategorií jmenovatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> denominatorVariablesIds = cteEstimates
                .Where(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColDenominatorVariableId.Name) != null)
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColDenominatorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // In case of ratios of single-phase totals include variables
            // of numerator as well as denominator in the input parameter _variables INT[].
            List<Nullable<int>> variablesIds =
                numeratorVariablesIds.Concat(second: denominatorVariablesIds)
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Období je stejné pro všechny odhady vybrané pro konfiguraci
            // načte se z prvního řádku:
            OLAPRatioEstimate firstEstimate = new(data: cteEstimates.FirstOrDefault());

            // Počátek a konec období odhadu se získá z prvního odhadu:
            if (!CheckEstimationPeriodExists(estimationPeriodId: firstEstimate.EstimationPeriodId)) { return; }
            EstimationPeriod estimationPeriod =
                Database.SNfiEsta.CEstimationPeriod[firstEstimate.EstimationPeriodId];

            // Node no.6 and Node no.7
            // Zapnout výpis Warning a Notice
            Setting.DBSuppressWarning = false;
            Database.Postgres.ExceptionFlag = false;

            // Node no.5
            // Get list of countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell
            // and for which all required variables are available
            TFnApiGetDataOptionsForSinglePhaseConfigList panelReferenceYearSetPairs =
                NfiEstaFunctions.FnApiGetDataOptionsForSinglePhaseConfig.Execute(
                    database: Database,
                    estimationCellsIds: estimationCellsIds,
                    variablesIds: variablesIds,
                    estimateDateBegin: estimationPeriod?.EstimateDateBegin,
                    estimateDateEnd: estimationPeriod?.EstimateDateEnd,
                    cellCoverageTolerance: Setting.CellCoverageTolerance);

            // Node no.8
            // Any exception from the function fn_api_get_data_options4single_phase_config?
            Setting.DBSuppressWarning = true;
            if (Database.Postgres.ExceptionFlag)
            {
                // Node no.9
                // Terminate the configuration process.
                return;
            }

            #endregion Panels for estimation cells

            // Node no.10 a no.11
            // Display all pairs of panels and refyearsets within each country, strata set and stratum.
            // Let user chose a set of panel and refrence-year set combinations within each country, strata set and stratum.
            FormEstimateConfiguration frmEstimateConfiguration =
                new(controlOwner: this)
                {
                    PanelReferenceYearSetPairs = panelReferenceYearSetPairs,
                    EstimationPeriod = estimationPeriod
                };

            if (frmEstimateConfiguration.ShowDialog() == DialogResult.OK)
            {
                StartConfigurationRatiosThreads(
                    cteEstimates: cteEstimates,
                    aggregatedEstimationCellGroups: frmEstimateConfiguration.AggregatedEstimationCellGroups);
            }
        }


        /// <summary>
        /// <para lang="cs">Vytvoření nové konfigurace odhadu úhrnu (regresní odhady)</para>
        /// <para lang="en">Create a new total estimate configuration (two-phase regression estimates)</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        private void AddNewGRegMapTotalEstimateConfiguration(
            IEnumerable<DataRow> dtEstimates)
        {
            if (!CheckEstimateConfStatusForConfiguration(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Node no.1:
            // dtEstimates obsahuje aktuální množinu odhadů zobrazenou ve výpočetním modulu

            // Kontrola z Node no.2 a případné zobrazení zprávy z Node no.3:
            if (!CheckOnlyOneEstimationCellCollection(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Kontrola z Node no.4 a případné zobrazení zprávy z Node no.5
            if (!CheckOnlyOneEstimationPeriod(
                dtEstimates: dtEstimates,
                GREGEstimate: true,
                verbose: true))
            { return; }

            // Rozhodování z Node no.6 (úhrn nebo podíl)
            // bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze regresní odhady úhrnů

            // Kontrola z Node no.8 a případné zobrazení zprávy z Node no.9
            if (!CheckOnlyGRegMapEstimates(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            int[] allowedStatus = [
                    (int) EstimateConfStatusEnum.NoConfiguration,
                    (int) EstimateConfStatusEnum.Configured,
                    (int) EstimateConfStatusEnum.Calculated ];

            IEnumerable<DataRow> cteEstimates = dtEstimates
                .Where(a => allowedStatus.Contains(value: a.Field<int>(OLAPTotalEstimateList.ColEstimateConfStatusId.Name)))
                .Where(a => a.Field<int>(columnName: OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.GRegMap);

            // Kontrola, zda množina zobrazených odhadů není prázdná
            // a je možné něco nakonfigurovat
            if (!CheckIsNotEmptyEstimateList(
                cteEstimates: cteEstimates,
                verbose: true))
            { return; }

            // Výpočetní období (je stejné pro celou skupinu, vezme se z prvního odhadu ve skupině):
            OLAPTotalEstimate firstEstimate = new(
                data: cteEstimates.FirstOrDefault());

            // Seznam výpočetních buněk se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> estimationCellIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColEstimationCellId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových členění se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> variableIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPTotalEstimateList.ColVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

#if !(DEBUG && TEST)
            // Volání api - uložené procedury v databázi, která implementuje Node no.10
            TFnApiGetOnePGroupsForGRegMapList onePGroupsForGRegMap =
                NfiEstaFunctions.FnApiGetOnePGroupsForGRegMap.Execute(
                    database: Database,
                    estimationPeriod: firstEstimate.EstimationPeriodId,
                    estimationCells: estimationCellIdList,
                    variables: variableIdList);
#else
            // Volání api - uložené procedury v databázi, která implementuje Node no.10
            TFnApiGetOnePGroupsForGRegMapList onePGroupsForGRegMap =
                NfiEstaFunctions.FnApiGetOnePGroupsForGRegMap.Execute(
                    database: Database,
                    estimationPeriod: 1,
                    estimationCells: [44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 56, 57, 58, 62],
                    variables: [1, 2, 3]);

            // Test pro zobrazení chybové hlášky
            //TFnApiGetOnePGroupsForGRegMapList onePGroupsForGRegMap =
            //    NfiEstaFunctions.FnApiGetOnePGroupsForGRegMap.Execute(
            //        database: Database,
            //        estimationPeriod: 1,
            //        estimationCells: [44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 56, 57, 58, 62],
            //        variables: [-1]);
#endif

            // Kontrola z Node no.11 a případné zobrazení zprávy z Node no.12
            if (!CheckCompleteGroupForEachStratum(
                onePGroupsForGRegMap: onePGroupsForGRegMap,
                verbose: true))
            { return; }

            // Node no.13 zobrazení formuláře se seznamem strat
            // a odpovídajících skupin panelů a referenčních období
            FormGRegPanelRefYearSetGroup frmGRegPanelRefYearSetGroup =
                new(controlOwner: this)
                {
                    OnePGroupsForGRegMap = onePGroupsForGRegMap
                };

            // Node no.14 na Node no.15
            // Uživatel na formuláři vybral skupinu panelů a referenčních období
            // pro každé stratum a kliknul na OK (jinak konec)
            if (frmGRegPanelRefYearSetGroup.ShowDialog() != DialogResult.OK)
            {
                frmGRegPanelRefYearSetGroup.Dispose();
                return;
            }

#if !(DEBUG && TEST)
            // Volání api - uložené procedury v databázi, která implementuje Node no.16
            // a vrací: Seznam všech panelů a referenčních období ve vybraných skupinách Node no.17
            TFnApiGetPanelRefYearSetCombinationsForGroupsList panelRefYearSetCombinationsForGroups =
                NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups
                .Execute(
                    database: Database,
                    panelRefYearSetGroups: frmGRegPanelRefYearSetGroup.PanelReferenceYearSetGroupIds);
#else
            // Volání api - uložené procedury v databázi, která implementuje Node no.16
            // a vrací: Seznam všech panelů a referenčních období ve vybraných skupinách Node no.17
            TFnApiGetPanelRefYearSetCombinationsForGroupsList panelRefYearSetCombinationsForGroups =
                NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups
                .Execute(
                    database: Database,
                    panelRefYearSetGroups: [20, 23]);
#endif

#if !(DEBUG && TEST)
            // Volání api - uložené procedury v databázi, která implementuje Node no.18
            TFnApiGetWModelsSigmaParamTypesList modelsSigmaParamTypes =
                NfiEstaFunctions.FnApiGetWModelsSigmaParamTypes.Execute(
                    database: Database,
                    panels: panelRefYearSetCombinationsForGroups.PanelsIds,
                    estimationCells: estimationCellIdList);
#else
            // Volání api - uložené procedury v databázi, která implementuje Node no.18
            TFnApiGetWModelsSigmaParamTypesList modelsSigmaParamTypes =
                NfiEstaFunctions.FnApiGetWModelsSigmaParamTypes.Execute(
                    database: Database,
                    panels: [1, 2],
                    estimationCells: [55, 56, 60, 61, 67, 68, 69, 70, 75, 76, 77, 83]);

            // Test pro zobrazení chybové hlášky
            //foreach (TFnApiGetWModelsSigmaParamTypes model in modelsSigmaParamTypes.Items)
            //{
            //    model.AllCellsIncluded = false;
            //}
#endif

            // Kontroly z Node no.19 a Node no.20, a případné zobrazení zprávy z Node no.21
            if (!CheckCompleteModelsSigmaParamAreaTypes(
                modelsSigmaParamTypes: modelsSigmaParamTypes,
                verbose: true))
            {
                frmGRegPanelRefYearSetGroup.Dispose();
                return;
            }

            // Node no.22 zobrazení formuláře se seznamem modelů a typů parametrizačních oblastí
            // Formulář požaduje zvolit model, sigma a param area_type (Node no.23, Node no.24)
            // Formulář požaduje zadat volbu synthetic estimation (Node no.25, Node no.26, Node no.27)
            FormGRegModelParamAreaType frmGRegModelParamAreaType =
                new(controlOwner: this)
                {
                    ModelsSigmaParamTypes = modelsSigmaParamTypes
                };

            // Pokud formulář není uzavřen tlačítkem OK, konfigurace končí (Node no.28)
            if (frmGRegModelParamAreaType.ShowDialog() != DialogResult.OK)
            {
                frmGRegPanelRefYearSetGroup.Dispose();
                frmGRegModelParamAreaType.Dispose();
                return;
            }

#if !(DEBUG && TEST)
            // Volání api - uložené procedury v databázi, která implementuje Node no.29
            TFnApiGetGroupForPanelRefYearSetCombinationsList groupsForPanelRefYearSetCombinations =
                NfiEstaFunctions.FnApiGetGroupForPanelRefYearSetCombinations.Execute(
                    database: Database,
                    panels: panelRefYearSetCombinationsForGroups.PanelsIds,
                    refYearSets: panelRefYearSetCombinationsForGroups.ReferenceYearSetsIds);
#else
            // Volání api - uložené procedury v databázi, která implementuje Node no.29
            TFnApiGetGroupForPanelRefYearSetCombinationsList groupsForPanelRefYearSetCombinations =
                NfiEstaFunctions.FnApiGetGroupForPanelRefYearSetCombinations.Execute(
                    database: Database,
                    panels: [1, 5, 7],
                    refYearSets: [2, 3, 3]);
#endif

            // Node no.29 -> neexistuje skupina panelů a ref. období
            if (groupsForPanelRefYearSetCombinations.Items.Count == 0)
            {
                // Node no.30, Node no.31 zobrazí formulář pro zadání info o skupině panelů a ref. období
                // Node no.32 formulář se nezavře, dokud nejsou zadané unique hodnoty
                FormGRegPanelRefYearSetGroupNew frmGRegPanelRefYearSetGroupNew =
                    new(controlOwner: this);

                if (frmGRegPanelRefYearSetGroupNew.ShowDialog() != DialogResult.OK)
                {
                    frmGRegPanelRefYearSetGroup.Dispose();
                    frmGRegModelParamAreaType.Dispose();
                    frmGRegPanelRefYearSetGroupNew.Dispose();
                    return;
                }

                // Node no.33 a Node no. 34
                // Validní hodnoty se uloží do databáze voláním
                // api funkce fn_api_save_panel_ref_year_set_group
                NfiEstaFunctions.FnApiSavePanelRefYearSetGroup.Execute(
                    database: Database,
                    panels: panelRefYearSetCombinationsForGroups.PanelsIds,
                    refYearSets: panelRefYearSetCombinationsForGroups.ReferenceYearSetsIds,
                    labelCs: frmGRegPanelRefYearSetGroupNew.LabelCs,
                    descriptionCs: frmGRegPanelRefYearSetGroupNew.DescriptionCs,
                    labelEn: frmGRegPanelRefYearSetGroupNew.LabelEn,
                    descriptionEn: frmGRegPanelRefYearSetGroupNew.DescriptionEn);

                // Node no.35
                // Načte skupinu panelů a ref. období
                // (celý objekt, nejen jeho id, ale také label, aby bylo možno zobrazit
                // výsledek na formuláři souhrn před uložením konfigurace
                groupsForPanelRefYearSetCombinations =
                    NfiEstaFunctions.FnApiGetGroupForPanelRefYearSetCombinations.Execute(
                        database: Database,
                        panels: panelRefYearSetCombinationsForGroups.PanelsIds,
                        refYearSets: panelRefYearSetCombinationsForGroups.ReferenceYearSetsIds);

                frmGRegPanelRefYearSetGroupNew.Dispose();
            }

            TFnApiGetGroupForPanelRefYearSetCombinations selectedPanelReferenceYearSetGroup =
                groupsForPanelRefYearSetCombinations.Items.FirstOrDefault<TFnApiGetGroupForPanelRefYearSetCombinations>();

            // Formulář zobrazí souhrn všech provedených voleb
            FormGRegConfigurationSummary frmGRegConfigurationSummary =
               new(controlOwner: this)
               {
                   SelectedPanelReferenceYearSetGroup =
                        selectedPanelReferenceYearSetGroup,
                   SelectedModelSigmaParamType =
                        frmGRegModelParamAreaType.SelectedModelSigmaParamType
               };

            // tlačítko Cancel formuláře ukončí proces konfigurace bez uložení
            if (frmGRegConfigurationSummary.ShowDialog() != DialogResult.OK)
            {
                frmGRegPanelRefYearSetGroup.Dispose();
                frmGRegModelParamAreaType.Dispose();
                frmGRegConfigurationSummary.Dispose();
                return;
            }

            // Node no. 36 tlačítko Save formuláře spustí cyklus
            // uložení konfigurací regresních odhadů,
            // pro zvolené výpočetní buňky a atributová členění
            bool dbChanged = false;
#if !(DEBUG && TEST)
            foreach (DataRow row in cteEstimates)
            {
                OLAPTotalEstimate estimate = new(data: row);

                Database.Postgres.ExceptionFlag = false;
                Nullable<int> result =
                    NfiEstaFunctions.FnApiSaveGregMapTotalConfig.Execute(
                        database: Database,
                        estimationPeriod: estimate.EstimationPeriodId,
                        estimationCell: estimate.EstimationCellId,
                        variable: estimate.VariableId,
                        panelRefYearSetGroup: frmGRegConfigurationSummary.PanelReferenceYearSetGroupId,
                        workingModel: frmGRegConfigurationSummary.WorkingModelId,
                        sigma: frmGRegConfigurationSummary.Sigma,
                        paramAreaType: frmGRegConfigurationSummary.ParamAreaTypeId,
                        forceSynthetic: frmGRegConfigurationSummary.ForceSynthetic);
                dbChanged = true;

                if (Database.Postgres.ExceptionFlag)
                {
                    Database.Postgres.ExceptionFlag = false;
                    break;
                }
            }
#else
            // Při testování zápis do databáze se neprovádí
#endif

            // Uvolnit zdroje po formulářích
            // (jinak vyvolá chybu při ukončování aplikace)
            frmGRegPanelRefYearSetGroup.Dispose();
            frmGRegModelParamAreaType.Dispose();
            frmGRegConfigurationSummary.Dispose();

            if (dbChanged)
            {
                // Pošle zprávu pro objekt OLAP, že proběhla konfigurace
                // a je nutné, aby se přenačetl z databáze
                EstimatesConfigured?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové konfigurace odhadu podílu (regresní odhady)</para>
        /// <para lang="en">Create a new ratio estimate configuration (two-phase regression estimates)</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        private void AddNewGRegMapRatioEstimateConfiguration(
            IEnumerable<DataRow> dtEstimates)
        {
            if (!CheckEstimateConfStatusForConfiguration(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Node no.1:
            // dtEstimates obsahuje aktuální množinu odhadů zobrazenou ve výpočetním modulu

            // Kontrola z Node no.2 a případné zobrazení zprávy z Node no.3:
            if (!CheckOnlyOneEstimationCellCollection(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Kontrola z Node no.4 a případné zobrazení zprávy z Node no.5
            if (!CheckOnlyOneEstimationPeriod(
                dtEstimates: dtEstimates,
                GREGEstimate: true,
                verbose: true))
            { return; }

            // Rozhodování z Node no.6 (úhrn nebo podíl)
            // bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly

            // Rozhodování z Node no.7 (jednofázový nebo regresní podíl)
            // bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly regresních odhadů

            // Rozhodování z Node no.37 (standardní nebo smíšený regresní podíl)
            // bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly dvou regresních odhadů (standardní)

            int[] allowedStatus = [
                    (int) EstimateConfStatusEnum.NoConfiguration,
                    (int) EstimateConfStatusEnum.Configured,
                    (int) EstimateConfStatusEnum.Calculated ];

            IEnumerable<DataRow> cteEstimates = dtEstimates
                .Where(a => allowedStatus.Contains(value: a.Field<int>(OLAPRatioEstimateList.ColEstimateConfStatusId.Name)))
                .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.GRegMap)
                .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.GRegMap);

            // Kontrola, zda množina zobrazených odhadů není prázdná
            // a je možné něco nakonfigurovat
            if (!CheckIsNotEmptyEstimateList(
                cteEstimates: cteEstimates,
                verbose: true))
            { return; }

            // Výpočetní období (je stejné pro celou skupinu, vezme se z prvního odhadu ve skupině):
            OLAPRatioEstimate firstEstimate = new(
                data: cteEstimates.FirstOrDefault());

            // Seznam výpočetních buněk se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> estimationCellIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColEstimationCellId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových členění čitatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> numeratorVariableIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColNumeratorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových členění jmenovatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> denominatorVariableIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColDenominatorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

#if !(DEBUG && TEST)
            // Node no.38
            // Vyhledání existujících konfigurací regresních odhadů úhrnu,
            // které mohou být použití pro konfiguraci podílu
            TFnApiGetGRegMapConfigsForRatioList gRegMapConfigsForRatio =
                NfiEstaFunctions.FnApiGetGRegMapConfigsForRatio.Execute(
                    database: Database,
                    estimationPeriod: firstEstimate?.EstimationPeriodId,
                    estimationCells: estimationCellIdList,
                    variablesNumerator: numeratorVariableIdList,
                    variablesDenominator: denominatorVariableIdList);
#else
            // Node no.38
            // Vyhledání existujících konfigurací regresních odhadů úhrnu,
            // které mohou být použití pro konfiguraci podílu
            TFnApiGetGRegMapConfigsForRatioList gRegMapConfigsForRatio =
                NfiEstaFunctions.FnApiGetGRegMapConfigsForRatio.Execute(
                    database: Database,
                    estimationPeriod: 1,
                    estimationCells: [55, 57, 69],
                    variablesNumerator: [1, 2, 3],
                    variablesDenominator: [3, 3, 3]);

            // Test 1: nenalezeny žádné páry konfigurací:
            // gRegMapConfigsForRatio = new TFnApiGetGRegMapConfigsForRatioList(database: Database);

            // Test 2: Všechny nalezené páry jsou nekonfigurovatelné:
            //foreach (TFnApiGetGRegMapConfigsForRatio item in gRegMapConfigsForRatio.Items)
            //{
            //    item.AllEstimatesConfigurable = false;
            //}

            // Test 3: Konfigurace pro čitatel nebo jmenovatel neexistují
            //foreach (TFnApiGetGRegMapConfigsForRatio item in gRegMapConfigsForRatio.Items)
            //{
            //    item.TotalEstimateConfNumeratorText = null;
            //    item.TotalEstimateConfDenominatorText = null;
            //}

            // Test 4: Konfigurace pro čitatel nebo jmenovatel různé délky
            //foreach (TFnApiGetGRegMapConfigsForRatio item in gRegMapConfigsForRatio.Items)
            //{
            //    item.TotalEstimateConfNumeratorText = "1;2;3";
            //    item.TotalEstimateConfDenominatorText = "1;2;3;4";
            //}
#endif

            // Node no.39 a no.40
            // Byly nalezeny nějaké páry konfigurací odpovídajích regregresních úhrnů pro podíl?
            if (!CheckExistMatchingGRegMapTotals(
                gRegMapConfigsForRatio: gRegMapConfigsForRatio,
                verbose: true)) { return; }

            // Node no. 41, 42, 43
            // Formulář pro zobrazení a výběr kombinací GREG-map odhadů úhrnů
            FormGRegMapConfigsForRatio frmGRegMapConfigsForRatio =
                new(controlOwner: this)
                {
                    Combinations = gRegMapConfigsForRatio
                };

            // Node no. 44
            // Formulář zavřen tlačítkem, Save, tj. byl proveden výběr
            // provede se zápis do databáze.
            // Aby bylo možné provést zápis musí být vybraná kombinace,
            // vybraná kombinace musí mít nenulové pole konfigurací čitatele a jmenovatele,
            // a délky polí čitatele a jmenovatele musí být stejné
            // Tlačítko Save spustí cyklus uložení konfigurací regresních odhadů podílů
            // pro jednotlivé páry konfigurací čitalele a jmenovatele
            bool dbChanged = false;
            if (frmGRegMapConfigsForRatio.ShowDialog() == DialogResult.OK)
            {
                if (frmGRegMapConfigsForRatio.SelectedCombination != null)
                {
                    if (frmGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList != null)
                    {
                        if (frmGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList != null)
                        {
                            if (frmGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList.Count ==
                                frmGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList.Count)
                            {
#if !(DEBUG && TEST)
                                for (int i = 0; i < frmGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList.Count; i++)
                                {
                                    Database.Postgres.ExceptionFlag = false;
                                    NfiEstaFunctions.FnApiSaveRatioConfig.Execute(
                                        database: Database,
                                        totalEstimateConfNumerator: frmGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList[i],
                                        totalEstimateConfDenominator: frmGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList[i]);
                                    dbChanged = true;

                                    if (Database.Postgres.ExceptionFlag)
                                    {
                                        // Přerušení cyklu v případě, že databáze poslala nějakou chybu
                                        Database.Postgres.ExceptionFlag = false;
                                        break;
                                    }
                                }
#else
                                // Při testování zápis do databáze se neprovádí
#endif
                            }
                        }
                    }
                }
            }
            frmGRegMapConfigsForRatio.Dispose();

            if (dbChanged)
            {
                // Pošle zprávu pro objekt OLAP, že proběhla konfigurace
                // a je nutné, aby se přenačetl z databáze
                EstimatesConfigured?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové konfigurace odhadu podílu (smíšené odhady, čitatel regresní, jmenovatel jednofázový),</para>
        /// <para lang="en">Create a new ratio estimate configuration (mixed estimates, numerator GRegMap, denominator single phase)</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        private void AddNewGRegMapToSingleRatioEstimateConfiguration(
            IEnumerable<DataRow> dtEstimates)
        {
            if (!CheckEstimateConfStatusForConfiguration(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Node no.1:
            // dtEstimates obsahuje aktuální množinu odhadů zobrazenou ve výpočetním modulu

            // Kontrola z Node no.2 a případné zobrazení zprávy z Node no.3:
            if (!CheckOnlyOneEstimationCellCollection(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Kontrola z Node no.4 a případné zobrazení zprávy z Node no.5
            if (!CheckOnlyOneEstimationPeriod(
                dtEstimates: dtEstimates,
                GREGEstimate: true,
                verbose: true))
            { return; }

            // Rozhodování z Node no.6 (úhrn nebo podíl)
            // bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly

            // Rozhodování z Node no.7 (jednofázový nebo regresní podíl)
            // bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly regresního odhadu

            // Rozhodování z Node no.37(standardní nebo smíšený regresní podíl)
            // bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly dvou smíšených odhadů
            // čitatel regresní, jmenovatel jednofázový

            int[] allowedStatus = [
                    (int) EstimateConfStatusEnum.NoConfiguration,
                    (int) EstimateConfStatusEnum.Configured,
                    (int) EstimateConfStatusEnum.Calculated ];

            IEnumerable<DataRow> cteEstimates = dtEstimates
                .Where(a => allowedStatus.Contains(value: a.Field<int>(OLAPRatioEstimateList.ColEstimateConfStatusId.Name)))
                .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.GRegMap)
                .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.SinglePhase);

            // Kontrola, zda množina zobrazených odhadů není prázdná
            // a je možné něco nakonfigurovat
            if (!CheckIsNotEmptyEstimateList(
                cteEstimates: cteEstimates,
                verbose: true))
            { return; }

            // Výpočetní období (je stejné pro celou skupinu, vezme se z prvního odhadu ve skupině):
            OLAPRatioEstimate firstEstimate = new(
                data: cteEstimates.FirstOrDefault());

            // Seznam výpočetních buněk se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> estimationCellIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColEstimationCellId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových členění čitatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> numeratorVariableIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColNumeratorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových členění jmenovatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> denominatorVariableIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColDenominatorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

#if !(DEBUG && TEST)
            // Node no.45
            TFnApiGetGRegMapToSingleConfigsForRatioList gRegMapToSingleConfigsForRatio =
                NfiEstaFunctions.FnApiGetGRegMapToSingleConfigsForRatio.Execute(
                    database: Database,
                    estimationPeriod: firstEstimate?.EstimationPeriodId,
                    estimationCells: estimationCellIdList,
                    variablesNumerator: numeratorVariableIdList,
                    variablesDenominator: denominatorVariableIdList);
#else
            // Node no.45
            TFnApiGetGRegMapToSingleConfigsForRatioList gRegMapToSingleConfigsForRatio =
                NfiEstaFunctions.FnApiGetGRegMapToSingleConfigsForRatio.Execute(
                    database: Database,
                    estimationPeriod: 1,
                    estimationCells: [55, 57, 69],
                    variablesNumerator: [1, 2, 3],
                    variablesDenominator: [3, 3, 3]);

            // Test 1: nenalezeny žádné páry konfigurací:
            // gRegMapToSingleConfigsForRatio = new TFnApiGetGRegMapToSingleConfigsForRatioList(database: Database);

            // Test 2: Všechny nalezené páry jsou nekonfigurovatelné:
            //foreach (TFnApiGetGRegMapToSingleConfigsForRatio item in gRegMapToSingleConfigsForRatio.Items)
            //{
            //    item.AllEstimatesConfigurable = false;
            //}

            // Test 3: Konfigurace pro čitatel nebo jmenovatel neexistují
            //foreach (TFnApiGetGRegMapToSingleConfigsForRatio item in gRegMapToSingleConfigsForRatio.Items)
            //{
            //    item.TotalEstimateConfNumeratorText = null;
            //    item.TotalEstimateConfDenominatorText = null;
            //}

            // Test 4: Konfigurace pro čitatel nebo jmenovatel různé délky
            //foreach (TFnApiGetGRegMapToSingleConfigsForRatio item in gRegMapToSingleConfigsForRatio.Items)
            //{
            //    item.TotalEstimateConfNumeratorText = "1;2;3";
            //    item.TotalEstimateConfDenominatorText = "1;2;3;4";
            //}
#endif

            // Node no.39 a no.46
            // Byly nalezeny nějaké páry konfigurací odpovídajích regresních a jednofázových odhadů úhrnů pro podíl?
            if (!CheckExistMatchingGRegMapToSingleTotals(
                gRegMapToSingleConfigsForRatio: gRegMapToSingleConfigsForRatio,
                verbose: true)) { return; }

            // Node no. 47, 42, 43
            // Formulář pro zobrazení a výběr kombinací GREG-map a jednofázových odhadů úhrnů
            FormGRegMapToSingleConfigsForRatio frmGRegMapToSingleConfigsForRatio =
                new(controlOwner: this)
                {
                    Combinations = gRegMapToSingleConfigsForRatio
                };

            // Node no. 44
            // Formulář zavřen tlačítkem, Save, tj. byl proveden výběr
            // provede se zápis do databáze.
            // Aby bylo možné provést zápis musí být vybraná kombinace,
            // vybraná kombinace musí mít nenulové pole konfigurací čitatele a jmenovatele,
            // a délky polí čitatele a jmenovatele musí být stejné
            // Tlačítko Save spustí cyklus uložení konfigurací smíšených regresních odhadů podílů
            // pro jednotlivé páry konfigurací čitalele a jmenovatele
            bool dbChanged = false;
            if (frmGRegMapToSingleConfigsForRatio.ShowDialog() == DialogResult.OK)
            {
                if (frmGRegMapToSingleConfigsForRatio.SelectedCombination != null)
                {
                    if (frmGRegMapToSingleConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList != null)
                    {
                        if (frmGRegMapToSingleConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList != null)
                        {
                            if (frmGRegMapToSingleConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList.Count ==
                                frmGRegMapToSingleConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList.Count)
                            {
#if !(DEBUG && TEST)
                                for (int i = 0; i < frmGRegMapToSingleConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList.Count; i++)
                                {
                                    Database.Postgres.ExceptionFlag = false;
                                    NfiEstaFunctions.FnApiSaveRatioConfig.Execute(
                                        database: Database,
                                        totalEstimateConfNumerator: frmGRegMapToSingleConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList[i],
                                        totalEstimateConfDenominator: frmGRegMapToSingleConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList[i]);
                                    dbChanged = true;

                                    if (Database.Postgres.ExceptionFlag)
                                    {
                                        // Přerušení cyklu v případě, že databáze poslala nějakou chybu
                                        Database.Postgres.ExceptionFlag = false;
                                        break;
                                    }
                                }
#else
                                // Při testování zápis do databáze se neprovádí
#endif
                            }
                        }
                    }
                }
            }
            frmGRegMapToSingleConfigsForRatio.Dispose();

            if (dbChanged)
            {
                // Pošle zprávu pro objekt OLAP, že proběhla konfigurace
                // a je nutné, aby se přenačetl z databáze
                EstimatesConfigured?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové konfigurace odhadu podílu (smíšené odhady, čitatel jednofázový, jmenovatel regresní),</para>
        /// <para lang="en">Create a new ratio estimate configuration (mixed estimates, numerator single phase, denominator GRegMap)</para>
        /// </summary>
        /// <param name="dtEstimates">
        /// <para lang="cs">Zobrazená množina odhadů</para>
        /// <para lang="en">Displayed estimate set</para>
        /// </param>
        private void AddNewSingleToGRegMapRatioEstimateConfiguration(
            IEnumerable<DataRow> dtEstimates)
        {
            if (!CheckEstimateConfStatusForConfiguration(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Node no.1:
            // dtEstimates obsahuje aktuální množinu odhadů zobrazenou ve výpočetním modulu

            // Kontrola z Node no.2 a případné zobrazení zprávy z Node no.3:
            if (!CheckOnlyOneEstimationCellCollection(
                dtEstimates: dtEstimates,
                verbose: true))
            { return; }

            // Kontrola z Node no.4 a případné zobrazení zprávy z Node no.5
            if (!CheckOnlyOneEstimationPeriod(
                dtEstimates: dtEstimates,
                GREGEstimate: true,
                verbose: true))
            { return; }

            // Rozhodování z Node no.6 bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly

            // Rozhodování z Node no.7 bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly dvou smíšených odhadů

            // Rozhodování z Node no.37 bylo provedeno v metodě AddNewEstimateConfiguration()
            // dtEstimates zde už obsahuje pouze podíly dvou smíšených odhadů

            int[] allowedStatus = [
                    (int) EstimateConfStatusEnum.NoConfiguration,
                    (int) EstimateConfStatusEnum.Configured,
                    (int) EstimateConfStatusEnum.Calculated ];

            IEnumerable<DataRow> cteEstimates = dtEstimates
                .Where(a => allowedStatus.Contains(value: a.Field<int>(OLAPRatioEstimateList.ColEstimateConfStatusId.Name)))
                .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.SinglePhase)
                .Where(a => a.Field<int>(columnName: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name) == (int)PhaseEstimateTypeEnum.GRegMap);

            // Kontrola, zda množina zobrazených odhadů není prázdná
            // a je možné něco nakonfigurovat
            if (!CheckIsNotEmptyEstimateList(
                cteEstimates: cteEstimates,
                verbose: true))
            { return; }

            // Výpočetní období (je stejné pro celou skupinu, vezme se z prvního odhadu ve skupině):
            OLAPRatioEstimate firstEstimate = new(
                data: cteEstimates.FirstOrDefault());

            // Seznam výpočetních buněk se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> estimationCellIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColEstimationCellId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových členění čitatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> numeratorVariableIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColNumeratorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

            // Seznam atributových členění jmenovatele se získá ze všech odhadů vybraných pro konfiguraci:
            List<Nullable<int>> denominatorVariableIdList = cteEstimates
                .Select(a => a.Field<Nullable<int>>(OLAPRatioEstimateList.ColDenominatorVariableId.Name))
                .Distinct<Nullable<int>>()
                .ToList<Nullable<int>>();

#if !(DEBUG && TEST)
            // Node no.45
            TFnApiGetSingleToGRegMapConfigsForRatioList singleToGRegMapConfigsForRatio =
                NfiEstaFunctions.FnApiGetSingleToGRegMapConfigsForRatio.Execute(
                    database: Database,
                    estimationPeriod: firstEstimate?.EstimationPeriodId,
                    estimationCells: estimationCellIdList,
                    variablesNumerator: numeratorVariableIdList,
                    variablesDenominator: denominatorVariableIdList);
#else
            // Node no.45
            TFnApiGetSingleToGRegMapConfigsForRatioList singleToGRegMapConfigsForRatio =
                NfiEstaFunctions.FnApiGetSingleToGRegMapConfigsForRatio.Execute(
                    database: Database,
                    estimationPeriod: 1,
                    estimationCells: [55, 57, 69],
                    variablesNumerator: [1, 2, 3],
                    variablesDenominator: [3, 3, 3]);

            // Test 1: nenalezeny žádné páry konfigurací:
            // singleToGRegMapConfigsForRatio = new TFnApiGetSingleToGRegMapConfigsForRatioList(database: Database);

            // Test 2: Všechny nalezené páry jsou nekonfigurovatelné:
            //foreach (TFnApiGetSingleToGRegMapConfigsForRatio item in singleToGRegMapConfigsForRatio.Items)
            //{
            //    item.AllEstimatesConfigurable = false;
            //}

            // Test 3: Konfigurace pro čitatel nebo jmenovatel neexistují
            //foreach (TFnApiGetSingleToGRegMapConfigsForRatio item in singleToGRegMapConfigsForRatio.Items)
            //{
            //    item.TotalEstimateConfNumeratorText = null;
            //    item.TotalEstimateConfDenominatorText = null;
            //}

            // Test 4: Konfigurace pro čitatel nebo jmenovatel různé délky
            //foreach (TFnApiGetSingleToGRegMapConfigsForRatio item in singleToGRegMapConfigsForRatio.Items)
            //{
            //    item.TotalEstimateConfNumeratorText = "1;2;3";
            //    item.TotalEstimateConfDenominatorText = "1;2;3;4";
            //}
#endif

            // Node no.39 a no.46
            // Byly nalezeny nějaké páry konfigurací odpovídajích regresních a jednofázových odhadů úhrnů pro podíl?
            if (!CheckExistMatchingSingleToGRegMapTotals(
                singleToGRegMapConfigsForRatio: singleToGRegMapConfigsForRatio,
                verbose: true)) { return; }

            // Node no. 47, 42, 43
            // Formulář pro zobrazení a výběr kombinací GREG-map a jednofázových odhadů úhrnů
            FormSingleToGRegMapConfigsForRatio frmSingleToGRegMapConfigsForRatio =
                new(controlOwner: this)
                {
                    Combinations = singleToGRegMapConfigsForRatio
                };

            // Node no. 44
            // Formulář zavřen tlačítkem, Save, tj. byl proveden výběr
            // provede se zápis do databáze.
            // Aby bylo možné provést zápis musí být vybraná kombinace,
            // vybraná kombinace musí mít nenulové pole konfigurací čitatele a jmenovatele,
            // a délky polí čitatele a jmenovatele musí být stejné
            // Tlačítko Save spustí cyklus uložení konfigurací smíšených regresních odhadů podílů
            // pro jednotlivé páry konfigurací čitalele a jmenovatele
            bool dbChanged = false;
            if (frmSingleToGRegMapConfigsForRatio.ShowDialog() == DialogResult.OK)
            {
                if (frmSingleToGRegMapConfigsForRatio.SelectedCombination != null)
                {
                    if (frmSingleToGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList != null)
                    {
                        if (frmSingleToGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList != null)
                        {
                            if (frmSingleToGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList.Count ==
                                frmSingleToGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList.Count)
                            {
#if !(DEBUG && TEST)
                                for (int i = 0; i < frmSingleToGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList.Count; i++)
                                {
                                    Database.Postgres.ExceptionFlag = false;
                                    NfiEstaFunctions.FnApiSaveRatioConfig.Execute(
                                        database: Database,
                                        totalEstimateConfNumerator: frmSingleToGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfNumeratorList[i],
                                        totalEstimateConfDenominator: frmSingleToGRegMapConfigsForRatio.SelectedCombination.TotalEstimateConfDenominatorList[i]);
                                    dbChanged = true;

                                    if (Database.Postgres.ExceptionFlag)
                                    {
                                        // Přerušení cyklu v případě, že databáze poslala nějakou chybu
                                        Database.Postgres.ExceptionFlag = false;
                                        break;
                                    }
                                }
#else
                                // Při testování zápis do databáze se neprovádí
#endif
                            }
                        }
                    }
                }
            }
            frmSingleToGRegMapConfigsForRatio.Dispose();

            if (dbChanged)
            {
                // Pošle zprávu pro objekt OLAP, že proběhla konfigurace
                // a je nutné, aby se přenačetl z databáze
                EstimatesConfigured?.Invoke(sender: this, e: new EventArgs());
            }
        }


        /// <summary>
        /// <para lang="cs">Spuštění vláken provádějících konfiguraci jednofázových odhadů úhrnů</para>
        /// <para lang="en">Starts threads that executes simple total estimates configurations</para>
        /// </summary>
        private void StartConfigurationTotalsThreads(
            IEnumerable<DataRow> cteEstimates,
            List<AggregatedEstimationCellGroup> aggregatedEstimationCellGroups)
        {
            if (!CheckSystemResourcesAvailability(configuration: true)) { return; }

            ControlThread =
                new ControlThread<
                    FnApiSaveSinglePhaseTotalConfigWorker,
                    FnApiSaveSinglePhaseTotalConfigTask,
                    OLAPTotalEstimate>();

            ThreadSafeQueue<FnApiSaveSinglePhaseTotalConfigTask> tasksToDo = new();
            tasksToDo.EnqueueRange(items:
                cteEstimates
                    .Select(a => new OLAPTotalEstimate(data: a))
                    .Select(a => new FnApiSaveSinglePhaseTotalConfigTask(
                        estimate: a,
                        aggregatedEstimationCellGroup:
                            aggregatedEstimationCellGroups
                                .Where(b => b.EstimationCellsIdsSortedList.Contains(
                                    item: a.EstimationCellId))
                                .FirstOrDefault<AggregatedEstimationCellGroup>())));

            ControlThread.OnControlStart += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    CtrEstimate?.Disable();

                    prgCalculation.Minimum = 0;
                    prgCalculation.Value = 0;
                    prgCalculation.Maximum = args.TasksTotal ?? 0;

                    splProgress.Panel1Collapsed = false;
                    splProgress.Panel2Collapsed = true;
                    lblCalculationStatus.Text = String.Empty;
                }));
            });

            ControlThread.OnTaskComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    prgCalculation.Value = args.TasksCompleted ?? 0;
                }));
            });

            ControlThread.OnControlComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    splProgress.Panel1Collapsed = true;
                    splProgress.Panel2Collapsed = false;
                    lblCalculationStatus.Text = strConfigurationComplete.Replace(
                        oldValue: "$1",
                        newValue: (args.ElapsedTime ?? 0.0).ToString(format: "N3"));

                    CtrEstimate?.Enable();

                    EstimatesConfigured?.Invoke(sender: this, e: new EventArgs());

                    if (ControlThread.Log.Any())
                    {
                        WriteErrorLogIntoFile(errorLog: ControlThread.Log);
                        FormErrorLog form = new(controlOwner: this)
                        {
                            ErrorLog = ControlThread.Log
                        };
                        if (form.ShowDialog() == DialogResult.OK) { };
                    }
                }));
            });

            ((ControlThread<
                FnApiSaveSinglePhaseTotalConfigWorker,
                FnApiSaveSinglePhaseTotalConfigTask,
                OLAPTotalEstimate>)
                ControlThread).Start(
                    workersCount: Setting.ConfigurationAvailableThreadsNumber,
                    tasksToDo: tasksToDo,
                    connection: Database.Postgres);
        }

        /// <summary>
        /// <para lang="cs">Spuštění vláken provádějících konfiguraci jednofázových odhadů podílů</para>
        /// <para lang="en">Starts threads that executes simple ratio estimates configurations</para>
        /// </summary>
        private void StartConfigurationRatiosThreads(
            IEnumerable<DataRow> cteEstimates,
            List<AggregatedEstimationCellGroup> aggregatedEstimationCellGroups)
        {
            if (!CheckSystemResourcesAvailability(configuration: true)) { return; }

            ControlThread =
                new ControlThread<
                    FnApiSaveSinglePhaseRatioConfigWorker,
                    FnApiSaveSinglePhaseRatioConfigTask,
                    OLAPRatioEstimate>();

            ThreadSafeQueue<FnApiSaveSinglePhaseRatioConfigTask> tasksToDo = new();
            tasksToDo.EnqueueRange(items:
                cteEstimates
                    .Select(a => new OLAPRatioEstimate(data: a))
                    .Select(a => new FnApiSaveSinglePhaseRatioConfigTask(
                        estimate: a,
                        aggregatedEstimationCellGroup:
                            aggregatedEstimationCellGroups
                                .Where(b => b.EstimationCellsIdsSortedList.Contains(
                                    item: a.EstimationCellId))
                                .FirstOrDefault<AggregatedEstimationCellGroup>())));

            ControlThread.OnControlStart += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    CtrEstimate?.Disable();

                    prgCalculation.Minimum = 0;
                    prgCalculation.Value = 0;
                    prgCalculation.Maximum = args.TasksTotal ?? 0;

                    splProgress.Panel1Collapsed = false;
                    splProgress.Panel2Collapsed = true;
                    lblCalculationStatus.Text = String.Empty;
                }));
            });

            ControlThread.OnTaskComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    prgCalculation.Value = args.TasksCompleted ?? 0;
                }));
            });

            ControlThread.OnControlComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    splProgress.Panel1Collapsed = true;
                    splProgress.Panel2Collapsed = false;
                    lblCalculationStatus.Text = strConfigurationComplete.Replace(
                        oldValue: "$1",
                        newValue: (args.ElapsedTime ?? 0.0).ToString(format: "N3"));

                    CtrEstimate?.Enable();

                    EstimatesConfigured?.Invoke(sender: this, e: new EventArgs());

                    if (ControlThread.Log.Any())
                    {
                        WriteErrorLogIntoFile(errorLog: ControlThread.Log);
                        FormErrorLog form = new(controlOwner: this)
                        {
                            ErrorLog = ControlThread.Log
                        };
                        if (form.ShowDialog() == DialogResult.OK) { };
                    }
                }));
            });

            ((ControlThread<
                FnApiSaveSinglePhaseRatioConfigWorker,
                FnApiSaveSinglePhaseRatioConfigTask,
                OLAPRatioEstimate>)
                ControlThread).Start(
                    workersCount: Setting.ConfigurationAvailableThreadsNumber,
                    tasksToDo: tasksToDo,
                    connection: Database.Postgres);
        }

        #endregion Configuration methods


        #region Calculation methods

        /// <summary>
        /// <para lang="cs">Spuštění výpočtu odhadů</para>
        /// <para lang="en">Start the estimates calculation</para>
        /// </summary>
        private void CalculateEstimates()
        {

            switch (DisplayType)
            {
                case DisplayType.Total:
                    CalculateTotalEstimates();
                    return;

                case DisplayType.Ratio:
                    CalculateRatioEstimates();
                    return;

                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(DisplayType)} unknown value.",
                        paramName: nameof(DisplayType));
            }

        }

        /// <summary>
        /// <para lang="cs">Spuštění výpočtu odhadů úhrnu</para>
        /// <para lang="en">Start the total estimates calculation</para>
        /// </summary>
        private void CalculateTotalEstimates()
        {
            // Datová tabulka s odhady vybranými pro výpočet
            // Data table with estimates selected for calculation
            DataTable dtEstimates = DataSourceTotal.Data.DefaultView.ToTable();

            // Povolený status odhadů pro výpočet
            // Výpočet není možné provést na odhadech status 100:
            //      nemají provedenou konfiguraci
            // Výpočet je možné provést na odhadech status = 200 nebo 300:
            //      mají provedenou konfiguraci - 200 nový výpočet, 300 přepočítání existujícího odhadu
            // Enabled estimation status for calculation
            // Calculation cannot be done on status estimates of 100
            //      not configured
            // Calculation can be done on estimates of status = 200 or 300
            //      have configuration done - 200 new calculation,  300 recalculation of an existing estimate
            int[] allowedStatus = [200, 300];
            if (dtEstimates.AsEnumerable()
                    .Where(a => !allowedStatus.Contains(
                        value: a.Field<int>(OLAPTotalEstimateList.ColEstimateConfStatusId.Name)))
                    .Any())
            {
                // Výpočet pouze pro odhady s konfigurací
                // Calculation only for estimates with configuration
                MessageBox.Show(
                    text: msgCalcEstimateWithConfiguration,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            // Povolená fáze odhadu - jednofázové odhady
            // Konfigurace a výpočet jiných typů odhadů nebyl zatím implementován
            // Enabled estimation phase - only one phase estimates
            // Configuration and calculation of other types of estimates has not been implemented yet
            int[] allowedPhase = [1, 2];
            if (dtEstimates.AsEnumerable()
                    .Where(a => !allowedPhase.Contains(
                        value: a.Field<int>(OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name)))
                    .Any())
            {
                // Výpočet pouze pro jednofázové odhady
                // Calculation for one-phase estimates only
                MessageBox.Show(
                    text: msgCalcEstimateNotOnePhase,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);

                return;
            }

            List<OLAPTotalEstimate> cteEstimates =
                [.. dtEstimates.AsEnumerable()
                    .Where(a => allowedPhase.Contains(
                            value: a.Field<int>(OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name)))
                    .Where(a => allowedStatus.Contains(
                            value: a.Field<int>(OLAPTotalEstimateList.ColEstimateConfStatusId.Name)))
                    .Select(a => new OLAPTotalEstimate(data: a))];

            if (cteEstimates.Count == 0)
            {
                // Žádné vybrané odhady pro výpočet
                // No selected estimates for calculation
                MessageBox.Show(
                    text: msgCalcEstimateNoneSelected,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            StartCalculationTotalsThreads(estimateConfs: cteEstimates);
        }

        /// <summary>
        /// <para lang="cs">Spuštění výpočtu odhadů podílu</para>
        /// <para lang="en">Start the ratio estimates calculation</para>
        /// </summary>
        private void CalculateRatioEstimates()
        {
            // Datová tabulka s odhady vybranými pro výpočet
            // Data table with estimates selected for calculation
            DataTable dtEstimates = DataSourceRatio.Data.DefaultView.ToTable();

            // Povolený status odhadů pro výpočet
            // Výpočet není možné provést na odhadech status 100:
            //      nemají provedenou konfiguraci
            // Výpočet je možné provést na odhadech status = 200 nebo 300:
            //      mají provedenou konfiguraci - 200 nový výpočet, 300 přepočítání existujícího odhadu
            // Enabled estimation status for calculation
            // Calculation cannot be done on status estimates of 100
            //      not configured
            // Calculation can be done on estimates of status = 200 or 300
            //      have configuration done - 200 new calculation,  300 recalculation of an existing estimate
            int[] allowedStatus = [200, 300];
            if (dtEstimates.AsEnumerable()
                .Where(a => !allowedStatus.Contains(
                                value: a.Field<int>(OLAPRatioEstimateList.ColEstimateConfStatusId.Name)))
                .Any())
            {
                // Výpočet pouze pro odhady s konfigurací
                // Calculation only for estimates with configuration
                MessageBox.Show(
                   text: msgCalcEstimateWithConfiguration,
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);

                return;
            }

            // Povolená fáze odhadu - jednofázové odhady
            // Konfigurace a výpočet jiných typů odhadů nebyl zatím implementován
            // Enabled estimation phase - only one phase estimates
            // Configuration and calculation of other types of estimates has not been implemented yet
            int[] allowedPhase = [1, 2];
            if (dtEstimates.AsEnumerable()
                .Where(a => !allowedPhase.Contains(
                     value: a.Field<int>(OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name)))
                .Any())
            {
                // Výpočet pouze pro jednofázové odhady
                // Calculation for one-phase estimates only
                MessageBox.Show(
                   text: msgCalcEstimateNotOnePhase,
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);

                return;
            }

            List<OLAPRatioEstimate> cteEstimates =
                [.. dtEstimates.AsEnumerable()
                    .Where(a => allowedPhase.Contains(
                            value: a.Field<int>(OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name)))
                    .Where(a => allowedStatus.Contains(
                            value: a.Field<int>(OLAPRatioEstimateList.ColEstimateConfStatusId.Name)))
                    .Select(a => new OLAPRatioEstimate(data: a))];

            if (cteEstimates.Count == 0)
            {
                // Žádné vybrané odhady pro výpočet
                // No selected estimates for calculation
                MessageBox.Show(
                    text: msgCalcEstimateNoneSelected,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            StartCalculationRatiosThreads(estimateConfs: cteEstimates);
        }

        /// <summary>
        /// <para lang="cs">Spuštění vláken provádějících výpočet odhadů úhrnů</para>
        /// <para lang="en">Starts threads that executes total estimates calculation</para>
        /// </summary>
        /// <param name="estimateConfs">
        /// <para lang="cs">Seznam odhadů pro výpočet</para>
        /// <para lang="en">List of estimates for calculation</para>
        /// </param>
        private void StartCalculationTotalsThreads(List<OLAPTotalEstimate> estimateConfs)
        {
            if (!CheckSystemResourcesAvailability(configuration: false)) { return; }

            ControlThread =
                new ControlThread<
                    FnApiMakeTotalEstimateWorker,
                    FnApiMakeTotalEstimateTask,
                    OLAPTotalEstimate>();

            ThreadSafeQueue<FnApiMakeTotalEstimateTask> tasksToDo = new();
            tasksToDo.EnqueueRange(items: estimateConfs
                .Select(a => new FnApiMakeTotalEstimateTask(estimate: a)));

            ControlThread.OnControlStart += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    CtrEstimate?.Disable();

                    prgCalculation.Minimum = 0;
                    prgCalculation.Value = 0;
                    prgCalculation.Maximum = args.TasksTotal ?? 0;

                    splProgress.Panel1Collapsed = false;
                    splProgress.Panel2Collapsed = true;
                    lblCalculationStatus.Text = String.Empty;
                }));
            });

            ControlThread.OnTaskComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    prgCalculation.Value = args.TasksCompleted ?? 0;
                }));
            });

            ControlThread.OnControlComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    splProgress.Panel1Collapsed = true;
                    splProgress.Panel2Collapsed = false;
                    lblCalculationStatus.Text = strCalculationComplete.Replace(
                        oldValue: "$1",
                        newValue: (args.ElapsedTime ?? 0.0).ToString(format: "N3"));

                    CtrEstimate?.Enable();

                    EstimatesCalculated?.Invoke(sender: this, e: new EventArgs());

                    if (ControlThread.Log.Any())
                    {
                        WriteErrorLogIntoFile(errorLog: ControlThread.Log);
                        FormErrorLog form = new(controlOwner: this)
                        {
                            ErrorLog = ControlThread.Log
                        };
                        if (form.ShowDialog() == DialogResult.OK) { };
                    }
                }));
            });

            ((ControlThread<
                FnApiMakeTotalEstimateWorker,
                FnApiMakeTotalEstimateTask,
                OLAPTotalEstimate>)
                ControlThread).Start(
                    workersCount: Setting.EstimateAvailableThreadsNumber,
                    tasksToDo: tasksToDo,
                    connection: Database.Postgres);
        }

        /// <summary>
        /// <para lang="cs">Spuštění vláken provádějících výpočet odhadů podílů</para>
        /// <para lang="en">Starts threads that executes ratio estimates calculation</para>
        /// </summary>
        /// <param name="estimateConfs">
        /// <para lang="cs">Seznam odhadů pro výpočet</para>
        /// <para lang="en">List of estimates for calculation</para>
        /// </param>
        private void StartCalculationRatiosThreads(List<OLAPRatioEstimate> estimateConfs)
        {
            if (!CheckSystemResourcesAvailability(configuration: false)) { return; }

            ControlThread =
                new ControlThread<
                    FnApiMakeRatioEstimateWorker,
                    FnApiMakeRatioEstimateTask,
                    OLAPRatioEstimate>();

            ThreadSafeQueue<FnApiMakeRatioEstimateTask> tasksToDo = new();
            tasksToDo.EnqueueRange(items: estimateConfs
                .Select(a => new FnApiMakeRatioEstimateTask(estimate: a)));

            ControlThread.OnControlStart += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    CtrEstimate?.Disable();

                    prgCalculation.Minimum = 0;
                    prgCalculation.Value = 0;
                    prgCalculation.Maximum = args.TasksTotal ?? 0;

                    splProgress.Panel1Collapsed = false;
                    splProgress.Panel2Collapsed = true;
                    lblCalculationStatus.Text = String.Empty;
                }));
            });

            ControlThread.OnTaskComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    prgCalculation.Value = args.TasksCompleted ?? 0;
                }));
            });

            ControlThread.OnControlComplete += new EventHandler(
            (sender, e) =>
            {
                ThreadEventArgs args = (ThreadEventArgs)e;

                BeginInvoke(new Action(() =>
                {
                    splProgress.Panel1Collapsed = true;
                    splProgress.Panel2Collapsed = false;
                    lblCalculationStatus.Text = strCalculationComplete.Replace(
                        oldValue: "$1",
                        newValue: (args.ElapsedTime ?? 0.0).ToString(format: "N3"));

                    CtrEstimate?.Enable();

                    EstimatesCalculated?.Invoke(sender: this, e: new EventArgs());

                    if (ControlThread.Log.Any())
                    {
                        WriteErrorLogIntoFile(errorLog: ControlThread.Log);
                        FormErrorLog form = new(controlOwner: this)
                        {
                            ErrorLog = ControlThread.Log
                        };
                        if (form.ShowDialog() == DialogResult.OK) { };
                    }
                }));
            });

            ((ControlThread<
                FnApiMakeRatioEstimateWorker,
                FnApiMakeRatioEstimateTask,
                OLAPRatioEstimate>)
                ControlThread).Start(
                    workersCount: Setting.EstimateAvailableThreadsNumber,
                    tasksToDo: tasksToDo,
                    connection: Database.Postgres);
        }

        #endregion Calculation methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Provede zápis logu do textového souboru</para>
        /// <para lang="en">Writes the log to a text file</para>
        /// </summary>
        /// <param name="errorLog">
        /// <para lang="cs">Seznam chybových zpráv z pracovních vláken</para>
        /// <para lang="en">List of error messages from worker threads</para>
        /// </param>
        private void WriteErrorLogIntoFile(
            ThreadSafeList<string> errorLog)
        {
            string folderName = @"log";
            string fileName = String.Concat(folderName, @"\module_estimate_log.txt");
            System.IO.StreamWriter sw = null;

            try
            {
                // Pokud neexistuje adresář, pak je vytvořen
                if (!System.IO.Directory.Exists(path: folderName))
                {
                    System.IO.Directory.CreateDirectory(path: folderName);
                }

                // Vytvoří StreamWriter
                if (System.IO.File.Exists(path: fileName))
                {
                    if (CtrEstimate.ErrorLogEntryNumber == 0)
                    {
                        // Pokud při daném spuštění aplikace ještě žádná chyba nebyla zaznamenána
                        // přepíše staré záznamy z předchozího spuštění
                        sw = new System.IO.StreamWriter(path: fileName, append: false);
                    }
                    else
                    {
                        // Pokud při daném spuštění aplikace jiz chyby zaznamenány byly
                        // přidá nové záznamy
                        sw = new System.IO.StreamWriter(path: fileName, append: true);
                    }
                }
                else
                {
                    sw = System.IO.File.CreateText(path: fileName);
                }

                // Zápis řádků do souboru
                sw.WriteLine(value: DateTime.Now.ToString());
                foreach (string line in errorLog.Items)
                {
                    sw.WriteLine(value: line);
                }
            }
            finally
            {
                CtrEstimate.ErrorLogEntryNumber++;
                sw?.Close();
            }
        }


        /// <summary>
        /// <para lang="cs">Export data to csv file</para>
        /// <para lang="en">Export dat do souboru csv</para>
        /// </summary>
        private void ExportData()
        {
            const char separator = (char)59;
            const char hyphen = (char)45;

            // Jméno souboru
            // File name
            string csvName =
                (LanguageVersion == LanguageVersion.National) ?
                    SelectedTargetVariableMetadata.IndicatorLabelCs :
                (LanguageVersion == LanguageVersion.International) ?
                    SelectedTargetVariableMetadata.IndicatorLabelEn :
                String.Empty;

            csvName ??= String.Empty;
            csvName = String.Concat(
                $"{SelectedTargetVariable.Id}_",
                $"{csvName}.csv");
            csvName = csvName.Replace(
                oldChar: ' ',
                newChar: '_');

            // Výběr souboru pro zápis dat
            // Selecting a file for writing data
            SaveFileDialog dlg = new()
            {
                FileName = csvName,
                Filter = "Text Files(*.csv) | *.csv"
            };

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                List<string> lines = [];

                List<string> headers =
                    (dgvResults.Columns
                        .OfType<DataGridViewColumn>()
                        .Where(a => a.Visible)
                        .OrderBy(a => a.DisplayIndex)
                        .Select(a => a.HeaderText)).ToList();

                List<string> dataPropertyNames =
                     (dgvResults.Columns
                        .OfType<DataGridViewColumn>()
                        .Where(a => a.Visible)
                        .OrderBy(a => a.DisplayIndex)
                        .Select(a => a.DataPropertyName)).ToList();

                // Hlavička
                // Header
                lines.Add(headers.Aggregate((a, b) => $"{a}{separator}{b}"));

                // Datové řádky
                // Data rows
                foreach (DataGridViewRow row in dgvResults.Rows)
                {
                    lines.Add(
                        row.Cells
                            .OfType<DataGridViewCell>()
                            .Where(a => dataPropertyNames.Contains(a.OwningColumn.DataPropertyName))
                            .OrderBy(a => dataPropertyNames.IndexOf(a.OwningColumn.DataPropertyName))
                            .Select(a =>
                                (a == null) ? String.Empty :
                                (a.Value == DBNull.Value) ? String.Empty :
                                    a.Value.ToString().Replace(separator, hyphen))
                            .Aggregate((a, b) => $"{a}{separator}{b}"));
                }

                // Zápis do souboru
                // Writing into file
                try
                {
                    if (System.IO.File.Exists(path: dlg.FileName))
                    {
                        System.IO.File.Delete(path: dlg.FileName);
                    }
                    System.IO.File.WriteAllLines(
                        path: dlg.FileName,
                        contents: [.. lines],
                        encoding: System.Text.Encoding.UTF8);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }
        }


        /// <summary>
        /// <para lang="cs">Zobrazení historie odhadu</para>
        /// <para lang="en">Display history of the estimate</para>
        /// </summary>
        private void DisplayHistory()
        {
            HistoryDisplayed = !HistoryDisplayed;
            DisplayHistoryClick?.Invoke(sender: this, e: new EventArgs());
        }


        /// <summary>
        /// <para lang="cs">Zobrazení panelů</para>
        /// <para lang="en">Display panels</para>
        /// </summary>
        private void DisplayPanels()
        {
            DisplayPanelsClick?.Invoke(sender: this, e: new EventArgs());
        }


        /// <summary>
        /// <para lang="cs"> Změna v seznamu období odhadu</para>
        /// <para lang="en">Change in the list of estimation periods</para>
        /// </summary>
        private void EditEstimationPeriod()
        {
#if DEBUG
            FormEstimationPeriod frmEstimationPeriod =
                new(controlOwner: this)
                {
                    ReadOnlyMode = false
                };
#else
            FormEstimationPeriod frmEstimationPeriod =
                new(controlOwner: this)
                {
                    ReadOnlyMode = !Database.UserIsManager
                };
#endif
            frmEstimationPeriod.ShowDialog();
            if (frmEstimationPeriod.DbChanged)
            {
                EstimationPeriodChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }


        /// <summary>
        /// <para lang="cs">Změna ve skupinách panelů</para>
        /// <para lang="en">Change in the list of panel groups</para>
        /// </summary>
        private void EditPanelReferenceYearSetGroup()
        {
#if DEBUG
            FormPanelRefYearSetGroup frmPanelRefYearSetGroup =
               new(controlOwner: this)
               {
                   ReadOnlyMode = false
               };
#else
            FormPanelRefYearSetGroup frmPanelRefYearSetGroup =
               new(controlOwner: this)
               {
                   ReadOnlyMode = !Database.UserIsManager
               };
#endif

            frmPanelRefYearSetGroup.ShowDialog();
            if (frmPanelRefYearSetGroup.DbChanged)
            {
                PanelReferenceYearSetGroupChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }


        /// <summary>
        /// <para lang="cs">Nahrání skupiny výpočetních buněk</para>
        /// <para lang="en">Loads group of estimation cells</para>
        /// </summary>
        private void LoadEstimationCellCollection()
        {
            FormEstimationCellCollection form =
                new(controlOwner: this)
                {
                    SelectedEstimationCellCollections = SelectedEstimationCellCollections
                };

            if (form.ShowDialog() == DialogResult.OK)
            {
                SelectedEstimationCellCollections = form.SelectedEstimationCellCollections;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání skupiny výpočetních období</para>
        /// <para lang="en">Loads group of estimation periods</para>
        /// </summary>
        private void LoadEstimationPeriod()
        {
            FormEstimationPeriodSelection form =
                new(controlOwner: this)
                {
                    SelectedEstimationPeriods = SelectedEstimationPeriods
                };

            if (form.ShowDialog() == DialogResult.OK)
            {
                SelectedEstimationPeriods = form.SelectedEstimationPeriods;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání skupiny vícefázových odhadů</para>
        /// <para lang="en">Loads group of multiple phase estimates</para>
        /// </summary>
        private void LoadPhaseEstimateType()
        {
            FormPhaseEstimateType form =
                new(controlOwner: this)
                {
                    DisplayType = DisplayType,
                    SelectedPhaseEstimateTypes = SelectedPhaseEstimateTypes
                };

            if (form.ShowDialog() == DialogResult.OK)
            {
                SelectedPhaseEstimateTypes = form.SelectedPhaseEstimateTypes;
            }
        }


        /// <summary>
        /// <para lang="cs">Zrušení fitru řádků</para>
        /// <para lang="en">Cancel row filter</para>
        /// </summary>
        private void CancelRowFilter()
        {
            RowFilter = new FilterRow();
        }


        /// <summary>
        /// <para lang="cs">Vyvolání události
        /// "Změna vybraného odhadu"</para>
        /// <para lang="en">Invoking the event
        /// "Selected estimate changed"</para>
        /// </summary>
        private void RaiseSelectedEstimateChanged()
        {
            btnDisplayPanels.Enabled = DisplayType switch
            {
                DisplayType.Total =>
                    SelectedEstimateTotal != null,
                DisplayType.Ratio =>
                    SelectedEstimateRatio != null,
                _ =>
                    throw new Exception(message: $"Argument {nameof(DisplayType)} unknown value."),
            };

            SelectedEstimateChanged?.Invoke(
                sender: this,
                e: new EventArgs());
        }

        #endregion Methods

    }

}