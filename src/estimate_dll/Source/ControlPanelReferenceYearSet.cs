﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro výběr páru panelu a referenčního období</para>
    /// <para lang="en">Control for panel and reference year set pair selection</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlPanelReferenceYearSet
        : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Pár panelu a referenčního období</para>
        /// <para lang="en">Panel and reference year set pair</para>
        /// </summary>
        private TFnApiGetDataOptionsForSinglePhaseConfig panelReferenceYearSetPair;

        /// <summary>
        /// <para lang="cs">Text pro hodnotu true</para>
        /// <para lang="en">String representation of true value</para>
        /// </summary>
        private string strTrue = true.ToString();

        /// <summary>
        /// <para lang="cs">Text pro hodnotu false</para>
        /// <para lang="en">String representation of value value</para>
        /// </summary>
        private string strFalse = false.ToString();

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Změna výběru páru panelu a referenčního období pro konfiguraci odhadu"</para>
        /// <para lang="en">Event "Change in selected panel and reference year set pair for estimate configuration"</para>
        /// </summary>
        public event EventHandler OnSelectionChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="panelReferenceYearSetPair">
        /// <para lang="cs">Pár panelu a referenčního období</para>
        /// <para lang="en">Panel and reference year set pair</para>
        /// </param>
        public ControlPanelReferenceYearSet(
            Control controlOwner,
            TFnApiGetDataOptionsForSinglePhaseConfig panelReferenceYearSetPair)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                panelReferenceYearSetPair: panelReferenceYearSetPair);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Pár panelu a referenčního období</para>
        /// <para lang="en">Panel and reference year set pair</para>
        /// </summary>
        public TFnApiGetDataOptionsForSinglePhaseConfig PanelReferenceYearSetPair
        {
            get
            {
                return panelReferenceYearSetPair ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(PanelReferenceYearSetPair)} must not be null.",
                        paramName: nameof(PanelReferenceYearSetPair));
            }
            set
            {
                panelReferenceYearSetPair = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(PanelReferenceYearSetPair)} must not be null.",
                        paramName: nameof(PanelReferenceYearSetPair));

                InitializeLabels();

                SetBackgroundColor();
            }
        }

        /// <summary>
        /// <para lang="cs">Je tato pár panelu a referenčního období vybraný pro konfiguraci odhadu?</para>
        /// <para lang="en">Is this panel and reference year set pair selected for estimate configuration?</para>
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return chkSelection.Checked;
            }
            set
            {
                chkSelection.Checked = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Je výpis ve stručné podobě</para>
        /// <para lang="en">Is list in a brief form</para>
        /// </summary>
        public bool IsCollapsed
        {
            get
            {
                return tlpData.RowStyles[0].Height == 0;
            }
            set
            {
                if (value)
                {
                    Collapse();
                }
                else
                {
                    Expand();
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Je zobrazený CheckBox</para>
        /// <para lang="en">Is check box displayed</para>
        /// </summary>
        public bool IsCheckBoxDisplayed
        {
            get
            {
                return tlpMain.ColumnStyles[0].Width != 0;
            }
            set
            {
                if (value)
                {
                    ShowCheckBox();
                }
                else
                {
                    HideCheckBox();
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(lblExpand),                                                    "+" },
                        { nameof(lblCollapse),                                                  "-" },
                        { nameof(lblCollapsedPanelCaption),                                     "Panel:" },
                        { nameof(lblCollapsedReferenceYearSetCaption),                          "Referenční období:" },
                        { nameof(lblPanelCaption),                                              "Panel:" },
                        { nameof(lblReferenceYearSetCaption),                                   "Referenční období:" },
                        { nameof(lblReferenceDateBeginCaption),                                 "Začátek referenčního období:" },
                        { nameof(lblReferenceDateEndCaption),                                   "Konec referenčního období:" },
                        { nameof(lblRefYearSetFullyWithinEstimationPeriodCaption),              "Referenční období plně uvnitř výpočetního období:" },
                        { nameof(lblShareOfRefYearSetIntersectedByEstimationPeriodCaption),     "Podíl referenčního období uvnitř výpočetního období:" },
                        { nameof(lblShareOfEstimationPeriodIntersectedByRefYearSetCaption),     "Podíl výpočetního období uvnitř referenčního období:" },
                        { nameof(lblSSizeCaption),                                              "Velikost výběru:" },
                        { nameof(lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption),        "Max.velikost výběru uvnitř výpočetního a ref.období:" },
                        { nameof(strTrue),                                                      "ANO" },
                        { nameof(strFalse),                                                     "NE" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlPanelReferenceYearSet),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(lblExpand),                                                    "+" },
                        { nameof(lblCollapse),                                                  "-" },
                        { nameof(lblCollapsedPanelCaption),                                     "Panel:" },
                        { nameof(lblCollapsedReferenceYearSetCaption),                          "Reference year set:" },
                        { nameof(lblPanelCaption),                                              "Panel:" },
                        { nameof(lblReferenceYearSetCaption),                                   "Reference year set:" },
                        { nameof(lblReferenceDateBeginCaption),                                 "Reference date begin:" },
                        { nameof(lblReferenceDateEndCaption),                                   "Reference date end:" },
                        { nameof(lblRefYearSetFullyWithinEstimationPeriodCaption),              "Reference year set fully within estimation period:" },
                        { nameof(lblShareOfRefYearSetIntersectedByEstimationPeriodCaption),     "Share of ref. year set intersected by estimation period:" },
                        { nameof(lblShareOfEstimationPeriodIntersectedByRefYearSetCaption),     "Share of estimation period intersected by ref. year set:" },
                        { nameof(lblSSizeCaption),                                              "Sample size:" },
                        { nameof(lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption),        "Max. sample size within est. period and ref. year set:" },
                        { nameof(strTrue),                                                      "YES" },
                        { nameof(strFalse),                                                     "NO" }
                     }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlPanelReferenceYearSet),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="panelReferenceYearSetPair">
        /// <para lang="cs">Pár panelu a referenčního období</para>
        /// <para lang="en">Panel and reference year set pair</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            TFnApiGetDataOptionsForSinglePhaseConfig panelReferenceYearSetPair)
        {
            ControlOwner = controlOwner;

            PanelReferenceYearSetPair = panelReferenceYearSetPair;

            IsSelected = false;

            IsCollapsed = true;

            IsCheckBoxDisplayed = true;

            lblExpand.Click += new EventHandler(
                (sender, e) => { Expand(); });

            lblCollapse.Click += new EventHandler(
                (sender, e) => { Collapse(); });

            chkSelection.CheckedChanged += new EventHandler(
                (sender, e) => { OnSelectionChanged?.Invoke(sender: this, e: e); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            lblExpand.Text =
                labels.TryGetValue(key: nameof(lblExpand),
                out string lblExpandText)
                    ? lblExpandText
                    : String.Empty;

            lblCollapse.Text =
                labels.TryGetValue(key: nameof(lblCollapse),
                out string lblCollapseText)
                    ? lblCollapseText
                    : String.Empty;

            strTrue =
                labels.TryGetValue(key: nameof(strTrue),
                out string strTrueText)
                    ? strTrueText
                    : String.Empty;

            strFalse =
                labels.TryGetValue(key: nameof(strFalse),
                out string strFalseText)
                    ? strFalseText
                    : String.Empty;

            lblCollapsedPanelCaption.Text =
                labels.TryGetValue(key: nameof(lblCollapsedPanelCaption),
                out string lblCollapsedPanelCaptionText)
                    ? lblCollapsedPanelCaptionText
                    : String.Empty;

            lblCollapsedPanelValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.PanelLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.PanelLabelCs ?? String.Empty
                        : PanelReferenceYearSetPair?.PanelLabelEn ?? String.Empty;

            ToolTip toolTipCollapsedPanelValue = new();
            toolTipCollapsedPanelValue.SetToolTip(
                control: lblCollapsedPanelValue,
                caption: (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.PanelExtendedLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.PanelExtendedLabelCs ?? String.Empty
                        : PanelReferenceYearSetPair?.PanelExtendedLabelEn ?? String.Empty);

            lblCollapsedReferenceYearSetCaption.Text =
                labels.TryGetValue(key: nameof(lblCollapsedReferenceYearSetCaption),
                out string lblCollapsedReferenceYearSetCaptionText)
                    ? lblCollapsedReferenceYearSetCaptionText
                    : String.Empty;

            lblCollapsedReferenceYearSetValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.ReferenceYearSetLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.ReferenceYearSetLabelCs ?? String.Empty
                        : PanelReferenceYearSetPair?.ReferenceYearSetLabelEn ?? String.Empty;

            ToolTip toolTipReferenceYearSetValue = new();
            toolTipReferenceYearSetValue.SetToolTip(
                control: lblCollapsedReferenceYearSetValue,
                caption: (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.ReferenceYearSetExtendedLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.ReferenceYearSetExtendedLabelCs ?? String.Empty
                        : PanelReferenceYearSetPair?.ReferenceYearSetExtendedLabelEn ?? String.Empty);

            lblPanelCaption.Text =
                labels.TryGetValue(key: nameof(lblPanelCaption),
                out string lblPanelCaptionText)
                    ? lblPanelCaptionText
                    : String.Empty;

            lblPanelValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.PanelLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.PanelLabelCs ?? String.Empty
                        : PanelReferenceYearSetPair?.PanelLabelEn ?? String.Empty;

            lblReferenceYearSetCaption.Text =
                labels.TryGetValue(key: nameof(lblReferenceYearSetCaption),
                out string lblReferenceYearSetCaptionText)
                    ? lblReferenceYearSetCaptionText
                    : String.Empty;

            lblReferenceYearSetValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.ReferenceYearSetLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.ReferenceYearSetLabelCs ?? String.Empty
                        : PanelReferenceYearSetPair?.ReferenceYearSetLabelEn ?? String.Empty;

            lblReferenceDateBeginCaption.Text =
                labels.TryGetValue(key: nameof(lblReferenceDateBeginCaption),
                out string lblReferenceDateBeginCaptionText)
                    ? lblReferenceDateBeginCaptionText
                    : String.Empty;

            lblReferenceDateBeginValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.ReferenceDateBeginText ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.ReferenceDateBeginText ?? String.Empty
                        : PanelReferenceYearSetPair?.ReferenceDateBeginText ?? String.Empty;

            lblReferenceDateEndCaption.Text =
                labels.TryGetValue(key: nameof(lblReferenceDateEndCaption),
                out string lblReferenceDateEndCaptionText)
                    ? lblReferenceDateEndCaptionText
                    : String.Empty;

            lblReferenceDateEndValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? PanelReferenceYearSetPair?.ReferenceDateEndText ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? PanelReferenceYearSetPair?.ReferenceDateEndText ?? String.Empty
                        : PanelReferenceYearSetPair?.ReferenceDateEndText ?? String.Empty;

            lblRefYearSetFullyWithinEstimationPeriodCaption.Text =
               labels.TryGetValue(key: nameof(lblRefYearSetFullyWithinEstimationPeriodCaption),
               out string lblRefYearSetFullyWithinEstimationPeriodCaptionText)
                   ? lblRefYearSetFullyWithinEstimationPeriodCaptionText
                   : String.Empty;

            lblRefYearSetFullyWithinEstimationPeriodValue.Text =
                (PanelReferenceYearSetPair?.RefYearSetFullyWithinEstimationPeriod == null)
                    ? Functions.StrNull
                    : (bool)PanelReferenceYearSetPair?.RefYearSetFullyWithinEstimationPeriod
                        ? strTrueText
                        : strFalseText;

            lblShareOfRefYearSetIntersectedByEstimationPeriodCaption.Text =
                labels.TryGetValue(key: nameof(lblShareOfRefYearSetIntersectedByEstimationPeriodCaption),
                out string lblShareOfRefYearSetIntersectedByEstimationPeriodCaptionText)
                    ? lblShareOfRefYearSetIntersectedByEstimationPeriodCaptionText
                    : String.Empty;

            lblShareOfRefYearSetIntersectedByEstimationPeriodValue.Text =
                    (PanelReferenceYearSetPair?.ShareOfRefYearSetIntersectedByEstimationPeriod == null)
                    ? Functions.StrNull
                    : ((double)PanelReferenceYearSetPair?.ShareOfRefYearSetIntersectedByEstimationPeriod)
                        .ToString(format: TFnApiGetDataOptionsForSinglePhaseConfigList
                            .ColShareOfRefYearSetIntersectedByEstimationPeriod.NumericFormat);

            lblShareOfEstimationPeriodIntersectedByRefYearSetCaption.Text =
                labels.TryGetValue(key: nameof(lblShareOfEstimationPeriodIntersectedByRefYearSetCaption),
                out string lblShareOfEstimationPeriodIntersectedByRefYearSetCaptionText)
                    ? lblShareOfEstimationPeriodIntersectedByRefYearSetCaptionText
                    : String.Empty;

            lblShareOfEstimationPeriodIntersectedByRefYearSetValue.Text =
                    (PanelReferenceYearSetPair?.ShareOfEstimationPeriodIntersectedByRefYearSet == null)
                    ? Functions.StrNull
                    : ((double)PanelReferenceYearSetPair?.ShareOfEstimationPeriodIntersectedByRefYearSet)
                        .ToString(format: TFnApiGetDataOptionsForSinglePhaseConfigList
                            .ColShareOfEstimationPeriodIntersectedByRefYearSet.NumericFormat);

            lblSSizeCaption.Text =
                labels.TryGetValue(key: nameof(lblSSizeCaption),
                out string lblSSizeCaptionText)
                    ? lblSSizeCaptionText
                    : String.Empty;

            lblSSizeValue.Text =
                    (PanelReferenceYearSetPair?.SSize == null)
                    ? Functions.StrNull
                    : ((int)PanelReferenceYearSetPair?.SSize).ToString();

            lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption.Text =
               labels.TryGetValue(key: nameof(lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaption),
               out string lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaptionText)
                   ? lblMaxSSizeWithinEstimationPeriodAndRefYearSetCaptionText
                   : String.Empty;

            lblMaxSSizeWithinEstimationPeriodAndRefYearSetValue.Text =
                (PanelReferenceYearSetPair?.MaxSSizeWithinEstimationPeriodAndRefYearSet == null)
                    ? Functions.StrNull
                    : (bool)PanelReferenceYearSetPair?.MaxSSizeWithinEstimationPeriodAndRefYearSet
                        ? strTrueText
                        : strFalseText;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">
        /// Nastaví barvu páru panelu a referenčního období
        /// podle časové shody referenčního období s obdobím odhadu
        /// </para>
        /// <para lang="en">
        /// Sets the color of the panel reference year set pair
        /// according to the time correspondence
        /// of the reference year set with the estimation period
        /// </para>
        /// </summary>
        private void SetBackgroundColor()
        {
            // Páry panelu a referenčního období
            // s časovou shodou s obdobím odhadu
            // jsou označeny zelenou, bez časové shody bílou
            BackColor = (PanelReferenceYearSetPair?.RefYearSetFullyWithinEstimationPeriod ?? false)
                ? Color.PaleGreen
                : Color.White;
        }

        /// <summary>
        /// <para lang="cs">
        /// Páry panelů a referenčního období, které jsou plně uvnitř výpočetního období (RefYearSetFullyWithinEstimationPeriod = TRUE)
        /// a s maximální velikostí výběru uvnitř výpočetního a referenčního období (MaxSSizeWithinEstimationPeriodAndRefYearSet = TRUE)
        /// jsou na formuláři předvybrané
        /// <para lang="en">
        /// The panels and reference-year sets fully within the estimation period (RefYearSetFullyWithinEstimationPeriod = TRUE)
        /// and with the maximum sample size (MaxSSizeWithinEstimationPeriodAndRefYearSet = TRUE) are to be preselected by nFIESTA GUI.
        /// <para lang="en">
        /// </summary>
        public void PreSelect()
        {
            if ((PanelReferenceYearSetPair?.RefYearSetFullyWithinEstimationPeriod ?? false) &&
                (PanelReferenceYearSetPair?.MaxSSizeWithinEstimationPeriodAndRefYearSet ?? false))
            {
                IsSelected = true;
            }
            else
            {
                IsSelected = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Zmenší vypis do stručné podoby</para>
        /// <para lang="en">Reduces the list to a brief form</para>
        /// </summary>
        private void Collapse()
        {
            tlpData.RowStyles[0].Height = 25;
            tlpData.RowStyles[1].Height = 0;
            tlpData.RowStyles[2].Height = 0;
            tlpData.RowStyles[3].Height = 0;
            tlpData.RowStyles[4].Height = 0;
            tlpData.RowStyles[5].Height = 0;
            Height = 25;
        }

        /// <summary>
        /// <para lang="cs">Zvětší výpis do podrobné podoby</para>
        /// <para lang="en">Expand the list into a detailed form</para>
        /// </summary>
        private void Expand()
        {
            tlpData.RowStyles[0].Height = 0;
            tlpData.RowStyles[1].Height = 25;
            tlpData.RowStyles[2].Height = 25;
            tlpData.RowStyles[3].Height = 25;
            tlpData.RowStyles[4].Height = 25;
            tlpData.RowStyles[5].Height = 25;
            Height = 125;
        }

        /// <summary>
        /// <para lang="cs">Skryje zaškrtávací políčko</para>
        /// <para lang="en">Hides the checkbox</para>
        /// </summary>
        private void HideCheckBox()
        {
            tlpMain.ColumnStyles[0].Width = 0;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí zaškrtávací políčko</para>
        /// <para lang="en">Displays the checkbox</para>
        /// </summary>
        private void ShowCheckBox()
        {
            tlpMain.ColumnStyles[0].Width = 25;
        }

        #endregion Methods

    }

}