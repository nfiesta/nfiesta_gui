﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormPanelRefYearSetGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlClose = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            splPanelRefYearSetPairs = new System.Windows.Forms.SplitContainer();
            splPanelRefYearSetGroups = new System.Windows.Forms.SplitContainer();
            grpPanelRefYearSetGroups = new System.Windows.Forms.GroupBox();
            lstPanelRefYearSetGroups = new System.Windows.Forms.ListBox();
            grpSelectedPanelRefYearSetGroup = new System.Windows.Forms.GroupBox();
            tlpSelectedPanelRefYearSetGroup = new System.Windows.Forms.TableLayoutPanel();
            tlpSelectedPanelRefYearSetGroupButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlUpdate = new System.Windows.Forms.Panel();
            btnUpdate = new System.Windows.Forms.Button();
            pnlDelete = new System.Windows.Forms.Panel();
            btnDelete = new System.Windows.Forms.Button();
            tlpSelectedPanelRefYearSetGroupColumns = new System.Windows.Forms.TableLayoutPanel();
            lblIdValue = new System.Windows.Forms.Label();
            lblIdCaption = new System.Windows.Forms.Label();
            lblDescriptionEnCaption = new System.Windows.Forms.Label();
            txtDescriptionEnValue = new System.Windows.Forms.TextBox();
            lblLabelEnCaption = new System.Windows.Forms.Label();
            txtLabelEnValue = new System.Windows.Forms.TextBox();
            lblDescriptionCsCaption = new System.Windows.Forms.Label();
            txtDescriptionCsValue = new System.Windows.Forms.TextBox();
            lblLabelCsCaption = new System.Windows.Forms.Label();
            txtLabelCsValue = new System.Windows.Forms.TextBox();
            pnlPanelRefYearSetPairs = new System.Windows.Forms.Panel();
            tlpPanels = new System.Windows.Forms.TableLayoutPanel();
            lblRowsCount = new System.Windows.Forms.Label();
            grpPanels = new System.Windows.Forms.GroupBox();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlClose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splPanelRefYearSetPairs).BeginInit();
            splPanelRefYearSetPairs.Panel1.SuspendLayout();
            splPanelRefYearSetPairs.Panel2.SuspendLayout();
            splPanelRefYearSetPairs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splPanelRefYearSetGroups).BeginInit();
            splPanelRefYearSetGroups.Panel1.SuspendLayout();
            splPanelRefYearSetGroups.Panel2.SuspendLayout();
            splPanelRefYearSetGroups.SuspendLayout();
            grpPanelRefYearSetGroups.SuspendLayout();
            grpSelectedPanelRefYearSetGroup.SuspendLayout();
            tlpSelectedPanelRefYearSetGroup.SuspendLayout();
            tlpSelectedPanelRefYearSetGroupButtons.SuspendLayout();
            pnlUpdate.SuspendLayout();
            pnlDelete.SuspendLayout();
            tlpSelectedPanelRefYearSetGroupColumns.SuspendLayout();
            pnlPanelRefYearSetPairs.SuspendLayout();
            tlpPanels.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(splPanelRefYearSetPairs, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 0;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlClose, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 455);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 46);
            tlpButtons.TabIndex = 3;
            // 
            // pnlClose
            // 
            pnlClose.Controls.Add(btnClose);
            pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClose.Location = new System.Drawing.Point(757, 0);
            pnlClose.Margin = new System.Windows.Forms.Padding(0);
            pnlClose.Name = "pnlClose";
            pnlClose.Padding = new System.Windows.Forms.Padding(6);
            pnlClose.Size = new System.Drawing.Size(187, 46);
            pnlClose.TabIndex = 16;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnClose.Location = new System.Drawing.Point(6, 6);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(175, 34);
            btnClose.TabIndex = 15;
            btnClose.Text = "btnClose";
            btnClose.UseVisualStyleBackColor = true;
            // 
            // splPanelRefYearSetPairs
            // 
            splPanelRefYearSetPairs.Dock = System.Windows.Forms.DockStyle.Fill;
            splPanelRefYearSetPairs.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splPanelRefYearSetPairs.Location = new System.Drawing.Point(0, 0);
            splPanelRefYearSetPairs.Margin = new System.Windows.Forms.Padding(0);
            splPanelRefYearSetPairs.Name = "splPanelRefYearSetPairs";
            splPanelRefYearSetPairs.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splPanelRefYearSetPairs.Panel1
            // 
            splPanelRefYearSetPairs.Panel1.Controls.Add(splPanelRefYearSetGroups);
            // 
            // splPanelRefYearSetPairs.Panel2
            // 
            splPanelRefYearSetPairs.Panel2.Controls.Add(pnlPanelRefYearSetPairs);
            splPanelRefYearSetPairs.Size = new System.Drawing.Size(944, 455);
            splPanelRefYearSetPairs.SplitterDistance = 288;
            splPanelRefYearSetPairs.SplitterWidth = 5;
            splPanelRefYearSetPairs.TabIndex = 0;
            // 
            // splPanelRefYearSetGroups
            // 
            splPanelRefYearSetGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            splPanelRefYearSetGroups.Location = new System.Drawing.Point(0, 0);
            splPanelRefYearSetGroups.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splPanelRefYearSetGroups.Name = "splPanelRefYearSetGroups";
            // 
            // splPanelRefYearSetGroups.Panel1
            // 
            splPanelRefYearSetGroups.Panel1.Controls.Add(grpPanelRefYearSetGroups);
            // 
            // splPanelRefYearSetGroups.Panel2
            // 
            splPanelRefYearSetGroups.Panel2.Controls.Add(grpSelectedPanelRefYearSetGroup);
            splPanelRefYearSetGroups.Size = new System.Drawing.Size(944, 288);
            splPanelRefYearSetGroups.SplitterDistance = 249;
            splPanelRefYearSetGroups.SplitterWidth = 5;
            splPanelRefYearSetGroups.TabIndex = 1;
            // 
            // grpPanelRefYearSetGroups
            // 
            grpPanelRefYearSetGroups.Controls.Add(lstPanelRefYearSetGroups);
            grpPanelRefYearSetGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanelRefYearSetGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpPanelRefYearSetGroups.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpPanelRefYearSetGroups.Location = new System.Drawing.Point(0, 0);
            grpPanelRefYearSetGroups.Margin = new System.Windows.Forms.Padding(0);
            grpPanelRefYearSetGroups.Name = "grpPanelRefYearSetGroups";
            grpPanelRefYearSetGroups.Padding = new System.Windows.Forms.Padding(6);
            grpPanelRefYearSetGroups.Size = new System.Drawing.Size(249, 288);
            grpPanelRefYearSetGroups.TabIndex = 0;
            grpPanelRefYearSetGroups.TabStop = false;
            grpPanelRefYearSetGroups.Text = "grpPanelRefYearSetGroups";
            // 
            // lstPanelRefYearSetGroups
            // 
            lstPanelRefYearSetGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            lstPanelRefYearSetGroups.FormattingEnabled = true;
            lstPanelRefYearSetGroups.Location = new System.Drawing.Point(6, 21);
            lstPanelRefYearSetGroups.Margin = new System.Windows.Forms.Padding(0);
            lstPanelRefYearSetGroups.Name = "lstPanelRefYearSetGroups";
            lstPanelRefYearSetGroups.Size = new System.Drawing.Size(237, 261);
            lstPanelRefYearSetGroups.Sorted = true;
            lstPanelRefYearSetGroups.TabIndex = 0;
            // 
            // grpSelectedPanelRefYearSetGroup
            // 
            grpSelectedPanelRefYearSetGroup.BackColor = System.Drawing.SystemColors.Control;
            grpSelectedPanelRefYearSetGroup.Controls.Add(tlpSelectedPanelRefYearSetGroup);
            grpSelectedPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSelectedPanelRefYearSetGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpSelectedPanelRefYearSetGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSelectedPanelRefYearSetGroup.Location = new System.Drawing.Point(0, 0);
            grpSelectedPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            grpSelectedPanelRefYearSetGroup.Name = "grpSelectedPanelRefYearSetGroup";
            grpSelectedPanelRefYearSetGroup.Padding = new System.Windows.Forms.Padding(6, 17, 6, 6);
            grpSelectedPanelRefYearSetGroup.Size = new System.Drawing.Size(690, 288);
            grpSelectedPanelRefYearSetGroup.TabIndex = 1;
            grpSelectedPanelRefYearSetGroup.TabStop = false;
            grpSelectedPanelRefYearSetGroup.Text = "grpSelectedPanelRefYearSetGroup";
            // 
            // tlpSelectedPanelRefYearSetGroup
            // 
            tlpSelectedPanelRefYearSetGroup.ColumnCount = 1;
            tlpSelectedPanelRefYearSetGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedPanelRefYearSetGroup.Controls.Add(tlpSelectedPanelRefYearSetGroupButtons, 0, 1);
            tlpSelectedPanelRefYearSetGroup.Controls.Add(tlpSelectedPanelRefYearSetGroupColumns, 0, 0);
            tlpSelectedPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSelectedPanelRefYearSetGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpSelectedPanelRefYearSetGroup.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpSelectedPanelRefYearSetGroup.Location = new System.Drawing.Point(6, 32);
            tlpSelectedPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            tlpSelectedPanelRefYearSetGroup.Name = "tlpSelectedPanelRefYearSetGroup";
            tlpSelectedPanelRefYearSetGroup.RowCount = 2;
            tlpSelectedPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpSelectedPanelRefYearSetGroup.Size = new System.Drawing.Size(678, 250);
            tlpSelectedPanelRefYearSetGroup.TabIndex = 0;
            // 
            // tlpSelectedPanelRefYearSetGroupButtons
            // 
            tlpSelectedPanelRefYearSetGroupButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpSelectedPanelRefYearSetGroupButtons.ColumnCount = 3;
            tlpSelectedPanelRefYearSetGroupButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedPanelRefYearSetGroupButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpSelectedPanelRefYearSetGroupButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpSelectedPanelRefYearSetGroupButtons.Controls.Add(pnlUpdate, 2, 0);
            tlpSelectedPanelRefYearSetGroupButtons.Controls.Add(pnlDelete, 1, 0);
            tlpSelectedPanelRefYearSetGroupButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSelectedPanelRefYearSetGroupButtons.Location = new System.Drawing.Point(0, 204);
            tlpSelectedPanelRefYearSetGroupButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpSelectedPanelRefYearSetGroupButtons.Name = "tlpSelectedPanelRefYearSetGroupButtons";
            tlpSelectedPanelRefYearSetGroupButtons.RowCount = 1;
            tlpSelectedPanelRefYearSetGroupButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedPanelRefYearSetGroupButtons.Size = new System.Drawing.Size(678, 46);
            tlpSelectedPanelRefYearSetGroupButtons.TabIndex = 4;
            // 
            // pnlUpdate
            // 
            pnlUpdate.Controls.Add(btnUpdate);
            pnlUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlUpdate.Location = new System.Drawing.Point(491, 0);
            pnlUpdate.Margin = new System.Windows.Forms.Padding(0);
            pnlUpdate.Name = "pnlUpdate";
            pnlUpdate.Padding = new System.Windows.Forms.Padding(6);
            pnlUpdate.Size = new System.Drawing.Size(187, 46);
            pnlUpdate.TabIndex = 16;
            // 
            // btnUpdate
            // 
            btnUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnUpdate.Location = new System.Drawing.Point(6, 6);
            btnUpdate.Margin = new System.Windows.Forms.Padding(0);
            btnUpdate.Name = "btnUpdate";
            btnUpdate.Size = new System.Drawing.Size(175, 34);
            btnUpdate.TabIndex = 15;
            btnUpdate.Text = "btnUpdate";
            btnUpdate.UseVisualStyleBackColor = true;
            // 
            // pnlDelete
            // 
            pnlDelete.Controls.Add(btnDelete);
            pnlDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDelete.Location = new System.Drawing.Point(304, 0);
            pnlDelete.Margin = new System.Windows.Forms.Padding(0);
            pnlDelete.Name = "pnlDelete";
            pnlDelete.Padding = new System.Windows.Forms.Padding(6);
            pnlDelete.Size = new System.Drawing.Size(187, 46);
            pnlDelete.TabIndex = 15;
            // 
            // btnDelete
            // 
            btnDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnDelete.Location = new System.Drawing.Point(6, 6);
            btnDelete.Margin = new System.Windows.Forms.Padding(0);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new System.Drawing.Size(175, 34);
            btnDelete.TabIndex = 16;
            btnDelete.Text = "btnDelete";
            btnDelete.UseVisualStyleBackColor = true;
            // 
            // tlpSelectedPanelRefYearSetGroupColumns
            // 
            tlpSelectedPanelRefYearSetGroupColumns.ColumnCount = 2;
            tlpSelectedPanelRefYearSetGroupColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            tlpSelectedPanelRefYearSetGroupColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(lblIdValue, 1, 0);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(lblIdCaption, 0, 0);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(lblDescriptionEnCaption, 0, 4);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(txtDescriptionEnValue, 1, 4);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(lblLabelEnCaption, 0, 3);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(txtLabelEnValue, 1, 3);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(lblDescriptionCsCaption, 0, 2);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(txtDescriptionCsValue, 1, 2);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(lblLabelCsCaption, 0, 1);
            tlpSelectedPanelRefYearSetGroupColumns.Controls.Add(txtLabelCsValue, 1, 1);
            tlpSelectedPanelRefYearSetGroupColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSelectedPanelRefYearSetGroupColumns.Location = new System.Drawing.Point(0, 0);
            tlpSelectedPanelRefYearSetGroupColumns.Margin = new System.Windows.Forms.Padding(0);
            tlpSelectedPanelRefYearSetGroupColumns.Name = "tlpSelectedPanelRefYearSetGroupColumns";
            tlpSelectedPanelRefYearSetGroupColumns.RowCount = 6;
            tlpSelectedPanelRefYearSetGroupColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSelectedPanelRefYearSetGroupColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSelectedPanelRefYearSetGroupColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSelectedPanelRefYearSetGroupColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSelectedPanelRefYearSetGroupColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSelectedPanelRefYearSetGroupColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedPanelRefYearSetGroupColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpSelectedPanelRefYearSetGroupColumns.Size = new System.Drawing.Size(678, 204);
            tlpSelectedPanelRefYearSetGroupColumns.TabIndex = 0;
            // 
            // lblIdValue
            // 
            lblIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdValue.ForeColor = System.Drawing.SystemColors.ControlText;
            lblIdValue.Location = new System.Drawing.Point(140, 0);
            lblIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblIdValue.Name = "lblIdValue";
            lblIdValue.Size = new System.Drawing.Size(538, 29);
            lblIdValue.TabIndex = 3;
            lblIdValue.Text = "lblIdValue";
            // 
            // lblIdCaption
            // 
            lblIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblIdCaption.Location = new System.Drawing.Point(0, 0);
            lblIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblIdCaption.Name = "lblIdCaption";
            lblIdCaption.Size = new System.Drawing.Size(140, 29);
            lblIdCaption.TabIndex = 0;
            lblIdCaption.Text = "lblIdCaption";
            // 
            // lblDescriptionEnCaption
            // 
            lblDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEnCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDescriptionEnCaption.Location = new System.Drawing.Point(0, 116);
            lblDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEnCaption.Name = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.Size = new System.Drawing.Size(140, 29);
            lblDescriptionEnCaption.TabIndex = 7;
            lblDescriptionEnCaption.Text = "lblDescriptionEnCaption";
            // 
            // txtDescriptionEnValue
            // 
            txtDescriptionEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEnValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtDescriptionEnValue.Location = new System.Drawing.Point(140, 116);
            txtDescriptionEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEnValue.Name = "txtDescriptionEnValue";
            txtDescriptionEnValue.Size = new System.Drawing.Size(538, 20);
            txtDescriptionEnValue.TabIndex = 9;
            // 
            // lblLabelEnCaption
            // 
            lblLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblLabelEnCaption.Location = new System.Drawing.Point(0, 87);
            lblLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEnCaption.Name = "lblLabelEnCaption";
            lblLabelEnCaption.Size = new System.Drawing.Size(140, 29);
            lblLabelEnCaption.TabIndex = 6;
            lblLabelEnCaption.Text = "lblLabelEnCaption";
            // 
            // txtLabelEnValue
            // 
            txtLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtLabelEnValue.Location = new System.Drawing.Point(140, 87);
            txtLabelEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEnValue.Name = "txtLabelEnValue";
            txtLabelEnValue.Size = new System.Drawing.Size(538, 20);
            txtLabelEnValue.TabIndex = 8;
            // 
            // lblDescriptionCsCaption
            // 
            lblDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCsCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDescriptionCsCaption.Location = new System.Drawing.Point(0, 58);
            lblDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCsCaption.Name = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.Size = new System.Drawing.Size(140, 29);
            lblDescriptionCsCaption.TabIndex = 2;
            lblDescriptionCsCaption.Text = "lblDescriptionCsCaption";
            // 
            // txtDescriptionCsValue
            // 
            txtDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCsValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtDescriptionCsValue.Location = new System.Drawing.Point(140, 58);
            txtDescriptionCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCsValue.Name = "txtDescriptionCsValue";
            txtDescriptionCsValue.Size = new System.Drawing.Size(538, 20);
            txtDescriptionCsValue.TabIndex = 5;
            // 
            // lblLabelCsCaption
            // 
            lblLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCsCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblLabelCsCaption.Location = new System.Drawing.Point(0, 29);
            lblLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCsCaption.Name = "lblLabelCsCaption";
            lblLabelCsCaption.Size = new System.Drawing.Size(140, 29);
            lblLabelCsCaption.TabIndex = 1;
            lblLabelCsCaption.Text = "lblLabelCsCaption";
            // 
            // txtLabelCsValue
            // 
            txtLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCsValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtLabelCsValue.Location = new System.Drawing.Point(140, 29);
            txtLabelCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCsValue.Name = "txtLabelCsValue";
            txtLabelCsValue.Size = new System.Drawing.Size(538, 20);
            txtLabelCsValue.TabIndex = 4;
            // 
            // pnlPanelRefYearSetPairs
            // 
            pnlPanelRefYearSetPairs.Controls.Add(tlpPanels);
            pnlPanelRefYearSetPairs.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPanelRefYearSetPairs.Location = new System.Drawing.Point(0, 0);
            pnlPanelRefYearSetPairs.Margin = new System.Windows.Forms.Padding(0);
            pnlPanelRefYearSetPairs.Name = "pnlPanelRefYearSetPairs";
            pnlPanelRefYearSetPairs.Size = new System.Drawing.Size(944, 162);
            pnlPanelRefYearSetPairs.TabIndex = 0;
            // 
            // tlpPanels
            // 
            tlpPanels.ColumnCount = 1;
            tlpPanels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanels.Controls.Add(lblRowsCount, 0, 1);
            tlpPanels.Controls.Add(grpPanels, 0, 0);
            tlpPanels.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpPanels.Location = new System.Drawing.Point(0, 0);
            tlpPanels.Name = "tlpPanels";
            tlpPanels.RowCount = 2;
            tlpPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpPanels.Size = new System.Drawing.Size(944, 162);
            tlpPanels.TabIndex = 0;
            // 
            // lblRowsCount
            // 
            lblRowsCount.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowsCount.ForeColor = System.Drawing.SystemColors.ControlText;
            lblRowsCount.Location = new System.Drawing.Point(0, 139);
            lblRowsCount.Margin = new System.Windows.Forms.Padding(0);
            lblRowsCount.Name = "lblRowsCount";
            lblRowsCount.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            lblRowsCount.Size = new System.Drawing.Size(944, 23);
            lblRowsCount.TabIndex = 2;
            lblRowsCount.Text = "lblRowsCount";
            // 
            // grpPanels
            // 
            grpPanels.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanels.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpPanels.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpPanels.Location = new System.Drawing.Point(0, 0);
            grpPanels.Margin = new System.Windows.Forms.Padding(0);
            grpPanels.Name = "grpPanels";
            grpPanels.Padding = new System.Windows.Forms.Padding(6);
            grpPanels.Size = new System.Drawing.Size(944, 139);
            grpPanels.TabIndex = 1;
            grpPanels.TabStop = false;
            grpPanels.Text = "grpPanels";
            // 
            // FormPanelRefYearSetGroup
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnClose;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormPanelRefYearSetGroup";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormPanelRefYearSetGroup";
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlClose.ResumeLayout(false);
            splPanelRefYearSetPairs.Panel1.ResumeLayout(false);
            splPanelRefYearSetPairs.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splPanelRefYearSetPairs).EndInit();
            splPanelRefYearSetPairs.ResumeLayout(false);
            splPanelRefYearSetGroups.Panel1.ResumeLayout(false);
            splPanelRefYearSetGroups.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splPanelRefYearSetGroups).EndInit();
            splPanelRefYearSetGroups.ResumeLayout(false);
            grpPanelRefYearSetGroups.ResumeLayout(false);
            grpSelectedPanelRefYearSetGroup.ResumeLayout(false);
            tlpSelectedPanelRefYearSetGroup.ResumeLayout(false);
            tlpSelectedPanelRefYearSetGroupButtons.ResumeLayout(false);
            pnlUpdate.ResumeLayout(false);
            pnlDelete.ResumeLayout(false);
            tlpSelectedPanelRefYearSetGroupColumns.ResumeLayout(false);
            tlpSelectedPanelRefYearSetGroupColumns.PerformLayout();
            pnlPanelRefYearSetPairs.ResumeLayout(false);
            tlpPanels.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.SplitContainer splPanelRefYearSetPairs;
        private System.Windows.Forms.SplitContainer splPanelRefYearSetGroups;
        private System.Windows.Forms.GroupBox grpPanelRefYearSetGroups;
        private System.Windows.Forms.GroupBox grpSelectedPanelRefYearSetGroup;
        private System.Windows.Forms.TableLayoutPanel tlpSelectedPanelRefYearSetGroup;
        private System.Windows.Forms.TableLayoutPanel tlpSelectedPanelRefYearSetGroupColumns;
        private System.Windows.Forms.TextBox txtDescriptionCsValue;
        private System.Windows.Forms.Label lblIdValue;
        private System.Windows.Forms.Label lblIdCaption;
        private System.Windows.Forms.Label lblLabelCsCaption;
        private System.Windows.Forms.Label lblDescriptionCsCaption;
        private System.Windows.Forms.TextBox txtLabelCsValue;
        private System.Windows.Forms.ListBox lstPanelRefYearSetGroups;
        private System.Windows.Forms.Label lblLabelEnCaption;
        private System.Windows.Forms.Label lblDescriptionEnCaption;
        private System.Windows.Forms.TextBox txtLabelEnValue;
        private System.Windows.Forms.TextBox txtDescriptionEnValue;
        private System.Windows.Forms.Panel pnlPanelRefYearSetPairs;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TableLayoutPanel tlpSelectedPanelRefYearSetGroupButtons;
        private System.Windows.Forms.Panel pnlUpdate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Panel pnlDelete;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox grpPanels;
        private System.Windows.Forms.TableLayoutPanel tlpPanels;
        private System.Windows.Forms.Label lblRowsCount;
    }

}