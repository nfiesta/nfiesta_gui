﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba jednotky a typu odhadu"</para>
    /// <para lang="en">Control "Unit and estimate type selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlUnit
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        // Výsledkem výběru v této komponentě jsou
        // vybraná jednotka, vybraný indikátor a jeho metadata pro jmenovatel
        // The result of the selection in this component are
        // selected unit of measure, selected indicator and its metadata for denominator

        /// <summary>
        /// <para lang="cs">Vybraná jednotka</para>
        /// <para lang="en">Selected unit of measure</para>
        /// </summary>
        private Unit selectedUnitOfMeasure;

        /// <summary>
        /// <para lang="cs">Vybraný indikátor jmenovatele</para>
        /// <para lang="en">Selected indicator for denominator</para>
        /// </summary>
        private ITargetVariable selectedDenominatorTargetVariable;

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru jmenovatele</para>
        /// <para lang="en">Selected indicator metadata for denominator</para>
        /// </summary>
        private ITargetVariableMetadata selectedDenominatorTargetVariableMetadata;

        private string strIndicator = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení nadpisu</para>
        /// <para lang="en">Control for display caption</para>
        /// </summary>
        private ControlCaption ctrCaption;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba indikátoru" pro jmenovatel</para>
        /// <para lang="en">Control "Indicator selection" for denominator</para>
        /// </summary>
        private ControlTargetVariable ctrDenominatorTargetVariable;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlUnit(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraný indikátor čitatele (read-only)</para>
        /// <para lang="en">Selected indicator for numerator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedTargetVariable
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrTargetVariable
                    .SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru čitatele (read-only)</para>
        /// <para lang="en">Selected indicator metadata for numerator (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedTargetVariableMetadata
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrTargetVariable
                    .SelectedTargetVariableMetadata;
            }
        }

        // Výsledkem výběru v této komponentě jsou
        // vybraná jednotka, vybraný indikátor a jeho metadata pro jmenovatel
        // The result of the selection in this component are
        // selected unit of measure, selected indicator and its metadata for denominator

        /// <summary>
        /// <para lang="cs">Vybraná jednotka</para>
        /// <para lang="en">Selected unit of measure</para>
        /// </summary>
        public Unit SelectedUnitOfMeasure
        {
            get
            {
                return selectedUnitOfMeasure;
            }
            set
            {
                selectedUnitOfMeasure = value;
                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný indikátor jmenovatele</para>
        /// <para lang="en">Selected indicator for denominator</para>
        /// </summary>
        public ITargetVariable SelectedDenominatorTargetVariable
        {
            get
            {
                return selectedDenominatorTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru jmenovatele</para>
        /// <para lang="en">Selected indicator metadata for denominator</para>
        /// </summary>
        public ITargetVariableMetadata SelectedDenominatorTargetVariableMetadata
        {
            get
            {
                return selectedDenominatorTargetVariableMetadata;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)</para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)</para>
        /// </summary>
        public DisplayType DisplayType
        {
            get
            {
                return
                    (cboSelectedUnitOfMeasure.SelectedItem == null)
                    ? DisplayType.Total
                    : (cboSelectedUnitOfMeasure.SelectedItem is not Unit)
                        ? DisplayType.Total
                        : ((Unit)cboSelectedUnitOfMeasure.SelectedItem).IsTotal
                            ? DisplayType.Total
                            : DisplayType.Ratio;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                "Předchozí" },
                        { nameof(btnNext),                    "Další" },
                        { nameof(cboSelectedUnitOfMeasure),   "LabelCs" },
                        { nameof(strIndicator),               "Indikátor:" },
                        { nameof(grpNumerator),               "Čitatel:" },
                        { nameof(grpDenominator),             "Jmenovatel:" },
                        { nameof(grpSelectedUnitOfMeasure),   "Jednotka:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlUnit),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                "Previous" },
                        { nameof(btnNext),                    "Next" },
                        { nameof(cboSelectedUnitOfMeasure),   "LabelEn" },
                        { nameof(strIndicator),               "Indicator:" },
                        { nameof(grpNumerator),               "Numerator:" },
                        { nameof(grpDenominator),             "Denominator:" },
                        { nameof(grpSelectedUnitOfMeasure),   "Unit:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlUnit),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            SelectedUnitOfMeasure = null;
            selectedDenominatorTargetVariable = null;
            selectedDenominatorTargetVariableMetadata = null;

            InitializeCaption();

            InitializeControlDenominatorTargetVariable();

            Visible = (Database != null) &&
               (Database.Postgres != null) &&
               Database.Postgres.Initialized;

            InitializeLabels();

            EnableButtonNext();

            onEdit = true;

            pnlMain.BackColor = Setting.BorderColor;
            pnlWorkSpace.BackColor = Setting.BorderColor;

            splCaption.BackColor = Setting.SpliterColor;
            splCaption.SplitterDistance = Setting.SplUnitCaptionDistance;

            onEdit = false;

            btnPrevious.Click += new EventHandler(
              (sender, e) =>
              {
                  PreviousClick?.Invoke(
                     sender: this,
                     e: new EventArgs());
              });

            btnNext.Click += new EventHandler(
                (sender, e) =>
                {
                    selectedDenominatorTargetVariable =
                        ctrDenominatorTargetVariable.CtrTargetVariableSelector.SelectedTargetVariable;
                    selectedDenominatorTargetVariableMetadata =
                        ctrDenominatorTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata;
                    NextClick?.Invoke(
                       sender: this,
                       e: new EventArgs());
                });

            cboSelectedUnitOfMeasure.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        UnitOfMeasureSelected();
                    }
                });

            splCaption.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplUnitCaptionDistance =
                            splCaption.SplitterDistance;
                    }
                });

            Resize += new EventHandler((sender, e) => { ReDraw(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku nadpisu</para>
        /// <para lang="en">Initialization of the control for display caption</para>
        /// </summary>
        private void InitializeCaption()
        {
            pnlCaption.Controls.Clear();
            ctrCaption = new ControlCaption(
                controlOwner: this,
                numeratorTargetVariableMetadata: SelectedTargetVariableMetadata,
                denominatorTargetVariableMetadata: null,
                variablePairs: null,
                captionVersion: CaptionVersion.Extended,
                withAttributeCategories: false,
                withIdentifiers: true)
            {
                Dock = DockStyle.Top
            };
            pnlCaption.Controls.Add(value: ctrCaption);
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku pro výběr indikátoru jmenovatele</para>
        /// <para lang="en">Initialization of the control for display indicator of denominator</para>
        /// </summary>
        private void InitializeControlDenominatorTargetVariable()
        {
            grpDenominator.Visible = false;
            grpDenominator.Controls.Clear();
            ctrDenominatorTargetVariable = new ControlTargetVariable(
                controlOwner: ControlOwner)
            {
                Dock = DockStyle.Fill,
                ForeColor = System.Drawing.SystemColors.ControlText
            };
            ctrDenominatorTargetVariable.LoadContent();
            ctrDenominatorTargetVariable.HideButtonsPanel();
            ctrDenominatorTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableChanged +=
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        DenominatorTargetVariableSelected();
                    }
                };
            ctrDenominatorTargetVariable.CreateControl();
            grpDenominator.Controls.Add(value: ctrDenominatorTargetVariable);
            grpDenominator.Visible = DisplayType == DisplayType.Ratio;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            grpDenominator.Visible = false;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            onEdit = true;
            cboSelectedUnitOfMeasure.DisplayMember =
                labels.TryGetValue(key: nameof(cboSelectedUnitOfMeasure),
                out string cboSelectedUnitOfMeasureText)
                    ? cboSelectedUnitOfMeasureText
                    : String.Empty;
            onEdit = false;

            strIndicator =
                labels.TryGetValue(key: nameof(strIndicator),
                out strIndicator)
                    ? strIndicator
                    : String.Empty;

            grpNumerator.Text =
                (SelectedUnitOfMeasure == null)
                    ? strIndicator
                    : SelectedUnitOfMeasure.IsTotal
                        ? strIndicator
                        : SelectedUnitOfMeasure.IsRatio
                            ? (labels.TryGetValue(key: nameof(grpNumerator),
                                out string grpNumeratorText)
                                    ? grpNumeratorText
                                    : String.Empty)
                            : strIndicator;

            grpDenominator.Text =
                labels.TryGetValue(key: nameof(grpDenominator),
                    out string grpDenominatorText)
                        ? grpDenominatorText
                        : String.Empty;

            grpSelectedUnitOfMeasure.Text =
               labels.TryGetValue(key: nameof(grpSelectedUnitOfMeasure),
                    out string grpSelectedUnitOfMeasureText)
                        ? grpSelectedUnitOfMeasureText
                        : String.Empty;

            ctrCaption?.InitializeLabels();

            ctrDenominatorTargetVariable?.InitializeLabels();

            grpDenominator.Visible = DisplayType == DisplayType.Ratio;
        }

        /// <summary>
        /// <para lang="cs">
        /// Povolí nebo zakáže přístup na další komponentu,
        /// podle toho, zda je nebo není vybraná jednotka,
        /// indikátor čitatele a indikátor jmenovatele
        /// </para>
        /// <para lang="en">
        /// Enables or disables access to the next component
        /// depending on whether or not the unit
        /// indicator for numerator and indicator for denominator are selected
        /// </para>
        /// </summary>
        private void EnableButtonNext()
        {
            if (SelectedUnitOfMeasure != null)
            {
                if (SelectedUnitOfMeasure.IsTotal)
                {
                    btnNext.Enabled =
                        (SelectedUnitOfMeasure != null) &&
                        (SelectedTargetVariable != null) &&
                        (SelectedTargetVariableMetadata != null);
                }
                else
                {
                    btnNext.Enabled =
                       (SelectedUnitOfMeasure != null) &&
                       (SelectedTargetVariable != null) &&
                       (SelectedTargetVariableMetadata != null) &&
                       (ctrDenominatorTargetVariable.CtrTargetVariableSelector.SelectedTargetVariable != null) &&
                       (ctrDenominatorTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata != null);
                }
            }
            else
            {
                btnNext.Enabled = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            MetadataElement numUnit
                = SelectedTargetVariableMetadata
                .MetadataElements[MetadataElementType.Unit];

            Database.SNfiEsta.CTargetVariableMetadata.ReLoad(
                cTargetVariable: Database.SNfiEsta.CTargetVariable);

            Database.SNfiEsta.CAggregateTargetVariableMetadata = new TargetVariableMetadataList(
                database: Database,
                data: Database.SNfiEsta.CTargetVariableMetadata.Aggregated().Data);

            IEnumerable<MetadataElement> denomUnits
                = Database.SNfiEsta.CAggregateTargetVariableMetadata.Items
                    .Select(a => a.MetadataElements[MetadataElementType.Unit])
                    .Distinct();

            List<Unit> units = [];
            units.Add(
                item: new Unit(
                    database: Database,
                    numerator: numUnit));
            foreach (MetadataElement denomUnit in denomUnits)
            {
                units.Add(
                    item: new Unit(
                        database: Database,
                        numerator: numUnit,
                        denominator: denomUnit));
            }

            onEdit = true;

            cboSelectedUnitOfMeasure.Items.Clear();
            foreach (Unit unit in units)
            {
                cboSelectedUnitOfMeasure.Items.Add(item: unit);
            }

            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            cboSelectedUnitOfMeasure.Text =
                labels.TryGetValue(key: nameof(cboSelectedUnitOfMeasure),
                    out string cboSelectedUnitOfMeasureText)
                        ? cboSelectedUnitOfMeasureText
                        : String.Empty;

            onEdit = false;

            if (cboSelectedUnitOfMeasure.Items.OfType<Unit>().Any())
            {
                cboSelectedUnitOfMeasure.SelectedIndex = 0;
            }

            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Spouští se po provedeném výběru jednotky</para>
        /// <para lang="en">Method starts after the unit selection is made</para>
        /// </summary>
        private void UnitOfMeasureSelected()
        {
            if (cboSelectedUnitOfMeasure.SelectedItem != null)
            {
                SelectedUnitOfMeasure = (Unit)cboSelectedUnitOfMeasure.SelectedItem;
                if (SelectedUnitOfMeasure.IsRatio)
                {
                    ctrDenominatorTargetVariable.SelectedUnitOfMeasure =
                        new Unit(
                            database: Database,
                            numerator: SelectedUnitOfMeasure.Denominator);
                }
            }
            else
            {
                SelectedUnitOfMeasure = null;
            }
            EnableButtonNext();
            grpDenominator.Visible = DisplayType == DisplayType.Ratio;
        }

        /// <summary>
        /// <para lang="cs">Spouští se po provedeném výběru indikátoru jmenovatele</para>
        /// <para lang="en">Method starts after selection of the denominator indicator</para>
        /// </summary>
        private void DenominatorTargetVariableSelected()
        {
            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Překreslení ovládacího prvku</para>
        /// <para lang="en">Redraw user control</para>
        /// </summary>
        public void ReDraw()
        {
            grpDenominator.Visible = false;
            foreach (ControlTargetVariable selector
                in grpDenominator.Controls.OfType<ControlTargetVariable>())
            {
                selector.ReDraw();
            }
            grpDenominator.Visible = DisplayType == DisplayType.Ratio;
            grpDenominator.Refresh();
        }

        #endregion Methods

    }

}