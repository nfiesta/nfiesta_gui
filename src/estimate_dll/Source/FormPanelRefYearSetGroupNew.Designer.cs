﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormPanelRefYearSetGroupNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpNewPanelRefYearSetGroup = new System.Windows.Forms.GroupBox();
            tlpNewPanelRefYearSetGroup = new System.Windows.Forms.TableLayoutPanel();
            lblDescriptionEnCaption = new System.Windows.Forms.Label();
            lblLabelEnCaption = new System.Windows.Forms.Label();
            lblDescriptionCsCaption = new System.Windows.Forms.Label();
            lblLabelCsCaption = new System.Windows.Forms.Label();
            txtDescriptionEnValue = new System.Windows.Forms.TextBox();
            txtLabelEnValue = new System.Windows.Forms.TextBox();
            txtDescriptionCsValue = new System.Windows.Forms.TextBox();
            txtLabelCsValue = new System.Windows.Forms.TextBox();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            grpNewPanelRefYearSetGroup.SuspendLayout();
            tlpNewPanelRefYearSetGroup.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlOK.SuspendLayout();
            pnlCancel.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.Transparent;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpNewPanelRefYearSetGroup, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 0;
            // 
            // grpNewPanelRefYearSetGroup
            // 
            grpNewPanelRefYearSetGroup.BackColor = System.Drawing.Color.Transparent;
            grpNewPanelRefYearSetGroup.Controls.Add(tlpNewPanelRefYearSetGroup);
            grpNewPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            grpNewPanelRefYearSetGroup.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpNewPanelRefYearSetGroup.ForeColor = System.Drawing.Color.MediumBlue;
            grpNewPanelRefYearSetGroup.Location = new System.Drawing.Point(0, 0);
            grpNewPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            grpNewPanelRefYearSetGroup.Name = "grpNewPanelRefYearSetGroup";
            grpNewPanelRefYearSetGroup.Padding = new System.Windows.Forms.Padding(5);
            grpNewPanelRefYearSetGroup.Size = new System.Drawing.Size(944, 461);
            grpNewPanelRefYearSetGroup.TabIndex = 4;
            grpNewPanelRefYearSetGroup.TabStop = false;
            grpNewPanelRefYearSetGroup.Text = "grpNewPanelRefYearSetGroup";
            // 
            // tlpNewPanelRefYearSetGroup
            // 
            tlpNewPanelRefYearSetGroup.BackColor = System.Drawing.Color.Transparent;
            tlpNewPanelRefYearSetGroup.ColumnCount = 2;
            tlpNewPanelRefYearSetGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpNewPanelRefYearSetGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpNewPanelRefYearSetGroup.Controls.Add(lblDescriptionEnCaption, 0, 3);
            tlpNewPanelRefYearSetGroup.Controls.Add(lblLabelEnCaption, 0, 2);
            tlpNewPanelRefYearSetGroup.Controls.Add(lblDescriptionCsCaption, 0, 1);
            tlpNewPanelRefYearSetGroup.Controls.Add(lblLabelCsCaption, 0, 0);
            tlpNewPanelRefYearSetGroup.Controls.Add(txtDescriptionEnValue, 1, 3);
            tlpNewPanelRefYearSetGroup.Controls.Add(txtLabelEnValue, 1, 2);
            tlpNewPanelRefYearSetGroup.Controls.Add(txtDescriptionCsValue, 1, 1);
            tlpNewPanelRefYearSetGroup.Controls.Add(txtLabelCsValue, 1, 0);
            tlpNewPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpNewPanelRefYearSetGroup.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpNewPanelRefYearSetGroup.ForeColor = System.Drawing.Color.Black;
            tlpNewPanelRefYearSetGroup.Location = new System.Drawing.Point(5, 25);
            tlpNewPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            tlpNewPanelRefYearSetGroup.Name = "tlpNewPanelRefYearSetGroup";
            tlpNewPanelRefYearSetGroup.RowCount = 5;
            tlpNewPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpNewPanelRefYearSetGroup.Size = new System.Drawing.Size(934, 431);
            tlpNewPanelRefYearSetGroup.TabIndex = 1;
            // 
            // lblDescriptionEnCaption
            // 
            lblDescriptionEnCaption.AutoEllipsis = true;
            lblDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblDescriptionEnCaption.ForeColor = System.Drawing.Color.Black;
            lblDescriptionEnCaption.Location = new System.Drawing.Point(0, 75);
            lblDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEnCaption.Name = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.Size = new System.Drawing.Size(150, 25);
            lblDescriptionEnCaption.TabIndex = 7;
            lblDescriptionEnCaption.Text = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelEnCaption
            // 
            lblLabelEnCaption.AutoEllipsis = true;
            lblLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblLabelEnCaption.ForeColor = System.Drawing.Color.Black;
            lblLabelEnCaption.Location = new System.Drawing.Point(0, 50);
            lblLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEnCaption.Name = "lblLabelEnCaption";
            lblLabelEnCaption.Size = new System.Drawing.Size(150, 25);
            lblLabelEnCaption.TabIndex = 6;
            lblLabelEnCaption.Text = "lblLabelEnCaption";
            lblLabelEnCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCsCaption
            // 
            lblDescriptionCsCaption.AutoEllipsis = true;
            lblDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblDescriptionCsCaption.ForeColor = System.Drawing.Color.Black;
            lblDescriptionCsCaption.Location = new System.Drawing.Point(0, 25);
            lblDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCsCaption.Name = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.Size = new System.Drawing.Size(150, 25);
            lblDescriptionCsCaption.TabIndex = 2;
            lblDescriptionCsCaption.Text = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelCsCaption
            // 
            lblLabelCsCaption.AutoEllipsis = true;
            lblLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblLabelCsCaption.ForeColor = System.Drawing.Color.Black;
            lblLabelCsCaption.Location = new System.Drawing.Point(0, 0);
            lblLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCsCaption.Name = "lblLabelCsCaption";
            lblLabelCsCaption.Size = new System.Drawing.Size(150, 25);
            lblLabelCsCaption.TabIndex = 1;
            lblLabelCsCaption.Text = "lblLabelCsCaption";
            lblLabelCsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescriptionEnValue
            // 
            txtDescriptionEnValue.BackColor = System.Drawing.Color.White;
            txtDescriptionEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEnValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtDescriptionEnValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionEnValue.Location = new System.Drawing.Point(151, 76);
            txtDescriptionEnValue.Margin = new System.Windows.Forms.Padding(1);
            txtDescriptionEnValue.Name = "txtDescriptionEnValue";
            txtDescriptionEnValue.Size = new System.Drawing.Size(782, 23);
            txtDescriptionEnValue.TabIndex = 9;
            // 
            // txtLabelEnValue
            // 
            txtLabelEnValue.BackColor = System.Drawing.Color.White;
            txtLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtLabelEnValue.ForeColor = System.Drawing.Color.Black;
            txtLabelEnValue.Location = new System.Drawing.Point(151, 51);
            txtLabelEnValue.Margin = new System.Windows.Forms.Padding(1);
            txtLabelEnValue.Name = "txtLabelEnValue";
            txtLabelEnValue.Size = new System.Drawing.Size(782, 23);
            txtLabelEnValue.TabIndex = 8;
            // 
            // txtDescriptionCsValue
            // 
            txtDescriptionCsValue.BackColor = System.Drawing.Color.White;
            txtDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCsValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtDescriptionCsValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionCsValue.Location = new System.Drawing.Point(151, 26);
            txtDescriptionCsValue.Margin = new System.Windows.Forms.Padding(1);
            txtDescriptionCsValue.Name = "txtDescriptionCsValue";
            txtDescriptionCsValue.Size = new System.Drawing.Size(782, 23);
            txtDescriptionCsValue.TabIndex = 5;
            // 
            // txtLabelCsValue
            // 
            txtLabelCsValue.BackColor = System.Drawing.Color.White;
            txtLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCsValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtLabelCsValue.ForeColor = System.Drawing.Color.Black;
            txtLabelCsValue.Location = new System.Drawing.Point(151, 1);
            txtLabelCsValue.Margin = new System.Windows.Forms.Padding(1);
            txtLabelCsValue.Name = "txtLabelCsValue";
            txtLabelCsValue.Size = new System.Drawing.Size(782, 23);
            txtLabelCsValue.TabIndex = 4;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 3;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 16;
            // 
            // btnOK
            // 
            btnOK.AutoEllipsis = true;
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 17;
            // 
            // btnCancel
            // 
            btnCancel.AutoEllipsis = true;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 15;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // FormPanelRefYearSetGroupNew
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormPanelRefYearSetGroupNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormPanelRefYearSetGroupNew";
            tlpMain.ResumeLayout(false);
            grpNewPanelRefYearSetGroup.ResumeLayout(false);
            tlpNewPanelRefYearSetGroup.ResumeLayout(false);
            tlpNewPanelRefYearSetGroup.PerformLayout();
            tlpButtons.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox grpNewPanelRefYearSetGroup;
        private System.Windows.Forms.TableLayoutPanel tlpNewPanelRefYearSetGroup;
        private System.Windows.Forms.Label lblDescriptionEnCaption;
        private System.Windows.Forms.Label lblLabelEnCaption;
        private System.Windows.Forms.Label lblDescriptionCsCaption;
        private System.Windows.Forms.Label lblLabelCsCaption;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Panel pnlCancel;
        public System.Windows.Forms.TextBox txtDescriptionEnValue;
        public System.Windows.Forms.TextBox txtLabelEnValue;
        public System.Windows.Forms.TextBox txtDescriptionCsValue;
        public System.Windows.Forms.TextBox txtLabelCsValue;
    }

}