﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormEstimateConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEstimateConfiguration));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            splCountryStrataSetStratum = new System.Windows.Forms.SplitContainer();
            grpCountryStrataSetStratum = new System.Windows.Forms.GroupBox();
            tvwCountryStrataSetStratum = new System.Windows.Forms.TreeView();
            ilTreeView = new System.Windows.Forms.ImageList(components);
            tlpPanelRefYearSetGroups = new System.Windows.Forms.TableLayoutPanel();
            tlpCountryStrataSetStratumButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlConfirm = new System.Windows.Forms.Panel();
            btnConfirm = new System.Windows.Forms.Button();
            lblSampleSizeForStratum = new System.Windows.Forms.Label();
            splPanelRefYearSetGroups = new System.Windows.Forms.SplitContainer();
            grpPanelRefYearSetGroups = new System.Windows.Forms.GroupBox();
            pnlPanelRefYearSetGroups = new System.Windows.Forms.Panel();
            grpPanelRefYearSetPairs = new System.Windows.Forms.GroupBox();
            pnlPanelRefYearSetPairs = new System.Windows.Forms.Panel();
            lblNumberOfPanelRefYearSetPairsForStratum = new System.Windows.Forms.Label();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splCountryStrataSetStratum).BeginInit();
            splCountryStrataSetStratum.Panel1.SuspendLayout();
            splCountryStrataSetStratum.Panel2.SuspendLayout();
            splCountryStrataSetStratum.SuspendLayout();
            grpCountryStrataSetStratum.SuspendLayout();
            tlpPanelRefYearSetGroups.SuspendLayout();
            tlpCountryStrataSetStratumButtons.SuspendLayout();
            pnlConfirm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splPanelRefYearSetGroups).BeginInit();
            splPanelRefYearSetGroups.Panel1.SuspendLayout();
            splPanelRefYearSetGroups.Panel2.SuspendLayout();
            splPanelRefYearSetGroups.SuspendLayout();
            grpPanelRefYearSetGroups.SuspendLayout();
            grpPanelRefYearSetPairs.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(splCountryStrataSetStratum, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 2;
            // 
            // splCountryStrataSetStratum
            // 
            splCountryStrataSetStratum.BackColor = System.Drawing.Color.Transparent;
            splCountryStrataSetStratum.Dock = System.Windows.Forms.DockStyle.Fill;
            splCountryStrataSetStratum.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splCountryStrataSetStratum.Location = new System.Drawing.Point(0, 0);
            splCountryStrataSetStratum.Margin = new System.Windows.Forms.Padding(0);
            splCountryStrataSetStratum.Name = "splCountryStrataSetStratum";
            // 
            // splCountryStrataSetStratum.Panel1
            // 
            splCountryStrataSetStratum.Panel1.Controls.Add(grpCountryStrataSetStratum);
            // 
            // splCountryStrataSetStratum.Panel2
            // 
            splCountryStrataSetStratum.Panel2.Controls.Add(tlpPanelRefYearSetGroups);
            splCountryStrataSetStratum.Size = new System.Drawing.Size(944, 461);
            splCountryStrataSetStratum.SplitterDistance = 250;
            splCountryStrataSetStratum.SplitterWidth = 5;
            splCountryStrataSetStratum.TabIndex = 6;
            // 
            // grpCountryStrataSetStratum
            // 
            grpCountryStrataSetStratum.BackColor = System.Drawing.Color.Transparent;
            grpCountryStrataSetStratum.Controls.Add(tvwCountryStrataSetStratum);
            grpCountryStrataSetStratum.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCountryStrataSetStratum.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpCountryStrataSetStratum.ForeColor = System.Drawing.Color.MediumBlue;
            grpCountryStrataSetStratum.Location = new System.Drawing.Point(0, 0);
            grpCountryStrataSetStratum.Margin = new System.Windows.Forms.Padding(0);
            grpCountryStrataSetStratum.Name = "grpCountryStrataSetStratum";
            grpCountryStrataSetStratum.Padding = new System.Windows.Forms.Padding(5);
            grpCountryStrataSetStratum.Size = new System.Drawing.Size(250, 461);
            grpCountryStrataSetStratum.TabIndex = 0;
            grpCountryStrataSetStratum.TabStop = false;
            grpCountryStrataSetStratum.Text = "grpCountryStrataSetStratum";
            // 
            // tvwCountryStrataSetStratum
            // 
            tvwCountryStrataSetStratum.AllowDrop = true;
            tvwCountryStrataSetStratum.BackColor = System.Drawing.Color.White;
            tvwCountryStrataSetStratum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            tvwCountryStrataSetStratum.Dock = System.Windows.Forms.DockStyle.Fill;
            tvwCountryStrataSetStratum.Font = new System.Drawing.Font("Segoe UI", 9F);
            tvwCountryStrataSetStratum.ForeColor = System.Drawing.Color.Black;
            tvwCountryStrataSetStratum.HideSelection = false;
            tvwCountryStrataSetStratum.ImageIndex = 4;
            tvwCountryStrataSetStratum.ImageList = ilTreeView;
            tvwCountryStrataSetStratum.LineColor = System.Drawing.Color.MediumBlue;
            tvwCountryStrataSetStratum.Location = new System.Drawing.Point(5, 25);
            tvwCountryStrataSetStratum.Margin = new System.Windows.Forms.Padding(0);
            tvwCountryStrataSetStratum.Name = "tvwCountryStrataSetStratum";
            tvwCountryStrataSetStratum.SelectedImageIndex = 4;
            tvwCountryStrataSetStratum.ShowLines = false;
            tvwCountryStrataSetStratum.ShowNodeToolTips = true;
            tvwCountryStrataSetStratum.ShowRootLines = false;
            tvwCountryStrataSetStratum.Size = new System.Drawing.Size(240, 431);
            tvwCountryStrataSetStratum.TabIndex = 11;
            // 
            // ilTreeView
            // 
            ilTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            ilTreeView.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ilTreeView.ImageStream");
            ilTreeView.TransparentColor = System.Drawing.Color.Transparent;
            ilTreeView.Images.SetKeyName(0, "RedBall");
            ilTreeView.Images.SetKeyName(1, "BlueBall");
            ilTreeView.Images.SetKeyName(2, "YellowBall");
            ilTreeView.Images.SetKeyName(3, "GreenBall");
            ilTreeView.Images.SetKeyName(4, "GrayBall");
            ilTreeView.Images.SetKeyName(5, "Lock");
            ilTreeView.Images.SetKeyName(6, "Schema");
            ilTreeView.Images.SetKeyName(7, "Vector");
            ilTreeView.Images.SetKeyName(8, "Raster");
            // 
            // tlpPanelRefYearSetGroups
            // 
            tlpPanelRefYearSetGroups.ColumnCount = 1;
            tlpPanelRefYearSetGroups.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanelRefYearSetGroups.Controls.Add(tlpCountryStrataSetStratumButtons, 0, 3);
            tlpPanelRefYearSetGroups.Controls.Add(lblSampleSizeForStratum, 0, 2);
            tlpPanelRefYearSetGroups.Controls.Add(splPanelRefYearSetGroups, 0, 0);
            tlpPanelRefYearSetGroups.Controls.Add(lblNumberOfPanelRefYearSetPairsForStratum, 0, 1);
            tlpPanelRefYearSetGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpPanelRefYearSetGroups.Location = new System.Drawing.Point(0, 0);
            tlpPanelRefYearSetGroups.Name = "tlpPanelRefYearSetGroups";
            tlpPanelRefYearSetGroups.RowCount = 4;
            tlpPanelRefYearSetGroups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanelRefYearSetGroups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpPanelRefYearSetGroups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpPanelRefYearSetGroups.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpPanelRefYearSetGroups.Size = new System.Drawing.Size(689, 461);
            tlpPanelRefYearSetGroups.TabIndex = 0;
            // 
            // tlpCountryStrataSetStratumButtons
            // 
            tlpCountryStrataSetStratumButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpCountryStrataSetStratumButtons.ColumnCount = 2;
            tlpCountryStrataSetStratumButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCountryStrataSetStratumButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpCountryStrataSetStratumButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpCountryStrataSetStratumButtons.Controls.Add(pnlConfirm, 1, 0);
            tlpCountryStrataSetStratumButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpCountryStrataSetStratumButtons.Location = new System.Drawing.Point(0, 421);
            tlpCountryStrataSetStratumButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpCountryStrataSetStratumButtons.Name = "tlpCountryStrataSetStratumButtons";
            tlpCountryStrataSetStratumButtons.RowCount = 1;
            tlpCountryStrataSetStratumButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCountryStrataSetStratumButtons.Size = new System.Drawing.Size(689, 40);
            tlpCountryStrataSetStratumButtons.TabIndex = 9;
            // 
            // pnlConfirm
            // 
            pnlConfirm.BackColor = System.Drawing.Color.Transparent;
            pnlConfirm.Controls.Add(btnConfirm);
            pnlConfirm.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlConfirm.Location = new System.Drawing.Point(529, 0);
            pnlConfirm.Margin = new System.Windows.Forms.Padding(0);
            pnlConfirm.Name = "pnlConfirm";
            pnlConfirm.Padding = new System.Windows.Forms.Padding(5);
            pnlConfirm.Size = new System.Drawing.Size(160, 40);
            pnlConfirm.TabIndex = 16;
            // 
            // btnConfirm
            // 
            btnConfirm.Dock = System.Windows.Forms.DockStyle.Fill;
            btnConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnConfirm.Location = new System.Drawing.Point(5, 5);
            btnConfirm.Margin = new System.Windows.Forms.Padding(0);
            btnConfirm.Name = "btnConfirm";
            btnConfirm.Size = new System.Drawing.Size(150, 30);
            btnConfirm.TabIndex = 15;
            btnConfirm.Text = "btnConfirm";
            btnConfirm.UseVisualStyleBackColor = true;
            // 
            // lblSampleSizeForStratum
            // 
            lblSampleSizeForStratum.AutoEllipsis = true;
            lblSampleSizeForStratum.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSampleSizeForStratum.Location = new System.Drawing.Point(0, 396);
            lblSampleSizeForStratum.Margin = new System.Windows.Forms.Padding(0);
            lblSampleSizeForStratum.Name = "lblSampleSizeForStratum";
            lblSampleSizeForStratum.Size = new System.Drawing.Size(689, 25);
            lblSampleSizeForStratum.TabIndex = 8;
            lblSampleSizeForStratum.Text = "lblSampleSizeForStratum";
            lblSampleSizeForStratum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splPanelRefYearSetGroups
            // 
            splPanelRefYearSetGroups.BackColor = System.Drawing.Color.Transparent;
            splPanelRefYearSetGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            splPanelRefYearSetGroups.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splPanelRefYearSetGroups.Location = new System.Drawing.Point(0, 0);
            splPanelRefYearSetGroups.Margin = new System.Windows.Forms.Padding(0);
            splPanelRefYearSetGroups.Name = "splPanelRefYearSetGroups";
            // 
            // splPanelRefYearSetGroups.Panel1
            // 
            splPanelRefYearSetGroups.Panel1.Controls.Add(grpPanelRefYearSetGroups);
            // 
            // splPanelRefYearSetGroups.Panel2
            // 
            splPanelRefYearSetGroups.Panel2.Controls.Add(grpPanelRefYearSetPairs);
            splPanelRefYearSetGroups.Size = new System.Drawing.Size(689, 371);
            splPanelRefYearSetGroups.SplitterDistance = 250;
            splPanelRefYearSetGroups.SplitterWidth = 5;
            splPanelRefYearSetGroups.TabIndex = 6;
            // 
            // grpPanelRefYearSetGroups
            // 
            grpPanelRefYearSetGroups.BackColor = System.Drawing.Color.Transparent;
            grpPanelRefYearSetGroups.Controls.Add(pnlPanelRefYearSetGroups);
            grpPanelRefYearSetGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanelRefYearSetGroups.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpPanelRefYearSetGroups.ForeColor = System.Drawing.Color.MediumBlue;
            grpPanelRefYearSetGroups.Location = new System.Drawing.Point(0, 0);
            grpPanelRefYearSetGroups.Margin = new System.Windows.Forms.Padding(0);
            grpPanelRefYearSetGroups.Name = "grpPanelRefYearSetGroups";
            grpPanelRefYearSetGroups.Padding = new System.Windows.Forms.Padding(5);
            grpPanelRefYearSetGroups.Size = new System.Drawing.Size(250, 371);
            grpPanelRefYearSetGroups.TabIndex = 0;
            grpPanelRefYearSetGroups.TabStop = false;
            grpPanelRefYearSetGroups.Text = "grpPanelRefYearSetGroups";
            // 
            // pnlPanelRefYearSetGroups
            // 
            pnlPanelRefYearSetGroups.AutoScroll = true;
            pnlPanelRefYearSetGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPanelRefYearSetGroups.Font = new System.Drawing.Font("Segoe UI", 9F);
            pnlPanelRefYearSetGroups.ForeColor = System.Drawing.Color.Black;
            pnlPanelRefYearSetGroups.Location = new System.Drawing.Point(5, 25);
            pnlPanelRefYearSetGroups.Margin = new System.Windows.Forms.Padding(0);
            pnlPanelRefYearSetGroups.Name = "pnlPanelRefYearSetGroups";
            pnlPanelRefYearSetGroups.Size = new System.Drawing.Size(240, 341);
            pnlPanelRefYearSetGroups.TabIndex = 0;
            // 
            // grpPanelRefYearSetPairs
            // 
            grpPanelRefYearSetPairs.Controls.Add(pnlPanelRefYearSetPairs);
            grpPanelRefYearSetPairs.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanelRefYearSetPairs.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpPanelRefYearSetPairs.ForeColor = System.Drawing.Color.MediumBlue;
            grpPanelRefYearSetPairs.Location = new System.Drawing.Point(0, 0);
            grpPanelRefYearSetPairs.Margin = new System.Windows.Forms.Padding(0);
            grpPanelRefYearSetPairs.Name = "grpPanelRefYearSetPairs";
            grpPanelRefYearSetPairs.Padding = new System.Windows.Forms.Padding(5);
            grpPanelRefYearSetPairs.Size = new System.Drawing.Size(434, 371);
            grpPanelRefYearSetPairs.TabIndex = 2;
            grpPanelRefYearSetPairs.TabStop = false;
            grpPanelRefYearSetPairs.Text = "grpPanelRefYearSetPairs";
            // 
            // pnlPanelRefYearSetPairs
            // 
            pnlPanelRefYearSetPairs.AutoScroll = true;
            pnlPanelRefYearSetPairs.BackColor = System.Drawing.Color.Transparent;
            pnlPanelRefYearSetPairs.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPanelRefYearSetPairs.Font = new System.Drawing.Font("Segoe UI", 9F);
            pnlPanelRefYearSetPairs.ForeColor = System.Drawing.Color.Black;
            pnlPanelRefYearSetPairs.Location = new System.Drawing.Point(5, 25);
            pnlPanelRefYearSetPairs.Margin = new System.Windows.Forms.Padding(0);
            pnlPanelRefYearSetPairs.Name = "pnlPanelRefYearSetPairs";
            pnlPanelRefYearSetPairs.Size = new System.Drawing.Size(424, 341);
            pnlPanelRefYearSetPairs.TabIndex = 12;
            // 
            // lblNumberOfPanelRefYearSetPairsForStratum
            // 
            lblNumberOfPanelRefYearSetPairsForStratum.AutoEllipsis = true;
            lblNumberOfPanelRefYearSetPairsForStratum.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumberOfPanelRefYearSetPairsForStratum.Location = new System.Drawing.Point(0, 371);
            lblNumberOfPanelRefYearSetPairsForStratum.Margin = new System.Windows.Forms.Padding(0);
            lblNumberOfPanelRefYearSetPairsForStratum.Name = "lblNumberOfPanelRefYearSetPairsForStratum";
            lblNumberOfPanelRefYearSetPairsForStratum.Size = new System.Drawing.Size(689, 25);
            lblNumberOfPanelRefYearSetPairsForStratum.TabIndex = 7;
            lblNumberOfPanelRefYearSetPairsForStratum.Text = "lblNumberOfPanelRefYearSetPairsForStratum";
            lblNumberOfPanelRefYearSetPairsForStratum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 4;
            // 
            // pnlCancel
            // 
            pnlCancel.BackColor = System.Drawing.Color.Transparent;
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 16;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 15;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.BackColor = System.Drawing.Color.Transparent;
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 15;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // FormEstimateConfiguration
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            KeyPreview = true;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "FormEstimateConfiguration";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormPanelsInEstimationCell";
            tlpMain.ResumeLayout(false);
            splCountryStrataSetStratum.Panel1.ResumeLayout(false);
            splCountryStrataSetStratum.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splCountryStrataSetStratum).EndInit();
            splCountryStrataSetStratum.ResumeLayout(false);
            grpCountryStrataSetStratum.ResumeLayout(false);
            tlpPanelRefYearSetGroups.ResumeLayout(false);
            tlpCountryStrataSetStratumButtons.ResumeLayout(false);
            pnlConfirm.ResumeLayout(false);
            splPanelRefYearSetGroups.Panel1.ResumeLayout(false);
            splPanelRefYearSetGroups.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splPanelRefYearSetGroups).EndInit();
            splPanelRefYearSetGroups.ResumeLayout(false);
            grpPanelRefYearSetGroups.ResumeLayout(false);
            grpPanelRefYearSetPairs.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.SplitContainer splCountryStrataSetStratum;
        private System.Windows.Forms.GroupBox grpCountryStrataSetStratum;
        private System.Windows.Forms.TreeView tvwCountryStrataSetStratum;
        private System.Windows.Forms.TableLayoutPanel tlpPanelRefYearSetGroups;
        private System.Windows.Forms.Label lblSampleSizeForStratum;
        private System.Windows.Forms.SplitContainer splPanelRefYearSetGroups;
        private System.Windows.Forms.GroupBox grpPanelRefYearSetGroups;
        private System.Windows.Forms.Panel pnlPanelRefYearSetGroups;
        private System.Windows.Forms.GroupBox grpPanelRefYearSetPairs;
        private System.Windows.Forms.Panel pnlPanelRefYearSetPairs;
        private System.Windows.Forms.Label lblNumberOfPanelRefYearSetPairsForStratum;
        private System.Windows.Forms.TableLayoutPanel tlpCountryStrataSetStratumButtons;
        private System.Windows.Forms.Panel pnlConfirm;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.ImageList ilTreeView;
    }

}