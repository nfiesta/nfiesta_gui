﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormColumnEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            pnlToolTipTextCs = new System.Windows.Forms.Panel();
            txtToolTipTextCs = new System.Windows.Forms.TextBox();
            pnlLabelToolTipTextCs = new System.Windows.Forms.Panel();
            lblToolTipTextCs = new System.Windows.Forms.Label();
            pnlHeaderTextCs = new System.Windows.Forms.Panel();
            txtHeaderTextCs = new System.Windows.Forms.TextBox();
            pnlLabelHeaderTextCs = new System.Windows.Forms.Panel();
            lblHeaderTextCs = new System.Windows.Forms.Label();
            pnlLabelToolTipTextEn = new System.Windows.Forms.Panel();
            lblToolTipTextEn = new System.Windows.Forms.Label();
            pnlLabelHeaderTextEn = new System.Windows.Forms.Panel();
            lblHeaderTextEn = new System.Windows.Forms.Label();
            pnlToolTipTextEn = new System.Windows.Forms.Panel();
            txtToolTipTextEn = new System.Windows.Forms.TextBox();
            pnlHeaderTextEn = new System.Windows.Forms.Panel();
            txtHeaderTextEn = new System.Windows.Forms.TextBox();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpWorkSpace.SuspendLayout();
            pnlToolTipTextCs.SuspendLayout();
            pnlLabelToolTipTextCs.SuspendLayout();
            pnlHeaderTextCs.SuspendLayout();
            pnlLabelHeaderTextCs.SuspendLayout();
            pnlLabelToolTipTextEn.SuspendLayout();
            pnlLabelHeaderTextEn.SuspendLayout();
            pnlToolTipTextEn.SuspendLayout();
            pnlHeaderTextEn.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.SystemColors.Control;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlMain.Size = new System.Drawing.Size(944, 501);
            pnlMain.TabIndex = 0;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.SystemColors.Control;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpWorkSpace, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(4, 3);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(936, 495);
            tlpMain.TabIndex = 2;
            // 
            // tlpWorkSpace
            // 
            tlpWorkSpace.ColumnCount = 2;
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 292F));
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Controls.Add(pnlToolTipTextCs, 1, 1);
            tlpWorkSpace.Controls.Add(pnlLabelToolTipTextCs, 0, 1);
            tlpWorkSpace.Controls.Add(pnlHeaderTextCs, 1, 0);
            tlpWorkSpace.Controls.Add(pnlLabelHeaderTextCs, 0, 0);
            tlpWorkSpace.Controls.Add(pnlLabelToolTipTextEn, 0, 4);
            tlpWorkSpace.Controls.Add(pnlLabelHeaderTextEn, 0, 3);
            tlpWorkSpace.Controls.Add(pnlToolTipTextEn, 1, 4);
            tlpWorkSpace.Controls.Add(pnlHeaderTextEn, 1, 3);
            tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpWorkSpace.Location = new System.Drawing.Point(0, 0);
            tlpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            tlpWorkSpace.Name = "tlpWorkSpace";
            tlpWorkSpace.Padding = new System.Windows.Forms.Padding(6);
            tlpWorkSpace.RowCount = 6;
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Size = new System.Drawing.Size(936, 449);
            tlpWorkSpace.TabIndex = 6;
            // 
            // pnlToolTipTextCs
            // 
            pnlToolTipTextCs.Controls.Add(txtToolTipTextCs);
            pnlToolTipTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlToolTipTextCs.Location = new System.Drawing.Point(298, 41);
            pnlToolTipTextCs.Margin = new System.Windows.Forms.Padding(0);
            pnlToolTipTextCs.Name = "pnlToolTipTextCs";
            pnlToolTipTextCs.Padding = new System.Windows.Forms.Padding(0, 5, 0, 6);
            pnlToolTipTextCs.Size = new System.Drawing.Size(632, 35);
            pnlToolTipTextCs.TabIndex = 3;
            // 
            // txtToolTipTextCs
            // 
            txtToolTipTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtToolTipTextCs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtToolTipTextCs.Location = new System.Drawing.Point(0, 5);
            txtToolTipTextCs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtToolTipTextCs.Name = "txtToolTipTextCs";
            txtToolTipTextCs.Size = new System.Drawing.Size(632, 21);
            txtToolTipTextCs.TabIndex = 1;
            // 
            // pnlLabelToolTipTextCs
            // 
            pnlLabelToolTipTextCs.Controls.Add(lblToolTipTextCs);
            pnlLabelToolTipTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLabelToolTipTextCs.Location = new System.Drawing.Point(6, 41);
            pnlLabelToolTipTextCs.Margin = new System.Windows.Forms.Padding(0);
            pnlLabelToolTipTextCs.Name = "pnlLabelToolTipTextCs";
            pnlLabelToolTipTextCs.Padding = new System.Windows.Forms.Padding(12, 8, 0, 9);
            pnlLabelToolTipTextCs.Size = new System.Drawing.Size(292, 35);
            pnlLabelToolTipTextCs.TabIndex = 2;
            // 
            // lblToolTipTextCs
            // 
            lblToolTipTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            lblToolTipTextCs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblToolTipTextCs.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblToolTipTextCs.Location = new System.Drawing.Point(12, 8);
            lblToolTipTextCs.Margin = new System.Windows.Forms.Padding(0);
            lblToolTipTextCs.Name = "lblToolTipTextCs";
            lblToolTipTextCs.Size = new System.Drawing.Size(280, 18);
            lblToolTipTextCs.TabIndex = 1;
            lblToolTipTextCs.Text = "lblToolTipTextCs";
            // 
            // pnlHeaderTextCs
            // 
            pnlHeaderTextCs.Controls.Add(txtHeaderTextCs);
            pnlHeaderTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlHeaderTextCs.Location = new System.Drawing.Point(298, 6);
            pnlHeaderTextCs.Margin = new System.Windows.Forms.Padding(0);
            pnlHeaderTextCs.Name = "pnlHeaderTextCs";
            pnlHeaderTextCs.Padding = new System.Windows.Forms.Padding(0, 5, 0, 6);
            pnlHeaderTextCs.Size = new System.Drawing.Size(632, 35);
            pnlHeaderTextCs.TabIndex = 1;
            // 
            // txtHeaderTextCs
            // 
            txtHeaderTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtHeaderTextCs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtHeaderTextCs.Location = new System.Drawing.Point(0, 5);
            txtHeaderTextCs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtHeaderTextCs.Name = "txtHeaderTextCs";
            txtHeaderTextCs.Size = new System.Drawing.Size(632, 21);
            txtHeaderTextCs.TabIndex = 0;
            // 
            // pnlLabelHeaderTextCs
            // 
            pnlLabelHeaderTextCs.Controls.Add(lblHeaderTextCs);
            pnlLabelHeaderTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLabelHeaderTextCs.Location = new System.Drawing.Point(6, 6);
            pnlLabelHeaderTextCs.Margin = new System.Windows.Forms.Padding(0);
            pnlLabelHeaderTextCs.Name = "pnlLabelHeaderTextCs";
            pnlLabelHeaderTextCs.Padding = new System.Windows.Forms.Padding(12, 8, 0, 9);
            pnlLabelHeaderTextCs.Size = new System.Drawing.Size(292, 35);
            pnlLabelHeaderTextCs.TabIndex = 0;
            // 
            // lblHeaderTextCs
            // 
            lblHeaderTextCs.Dock = System.Windows.Forms.DockStyle.Fill;
            lblHeaderTextCs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblHeaderTextCs.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblHeaderTextCs.Location = new System.Drawing.Point(12, 8);
            lblHeaderTextCs.Margin = new System.Windows.Forms.Padding(0);
            lblHeaderTextCs.Name = "lblHeaderTextCs";
            lblHeaderTextCs.Size = new System.Drawing.Size(280, 18);
            lblHeaderTextCs.TabIndex = 0;
            lblHeaderTextCs.Text = "lblHeaderTextCs";
            // 
            // pnlLabelToolTipTextEn
            // 
            pnlLabelToolTipTextEn.Controls.Add(lblToolTipTextEn);
            pnlLabelToolTipTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLabelToolTipTextEn.Location = new System.Drawing.Point(6, 123);
            pnlLabelToolTipTextEn.Margin = new System.Windows.Forms.Padding(0);
            pnlLabelToolTipTextEn.Name = "pnlLabelToolTipTextEn";
            pnlLabelToolTipTextEn.Padding = new System.Windows.Forms.Padding(12, 8, 0, 9);
            pnlLabelToolTipTextEn.Size = new System.Drawing.Size(292, 35);
            pnlLabelToolTipTextEn.TabIndex = 6;
            // 
            // lblToolTipTextEn
            // 
            lblToolTipTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblToolTipTextEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblToolTipTextEn.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblToolTipTextEn.Location = new System.Drawing.Point(12, 8);
            lblToolTipTextEn.Margin = new System.Windows.Forms.Padding(0);
            lblToolTipTextEn.Name = "lblToolTipTextEn";
            lblToolTipTextEn.Size = new System.Drawing.Size(280, 18);
            lblToolTipTextEn.TabIndex = 1;
            lblToolTipTextEn.Text = "lblToolTipTextEn";
            // 
            // pnlLabelHeaderTextEn
            // 
            pnlLabelHeaderTextEn.Controls.Add(lblHeaderTextEn);
            pnlLabelHeaderTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLabelHeaderTextEn.Location = new System.Drawing.Point(6, 88);
            pnlLabelHeaderTextEn.Margin = new System.Windows.Forms.Padding(0);
            pnlLabelHeaderTextEn.Name = "pnlLabelHeaderTextEn";
            pnlLabelHeaderTextEn.Padding = new System.Windows.Forms.Padding(12, 8, 0, 9);
            pnlLabelHeaderTextEn.Size = new System.Drawing.Size(292, 35);
            pnlLabelHeaderTextEn.TabIndex = 4;
            // 
            // lblHeaderTextEn
            // 
            lblHeaderTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblHeaderTextEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblHeaderTextEn.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblHeaderTextEn.Location = new System.Drawing.Point(12, 8);
            lblHeaderTextEn.Margin = new System.Windows.Forms.Padding(0);
            lblHeaderTextEn.Name = "lblHeaderTextEn";
            lblHeaderTextEn.Size = new System.Drawing.Size(280, 18);
            lblHeaderTextEn.TabIndex = 1;
            lblHeaderTextEn.Text = "lblHeaderTextEn";
            // 
            // pnlToolTipTextEn
            // 
            pnlToolTipTextEn.Controls.Add(txtToolTipTextEn);
            pnlToolTipTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlToolTipTextEn.Location = new System.Drawing.Point(298, 123);
            pnlToolTipTextEn.Margin = new System.Windows.Forms.Padding(0);
            pnlToolTipTextEn.Name = "pnlToolTipTextEn";
            pnlToolTipTextEn.Padding = new System.Windows.Forms.Padding(0, 5, 0, 6);
            pnlToolTipTextEn.Size = new System.Drawing.Size(632, 35);
            pnlToolTipTextEn.TabIndex = 7;
            // 
            // txtToolTipTextEn
            // 
            txtToolTipTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtToolTipTextEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtToolTipTextEn.Location = new System.Drawing.Point(0, 5);
            txtToolTipTextEn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtToolTipTextEn.Name = "txtToolTipTextEn";
            txtToolTipTextEn.Size = new System.Drawing.Size(632, 21);
            txtToolTipTextEn.TabIndex = 1;
            // 
            // pnlHeaderTextEn
            // 
            pnlHeaderTextEn.Controls.Add(txtHeaderTextEn);
            pnlHeaderTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlHeaderTextEn.Location = new System.Drawing.Point(298, 88);
            pnlHeaderTextEn.Margin = new System.Windows.Forms.Padding(0);
            pnlHeaderTextEn.Name = "pnlHeaderTextEn";
            pnlHeaderTextEn.Padding = new System.Windows.Forms.Padding(0, 5, 0, 6);
            pnlHeaderTextEn.Size = new System.Drawing.Size(632, 35);
            pnlHeaderTextEn.TabIndex = 5;
            // 
            // txtHeaderTextEn
            // 
            txtHeaderTextEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtHeaderTextEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtHeaderTextEn.Location = new System.Drawing.Point(0, 5);
            txtHeaderTextEn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtHeaderTextEn.Name = "txtHeaderTextEn";
            txtHeaderTextEn.Size = new System.Drawing.Size(632, 21);
            txtHeaderTextEn.TabIndex = 1;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 449);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(936, 46);
            tlpButtons.TabIndex = 3;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(749, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(6);
            pnlCancel.Size = new System.Drawing.Size(187, 46);
            pnlCancel.TabIndex = 16;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(6, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 34);
            btnCancel.TabIndex = 15;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(562, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(6);
            pnlOK.Size = new System.Drawing.Size(187, 46);
            pnlOK.TabIndex = 15;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(6, 6);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(175, 34);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // FormColumnEdit
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormColumnEdit";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormColumnEdit";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpWorkSpace.ResumeLayout(false);
            pnlToolTipTextCs.ResumeLayout(false);
            pnlToolTipTextCs.PerformLayout();
            pnlLabelToolTipTextCs.ResumeLayout(false);
            pnlHeaderTextCs.ResumeLayout(false);
            pnlHeaderTextCs.PerformLayout();
            pnlLabelHeaderTextCs.ResumeLayout(false);
            pnlLabelToolTipTextEn.ResumeLayout(false);
            pnlLabelHeaderTextEn.ResumeLayout(false);
            pnlToolTipTextEn.ResumeLayout(false);
            pnlToolTipTextEn.PerformLayout();
            pnlHeaderTextEn.ResumeLayout(false);
            pnlHeaderTextEn.PerformLayout();
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.Panel pnlToolTipTextEn;
        private System.Windows.Forms.Panel pnlLabelToolTipTextEn;
        private System.Windows.Forms.Panel pnlHeaderTextEn;
        private System.Windows.Forms.Panel pnlLabelHeaderTextEn;
        private System.Windows.Forms.Panel pnlToolTipTextCs;
        private System.Windows.Forms.Panel pnlLabelToolTipTextCs;
        private System.Windows.Forms.Panel pnlHeaderTextCs;
        private System.Windows.Forms.Panel pnlLabelHeaderTextCs;
        private System.Windows.Forms.TextBox txtToolTipTextEn;
        private System.Windows.Forms.Label lblToolTipTextEn;
        private System.Windows.Forms.TextBox txtHeaderTextEn;
        private System.Windows.Forms.Label lblHeaderTextEn;
        private System.Windows.Forms.TextBox txtToolTipTextCs;
        private System.Windows.Forms.Label lblToolTipTextCs;
        private System.Windows.Forms.TextBox txtHeaderTextCs;
        private System.Windows.Forms.Label lblHeaderTextCs;
    }

}