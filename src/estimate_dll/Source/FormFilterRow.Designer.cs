﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormFilterRow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            pnlCaption = new System.Windows.Forms.Panel();
            lblCaption = new System.Windows.Forms.Label();
            pnlTools = new System.Windows.Forms.Panel();
            pnlCheckAll = new System.Windows.Forms.Panel();
            chkCheckAll = new System.Windows.Forms.CheckBox();
            pnlCheckedList = new System.Windows.Forms.Panel();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            tlpWorkSpace.SuspendLayout();
            pnlCaption.SuspendLayout();
            pnlTools.SuspendLayout();
            pnlCheckAll.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.Color.DarkSeaGreen;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlMain.Size = new System.Drawing.Size(960, 540);
            pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.SystemColors.Control;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Controls.Add(tlpWorkSpace, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(4, 3);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(952, 534);
            tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            pnlButtons.BackColor = System.Drawing.SystemColors.Control;
            pnlButtons.Controls.Add(tlpButtons);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 488);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(952, 46);
            pnlButtons.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(952, 46);
            tlpButtons.TabIndex = 2;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(765, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            pnlCancel.Size = new System.Drawing.Size(187, 46);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(6, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 34);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(578, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            pnlOK.Size = new System.Drawing.Size(187, 46);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(6, 6);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(175, 34);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // tlpWorkSpace
            // 
            tlpWorkSpace.ColumnCount = 1;
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Controls.Add(pnlCaption, 0, 0);
            tlpWorkSpace.Controls.Add(pnlTools, 0, 1);
            tlpWorkSpace.Controls.Add(pnlCheckedList, 0, 2);
            tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpWorkSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tlpWorkSpace.Location = new System.Drawing.Point(0, 0);
            tlpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            tlpWorkSpace.Name = "tlpWorkSpace";
            tlpWorkSpace.RowCount = 3;
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Size = new System.Drawing.Size(952, 488);
            tlpWorkSpace.TabIndex = 4;
            // 
            // pnlCaption
            // 
            pnlCaption.BackColor = System.Drawing.Color.DarkBlue;
            pnlCaption.Controls.Add(lblCaption);
            pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            pnlCaption.ForeColor = System.Drawing.Color.White;
            pnlCaption.Location = new System.Drawing.Point(0, 0);
            pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            pnlCaption.Name = "pnlCaption";
            pnlCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            pnlCaption.Size = new System.Drawing.Size(952, 35);
            pnlCaption.TabIndex = 0;
            // 
            // lblCaption
            // 
            lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCaption.Location = new System.Drawing.Point(12, 0);
            lblCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCaption.Name = "lblCaption";
            lblCaption.Size = new System.Drawing.Size(940, 35);
            lblCaption.TabIndex = 0;
            lblCaption.Text = "lblCaption";
            lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlTools
            // 
            pnlTools.AutoScroll = true;
            pnlTools.BackColor = System.Drawing.SystemColors.Control;
            pnlTools.Controls.Add(pnlCheckAll);
            pnlTools.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTools.Location = new System.Drawing.Point(0, 35);
            pnlTools.Margin = new System.Windows.Forms.Padding(0);
            pnlTools.Name = "pnlTools";
            pnlTools.Size = new System.Drawing.Size(952, 35);
            pnlTools.TabIndex = 2;
            // 
            // pnlCheckAll
            // 
            pnlCheckAll.AutoScroll = true;
            pnlCheckAll.BackColor = System.Drawing.Color.White;
            pnlCheckAll.Controls.Add(chkCheckAll);
            pnlCheckAll.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCheckAll.Location = new System.Drawing.Point(0, 0);
            pnlCheckAll.Margin = new System.Windows.Forms.Padding(0);
            pnlCheckAll.Name = "pnlCheckAll";
            pnlCheckAll.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            pnlCheckAll.Size = new System.Drawing.Size(952, 35);
            pnlCheckAll.TabIndex = 2;
            // 
            // chkCheckAll
            // 
            chkCheckAll.AutoSize = true;
            chkCheckAll.Dock = System.Windows.Forms.DockStyle.Fill;
            chkCheckAll.Location = new System.Drawing.Point(12, 0);
            chkCheckAll.Margin = new System.Windows.Forms.Padding(0);
            chkCheckAll.Name = "chkCheckAll";
            chkCheckAll.Size = new System.Drawing.Size(940, 35);
            chkCheckAll.TabIndex = 0;
            chkCheckAll.Text = "chkCheckAll";
            chkCheckAll.ThreeState = true;
            chkCheckAll.UseVisualStyleBackColor = true;
            // 
            // pnlCheckedList
            // 
            pnlCheckedList.AutoScroll = true;
            pnlCheckedList.BackColor = System.Drawing.Color.White;
            pnlCheckedList.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCheckedList.Location = new System.Drawing.Point(0, 70);
            pnlCheckedList.Margin = new System.Windows.Forms.Padding(0);
            pnlCheckedList.Name = "pnlCheckedList";
            pnlCheckedList.Size = new System.Drawing.Size(952, 418);
            pnlCheckedList.TabIndex = 1;
            // 
            // FormFilterRow
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(960, 540);
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormFilterRow";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormFilterRows";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            tlpWorkSpace.ResumeLayout(false);
            pnlCaption.ResumeLayout(false);
            pnlTools.ResumeLayout(false);
            pnlCheckAll.ResumeLayout(false);
            pnlCheckAll.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Panel pnlCheckedList;
        private System.Windows.Forms.Panel pnlTools;
        private System.Windows.Forms.Panel pnlCheckAll;
        private System.Windows.Forms.CheckBox chkCheckAll;
    }

}