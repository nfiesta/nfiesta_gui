﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro nastavení filtru řádků (číselné hodnoty)</para>
    /// <para lang="en">Form for setting row filter (numeric values)</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormFilterRowNumeric
            : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </summary>
        private DataGridView dgvData;

        /// <summary>
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </summary>
        private DataGridViewColumn dgvColumn;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="dgvData">
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </param>
        /// <param name="dgvColumn">
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </param>
        public FormFilterRowNumeric(
            Control controlOwner,
            DataGridView dgvData,
            DataGridViewColumn dgvColumn)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                dgvData: dgvData,
                dgvColumn: dgvColumn);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlViewOLAP)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlViewOLAP)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlViewOLAP)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlViewOLAP)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">
        /// Typ zobrazení ovládacího prvku.
        /// Zobrazuje se OLAP pro úhrny nebo OLAP pro podíly? (read-only)</para>
        /// <para lang="en">
        /// Control display type
        /// Is the OLAP for total or the OLAP for ratio displayed? (read-only)</para>
        /// </summary>
        public DisplayType DisplayType
        {
            get
            {
                return ((ControlViewOLAP)ControlOwner).DisplayType;
            }
        }

        /// <summary>
        /// <para lang="cs">Filter řádků v DataGridView (read-only)</para>
        /// <para lang="en">Row filter in DataGridView (read-only)</para>
        /// </summary>
        public FilterRow RowFilter
        {
            get
            {
                return ((ControlViewOLAP)ControlOwner).RowFilter;
            }
            set
            {
                ((ControlViewOLAP)ControlOwner).RowFilter = value;
            }
        }

        /// <summary>
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </summary>
        public DataGridView DgvData
        {
            get
            {
                return dgvData ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvData)} must not be null.",
                        paramName: nameof(DgvData));
            }
            set
            {
                dgvData = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvData)} must not be null.",
                        paramName: nameof(DgvData));
            }
        }

        /// <summary>
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </summary>
        public DataGridViewColumn DgvColumn
        {
            get
            {
                return dgvColumn ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvColumn)} must not be null.",
                        paramName: nameof(DgvColumn));
            }
            set
            {
                dgvColumn = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(DgvColumn)} must not be null.",
                        paramName: nameof(DgvColumn));
            }
        }

        /// <summary>
        /// <para lang="cs">ColumnMetadata (read-only)</para>
        /// <para lang="en">ColumnMetadata (read-only)</para>
        /// </summary>
        public ColumnMetadata ColMetadata
        {
            get
            {
                return
                     ControlViewOLAP.GetColumnMetadata(
                        displayType: DisplayType,
                        dgvData: DgvData,
                        dgvColumn: DgvColumn);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam unikátních hodnot vyskytujících se ve zvoleném datovém sloupci (read-only)</para>
        /// <para lang="en">List of unique values occurring in the selected data column (read-only)</para>
        /// </summary>
        private List<ComboBoxItem> Items
        {
            get
            {
                List<ComboBoxItem> items
                    = [new ComboBoxItem(controlOwner: this) { IsEmpty = true }];

                if (DgvData.Tag == null)
                {
                    return items;
                }

                if (ColMetadata == null)
                {
                    return items;
                }

                switch (DisplayType)
                {
                    case DisplayType.Total:
                        items =
                            [.. ((OLAPTotalEstimateList)DgvData.Tag).Data.AsEnumerable()
                                .Select(a =>
                                    Functions.GetNDoubleArg(
                                        row: a,
                                        name: ColMetadata.Name,
                                        defaultValue: null))
                                .Where(a => a != null)
                                .Select(a => new ComboBoxItem(controlOwner: this) { IsEmpty = false, Value = a })];
                        items.Add(new ComboBoxItem(controlOwner: this) { IsEmpty = true });
                        return
                            [.. items.Distinct().OrderBy(a => a)];

                    case DisplayType.Ratio:
                        items =
                           [.. ((OLAPRatioEstimateList)DgvData.Tag).Data.AsEnumerable()
                                .Select(a =>
                                    Functions.GetNDoubleArg(
                                        row: a,
                                        name: ColMetadata.Name,
                                        defaultValue: null))
                                .Select(a => new ComboBoxItem(controlOwner: this) { IsEmpty = false, Value = a })];
                        items.Add(new ComboBoxItem(controlOwner: this) { IsEmpty = true });
                        return
                            [.. items.Distinct().OrderBy(a => a)];

                    default:
                        return items;
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormFilterRowNumeric),     "Výběr řádků tabulky" },
                        { nameof(btnCancel),                "Zrušit" },
                        { nameof(btnOK),                    "OK" },
                        { nameof(cboNumericValue1),         "Label" },
                        { nameof(cboNumericValue2),         "Label" },
                        { nameof(cboRelationOperators1),    "LabelCs" },
                        { nameof(cboRelationOperators2),    "LabelCs" },
                        { nameof(rdoAnd),                   "A" },
                        { nameof(rdoOr),                    "NEBO" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormFilterRowNumeric),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormFilterRowNumeric),     "Table rows selection" },
                        { nameof(btnCancel),                "Cancel" },
                        { nameof(btnOK),                    "OK" },
                        { nameof(cboNumericValue1),         "Label" },
                        { nameof(cboNumericValue2),         "Label" },
                        { nameof(cboRelationOperators1),    "LabelEn" },
                        { nameof(cboRelationOperators2),    "LabelEn" },
                        { nameof(rdoAnd),                   "AND" },
                        { nameof(rdoOr),                    "OR" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormFilterRowNumeric),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="dgvData">
        /// <para lang="cs">DataGridView</para>
        /// <para lang="en">DataGridView</para>
        /// </param>
        /// <param name="dgvColumn">
        /// <para lang="cs">DataGridViewColumn</para>
        /// <para lang="en">DataGridViewColumn</para>
        /// </param>
        public void Initialize(
            Control controlOwner,
            DataGridView dgvData,
            DataGridViewColumn dgvColumn)
        {
            ControlOwner = controlOwner;
            DgvData = dgvData;
            DgvColumn = dgvColumn;

            cboRelationOperators1.DataSource
                = RelationalOperator.RelationalOperators(languageFile: LanguageFile);

            cboRelationOperators2.DataSource
                = RelationalOperator.RelationalOperators(languageFile: LanguageFile);

            cboNumericValue1.DataSource
                = Items;

            cboNumericValue2.DataSource
                = Items;

            rdoAnd.Checked = true;

            rdoOr.Checked = false;

            InitializeLabels();

            LoadContent();

            btnOK.Click += new EventHandler(
               (sender, e) =>
               {
                   SetRowFilter();
                   DialogResult = DialogResult.OK;
                   Close();
               });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormFilterRowNumeric),
                    out string frmFilterRowNumericText)
                        ? frmFilterRowNumericText
                        : String.Empty;

            rdoAnd.Text =
                labels.TryGetValue(key: nameof(rdoAnd),
                    out string rdoAndText)
                        ? rdoAndText
                        : String.Empty;

            rdoOr.Text =
                labels.TryGetValue(key: nameof(rdoOr),
                    out string rdoOrText)
                        ? rdoOrText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            cboNumericValue1.DisplayMember =
                labels.TryGetValue(key: nameof(cboNumericValue1),
                    out string cboNumericValue1DisplayMember)
                        ? cboNumericValue1DisplayMember
                        : String.Empty;

            cboNumericValue2.DisplayMember =
                labels.TryGetValue(key: nameof(cboNumericValue2),
                    out string cboNumericValue2DisplayMember)
                        ? cboNumericValue2DisplayMember
                        : String.Empty;

            cboRelationOperators1.DisplayMember =
                labels.TryGetValue(key: nameof(cboRelationOperators1),
                    out string cboRelationOperators1DisplayMember)
                        ? cboRelationOperators1DisplayMember
                        : String.Empty;

            cboRelationOperators2.DisplayMember =
                labels.TryGetValue(key: nameof(cboRelationOperators2),
                    out string cboRelationOperators2DisplayMember)
                        ? cboRelationOperators2DisplayMember
                        : String.Empty;

            lblCaption.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? ColMetadata.HeaderTextEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ColMetadata.HeaderTextCs
                        : ColMetadata.HeaderTextEn;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">
        /// Nastavit filtr řádků tabulky</para>
        /// <para lang="en">
        /// Set table rows filter</para>
        /// </summary>
        private void SetRowFilter()
        {
            RelationalOperator operator1 =
                (cboRelationOperators1.SelectedItem != null) ?
                (RelationalOperator)cboRelationOperators1.SelectedItem : null;

            RelationalOperator operator2 =
                (cboRelationOperators2.SelectedItem != null) ?
                (RelationalOperator)cboRelationOperators2.SelectedItem : null;

            ComboBoxItem item1 =
                (cboNumericValue1.SelectedItem != null) ?
                (ComboBoxItem)cboNumericValue1.SelectedItem : null;

            ComboBoxItem item2 =
               (cboNumericValue2.SelectedItem != null) ?
               (ComboBoxItem)cboNumericValue2.SelectedItem : null;

            string part1 = (!operator1.IsEmpty && !item1.IsEmpty) ?
                $"({ColMetadata.Name} {operator1.Operator} {item1.Value})"
                : String.Empty;

            string part2 = (!operator2.IsEmpty && !item2.IsEmpty) ?
               $"({ColMetadata.Name} {operator2.Operator} {item2.Value})"
               : String.Empty;

            string logicalOperator = rdoAnd.Checked ? "AND" : "OR";

            string condition =
                (String.IsNullOrEmpty(value: part1) && String.IsNullOrEmpty(value: part2)) ? String.Empty :
                (!String.IsNullOrEmpty(value: part1) && String.IsNullOrEmpty(value: part2)) ? part1 :
                (String.IsNullOrEmpty(value: part1) && !String.IsNullOrEmpty(value: part2)) ? part2 :
                $"({part1} {logicalOperator} {part2})";

            MessageBox.Show(
                text: condition,
                caption: String.Empty,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);

            FilterRow rowFilter = RowFilter;

            if (String.IsNullOrEmpty(value: condition))
            {
                rowFilter.Remove(
                    column: ColMetadata);
            }
            else
            {
                rowFilter.Add(
                  column: ColMetadata,
                  condition: condition);
            }

            RowFilter = rowFilter;
        }

        #endregion Methods


        #region Private Classes

        /// <summary>
        /// <para lang="cs">Třída: položka ComboBoxu</para>
        /// <para lang="en">Class: ComboBox item</para>
        /// </summary>
        private class ComboBoxItem : IComparable
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vlastník</para>
            /// <para lang="en">Owner</para>
            /// </summary>
            private Control controlOwner;

            /// <summary>
            /// <para lang="cs">Prázdná položka</para>
            /// <para lang="en">Empty item</para>
            /// </summary>
            private bool isEmpty;

            /// <summary>
            /// <para lang="cs">Hodnota</para>
            /// <para lang="en">Value</para>
            /// </summary>
            private Nullable<double> val;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="controlOwner">
            /// <para lang="cs">Vlastník</para>
            /// <para lang="en">Owner</para>
            /// </param>
            public ComboBoxItem(Control controlOwner)
            {
                ControlOwner = controlOwner;
                IsEmpty = true;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Vlastník</para>
            /// <para lang="en">Owner</para>
            /// </summary>
            public Control ControlOwner
            {
                get
                {
                    if (controlOwner == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(ControlOwner)} must not be null.",
                            paramName: nameof(ControlOwner));
                    }

                    if (controlOwner is not FormFilterRowNumeric)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormFilterRowNumeric)}.",
                            paramName: nameof(ControlOwner));
                    }

                    return controlOwner;
                }
                private set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(ControlOwner)} must not be null.",
                            paramName: nameof(ControlOwner));
                    }

                    if (value is not FormFilterRowNumeric)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormFilterRowNumeric)}.",
                            paramName: nameof(ControlOwner));
                    }

                    controlOwner = value;
                }
            }

            /// <summary>
            /// <para lang="cs">ColumnMetadata (read-only)</para>
            /// <para lang="en">ColumnMetadata (read-only)</para>
            /// </summary>
            public ColumnMetadata ColMetadata
            {
                get
                {
                    return ((FormFilterRowNumeric)ControlOwner).ColMetadata;
                }
            }

            /// <summary>
            /// <para lang="cs">Prázdná položka</para>
            /// <para lang="en">Empty item</para>
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    return isEmpty;
                }
                set
                {
                    isEmpty = value;
                    if (IsEmpty)
                    {
                        Value = null;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Hodnota</para>
            /// <para lang="en">Value</para>
            /// </summary>
            public Nullable<double> Value
            {
                get
                {
                    return val;
                }
                set
                {
                    val = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Popisek</para>
            /// <para lang="en">Label</para>
            /// </summary>
            public string Label
            {
                get
                {
                    if (IsEmpty)
                    {
                        return String.Empty;
                    }

                    if (Value == null)
                    {
                        return Functions.StrNull;
                    }

                    return ((double)Value).ToString();
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Compares current object with other object
            /// </summary>
            /// <param name="obj">Other object</param>
            /// <returns>
            /// +1 if current object is greater than other object,
            /// 0 if they are equal,
            /// -1 if current object is less than other object</returns>
            public int CompareTo(object obj)
            {
                ComboBoxItem other = (ComboBoxItem)obj;

                if (IsEmpty)
                {
                    if (other.IsEmpty)
                    {
                        return 0;
                    }
                    else if (other.Value == null)
                    {
                        return -1;
                    }
                    else
                    {
                        return -1;
                    }
                }

                else if (Value == null)
                {
                    if (other.IsEmpty)
                    {
                        return 1;
                    }
                    else if (other.Value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }

                else
                {
                    if (other.IsEmpty)
                    {
                        return 1;
                    }
                    else if (other.Value == null)
                    {
                        return 1;
                    }
                    else
                    {
                        if ((double)Value > (double)other.Value)
                        {
                            return 1;
                        }
                        else if ((double)Value == (double)Value)
                        {
                            return 0;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
            }

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true/false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not ComboBoxItem Type, return False
                if (obj is not ComboBoxItem)
                {
                    return false;
                }

                if (IsEmpty)
                {
                    return ((ComboBoxItem)obj).IsEmpty;
                }

                if (Value == null)
                {
                    return ((ComboBoxItem)obj).Value == null;
                }

                return
                    Value == ((ComboBoxItem)obj).Value;
            }

            /// <summary>
            /// Returns the hash code
            /// </summary>
            /// <returns>Hash code</returns>
            public override int GetHashCode()
            {
                return
                    IsEmpty.GetHashCode() ^
                    Value.GetHashCode();
            }

            /// <summary>
            /// Returns the string that represents current object
            /// </summary>
            /// <returns>Returns the string that represents current object</returns>
            public override string ToString()
            {
                return Label;
            }

            #endregion Methods

        }

        #endregion Private Classes

    }

}