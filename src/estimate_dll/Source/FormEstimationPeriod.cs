﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Formulář pro editaci výpočetních období</para>
    /// <para lang="en">Form for editing estimation periods</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormEstimationPeriod
           : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Editace výpočetních období není povolena</para>
        /// <para lang="en">Editing of estimation periods is not allowed</para>
        /// </summary>
        private bool readOnlyMode;

        /// <summary>
        /// <para lang="cs">Indikátor změny v databázi</para>
        /// <para lang="en">Database changed indicator</para>
        /// </summary>
        private bool dbChanged;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        private string msgNoneSelectedEstimationPeriod = String.Empty;
        private string msgSelectedEstimationPeriodNotExists = String.Empty;
        private string msgNoneSelectedEstimationPeriodToDelete = String.Empty;
        private string msgSelectedEstimationPeriodCannotBeDeleted = String.Empty;
        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        private string strRowsCountNone = String.Empty;
        private string strRowsCountOne = String.Empty;
        private string strRowsCountSome = String.Empty;
        private string strRowsCountMany = String.Empty;

        /// <summary>
        /// <para lang="cs">Seznam období odhadu, který vrací funkce fn_api_get_list_of_estimation_periods</para>
        /// <para lang="en">A list of estimation periods returned by the fn_api_get_list_of_estimation_periods function</para>
        /// </summary>
        private EstimationPeriodList estimationPeriodList;

        #endregion Private Fields


        //#region Controls

        ///// <summary>
        ///// <para lang="cs">Ovládací prvek "Zobrazení období"</para>
        ///// <para lang="en">Control "Display periods"</para>
        ///// </summary>
        //private ControlViewPeriods ctrViewPeriods;

        //#endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormEstimationPeriod(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Editace výpočetních období není povolena</para>
        /// <para lang="en">Editing of calculation periods is not allowed</para>
        /// </summary>
        public bool ReadOnlyMode
        {
            get
            {
                return readOnlyMode;
            }
            set
            {
                readOnlyMode = value;

                txtLabelCsValue.Enabled = !ReadOnlyMode;
                txtLabelEnValue.Enabled = !ReadOnlyMode;
                txtDescriptionCsValue.Enabled = !ReadOnlyMode;
                txtDescriptionEnValue.Enabled = !ReadOnlyMode;

                if (!ReadOnlyMode)
                {
                    btnInsert.Visible = true;
                    tlpEstimationPeriodButtons.ColumnStyles[1].Width = 160;
                }
                else
                {
                    btnInsert.Visible = false;
                    tlpEstimationPeriodButtons.ColumnStyles[1].Width = 0;
                }

                EstimationPeriod selectedEstimationPeriod =
                    (EstimationPeriod)lstEstimationPeriodList.SelectedItem;

                if ((!ReadOnlyMode) &&
                    (selectedEstimationPeriod != null))
                {
                    btnUpdate.Visible = true;
                    tlpEstimationPeriodButtons.ColumnStyles[2].Width = 160;
                }
                else
                {
                    btnUpdate.Visible = false;
                    tlpEstimationPeriodButtons.ColumnStyles[2].Width = 0;
                }

                if ((!ReadOnlyMode) &&
                    (selectedEstimationPeriod != null))
                {
                    btnDelete.Visible = true;
                    tlpEstimationPeriodButtons.ColumnStyles[3].Width = 160;
                }
                else
                {
                    btnDelete.Visible = false;
                    tlpEstimationPeriodButtons.ColumnStyles[3].Width = 0;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor změny v databázi</para>
        /// <para lang="en">Database changed indicator</para>
        /// </summary>
        public bool DbChanged
        {
            get
            {
                return dbChanged;
            }
            set
            {
                dbChanged = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimationPeriod),                         "Výpočetní období" },
                        { nameof(btnClose),                                     "Zavřít" },
                        { nameof(btnInsert),                                    "Nové výpočetní období" },
                        { nameof(btnUpdate),                                    "Zapsat do databáze" },
                        { nameof(btnDelete),                                    "Smazat z databáze" },
                        { nameof(grpEstimationPeriod),                          "Vybrané výpočetní období:" },
                        { nameof(grpEstimationPeriodList),                      "Seznam výpočetních období:" },
                        { nameof(lstEstimationPeriodList),                      "LabelCs" },
                        { nameof(lblIdCaption),                                 "Identifikační číslo:" },
                        { nameof(lblEstimationDateBeginCaption),                "Začátek období:" },
                        { nameof(lblEstimationDateEndCaption),                  "Konec období:" },
                        { nameof(lblLabelCsCaption),                            "Zkratka (národní):" },
                        { nameof(lblDescriptionCsCaption),                      "Popis (národní):" },
                        { nameof(lblLabelEnCaption),                            "Zkratka (anglická):" },
                        { nameof(lblDescriptionEnCaption),                      "Popis (anglický):" },
                        { nameof(lblDefaultInOlap),                             "Aktivní jako výchozí:" },
                        { nameof(tipDefaultInOlap),                             "Omezuje zobrazení období na období nastavená na ano (projeví se až při dalším výpočtu)." },
                        { nameof(msgNoneSelectedEstimationPeriod),              "Není vybrané žádné výpočetní období pro editaci"},
                        { nameof(msgSelectedEstimationPeriodNotExists),         "Vybrané výpočetní období neexistuje." },
                        { nameof(msgNoneSelectedEstimationPeriodToDelete),      "Není vybrané žádné výpočetní období pro smazání."},
                        { nameof(msgSelectedEstimationPeriodCannotBeDeleted),   "Vybrané výpočetní období již bylo použito pro výpočet odhadu a nelze je smazat."},
                        { nameof(msgLabelCsIsEmpty),                            "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),                      "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),                            "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),                      "Popis (anglický) musí být vyplněn." },
                        { nameof(msgLabelCsExists),                             "Použitá zkratka (národní) již existuje pro jiné výpočetní období." },
                        { nameof(msgDescriptionCsExists),                       "Použitý popis (národní) již existuje pro jiné výpočetní období." },
                        { nameof(msgLabelEnExists),                             "Použitá zkratka (anglická) již existuje pro jiné výpočetní období." },
                        { nameof(msgDescriptionEnExists),                       "Použitý popis (anglický) již existuje pro jiné výpočetní období." },
                        { nameof(strRowsCountNone),                             "Není vybráno žádné období." },
                        { nameof(strRowsCountOne),                              "Je vybráno $1 období." },
                        { nameof(strRowsCountSome),                             "Jsou vybrána $1 období." },
                        { nameof(strRowsCountMany),                             "Je vybráno $1 období." },
                        { nameof(grpEstimationPeriods),                         "Období:" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}",
                            "Identifikační číslo období" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}",
                            "Začátek období" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}",
                            "Konec období" },
                        { nameof(rdoFalse),                                     "ne" },
                        { nameof(rdoTrue),                                      "ano" },
                     }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimationPeriod),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormEstimationPeriod),                         "Estimation periods" },
                        { nameof(btnClose),                                     "Close" },
                        { nameof(btnInsert),                                    "New estimation period" },
                        { nameof(btnUpdate),                                    "Update in database" },
                        { nameof(btnDelete),                                    "Delete from database" },
                        { nameof(grpEstimationPeriodList),                      "List of estimation periods:" },
                        { nameof(grpEstimationPeriod),                          "Selected estimation period:" },
                        { nameof(lstEstimationPeriodList),                      "LabelEn" },
                        { nameof(lblIdCaption),                                 "Identifier:" },
                        { nameof(lblEstimationDateBeginCaption),                "Estimation date begin:" },
                        { nameof(lblEstimationDateEndCaption),                  "Estimation date end:" },
                        { nameof(lblLabelCsCaption),                            "Label (national):" },
                        { nameof(lblDescriptionCsCaption),                      "Description (national):" },
                        { nameof(lblLabelEnCaption),                            "Label (English):" },
                        { nameof(lblDescriptionEnCaption),                      "Description (English):" },
                        { nameof(lblDefaultInOlap),                             "Active by default:" },
                        { nameof(tipDefaultInOlap),                             "Limits the display of periods to those set to yes (it performs in the next calculation)." },
                        { nameof(msgNoneSelectedEstimationPeriod),              "There is no estimation period selected for edit."},
                        { nameof(msgSelectedEstimationPeriodNotExists),         "Selected estimation period does not exist." },
                        { nameof(msgNoneSelectedEstimationPeriodToDelete),      "There is no estimation period selected to be deleted."},
                        { nameof(msgSelectedEstimationPeriodCannotBeDeleted),   "Selected estimation period has been already used in the estimation calculation and it cannot be deleted."},
                        { nameof(msgLabelCsIsEmpty),                            "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),                      "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),                            "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),                      "Description (English) cannot be empty." },
                        { nameof(msgLabelCsExists),                             "This label (national) already exists for another estimation period." },
                        { nameof(msgDescriptionCsExists),                       "This description (national) already exists for another estimation period." },
                        { nameof(msgLabelEnExists),                             "This label (English) already exists for another estimation period." },
                        { nameof(msgDescriptionEnExists),                       "This description (English) already exists for another estimation period." },
                        { nameof(strRowsCountNone),                             "No estimation period is selected." },
                        { nameof(strRowsCountOne),                              "$1 estimation period is selected." },
                        { nameof(strRowsCountSome),                             "$1 estimation periods are selected." },
                        { nameof(strRowsCountMany),                             "$1 estimation periods are selected." },
                        { nameof(grpEstimationPeriods),                         "Periods:" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}",
                            "Estimation period identifier" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}",
                            "Estimate date begin" },
                        { $"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}",
                            "Estimate date end" },
                        { nameof(rdoFalse),                                     "no" },
                        { nameof(rdoTrue),                                      "yes" },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormEstimationPeriod),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            onEdit = false;

            //InitializeViewPeriods();

            LoadContent();

            ReadOnlyMode = false;

            DbChanged = false;

            InitializeLabels();

            EstimationPeriodSelected();

            btnInsert.Click += new EventHandler(
               (sender, e) =>
               {
                   InsertIntoDb();
               });

            btnUpdate.Click += new EventHandler(
                (sender, e) =>
                {
                    UpdateDb();
                });

            btnDelete.Click += new EventHandler(
                (sender, e) =>
                {
                    DeleteFromDb();
                });

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            lstEstimationPeriodList.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        EstimationPeriodSelected();
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            //ctrViewPeriods?.InitializeLabels();

            Text =
                labels.TryGetValue(key: nameof(FormEstimationPeriod),
                out string frmFormEstimationPeriodText)
                    ? frmFormEstimationPeriodText
                    : String.Empty;

            btnClose.Text =
                labels.TryGetValue(key: nameof(btnClose),
                out string btnCloseText)
                    ? btnCloseText
                    : String.Empty;

            btnInsert.Text =
                labels.TryGetValue(key: nameof(btnInsert),
                out string btnInsertText)
                    ? btnInsertText
                    : String.Empty;

            btnUpdate.Text =
                labels.TryGetValue(key: nameof(btnUpdate),
                out string btnUpdateText)
                    ? btnUpdateText
                    : String.Empty;

            btnDelete.Text =
                labels.TryGetValue(key: nameof(btnDelete),
                out string btnDeleteText)
                    ? btnDeleteText
                    : String.Empty;

            grpEstimationPeriodList.Text =
                labels.TryGetValue(key: nameof(grpEstimationPeriodList),
                out string grpEstimationPeriodListText)
                    ? grpEstimationPeriodListText
                    : String.Empty;

            grpEstimationPeriod.Text =
                labels.TryGetValue(key: nameof(grpEstimationPeriod),
                out string grpEstimationPeriodText)
                    ? grpEstimationPeriodText
                    : String.Empty;

            grpEstimationPeriods.Text =
                labels.TryGetValue(key: nameof(grpEstimationPeriods),
                out string grpEstimationPeriodsText)
                    ? grpEstimationPeriodsText
                    : String.Empty;

            onEdit = true;
            lstEstimationPeriodList.DisplayMember =
                labels.TryGetValue(key: nameof(lstEstimationPeriodList),
                out string lstEstimationPeriodListText)
                    ? lstEstimationPeriodListText
                    : String.Empty;
            onEdit = false;

            lblIdCaption.Text =
                labels.TryGetValue(key: nameof(lblIdCaption),
                out string lblIdCaptionText)
                    ? lblIdCaptionText
                    : String.Empty;

            lblEstimationDateBeginCaption.Text =
                labels.TryGetValue(key: nameof(lblEstimationDateBeginCaption),
                out string lblEstimationDateBeginCaptionText)
                    ? lblEstimationDateBeginCaptionText
                    : String.Empty;

            lblEstimationDateEndCaption.Text =
                labels.TryGetValue(key: nameof(lblEstimationDateEndCaption),
                out string lblEstimationDateEndCaptionText)
                    ? lblEstimationDateEndCaptionText
                    : String.Empty;

            lblLabelCsCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCsCaption),
                out string lblLabelCsCaptionText)
                    ? lblLabelCsCaptionText
                    : String.Empty;

            lblDescriptionCsCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionCsCaption),
                out string lblDescriptionCsCaptionText)
                    ? lblDescriptionCsCaptionText
                    : String.Empty;

            lblLabelEnCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelEnCaption),
                out string lblLabelEnCaptionText)
                    ? lblLabelEnCaptionText
                    : String.Empty;

            lblDescriptionEnCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionEnCaption),
                out string lblDescriptionEnCaptionText)
                    ? lblDescriptionEnCaptionText
                    : String.Empty;

            lblDefaultInOlap.Text =
                labels.TryGetValue(key: nameof(lblDefaultInOlap),
                out string lblDefaultInOlapText)
                    ? lblDefaultInOlapText
                    : String.Empty;

            rdoFalse.Text =
                labels.TryGetValue(key: nameof(rdoFalse),
                out string rdoFalseText)
                    ? rdoFalseText
                    : String.Empty;

            rdoTrue.Text =
                labels.TryGetValue(key: nameof(rdoTrue),
                out string rdoTrueText)
                    ? rdoTrueText
                    : String.Empty;

            msgNoneSelectedEstimationPeriod =
                labels.TryGetValue(key: nameof(msgNoneSelectedEstimationPeriod),
                out msgNoneSelectedEstimationPeriod)
                    ? msgNoneSelectedEstimationPeriod
                    : String.Empty;

            msgSelectedEstimationPeriodNotExists =
               labels.TryGetValue(key: nameof(msgSelectedEstimationPeriodNotExists),
               out msgSelectedEstimationPeriodNotExists)
                   ? msgSelectedEstimationPeriodNotExists
                   : String.Empty;

            msgNoneSelectedEstimationPeriodToDelete =
                labels.TryGetValue(key: nameof(msgNoneSelectedEstimationPeriodToDelete),
                out msgNoneSelectedEstimationPeriodToDelete)
                    ? msgNoneSelectedEstimationPeriodToDelete
                    : String.Empty;

            msgSelectedEstimationPeriodCannotBeDeleted =
               labels.TryGetValue(key: nameof(msgSelectedEstimationPeriodCannotBeDeleted),
               out msgSelectedEstimationPeriodCannotBeDeleted)
                   ? msgSelectedEstimationPeriodCannotBeDeleted
                   : String.Empty;

            msgLabelCsIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                out msgLabelCsIsEmpty)
                    ? msgLabelCsIsEmpty
                    : String.Empty;

            msgDescriptionCsIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                out msgDescriptionCsIsEmpty)
                    ? msgDescriptionCsIsEmpty
                    : String.Empty;

            msgLabelEnIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                out msgLabelEnIsEmpty)
                    ? msgLabelEnIsEmpty
                    : String.Empty;

            msgDescriptionEnIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                out msgDescriptionEnIsEmpty)
                    ? msgDescriptionEnIsEmpty
                    : String.Empty;

            msgLabelCsExists =
                labels.TryGetValue(key: nameof(msgLabelCsExists),
                out msgLabelCsExists)
                    ? msgLabelCsExists
                    : String.Empty;

            msgDescriptionCsExists =
                labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                out msgDescriptionCsExists)
                    ? msgDescriptionCsExists
                    : String.Empty;

            msgLabelEnExists =
                labels.TryGetValue(key: nameof(msgLabelEnExists),
                out msgLabelEnExists)
                    ? msgLabelEnExists
                    : String.Empty;

            msgDescriptionEnExists =
                labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                out msgDescriptionEnExists)
                    ? msgDescriptionEnExists
                    : String.Empty;

            EstimationPeriodList.SetColumnHeader(
               columnName: EstimationPeriodList.ColId.Name,
               headerTextCs: labels[$"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}"],
               headerTextEn: labels[$"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColId.Name}"]
            );

            EstimationPeriodList.SetColumnHeader(
               columnName: EstimationPeriodList.ColEstimateDateBegin.Name,
               headerTextCs: labels[$"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}"],
               headerTextEn: labels[$"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateBegin.Name}"]
            );

            EstimationPeriodList.SetColumnHeader(
              columnName: EstimationPeriodList.ColEstimateDateEnd.Name,
              headerTextCs: labels[$"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}"],
              headerTextEn: labels[$"col-{EstimationPeriodList.Name}.{EstimationPeriodList.ColEstimateDateEnd.Name}"]
           );
        }

        ///// <summary>
        ///// <para lang="cs">Inicializace ovládacího prvku pro "Zobrazení období"</para>
        ///// <para lang="en">Initialization of the control for "Display periods"</para>
        ///// </summary>
        //private void InitializeViewPeriods()
        //{
        //    pnlEstimationPeriods.Controls.Clear();

        //    ctrViewPeriods =
        //        new ControlViewPeriods(controlOwner: this)
        //        {
        //            DisplayButtonFilterColumns = false,
        //            DisplayButtonClose = false,
        //            Dock = DockStyle.Fill
        //        };

        //    ctrViewPeriods.Closed += (sender, e) => { };

        //    ctrViewPeriods.CreateControl();

        //    pnlEstimationPeriods.Controls.Add(value: ctrViewPeriods);
        //}

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent()
        {
            onEdit = true;

            estimationPeriodList = NfiEstaFunctions.FnApiGetListOfEstimationPeriods.Execute(Database);

            lstEstimationPeriodList.DataSource = estimationPeriodList.Items;
            onEdit = false;

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Vybrání výpočetního období</para>
        /// <para lang="en">Selecting the estimation period</para>
        /// </summary>
        private void EstimationPeriodSelected()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            EstimationPeriod selectedEstimationPeriod =
                (lstEstimationPeriodList.SelectedItem == null) ? null :
                (EstimationPeriod)lstEstimationPeriodList.SelectedItem;

            lblIdCaption.Visible = (selectedEstimationPeriod != null);
            lblIdValue.Visible = (selectedEstimationPeriod != null);

            lblEstimationDateBeginCaption.Visible = (selectedEstimationPeriod != null);
            lblEstimationDateBeginValue.Visible = (selectedEstimationPeriod != null);

            lblEstimationDateEndCaption.Visible = (selectedEstimationPeriod != null);
            lblEstimationDateEndValue.Visible = (selectedEstimationPeriod != null);

            lblLabelCsCaption.Visible = (selectedEstimationPeriod != null);
            txtLabelCsValue.Visible = (selectedEstimationPeriod != null);

            lblDescriptionCsCaption.Visible = (selectedEstimationPeriod != null);
            txtDescriptionCsValue.Visible = (selectedEstimationPeriod != null);

            lblLabelEnCaption.Visible = (selectedEstimationPeriod != null);
            txtLabelEnValue.Visible = (selectedEstimationPeriod != null);

            lblDescriptionEnCaption.Visible = (selectedEstimationPeriod != null);
            txtDescriptionEnValue.Visible = (selectedEstimationPeriod != null);

            lblDefaultInOlap.Visible = (selectedEstimationPeriod != null);
            rdoFalse.Visible = (selectedEstimationPeriod != null);
            rdoTrue.Visible = (selectedEstimationPeriod != null);
            tipDefaultInOlap.SetToolTip(lblDefaultInOlap, labels.ContainsKey(key: nameof(tipDefaultInOlap)) ? labels[nameof(tipDefaultInOlap)] : String.Empty);

            btnUpdate.Visible = (selectedEstimationPeriod != null) && (!this.readOnlyMode);

            if (selectedEstimationPeriod != null)
            {
                lblIdValue.Text = selectedEstimationPeriod.Id.ToString();
                lblEstimationDateBeginValue.Text = selectedEstimationPeriod.EstimateDateBeginText;
                lblEstimationDateEndValue.Text = selectedEstimationPeriod.EstimateDateEndText;
                txtLabelCsValue.Text = selectedEstimationPeriod.LabelCs;
                txtDescriptionCsValue.Text = selectedEstimationPeriod.DescriptionCs;
                txtLabelEnValue.Text = selectedEstimationPeriod.LabelEn;
                txtDescriptionEnValue.Text = selectedEstimationPeriod.DescriptionEn;

                if (selectedEstimationPeriod.DefaultInOlap)
                {
                    rdoTrue.Checked = true;
                    rdoFalse.Checked = false;
                }
                else
                {
                    rdoTrue.Checked = false;
                    rdoFalse.Checked = true;
                }

                LoadContentOfDgvEstimationPeriods();
            }
            else
            {
                lblIdValue.Text = String.Empty;
                lblEstimationDateBeginValue.Text = String.Empty;
                lblEstimationDateEndValue.Text = String.Empty;
                txtLabelCsValue.Text = String.Empty;
                txtDescriptionCsValue.Text = String.Empty;
                txtLabelEnValue.Text = String.Empty;
                txtDescriptionEnValue.Text = String.Empty;
                rdoTrue.Checked = false;
                rdoFalse.Checked = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat do DataGridView dgvEstimationPeriods</para>
        /// <para lang="en">Uploading data in the dgvEstimationPeriods DataGridView</para>
        /// </summary>
        private void LoadContentOfDgvEstimationPeriods()
        {
            grpEstimationPeriods.Controls.Clear();
            DataGridView dgvEstimationPeriods = new() { Dock = DockStyle.Fill };
            grpEstimationPeriods.Controls.Add(dgvEstimationPeriods);

            EstimationPeriodList.SetColumnsVisibility();

            EstimationPeriodList.ColLabelCs.Visible = false;
            EstimationPeriodList.ColDescriptionCs.Visible = false;
            EstimationPeriodList.ColLabelEn.Visible = false;
            EstimationPeriodList.ColDescriptionEn.Visible = false;
            EstimationPeriodList.ColDefaultInOlap.Visible = false;

            dgvEstimationPeriods.DataSource = estimationPeriodList.Data;
            estimationPeriodList.FormatDataGridView(dgvEstimationPeriods);

            SetLabelRowsCountValue(count: dgvEstimationPeriods.Rows.Count);
        }

        /// <summary>
        /// <para lang="cs">Nastavení popisku s počtem vybraných řádků</para>
        /// <para lang="en">Setting the label with the number of selected rows</para>
        /// </summary>
        /// <param name="count">
        /// <para lang="cs">Počet vybraných řádků</para>
        /// <para lang="en">Number of selected rows</para>
        /// </param>
        private void SetLabelRowsCountValue(int count)
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            switch (count)
            {
                case 0:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountNone),
                            out strRowsCountNone)
                                ? strRowsCountNone
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 1:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountOne),
                            out strRowsCountOne)
                                ? strRowsCountOne
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 2:
                case 3:
                case 4:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountSome),
                            out strRowsCountSome)
                                ? strRowsCountSome
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                default:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountMany),
                            out strRowsCountMany)
                                ? strRowsCountMany
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis nového výpočetního období do databáze</para>
        /// <para lang="en">Inserting a new estimation period into the database</para>
        /// </summary>
        private void InsertIntoDb()
        {
            FormEstimationPeriodNew frmEstimationPeriodNew = new(controlOwner: this);

            if (frmEstimationPeriodNew.ShowDialog() == DialogResult.OK)
            {
                DbChanged = true;
                LoadContent();

                if (lstEstimationPeriodList.Items.Count > 0)
                {
                    lstEstimationPeriodList.SelectedIndex = lstEstimationPeriodList.Items.Count - 1;

                    if (!ReadOnlyMode)
                    {
                        btnUpdate.Visible = true;
                        tlpEstimationPeriodButtons.ColumnStyles[2].Width = 160;
                        btnDelete.Visible = true;
                        tlpEstimationPeriodButtons.ColumnStyles[3].Width = 160;
                    }
                    else
                    {
                        btnUpdate.Visible = false;
                        tlpEstimationPeriodButtons.ColumnStyles[2].Width = 0;
                        btnDelete.Visible = false;
                        tlpEstimationPeriodButtons.ColumnStyles[3].Width = 0;
                    }

                }

                EstimationPeriodSelected();
                LoadContentOfDgvEstimationPeriods();
            }
        }

        /// <summary>
        /// <para lang="cs">Aktualizace vybraného výpočetního období v databázi</para>
        /// <para lang="en">Update the selected estimation period in the database</para>
        /// </summary>
        private void UpdateDb()
        {
            EstimationPeriod selectedEstimationPeriod =
                (lstEstimationPeriodList.SelectedItem == null) ? null :
                (EstimationPeriod)lstEstimationPeriodList.SelectedItem;

            string labelCs = txtLabelCsValue.Text.Trim();
            string descriptionCs = txtDescriptionCsValue.Text.Trim();
            string labelEn = txtLabelEnValue.Text.Trim();
            string descriptionEn = txtDescriptionEnValue.Text.Trim();
            bool defaultInOlap = rdoTrue.Checked;

            if (selectedEstimationPeriod == null)
            {
                // Žádné období není vybrané pro editaci
                // No period is selected to be updated
                MessageBox.Show(
                    text: msgNoneSelectedEstimationPeriod,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if (String.IsNullOrEmpty(value: labelCs))
            {
                // LabelCs - prázdné
                // LabelCs - empty
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                // DescriptionCs - prázdné
                // DescriptionCs - empty
                MessageBox.Show(
                    text: msgDescriptionCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                // LabelEn - prázdné
                // LabelEn - empty
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                // DescriptionEn - prázdné
                // DescriptionEn - empty
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if ((selectedEstimationPeriod.LabelCs != labelCs) &&
                 estimationPeriodList.Items
                    .Where(a => a.LabelCs == labelCs)
                    .Any())
            {
                // LabelCs - duplicitní
                // LabelCs - duplicate
                MessageBox.Show(
                    text: msgLabelCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if ((selectedEstimationPeriod.DescriptionCs != descriptionCs) &&
                 estimationPeriodList.Items
                    .Where(a => a.DescriptionCs == descriptionCs)
                    .Any())
            {
                // DescriptionCs - duplicitní
                // DescriptionCs - duplicate
                MessageBox.Show(
                    text: msgDescriptionCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if ((selectedEstimationPeriod.LabelEn != labelEn) &&
                 estimationPeriodList.Items
                    .Where(a => a.LabelEn == labelEn)
                    .Any())
            {
                // LabelEn - duplicitní
                // LabelEn - duplicate
                MessageBox.Show(
                    text: msgLabelEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if ((selectedEstimationPeriod.DescriptionEn != descriptionEn) &&
                estimationPeriodList.Items
                   .Where(a => a.DescriptionEn == descriptionEn)
                   .Any())
            {
                // DescriptionEn - duplicitní
                // DescriptionEn - duplicate
                MessageBox.Show(
                    text: msgDescriptionEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            selectedEstimationPeriod.LabelCs = labelCs;
            selectedEstimationPeriod.DescriptionCs = descriptionCs;
            selectedEstimationPeriod.LabelEn = labelEn;
            selectedEstimationPeriod.DescriptionEn = descriptionEn;
            selectedEstimationPeriod.DefaultInOlap = defaultInOlap;

            //Database.SNfiEsta.CEstimationPeriod
            //    .Update(item: selectedEstimationPeriod);

            NfiEstaFunctions.FnApiUpdateEstimationPeriod.Execute(
                Database,
                selectedEstimationPeriod.Id,
                labelCs,
                descriptionCs,
                labelEn,
                descriptionEn,
                defaultInOlap);

            DbChanged = true;

            LoadContent();

            // Výběr editované položky, po aktualizaci databáze
            // Selection of the edited item after database update
            if (estimationPeriodList.Items
                   .Where(a => a.Id == selectedEstimationPeriod.Id)
                   .Any())
            {
                lstEstimationPeriodList.SelectedItem =
                    estimationPeriodList.Items
                        .Where(a => a.Id == selectedEstimationPeriod.Id)
                        .First();
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání vybraného výpočetního období z databáze</para>
        /// <para lang="en">Delete the selected estimation period from the database</para>
        /// </summary>
        private void DeleteFromDb()
        {
            // Vybrané výpočetní období
            // Selected estimation period
            EstimationPeriod selectedEstimationPeriod =
                (lstEstimationPeriodList.SelectedItem == null) ? null :
                (EstimationPeriod)lstEstimationPeriodList.SelectedItem;

            if (selectedEstimationPeriod == null)
            {
                // Žádné období není vybrané
                // No group is selected
                MessageBox.Show(
                    text: msgNoneSelectedEstimationPeriodToDelete,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (!estimationPeriodList.Items
                .Where(a => a.Id == selectedEstimationPeriod.Id)
                .Any())
            {
                // Vybrané období není v databázové tabulce (nemůže nastat)
                // Selected period is not in the database table (this cannot happen)
                MessageBox.Show(
                    text: msgSelectedEstimationPeriodNotExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            TFnApiBeforeDeleteEstimationPeriodList result = NfiEstaFunctions.FnApiBeforeDeleteEstimationPeriod.Execute(Database, selectedEstimationPeriod.Id);

            if (result.TotalEstimateConfExist == false)
            {
                NfiEstaFunctions.FnApiDeleteEstimationPeriod.Execute(Database, selectedEstimationPeriod.Id);

                DbChanged = true;

                LoadContent();

                // Vybere první položku v seznamu
                // Selects the first item in the list
                if (lstEstimationPeriodList.Items.Count > 0)
                {
                    lstEstimationPeriodList.SelectedIndex = 0;
                    EstimationPeriodSelected();
                }
                else
                {
                    txtLabelCsValue.Text = String.Empty;
                    txtLabelEnValue.Text = String.Empty;
                    txtDescriptionCsValue.Text = String.Empty;
                    txtDescriptionEnValue.Text = String.Empty;
                    lblIdValue.Text = String.Empty;
                    lblEstimationDateBeginValue.Text = String.Empty;
                    lblEstimationDateEndValue.Text = String.Empty;
                    rdoTrue.Checked = false;
                    rdoFalse.Checked = false;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    tlpEstimationPeriodButtons.ColumnStyles[2].Width = 0;
                    tlpEstimationPeriodButtons.ColumnStyles[3].Width = 0;
                    tlpEstimationPeriodButtons.SetColumn(btnInsert, 3);
                    grpEstimationPeriods.Controls.Clear();
                    SetLabelRowsCountValue(count: 0);
                    lstEstimationPeriodList.DataSource = null;
                    EstimationPeriodSelected();
                }
            }
            else
            {
                // Vybrané období má true v tabulce nfiesta.t_total_estimate_conf a nelze jej smazat
                // Selected period has got a true value in the nfiesta.t_total_estimate_conf table and it cannot be deleted
                MessageBox.Show(
                    text: msgSelectedEstimationPeriodCannotBeDeleted,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        #endregion Methods
    }

}