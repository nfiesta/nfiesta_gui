﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormEstimationPeriodNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            tlpNewPeriod = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlInsert = new System.Windows.Forms.Panel();
            btnInsert = new System.Windows.Forms.Button();
            grpNewPeriod = new System.Windows.Forms.GroupBox();
            tlpNewPeriodInside = new System.Windows.Forms.TableLayoutPanel();
            lblDate = new System.Windows.Forms.Label();
            pnlDateEnd = new System.Windows.Forms.Panel();
            txtLabel = new System.Windows.Forms.TextBox();
            lblLabel = new System.Windows.Forms.Label();
            lblDescription = new System.Windows.Forms.Label();
            lblLabelEn = new System.Windows.Forms.Label();
            lblDescriptionEn = new System.Windows.Forms.Label();
            lblDefaultInOlap = new System.Windows.Forms.Label();
            txtDescription = new System.Windows.Forms.TextBox();
            txtLabelEn = new System.Windows.Forms.TextBox();
            pnlRadioButtons = new System.Windows.Forms.Panel();
            rdoTrue = new System.Windows.Forms.RadioButton();
            rdoFalse = new System.Windows.Forms.RadioButton();
            txtDescriptionEn = new System.Windows.Forms.TextBox();
            pnlDate = new System.Windows.Forms.Panel();
            dtpTo = new System.Windows.Forms.DateTimePicker();
            dtpFrom = new System.Windows.Forms.DateTimePicker();
            lblFrom = new System.Windows.Forms.Label();
            lblTo = new System.Windows.Forms.Label();
            tipDefaultInOlap = new System.Windows.Forms.ToolTip(components);
            tlpNewPeriod.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlInsert.SuspendLayout();
            grpNewPeriod.SuspendLayout();
            tlpNewPeriodInside.SuspendLayout();
            pnlDateEnd.SuspendLayout();
            pnlRadioButtons.SuspendLayout();
            pnlDate.SuspendLayout();
            SuspendLayout();
            // 
            // tlpNewPeriod
            // 
            tlpNewPeriod.ColumnCount = 1;
            tlpNewPeriod.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpNewPeriod.Controls.Add(tlpButtons, 0, 1);
            tlpNewPeriod.Controls.Add(grpNewPeriod, 0, 0);
            tlpNewPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpNewPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpNewPeriod.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpNewPeriod.Location = new System.Drawing.Point(0, 0);
            tlpNewPeriod.Margin = new System.Windows.Forms.Padding(0);
            tlpNewPeriod.Name = "tlpNewPeriod";
            tlpNewPeriod.RowCount = 2;
            tlpNewPeriod.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpNewPeriod.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpNewPeriod.Size = new System.Drawing.Size(944, 501);
            tlpNewPeriod.TabIndex = 1;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlInsert, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 455);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 46);
            tlpButtons.TabIndex = 4;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(757, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(6);
            pnlCancel.Size = new System.Drawing.Size(187, 46);
            pnlCancel.TabIndex = 16;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(6, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 34);
            btnCancel.TabIndex = 15;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlInsert
            // 
            pnlInsert.Controls.Add(btnInsert);
            pnlInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInsert.Location = new System.Drawing.Point(570, 0);
            pnlInsert.Margin = new System.Windows.Forms.Padding(0);
            pnlInsert.Name = "pnlInsert";
            pnlInsert.Padding = new System.Windows.Forms.Padding(6);
            pnlInsert.Size = new System.Drawing.Size(187, 46);
            pnlInsert.TabIndex = 15;
            // 
            // btnInsert
            // 
            btnInsert.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            btnInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnInsert.Location = new System.Drawing.Point(6, 6);
            btnInsert.Margin = new System.Windows.Forms.Padding(0);
            btnInsert.Name = "btnInsert";
            btnInsert.Size = new System.Drawing.Size(175, 34);
            btnInsert.TabIndex = 12;
            btnInsert.Text = "btnInsert";
            btnInsert.UseVisualStyleBackColor = true;
            // 
            // grpNewPeriod
            // 
            grpNewPeriod.Controls.Add(tlpNewPeriodInside);
            grpNewPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            grpNewPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpNewPeriod.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpNewPeriod.Location = new System.Drawing.Point(4, 3);
            grpNewPeriod.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpNewPeriod.Name = "grpNewPeriod";
            grpNewPeriod.Padding = new System.Windows.Forms.Padding(6);
            grpNewPeriod.Size = new System.Drawing.Size(936, 449);
            grpNewPeriod.TabIndex = 3;
            grpNewPeriod.TabStop = false;
            // 
            // tlpNewPeriodInside
            // 
            tlpNewPeriodInside.ColumnCount = 2;
            tlpNewPeriodInside.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            tlpNewPeriodInside.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpNewPeriodInside.Controls.Add(lblDate, 0, 0);
            tlpNewPeriodInside.Controls.Add(pnlDateEnd, 1, 1);
            tlpNewPeriodInside.Controls.Add(lblLabel, 0, 1);
            tlpNewPeriodInside.Controls.Add(lblDescription, 0, 2);
            tlpNewPeriodInside.Controls.Add(lblLabelEn, 0, 3);
            tlpNewPeriodInside.Controls.Add(lblDescriptionEn, 0, 4);
            tlpNewPeriodInside.Controls.Add(lblDefaultInOlap, 0, 5);
            tlpNewPeriodInside.Controls.Add(txtDescription, 1, 2);
            tlpNewPeriodInside.Controls.Add(txtLabelEn, 1, 3);
            tlpNewPeriodInside.Controls.Add(pnlRadioButtons, 1, 5);
            tlpNewPeriodInside.Controls.Add(txtDescriptionEn, 1, 4);
            tlpNewPeriodInside.Controls.Add(pnlDate, 1, 0);
            tlpNewPeriodInside.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpNewPeriodInside.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpNewPeriodInside.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpNewPeriodInside.Location = new System.Drawing.Point(6, 21);
            tlpNewPeriodInside.Margin = new System.Windows.Forms.Padding(0);
            tlpNewPeriodInside.Name = "tlpNewPeriodInside";
            tlpNewPeriodInside.RowCount = 7;
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpNewPeriodInside.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpNewPeriodInside.Size = new System.Drawing.Size(924, 422);
            tlpNewPeriodInside.TabIndex = 5;
            // 
            // lblDate
            // 
            lblDate.AutoSize = true;
            lblDate.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDate.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDate.Location = new System.Drawing.Point(0, 0);
            lblDate.Margin = new System.Windows.Forms.Padding(0);
            lblDate.Name = "lblDate";
            lblDate.Size = new System.Drawing.Size(210, 25);
            lblDate.TabIndex = 1;
            lblDate.Text = "lblDate";
            // 
            // pnlDateEnd
            // 
            pnlDateEnd.Controls.Add(txtLabel);
            pnlDateEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDateEnd.Location = new System.Drawing.Point(210, 25);
            pnlDateEnd.Margin = new System.Windows.Forms.Padding(0);
            pnlDateEnd.Name = "pnlDateEnd";
            pnlDateEnd.Size = new System.Drawing.Size(714, 25);
            pnlDateEnd.TabIndex = 18;
            // 
            // txtLabel
            // 
            txtLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabel.Location = new System.Drawing.Point(0, 0);
            txtLabel.Margin = new System.Windows.Forms.Padding(0);
            txtLabel.Name = "txtLabel";
            txtLabel.Size = new System.Drawing.Size(714, 23);
            txtLabel.TabIndex = 14;
            // 
            // lblLabel
            // 
            lblLabel.AutoSize = true;
            lblLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            lblLabel.Location = new System.Drawing.Point(0, 25);
            lblLabel.Margin = new System.Windows.Forms.Padding(0);
            lblLabel.Name = "lblLabel";
            lblLabel.Size = new System.Drawing.Size(210, 25);
            lblLabel.TabIndex = 3;
            lblLabel.Text = "lblLabel";
            // 
            // lblDescription
            // 
            lblDescription.AutoSize = true;
            lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescription.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescription.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDescription.Location = new System.Drawing.Point(0, 50);
            lblDescription.Margin = new System.Windows.Forms.Padding(0);
            lblDescription.Name = "lblDescription";
            lblDescription.Size = new System.Drawing.Size(210, 25);
            lblDescription.TabIndex = 4;
            lblDescription.Text = "lblDescription";
            // 
            // lblLabelEn
            // 
            lblLabelEn.AutoSize = true;
            lblLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelEn.ForeColor = System.Drawing.SystemColors.ControlText;
            lblLabelEn.Location = new System.Drawing.Point(0, 75);
            lblLabelEn.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEn.Name = "lblLabelEn";
            lblLabelEn.Size = new System.Drawing.Size(210, 25);
            lblLabelEn.TabIndex = 5;
            lblLabelEn.Text = "lblLabelEn";
            // 
            // lblDescriptionEn
            // 
            lblDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionEn.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDescriptionEn.Location = new System.Drawing.Point(0, 100);
            lblDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEn.Name = "lblDescriptionEn";
            lblDescriptionEn.Size = new System.Drawing.Size(210, 25);
            lblDescriptionEn.TabIndex = 6;
            lblDescriptionEn.Text = "lblDescriptionEn";
            // 
            // lblDefaultInOlap
            // 
            lblDefaultInOlap.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDefaultInOlap.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDefaultInOlap.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDefaultInOlap.Location = new System.Drawing.Point(0, 125);
            lblDefaultInOlap.Margin = new System.Windows.Forms.Padding(0);
            lblDefaultInOlap.Name = "lblDefaultInOlap";
            lblDefaultInOlap.Size = new System.Drawing.Size(210, 25);
            lblDefaultInOlap.TabIndex = 19;
            lblDefaultInOlap.Text = "lblDefaultInOlap";
            // 
            // txtDescription
            // 
            txtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescription.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtDescription.Location = new System.Drawing.Point(210, 50);
            txtDescription.Margin = new System.Windows.Forms.Padding(0);
            txtDescription.Name = "txtDescription";
            txtDescription.Size = new System.Drawing.Size(714, 23);
            txtDescription.TabIndex = 11;
            // 
            // txtLabelEn
            // 
            txtLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelEn.Location = new System.Drawing.Point(210, 75);
            txtLabelEn.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEn.Name = "txtLabelEn";
            txtLabelEn.Size = new System.Drawing.Size(714, 23);
            txtLabelEn.TabIndex = 12;
            // 
            // pnlRadioButtons
            // 
            pnlRadioButtons.Controls.Add(rdoTrue);
            pnlRadioButtons.Controls.Add(rdoFalse);
            pnlRadioButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlRadioButtons.Location = new System.Drawing.Point(210, 125);
            pnlRadioButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlRadioButtons.Name = "pnlRadioButtons";
            pnlRadioButtons.Size = new System.Drawing.Size(714, 25);
            pnlRadioButtons.TabIndex = 20;
            // 
            // rdoTrue
            // 
            rdoTrue.AutoSize = true;
            rdoTrue.Location = new System.Drawing.Point(62, 5);
            rdoTrue.Name = "rdoTrue";
            rdoTrue.Size = new System.Drawing.Size(62, 17);
            rdoTrue.TabIndex = 1;
            rdoTrue.TabStop = true;
            rdoTrue.Text = "rdoTrue";
            rdoTrue.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            rdoTrue.UseVisualStyleBackColor = true;
            // 
            // rdoFalse
            // 
            rdoFalse.AutoSize = true;
            rdoFalse.Location = new System.Drawing.Point(0, 5);
            rdoFalse.Name = "rdoFalse";
            rdoFalse.Size = new System.Drawing.Size(65, 17);
            rdoFalse.TabIndex = 0;
            rdoFalse.TabStop = true;
            rdoFalse.Text = "rdoFalse";
            rdoFalse.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            rdoFalse.UseVisualStyleBackColor = true;
            // 
            // txtDescriptionEn
            // 
            txtDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtDescriptionEn.Location = new System.Drawing.Point(210, 100);
            txtDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEn.Name = "txtDescriptionEn";
            txtDescriptionEn.Size = new System.Drawing.Size(714, 23);
            txtDescriptionEn.TabIndex = 13;
            // 
            // pnlDate
            // 
            pnlDate.Controls.Add(dtpTo);
            pnlDate.Controls.Add(dtpFrom);
            pnlDate.Controls.Add(lblFrom);
            pnlDate.Controls.Add(lblTo);
            pnlDate.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDate.Location = new System.Drawing.Point(210, 0);
            pnlDate.Margin = new System.Windows.Forms.Padding(0);
            pnlDate.Name = "pnlDate";
            pnlDate.Size = new System.Drawing.Size(714, 25);
            pnlDate.TabIndex = 21;
            // 
            // dtpTo
            // 
            dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            dtpTo.Location = new System.Drawing.Point(256, 3);
            dtpTo.Name = "dtpTo";
            dtpTo.Size = new System.Drawing.Size(200, 20);
            dtpTo.TabIndex = 1;
            // 
            // dtpFrom
            // 
            dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            dtpFrom.Location = new System.Drawing.Point(28, 3);
            dtpFrom.Name = "dtpFrom";
            dtpFrom.Size = new System.Drawing.Size(200, 20);
            dtpFrom.TabIndex = 0;
            // 
            // lblFrom
            // 
            lblFrom.AutoSize = true;
            lblFrom.Location = new System.Drawing.Point(0, 8);
            lblFrom.Name = "lblFrom";
            lblFrom.Size = new System.Drawing.Size(40, 13);
            lblFrom.TabIndex = 2;
            lblFrom.Text = "lblFrom";
            // 
            // lblTo
            // 
            lblTo.AutoSize = true;
            lblTo.Location = new System.Drawing.Point(234, 8);
            lblTo.Name = "lblTo";
            lblTo.Size = new System.Drawing.Size(30, 13);
            lblTo.TabIndex = 3;
            lblTo.Text = "lblTo";
            // 
            // FormEstimationPeriodNew
            // 
            AcceptButton = btnInsert;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpNewPeriod);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormEstimationPeriodNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormEstimationPeriodNew";
            tlpNewPeriod.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlInsert.ResumeLayout(false);
            grpNewPeriod.ResumeLayout(false);
            tlpNewPeriodInside.ResumeLayout(false);
            tlpNewPeriodInside.PerformLayout();
            pnlDateEnd.ResumeLayout(false);
            pnlDateEnd.PerformLayout();
            pnlRadioButtons.ResumeLayout(false);
            pnlRadioButtons.PerformLayout();
            pnlDate.ResumeLayout(false);
            pnlDate.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpNewPeriod;
        private System.Windows.Forms.GroupBox grpNewPeriod;
        private System.Windows.Forms.TableLayoutPanel tlpNewPeriodInside;
        private System.Windows.Forms.TextBox txtDescriptionEn;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.TextBox txtLabelEn;
        private System.Windows.Forms.Label lblLabel;
        private System.Windows.Forms.Label lblDescriptionEn;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblLabelEn;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtLabel;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlInsert;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Panel pnlDateEnd;
        private System.Windows.Forms.Label lblDefaultInOlap;
        private System.Windows.Forms.Panel pnlRadioButtons;
        private System.Windows.Forms.RadioButton rdoTrue;
        private System.Windows.Forms.RadioButton rdoFalse;
        private System.Windows.Forms.ToolTip tipDefaultInOlap;
        private System.Windows.Forms.Panel pnlDate;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
    }

}