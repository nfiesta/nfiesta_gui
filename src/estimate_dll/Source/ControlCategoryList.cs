﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení seznamu atributových kategorií - třída pro Designer</para>
    /// <para lang="en">Control for display list of attribute categories - class for Designer</para>
    /// </summary>
    internal partial class ControlCategoryListDesign
        : UserControl
    {
        protected ControlCategoryListDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.GroupBox GrpCategory => grpCategory;
        protected System.Windows.Forms.Label LblCount => lblCount;
        protected System.Windows.Forms.Panel PnlCategory => pnlCategory;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení seznamu atributových kategorií</para>
    /// <para lang="en">Control for display list of attribute categories</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal class ControlCategoryList<TCategory>
            : ControlCategoryListDesign, INfiEstaControl, IEstimateControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Výška položky v seznamu</para>
        /// <para lang="en">Item height</para>
        /// </summary>
        private const int ItemHeight = 20;

        /// <summary>
        /// <para lang="cs">Odsazení položky od levého okraje panelu</para>
        /// <para lang="en">Item offset from left panel margin</para>
        /// </summary>
        private const int ItemOffset = 5;

        /// <summary>
        /// <para lang="cs">Předpokládaná šířka scrollbaru v případě, že se položky v seznamu nezobrazují všechny</para>
        /// <para lang="en">Estimated width of the scrollbar if not all items in the list are displayed</para>
        /// </summary>
        private const int ScrollBarWidth = 30;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Seznam kategorií plošné domény</para>
        /// <para lang="en">List of area domain categories</para>
        /// </summary>
        private AreaDomainCategoryList areaDomainCategories;

        /// <summary>
        /// <para lang="cs">Seznam kategorií subpopulace</para>
        /// <para lang="en">List of subpopulation categories</para>
        /// </summary>
        private SubPopulationCategoryList subPopulationCategories;

        /// <summary>
        /// <para lang="cs">Seznam atributových kategorií</para>
        /// <para lang="en">List of attribute categories</para>
        /// </summary>
        private VariablePairList attributeCategories;

        private string strRowsCountNone = String.Empty;
        private string strRowsCountOne = String.Empty;
        private string strRowsCountSome = String.Empty;
        private string strRowsCountMany = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlCategoryList(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ atributové kategorie (read-only)</para>
        /// <para lang="en">Attribute category type (read-only)</para>
        /// </summary>
        public static CategoryType CategoryType
        {
            get
            {
                return typeof(TCategory).FullName switch
                {
                    "ZaJi.NfiEstaPg.Core.AreaDomainCategory" =>
                        CategoryType.AreaDomainCategory,

                    "ZaJi.NfiEstaPg.Core.SubPopulationCategory" =>
                        CategoryType.SubPopulationCategory,

                    "ZaJi.NfiEstaPg.VariablePair" =>
                        CategoryType.VariablePair,

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(Items)} must be type ",
                                $"of {nameof(List<AreaDomainCategory>)}<{nameof(AreaDomainCategory)}> ",
                                $"or {nameof(List<SubPopulationCategory>)}<{nameof(SubPopulationCategory)}> ",
                                $"or {nameof(List<VariablePair>)}<{nameof(VariablePair)}> "),
                            paramName: nameof(Items)),
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam kategorií plošné domény</para>
        /// <para lang="en">List of area domain categories</para>
        /// </summary>
        public AreaDomainCategoryList AreaDomainCategories
        {
            get
            {
                if (CategoryType == CategoryType.AreaDomainCategory)
                {
                    return areaDomainCategories ??
                        new AreaDomainCategoryList(database: Database);
                }
                else
                {
                    throw new ArgumentException(
                        message: $"Items in {nameof(AreaDomainCategories)} must be type of {nameof(AreaDomainCategory)}.",
                        paramName: nameof(AreaDomainCategories));
                }
            }
            set
            {
                if (CategoryType == CategoryType.AreaDomainCategory)
                {
                    areaDomainCategories = value ??
                        new AreaDomainCategoryList(database: Database);
                    CreateItems();
                    InitializeLabels();
                }
                else
                {
                    throw new ArgumentException(
                        message: $"Items in {nameof(AreaDomainCategories)} must be type of {nameof(AreaDomainCategory)}.",
                        paramName: nameof(AreaDomainCategories));
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam kategorií subpopulace</para>
        /// <para lang="en">List of subpopulation categories</para>
        /// </summary>
        public SubPopulationCategoryList SubPopulationCategories
        {
            get
            {
                if (CategoryType == CategoryType.SubPopulationCategory)
                {
                    return subPopulationCategories ??
                        new SubPopulationCategoryList(database: Database);
                }
                else
                {
                    throw new ArgumentException(
                        message: $"Items in {nameof(SubPopulationCategories)} must be type of {nameof(SubPopulationCategory)}.",
                        paramName: nameof(SubPopulationCategories));
                }
            }
            set
            {
                if (CategoryType == CategoryType.SubPopulationCategory)
                {
                    subPopulationCategories = value ??
                        new SubPopulationCategoryList(database: Database);
                    CreateItems();
                    InitializeLabels();
                }
                else
                {
                    throw new ArgumentException(
                        message: $"Items in {nameof(SubPopulationCategories)} must be type of {nameof(SubPopulationCategory)}.",
                        paramName: nameof(SubPopulationCategories));
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam atributových kategorií</para>
        /// <para lang="en">List of attribute categories</para>
        /// </summary>
        public VariablePairList AttributeCategories
        {
            get
            {
                if (CategoryType == CategoryType.VariablePair)
                {
                    return attributeCategories ??
                        new VariablePairList(database: Database);
                }
                else
                {
                    throw new ArgumentException(
                        message: $"Items in {nameof(AttributeCategories)} must be type of {nameof(VariablePair)}.",
                        paramName: nameof(AttributeCategories));
                }
            }
            set
            {
                if (CategoryType == CategoryType.VariablePair)
                {
                    attributeCategories = value ??
                        new VariablePairList(database: Database);
                    CreateItems();
                    InitializeLabels();
                }
                else
                {
                    throw new ArgumentException(
                        message: $"Items in {nameof(AttributeCategories)} must be type of {nameof(VariablePair)}.",
                        paramName: nameof(AttributeCategories));
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Položky seznamu atributových kategorií</para>
        /// <para lang="en">Items of the attribute categories list</para>
        /// </summary>
        private List<TCategory> Items
        {
            get
            {
                return CategoryType switch
                {
                    CategoryType.AreaDomainCategory => (List<TCategory>)
                        Convert.ChangeType(
                            value: AreaDomainCategories.Items,
                            conversionType: typeof(List<TCategory>)),

                    CategoryType.SubPopulationCategory => (List<TCategory>)
                        Convert.ChangeType(
                            value: SubPopulationCategories.Items,
                            conversionType: typeof(List<TCategory>)),

                    CategoryType.VariablePair => (List<TCategory>)
                        Convert.ChangeType(
                            value: AttributeCategories.VariablePairs,
                            conversionType: typeof(List<TCategory>)),

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(Items)} must be type ",
                                $"of {nameof(List<AreaDomainCategory>)}<{nameof(AreaDomainCategory)}> ",
                                $"or {nameof(List<SubPopulationCategory>)}<{nameof(SubPopulationCategory)}> ",
                                $"or {nameof(List<VariablePair>)}<{nameof(VariablePair)}> "),
                            paramName: nameof(Items)),
                };
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => CategoryType switch
                {
                    CategoryType.AreaDomainCategory => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategory),                  "Kategorie plošné domény:" },
                            { nameof(strRowsCountNone),             "Není vybrána žádná kategorie plošné domény." },
                            { nameof(strRowsCountOne),              "Byla vybrána $1 kategorie plošné domény." },
                            { nameof(strRowsCountSome),             "Byly vybrány $1 kategorie plošné domény." },
                            { nameof(strRowsCountMany),             "Bylo vybráno $1 kategorií plošné domény." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryList<AreaDomainCategory>)}{nameof(AreaDomainCategory)}",
                            out Dictionary<string, string> dictNationalAreaDomainCategory)
                                ? dictNationalAreaDomainCategory
                                : [],

                    CategoryType.SubPopulationCategory => (languageFile == null) ?
                        new Dictionary<string, string>()
                        {
                            { nameof(GrpCategory),                  "Kategorie subpopulace:" },
                            { nameof(strRowsCountNone),             "Není vybrána žádná kategorie subpopulace." },
                            { nameof(strRowsCountOne),              "Byla vybrána $1 kategorie subpopulace." },
                            { nameof(strRowsCountSome),             "Byly vybrány $1 kategorie subpopulace." },
                            { nameof(strRowsCountMany),             "Bylo vybráno $1 kategorií subpopulace." }
                        } :
                        languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryList<SubPopulationCategory>)}{nameof(SubPopulationCategory)}",
                            out Dictionary<string, string> dictNationalSubPopulationCategory)
                                ? dictNationalSubPopulationCategory
                                : [],

                    CategoryType.VariablePair => (languageFile == null) ?
                        new Dictionary<string, string>()
                        {
                            { nameof(GrpCategory),                  "Atributové kategorie:" },
                            { nameof(strRowsCountNone),             "Není vybrána žádná atributová kategorie." },
                            { nameof(strRowsCountOne),              "Byla vybrána $1 atributová kategorie." },
                            { nameof(strRowsCountSome),             "Byly vybrány $1 atributové kategorie." },
                            { nameof(strRowsCountMany),             "Bylo vybráno $1 atributových kategorií." }
                        } :
                        languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryList<VariablePair>)}{nameof(VariablePair)}",
                            out Dictionary<string, string> dictNationalVariablePair)
                                ? dictNationalVariablePair
                                : [],

                    _ => [],
                },

                LanguageVersion.International => CategoryType switch
                {
                    CategoryType.AreaDomainCategory => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategory),                  "Area domain categories:" },
                            { nameof(strRowsCountNone),             "No area domain category was selected." },
                            { nameof(strRowsCountOne),              "$1 area domain category was selected." },
                            { nameof(strRowsCountSome),             "$1 area domain categories were selected."  },
                            { nameof(strRowsCountMany),             "$1 area domain categories were selected."  }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                           key: $"{nameof(ControlCategoryList<AreaDomainCategory>)}{nameof(AreaDomainCategory)}",
                            out Dictionary<string, string> dictInternationalAreaDomainCategory)
                                ? dictInternationalAreaDomainCategory
                                : [],

                    CategoryType.SubPopulationCategory => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategory),                  "Subpopulation categories:" },
                            { nameof(strRowsCountNone),             "No subpopulation category was selected." },
                            { nameof(strRowsCountOne),              "$1 subpopulation category was selected." },
                            { nameof(strRowsCountSome),             "$1 subpopulation categories were selected."  },
                            { nameof(strRowsCountMany),             "$1 subpopulation categories were selected."  }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryList<SubPopulationCategory>)}{nameof(SubPopulationCategory)}",
                            out Dictionary<string, string> dictInternationalSubPopulationCategory)
                                ? dictInternationalSubPopulationCategory
                                : [],

                    CategoryType.VariablePair => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategory),                  "Attribute categories:" },
                            { nameof(strRowsCountNone),             "No attribute category was selected." },
                            { nameof(strRowsCountOne),              "$1 attribute category was selected." },
                            { nameof(strRowsCountSome),             "$1 attribute categories were selected."  },
                            { nameof(strRowsCountMany),             "$1 attribute categories were selected."  }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlCategoryList<VariablePair>)}{nameof(VariablePair)}",
                            out Dictionary<string, string> dictInternationalVariablePair)
                                ? dictInternationalVariablePair
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            Resize += (sender, e) => { ResizeItems(); };
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            GrpCategory.Text =
                labels.TryGetValue(key: nameof(GrpCategory),
                out string grpCategoryText)
                    ? grpCategoryText
                    : String.Empty;

            foreach (ControlCategoryItem<TCategory> item in PnlCategory.Controls)
            {
                item.InitializeLabels();
            }

            DisplayItemsCount();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Odstranění všech položek ze seznamu</para>
        /// <para lang="en">Removes all items from list</para>
        /// </summary>
        public void Clear()
        {
            areaDomainCategories = null;
            subPopulationCategories = null;
            attributeCategories = null;

            PnlCategory.Controls.Clear();
            DisplayItemsCount();
        }

        /// <summary>
        /// <para lang="cs">Vytvoření položek seznamu</para>
        /// <para lang="en">Creates list items</para>
        /// </summary>
        private void CreateItems()
        {
            PnlCategory.Controls.Clear();

            foreach (TCategory item in Items)
            {
                PnlCategory.Controls.Add(
                    value: new ControlCategoryItem<TCategory>(
                        controlOwner: this,
                        item: item));
            }

            ResizeItems();
            DisplayItemsCount();
        }

        /// <summary>
        /// <para lang="cs">Nastaví velikost a umístění položek v seznamu</para>
        /// <para lang="en">Method sets size and position of the items in list</para>
        /// </summary>
        private void ResizeItems()
        {
            for (int i = 0; i < PnlCategory.Controls.Count; i++)
            {
                PnlCategory.Controls[i].Height = ItemHeight;
                PnlCategory.Controls[i].Left = ItemOffset;
                PnlCategory.Controls[i].Top = i * ItemHeight;
                PnlCategory.Controls[i].Width = PnlCategory.Width - ItemOffset - ScrollBarWidth;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí informaci o počtu kategorií</para>
        /// <para lang="en">Method displays information about count of categories</para>
        /// </summary>
        private void DisplayItemsCount()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            switch (PnlCategory.Controls.Count)
            {
                case 0:
                    LblCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountNone), out strRowsCountNone)
                            ? strRowsCountNone
                            : String.Empty)
                        .Replace(oldValue: "$1", newValue: PnlCategory.Controls.Count.ToString());
                    return;

                case 1:
                    LblCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountOne), out strRowsCountOne)
                            ? strRowsCountOne
                            : String.Empty)
                        .Replace(oldValue: "$1", newValue: PnlCategory.Controls.Count.ToString());
                    return;

                case 2:
                case 3:
                case 4:
                    LblCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountSome), out strRowsCountSome)
                            ? strRowsCountSome
                            : String.Empty)
                        .Replace(oldValue: "$1", newValue: PnlCategory.Controls.Count.ToString());
                    return;

                default:
                    LblCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountMany), out strRowsCountMany)
                            ? strRowsCountMany
                            : String.Empty)
                        .Replace(oldValue: "$1", newValue: PnlCategory.Controls.Count.ToString());
                    return;
            }
        }

        #endregion Methods

    }

}
