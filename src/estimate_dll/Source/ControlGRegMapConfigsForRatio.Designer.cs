﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlGRegMapConfigsForRatio
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpData = new System.Windows.Forms.TableLayoutPanel();
            lblDenominatorModelSigmaValue = new System.Windows.Forms.Label();
            lblDenominatorModelSigmaCaption = new System.Windows.Forms.Label();
            lblDenominatorModelValue = new System.Windows.Forms.Label();
            lblDenominatorModelCaption = new System.Windows.Forms.Label();
            lblForceSyntheticValue = new System.Windows.Forms.Label();
            lblForceSyntheticCaption = new System.Windows.Forms.Label();
            lblNumeratorModelSigmaValue = new System.Windows.Forms.Label();
            lblNumeratorModelSigmaCaption = new System.Windows.Forms.Label();
            lblParamAreaTypeValue = new System.Windows.Forms.Label();
            lblParamAreaTypeCaption = new System.Windows.Forms.Label();
            lblNumeratorModelValue = new System.Windows.Forms.Label();
            lblNumeratorModelCaption = new System.Windows.Forms.Label();
            lblPanelRefYearSetGroupValue = new System.Windows.Forms.Label();
            lblPanelRefYearSetGroupCaption = new System.Windows.Forms.Label();
            rdoSelected = new System.Windows.Forms.RadioButton();
            lblAllEstimatesConfigurableCaption = new System.Windows.Forms.Label();
            lblAllEstimatesConfigurableValue = new System.Windows.Forms.Label();
            tlpMain.SuspendLayout();
            tlpData.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.WhiteSmoke;
            tlpMain.ColumnCount = 2;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpData, 1, 0);
            tlpMain.Controls.Add(rdoSelected, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(5, 5);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 1;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Size = new System.Drawing.Size(950, 80);
            tlpMain.TabIndex = 1;
            // 
            // tlpData
            // 
            tlpData.ColumnCount = 6;
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            tlpData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpData.Controls.Add(lblDenominatorModelSigmaValue, 4, 3);
            tlpData.Controls.Add(lblDenominatorModelSigmaCaption, 3, 3);
            tlpData.Controls.Add(lblDenominatorModelValue, 4, 2);
            tlpData.Controls.Add(lblDenominatorModelCaption, 3, 2);
            tlpData.Controls.Add(lblForceSyntheticValue, 1, 2);
            tlpData.Controls.Add(lblForceSyntheticCaption, 0, 2);
            tlpData.Controls.Add(lblNumeratorModelSigmaValue, 4, 1);
            tlpData.Controls.Add(lblNumeratorModelSigmaCaption, 3, 1);
            tlpData.Controls.Add(lblNumeratorModelValue, 4, 0);
            tlpData.Controls.Add(lblNumeratorModelCaption, 3, 0);
            tlpData.Controls.Add(lblAllEstimatesConfigurableCaption, 0, 0);
            tlpData.Controls.Add(lblAllEstimatesConfigurableValue, 1, 0);
            tlpData.Controls.Add(lblParamAreaTypeCaption, 0, 3);
            tlpData.Controls.Add(lblPanelRefYearSetGroupCaption, 0, 1);
            tlpData.Controls.Add(lblPanelRefYearSetGroupValue, 1, 1);
            tlpData.Controls.Add(lblParamAreaTypeValue, 1, 3);
            tlpData.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpData.Location = new System.Drawing.Point(40, 0);
            tlpData.Margin = new System.Windows.Forms.Padding(0);
            tlpData.Name = "tlpData";
            tlpData.RowCount = 5;
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpData.Size = new System.Drawing.Size(910, 80);
            tlpData.TabIndex = 1;
            // 
            // lblDenominatorModelSigmaValue
            // 
            lblDenominatorModelSigmaValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDenominatorModelSigmaValue.Location = new System.Drawing.Point(660, 60);
            lblDenominatorModelSigmaValue.Margin = new System.Windows.Forms.Padding(0);
            lblDenominatorModelSigmaValue.Name = "lblDenominatorModelSigmaValue";
            lblDenominatorModelSigmaValue.Size = new System.Drawing.Size(250, 20);
            lblDenominatorModelSigmaValue.TabIndex = 22;
            lblDenominatorModelSigmaValue.Text = "lblDenominatorModelSigmaValue";
            lblDenominatorModelSigmaValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorModelSigmaCaption
            // 
            lblDenominatorModelSigmaCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDenominatorModelSigmaCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDenominatorModelSigmaCaption.Location = new System.Drawing.Point(460, 60);
            lblDenominatorModelSigmaCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDenominatorModelSigmaCaption.Name = "lblDenominatorModelSigmaCaption";
            lblDenominatorModelSigmaCaption.Size = new System.Drawing.Size(200, 20);
            lblDenominatorModelSigmaCaption.TabIndex = 21;
            lblDenominatorModelSigmaCaption.Text = "lblDenominatorModelSigmaCaption";
            lblDenominatorModelSigmaCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorModelValue
            // 
            lblDenominatorModelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDenominatorModelValue.Location = new System.Drawing.Point(660, 40);
            lblDenominatorModelValue.Margin = new System.Windows.Forms.Padding(0);
            lblDenominatorModelValue.Name = "lblDenominatorModelValue";
            lblDenominatorModelValue.Size = new System.Drawing.Size(250, 20);
            lblDenominatorModelValue.TabIndex = 16;
            lblDenominatorModelValue.Text = "lblDenominatorModelValue";
            lblDenominatorModelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorModelCaption
            // 
            lblDenominatorModelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDenominatorModelCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDenominatorModelCaption.Location = new System.Drawing.Point(460, 40);
            lblDenominatorModelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDenominatorModelCaption.Name = "lblDenominatorModelCaption";
            lblDenominatorModelCaption.Size = new System.Drawing.Size(200, 20);
            lblDenominatorModelCaption.TabIndex = 15;
            lblDenominatorModelCaption.Text = "lblDenominatorModelCaption";
            lblDenominatorModelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblForceSyntheticValue
            // 
            lblForceSyntheticValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblForceSyntheticValue.Location = new System.Drawing.Point(200, 40);
            lblForceSyntheticValue.Margin = new System.Windows.Forms.Padding(0);
            lblForceSyntheticValue.Name = "lblForceSyntheticValue";
            lblForceSyntheticValue.Size = new System.Drawing.Size(250, 20);
            lblForceSyntheticValue.TabIndex = 13;
            lblForceSyntheticValue.Text = "lblForceSyntheticValue";
            lblForceSyntheticValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblForceSyntheticCaption
            // 
            lblForceSyntheticCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblForceSyntheticCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblForceSyntheticCaption.Location = new System.Drawing.Point(0, 40);
            lblForceSyntheticCaption.Margin = new System.Windows.Forms.Padding(0);
            lblForceSyntheticCaption.Name = "lblForceSyntheticCaption";
            lblForceSyntheticCaption.Size = new System.Drawing.Size(200, 20);
            lblForceSyntheticCaption.TabIndex = 12;
            lblForceSyntheticCaption.Text = "lblForceSyntheticCaption";
            lblForceSyntheticCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelSigmaValue
            // 
            lblNumeratorModelSigmaValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelSigmaValue.Location = new System.Drawing.Point(660, 20);
            lblNumeratorModelSigmaValue.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelSigmaValue.Name = "lblNumeratorModelSigmaValue";
            lblNumeratorModelSigmaValue.Size = new System.Drawing.Size(250, 20);
            lblNumeratorModelSigmaValue.TabIndex = 10;
            lblNumeratorModelSigmaValue.Text = "lblNumeratorModelSigmaValue";
            lblNumeratorModelSigmaValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelSigmaCaption
            // 
            lblNumeratorModelSigmaCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelSigmaCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblNumeratorModelSigmaCaption.Location = new System.Drawing.Point(460, 20);
            lblNumeratorModelSigmaCaption.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelSigmaCaption.Name = "lblNumeratorModelSigmaCaption";
            lblNumeratorModelSigmaCaption.Size = new System.Drawing.Size(200, 20);
            lblNumeratorModelSigmaCaption.TabIndex = 9;
            lblNumeratorModelSigmaCaption.Text = "lblNumeratorModelSigmaCaption";
            lblNumeratorModelSigmaCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblParamAreaTypeValue
            // 
            lblParamAreaTypeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblParamAreaTypeValue.Location = new System.Drawing.Point(200, 60);
            lblParamAreaTypeValue.Margin = new System.Windows.Forms.Padding(0);
            lblParamAreaTypeValue.Name = "lblParamAreaTypeValue";
            lblParamAreaTypeValue.Size = new System.Drawing.Size(250, 20);
            lblParamAreaTypeValue.TabIndex = 7;
            lblParamAreaTypeValue.Text = "lblParamAreaTypeValue";
            lblParamAreaTypeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblParamAreaTypeCaption
            // 
            lblParamAreaTypeCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblParamAreaTypeCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblParamAreaTypeCaption.Location = new System.Drawing.Point(0, 60);
            lblParamAreaTypeCaption.Margin = new System.Windows.Forms.Padding(0);
            lblParamAreaTypeCaption.Name = "lblParamAreaTypeCaption";
            lblParamAreaTypeCaption.Size = new System.Drawing.Size(200, 20);
            lblParamAreaTypeCaption.TabIndex = 6;
            lblParamAreaTypeCaption.Text = "lblParamAreaTypeCaption";
            lblParamAreaTypeCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelValue
            // 
            lblNumeratorModelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelValue.Location = new System.Drawing.Point(660, 0);
            lblNumeratorModelValue.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelValue.Name = "lblNumeratorModelValue";
            lblNumeratorModelValue.Size = new System.Drawing.Size(250, 20);
            lblNumeratorModelValue.TabIndex = 4;
            lblNumeratorModelValue.Text = "lblNumeratorModelValue";
            lblNumeratorModelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumeratorModelCaption
            // 
            lblNumeratorModelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNumeratorModelCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblNumeratorModelCaption.Location = new System.Drawing.Point(460, 0);
            lblNumeratorModelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblNumeratorModelCaption.Name = "lblNumeratorModelCaption";
            lblNumeratorModelCaption.Size = new System.Drawing.Size(200, 20);
            lblNumeratorModelCaption.TabIndex = 3;
            lblNumeratorModelCaption.Text = "lblNumeratorModelCaption";
            lblNumeratorModelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupValue
            // 
            lblPanelRefYearSetGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelRefYearSetGroupValue.Location = new System.Drawing.Point(200, 20);
            lblPanelRefYearSetGroupValue.Margin = new System.Windows.Forms.Padding(0);
            lblPanelRefYearSetGroupValue.Name = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.Size = new System.Drawing.Size(250, 20);
            lblPanelRefYearSetGroupValue.TabIndex = 1;
            lblPanelRefYearSetGroupValue.Text = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupCaption
            // 
            lblPanelRefYearSetGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPanelRefYearSetGroupCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblPanelRefYearSetGroupCaption.Location = new System.Drawing.Point(0, 20);
            lblPanelRefYearSetGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            lblPanelRefYearSetGroupCaption.Name = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.Size = new System.Drawing.Size(200, 20);
            lblPanelRefYearSetGroupCaption.TabIndex = 0;
            lblPanelRefYearSetGroupCaption.Text = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoSelected
            // 
            rdoSelected.AutoSize = true;
            rdoSelected.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            rdoSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            rdoSelected.Location = new System.Drawing.Point(0, 0);
            rdoSelected.Margin = new System.Windows.Forms.Padding(0);
            rdoSelected.Name = "rdoSelected";
            rdoSelected.Size = new System.Drawing.Size(40, 80);
            rdoSelected.TabIndex = 0;
            rdoSelected.TabStop = true;
            rdoSelected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            rdoSelected.UseVisualStyleBackColor = true;
            // 
            // lblAllEstimatesConfigurableCaption
            // 
            lblAllEstimatesConfigurableCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAllEstimatesConfigurableCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAllEstimatesConfigurableCaption.Location = new System.Drawing.Point(0, 0);
            lblAllEstimatesConfigurableCaption.Margin = new System.Windows.Forms.Padding(0);
            lblAllEstimatesConfigurableCaption.Name = "lblAllEstimatesConfigurableCaption";
            lblAllEstimatesConfigurableCaption.Size = new System.Drawing.Size(200, 20);
            lblAllEstimatesConfigurableCaption.TabIndex = 23;
            lblAllEstimatesConfigurableCaption.Text = "lblAllEstimatesConfigurableCaption";
            lblAllEstimatesConfigurableCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAllEstimatesConfigurableValue
            // 
            lblAllEstimatesConfigurableValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAllEstimatesConfigurableValue.Location = new System.Drawing.Point(200, 0);
            lblAllEstimatesConfigurableValue.Margin = new System.Windows.Forms.Padding(0);
            lblAllEstimatesConfigurableValue.Name = "lblAllEstimatesConfigurableValue";
            lblAllEstimatesConfigurableValue.Size = new System.Drawing.Size(250, 20);
            lblAllEstimatesConfigurableValue.TabIndex = 24;
            lblAllEstimatesConfigurableValue.Text = "lblAllEstimatesConfigurableValue";
            lblAllEstimatesConfigurableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlGRegMapConfigsForRatio
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Silver;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlGRegMapConfigsForRatio";
            Padding = new System.Windows.Forms.Padding(5);
            Size = new System.Drawing.Size(960, 90);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            tlpData.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpData;
        private System.Windows.Forms.RadioButton rdoSelected;
        private System.Windows.Forms.Label lblDenominatorModelSigmaValue;
        private System.Windows.Forms.Label lblDenominatorModelSigmaCaption;
        private System.Windows.Forms.Label lblDenominatorModelValue;
        private System.Windows.Forms.Label lblDenominatorModelCaption;
        private System.Windows.Forms.Label lblForceSyntheticValue;
        private System.Windows.Forms.Label lblForceSyntheticCaption;
        private System.Windows.Forms.Label lblNumeratorModelSigmaValue;
        private System.Windows.Forms.Label lblNumeratorModelSigmaCaption;
        private System.Windows.Forms.Label lblParamAreaTypeValue;
        private System.Windows.Forms.Label lblParamAreaTypeCaption;
        private System.Windows.Forms.Label lblNumeratorModelValue;
        private System.Windows.Forms.Label lblNumeratorModelCaption;
        private System.Windows.Forms.Label lblPanelRefYearSetGroupValue;
        private System.Windows.Forms.Label lblPanelRefYearSetGroupCaption;
        private System.Windows.Forms.Label lblAllEstimatesConfigurableCaption;
        private System.Windows.Forms.Label lblAllEstimatesConfigurableValue;
    }

}