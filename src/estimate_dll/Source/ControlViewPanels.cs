﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Zobrazení panelů pro vybraný odhad"</para>
    /// <para lang="en">Control "Display panels for selected estimate"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlViewPanels
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Seznam párů panelů s odpovídajícím referenčním obdobím</para>
        /// <para lang="en">List of pairs of panels with corresponding reference year set</para>
        /// </summary>
        private VwPanelRefYearSetPairList dataSource;

        private string strRowsCountNone = String.Empty;
        private string strRowsCountOne = String.Empty;
        private string strRowsCountSome = String.Empty;
        private string strRowsCountMany = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">DataGridView pro zobrazení panelů pro vybraný odhad</para>
        /// <para lang="en">DataGridView for display panels for selected estimate</para>
        /// </summary>
        private DataGridView dgvPanels;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Zavřít zobrazení panelů pro vybraný odhad"</para>
        /// <para lang="en">Event
        /// "Close the display of panels for selected estimate"</para>
        /// </summary>
        public event EventHandler Closed;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlViewPanels(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Seznam párů panelů s odpovídajícím referenčním obdobím</para>
        /// <para lang="en">List of pairs of panels with corresponding reference year set</para>
        /// </summary>
        public VwPanelRefYearSetPairList DataSource
        {
            get
            {
                return
                    dataSource ??
                    new VwPanelRefYearSetPairList(database: Database);
            }
            set
            {
                dataSource = value ??
                    new VwPanelRefYearSetPairList(database: Database);
                InitializeDataGridView();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazené sloupce</para>
        /// <para lang="en">Displayed columns</para>
        /// </summary>
        public string[] VisibleColumns
        {
            get
            {
                return
                    Setting.ViewPanelsVisibleColumns;
            }
            set
            {
                Setting.ViewPanelsVisibleColumns = value;
                VwPanelRefYearSetPairList.SetColumnsVisibility(
                    columnNames: Setting.ViewPanelsVisibleColumns);
                InitializeDataGridView();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Podmínka pro výběr záznamů v DataGridView
        /// </para>
        /// <para lang="en">
        /// Condition for selecting records in DataGridView
        /// </para>
        /// </summary>
        public string RowFilter
        {
            get
            {
                return DataSource.Data.DefaultView.RowFilter;
            }
            set
            {
                DataSource.RowFilter = value;

                SetLabelRowsCountValue(count: dgvPanels.Rows.Count);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazit tlačítko pro výběr sloupců tabulky?</para>
        /// <para lang="en">Show button for selecting table columns?</para>
        /// </summary>
        public bool DisplayButtonFilterColumns
        {
            get
            {
                return btnFilterColumns.Visible;
            }
            set
            {
                btnFilterColumns.Visible = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazit tlačítko pro zavření ovládacího prvku?</para>
        /// <para lang="en">Show button to close the control?</para>
        /// </summary>
        public bool DisplayButtonClose
        {
            get
            {
                return btnClose.Visible;
            }
            set
            {
                btnClose.Visible = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnFilterColumns),                  "Výběr sloupců tabulky panelů a roků měření" },
                        { nameof(btnClose),                          "Zavřít" },
                        { nameof(grpData),                           "Skupina panelů:" },
                        { nameof(strRowsCountNone),                  "Není vybrán žádný panel." },
                        { nameof(strRowsCountOne),                   "Je vybrán $1 panel." },
                        { nameof(strRowsCountSome),                  "Jsou vybrány $1 panely." },
                        { nameof(strRowsCountMany),                  "Je vybráno $1 panelů." },
                        { nameof(tsrTools),                          String.Empty },

                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColId.Name}",
                            "Identifikační číslo panelu s odpovídající sezónou měření" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColId.Name}",
                            "Identifikační číslo panelu s odpovídající sezónou měření" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name}",
                            "Identifikační číslo skupiny panelů s odpovídajícími sezónami měření" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name}",
                            "Identifikační číslo skupiny panelů s odpovídajícími sezónami měření" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelId.Name}",
                            "Identifikační číslo panelu" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelId.Name}",
                            "Identifikační číslo panelu" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelName.Name}",
                            "Jméno panelu" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelName.Name}",
                            "Jméno panelu" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelLabel.Name}",
                            "Popisek panelu" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelLabel.Name}",
                            "Popisek panelu" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelClusterCount.Name}",
                            "Celkový počet klastrů v panelu" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelClusterCount.Name}",
                            "Celkový počet klastrů v panelu" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelPlotCount.Name}",
                            "Celkový počet inventarizačních ploch v panelu" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelPlotCount.Name}",
                            "Celkový počet inventarizačních ploch v panelu" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetId.Name}",
                            "Identifikační číslo roku nebo sezóny měření" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetId.Name}",
                            "Identifikační číslo roku nebo sezóny měření" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetName.Name}",
                            "Jméno roku nebo sezóny měření" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetName.Name}",
                            "Jméno roku nebo sezóny měření" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetLabel.Name}",
                            "Popisek roku nebo sezóny měření" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetLabel.Name}",
                            "Popisek roku nebo sezóny měření" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateBegin.Name}",
                            "Datum začátku sezóny měření" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateBegin.Name}",
                            "Datum začátku sezóny měření" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateEnd.Name}",
                            "Datum konce sezóny měření" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateEnd.Name}",
                            "Datum konce sezóny měření" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlViewPanels),
                            out Dictionary<string, string> dictNational)
                                ? dictNational
                                : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnFilterColumns),                       "Table panels column selection" },
                        { nameof(btnClose),                               "Close" },
                        { nameof(grpData),                                "Panels:" },
                        { nameof(strRowsCountNone),                       "No panel is selected." },
                        { nameof(strRowsCountOne),                        "$1 panel is selected." },
                        { nameof(strRowsCountSome),                       "$1 panels are selected." },
                        { nameof(strRowsCountMany),                       "$1 panels are selected." },
                        { nameof(tsrTools),                               String.Empty },

                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColId.Name}",
                            "Panel with corresponding reference year set identifier" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColId.Name}",
                            "Panel with corresponding reference year set identifier" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name}",
                            "Group of panels with corresponding reference year sets identifier" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name}",
                            "Group of panels with corresponding reference year sets identifier" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelId.Name}",
                            "Sampling panel identifier" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelId.Name}",
                            "Sampling panel identifier" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelName.Name}",
                            "Sampling panel name" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelName.Name}",
                            "Sampling panel name" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelLabel.Name}",
                            "Sampling panel label" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelLabel.Name}",
                            "Sampling panel label" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelClusterCount.Name}",
                            "Total number of clusters on panel" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelClusterCount.Name}",
                            "Total number of clusters on panel" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelPlotCount.Name}",
                            "Total number of plots on panel" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColPanelPlotCount.Name}",
                            "Total number of plots on panel" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetId.Name}",
                            "Reference year or season set identifier" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetId.Name}",
                            "Reference year or season set identifier" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetName.Name}",
                            "Reference year or season set name" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetName.Name}",
                            "Reference year or season set name" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetLabel.Name}",
                            "Reference year or season set label" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceYearSetLabel.Name}",
                            "Reference year or season set label" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateBegin.Name}",
                            "Date from which the period set begins" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateBegin.Name}",
                            "Date from which the period set begins" },
                        { $"col-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateEnd.Name}",
                            "Date to which the period set ends" },
                        { $"tip-{VwPanelRefYearSetPairList.Name}.{VwPanelRefYearSetPairList.ColReferenceDateEnd.Name}",
                            "Date to which the period set ends" }
                    }
                        : languageFile.InternationalVersion.Data.TryGetValue(key: nameof(ControlViewPanels),
                            out Dictionary<string, string> dictInternational)
                                ? dictInternational
                                : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            DataSource = new VwPanelRefYearSetPairList(database: Database);

            InitializeLabels();

            LoadContent();

            DisplayButtonFilterColumns = false;

            DisplayButtonClose = false;

            Setting.LoadViewPanelsColumnsWidth();

            btnFilterColumns.Click += new EventHandler(
                (sender, e) =>
                {
                    // TODO
                });

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    Closed?.Invoke(sender: sender, e: e);
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace DataGridView pro zobrazení panelů pro vybraný odhad</para>
        /// <para lang="en">Initialization of the DataGridView for display panels for selected estimate</para>
        /// </summary>
        private void InitializeDataGridView()
        {
            pnlData.Controls.Clear();

            dgvPanels = new DataGridView()
            {
                Dock = DockStyle.Fill
            };

            dgvPanels.SelectionChanged += (sender, e) =>
            {
                dgvPanels.ClearSelection();
            };

            dgvPanels.ColumnWidthChanged += (sender, e) =>
            {
                ColumnMetadata col =
                    VwPanelRefYearSetPairList.Cols.Values
                        .Where(a => a.Name == e.Column.DataPropertyName)
                        .FirstOrDefault<ColumnMetadata>();
                if (col != null)
                {
                    col.Width = e.Column.Width;
                    Setting.SaveViewPanelsColumnsWidth();
                }
            };

            dgvPanels.CreateControl();

            pnlData.Controls.Add(value: dgvPanels);

            DataSource.SetDataGridViewDataSource(
                dataGridView: dgvPanels);

            DataSource.FormatDataGridView(
                dataGridView: dgvPanels);

            SetLabelRowsCountValue(count: dgvPanels.Rows.Count);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            #region Dictionary

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Dictionary<string, string> labelsCs = Dictionary(
                languageVersion: LanguageVersion.National,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsEn = Dictionary(
                languageVersion: LanguageVersion.International,
                languageFile: LanguageFile);

            #endregion Dictionary

            #region Controls

            btnFilterColumns.Text =
                labels.TryGetValue(key: nameof(btnFilterColumns),
                out string btnFilterColumnsText)
                    ? btnFilterColumnsText
                    : String.Empty;

            btnClose.Text =
                labels.TryGetValue(key: nameof(btnClose),
                out string btnCloseText)
                    ? btnCloseText
                    : String.Empty;

            grpData.Text =
                labels.TryGetValue(key: nameof(grpData),
                out string grpDataText)
                    ? grpDataText
                    : String.Empty;

            tsrTools.Text =
                labels.TryGetValue(key: nameof(tsrTools),
                out string tsrToolsText)
                    ? tsrToolsText
                    : String.Empty;

            #endregion Controls

            #region Visible Columns

            VisibleColumns =
                Setting.ViewPanelsVisibleColumns
                    .Distinct()
                    .ToArray<string>();

            #endregion Visible Columns

            #region VwPanelRefYearSetPairList

            foreach (ColumnMetadata column in VwPanelRefYearSetPairList.Cols.Values)
            {
                VwPanelRefYearSetPairList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{VwPanelRefYearSetPairList.Name}.{column.Name}") ?
                    labelsCs[$"col-{VwPanelRefYearSetPairList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{VwPanelRefYearSetPairList.Name}.{column.Name}") ?
                    labelsEn[$"col-{VwPanelRefYearSetPairList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{VwPanelRefYearSetPairList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{VwPanelRefYearSetPairList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{VwPanelRefYearSetPairList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{VwPanelRefYearSetPairList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion VwPanelRefYearSetPairList
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
        }

        /// <summary>
        /// <para lang="cs">Nastavení popisku s počtem vybraných řádků</para>
        /// <para lang="en">Setting the label with the number of selected rows</para>
        /// </summary>
        /// <param name="count">
        /// <para lang="cs">Počet vybraných řádků</para>
        /// <para lang="en">Number of selected rows</para>
        /// </param>
        private void SetLabelRowsCountValue(int count)
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            switch (count)
            {
                case 0:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountNone),
                            out strRowsCountNone)
                                ? strRowsCountNone
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 1:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountOne),
                            out strRowsCountOne)
                                ? strRowsCountOne
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                case 2:
                case 3:
                case 4:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountSome),
                            out strRowsCountSome)
                                ? strRowsCountSome
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;

                default:
                    lblRowsCount.Text =
                        (labels.TryGetValue(key: nameof(strRowsCountMany),
                            out strRowsCountMany)
                                ? strRowsCountMany
                                : String.Empty)
                         .Replace(oldValue: "$1", newValue: count.ToString());
                    return;
            }
        }

        #endregion Methods

    }

}