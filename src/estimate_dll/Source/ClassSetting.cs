﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class Setting
        {

            #region Constants

#if DEBUG
            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Default value - Database server connection parameters</para>
            /// </summary>
            private static readonly PostgreSQL.Setting DefaultDBSetting = new()
            {
                ApplicationName = "NfiEstaGUI: Module Estimate",
                LanguageVersion = LanguageVersion.National,
                Host = "localhost",
                Hosts = ["bran-nfiesta", "localhost"],
                Port = 5555,
                Ports = [5432, 5433, 5555],
                Database = "pathfinder_ds_1_5_jirka",
                Databases = [
                    "nfi_esta", "nfi_esta_dev", "nfiesta_test", "contrib_regression_nfiesta_data",
                    "pathfinder_ds", "pathfinder_ds_1_5","pathfinder_ds_1_5_jirka"],
                UserName = "jirka",
                UserNames = ["jirka", "vagrant"],
                CommandTimeout = 0,         // SQL příkazy mají neomezenou dobu na vykonání
                KeepAlive = 180,            // keep alive je posíláno 1x za 3 minuty
                TcpKeepAlive = true,        // tcp keep alive zapnuto
                TcpKeepAliveTime = 60,      // tcp keep alive je posíláno 1x za 1 minutu
                TcpKeepAliveInterval = 5
            };
#else
            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Default value - Database server connection parameters</para>
            /// </summary>
            private static readonly PostgreSQL.Setting DefaultDBSetting = new()
            {
                ApplicationName = "NfiEstaGUI: Module Estimate",
                LanguageVersion = LanguageVersion.International,
                Host = "localhost",
                Hosts = ["bran-nfiesta", "localhost"],
                Port = 5555,
                Ports = [5432, 5433, 5555],
                Database = "pathfinder_ds",
                Databases = [
                    "nfi_esta",
                    "pathfinder_ds", "pathfinder_ds_1_5"],
                UserName = "vagrant",
                UserNames = ["vagrant"],
                CommandTimeout = 0,         // SQL příkazy mají neomezenou dobu na vykonání
                KeepAlive = 180,            // keep alive je posíláno 1x za 3 minuty
                TcpKeepAlive = true,        // tcp keep alive zapnuto
                TcpKeepAliveTime = 60,      // tcp keep alive je posíláno 1x za 1 minutu
                TcpKeepAliveInterval = 5
            };
#endif

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Požadovaný počet vláken pro konfiguraci odhadů</para>
            /// <para lang="en">Default value - Required number of threads to configure estimates</para>
            /// </summary>
            public const int DefaultConfigurationRequiredThreadsNumber = 10;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Dostupný počet vláken pro konfiguraci odhadů</para>
            /// <para lang="en">Default value - Available number of threads to configure estimates</para>
            /// </summary>
            private const int DefaultConfigurationAvailableThreadsNumber = 10;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Požadovaný počet vláken pro výpočet odhadů</para>
            /// <para lang="en">Default value - Required number of threads to calculate estimates</para>
            /// </summary>
            public const int DefaultEstimateRequiredThreadsNumber = 10;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Dostupný počet vláken pro výpočet odhadů</para>
            /// <para lang="en">Default value - Available number of threads to calculate estimates</para>
            /// </summary>
            private const int DefaultEstimateAvailableThreadsNumber = 10;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Mezní hodnota pro kontrolu atributové a geografické aditivity</para>
            /// <para lang="en">Default value - Attribute and geographical additivity threshold</para>
            /// </summary>
            public const double DefaultAdditivityLimit = 0.00000001;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Tolerance pokrytí oblasti odhadu</para>
            /// <para lang="en">Default value - Cell coverage tolerance</para>
            /// </summary>
            public const double DefaultCellCoverageTolerance = 0.001;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Maximální počet výpočetních buněk na jednu kolekci</para>
            /// <para lang="en">Default value - Maximum number of estimation cells per collection</para>
            /// </summary>
            private const int DefaultEstimationCellsCountLimit = 5000;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazit stavový řádek</para>
            /// <para lang="en">Default value - Display status strip</para>
            /// </summary>
            private const bool DefaultStatusStripVisible = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Default value - Display executed SQL commands when debugging module</para>
            /// </summary>
            private const bool DefaultVerbose = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Default value - Suppressing the display of error message from SQL statement</para>
            /// </summary>
            private const bool DefaultDBSuppressError = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Default value - Suppressing the display of messages from SQL statement</para>
            /// </summary>
            private const bool DefaultDBSuppressWarning = true;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazené sloupce výsledků odhadů úhrnu</para>
            /// <para lang="en">Default value - Displayed column in total estimate results</para>
            /// </summary>
            private static readonly string[] DefaultViewOLAPTotalsVisibleColumns =
            [
                OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name,
                OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name,
                OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name,
                OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name,
                OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name,
                OLAPTotalEstimateList.ColVariableLabelEn.Name,
                OLAPTotalEstimateList.ColPointEstimate.Name,
                OLAPTotalEstimateList.ColStandardError.Name,
                OLAPTotalEstimateList.ColMinSSize.Name,
                OLAPTotalEstimateList.ColActSSize.Name,
                OLAPTotalEstimateList.ColConfidenceInterval.Name,
                OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name,
                OLAPTotalEstimateList.ColIsAdditive.Name,
                OLAPTotalEstimateList.ColIsLatest.Name,
                OLAPTotalEstimateList.ColCalcStarted.Name
            ];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazené sloupce výsledků odhadů podílu</para>
            /// <para lang="en">Default value - Displayed column in ratio estimate results</para>
            /// </summary>
            private static readonly string[] DefaultViewOLAPRatiosVisibleColumns =
            [
                OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name,
                OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name,
                OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name,
                OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name,
                OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name,
                OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name,
                OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name,
                OLAPRatioEstimateList.ColPointEstimate.Name,
                OLAPRatioEstimateList.ColStandardError.Name,
                OLAPRatioEstimateList.ColMinSSize.Name,
                OLAPRatioEstimateList.ColActSSize.Name,
                OLAPRatioEstimateList.ColConfidenceInterval.Name,
                OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name,
                OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name,
                OLAPRatioEstimateList.ColIsAdditive.Name,
                OLAPRatioEstimateList.ColIsLatest.Name,
                OLAPRatioEstimateList.ColCalcStarted.Name
            ];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazené sloupce v tabulce panelů</para>
            /// <para lang="en">Default value - Displayed column in table of panels</para>
            /// </summary>
            private static readonly string[] DefaultViewPanelsVisibleColumns =
            [
                VwPanelRefYearSetPairList.ColPanelLabel.Name,
                VwPanelRefYearSetPairList.ColReferenceYearSetLabel.Name,
                VwPanelRefYearSetPairList.ColPanelClusterCount.Name,
                VwPanelRefYearSetPairList.ColPanelPlotCount.Name
            ];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazené sloupce v tabulce období</para>
            /// <para lang="en">Default value - Displayed column in table of periods</para>
            /// </summary>
            private static readonly string[] DefaultViewPeriodsVisibleColumns =
            [
                EstimationPeriodList.ColId.Name,
                EstimationPeriodList.ColEstimateDateBegin.Name,
                EstimationPeriodList.ColEstimateDateEnd.Name
            ];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Šířka sloupce v tabulkách</para>
            /// <para lang="en">Default value - Column width in data tables</para>
            /// </summary>
            public const int DefaultColumnWidth = 100;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva oddělovačů</para>
            /// <para lang="en">Default value - Splitters color</para>
            /// </summary>
            private static readonly Color DefaultSpliterColor = SystemColors.Control;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva okraje ovládacího prvku</para>
            /// <para lang="en">Default value - Control border color</para>
            /// </summary>
            private static readonly Color DefaultBorderColor = SystemColors.Control;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Default value - Splitter distance of the caption
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private const int DefaultSplTargetVariableCaptionDistance = 40;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače mezi tématickým okruhem a indikátorem
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Default value - Splitter distance between topic and indicator
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private const int DefaultSplTopicDistance = 250;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače mezi indikátorem a položkami metadat
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Default value - Splitter distance between indicator and metadata items
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private const int DefaultSplIndicatorDistance = 250;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba jednotky a typu odhadu"</para>
            /// <para lang="en">Default value - Splitter distance of the caption
            /// in the Control "Unit and estimate type selection"</para>
            /// </summary>
            private const int DefaultSplUnitCaptionDistance = 130;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba atributových kategorií pro odhad úhrnu"</para>
            /// <para lang="en">Default value - Splitter distance of the caption
            /// in the Control "Attribute categories selection for total estimate"</para>
            /// </summary>
            private const int DefaultSplVariableCaptionDistance = 150;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače
            /// mezi kategoriemi plošné domény nebo subpopulace a atributovými kategoriemi
            /// na ovládacím prvku "Volba atributových kategorií pro odhad úhrnu"</para>
            /// <para lang="en">Default value - Splitter distance
            /// between area or subpopulation categories and attribute categories
            /// in the Control "Attribute categories selection for total estimate"</para>
            /// </summary>
            private const int DefaultSplVariableDistance = 200;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače nadpisu pro čitatel
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Default value - Splitter distance of the caption for numerator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private const int DefaultSplNumeratorDistance = 75;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače nadpisu pro jmenovatel
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Default value - Splitter distance of the caption for denominator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private const int DefaultSplDenominatorDistance = 75;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače
            /// mezi čitatelem a jmenovatelem
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Default value - Splitter distance
            /// between numerator and denominator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private const int DefaultSplNumeratorDenominatorDistance = 170;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače
            /// mezi kategoriemi plošné domény nebo subpopulace a atributovými kategoriemi
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Default value - Splitter distance
            /// between area or subpopulation categories and attribute categories
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private const int DefaultSplVariableRatioDistance = 340;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Konfigurace a výpočet odhadů"</para>
            /// <para lang="en">Default value - Splitter distance of the caption
            /// in the Control "Configuration and calculation estimates"</para>
            /// </summary>
            private const int DefaultSplEstimateResultCaptionDistance = 200;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače
            /// mezi seznamem filtrů řádků a zobrazením výsledků odhadů
            /// na ovládacím prvku "Konfigurace a výpočet odhadů"</para>
            /// <para lang="en">Default value - Splitter distance
            /// between list of row filters and estimate results
            /// in the Control "Configuration and calculation estimates"</para>
            /// </summary>
            private const int DefaultSplEstimateResultDistance = 350;

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            private PostgreSQL.Setting dbSetting;

            /// <summary>
            /// <para lang="cs">Požadovaný počet vláken pro konfiguraci odhadů</para>
            /// <para lang="en">Required number of threads to configure estimates</para>
            /// </summary>
            private Nullable<int> configurationRequiredThreadsNumber;

            /// <summary>
            /// <para lang="cs">Dostupný počet vláken pro konfiguraci odhadů</para>
            /// <para lang="en">Available number of threads to configure estimates</para>
            /// </summary>
            private Nullable<int> configurationAvailableThreadsNumber;

            /// <summary>
            /// <para lang="cs">Požadovaný počet vláken pro výpočet odhadů</para>
            /// <para lang="en">Required number of threads to calculate estimates</para>
            /// </summary>
            private Nullable<int> estimateRequiredThreadsNumber;

            /// <summary>
            /// <para lang="cs">Dostupný počet vláken pro výpočet odhadů</para>
            /// <para lang="en">Available number of threads to calculate estimates</para>
            /// </summary>
            private Nullable<int> estimateAvailableThreadsNumber;

            /// <summary>
            /// <para lang="cs">Mezní hodnota pro kontrolu atributové a geografické aditivity</para>
            /// <para lang="en">Attribute and geographical additivity threshold</para>
            /// </summary>
            private Nullable<double> additivityLimit;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Tolerance pokrytí oblasti odhadu</para>
            /// <para lang="en">Default value - Cell coverage tolerance</para>
            /// </summary>
            private Nullable<double> cellCoverageTolerance;

            /// <summary>
            /// <para lang="cs">Maximální počet výpočetních buněk na jednu kolekci</para>
            /// <para lang="en">Maximum number of estimation cells per collection</para>
            /// </summary>
            private Nullable<int> estimationCellsCountLimit;

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            private Nullable<bool> statusStripVisible;

            /// <summary>
            /// <para lang="cs">Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Display executed SQL commands when debugging module</para>
            /// </summary>
            private Nullable<bool> verbose;

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of error message from SQL statement</para>
            /// </summary>
            private Nullable<bool> dbSuppressError;

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of messages from SQL statement</para>
            /// </summary>
            private Nullable<bool> dbSuppressWarning;

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce výsledků odhadů úhrnu</para>
            /// <para lang="en">Displayed column in total estimate results table</para>
            /// </summary>
            private string[] viewOLAPTotalsVisibleColumns;

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce výsledků odhadů podílu</para>
            /// <para lang="en">Displayed column in ratio estimate results table</para>
            /// </summary>
            private string[] viewOLAPRatiosVisibleColumns;

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce panelů</para>
            /// <para lang="en">Displayed column in table of panels</para>
            /// </summary>
            private string[] viewPanelsVisibleColumns;

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce období</para>
            /// <para lang="en">Displayed column in table of periods</para>
            /// </summary>
            private string[] viewPeriodsVisibleColumns;

            /// <summary>
            /// <para lang="cs">Šířky sloupců v tabulce výsledků odhadů úhrnu</para>
            /// <para lang="en">Columns width in total estimate results table</para>
            /// </summary>
            private int[] viewOLAPTotalsColumnsWidth;

            /// <summary>
            /// <para lang="cs">Šířky sloupců v tabulce výsledků odhadů podílu</para>
            /// <para lang="en">Columns width in ratio estimate results table</para>
            /// </summary>
            private int[] viewOLAPRatiosColumnsWidth;

            /// <summary>
            /// <para lang="cs">Šířky sloupců v tabulce panelů</para>
            /// <para lang="en">Columns width in table of panels</para>
            /// </summary>
            private int[] viewPanelsColumnsWidth;

            /// <summary>
            /// <para lang="cs">Šířky sloupců  v tabulce období</para>
            /// <para lang="en">Columns width in table of periods</para>
            /// </summary>
            private int[] viewPeriodsColumnsWidth;

            /// <summary>
            /// <para lang="cs">Barva oddělovačů</para>
            /// <para lang="en">Splitters color</para>
            /// </summary>
            private Color spliterColor;

            /// <summary>
            /// <para lang="cs">Barva okraje ovládacího prvku</para>
            /// <para lang="en">Control border color</para>
            /// </summary>
            private Color borderColor;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private Nullable<int> splTargetVariableCaptionDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače mezi tématickým okruhem a indikátorem
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance between topic and indicator
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private Nullable<int> splTopicDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače mezi indikátorem a položkami metadat
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance between indicator and metadata items
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private Nullable<int> splIndicatorDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba jednotky a typu odhadu"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Unit and estimate type selection"</para>
            /// </summary>
            private Nullable<int> splUnitCaptionDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba atributových kategorií pro odhad úhrnu"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Attribute categories selection for total estimate"</para>
            /// </summary>
            private Nullable<int> splVariableCaptionDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi kategoriemi plošné domény nebo subpopulace a atributovými kategoriemi
            /// na ovládacím prvku "Volba atributových kategorií pro odhad úhrnu"</para>
            /// <para lang="en">Splitter distance
            /// between area or subpopulation categories and attribute categories
            /// in the Control "Attribute categories selection for total estimate"</para>
            /// </summary>
            private Nullable<int> splVariableDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu pro čitatel
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance of the caption for numerator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private Nullable<int> splNumeratorDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu pro jmenovatel
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance of the caption for denominator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private Nullable<int> splDenominatorDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi čitatelem a jmenovatelem
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance
            /// between numerator and denominator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private Nullable<int> splNumeratorDenominatorDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi kategoriemi plošné domény nebo subpopulace a atributovými kategoriemi
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance
            /// between area or subpopulation categories and attribute categories
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            private Nullable<int> splVariableRatioDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Konfigurace a výpočet odhadů"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Configuration and calculation estimates"</para>
            /// </summary>
            private Nullable<int> splEstimateResultCaptionDistance;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi seznamem filtrů řádků a zobrazením výsledků odhadů
            /// na ovládacím prvku "Konfigurace a výpočet odhadů"</para>
            /// <para lang="en">Splitter distance
            /// between list of row filters and estimate results
            /// in the Control "Configuration and calculation estimates"</para>
            /// </summary>
            private Nullable<int> splEstimateResultDistance;

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            private JsonSerializerOptions serializerOptions;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu</para>
            /// <para lang="en">Object constructor</para>
            /// </summary>
            public Setting()
            {
                SerializerOptions = null;
                DBSetting = DefaultDBSetting;
                ConfigurationRequiredThreadsNumber = DefaultConfigurationRequiredThreadsNumber;
                ConfigurationAvailableThreadsNumber = DefaultConfigurationAvailableThreadsNumber;
                EstimateRequiredThreadsNumber = DefaultEstimateRequiredThreadsNumber;
                EstimateAvailableThreadsNumber = DefaultEstimateAvailableThreadsNumber;
                AdditivityLimit = DefaultAdditivityLimit;
                CellCoverageTolerance = DefaultCellCoverageTolerance;
                EstimationCellsCountLimit = DefaultEstimationCellsCountLimit;
                StatusStripVisible = DefaultStatusStripVisible;
                Verbose = DefaultVerbose;
                DBSuppressError = DefaultDBSuppressError;
                DBSuppressWarning = DefaultDBSuppressWarning;
                ViewOLAPTotalsVisibleColumns = DefaultViewOLAPTotalsVisibleColumns;
                ViewOLAPRatiosVisibleColumns = DefaultViewOLAPRatiosVisibleColumns;
                ViewPanelsVisibleColumns = DefaultViewPanelsVisibleColumns;
                ViewPeriodsVisibleColumns = DefaultViewPeriodsVisibleColumns;
                ViewOLAPTotalsColumnsWidth = null;
                ViewOLAPRatiosColumnsWidth = null;
                ViewPanelsColumnsWidth = null;
                ViewPeriodsColumnsWidth = null;
                SpliterColor = DefaultSpliterColor;
                BorderColor = DefaultBorderColor;
                SplTargetVariableCaptionDistance = DefaultSplTargetVariableCaptionDistance;
                SplTopicDistance = DefaultSplTopicDistance;
                SplIndicatorDistance = DefaultSplIndicatorDistance;
                SplUnitCaptionDistance = DefaultSplUnitCaptionDistance;
                SplVariableCaptionDistance = DefaultSplVariableCaptionDistance;
                SplVariableDistance = DefaultSplVariableDistance;
                SplNumeratorDistance = DefaultSplNumeratorDistance;
                SplDenominatorDistance = DefaultSplDenominatorDistance;
                SplNumeratorDenominatorDistance = DefaultSplNumeratorDenominatorDistance;
                SplVariableRatioDistance = DefaultSplVariableRatioDistance;
                SplEstimateResultCaptionDistance = DefaultSplEstimateResultCaptionDistance;
                SplEstimateResultDistance = DefaultSplEstimateResultDistance;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            public PostgreSQL.Setting DBSetting
            {
                get
                {
                    return dbSetting ?? DefaultDBSetting;
                }
                set
                {
                    dbSetting = value ?? DefaultDBSetting;
                }
            }


            /// <summary>
            /// <para lang="cs">Požadovaný počet vláken pro konfiguraci odhadů</para>
            /// <para lang="en">Required number of threads to configure estimates</para>
            /// </summary>
            public int ConfigurationRequiredThreadsNumber
            {
                get
                {
                    return configurationRequiredThreadsNumber ?? DefaultConfigurationRequiredThreadsNumber;
                }
                set
                {
                    configurationRequiredThreadsNumber = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Dostupný počet vláken pro konfiguraci odhadů</para>
            /// <para lang="en">Available number of threads to configure estimates</para>
            /// </summary>
            public int ConfigurationAvailableThreadsNumber
            {
                get
                {
                    return configurationAvailableThreadsNumber ?? DefaultConfigurationAvailableThreadsNumber;
                }
                set
                {
                    configurationAvailableThreadsNumber = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Požadovaný počet vláken pro výpočet odhadů</para>
            /// <para lang="en">Required number of threads to calculate estimates</para>
            /// </summary>
            public int EstimateRequiredThreadsNumber
            {
                get
                {
                    return estimateRequiredThreadsNumber ?? DefaultEstimateRequiredThreadsNumber;
                }
                set
                {
                    estimateRequiredThreadsNumber = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Dostupný počet vláken pro výpočet odhadů</para>
            /// <para lang="en">Available number of threads to calculate estimates</para>
            /// </summary>
            public int EstimateAvailableThreadsNumber
            {
                get
                {
                    return estimateAvailableThreadsNumber ?? DefaultEstimateAvailableThreadsNumber;
                }
                set
                {
                    estimateAvailableThreadsNumber = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Mezní hodnota pro kontrolu atributové a geografické aditivity</para>
            /// <para lang="en">Attribute and geographical additivity threshold</para>
            /// </summary>
            public double AdditivityLimit
            {
                get
                {
                    return additivityLimit ?? DefaultAdditivityLimit;
                }
                set
                {
                    additivityLimit = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Tolerance pokrytí oblasti odhadu</para>
            /// <para lang="en">Default value - Cell coverage tolerance</para>
            /// </summary>
            public double CellCoverageTolerance
            {
                get
                {
                    return cellCoverageTolerance ?? DefaultCellCoverageTolerance;
                }
                set
                {
                    cellCoverageTolerance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Maximální počet výpočetních buněk na jednu kolekci</para>
            /// <para lang="en">Maximum number of estimation cells per collection</para>
            /// </summary>
            public int EstimationCellsCountLimit
            {
                get
                {
                    return estimationCellsCountLimit ?? DefaultEstimationCellsCountLimit;
                }
                set
                {
                    estimationCellsCountLimit = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            public bool StatusStripVisible
            {
                get
                {
                    return statusStripVisible ?? DefaultStatusStripVisible;
                }
                set
                {
                    statusStripVisible = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Display executed SQL commands when debugging module</para>
            /// </summary>
            public bool Verbose
            {
                get
                {
                    return verbose ?? DefaultVerbose;
                }
                set
                {
                    verbose = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of error message from SQL statement</para>
            /// </summary>
            public bool DBSuppressError
            {
                get
                {
                    return dbSuppressError ?? DefaultDBSuppressError;
                }
                set
                {
                    dbSuppressError = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of messages from SQL statement</para>
            /// </summary>
            public bool DBSuppressWarning
            {
                get
                {
                    return dbSuppressWarning ?? DefaultDBSuppressWarning;
                }
                set
                {
                    dbSuppressWarning = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce výsledků odhadů úhrnu</para>
            /// <para lang="en">Displayed column in total estimate results table</para>
            /// </summary>
            public string[] ViewOLAPTotalsVisibleColumns
            {
                get
                {
                    return viewOLAPTotalsVisibleColumns ?? DefaultViewOLAPTotalsVisibleColumns;
                }
                set
                {
                    viewOLAPTotalsVisibleColumns = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce výsledků odhadů podílu</para>
            /// <para lang="en">Displayed column in ratio estimate results table</para>
            /// </summary>
            public string[] ViewOLAPRatiosVisibleColumns
            {
                get
                {
                    return viewOLAPRatiosVisibleColumns ?? DefaultViewOLAPRatiosVisibleColumns;
                }
                set
                {
                    viewOLAPRatiosVisibleColumns = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce panelů</para>
            /// <para lang="en">Displayed column in table of panels</para>
            /// </summary>
            public string[] ViewPanelsVisibleColumns
            {
                get
                {
                    return viewPanelsVisibleColumns ?? DefaultViewPanelsVisibleColumns;
                }
                set
                {
                    viewPanelsVisibleColumns = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazené sloupce v tabulce období</para>
            /// <para lang="en">Displayed column in table of periods</para>
            /// </summary>
            public string[] ViewPeriodsVisibleColumns
            {
                get
                {
                    return viewPeriodsVisibleColumns ?? DefaultViewPeriodsVisibleColumns;
                }
                set
                {
                    viewPeriodsVisibleColumns = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Šířky sloupců v tabulce výsledků odhadů úhrnu</para>
            /// <para lang="en">Columns width in total estimate results table</para>
            /// </summary>
            public int[] ViewOLAPTotalsColumnsWidth
            {
                get
                {
                    return viewOLAPTotalsColumnsWidth ??
                        OLAPTotalEstimateList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
                set
                {
                    viewOLAPTotalsColumnsWidth = value ??
                        OLAPTotalEstimateList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
            }

            /// <summary>
            /// <para lang="cs">Šířky sloupců v tabulce výsledků odhadů podílu</para>
            /// <para lang="en">Columns width in ratio estimate results table</para>
            /// </summary>
            public int[] ViewOLAPRatiosColumnsWidth
            {
                get
                {
                    return viewOLAPRatiosColumnsWidth ??
                        OLAPRatioEstimateList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
                set
                {
                    viewOLAPRatiosColumnsWidth = value ??
                        OLAPRatioEstimateList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
            }

            /// <summary>
            /// <para lang="cs">Šířky sloupců v tabulce panelů</para>
            /// <para lang="en">Columns width in table of panels</para>
            /// </summary>
            public int[] ViewPanelsColumnsWidth
            {
                get
                {
                    return viewPanelsColumnsWidth ??
                        VwPanelRefYearSetPairList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
                set
                {
                    viewPanelsColumnsWidth = value ??
                        VwPanelRefYearSetPairList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
            }

            /// <summary>
            /// <para lang="cs">Šířky sloupců  v tabulce období</para>
            /// <para lang="en">Columns width in table of periods</para>
            /// </summary>
            public int[] ViewPeriodsColumnsWidth
            {
                get
                {
                    return viewPeriodsColumnsWidth ??
                        EstimationPeriodList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
                set
                {
                    viewPeriodsColumnsWidth = value ??
                        EstimationPeriodList.Cols.Values.OrderBy(a => a.Name).Select(a => DefaultColumnWidth).ToArray<int>();
                }
            }

            /// <summary>
            /// <para lang="cs">Barva oddělovačů</para>
            /// <para lang="en">Splitters color</para>
            /// </summary>
            public Color SpliterColor
            {
                get
                {
                    return spliterColor;
                }
                set
                {
                    spliterColor = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Barva okraje ovládacího prvku</para>
            /// <para lang="en">Control border color</para>
            /// </summary>
            public Color BorderColor
            {
                get
                {
                    return borderColor;
                }
                set
                {
                    borderColor = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Indicator selection"</para>
            /// </summary>
            public int SplTargetVariableCaptionDistance
            {
                get
                {
                    return splTargetVariableCaptionDistance ?? DefaultSplTargetVariableCaptionDistance;
                }
                set
                {
                    splTargetVariableCaptionDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače mezi tématickým okruhem a indikátorem
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance between topic and indicator
            /// in the Control "Indicator selection"</para>
            /// </summary>
            public int SplTopicDistance
            {
                get
                {
                    return splTopicDistance ?? DefaultSplTopicDistance;
                }
                set
                {
                    splTopicDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače mezi indikátorem a položkami metadat
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance between indicator and metadata items
            /// in the Control "Indicator selection"</para>
            /// </summary>
            public int SplIndicatorDistance
            {
                get
                {
                    return splIndicatorDistance ?? DefaultSplIndicatorDistance;
                }
                set
                {
                    splIndicatorDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba jednotky a typu odhadu"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Unit and estimate type selection"</para>
            /// </summary>
            public int SplUnitCaptionDistance
            {
                get
                {
                    return splUnitCaptionDistance ?? DefaultSplUnitCaptionDistance;
                }
                set
                {
                    splUnitCaptionDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Volba atributových kategorií pro odhad úhrnu"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Attribute categories selection for total estimate"</para>
            /// </summary>
            public int SplVariableCaptionDistance
            {
                get
                {
                    return splVariableCaptionDistance ?? DefaultSplVariableCaptionDistance;
                }
                set
                {
                    splVariableCaptionDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi kategoriemi plošné domény nebo subpopulace a atributovými kategoriemi
            /// na ovládacím prvku "Volba atributových kategorií pro odhad úhrnu"</para>
            /// <para lang="en">Splitter distance
            /// between area or subpopulation categories and attribute categories
            /// in the Control "Attribute categories selection for total estimate"</para>
            /// </summary>
            public int SplVariableDistance
            {
                get
                {
                    return splVariableDistance ?? DefaultSplVariableDistance;
                }
                set
                {
                    splVariableDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu pro čitatel
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance of the caption for numerator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            public int SplNumeratorDistance
            {
                get
                {
                    return splNumeratorDistance ?? DefaultSplNumeratorDistance;
                }
                set
                {
                    splNumeratorDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu pro jmenovatel
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance of the caption for denominator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            public int SplDenominatorDistance
            {
                get
                {
                    return splDenominatorDistance ?? DefaultSplDenominatorDistance;
                }
                set
                {
                    splDenominatorDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi čitatelem a jmenovatelem
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance
            /// between numerator and denominator
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            public int SplNumeratorDenominatorDistance
            {
                get
                {
                    return splNumeratorDenominatorDistance ?? DefaultSplNumeratorDenominatorDistance;
                }
                set
                {
                    splNumeratorDenominatorDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi kategoriemi plošné domény nebo subpopulace a atributovými kategoriemi
            /// na ovládacím prvku "Volba atributových kategorií pro odhad podílu"</para>
            /// <para lang="en">Splitter distance
            /// between area or subpopulation categories and attribute categories
            /// in the Control "Attribute categories selection for ratio estimate"</para>
            /// </summary>
            public int SplVariableRatioDistance
            {
                get
                {
                    return splVariableRatioDistance ?? DefaultSplVariableRatioDistance;
                }
                set
                {
                    splVariableRatioDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače nadpisu
            /// na ovládacím prvku "Konfigurace a výpočet odhadů"</para>
            /// <para lang="en">Splitter distance of the caption
            /// in the Control "Configuration and calculation estimates"</para>
            /// </summary>
            public int SplEstimateResultCaptionDistance
            {
                get
                {
                    return splEstimateResultCaptionDistance ?? DefaultSplEstimateResultCaptionDistance;
                }
                set
                {
                    splEstimateResultCaptionDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače
            /// mezi seznamem filtrů řádků a zobrazením výsledků odhadů
            /// na ovládacím prvku "Konfigurace a výpočet odhadů"</para>
            /// <para lang="en">Splitter distance
            /// between list of row filters and estimate results
            /// in the Control "Configuration and calculation estimates"</para>
            /// </summary>
            public int SplEstimateResultDistance
            {
                get
                {
                    return splEstimateResultDistance ?? DefaultSplEstimateResultDistance;
                }
                set
                {
                    splEstimateResultDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            [JsonIgnore]
            private JsonSerializerOptions SerializerOptions
            {
                get
                {
                    return serializerOptions ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
                set
                {
                    serializerOptions = value ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
            }

            /// <summary>
            /// <para lang="cs">Serializace objektu do formátu JSON</para>
            /// <para lang="en">Object serialization to JSON format</para>
            /// </summary>
            [JsonIgnore]
            public string JSON
            {
                get
                {
                    return JsonSerializer.Serialize(
                        value: this,
                        options: SerializerOptions);
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Obnovení objektu z formátu JSON</para>
            /// <para lang="en">Restoring an object from JSON format</para>
            /// </summary>
            /// <param name="json">
            /// <para lang="cs">JSON text</para>
            /// <para lang="en">JSON text</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Nastavení modulu</para>
            /// <para lang="en">Module setting</para>
            /// </returns>
            public static Setting Deserialize(string json)
            {
                return
                    JsonSerializer.Deserialize<Setting>(json: json);
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Uložení šířky sloupců v tabulce výsledků odhadů úhrnu</para>
            /// <para lang="en">Save columns width in total estimate results table into setting</para>
            /// </summary>
            public void SaveViewOLAPTotalsColumnsWidth()
            {
                int i = 0;
                ViewOLAPTotalsColumnsWidth = null;
                foreach (ColumnMetadata col in OLAPTotalEstimateList.Cols.Values.OrderBy(a => a.Name))
                {
                    ViewOLAPTotalsColumnsWidth[i++] = col.Width;
                }
            }

            /// <summary>
            /// <para lang="cs">Načtení šířky sloupců v tabulce výsledků odhadů úhrnu</para>
            /// <para lang="en">Load columns width in total estimate results table from setting</para>
            /// </summary>
            public void LoadViewOLAPTotalsColumnsWidth()
            {
                int i = 0;
                foreach (ColumnMetadata col in OLAPTotalEstimateList.Cols.Values.OrderBy(a => a.Name))
                {
                    if (i < ViewOLAPTotalsColumnsWidth.Length)
                    {
                        col.Width = ViewOLAPTotalsColumnsWidth[i++];
                    }
                    else
                    {
                        col.Width = DefaultColumnWidth;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Uložení šířky sloupců v tabulce výsledků odhadů podílu do nastavení</para>
            /// <para lang="en">Save columns width in ratio estimate results table into setting</para>
            /// </summary>
            public void SaveViewOLAPRatiosColumnsWidth()
            {
                int i = 0;
                ViewOLAPRatiosColumnsWidth = null;
                foreach (ColumnMetadata col in OLAPRatioEstimateList.Cols.Values.OrderBy(a => a.Name))
                {
                    ViewOLAPRatiosColumnsWidth[i++] = col.Width;
                }
            }

            /// <summary>
            /// <para lang="cs">Načtení šířky sloupců v tabulce výsledků odhadů podílu z nastavení</para>
            /// <para lang="en">Load columns width in ratio estimate results table from setting</para>
            /// </summary>
            public void LoadViewOLAPRatiosColumnsWidth()
            {
                int i = 0;
                foreach (ColumnMetadata col in OLAPRatioEstimateList.Cols.Values.OrderBy(a => a.Name))
                {
                    if (i < ViewOLAPRatiosColumnsWidth.Length)
                    {
                        col.Width = ViewOLAPRatiosColumnsWidth[i++];
                    }
                    else
                    {
                        col.Width = DefaultColumnWidth;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Uložení šířky sloupců v tabulce panelů do nastavení</para>
            /// <para lang="en">Save columns width in table of panels into setting</para>
            /// </summary>
            public void SaveViewPanelsColumnsWidth()
            {
                int i = 0;
                ViewPanelsColumnsWidth = null;
                foreach (ColumnMetadata col in VwPanelRefYearSetPairList.Cols.Values.OrderBy(a => a.Name))
                {
                    ViewPanelsColumnsWidth[i++] = col.Width;
                }
            }

            /// <summary>
            /// <para lang="cs">Načtení šířky sloupců v tabulce panelů z nastavení</para>
            /// <para lang="en">Load columns width in table of panels from setting</para>
            /// </summary>
            public void LoadViewPanelsColumnsWidth()
            {
                int i = 0;
                foreach (ColumnMetadata col in VwPanelRefYearSetPairList.Cols.Values.OrderBy(a => a.Name))
                {
                    if (i < ViewPanelsColumnsWidth.Length)
                    {
                        col.Width = ViewPanelsColumnsWidth[i++];
                    }
                    else
                    {
                        col.Width = DefaultColumnWidth;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Uložení šířky sloupců v tabulce období do nastavení</para>
            /// <para lang="en">Save columns width in table of periods into setting</para>
            /// </summary>
            public void SaveViewPeriodsColumnsWidth()
            {
                int i = 0;
                ViewPeriodsColumnsWidth = null;
                foreach (ColumnMetadata col in EstimationPeriodList.Cols.Values.OrderBy(a => a.Name))
                {
                    ViewPeriodsColumnsWidth[i++] = col.Width;
                }
            }

            /// <summary>
            /// <para lang="cs">Načtení šířky sloupců v tabulce období z nastavení</para>
            /// <para lang="en">Load columns width in table of periods from setting</para>
            /// </summary>
            public void LoadViewPeriodsColumnsWidth()
            {
                int i = 0;
                foreach (ColumnMetadata col in EstimationPeriodList.Cols.Values.OrderBy(a => a.Name))
                {
                    if (i < ViewPeriodsColumnsWidth.Length)
                    {
                        col.Width = ViewPeriodsColumnsWidth[i++];
                    }
                    else
                    {
                        col.Width = DefaultColumnWidth;
                    }
                }
            }

            #endregion Methods


            #region Style

            #region Button

            /// <summary>
            /// <para lang="cs">Styl písma pro tlačítko</para>
            /// <para lang="en">Font style for button</para>
            /// </summary>
            public static readonly Font ButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro tlačítko</para>
            /// <para lang="en">Font color for button</para>
            /// </summary>
            public static readonly Color ButtonForeColor =
                Color.Black;

            #endregion Button


            #region ComboBox

            /// <summary>
            /// <para lang="cs">Styl písma pro rozbalovací seznam</para>
            /// <para lang="en">Font style for combo box</para>
            /// </summary>
            public static readonly Font ComboBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro rozbalovací seznam</para>
            /// <para lang="en">Font color for combo box</para>
            /// </summary>
            public static readonly Color ComboBoxForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro rozbalovací seznam</para>
            /// <para lang="en">Back color for combo box</para>
            /// </summary>
            public static readonly Color ComboBoxBackColor =
                Color.White;

            #endregion ComboBox


            #region CheckBox

            /// <summary>
            /// <para lang="cs">Styl písma pro zaškrtávací políčko</para>
            /// <para lang="en">Font style for check box</para>
            /// </summary>
            public static readonly Font CheckBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro zaškrtávací políčko</para>
            /// <para lang="en">Font color for check box</para>
            /// </summary>
            public static readonly Color CheckBoxForeColor =
                Color.Black;

            #endregion CheckBox


            #region CheckedListBox

            /// <summary>
            /// <para lang="cs">Styl písma pro zaškrtávací seznam</para>
            /// <para lang="en">Font style for checked list box</para>
            /// </summary>
            public static readonly Font CheckedListBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro zaškrtávací seznam</para>
            /// <para lang="en">Font color for checked list box</para>
            /// </summary>
            public static readonly Color CheckedListBoxForeColor =
                Color.Black;

            #endregion CheckedListBox


            #region Form

            /// <summary>
            /// <para lang="cs">Styl písma pro formulář</para>
            /// <para lang="en">Font style for form</para>
            /// </summary>
            public static readonly Font FormFont = new(
                    familyName: "Segoe UI",
                    emSize: 9F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro formulář</para>
            /// <para lang="en">Font color for form</para>
            /// </summary>
            public static readonly Color FormForeColor =
                Color.Black;

            #endregion Form


            #region GroupBox

            /// <summary>
            ///
            /// </summary>
            public static readonly Color GroupBoxBackColor =
                Color.Transparent;

            /// <summary>
            ///
            /// </summary>
            public static readonly DockStyle GroupBoxDock =
                DockStyle.Fill;

            /// <summary>
            /// <para lang="cs">Styl písma pro skupinu</para>
            /// <para lang="en">Font style for group box</para>
            /// </summary>
            public static readonly Font GroupBoxFont = new(
                    familyName: "Segoe UI",
                    emSize: 11.25F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro skupinu</para>
            /// <para lang="en">Font color for group box</para>
            /// </summary>
            public static readonly Color GroupBoxForeColor =
                Color.MediumBlue;

            /// <summary>
            ///
            /// </summary>
            public static readonly Padding GroupBoxMargin =
                new(left: 0, top: 0, right: 0, bottom: 0);

            /// <summary>
            ///
            /// </summary>
            public static readonly Padding GroupBoxPadding =
                new(left: 5, top: 5, right: 5, bottom: 5);

            #endregion GroupBox


            #region Label

            /// <summary>
            /// <para lang="cs">Styl písma pro popisek</para>
            /// <para lang="en">Font style for label</para>
            /// </summary>
            public static readonly Font LabelFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek</para>
            /// <para lang="en">Font color for label</para>
            /// </summary>
            public static readonly Color LabelForeColor =
                Color.MediumBlue;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s hodnotou</para>
            /// <para lang="en">Font style for label with value</para>
            /// </summary>
            public static readonly Font LabelValueFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s hodnotou</para>
            /// <para lang="en">Font color for label with value</para>
            /// </summary>
            public static readonly Color LabelValueForeColor =
                Color.Black;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s chybovou hláškou</para>
            /// <para lang="en">Font style for label with error message</para>
            /// </summary>
            public static readonly Font LabelErrorFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s chybovou hláškou</para>
            /// <para lang="en">Font color for label with error message</para>
            /// </summary>
            public static readonly Color LabelErrorForeColor =
                Color.Red;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s nadpisem</para>
            /// <para lang="en">Font style for label with main caption</para>
            /// </summary>
            public static readonly Font LabelMainFont = new(
                   familyName: "Segoe UI",
                   emSize: 11.25F,
                   style: FontStyle.Bold,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s nadpisem</para>
            /// <para lang="en">Font color for label with main caption</para>
            /// </summary>
            public static readonly Color LabelMainForeColor =
                Color.Black;

            #endregion Label


            #region ListBox

            /// <summary>
            /// <para lang="cs">Styl písma pro seznam</para>
            /// <para lang="en">Font style for list box</para>
            /// </summary>
            public static readonly Font ListBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro seznam</para>
            /// <para lang="en">Font color for list box</para>
            /// </summary>
            public static readonly Color ListBoxForeColor =
                Color.Black;

            #endregion ListBox


            #region Panel

            /// <summary>
            ///
            /// </summary>
            public static readonly Color PanelBackColor =
                Color.Transparent;

            /// <summary>
            ///
            /// </summary>
            public static readonly BorderStyle PanelBorderStyle =
                BorderStyle.None;

            /// <summary>
            ///
            /// </summary>
            public static readonly DockStyle PanelDock =
                DockStyle.Fill;

            /// <summary>
            /// <para lang="cs">Styl písma pro skupinu</para>
            /// <para lang="en">Font style for group box</para>
            /// </summary>
            public static readonly Font PanelFont = new(
                    familyName: "Segoe UI",
                    emSize: 9F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro skupinu</para>
            /// <para lang="en">Font color for group box</para>
            /// </summary>
            public static readonly Color PanelForeColor =
                Color.Black;

            /// <summary>
            ///
            /// </summary>
            public static readonly Padding PanelMargin =
                new(left: 0, top: 0, right: 0, bottom: 0);

            /// <summary>
            ///
            /// </summary>
            public static readonly Padding PanelPadding =
                new(left: 0, top: 0, right: 0, bottom: 0);

            #endregion Panel


            #region RadioButton

            /// <summary>
            /// <para lang="cs">Styl písma pro přepínací tlačítko</para>
            /// <para lang="en">Font style for radio button</para>
            /// </summary>
            public static readonly Font RadioButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro přepínací tlačítko</para>
            /// <para lang="en">Font color for radio button</para>
            /// </summary>
            public static readonly Color RadioButtonForeColor =
                Color.Black;

            #endregion RadioButton


            #region Table

            /// <summary>
            /// <para lang="cs">Styl písma v hlavičce tabulky</para>
            /// <para lang="en">Font style in table header</para>
            /// </summary>
            public static readonly Font TableHeaderFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma v hlavičce tabulky</para>
            /// <para lang="en">Font color in table header</para>
            /// </summary>
            public static readonly Color TableHeaderForeColor =
                Color.White;

            /// <summary>
            /// <para lang="cs">Barva pozadí hlavičky tabulky</para>
            /// <para lang="en">Table header back color</para>
            /// </summary>
            public static readonly Color TableHeaderBackColor =
                Color.DarkBlue;


            /// <summary>
            /// <para lang="cs">Styl písma pro řádek tabulky</para>
            /// <para lang="en">Font style for table row</para>
            /// </summary>
            public static readonly Font TableRowFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro řádek tabulky</para>
            /// <para lang="en">Font color for table row</para>
            /// </summary>
            public static readonly Color TableRowForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro řádek tabulky</para>
            /// <para lang="en">Table row back color</para>
            /// </summary>
            public static readonly Color TableRowBackColor =
                Color.White;


            /// <summary>
            /// <para lang="cs">Styl písma pro vybraný řádek tabulky</para>
            /// <para lang="en">Font style for selected table row</para>
            /// </summary>
            public static readonly Font TableSelectedRowFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro vzbrařádek tabulky</para>
            /// <para lang="en">Font color for table row</para>
            /// </summary>
            public static readonly Color TableSelectedRowForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro řádek tabulky</para>
            /// <para lang="en">Table row back color</para>
            /// </summary>
            public static readonly Color TableSelectedRowBackColor =
                Color.Bisque;

            #endregion Table


            #region TextBox

            /// <summary>
            /// <para lang="cs">Styl písma pro textové políčko</para>
            /// <para lang="en">Font style for text box</para>
            /// </summary>
            public static readonly Font TextBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro textové políčko</para>
            /// <para lang="en">Font color for text box</para>
            /// </summary>
            public static readonly Color TextBoxForeColor =
                Color.Black;


            /// <summary>
            /// <para lang="cs">Styl písma pro textové políčko s kódem</para>
            /// <para lang="en">Font style for text box</para>
            /// </summary>
            public static readonly Font TextBoxCodeFont = new(
                   familyName: "Courier New",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro textové políčko s kódem</para>
            /// <para lang="en">Font color for text box with code</para>
            /// </summary>
            public static readonly Color TextBoxCodeForeColor =
                Color.Black;

            #endregion TextBox


            #region ToolStripButton

            /// <summary>
            /// <para lang="cs">Styl písma pro tlačítko</para>
            /// <para lang="en">Font style for tool strip button</para>
            /// </summary>
            public static readonly Font ToolStripButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro tlačítko</para>
            /// <para lang="en">Font color for button</para>
            /// </summary>
            public static readonly Color ToolStripButtonForeColor =
                Color.Black;

            #endregion ToolStripButton

            #endregion Style

        }

    }
}
