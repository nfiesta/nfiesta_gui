﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System.Collections.Generic;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{ 

    partial class ControlSingleToGRegMapConfigsForRatio
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new TableLayoutPanel();
            tlpData = new TableLayoutPanel();
            lblDenominatorForceSyntheticValue = new Label();
            lblDenominatorForceSyntheticCaption = new Label();
            lblDenominatorParamAreaTypeValue = new Label();
            lblDenominatorParamAreaTypeCaption = new Label();
            lblPanelRefYearSetGroupValue = new Label();
            lblPanelRefYearSetGroupCaption = new Label();
            lblDenominatorModelCaption = new Label();
            lblDenominatorModelValue = new Label();
            lblDenominatorModelSigmaCaption = new Label();
            lblDenominatorModelSigmaValue = new Label();
            rdoSelected = new RadioButton();
            lblAllEstimatesConfigurableCaption = new Label();
            lblAllEstimatesConfigurableValue = new Label();
            tlpMain.SuspendLayout();
            tlpData.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.WhiteSmoke;
            tlpMain.ColumnCount = 2;
            tlpMain.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 40F));
            tlpMain.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpData, 1, 0);
            tlpMain.Controls.Add(rdoSelected, 0, 0);
            tlpMain.Dock = DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(5, 5);
            tlpMain.Margin = new Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 1;
            tlpMain.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tlpMain.Size = new System.Drawing.Size(950, 60);
            tlpMain.TabIndex = 2;
            // 
            // tlpData
            // 
            tlpData.ColumnCount = 6;
            tlpData.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tlpData.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 250F));
            tlpData.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 10F));
            tlpData.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tlpData.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 250F));
            tlpData.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tlpData.Controls.Add(lblDenominatorForceSyntheticValue, 1, 2);
            tlpData.Controls.Add(lblDenominatorForceSyntheticCaption, 0, 2);
            tlpData.Controls.Add(lblDenominatorModelSigmaCaption, 3, 2);
            tlpData.Controls.Add(lblDenominatorModelCaption, 3, 1);
            tlpData.Controls.Add(lblDenominatorModelSigmaValue, 4, 2);
            tlpData.Controls.Add(lblDenominatorModelValue, 4, 1);
            tlpData.Controls.Add(lblDenominatorParamAreaTypeCaption, 3, 0);
            tlpData.Controls.Add(lblDenominatorParamAreaTypeValue, 4, 0);
            tlpData.Controls.Add(lblPanelRefYearSetGroupCaption, 0, 1);
            tlpData.Controls.Add(lblPanelRefYearSetGroupValue, 1, 1);
            tlpData.Controls.Add(lblAllEstimatesConfigurableCaption, 0, 0);
            tlpData.Controls.Add(lblAllEstimatesConfigurableValue, 1, 0);
            tlpData.Dock = DockStyle.Fill;
            tlpData.Location = new System.Drawing.Point(40, 0);
            tlpData.Margin = new Padding(0);
            tlpData.Name = "tlpData";
            tlpData.RowCount = 4;
            tlpData.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tlpData.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tlpData.Size = new System.Drawing.Size(910, 60);
            tlpData.TabIndex = 1;
            // 
            // lblDenominatorForceSyntheticValue
            // 
            lblDenominatorForceSyntheticValue.Dock = DockStyle.Fill;
            lblDenominatorForceSyntheticValue.Location = new System.Drawing.Point(200, 40);
            lblDenominatorForceSyntheticValue.Margin = new Padding(0);
            lblDenominatorForceSyntheticValue.Name = "lblDenominatorForceSyntheticValue";
            lblDenominatorForceSyntheticValue.Size = new System.Drawing.Size(250, 20);
            lblDenominatorForceSyntheticValue.TabIndex = 13;
            lblDenominatorForceSyntheticValue.Text = "lblDenominatorForceSyntheticValue";
            lblDenominatorForceSyntheticValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorForceSyntheticCaption
            // 
            lblDenominatorForceSyntheticCaption.Dock = DockStyle.Fill;
            lblDenominatorForceSyntheticCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDenominatorForceSyntheticCaption.Location = new System.Drawing.Point(0, 40);
            lblDenominatorForceSyntheticCaption.Margin = new Padding(0);
            lblDenominatorForceSyntheticCaption.Name = "lblDenominatorForceSyntheticCaption";
            lblDenominatorForceSyntheticCaption.Size = new System.Drawing.Size(200, 20);
            lblDenominatorForceSyntheticCaption.TabIndex = 12;
            lblDenominatorForceSyntheticCaption.Text = "lblDenominatorForceSyntheticCaption";
            lblDenominatorForceSyntheticCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorParamAreaTypeValue
            // 
            lblDenominatorParamAreaTypeValue.Dock = DockStyle.Fill;
            lblDenominatorParamAreaTypeValue.Location = new System.Drawing.Point(660, 0);
            lblDenominatorParamAreaTypeValue.Margin = new Padding(0);
            lblDenominatorParamAreaTypeValue.Name = "lblDenominatorParamAreaTypeValue";
            lblDenominatorParamAreaTypeValue.Size = new System.Drawing.Size(250, 20);
            lblDenominatorParamAreaTypeValue.TabIndex = 7;
            lblDenominatorParamAreaTypeValue.Text = "lblDenominatorParamAreaTypeValue";
            lblDenominatorParamAreaTypeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorParamAreaTypeCaption
            // 
            lblDenominatorParamAreaTypeCaption.Dock = DockStyle.Fill;
            lblDenominatorParamAreaTypeCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDenominatorParamAreaTypeCaption.Location = new System.Drawing.Point(460, 0);
            lblDenominatorParamAreaTypeCaption.Margin = new Padding(0);
            lblDenominatorParamAreaTypeCaption.Name = "lblDenominatorParamAreaTypeCaption";
            lblDenominatorParamAreaTypeCaption.Size = new System.Drawing.Size(200, 20);
            lblDenominatorParamAreaTypeCaption.TabIndex = 6;
            lblDenominatorParamAreaTypeCaption.Text = "lblDenominatorParamAreaTypeCaption";
            lblDenominatorParamAreaTypeCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupValue
            // 
            lblPanelRefYearSetGroupValue.Dock = DockStyle.Fill;
            lblPanelRefYearSetGroupValue.Location = new System.Drawing.Point(200, 20);
            lblPanelRefYearSetGroupValue.Margin = new Padding(0);
            lblPanelRefYearSetGroupValue.Name = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.Size = new System.Drawing.Size(250, 20);
            lblPanelRefYearSetGroupValue.TabIndex = 1;
            lblPanelRefYearSetGroupValue.Text = "lblPanelRefYearSetGroupValue";
            lblPanelRefYearSetGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPanelRefYearSetGroupCaption
            // 
            lblPanelRefYearSetGroupCaption.Dock = DockStyle.Fill;
            lblPanelRefYearSetGroupCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblPanelRefYearSetGroupCaption.Location = new System.Drawing.Point(0, 20);
            lblPanelRefYearSetGroupCaption.Margin = new Padding(0);
            lblPanelRefYearSetGroupCaption.Name = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.Size = new System.Drawing.Size(200, 20);
            lblPanelRefYearSetGroupCaption.TabIndex = 0;
            lblPanelRefYearSetGroupCaption.Text = "lblPanelRefYearSetGroupCaption";
            lblPanelRefYearSetGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorModelCaption
            // 
            lblDenominatorModelCaption.Dock = DockStyle.Fill;
            lblDenominatorModelCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDenominatorModelCaption.Location = new System.Drawing.Point(460, 20);
            lblDenominatorModelCaption.Margin = new Padding(0);
            lblDenominatorModelCaption.Name = "lblDenominatorModelCaption";
            lblDenominatorModelCaption.Size = new System.Drawing.Size(200, 20);
            lblDenominatorModelCaption.TabIndex = 15;
            lblDenominatorModelCaption.Text = "lblDenominatorModelCaption";
            lblDenominatorModelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorModelValue
            // 
            lblDenominatorModelValue.Dock = DockStyle.Fill;
            lblDenominatorModelValue.Location = new System.Drawing.Point(660, 20);
            lblDenominatorModelValue.Margin = new Padding(0);
            lblDenominatorModelValue.Name = "lblDenominatorModelValue";
            lblDenominatorModelValue.Size = new System.Drawing.Size(250, 20);
            lblDenominatorModelValue.TabIndex = 16;
            lblDenominatorModelValue.Text = "lblDenominatorModelValue";
            lblDenominatorModelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorModelSigmaCaption
            // 
            lblDenominatorModelSigmaCaption.Dock = DockStyle.Fill;
            lblDenominatorModelSigmaCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDenominatorModelSigmaCaption.Location = new System.Drawing.Point(460, 40);
            lblDenominatorModelSigmaCaption.Margin = new Padding(0);
            lblDenominatorModelSigmaCaption.Name = "lblDenominatorModelSigmaCaption";
            lblDenominatorModelSigmaCaption.Size = new System.Drawing.Size(200, 20);
            lblDenominatorModelSigmaCaption.TabIndex = 21;
            lblDenominatorModelSigmaCaption.Text = "lblDenominatorModelSigmaCaption";
            lblDenominatorModelSigmaCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDenominatorModelSigmaValue
            // 
            lblDenominatorModelSigmaValue.Dock = DockStyle.Fill;
            lblDenominatorModelSigmaValue.Location = new System.Drawing.Point(660, 40);
            lblDenominatorModelSigmaValue.Margin = new Padding(0);
            lblDenominatorModelSigmaValue.Name = "lblDenominatorModelSigmaValue";
            lblDenominatorModelSigmaValue.Size = new System.Drawing.Size(250, 20);
            lblDenominatorModelSigmaValue.TabIndex = 22;
            lblDenominatorModelSigmaValue.Text = "lblDenominatorModelSigmaValue";
            lblDenominatorModelSigmaValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdoSelected
            // 
            rdoSelected.AutoSize = true;
            rdoSelected.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            rdoSelected.Dock = DockStyle.Fill;
            rdoSelected.Location = new System.Drawing.Point(0, 0);
            rdoSelected.Margin = new Padding(0);
            rdoSelected.Name = "rdoSelected";
            rdoSelected.Size = new System.Drawing.Size(40, 60);
            rdoSelected.TabIndex = 0;
            rdoSelected.TabStop = true;
            rdoSelected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            rdoSelected.UseVisualStyleBackColor = true;
            // 
            // lblAllEstimatesConfigurableCaption
            // 
            lblAllEstimatesConfigurableCaption.Dock = DockStyle.Fill;
            lblAllEstimatesConfigurableCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAllEstimatesConfigurableCaption.Location = new System.Drawing.Point(0, 0);
            lblAllEstimatesConfigurableCaption.Margin = new Padding(0);
            lblAllEstimatesConfigurableCaption.Name = "lblAllEstimatesConfigurableCaption";
            lblAllEstimatesConfigurableCaption.Size = new System.Drawing.Size(200, 20);
            lblAllEstimatesConfigurableCaption.TabIndex = 23;
            lblAllEstimatesConfigurableCaption.Text = "lblAllEstimatesConfigurableCaption";
            lblAllEstimatesConfigurableCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAllEstimatesConfigurableValue
            // 
            lblAllEstimatesConfigurableValue.Dock = DockStyle.Fill;
            lblAllEstimatesConfigurableValue.Location = new System.Drawing.Point(200, 0);
            lblAllEstimatesConfigurableValue.Margin = new Padding(0);
            lblAllEstimatesConfigurableValue.Name = "lblAllEstimatesConfigurableValue";
            lblAllEstimatesConfigurableValue.Size = new System.Drawing.Size(250, 20);
            lblAllEstimatesConfigurableValue.TabIndex = 24;
            lblAllEstimatesConfigurableValue.Text = "lblAllEstimatesConfigurableValue";
            lblAllEstimatesConfigurableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlSingleToGRegMapConfigsForRatio
            // 
            AutoScaleMode = AutoScaleMode.None;
            BackColor = System.Drawing.Color.Silver;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new Padding(0);
            Name = "ControlSingleToGRegMapConfigsForRatio";
            Padding = new Padding(5);
            Size = new System.Drawing.Size(960, 70);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            tlpData.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tlpMain;
        private TableLayoutPanel tlpData;
        private Label lblDenominatorModelSigmaValue;
        private Label lblDenominatorModelSigmaCaption;
        private Label lblDenominatorModelValue;
        private Label lblDenominatorModelCaption;
        private Label lblDenominatorForceSyntheticValue;
        private Label lblDenominatorForceSyntheticCaption;
        private Label lblDenominatorParamAreaTypeValue;
        private Label lblDenominatorParamAreaTypeCaption;
        private Label lblPanelRefYearSetGroupValue;
        private Label lblPanelRefYearSetGroupCaption;
        private RadioButton rdoSelected;
        private Label lblAllEstimatesConfigurableCaption;
        private Label lblAllEstimatesConfigurableValue;
    }

}