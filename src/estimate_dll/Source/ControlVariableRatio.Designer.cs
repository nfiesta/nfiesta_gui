﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlVariableRatio
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlPrevious = new System.Windows.Forms.Panel();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.pnlNext = new System.Windows.Forms.Panel();
            this.btnNext = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splVariableRatio = new System.Windows.Forms.SplitContainer();
            this.splNumeratorDenominator = new System.Windows.Forms.SplitContainer();
            this.grpNumerator = new System.Windows.Forms.GroupBox();
            this.splNumerator = new System.Windows.Forms.SplitContainer();
            this.pnlNumeratorCaption = new System.Windows.Forms.Panel();
            this.tlpNumerator = new System.Windows.Forms.TableLayoutPanel();
            this.splNumeratorAreaDomain = new System.Windows.Forms.SplitContainer();
            this.grpNumeratorAreaDomain = new System.Windows.Forms.GroupBox();
            this.cboNumeratorAreaDomain = new System.Windows.Forms.ComboBox();
            this.splNumeratorSubPopulation = new System.Windows.Forms.SplitContainer();
            this.grpNumeratorSubPopulation = new System.Windows.Forms.GroupBox();
            this.cboNumeratorSubPopulation = new System.Windows.Forms.ComboBox();
            this.grpDenominator = new System.Windows.Forms.GroupBox();
            this.splDenominator = new System.Windows.Forms.SplitContainer();
            this.pnlDenominatorCaption = new System.Windows.Forms.Panel();
            this.tlpDenominator = new System.Windows.Forms.TableLayoutPanel();
            this.splDenominatorSubPopulation = new System.Windows.Forms.SplitContainer();
            this.grpDenominatorSubPopulation = new System.Windows.Forms.GroupBox();
            this.cboDenominatorSubPopulation = new System.Windows.Forms.ComboBox();
            this.splDenominatorAreaDomain = new System.Windows.Forms.SplitContainer();
            this.grpDenominatorAreaDomain = new System.Windows.Forms.GroupBox();
            this.cboDenominatorAreaDomain = new System.Windows.Forms.ComboBox();
            this.pnlAttributeCategories = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlPrevious.SuspendLayout();
            this.pnlNext.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splVariableRatio)).BeginInit();
            this.splVariableRatio.Panel1.SuspendLayout();
            this.splVariableRatio.Panel2.SuspendLayout();
            this.splVariableRatio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splNumeratorDenominator)).BeginInit();
            this.splNumeratorDenominator.Panel1.SuspendLayout();
            this.splNumeratorDenominator.Panel2.SuspendLayout();
            this.splNumeratorDenominator.SuspendLayout();
            this.grpNumerator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splNumerator)).BeginInit();
            this.splNumerator.Panel1.SuspendLayout();
            this.splNumerator.Panel2.SuspendLayout();
            this.splNumerator.SuspendLayout();
            this.tlpNumerator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splNumeratorAreaDomain)).BeginInit();
            this.splNumeratorAreaDomain.Panel1.SuspendLayout();
            this.splNumeratorAreaDomain.SuspendLayout();
            this.grpNumeratorAreaDomain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splNumeratorSubPopulation)).BeginInit();
            this.splNumeratorSubPopulation.Panel1.SuspendLayout();
            this.splNumeratorSubPopulation.SuspendLayout();
            this.grpNumeratorSubPopulation.SuspendLayout();
            this.grpDenominator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splDenominator)).BeginInit();
            this.splDenominator.Panel1.SuspendLayout();
            this.splDenominator.Panel2.SuspendLayout();
            this.splDenominator.SuspendLayout();
            this.tlpDenominator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splDenominatorSubPopulation)).BeginInit();
            this.splDenominatorSubPopulation.Panel1.SuspendLayout();
            this.splDenominatorSubPopulation.SuspendLayout();
            this.grpDenominatorSubPopulation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splDenominatorAreaDomain)).BeginInit();
            this.splDenominatorAreaDomain.Panel1.SuspendLayout();
            this.splDenominatorAreaDomain.SuspendLayout();
            this.grpDenominatorAreaDomain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 1;
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.SystemColors.Control;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 1);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(3, 3);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.Size = new System.Drawing.Size(954, 534);
            this.tlpMain.TabIndex = 2;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.Controls.Add(this.pnlPrevious, 1, 0);
            this.tlpButtons.Controls.Add(this.pnlNext, 2, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 494);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(954, 40);
            this.tlpButtons.TabIndex = 13;
            // 
            // pnlPrevious
            // 
            this.pnlPrevious.Controls.Add(this.btnPrevious);
            this.pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrevious.Location = new System.Drawing.Point(634, 0);
            this.pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPrevious.Name = "pnlPrevious";
            this.pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPrevious.Size = new System.Drawing.Size(160, 40);
            this.pnlPrevious.TabIndex = 17;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrevious.Location = new System.Drawing.Point(5, 5);
            this.btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(150, 30);
            this.btnPrevious.TabIndex = 15;
            this.btnPrevious.Text = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlNext
            // 
            this.pnlNext.Controls.Add(this.btnNext);
            this.pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNext.Location = new System.Drawing.Point(794, 0);
            this.pnlNext.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNext.Name = "pnlNext";
            this.pnlNext.Padding = new System.Windows.Forms.Padding(5);
            this.pnlNext.Size = new System.Drawing.Size(160, 40);
            this.pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnNext.Location = new System.Drawing.Point(5, 5);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 15;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlWorkSpace.Controls.Add(this.splVariableRatio);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 494);
            this.pnlWorkSpace.TabIndex = 5;
            // 
            // splVariableRatio
            // 
            this.splVariableRatio.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splVariableRatio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splVariableRatio.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splVariableRatio.Location = new System.Drawing.Point(0, 0);
            this.splVariableRatio.Margin = new System.Windows.Forms.Padding(0);
            this.splVariableRatio.Name = "splVariableRatio";
            this.splVariableRatio.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splVariableRatio.Panel1
            // 
            this.splVariableRatio.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splVariableRatio.Panel1.Controls.Add(this.splNumeratorDenominator);
            this.splVariableRatio.Panel1MinSize = 0;
            // 
            // splVariableRatio.Panel2
            // 
            this.splVariableRatio.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splVariableRatio.Panel2.Controls.Add(this.pnlAttributeCategories);
            this.splVariableRatio.Panel2MinSize = 0;
            this.splVariableRatio.Size = new System.Drawing.Size(954, 491);
            this.splVariableRatio.SplitterDistance = 340;
            this.splVariableRatio.SplitterWidth = 1;
            this.splVariableRatio.TabIndex = 6;
            // 
            // splNumeratorDenominator
            // 
            this.splNumeratorDenominator.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splNumeratorDenominator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splNumeratorDenominator.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splNumeratorDenominator.Location = new System.Drawing.Point(0, 0);
            this.splNumeratorDenominator.Margin = new System.Windows.Forms.Padding(0);
            this.splNumeratorDenominator.Name = "splNumeratorDenominator";
            this.splNumeratorDenominator.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splNumeratorDenominator.Panel1
            // 
            this.splNumeratorDenominator.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splNumeratorDenominator.Panel1.Controls.Add(this.grpNumerator);
            this.splNumeratorDenominator.Panel1MinSize = 0;
            // 
            // splNumeratorDenominator.Panel2
            // 
            this.splNumeratorDenominator.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splNumeratorDenominator.Panel2.Controls.Add(this.grpDenominator);
            this.splNumeratorDenominator.Panel2MinSize = 0;
            this.splNumeratorDenominator.Size = new System.Drawing.Size(954, 340);
            this.splNumeratorDenominator.SplitterDistance = 170;
            this.splNumeratorDenominator.SplitterWidth = 1;
            this.splNumeratorDenominator.TabIndex = 0;
            // 
            // grpNumerator
            // 
            this.grpNumerator.Controls.Add(this.splNumerator);
            this.grpNumerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpNumerator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpNumerator.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpNumerator.Location = new System.Drawing.Point(0, 0);
            this.grpNumerator.Margin = new System.Windows.Forms.Padding(5);
            this.grpNumerator.Name = "grpNumerator";
            this.grpNumerator.Padding = new System.Windows.Forms.Padding(5);
            this.grpNumerator.Size = new System.Drawing.Size(954, 170);
            this.grpNumerator.TabIndex = 7;
            this.grpNumerator.TabStop = false;
            this.grpNumerator.Text = "grpNumerator";
            // 
            // splNumerator
            // 
            this.splNumerator.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splNumerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splNumerator.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splNumerator.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splNumerator.Location = new System.Drawing.Point(5, 20);
            this.splNumerator.Margin = new System.Windows.Forms.Padding(0);
            this.splNumerator.Name = "splNumerator";
            this.splNumerator.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splNumerator.Panel1
            // 
            this.splNumerator.Panel1.Controls.Add(this.pnlNumeratorCaption);
            this.splNumerator.Panel1MinSize = 0;
            // 
            // splNumerator.Panel2
            // 
            this.splNumerator.Panel2.Controls.Add(this.tlpNumerator);
            this.splNumerator.Panel2MinSize = 0;
            this.splNumerator.Size = new System.Drawing.Size(944, 145);
            this.splNumerator.SplitterDistance = 75;
            this.splNumerator.SplitterWidth = 1;
            this.splNumerator.TabIndex = 1;
            // 
            // pnlNumeratorCaption
            // 
            this.pnlNumeratorCaption.BackColor = System.Drawing.SystemColors.Control;
            this.pnlNumeratorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNumeratorCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlNumeratorCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlNumeratorCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlNumeratorCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNumeratorCaption.Name = "pnlNumeratorCaption";
            this.pnlNumeratorCaption.Size = new System.Drawing.Size(944, 75);
            this.pnlNumeratorCaption.TabIndex = 9;
            // 
            // tlpNumerator
            // 
            this.tlpNumerator.BackColor = System.Drawing.SystemColors.Control;
            this.tlpNumerator.ColumnCount = 2;
            this.tlpNumerator.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpNumerator.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpNumerator.Controls.Add(this.splNumeratorAreaDomain, 0, 0);
            this.tlpNumerator.Controls.Add(this.splNumeratorSubPopulation, 1, 0);
            this.tlpNumerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpNumerator.Location = new System.Drawing.Point(0, 0);
            this.tlpNumerator.Margin = new System.Windows.Forms.Padding(0);
            this.tlpNumerator.Name = "tlpNumerator";
            this.tlpNumerator.RowCount = 1;
            this.tlpNumerator.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpNumerator.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpNumerator.Size = new System.Drawing.Size(944, 69);
            this.tlpNumerator.TabIndex = 8;
            // 
            // splNumeratorAreaDomain
            // 
            this.splNumeratorAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splNumeratorAreaDomain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splNumeratorAreaDomain.IsSplitterFixed = true;
            this.splNumeratorAreaDomain.Location = new System.Drawing.Point(0, 0);
            this.splNumeratorAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            this.splNumeratorAreaDomain.Name = "splNumeratorAreaDomain";
            this.splNumeratorAreaDomain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splNumeratorAreaDomain.Panel1
            // 
            this.splNumeratorAreaDomain.Panel1.Controls.Add(this.grpNumeratorAreaDomain);
            this.splNumeratorAreaDomain.Panel1.Padding = new System.Windows.Forms.Padding(5);
            this.splNumeratorAreaDomain.Panel2Collapsed = true;
            this.splNumeratorAreaDomain.Size = new System.Drawing.Size(472, 69);
            this.splNumeratorAreaDomain.SplitterDistance = 30;
            this.splNumeratorAreaDomain.TabIndex = 2;
            // 
            // grpNumeratorAreaDomain
            // 
            this.grpNumeratorAreaDomain.Controls.Add(this.cboNumeratorAreaDomain);
            this.grpNumeratorAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpNumeratorAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpNumeratorAreaDomain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpNumeratorAreaDomain.Location = new System.Drawing.Point(5, 5);
            this.grpNumeratorAreaDomain.Margin = new System.Windows.Forms.Padding(5);
            this.grpNumeratorAreaDomain.Name = "grpNumeratorAreaDomain";
            this.grpNumeratorAreaDomain.Padding = new System.Windows.Forms.Padding(5);
            this.grpNumeratorAreaDomain.Size = new System.Drawing.Size(462, 59);
            this.grpNumeratorAreaDomain.TabIndex = 5;
            this.grpNumeratorAreaDomain.TabStop = false;
            this.grpNumeratorAreaDomain.Text = "grpNumeratorAreaDomain";
            // 
            // cboNumeratorAreaDomain
            // 
            this.cboNumeratorAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboNumeratorAreaDomain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNumeratorAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cboNumeratorAreaDomain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboNumeratorAreaDomain.FormattingEnabled = true;
            this.cboNumeratorAreaDomain.Location = new System.Drawing.Point(5, 20);
            this.cboNumeratorAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            this.cboNumeratorAreaDomain.Name = "cboNumeratorAreaDomain";
            this.cboNumeratorAreaDomain.Size = new System.Drawing.Size(452, 24);
            this.cboNumeratorAreaDomain.TabIndex = 0;
            // 
            // splNumeratorSubPopulation
            // 
            this.splNumeratorSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splNumeratorSubPopulation.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splNumeratorSubPopulation.IsSplitterFixed = true;
            this.splNumeratorSubPopulation.Location = new System.Drawing.Point(472, 0);
            this.splNumeratorSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            this.splNumeratorSubPopulation.Name = "splNumeratorSubPopulation";
            this.splNumeratorSubPopulation.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splNumeratorSubPopulation.Panel1
            // 
            this.splNumeratorSubPopulation.Panel1.Controls.Add(this.grpNumeratorSubPopulation);
            this.splNumeratorSubPopulation.Panel1.Padding = new System.Windows.Forms.Padding(5);
            this.splNumeratorSubPopulation.Panel2Collapsed = true;
            this.splNumeratorSubPopulation.Size = new System.Drawing.Size(472, 69);
            this.splNumeratorSubPopulation.SplitterDistance = 26;
            this.splNumeratorSubPopulation.TabIndex = 1;
            // 
            // grpNumeratorSubPopulation
            // 
            this.grpNumeratorSubPopulation.Controls.Add(this.cboNumeratorSubPopulation);
            this.grpNumeratorSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpNumeratorSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpNumeratorSubPopulation.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpNumeratorSubPopulation.Location = new System.Drawing.Point(5, 5);
            this.grpNumeratorSubPopulation.Margin = new System.Windows.Forms.Padding(5);
            this.grpNumeratorSubPopulation.Name = "grpNumeratorSubPopulation";
            this.grpNumeratorSubPopulation.Padding = new System.Windows.Forms.Padding(5);
            this.grpNumeratorSubPopulation.Size = new System.Drawing.Size(462, 59);
            this.grpNumeratorSubPopulation.TabIndex = 6;
            this.grpNumeratorSubPopulation.TabStop = false;
            this.grpNumeratorSubPopulation.Text = "grpNumeratorSubPopulation";
            // 
            // cboNumeratorSubPopulation
            // 
            this.cboNumeratorSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboNumeratorSubPopulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNumeratorSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cboNumeratorSubPopulation.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboNumeratorSubPopulation.FormattingEnabled = true;
            this.cboNumeratorSubPopulation.Location = new System.Drawing.Point(5, 20);
            this.cboNumeratorSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            this.cboNumeratorSubPopulation.Name = "cboNumeratorSubPopulation";
            this.cboNumeratorSubPopulation.Size = new System.Drawing.Size(452, 24);
            this.cboNumeratorSubPopulation.TabIndex = 1;
            // 
            // grpDenominator
            // 
            this.grpDenominator.Controls.Add(this.splDenominator);
            this.grpDenominator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDenominator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpDenominator.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDenominator.Location = new System.Drawing.Point(0, 0);
            this.grpDenominator.Margin = new System.Windows.Forms.Padding(5);
            this.grpDenominator.Name = "grpDenominator";
            this.grpDenominator.Padding = new System.Windows.Forms.Padding(5);
            this.grpDenominator.Size = new System.Drawing.Size(954, 169);
            this.grpDenominator.TabIndex = 5;
            this.grpDenominator.TabStop = false;
            this.grpDenominator.Text = "grpDenominator";
            // 
            // splDenominator
            // 
            this.splDenominator.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splDenominator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splDenominator.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splDenominator.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splDenominator.Location = new System.Drawing.Point(5, 20);
            this.splDenominator.Margin = new System.Windows.Forms.Padding(0);
            this.splDenominator.Name = "splDenominator";
            this.splDenominator.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splDenominator.Panel1
            // 
            this.splDenominator.Panel1.Controls.Add(this.pnlDenominatorCaption);
            this.splDenominator.Panel1MinSize = 0;
            // 
            // splDenominator.Panel2
            // 
            this.splDenominator.Panel2.Controls.Add(this.tlpDenominator);
            this.splDenominator.Panel2MinSize = 0;
            this.splDenominator.Size = new System.Drawing.Size(944, 144);
            this.splDenominator.SplitterDistance = 75;
            this.splDenominator.SplitterWidth = 1;
            this.splDenominator.TabIndex = 1;
            // 
            // pnlDenominatorCaption
            // 
            this.pnlDenominatorCaption.BackColor = System.Drawing.SystemColors.Control;
            this.pnlDenominatorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDenominatorCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlDenominatorCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlDenominatorCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlDenominatorCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlDenominatorCaption.Name = "pnlDenominatorCaption";
            this.pnlDenominatorCaption.Size = new System.Drawing.Size(944, 75);
            this.pnlDenominatorCaption.TabIndex = 9;
            // 
            // tlpDenominator
            // 
            this.tlpDenominator.BackColor = System.Drawing.SystemColors.Control;
            this.tlpDenominator.ColumnCount = 2;
            this.tlpDenominator.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDenominator.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDenominator.Controls.Add(this.splDenominatorSubPopulation, 0, 0);
            this.tlpDenominator.Controls.Add(this.splDenominatorAreaDomain, 0, 0);
            this.tlpDenominator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDenominator.Location = new System.Drawing.Point(0, 0);
            this.tlpDenominator.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDenominator.Name = "tlpDenominator";
            this.tlpDenominator.RowCount = 1;
            this.tlpDenominator.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDenominator.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDenominator.Size = new System.Drawing.Size(944, 68);
            this.tlpDenominator.TabIndex = 8;
            // 
            // splDenominatorSubPopulation
            // 
            this.splDenominatorSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splDenominatorSubPopulation.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splDenominatorSubPopulation.IsSplitterFixed = true;
            this.splDenominatorSubPopulation.Location = new System.Drawing.Point(472, 0);
            this.splDenominatorSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            this.splDenominatorSubPopulation.Name = "splDenominatorSubPopulation";
            this.splDenominatorSubPopulation.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splDenominatorSubPopulation.Panel1
            // 
            this.splDenominatorSubPopulation.Panel1.Controls.Add(this.grpDenominatorSubPopulation);
            this.splDenominatorSubPopulation.Panel1.Padding = new System.Windows.Forms.Padding(5);
            this.splDenominatorSubPopulation.Panel2Collapsed = true;
            this.splDenominatorSubPopulation.Size = new System.Drawing.Size(472, 68);
            this.splDenominatorSubPopulation.SplitterDistance = 30;
            this.splDenominatorSubPopulation.SplitterWidth = 1;
            this.splDenominatorSubPopulation.TabIndex = 2;
            // 
            // grpDenominatorSubPopulation
            // 
            this.grpDenominatorSubPopulation.Controls.Add(this.cboDenominatorSubPopulation);
            this.grpDenominatorSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDenominatorSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDenominatorSubPopulation.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDenominatorSubPopulation.Location = new System.Drawing.Point(5, 5);
            this.grpDenominatorSubPopulation.Margin = new System.Windows.Forms.Padding(5);
            this.grpDenominatorSubPopulation.Name = "grpDenominatorSubPopulation";
            this.grpDenominatorSubPopulation.Padding = new System.Windows.Forms.Padding(5);
            this.grpDenominatorSubPopulation.Size = new System.Drawing.Size(462, 58);
            this.grpDenominatorSubPopulation.TabIndex = 4;
            this.grpDenominatorSubPopulation.TabStop = false;
            this.grpDenominatorSubPopulation.Text = "grpDenominatorSubPopulation";
            // 
            // cboDenominatorSubPopulation
            // 
            this.cboDenominatorSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboDenominatorSubPopulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDenominatorSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cboDenominatorSubPopulation.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboDenominatorSubPopulation.FormattingEnabled = true;
            this.cboDenominatorSubPopulation.Location = new System.Drawing.Point(5, 20);
            this.cboDenominatorSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            this.cboDenominatorSubPopulation.Name = "cboDenominatorSubPopulation";
            this.cboDenominatorSubPopulation.Size = new System.Drawing.Size(452, 24);
            this.cboDenominatorSubPopulation.TabIndex = 1;
            // 
            // splDenominatorAreaDomain
            // 
            this.splDenominatorAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splDenominatorAreaDomain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splDenominatorAreaDomain.IsSplitterFixed = true;
            this.splDenominatorAreaDomain.Location = new System.Drawing.Point(0, 0);
            this.splDenominatorAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            this.splDenominatorAreaDomain.Name = "splDenominatorAreaDomain";
            this.splDenominatorAreaDomain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splDenominatorAreaDomain.Panel1
            // 
            this.splDenominatorAreaDomain.Panel1.Controls.Add(this.grpDenominatorAreaDomain);
            this.splDenominatorAreaDomain.Panel1.Padding = new System.Windows.Forms.Padding(5);
            this.splDenominatorAreaDomain.Panel2Collapsed = true;
            this.splDenominatorAreaDomain.Size = new System.Drawing.Size(472, 68);
            this.splDenominatorAreaDomain.SplitterDistance = 30;
            this.splDenominatorAreaDomain.SplitterWidth = 1;
            this.splDenominatorAreaDomain.TabIndex = 1;
            // 
            // grpDenominatorAreaDomain
            // 
            this.grpDenominatorAreaDomain.Controls.Add(this.cboDenominatorAreaDomain);
            this.grpDenominatorAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDenominatorAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDenominatorAreaDomain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDenominatorAreaDomain.Location = new System.Drawing.Point(5, 5);
            this.grpDenominatorAreaDomain.Margin = new System.Windows.Forms.Padding(5);
            this.grpDenominatorAreaDomain.Name = "grpDenominatorAreaDomain";
            this.grpDenominatorAreaDomain.Padding = new System.Windows.Forms.Padding(5);
            this.grpDenominatorAreaDomain.Size = new System.Drawing.Size(462, 58);
            this.grpDenominatorAreaDomain.TabIndex = 2;
            this.grpDenominatorAreaDomain.TabStop = false;
            this.grpDenominatorAreaDomain.Text = "grpDenominatorAreaDomain";
            // 
            // cboDenominatorAreaDomain
            // 
            this.cboDenominatorAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboDenominatorAreaDomain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDenominatorAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cboDenominatorAreaDomain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboDenominatorAreaDomain.FormattingEnabled = true;
            this.cboDenominatorAreaDomain.Location = new System.Drawing.Point(5, 20);
            this.cboDenominatorAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            this.cboDenominatorAreaDomain.Name = "cboDenominatorAreaDomain";
            this.cboDenominatorAreaDomain.Size = new System.Drawing.Size(452, 24);
            this.cboDenominatorAreaDomain.TabIndex = 0;
            // 
            // pnlAttributeCategories
            // 
            this.pnlAttributeCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAttributeCategories.Location = new System.Drawing.Point(0, 0);
            this.pnlAttributeCategories.Margin = new System.Windows.Forms.Padding(0);
            this.pnlAttributeCategories.Name = "pnlAttributeCategories";
            this.pnlAttributeCategories.Size = new System.Drawing.Size(954, 150);
            this.pnlAttributeCategories.TabIndex = 4;
            // 
            // ControlVariableRatio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlVariableRatio";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlPrevious.ResumeLayout(false);
            this.pnlNext.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splVariableRatio.Panel1.ResumeLayout(false);
            this.splVariableRatio.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splVariableRatio)).EndInit();
            this.splVariableRatio.ResumeLayout(false);
            this.splNumeratorDenominator.Panel1.ResumeLayout(false);
            this.splNumeratorDenominator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splNumeratorDenominator)).EndInit();
            this.splNumeratorDenominator.ResumeLayout(false);
            this.grpNumerator.ResumeLayout(false);
            this.splNumerator.Panel1.ResumeLayout(false);
            this.splNumerator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splNumerator)).EndInit();
            this.splNumerator.ResumeLayout(false);
            this.tlpNumerator.ResumeLayout(false);
            this.splNumeratorAreaDomain.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splNumeratorAreaDomain)).EndInit();
            this.splNumeratorAreaDomain.ResumeLayout(false);
            this.grpNumeratorAreaDomain.ResumeLayout(false);
            this.splNumeratorSubPopulation.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splNumeratorSubPopulation)).EndInit();
            this.splNumeratorSubPopulation.ResumeLayout(false);
            this.grpNumeratorSubPopulation.ResumeLayout(false);
            this.grpDenominator.ResumeLayout(false);
            this.splDenominator.Panel1.ResumeLayout(false);
            this.splDenominator.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splDenominator)).EndInit();
            this.splDenominator.ResumeLayout(false);
            this.tlpDenominator.ResumeLayout(false);
            this.splDenominatorSubPopulation.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splDenominatorSubPopulation)).EndInit();
            this.splDenominatorSubPopulation.ResumeLayout(false);
            this.grpDenominatorSubPopulation.ResumeLayout(false);
            this.splDenominatorAreaDomain.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splDenominatorAreaDomain)).EndInit();
            this.splDenominatorAreaDomain.ResumeLayout(false);
            this.grpDenominatorAreaDomain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splVariableRatio;
        private System.Windows.Forms.SplitContainer splNumeratorDenominator;
        private System.Windows.Forms.GroupBox grpNumerator;
        private System.Windows.Forms.GroupBox grpDenominator;
        private System.Windows.Forms.Panel pnlAttributeCategories;
        private System.Windows.Forms.SplitContainer splNumerator;
        private System.Windows.Forms.Panel pnlNumeratorCaption;
        private System.Windows.Forms.TableLayoutPanel tlpNumerator;
        private System.Windows.Forms.SplitContainer splNumeratorAreaDomain;
        private System.Windows.Forms.GroupBox grpNumeratorAreaDomain;
        private System.Windows.Forms.ComboBox cboNumeratorAreaDomain;
        private System.Windows.Forms.SplitContainer splNumeratorSubPopulation;
        private System.Windows.Forms.GroupBox grpNumeratorSubPopulation;
        private System.Windows.Forms.ComboBox cboNumeratorSubPopulation;
        private System.Windows.Forms.SplitContainer splDenominator;
        private System.Windows.Forms.Panel pnlDenominatorCaption;
        private System.Windows.Forms.TableLayoutPanel tlpDenominator;
        private System.Windows.Forms.SplitContainer splDenominatorSubPopulation;
        private System.Windows.Forms.GroupBox grpDenominatorSubPopulation;
        private System.Windows.Forms.ComboBox cboDenominatorSubPopulation;
        private System.Windows.Forms.SplitContainer splDenominatorAreaDomain;
        private System.Windows.Forms.GroupBox grpDenominatorAreaDomain;
        private System.Windows.Forms.ComboBox cboDenominatorAreaDomain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
    }

}
