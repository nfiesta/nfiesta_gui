﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlEstimate
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpThreadButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlStop = new System.Windows.Forms.Panel();
            btnStop = new System.Windows.Forms.Button();
            pnlMain = new System.Windows.Forms.Panel();
            pnlControls = new System.Windows.Forms.Panel();
            ssrMain = new System.Windows.Forms.StatusStrip();
            lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            lblNfiestaExtension = new System.Windows.Forms.ToolStripStatusLabel();
            lblSDesignExtension = new System.Windows.Forms.ToolStripStatusLabel();
            mnsMain = new System.Windows.Forms.MenuStrip();
            tsmiDatabase = new System.Windows.Forms.ToolStripMenuItem();
            tsmiConnect = new System.Windows.Forms.ToolStripMenuItem();
            tsmiSeparator = new System.Windows.Forms.ToolStripSeparator();
            tsmiSDesignExtension = new System.Windows.Forms.ToolStripMenuItem();
            tsmiSResultsExtension = new System.Windows.Forms.ToolStripMenuItem();
            tsmiNfiEstaExtension = new System.Windows.Forms.ToolStripMenuItem();
            tlpMain.SuspendLayout();
            tlpThreadButtons.SuspendLayout();
            pnlStop.SuspendLayout();
            pnlMain.SuspendLayout();
            ssrMain.SuspendLayout();
            mnsMain.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.Controls.Add(tlpThreadButtons, 0, 1);
            tlpMain.Controls.Add(pnlMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(2, 2);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(956, 536);
            tlpMain.TabIndex = 6;
            // 
            // tlpThreadButtons
            // 
            tlpThreadButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpThreadButtons.ColumnCount = 3;
            tlpThreadButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpThreadButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tlpThreadButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpThreadButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpThreadButtons.Controls.Add(pnlStop, 1, 0);
            tlpThreadButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpThreadButtons.Location = new System.Drawing.Point(0, 490);
            tlpThreadButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpThreadButtons.Name = "tlpThreadButtons";
            tlpThreadButtons.RowCount = 1;
            tlpThreadButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpThreadButtons.Size = new System.Drawing.Size(956, 46);
            tlpThreadButtons.TabIndex = 13;
            // 
            // pnlStop
            // 
            pnlStop.Controls.Add(btnStop);
            pnlStop.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlStop.Location = new System.Drawing.Point(378, 0);
            pnlStop.Margin = new System.Windows.Forms.Padding(0);
            pnlStop.Name = "pnlStop";
            pnlStop.Padding = new System.Windows.Forms.Padding(6);
            pnlStop.Size = new System.Drawing.Size(200, 46);
            pnlStop.TabIndex = 16;
            // 
            // btnStop
            // 
            btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
            btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnStop.Location = new System.Drawing.Point(6, 6);
            btnStop.Margin = new System.Windows.Forms.Padding(0);
            btnStop.Name = "btnStop";
            btnStop.Size = new System.Drawing.Size(188, 34);
            btnStop.TabIndex = 15;
            btnStop.Text = "btnStop";
            btnStop.UseVisualStyleBackColor = true;
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.SystemColors.Control;
            pnlMain.Controls.Add(pnlControls);
            pnlMain.Controls.Add(ssrMain);
            pnlMain.Controls.Add(mnsMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(956, 490);
            pnlMain.TabIndex = 6;
            // 
            // pnlControls
            // 
            pnlControls.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlControls.Location = new System.Drawing.Point(0, 24);
            pnlControls.Margin = new System.Windows.Forms.Padding(0);
            pnlControls.Name = "pnlControls";
            pnlControls.Size = new System.Drawing.Size(956, 391);
            pnlControls.TabIndex = 4;
            // 
            // ssrMain
            // 
            ssrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { lblStatus, lblNfiestaExtension, lblSDesignExtension });
            ssrMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            ssrMain.Location = new System.Drawing.Point(0, 415);
            ssrMain.Name = "ssrMain";
            ssrMain.Padding = new System.Windows.Forms.Padding(1, 3, 1, 25);
            ssrMain.Size = new System.Drawing.Size(956, 75);
            ssrMain.TabIndex = 2;
            ssrMain.Text = "StatusMain";
            // 
            // lblStatus
            // 
            lblStatus.Margin = new System.Windows.Forms.Padding(0);
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new System.Drawing.Size(953, 15);
            lblStatus.Text = "lblStatus";
            // 
            // lblNfiestaExtension
            // 
            lblNfiestaExtension.Margin = new System.Windows.Forms.Padding(0);
            lblNfiestaExtension.Name = "lblNfiestaExtension";
            lblNfiestaExtension.Size = new System.Drawing.Size(953, 15);
            lblNfiestaExtension.Text = "lblNfiestaExtension";
            // 
            // lblSDesignExtension
            // 
            lblSDesignExtension.Margin = new System.Windows.Forms.Padding(0);
            lblSDesignExtension.Name = "lblSDesignExtension";
            lblSDesignExtension.Size = new System.Drawing.Size(953, 15);
            lblSDesignExtension.Text = "lblSDesignExtension";
            // 
            // mnsMain
            // 
            mnsMain.BackColor = System.Drawing.Color.Transparent;
            mnsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiDatabase });
            mnsMain.Location = new System.Drawing.Point(0, 0);
            mnsMain.Name = "mnsMain";
            mnsMain.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            mnsMain.Size = new System.Drawing.Size(956, 24);
            mnsMain.TabIndex = 1;
            mnsMain.Text = "MenuMain";
            // 
            // tsmiDatabase
            // 
            tsmiDatabase.BackColor = System.Drawing.Color.Transparent;
            tsmiDatabase.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiConnect, tsmiSeparator, tsmiSDesignExtension, tsmiSResultsExtension, tsmiNfiEstaExtension });
            tsmiDatabase.Name = "tsmiDatabase";
            tsmiDatabase.Padding = new System.Windows.Forms.Padding(0);
            tsmiDatabase.Size = new System.Drawing.Size(82, 24);
            tsmiDatabase.Text = "tsmiDatabase";
            // 
            // tsmiConnect
            // 
            tsmiConnect.Name = "tsmiConnect";
            tsmiConnect.Size = new System.Drawing.Size(191, 22);
            tsmiConnect.Text = "tsmiConnect";
            // 
            // tsmiSeparator
            // 
            tsmiSeparator.Name = "tsmiSeparator";
            tsmiSeparator.Size = new System.Drawing.Size(188, 6);
            tsmiSeparator.Visible = false;
            // 
            // tsmiSDesignExtension
            // 
            tsmiSDesignExtension.Name = "tsmiSDesignExtension";
            tsmiSDesignExtension.Size = new System.Drawing.Size(191, 22);
            tsmiSDesignExtension.Text = "tsmiSDesignExtension";
            tsmiSDesignExtension.Visible = false;
            // 
            // tsmiSResultsExtension
            // 
            tsmiSResultsExtension.Name = "tsmiSResultsExtension";
            tsmiSResultsExtension.Size = new System.Drawing.Size(191, 22);
            tsmiSResultsExtension.Text = "tsmiSResultsExtension";
            // 
            // tsmiNfiEstaExtension
            // 
            tsmiNfiEstaExtension.Name = "tsmiNfiEstaExtension";
            tsmiNfiEstaExtension.Size = new System.Drawing.Size(191, 22);
            tsmiNfiEstaExtension.Text = "tsmiNfiEstaExtension";
            tsmiNfiEstaExtension.Visible = false;
            // 
            // ControlEstimate
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.DarkSeaGreen;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlEstimate";
            Padding = new System.Windows.Forms.Padding(2);
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            tlpThreadButtons.ResumeLayout(false);
            pnlStop.ResumeLayout(false);
            pnlMain.ResumeLayout(false);
            pnlMain.PerformLayout();
            ssrMain.ResumeLayout(false);
            ssrMain.PerformLayout();
            mnsMain.ResumeLayout(false);
            mnsMain.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpThreadButtons;
        private System.Windows.Forms.Panel pnlStop;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.StatusStrip ssrMain;
        private System.Windows.Forms.MenuStrip mnsMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiDatabase;
        private System.Windows.Forms.ToolStripMenuItem tsmiConnect;
        private System.Windows.Forms.ToolStripSeparator tsmiSeparator;
        private System.Windows.Forms.ToolStripMenuItem tsmiSDesignExtension;
        private System.Windows.Forms.ToolStripMenuItem tsmiNfiEstaExtension;

        /// <summary>
        /// <para lang="cs">Stav připojení k databázi</para>            
        /// <para lang="en">Database connection status</para>
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblStatus;

        /// <summary>
        /// <para lang="cs">Verze extenze nfiesta</para>            
        /// <para lang="en">Database extension nfiesta version</para>
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblNfiestaExtension;

        /// <summary>
        /// <para lang="cs">Verze extenze sdesing</para>            
        /// <para lang="en">Database extension sdesing version</para>
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblSDesignExtension;
        private System.Windows.Forms.ToolStripMenuItem tsmiSResultsExtension;
    }

}
