﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlCategoryListDesign
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.grpCategory = new System.Windows.Forms.GroupBox();
            this.tlpCategory = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCategory = new System.Windows.Forms.Panel();
            this.lblCount = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.grpCategory.SuspendLayout();
            this.tlpCategory.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.grpCategory);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(5, 5);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(140, 140);
            this.pnlMain.TabIndex = 0;
            // 
            // grpCategory
            // 
            this.grpCategory.Controls.Add(this.tlpCategory);
            this.grpCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCategory.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpCategory.Location = new System.Drawing.Point(0, 0);
            this.grpCategory.Margin = new System.Windows.Forms.Padding(5);
            this.grpCategory.Name = "grpCategory";
            this.grpCategory.Padding = new System.Windows.Forms.Padding(5);
            this.grpCategory.Size = new System.Drawing.Size(140, 140);
            this.grpCategory.TabIndex = 3;
            this.grpCategory.TabStop = false;
            this.grpCategory.Text = "grpCategory";
            // 
            // tlpCategory
            // 
            this.tlpCategory.ColumnCount = 1;
            this.tlpCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCategory.Controls.Add(this.pnlCategory, 0, 0);
            this.tlpCategory.Controls.Add(this.lblCount, 0, 1);
            this.tlpCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCategory.Location = new System.Drawing.Point(5, 20);
            this.tlpCategory.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCategory.Name = "tlpCategory";
            this.tlpCategory.RowCount = 2;
            this.tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpCategory.Size = new System.Drawing.Size(130, 115);
            this.tlpCategory.TabIndex = 0;
            // 
            // pnlCategory
            // 
            this.pnlCategory.AutoScroll = true;
            this.pnlCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlCategory.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlCategory.Location = new System.Drawing.Point(0, 0);
            this.pnlCategory.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCategory.Name = "pnlCategory";
            this.pnlCategory.Size = new System.Drawing.Size(130, 95);
            this.pnlCategory.TabIndex = 1;
            // 
            // lblCount
            // 
            this.lblCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCount.Location = new System.Drawing.Point(0, 95);
            this.lblCount.Margin = new System.Windows.Forms.Padding(0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(130, 20);
            this.lblCount.TabIndex = 2;
            this.lblCount.Text = "lblCount";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlCategoryList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlCategoryList";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.pnlMain.ResumeLayout(false);
            this.grpCategory.ResumeLayout(false);
            this.tlpCategory.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.GroupBox grpCategory;
        private System.Windows.Forms.TableLayoutPanel tlpCategory;
        private System.Windows.Forms.Panel pnlCategory;
        private System.Windows.Forms.Label lblCount;
    }

}
