﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class FormEstimationPeriod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            tlpEstimationPeriods = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlClose = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            splEstimationPeriodData = new System.Windows.Forms.SplitContainer();
            splEstimationPeriod = new System.Windows.Forms.SplitContainer();
            grpEstimationPeriodList = new System.Windows.Forms.GroupBox();
            lstEstimationPeriodList = new System.Windows.Forms.ListBox();
            grpEstimationPeriod = new System.Windows.Forms.GroupBox();
            tlpEstimationPeriod = new System.Windows.Forms.TableLayoutPanel();
            tlpEstimationPeriodButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlInsert = new System.Windows.Forms.Panel();
            btnInsert = new System.Windows.Forms.Button();
            pnlUpdate = new System.Windows.Forms.Panel();
            btnUpdate = new System.Windows.Forms.Button();
            pnlDelete = new System.Windows.Forms.Panel();
            btnDelete = new System.Windows.Forms.Button();
            tlpEstimationPeriodColumns = new System.Windows.Forms.TableLayoutPanel();
            lblDefaultInOlap = new System.Windows.Forms.Label();
            txtDescriptionEnValue = new System.Windows.Forms.TextBox();
            lblIdValue = new System.Windows.Forms.Label();
            lblIdCaption = new System.Windows.Forms.Label();
            lblLabelEnCaption = new System.Windows.Forms.Label();
            lblDescriptionCsCaption = new System.Windows.Forms.Label();
            lblLabelCsCaption = new System.Windows.Forms.Label();
            lblEstimationDateBeginCaption = new System.Windows.Forms.Label();
            lblEstimationDateEndCaption = new System.Windows.Forms.Label();
            txtLabelEnValue = new System.Windows.Forms.TextBox();
            txtDescriptionCsValue = new System.Windows.Forms.TextBox();
            txtLabelCsValue = new System.Windows.Forms.TextBox();
            lblEstimationDateBeginValue = new System.Windows.Forms.Label();
            lblEstimationDateEndValue = new System.Windows.Forms.Label();
            lblDescriptionEnCaption = new System.Windows.Forms.Label();
            pnlRadioButtons = new System.Windows.Forms.Panel();
            rdoTrue = new System.Windows.Forms.RadioButton();
            rdoFalse = new System.Windows.Forms.RadioButton();
            pnlEstimationPeriods = new System.Windows.Forms.Panel();
            tlpPanels = new System.Windows.Forms.TableLayoutPanel();
            lblRowsCount = new System.Windows.Forms.Label();
            grpEstimationPeriods = new System.Windows.Forms.GroupBox();
            tipDefaultInOlap = new System.Windows.Forms.ToolTip(components);
            tlpEstimationPeriods.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlClose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splEstimationPeriodData).BeginInit();
            splEstimationPeriodData.Panel1.SuspendLayout();
            splEstimationPeriodData.Panel2.SuspendLayout();
            splEstimationPeriodData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splEstimationPeriod).BeginInit();
            splEstimationPeriod.Panel1.SuspendLayout();
            splEstimationPeriod.Panel2.SuspendLayout();
            splEstimationPeriod.SuspendLayout();
            grpEstimationPeriodList.SuspendLayout();
            grpEstimationPeriod.SuspendLayout();
            tlpEstimationPeriod.SuspendLayout();
            tlpEstimationPeriodButtons.SuspendLayout();
            pnlInsert.SuspendLayout();
            pnlUpdate.SuspendLayout();
            pnlDelete.SuspendLayout();
            tlpEstimationPeriodColumns.SuspendLayout();
            pnlRadioButtons.SuspendLayout();
            pnlEstimationPeriods.SuspendLayout();
            tlpPanels.SuspendLayout();
            SuspendLayout();
            // 
            // tlpEstimationPeriods
            // 
            tlpEstimationPeriods.ColumnCount = 1;
            tlpEstimationPeriods.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriods.Controls.Add(tlpButtons, 0, 1);
            tlpEstimationPeriods.Controls.Add(splEstimationPeriodData, 0, 0);
            tlpEstimationPeriods.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpEstimationPeriods.Location = new System.Drawing.Point(0, 0);
            tlpEstimationPeriods.Margin = new System.Windows.Forms.Padding(0);
            tlpEstimationPeriods.Name = "tlpEstimationPeriods";
            tlpEstimationPeriods.RowCount = 2;
            tlpEstimationPeriods.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriods.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpEstimationPeriods.Size = new System.Drawing.Size(944, 543);
            tlpEstimationPeriods.TabIndex = 1;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlClose, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 497);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 46);
            tlpButtons.TabIndex = 4;
            // 
            // pnlClose
            // 
            pnlClose.Controls.Add(btnClose);
            pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClose.Location = new System.Drawing.Point(757, 0);
            pnlClose.Margin = new System.Windows.Forms.Padding(0);
            pnlClose.Name = "pnlClose";
            pnlClose.Padding = new System.Windows.Forms.Padding(6);
            pnlClose.Size = new System.Drawing.Size(187, 46);
            pnlClose.TabIndex = 16;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnClose.Location = new System.Drawing.Point(6, 6);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(175, 34);
            btnClose.TabIndex = 15;
            btnClose.Text = "btnClose";
            btnClose.UseVisualStyleBackColor = true;
            // 
            // splEstimationPeriodData
            // 
            splEstimationPeriodData.Dock = System.Windows.Forms.DockStyle.Fill;
            splEstimationPeriodData.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splEstimationPeriodData.Location = new System.Drawing.Point(0, 0);
            splEstimationPeriodData.Margin = new System.Windows.Forms.Padding(0);
            splEstimationPeriodData.Name = "splEstimationPeriodData";
            splEstimationPeriodData.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splEstimationPeriodData.Panel1
            // 
            splEstimationPeriodData.Panel1.Controls.Add(splEstimationPeriod);
            // 
            // splEstimationPeriodData.Panel2
            // 
            splEstimationPeriodData.Panel2.Controls.Add(pnlEstimationPeriods);
            splEstimationPeriodData.Size = new System.Drawing.Size(944, 497);
            splEstimationPeriodData.SplitterDistance = 319;
            splEstimationPeriodData.SplitterWidth = 5;
            splEstimationPeriodData.TabIndex = 0;
            // 
            // splEstimationPeriod
            // 
            splEstimationPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            splEstimationPeriod.Location = new System.Drawing.Point(0, 0);
            splEstimationPeriod.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splEstimationPeriod.Name = "splEstimationPeriod";
            // 
            // splEstimationPeriod.Panel1
            // 
            splEstimationPeriod.Panel1.Controls.Add(grpEstimationPeriodList);
            // 
            // splEstimationPeriod.Panel2
            // 
            splEstimationPeriod.Panel2.Controls.Add(grpEstimationPeriod);
            splEstimationPeriod.Size = new System.Drawing.Size(944, 319);
            splEstimationPeriod.SplitterDistance = 249;
            splEstimationPeriod.SplitterWidth = 5;
            splEstimationPeriod.TabIndex = 1;
            // 
            // grpEstimationPeriodList
            // 
            grpEstimationPeriodList.Controls.Add(lstEstimationPeriodList);
            grpEstimationPeriodList.Dock = System.Windows.Forms.DockStyle.Fill;
            grpEstimationPeriodList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpEstimationPeriodList.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpEstimationPeriodList.Location = new System.Drawing.Point(0, 0);
            grpEstimationPeriodList.Margin = new System.Windows.Forms.Padding(0);
            grpEstimationPeriodList.Name = "grpEstimationPeriodList";
            grpEstimationPeriodList.Padding = new System.Windows.Forms.Padding(6);
            grpEstimationPeriodList.Size = new System.Drawing.Size(249, 319);
            grpEstimationPeriodList.TabIndex = 0;
            grpEstimationPeriodList.TabStop = false;
            grpEstimationPeriodList.Text = "grpEstimationPeriodList";
            // 
            // lstEstimationPeriodList
            // 
            lstEstimationPeriodList.Dock = System.Windows.Forms.DockStyle.Fill;
            lstEstimationPeriodList.FormattingEnabled = true;
            lstEstimationPeriodList.Location = new System.Drawing.Point(6, 21);
            lstEstimationPeriodList.Margin = new System.Windows.Forms.Padding(0);
            lstEstimationPeriodList.Name = "lstEstimationPeriodList";
            lstEstimationPeriodList.Size = new System.Drawing.Size(237, 292);
            lstEstimationPeriodList.Sorted = true;
            lstEstimationPeriodList.TabIndex = 0;
            // 
            // grpEstimationPeriod
            // 
            grpEstimationPeriod.BackColor = System.Drawing.SystemColors.Control;
            grpEstimationPeriod.Controls.Add(tlpEstimationPeriod);
            grpEstimationPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            grpEstimationPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpEstimationPeriod.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpEstimationPeriod.Location = new System.Drawing.Point(0, 0);
            grpEstimationPeriod.Margin = new System.Windows.Forms.Padding(0);
            grpEstimationPeriod.Name = "grpEstimationPeriod";
            grpEstimationPeriod.Padding = new System.Windows.Forms.Padding(6, 17, 6, 6);
            grpEstimationPeriod.Size = new System.Drawing.Size(690, 319);
            grpEstimationPeriod.TabIndex = 1;
            grpEstimationPeriod.TabStop = false;
            grpEstimationPeriod.Text = "grpEstimationPeriod";
            // 
            // tlpEstimationPeriod
            // 
            tlpEstimationPeriod.ColumnCount = 1;
            tlpEstimationPeriod.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriod.Controls.Add(tlpEstimationPeriodButtons, 0, 1);
            tlpEstimationPeriod.Controls.Add(tlpEstimationPeriodColumns, 0, 0);
            tlpEstimationPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpEstimationPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpEstimationPeriod.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpEstimationPeriod.Location = new System.Drawing.Point(6, 32);
            tlpEstimationPeriod.Margin = new System.Windows.Forms.Padding(0);
            tlpEstimationPeriod.Name = "tlpEstimationPeriod";
            tlpEstimationPeriod.RowCount = 2;
            tlpEstimationPeriod.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriod.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpEstimationPeriod.Size = new System.Drawing.Size(678, 281);
            tlpEstimationPeriod.TabIndex = 0;
            // 
            // tlpEstimationPeriodButtons
            // 
            tlpEstimationPeriodButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpEstimationPeriodButtons.ColumnCount = 4;
            tlpEstimationPeriodButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriodButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpEstimationPeriodButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpEstimationPeriodButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpEstimationPeriodButtons.Controls.Add(pnlInsert, 1, 0);
            tlpEstimationPeriodButtons.Controls.Add(pnlUpdate, 2, 0);
            tlpEstimationPeriodButtons.Controls.Add(pnlDelete, 3, 0);
            tlpEstimationPeriodButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpEstimationPeriodButtons.Location = new System.Drawing.Point(0, 235);
            tlpEstimationPeriodButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpEstimationPeriodButtons.Name = "tlpEstimationPeriodButtons";
            tlpEstimationPeriodButtons.RowCount = 1;
            tlpEstimationPeriodButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriodButtons.Size = new System.Drawing.Size(678, 46);
            tlpEstimationPeriodButtons.TabIndex = 5;
            // 
            // pnlInsert
            // 
            pnlInsert.Controls.Add(btnInsert);
            pnlInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInsert.Location = new System.Drawing.Point(117, 0);
            pnlInsert.Margin = new System.Windows.Forms.Padding(0);
            pnlInsert.Name = "pnlInsert";
            pnlInsert.Padding = new System.Windows.Forms.Padding(6);
            pnlInsert.Size = new System.Drawing.Size(187, 46);
            pnlInsert.TabIndex = 15;
            // 
            // btnInsert
            // 
            btnInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            btnInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnInsert.Location = new System.Drawing.Point(6, 6);
            btnInsert.Margin = new System.Windows.Forms.Padding(0);
            btnInsert.Name = "btnInsert";
            btnInsert.Size = new System.Drawing.Size(175, 34);
            btnInsert.TabIndex = 16;
            btnInsert.Text = "btnInsert";
            btnInsert.UseVisualStyleBackColor = true;
            // 
            // pnlUpdate
            // 
            pnlUpdate.Controls.Add(btnUpdate);
            pnlUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlUpdate.Location = new System.Drawing.Point(304, 0);
            pnlUpdate.Margin = new System.Windows.Forms.Padding(0);
            pnlUpdate.Name = "pnlUpdate";
            pnlUpdate.Padding = new System.Windows.Forms.Padding(6);
            pnlUpdate.Size = new System.Drawing.Size(187, 46);
            pnlUpdate.TabIndex = 16;
            // 
            // btnUpdate
            // 
            btnUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnUpdate.Location = new System.Drawing.Point(6, 6);
            btnUpdate.Margin = new System.Windows.Forms.Padding(0);
            btnUpdate.Name = "btnUpdate";
            btnUpdate.Size = new System.Drawing.Size(175, 34);
            btnUpdate.TabIndex = 15;
            btnUpdate.Text = "btnUpdate";
            btnUpdate.UseVisualStyleBackColor = true;
            // 
            // pnlDelete
            // 
            pnlDelete.Controls.Add(btnDelete);
            pnlDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDelete.Location = new System.Drawing.Point(491, 0);
            pnlDelete.Margin = new System.Windows.Forms.Padding(0);
            pnlDelete.Name = "pnlDelete";
            pnlDelete.Padding = new System.Windows.Forms.Padding(6);
            pnlDelete.Size = new System.Drawing.Size(187, 46);
            pnlDelete.TabIndex = 17;
            // 
            // btnDelete
            // 
            btnDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnDelete.Location = new System.Drawing.Point(6, 6);
            btnDelete.Margin = new System.Windows.Forms.Padding(0);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new System.Drawing.Size(175, 34);
            btnDelete.TabIndex = 15;
            btnDelete.Text = "btnDelete";
            btnDelete.UseVisualStyleBackColor = true;
            // 
            // tlpEstimationPeriodColumns
            // 
            tlpEstimationPeriodColumns.ColumnCount = 2;
            tlpEstimationPeriodColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            tlpEstimationPeriodColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriodColumns.Controls.Add(lblDefaultInOlap, 0, 7);
            tlpEstimationPeriodColumns.Controls.Add(txtDescriptionEnValue, 1, 6);
            tlpEstimationPeriodColumns.Controls.Add(lblIdValue, 1, 0);
            tlpEstimationPeriodColumns.Controls.Add(lblIdCaption, 0, 0);
            tlpEstimationPeriodColumns.Controls.Add(lblLabelEnCaption, 0, 5);
            tlpEstimationPeriodColumns.Controls.Add(lblDescriptionCsCaption, 0, 4);
            tlpEstimationPeriodColumns.Controls.Add(lblLabelCsCaption, 0, 3);
            tlpEstimationPeriodColumns.Controls.Add(lblEstimationDateBeginCaption, 0, 1);
            tlpEstimationPeriodColumns.Controls.Add(lblEstimationDateEndCaption, 0, 2);
            tlpEstimationPeriodColumns.Controls.Add(txtLabelEnValue, 1, 5);
            tlpEstimationPeriodColumns.Controls.Add(txtDescriptionCsValue, 1, 4);
            tlpEstimationPeriodColumns.Controls.Add(txtLabelCsValue, 1, 3);
            tlpEstimationPeriodColumns.Controls.Add(lblEstimationDateBeginValue, 1, 1);
            tlpEstimationPeriodColumns.Controls.Add(lblEstimationDateEndValue, 1, 2);
            tlpEstimationPeriodColumns.Controls.Add(lblDescriptionEnCaption, 0, 6);
            tlpEstimationPeriodColumns.Controls.Add(pnlRadioButtons, 1, 7);
            tlpEstimationPeriodColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpEstimationPeriodColumns.Location = new System.Drawing.Point(0, 0);
            tlpEstimationPeriodColumns.Margin = new System.Windows.Forms.Padding(0);
            tlpEstimationPeriodColumns.Name = "tlpEstimationPeriodColumns";
            tlpEstimationPeriodColumns.RowCount = 9;
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpEstimationPeriodColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationPeriodColumns.Size = new System.Drawing.Size(678, 235);
            tlpEstimationPeriodColumns.TabIndex = 0;
            // 
            // lblDefaultInOlap
            // 
            lblDefaultInOlap.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDefaultInOlap.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDefaultInOlap.Location = new System.Drawing.Point(0, 203);
            lblDefaultInOlap.Margin = new System.Windows.Forms.Padding(0);
            lblDefaultInOlap.Name = "lblDefaultInOlap";
            lblDefaultInOlap.Size = new System.Drawing.Size(140, 29);
            lblDefaultInOlap.TabIndex = 15;
            lblDefaultInOlap.Text = "lblDefaultInOlap";
            // 
            // txtDescriptionEnValue
            // 
            txtDescriptionEnValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtDescriptionEnValue.Location = new System.Drawing.Point(140, 174);
            txtDescriptionEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEnValue.Name = "txtDescriptionEnValue";
            txtDescriptionEnValue.Size = new System.Drawing.Size(538, 20);
            txtDescriptionEnValue.TabIndex = 14;
            // 
            // lblIdValue
            // 
            lblIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdValue.ForeColor = System.Drawing.SystemColors.ControlText;
            lblIdValue.Location = new System.Drawing.Point(140, 0);
            lblIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblIdValue.Name = "lblIdValue";
            lblIdValue.Size = new System.Drawing.Size(538, 29);
            lblIdValue.TabIndex = 3;
            lblIdValue.Text = "lblIdValue";
            // 
            // lblIdCaption
            // 
            lblIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblIdCaption.Location = new System.Drawing.Point(0, 0);
            lblIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblIdCaption.Name = "lblIdCaption";
            lblIdCaption.Size = new System.Drawing.Size(140, 29);
            lblIdCaption.TabIndex = 0;
            lblIdCaption.Text = "lblIdCaption";
            // 
            // lblLabelEnCaption
            // 
            lblLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblLabelEnCaption.Location = new System.Drawing.Point(0, 145);
            lblLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEnCaption.Name = "lblLabelEnCaption";
            lblLabelEnCaption.Size = new System.Drawing.Size(140, 29);
            lblLabelEnCaption.TabIndex = 6;
            lblLabelEnCaption.Text = "lblLabelEnCaption";
            // 
            // lblDescriptionCsCaption
            // 
            lblDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCsCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDescriptionCsCaption.Location = new System.Drawing.Point(0, 116);
            lblDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCsCaption.Name = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.Size = new System.Drawing.Size(140, 29);
            lblDescriptionCsCaption.TabIndex = 2;
            lblDescriptionCsCaption.Text = "lblDescriptionCsCaption";
            // 
            // lblLabelCsCaption
            // 
            lblLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCsCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblLabelCsCaption.Location = new System.Drawing.Point(0, 87);
            lblLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCsCaption.Name = "lblLabelCsCaption";
            lblLabelCsCaption.Size = new System.Drawing.Size(140, 29);
            lblLabelCsCaption.TabIndex = 1;
            lblLabelCsCaption.Text = "lblLabelCsCaption";
            // 
            // lblEstimationDateBeginCaption
            // 
            lblEstimationDateBeginCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblEstimationDateBeginCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblEstimationDateBeginCaption.Location = new System.Drawing.Point(0, 29);
            lblEstimationDateBeginCaption.Margin = new System.Windows.Forms.Padding(0);
            lblEstimationDateBeginCaption.Name = "lblEstimationDateBeginCaption";
            lblEstimationDateBeginCaption.Size = new System.Drawing.Size(140, 29);
            lblEstimationDateBeginCaption.TabIndex = 10;
            lblEstimationDateBeginCaption.Text = "lblEstimationDateBeginCaption";
            // 
            // lblEstimationDateEndCaption
            // 
            lblEstimationDateEndCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblEstimationDateEndCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblEstimationDateEndCaption.Location = new System.Drawing.Point(0, 58);
            lblEstimationDateEndCaption.Margin = new System.Windows.Forms.Padding(0);
            lblEstimationDateEndCaption.Name = "lblEstimationDateEndCaption";
            lblEstimationDateEndCaption.Size = new System.Drawing.Size(140, 29);
            lblEstimationDateEndCaption.TabIndex = 11;
            lblEstimationDateEndCaption.Text = "lblEstimationDateEndCaption";
            // 
            // txtLabelEnValue
            // 
            txtLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtLabelEnValue.Location = new System.Drawing.Point(140, 145);
            txtLabelEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEnValue.Name = "txtLabelEnValue";
            txtLabelEnValue.Size = new System.Drawing.Size(538, 20);
            txtLabelEnValue.TabIndex = 8;
            // 
            // txtDescriptionCsValue
            // 
            txtDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCsValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtDescriptionCsValue.Location = new System.Drawing.Point(140, 116);
            txtDescriptionCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCsValue.Name = "txtDescriptionCsValue";
            txtDescriptionCsValue.Size = new System.Drawing.Size(538, 20);
            txtDescriptionCsValue.TabIndex = 5;
            // 
            // txtLabelCsValue
            // 
            txtLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCsValue.ForeColor = System.Drawing.SystemColors.WindowText;
            txtLabelCsValue.Location = new System.Drawing.Point(140, 87);
            txtLabelCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCsValue.Name = "txtLabelCsValue";
            txtLabelCsValue.Size = new System.Drawing.Size(538, 20);
            txtLabelCsValue.TabIndex = 4;
            // 
            // lblEstimationDateBeginValue
            // 
            lblEstimationDateBeginValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblEstimationDateBeginValue.ForeColor = System.Drawing.SystemColors.ControlText;
            lblEstimationDateBeginValue.Location = new System.Drawing.Point(140, 29);
            lblEstimationDateBeginValue.Margin = new System.Windows.Forms.Padding(0);
            lblEstimationDateBeginValue.Name = "lblEstimationDateBeginValue";
            lblEstimationDateBeginValue.Size = new System.Drawing.Size(538, 29);
            lblEstimationDateBeginValue.TabIndex = 12;
            lblEstimationDateBeginValue.Text = "lblEstimationDateBeginValue";
            // 
            // lblEstimationDateEndValue
            // 
            lblEstimationDateEndValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblEstimationDateEndValue.ForeColor = System.Drawing.SystemColors.ControlText;
            lblEstimationDateEndValue.Location = new System.Drawing.Point(140, 58);
            lblEstimationDateEndValue.Margin = new System.Windows.Forms.Padding(0);
            lblEstimationDateEndValue.Name = "lblEstimationDateEndValue";
            lblEstimationDateEndValue.Size = new System.Drawing.Size(538, 29);
            lblEstimationDateEndValue.TabIndex = 13;
            lblEstimationDateEndValue.Text = "lblEstimationDateEndValue";
            // 
            // lblDescriptionEnCaption
            // 
            lblDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEnCaption.ForeColor = System.Drawing.SystemColors.ControlText;
            lblDescriptionEnCaption.Location = new System.Drawing.Point(0, 174);
            lblDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEnCaption.Name = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.Size = new System.Drawing.Size(140, 29);
            lblDescriptionEnCaption.TabIndex = 7;
            lblDescriptionEnCaption.Text = "lblDescriptionEnCaption";
            // 
            // pnlRadioButtons
            // 
            pnlRadioButtons.Controls.Add(rdoTrue);
            pnlRadioButtons.Controls.Add(rdoFalse);
            pnlRadioButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlRadioButtons.Location = new System.Drawing.Point(140, 203);
            pnlRadioButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlRadioButtons.Name = "pnlRadioButtons";
            pnlRadioButtons.Size = new System.Drawing.Size(538, 29);
            pnlRadioButtons.TabIndex = 16;
            // 
            // rdoTrue
            // 
            rdoTrue.AutoSize = true;
            rdoTrue.Location = new System.Drawing.Point(61, -2);
            rdoTrue.Name = "rdoTrue";
            rdoTrue.Size = new System.Drawing.Size(62, 17);
            rdoTrue.TabIndex = 1;
            rdoTrue.TabStop = true;
            rdoTrue.Text = "rdoTrue";
            rdoTrue.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            rdoTrue.UseVisualStyleBackColor = true;
            // 
            // rdoFalse
            // 
            rdoFalse.AutoSize = true;
            rdoFalse.Location = new System.Drawing.Point(0, -2);
            rdoFalse.Name = "rdoFalse";
            rdoFalse.Size = new System.Drawing.Size(65, 17);
            rdoFalse.TabIndex = 0;
            rdoFalse.TabStop = true;
            rdoFalse.Text = "rdoFalse";
            rdoFalse.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            rdoFalse.UseVisualStyleBackColor = true;
            // 
            // pnlEstimationPeriods
            // 
            pnlEstimationPeriods.Controls.Add(tlpPanels);
            pnlEstimationPeriods.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlEstimationPeriods.Location = new System.Drawing.Point(0, 0);
            pnlEstimationPeriods.Margin = new System.Windows.Forms.Padding(0);
            pnlEstimationPeriods.Name = "pnlEstimationPeriods";
            pnlEstimationPeriods.Size = new System.Drawing.Size(944, 173);
            pnlEstimationPeriods.TabIndex = 0;
            // 
            // tlpPanels
            // 
            tlpPanels.ColumnCount = 1;
            tlpPanels.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanels.Controls.Add(lblRowsCount, 0, 1);
            tlpPanels.Controls.Add(grpEstimationPeriods, 0, 0);
            tlpPanels.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpPanels.Location = new System.Drawing.Point(0, 0);
            tlpPanels.Name = "tlpPanels";
            tlpPanels.RowCount = 2;
            tlpPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpPanels.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpPanels.Size = new System.Drawing.Size(944, 173);
            tlpPanels.TabIndex = 1;
            // 
            // lblRowsCount
            // 
            lblRowsCount.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowsCount.ForeColor = System.Drawing.SystemColors.ControlText;
            lblRowsCount.Location = new System.Drawing.Point(0, 150);
            lblRowsCount.Margin = new System.Windows.Forms.Padding(0);
            lblRowsCount.Name = "lblRowsCount";
            lblRowsCount.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            lblRowsCount.Size = new System.Drawing.Size(944, 23);
            lblRowsCount.TabIndex = 2;
            lblRowsCount.Text = "lblRowsCount";
            // 
            // grpEstimationPeriods
            // 
            grpEstimationPeriods.Dock = System.Windows.Forms.DockStyle.Fill;
            grpEstimationPeriods.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpEstimationPeriods.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpEstimationPeriods.Location = new System.Drawing.Point(0, 0);
            grpEstimationPeriods.Margin = new System.Windows.Forms.Padding(0);
            grpEstimationPeriods.Name = "grpEstimationPeriods";
            grpEstimationPeriods.Padding = new System.Windows.Forms.Padding(6);
            grpEstimationPeriods.Size = new System.Drawing.Size(944, 150);
            grpEstimationPeriods.TabIndex = 1;
            grpEstimationPeriods.TabStop = false;
            grpEstimationPeriods.Text = "grpEstimationPeriods";
            // 
            // FormEstimationPeriod
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnClose;
            ClientSize = new System.Drawing.Size(944, 543);
            Controls.Add(tlpEstimationPeriods);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormEstimationPeriod";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormEstimationPeriod";
            tlpEstimationPeriods.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlClose.ResumeLayout(false);
            splEstimationPeriodData.Panel1.ResumeLayout(false);
            splEstimationPeriodData.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splEstimationPeriodData).EndInit();
            splEstimationPeriodData.ResumeLayout(false);
            splEstimationPeriod.Panel1.ResumeLayout(false);
            splEstimationPeriod.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splEstimationPeriod).EndInit();
            splEstimationPeriod.ResumeLayout(false);
            grpEstimationPeriodList.ResumeLayout(false);
            grpEstimationPeriod.ResumeLayout(false);
            tlpEstimationPeriod.ResumeLayout(false);
            tlpEstimationPeriodButtons.ResumeLayout(false);
            pnlInsert.ResumeLayout(false);
            pnlUpdate.ResumeLayout(false);
            pnlDelete.ResumeLayout(false);
            tlpEstimationPeriodColumns.ResumeLayout(false);
            tlpEstimationPeriodColumns.PerformLayout();
            pnlRadioButtons.ResumeLayout(false);
            pnlRadioButtons.PerformLayout();
            pnlEstimationPeriods.ResumeLayout(false);
            tlpPanels.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpEstimationPeriods;
        private System.Windows.Forms.SplitContainer splEstimationPeriodData;
        private System.Windows.Forms.SplitContainer splEstimationPeriod;
        private System.Windows.Forms.GroupBox grpEstimationPeriodList;
        private System.Windows.Forms.ListBox lstEstimationPeriodList;
        private System.Windows.Forms.GroupBox grpEstimationPeriod;
        private System.Windows.Forms.TableLayoutPanel tlpEstimationPeriod;
        private System.Windows.Forms.TableLayoutPanel tlpEstimationPeriodColumns;
        private System.Windows.Forms.TextBox txtDescriptionCsValue;
        private System.Windows.Forms.Label lblIdValue;
        private System.Windows.Forms.Label lblIdCaption;
        private System.Windows.Forms.Label lblLabelCsCaption;
        private System.Windows.Forms.Label lblDescriptionCsCaption;
        private System.Windows.Forms.TextBox txtLabelCsValue;
        private System.Windows.Forms.Label lblLabelEnCaption;
        private System.Windows.Forms.Label lblDescriptionEnCaption;
        private System.Windows.Forms.TextBox txtLabelEnValue;
        private System.Windows.Forms.Label lblEstimationDateBeginCaption;
        private System.Windows.Forms.Label lblEstimationDateEndCaption;
        private System.Windows.Forms.Label lblEstimationDateBeginValue;
        private System.Windows.Forms.Label lblEstimationDateEndValue;
        private System.Windows.Forms.Panel pnlEstimationPeriods;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TableLayoutPanel tlpEstimationPeriodButtons;
        private System.Windows.Forms.Panel pnlUpdate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Panel pnlInsert;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.TableLayoutPanel tlpPanels;
        private System.Windows.Forms.Label lblRowsCount;
        private System.Windows.Forms.GroupBox grpEstimationPeriods;
        private System.Windows.Forms.TextBox txtDescriptionEnValue;
        private System.Windows.Forms.Label lblDefaultInOlap;
        private System.Windows.Forms.Panel pnlRadioButtons;
        private System.Windows.Forms.RadioButton rdoTrue;
        private System.Windows.Forms.RadioButton rdoFalse;
        private System.Windows.Forms.ToolTip tipDefaultInOlap;
        private System.Windows.Forms.Panel pnlDelete;
        private System.Windows.Forms.Button btnDelete;
    }

}