﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba atributových kategorií pro odhad podílu"</para>
    /// <para lang="en">Control "Attribute categories selection for ratio estimate"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlVariableRatio
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení nadpisu pro čitatel</para>
        /// <para lang="en">Control for display caption for numerator</para>
        /// </summary>
        private ControlCaption ctrNumeratorCaption;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení nadpisu pro jmenovatel</para>
        /// <para lang="en">Control for display caption for denominator</para>
        /// </summary>
        private ControlCaption ctrDenominatorCaption;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení kategorií plošných domén čitatele</para>
        /// <para lang="en">Control for display area domain categories for numerator</para>
        /// </summary>
        private ControlCategoryList<AreaDomainCategory> ctrNumeratorAreaDomainCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení kategorií subpopulace čitatele</para>
        /// <para lang="en">Control for display subpopulation categories for numerator</para>
        /// </summary>
        private ControlCategoryList<SubPopulationCategory> ctrNumeratorSubPopulationCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení atributových kategorií čitatele</para>
        /// <para lang="en">Control for display attribute categories fro numerator</para>
        /// </summary>
        private ControlCategoryList<VariablePair> ctrNumeratorAttributeCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení kategorií plošných domén jmenovatele</para>
        /// <para lang="en">Control for display area domain categories for denominator</para>
        /// </summary>
        private ControlCategoryList<AreaDomainCategory> ctrDenominatorAreaDomainCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení kategorií subpopulace jmenovatele</para>
        /// <para lang="en">Control for display subpopulation categories for denominator</para>
        /// </summary>
        private ControlCategoryList<SubPopulationCategory> ctrDenominatorSubPopulationCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení atributových kategorií jmenovatele</para>
        /// <para lang="en">Control for display attribute categories fro denominator</para>
        /// </summary>
        private ControlCategoryList<VariablePair> ctrDenominatorAttributeCategories;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení atributových kategorií</para>
        /// <para lang="en">Control for display attribute categories</para>
        /// </summary>
        private ControlCategoryList<VariablePair> ctrAttributeCategories;

        #endregion Controls;


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlVariableRatio(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlEstimate)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlEstimate)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraný indikátor čitatele (read-only)</para>
        /// <para lang="en">Selected indicator for numerator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedTargetVariable
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrTargetVariable
                    .SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru čitatele (read-only)</para>
        /// <para lang="en">Selected indicator metadata for numerator (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedTargetVariableMetadata
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrTargetVariable
                    .SelectedTargetVariableMetadata;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná jednotka (read-only)</para>
        /// <para lang="en">Selected unit of measure (read-only)</para>
        /// </summary>
        public Unit SelectedUnitOfMeasure
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrUnit
                    .SelectedUnitOfMeasure;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný indikátor jmenovatele (read-only)</para>
        /// <para lang="en">Selected indicator for denominator (read-only)</para>
        /// </summary>
        public ITargetVariable SelectedDenominatorTargetVariable
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrUnit
                    .SelectedDenominatorTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybraného indikátoru jmenovatele (read-only)</para>
        /// <para lang="en">Selected indicator metadata for denominator (read-only)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedDenominatorTargetVariableMetadata
        {
            get
            {
                return ((ControlEstimate)ControlOwner)
                    .CtrUnit
                    .SelectedDenominatorTargetVariableMetadata;
            }
        }

        // Výsledkem výběru v této komponentě jsou vybrané atributové kategorie:
        // The result of the selection in this component are selected attribute categories:

        /// <summary>
        /// <para lang="cs">Vybrané atributové kategorie</para>
        /// <para lang="en">Selected attribute categories</para>
        /// </summary>
        public VariablePairList SelectedVariables
        {
            get
            {
                return
                    ctrAttributeCategories.AttributeCategories;
            }
            set
            {
                ctrAttributeCategories.AttributeCategories = value;
                EnableButtonNext();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                    "Předchozí" },
                        { nameof(btnNext),                        "Další" },
                        { nameof(cboNumeratorAreaDomain),         "ExtendedLabelCs" },
                        { nameof(cboDenominatorAreaDomain),       "ExtendedLabelCs" },
                        { nameof(cboNumeratorSubPopulation),      "ExtendedLabelCs" },
                        { nameof(cboDenominatorSubPopulation),    "ExtendedLabelCs" },
                        { nameof(grpNumerator),                   "Čitatel:" },
                        { nameof(grpNumeratorAreaDomain),         "Plošná doména:" },
                        { nameof(grpNumeratorSubPopulation),      "Subpopulace:" },
                        { nameof(grpDenominator),                 "Jmenovatel:" },
                        { nameof(grpDenominatorAreaDomain),       "Plošná doména:" },
                        { nameof(grpDenominatorSubPopulation),    "Subpopulace:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlVariableRatio),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                    "Previous" },
                        { nameof(btnNext),                        "Next" },
                        { nameof(cboNumeratorAreaDomain),         "ExtendedLabelEn" },
                        { nameof(cboDenominatorAreaDomain),       "ExtendedLabelEn" },
                        { nameof(cboNumeratorSubPopulation),      "ExtendedLabelEn" },
                        { nameof(cboDenominatorSubPopulation),    "ExtendedLabelEn" },
                        { nameof(grpNumerator),                   "Numerator:" },
                        { nameof(grpNumeratorAreaDomain),         "Area domain:" },
                        { nameof(grpNumeratorSubPopulation),      "Subpopulation:" },
                        { nameof(grpDenominator),                 "Denominator:" },
                        { nameof(grpDenominatorAreaDomain),       "Area domain:" },
                        { nameof(grpDenominatorSubPopulation),    "Subpopulation:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlVariableRatio),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            InitializeNumeratorCaption();

            InitializeDenominatorCaption();

            InitializeCategoryLists();

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            EnableButtonNext();

            onEdit = true;

            pnlMain.BackColor = Setting.BorderColor;
            pnlWorkSpace.BackColor = Setting.BorderColor;

            splNumerator.BackColor = Setting.SpliterColor;
            splNumerator.SplitterDistance = Setting.SplNumeratorDistance;

            splDenominator.BackColor = Setting.SpliterColor;
            splDenominator.SplitterDistance = Setting.SplDenominatorDistance;

            splNumeratorDenominator.BackColor = Setting.SpliterColor;
            splNumeratorDenominator.SplitterDistance = Setting.SplNumeratorDenominatorDistance;

            splVariableRatio.BackColor = Setting.SpliterColor;
            splVariableRatio.SplitterDistance = Setting.SplVariableRatioDistance;

            onEdit = false;

            btnPrevious.Click += new EventHandler(
              (sender, e) =>
              {
                  PreviousClick?.Invoke(
                        sender: this,
                        e: new EventArgs());
              });

            btnNext.Click += new EventHandler(
                (sender, e) =>
                {
                    NextClick?.Invoke(
                        sender: this,
                        e: new EventArgs());
                });

            cboNumeratorAreaDomain.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SetNumeratorAttributeCategories();
                    }
                });

            cboNumeratorSubPopulation.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SetNumeratorAttributeCategories();
                    }
                });

            cboDenominatorAreaDomain.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SetDenominatorAttributeCategories();
                    }
                });

            cboDenominatorSubPopulation.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SetDenominatorAttributeCategories();
                    }
                });

            splNumerator.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplNumeratorDistance =
                            splNumerator.SplitterDistance;
                    }
                });

            splDenominator.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplDenominatorDistance =
                            splDenominator.SplitterDistance;
                    }
                });

            splNumeratorDenominator.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplNumeratorDenominatorDistance =
                            splNumeratorDenominator.SplitterDistance;
                    }
                });

            splVariableRatio.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        Setting.SplVariableRatioDistance =
                            splVariableRatio.SplitterDistance;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku nadpisu pro čitatel</para>
        /// <para lang="en">Initialization of the control for display caption for numerator</para>
        /// </summary>
        private void InitializeNumeratorCaption()
        {
            pnlNumeratorCaption.Controls.Clear();
            ctrNumeratorCaption = new ControlCaption(
                controlOwner: this,
                numeratorTargetVariableMetadata: SelectedTargetVariableMetadata,
                denominatorTargetVariableMetadata: null,
                variablePairs: null,
                captionVersion: CaptionVersion.Wide,
                withAttributeCategories: false,
                withIdentifiers: true)
            {
                Dock = DockStyle.Fill
            };
            pnlNumeratorCaption.Controls.Add(
                value: ctrNumeratorCaption);
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku nadpisu pro jmenovatel</para>
        /// <para lang="en">Initialization of the control for display caption for denominator</para>
        /// </summary>
        private void InitializeDenominatorCaption()
        {
            pnlDenominatorCaption.Controls.Clear();
            ctrDenominatorCaption = new ControlCaption(
                controlOwner: this,
                numeratorTargetVariableMetadata: SelectedDenominatorTargetVariableMetadata,
                denominatorTargetVariableMetadata: null,
                captionVersion: CaptionVersion.Wide,
                withAttributeCategories: false,
                withIdentifiers: true)
            {
                Dock = DockStyle.Fill
            };
            pnlDenominatorCaption.Controls.Add(
                value: ctrDenominatorCaption);
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamů atributových kategorií</para>
        /// <para lang="en">Initialization of attribute category lists</para>
        /// </summary>
        private void InitializeCategoryLists()
        {
            ctrNumeratorAreaDomainCategories =
                new ControlCategoryList<AreaDomainCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            ctrNumeratorSubPopulationCategories =
                new ControlCategoryList<SubPopulationCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            ctrNumeratorAttributeCategories =
                new ControlCategoryList<VariablePair>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            ctrDenominatorAreaDomainCategories =
                new ControlCategoryList<AreaDomainCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            ctrDenominatorSubPopulationCategories =
                new ControlCategoryList<SubPopulationCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            ctrDenominatorAttributeCategories =
                new ControlCategoryList<VariablePair>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            pnlAttributeCategories.Controls.Clear();
            ctrAttributeCategories =
                new ControlCategoryList<VariablePair>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            pnlAttributeCategories.Controls.Add(
                value: ctrAttributeCategories);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            onEdit = true;
            cboNumeratorAreaDomain.DisplayMember =
                labels.TryGetValue(key: nameof(cboNumeratorAreaDomain),
                out string cboNumeratorAreaDomainText)
                    ? cboNumeratorAreaDomainText
                    : String.Empty;
            onEdit = false;

            onEdit = true;
            cboNumeratorSubPopulation.DisplayMember =
                labels.TryGetValue(key: nameof(cboNumeratorSubPopulation),
                out string cboNumeratorSubPopulationText)
                    ? cboNumeratorSubPopulationText
                    : String.Empty;
            onEdit = false;

            onEdit = true;
            cboDenominatorAreaDomain.DisplayMember =
                labels.TryGetValue(key: nameof(cboDenominatorAreaDomain),
                out string cboDenominatorAreaDomainText)
                    ? cboDenominatorAreaDomainText
                    : String.Empty;
            onEdit = false;

            onEdit = true;
            cboDenominatorSubPopulation.DisplayMember =
                labels.TryGetValue(key: nameof(cboDenominatorSubPopulation),
                out string cboDenominatorSubPopulationText)
                    ? cboDenominatorSubPopulationText
                    : String.Empty;
            onEdit = false;

            grpNumerator.Text =
                labels.TryGetValue(key: nameof(grpNumerator),
                out string grpNumeratorText)
                    ? grpNumeratorText
                    : String.Empty;

            grpNumeratorAreaDomain.Text =
                labels.TryGetValue(key: nameof(grpNumeratorAreaDomain),
                out string grpNumeratorAreaDomainText)
                    ? grpNumeratorAreaDomainText
                    : String.Empty;

            grpNumeratorSubPopulation.Text =
                labels.TryGetValue(key: nameof(grpNumeratorSubPopulation),
                out string grpNumeratorSubPopulationText)
                    ? grpNumeratorSubPopulationText
                    : String.Empty;

            grpDenominator.Text =
                labels.TryGetValue(key: nameof(grpDenominator),
                out string grpDenominatorText)
                    ? grpDenominatorText
                    : String.Empty;

            grpDenominatorAreaDomain.Text =
                labels.TryGetValue(key: nameof(grpDenominatorAreaDomain),
                out string grpDenominatorAreaDomainText)
                    ? grpDenominatorAreaDomainText
                    : String.Empty;

            grpDenominatorSubPopulation.Text =
                labels.TryGetValue(key: nameof(grpDenominatorSubPopulation),
                out string grpDenominatorSubPopulationText)
                    ? grpDenominatorSubPopulationText
                    : String.Empty;

            ctrNumeratorCaption?.InitializeLabels();

            ctrDenominatorCaption?.InitializeLabels();

            ctrAttributeCategories?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">
        /// Povolí nebo zakáže přístup na další komponentu,
        /// podle toho, zda je nebo není vybraný indikátor čitatele a jeho metadata,
        /// indikátor jmenovatele a jeho metadata,
        /// jednotka a kategorie atributového členění
        /// </para>
        /// <para lang="en">
        /// Enables or disables access to the next component
        /// depending on whether or not the indicator for numerator and its metadata are selected,
        /// indicator for denominator and its metadata are selected,
        /// the unit of measure and attribute categories are selected
        /// </para>
        /// </summary>
        private void EnableButtonNext()
        {
            btnNext.Enabled =
                (SelectedTargetVariable != null) &&
                (SelectedTargetVariableMetadata != null) &&
                (SelectedUnitOfMeasure != null) &&
                (SelectedDenominatorTargetVariable != null) &&
                (SelectedDenominatorTargetVariableMetadata != null) &&
                (SelectedVariables != null) &&
                (SelectedVariables.Count > 0);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            SetComboNumeratorAreaDomain();
            SetComboNumeratorSubPopulation();

            SetNumeratorAttributeCategories();
        }

        /// <summary>
        /// <para lang="cs">Nastaví položky v ComboBox AreaDomain pro čitatel</para>
        /// <para lang="en">Method sets items in ComboBox AreaDomain for numerator</para>
        /// </summary>
        private void SetComboNumeratorAreaDomain()
        {
            List<AreaDomain> numeratorAreaDomainItems
                = NfiEstaFunctions.FnGetAreaDomainsForTargetVariable
                    .Execute(
                        database: Database,
                        targetVariableId: SelectedTargetVariable.Id,
                        numeratorAreaDomainId: null)
                    .Items;

            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            onEdit = true;
            cboNumeratorAreaDomain.DataSource = numeratorAreaDomainItems;
            cboNumeratorAreaDomain.DisplayMember =
                labels.TryGetValue(key: nameof(cboNumeratorAreaDomain),
                out string cboNumeratorAreaDomainText)
                    ? cboNumeratorAreaDomainText
                    : String.Empty;

            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Nastaví položky v ComboBox SubPopulation pro čitatel</para>
        /// <para lang="en">Method sets items in ComboBox SubPopulation for numerator</para>
        /// </summary>
        private void SetComboNumeratorSubPopulation()
        {
            List<SubPopulation> numeratorSubPopulationItems
                = NfiEstaFunctions.FnGetSubPopulationsForTargetVariable
                    .Execute(
                        database: Database,
                        targetVariableId: SelectedTargetVariable.Id,
                        numeratorSubPopulationId: null)
                    .Items;

            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            onEdit = true;
            cboNumeratorSubPopulation.DataSource = numeratorSubPopulationItems;
            cboNumeratorSubPopulation.DisplayMember =
                labels.TryGetValue(key: nameof(cboNumeratorSubPopulation),
                out string cboNumeratorSubPopulationText)
                    ? cboNumeratorSubPopulationText
                    : String.Empty;
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Výběr atributových kategorií čitatele</para>
        /// <para lang="en">Select attribute categories for numerator</para>
        /// </summary>
        private void SetNumeratorAttributeCategories()
        {
            if ((cboNumeratorAreaDomain.SelectedItem == null) ||
                (cboNumeratorSubPopulation.SelectedItem == null))
            {
                if (cboNumeratorAreaDomain.SelectedItem == null)
                {
                    ctrNumeratorAreaDomainCategories.Clear();
                    ctrDenominatorAreaDomainCategories.Clear();
                }
                if (cboNumeratorSubPopulation.SelectedItem == null)
                {
                    ctrNumeratorSubPopulationCategories.Clear();
                    ctrDenominatorSubPopulationCategories.Clear();
                }
                ctrNumeratorAttributeCategories.Clear();
                ctrDenominatorAttributeCategories.Clear();
                ctrAttributeCategories.Clear();

                return;
            }

            AreaDomain selectedNumeratorAreaDomain = (AreaDomain)cboNumeratorAreaDomain.SelectedItem;

            SubPopulation selectedNumeratorSubPopulation = (SubPopulation)cboNumeratorSubPopulation.SelectedItem;

            ctrNumeratorAreaDomainCategories.AreaDomainCategories =
                NfiEstaFunctions.FnGetAreaSubPopulationCategories.AreaDomainCategories(
                    database: Database,
                    cAreaDomainCategory: Database.SNfiEsta.CAreaDomainCategory,
                    targetVariable: SelectedTargetVariable,
                    areaDomain: selectedNumeratorAreaDomain);

            ctrNumeratorSubPopulationCategories.SubPopulationCategories =
                NfiEstaFunctions.FnGetAreaSubPopulationCategories.SubPopulationCategories(
                    database: Database,
                    cSubPopulationCategory: Database.SNfiEsta.CSubPopulationCategory,
                    targetVariable: SelectedTargetVariable,
                    subPopulation: selectedNumeratorSubPopulation);

            ctrNumeratorAttributeCategories.AttributeCategories =
                NfiEstaFunctions.FnGetAttributeCategoriesForTargetVariable.Execute(
                    database: Database,
                    vwVariable: Database.SNfiEsta.VVariable,
                    numeratorTargetVariableId: SelectedTargetVariable.Id,
                    numeratorAreaDomainId: selectedNumeratorAreaDomain.Id,
                    numeratorSubPopulationId: selectedNumeratorSubPopulation.Id,
                    denominatorTargetVariableId: null,
                    denominatorAreaDomainId: null,
                    denominatorSubPopulationId: null);

            SetComboDenominatorAreaDomain();
            SetComboDenominatorSubPopulation();

            SetDenominatorAttributeCategories();
        }

        /// <summary>
        /// <para lang="cs">Nastaví položky v ComboBox AreaDomain pro jmenovatel</para>
        /// <para lang="en">Method sets items in ComboBox AreaDomain for denominator</para>
        /// </summary>
        private void SetComboDenominatorAreaDomain()
        {
            List<AreaDomain> denominatorAreaDomainItems;

            if (cboNumeratorAreaDomain.SelectedItem != null)
            {
                AreaDomain selectedAreaDomain =
                    (AreaDomain)cboNumeratorAreaDomain.SelectedItem;

                denominatorAreaDomainItems
                    = NfiEstaFunctions.FnGetAreaDomainsForTargetVariable
                        .Execute(
                            database: Database,
                            targetVariableId: SelectedTargetVariable.Id,
                            numeratorAreaDomainId: selectedAreaDomain.Id)
                        .Items;
            }
            else
            {
                denominatorAreaDomainItems = [];
            }

            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            onEdit = true;
            cboDenominatorAreaDomain.DataSource = denominatorAreaDomainItems;
            cboDenominatorAreaDomain.DisplayMember =
                labels.TryGetValue(key: nameof(cboDenominatorAreaDomain),
                out string cboDenominatorAreaDomainText)
                    ? cboDenominatorAreaDomainText
                    : String.Empty;
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Nastaví položky v ComboBox SubPopulation pro jmenovatel</para>
        /// <para lang="en">Method sets items in ComboBox SubPopulation for denominator</para>
        /// </summary>
        private void SetComboDenominatorSubPopulation()
        {
            List<SubPopulation> denominatorSubPopulationItems;

            if (cboNumeratorSubPopulation.SelectedItem != null)
            {
                SubPopulation selectedSubPopulation =
                    (SubPopulation)cboNumeratorSubPopulation.SelectedItem;

                denominatorSubPopulationItems
                = NfiEstaFunctions.FnGetSubPopulationsForTargetVariable
                    .Execute(
                        database: Database,
                        targetVariableId: SelectedTargetVariable.Id,
                        numeratorSubPopulationId: selectedSubPopulation.Id)
                    .Items;
            }
            else
            {
                denominatorSubPopulationItems = [];
            }

            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            onEdit = true;
            cboDenominatorSubPopulation.DataSource = denominatorSubPopulationItems;
            cboDenominatorSubPopulation.DisplayMember =
                labels.TryGetValue(key: nameof(cboDenominatorSubPopulation),
                out string cboDenominatorSubPopulationText)
                    ? cboDenominatorSubPopulationText
                    : String.Empty;
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Výběr atributových kategorií jmenovatele</para>
        /// <para lang="en">Select attribute categories for denominator</para>
        /// </summary>
        private void SetDenominatorAttributeCategories()
        {
            if ((cboDenominatorAreaDomain.SelectedItem == null) ||
                (cboDenominatorSubPopulation.SelectedItem == null))
            {
                if (cboDenominatorAreaDomain.SelectedItem == null)
                {
                    ctrDenominatorAreaDomainCategories.Clear();
                }
                if (cboDenominatorSubPopulation.SelectedItem == null)
                {
                    ctrDenominatorSubPopulationCategories.Clear();
                }
                ctrDenominatorAttributeCategories.Clear();
                ctrAttributeCategories.Clear();

                return;
            }

            AreaDomain selectedNumeratorAreaDomain = (AreaDomain)cboNumeratorAreaDomain.SelectedItem;

            SubPopulation selectedNumeratorSubPopulation = (SubPopulation)cboNumeratorSubPopulation.SelectedItem;

            AreaDomain selectedDenominatorAreaDomain = (AreaDomain)cboDenominatorAreaDomain.SelectedItem;

            SubPopulation selectedDenominatorSubPopulation = (SubPopulation)cboDenominatorSubPopulation.SelectedItem;

            ctrDenominatorAreaDomainCategories.AreaDomainCategories =
                NfiEstaFunctions.FnGetAreaSubPopulationCategories.AreaDomainCategories(
                    database: Database,
                    cAreaDomainCategory: Database.SNfiEsta.CAreaDomainCategory,
                    targetVariable: SelectedDenominatorTargetVariable,
                    areaDomain: selectedDenominatorAreaDomain);

            ctrDenominatorSubPopulationCategories.SubPopulationCategories =
                NfiEstaFunctions.FnGetAreaSubPopulationCategories.SubPopulationCategories(
                    database: Database,
                    cSubPopulationCategory: Database.SNfiEsta.CSubPopulationCategory,
                    targetVariable: SelectedDenominatorTargetVariable,
                    subPopulation: selectedDenominatorSubPopulation);

            ctrDenominatorAttributeCategories.AttributeCategories =
                NfiEstaFunctions.FnGetAttributeCategoriesForTargetVariable.Execute(
                    database: Database,
                    vwVariable: Database.SNfiEsta.VVariable,
                    numeratorTargetVariableId: SelectedDenominatorTargetVariable.Id,
                    numeratorAreaDomainId: selectedDenominatorAreaDomain.Id,
                    numeratorSubPopulationId: selectedDenominatorSubPopulation.Id,
                    denominatorTargetVariableId: null,
                    denominatorAreaDomainId: null,
                    denominatorSubPopulationId: null);

            SelectedVariables =
                NfiEstaFunctions.FnGetAttributeCategoriesForTargetVariable.Execute(
                    database: Database,
                    vwVariable: Database.SNfiEsta.VVariable,
                    numeratorTargetVariableId: SelectedTargetVariable.Id,
                    numeratorAreaDomainId: selectedNumeratorAreaDomain.Id,
                    numeratorSubPopulationId: selectedNumeratorSubPopulation.Id,
                    denominatorTargetVariableId: SelectedDenominatorTargetVariable.Id,
                    denominatorAreaDomainId: selectedDenominatorAreaDomain.Id,
                    denominatorSubPopulationId: selectedDenominatorSubPopulation.Id);
        }

        #endregion Methods

    }

}