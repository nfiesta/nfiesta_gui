﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Modul pro konfiguraci a výpočet odhadů"</para>
    /// <para lang="en">Control "Module for configuration and calculation estimates"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class ControlEstimate
        : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Databázové tabulky</para>
        /// <para lang="en">Database tables</para>
        /// </summary>
        private NfiEstaDB database;

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        private LanguageVersion languageVersion;

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        private LanguageFile languageFile;

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        private Setting setting;

        /// <summary>
        /// <para lang="cs">Počet záznamů v error-logu</para>
        /// <para lang="en">Number of entries in error-log</para>
        /// </summary>
        private int errorLogEntryNumber;

        private string msgReset = String.Empty;
        private string msgSQLCommandsDisplayEnabled = String.Empty;
        private string msgSQLCommandsDisplayDisabled = String.Empty;
        private string msgNoDatabaseConnection = String.Empty;
        private string msgNoDbExtension = String.Empty;
        private string msgNoDbSchema = String.Empty;
        private string msgInvalidDbVersion = String.Empty;
        private string msgValidDbVersion = String.Empty;
        private string msgNoEstimationCellCollection = String.Empty;
        private string msgLargeEstimationCellCollection = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba indikátoru"</para>
        /// <para lang="en">Control "Indicator selection"</para>
        /// </summary>
        private ControlTargetVariable ctrTargetVariable;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba indikátoru"</para>
        /// <para lang="en">Control "Indicator selection"</para>
        /// </summary>
        internal ControlTargetVariable CtrTargetVariable
        {
            get
            {
                return ctrTargetVariable ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrTargetVariable)} is null.",
                        paramName: nameof(CtrTargetVariable));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba jednotky a typu odhadu"</para>
        /// <para lang="en">Control "Unit and estimate type selection"</para>
        /// </summary>
        private ControlUnit ctrUnit;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba jednotky a typu odhadu"</para>
        /// <para lang="en">Control "Unit and estimate type selection"</para>
        /// </summary>
        internal ControlUnit CtrUnit
        {
            get
            {
                return ctrUnit ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrUnit)} is null.",
                        paramName: nameof(CtrUnit));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba atributových kategorií pro odhad úhrnu"</para>
        /// <para lang="en">Control "Attribute categories selection for total estimate"</para>
        /// </summary>
        private ControlVariable ctrVariable;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba atributových kategorií pro odhad úhrnu"</para>
        /// <para lang="en">Control "Attribute categories selection for total estimate"</para>
        /// </summary>
        internal ControlVariable CtrVariable
        {
            get
            {
                return ctrVariable ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrVariable)} is null.",
                        paramName: nameof(CtrVariable));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba atributových kategorií pro odhad podílu"</para>
        /// <para lang="en">Control "Attribute categories selection for ratio estimate"</para>
        /// </summary>
        private ControlVariableRatio ctrVariableRatio;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba atributových kategorií pro odhad podílu"</para>
        /// <para lang="en">Control "Attribute categories selection for ratio estimate"</para>
        /// </summary>
        internal ControlVariableRatio CtrVariableRatio
        {
            get
            {
                return ctrVariableRatio ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrVariableRatio)} is null.",
                        paramName: nameof(CtrVariableRatio));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Konfigurace a výpočet odhadů"</para>
        /// <para lang="en">Control "Configuration and calculation estimates"</para>
        /// </summary>
        private ControlEstimateResult ctrEstimateResult;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Konfigurace a výpočet odhadů"</para>
        /// <para lang="en">Control "Configuration and calculation estimates"</para>
        /// </summary>
        internal ControlEstimateResult CtrEstimateResult
        {
            get
            {
                return ctrEstimateResult ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrEstimateResult)} is null.",
                        paramName: nameof(CtrEstimateResult));
            }
        }

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        public ControlEstimate()
        {
            ErrorLogEntryNumber = 0;

            InitializeComponent();

            Initialize(
                controlOwner: null,
                languageVersion: LanguageVersion.International,
                languageFile: new LanguageFile(),
                setting: new Setting(),
                withEventHandlers: true);
        }

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </param>
        public ControlEstimate(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting)
        {
            ErrorLogEntryNumber = 0;

            InitializeComponent();

            Initialize(
                controlOwner: controlOwner,
                languageVersion: languageVersion,
                languageFile: languageFile,
                setting: setting,
                withEventHandlers: true);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                return controlOwner;
            }
            set
            {
                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return languageVersion;
            }
            set
            {
                languageVersion = value;
                Setting.DBSetting.LanguageVersion = LanguageVersion;
                Database.Postgres.Setting = Setting.DBSetting;

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return languageFile ??
                    new LanguageFile();
            }
            set
            {
                languageFile = value ??
                    new LanguageFile();

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return setting ??
                    new Setting();
            }
            set
            {
                setting = value ??
                    new Setting();
                Database.Postgres.Setting = Setting.DBSetting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Počet záznamů v error-logu</para>
        /// <para lang="en">Number of entries in error-log</para>
        /// </summary>
        internal int ErrorLogEntryNumber
        {
            get
            {
                return errorLogEntryNumber;
            }
            set
            {
                errorLogEntryNumber =
                    (value <= 0)
                    ? 0 :
                    value;
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Defaultní fáze odhadu pro čitatel,
        /// použije se jednofázový odhad, dokud uživatel nevybere jiný
        /// </para>
        /// <para lang="en">
        /// Default estimate phase for numerator
        /// one-phase estimate is used until user change the selection
        /// </para>
        /// </summary>
        internal PhaseEstimateType DefaultNumeratorPhaseEstimateType
        {
            get
            {
                return
                    Database.SNfiEsta.CPhaseEstimateType.Items
                    .Where(a => a.Id == (int)PhaseEstimateTypeEnum.SinglePhase)
                    .FirstOrDefault<PhaseEstimateType>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Defaultní seznam fází odhadů pro čitatel
        /// jedno-prvkový seznam s defaultní fází odhadu pro čitatel
        /// </para>
        /// <para lang="en">
        /// Default list of estimate phases for numerator
        /// one-item list with default estimate phase for numerator
        /// </para>
        /// </summary>
        internal PhaseEstimateTypeList DefaultNumeratorPhaseEstimateTypeList
        {
            get
            {
                if (DefaultNumeratorPhaseEstimateType == null)
                {
                    return null;
                }

                return
                    new PhaseEstimateTypeList(
                        database: Database,
                        rows:
                            Database.SNfiEsta.CPhaseEstimateType.Data.AsEnumerable()
                            .Where(a => a.Field<int>(PhaseEstimateTypeList.ColId.Name) == DefaultNumeratorPhaseEstimateType.Id));
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Defaultní fáze odhadu pro jmenovatel,
        /// použije se jednofázový odhad, dokud uživatel nevybere jiný
        /// </para>
        /// <para lang="en">
        /// Default estimate phase for denominator
        /// one-phase estimate is used until user change the selection
        /// </para>
        /// </summary>
        internal PhaseEstimateType DefaultDenominatorPhaseEstimateType
        {
            get
            {
                return
                    Database.SNfiEsta.CPhaseEstimateType.Items
                    .Where(a => a.Id == (int)PhaseEstimateTypeEnum.SinglePhase)
                    .FirstOrDefault<PhaseEstimateType>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Defaultní seznam fází odhadů pro jmenovatel
        /// jedno-prvkový seznam s defaultní fází odhadu pro jmenovatel
        /// </para>
        /// <para lang="en">
        /// Default list of estimate phases for denominator
        /// one-item list with default estimate phase for denominator
        /// </para>
        /// </summary>
        internal PhaseEstimateTypeList DefaultDenominatorPhaseEstimateTypeList
        {
            get
            {
                if (DefaultDenominatorPhaseEstimateType == null)
                {
                    return null;
                }

                return
                    new PhaseEstimateTypeList(
                        database: Database,
                        rows:
                            Database.SNfiEsta.CPhaseEstimateType.Data.AsEnumerable()
                            .Where(a => a.Field<int>(PhaseEstimateTypeList.ColId.Name) == DefaultDenominatorPhaseEstimateType.Id));
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Počet výpočetních buněk,
        /// v kolekci s nejmenším počtem výpočetních buněk
        /// </para>
        /// <para lang="en">
        /// Number of estimation cells,
        /// in the collection with the smallest number of estimation cells
        /// </para>
        /// </summary>
        internal TFnGetEstimationCellsNumberInCollection DefaultEstimationCellCollectionCount
        {
            get
            {
                if (
                    !Database.SNfiEsta.VEstimationCellsCount.Items
                    .Where(a => a.EstimationCellsNumber > 0)
                    .Any())
                {
                    // V databázi neexistuje kolekce s alespoň jednou výpočetní buňkou
                    return null;
                }

                long minCount =
                    Database.SNfiEsta.VEstimationCellsCount.Items
                        .Where(a => a.EstimationCellsNumber > 0)
                        .Select(a => a.EstimationCellsNumber)
                        .Min<long>();
                return
                        Database.SNfiEsta.VEstimationCellsCount.Items
                        .Where(a => a.EstimationCellsNumber == minCount)
                        .OrderBy(a => a.Id)
                        .FirstOrDefault<TFnGetEstimationCellsNumberInCollection>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Defaultní kolekce výpočetních buněk,
        /// použije se kolekce s nejmenším počtem výpočetních buněk,
        /// dokud si uživatel nevybere jinou
        /// </para>
        /// <para lang="en">
        /// Default collection of estimation cells,
        /// the collection with the smallest number of estimation cells,
        /// it is used until user change the selection
        /// </para>
        /// </summary>
        internal EstimationCellCollection DefaultEstimationCellCollection
        {
            get
            {
                if (DefaultEstimationCellCollectionCount == null)
                {
                    // V databázi neexistuje kolekce s alespoň jednou výpočetní buňkou
                    return null;
                }

                // Kolekce s nejmenším početem výpočetních buněk
                return
                    Database.SNfiEsta.CEstimationCellCollection.Items
                    .Where(a => a.Id == DefaultEstimationCellCollectionCount.Id)
                    .FirstOrDefault<EstimationCellCollection>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Defaultní seznam kolekcí výpočetních buněk,
        /// jedno-prvkový seznam s defaultní kolekcí výpočetních buněk
        /// </para>
        /// <para lang="en">
        /// Default list of estimation cell collections
        /// one-item list with default estimation cell collection
        /// </para>
        /// </summary>
        internal EstimationCellCollectionList DefaultEstimationCellCollectionList
        {
            get
            {
                if (DefaultEstimationCellCollection == null)
                {
                    return null;
                }

                return
                    new EstimationCellCollectionList(
                        database: Database,
                        rows:
                            Database.SNfiEsta.CEstimationCellCollection.Data.AsEnumerable()
                            .Where(a => a.Field<int>(columnName: EstimationCellCollectionList.ColId.Name) == DefaultEstimationCellCollection.Id));
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Defaultní seznam výpočetních období
        /// (Výpočetní období označená v databázi attributem default_in_olap = true)
        /// </para>
        /// <para lang="en">
        /// Default list of estimation estimation periods
        /// (Estimation periods marked in the database attribute default_in_olap = true)
        /// </para>
        /// </summary>
        internal EstimationPeriodList DefaultSelectedEstimationPeriodList
        {
            get
            {
                return
                    new EstimationPeriodList(
                        database: Database,
                        rows: Database.SNfiEsta.CEstimationPeriod.Data.AsEnumerable()
                        .Where(a => a.Field<Nullable<bool>>(columnName: EstimationPeriodList.ColDefaultInOlap.Name) ?? false));
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(tsmiDatabase),                       "Databáze" },
                        { nameof(tsmiConnect),                        "Připojit" },
                        { nameof(tsmiSDesignExtension),               "Data extenze $1" },
                        { nameof(tsmiSResultsExtension),              "Data extenze $1" },
                        { nameof(tsmiNfiEstaExtension),               "Data extenze $1" },
                        { nameof(btnStop),                            "Zastavit výpočet" },
                        { nameof(msgReset),                           "Nastavení modulu pro konfiguraci a výpočet bylo resetováno na výchozí hodnoty." },
                        { nameof(msgSQLCommandsDisplayEnabled),       "Povoleno zobrazování SQL příkazů." },
                        { nameof(msgSQLCommandsDisplayDisabled),      "Zakázáno zobrazování SQL příkazů." },
                        { nameof(msgNoDatabaseConnection),            "Aplikace není připojena k databázi." },
                        { nameof(msgNoDbExtension),                   "V databázi není nainstalovaná extenze $1." },
                        { nameof(msgNoDbSchema),                      "V databázi neexistuje schéma $1." },
                        { nameof(msgInvalidDbVersion),                "Nainstalovaná verze databázové extenze $1 $2 se neshoduje s očekávanou verzí $3." },
                        { nameof(msgValidDbVersion),                  "Databázová extenze $1 $2 [$3]." },
                        { nameof(msgNoEstimationCellCollection),      "V databázi neexistuje kolekce s alespoň jednou výpočetní buňkou." },
                        { nameof(msgLargeEstimationCellCollection),   "Počet výpočetních buněk v kolekci $1 $0přesahuje požadovaný limit $2 výpočetních buněk." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlEstimate),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(tsmiDatabase),                      "Database"},
                        { nameof(tsmiConnect),                       "Connect"},
                        { nameof(tsmiSDesignExtension),              "Data from extension $1"},
                        { nameof(tsmiSResultsExtension),             "Data from extension $1" },
                        { nameof(tsmiNfiEstaExtension),              "Data from extension $1"},
                        { nameof(btnStop),                           "Stop calculation" },
                        { nameof(msgReset),                          "Configuration and calculation estimate module settings have been reset to default values." },
                        { nameof(msgSQLCommandsDisplayEnabled),      "Display of SQL statements enabled." },
                        { nameof(msgSQLCommandsDisplayDisabled),     "Display of SQL statements disabled." },
                        { nameof(msgNoDatabaseConnection),           "Application is not connected to database."},
                        { nameof(msgNoDbExtension),                  "Database extension $1 is not installed."},
                        { nameof(msgNoDbSchema),                     "Schema $1 does not exist in the database." },
                        { nameof(msgInvalidDbVersion),               "Installed version of the database extension $1 $2 does not correspond with expected version $3." },
                        { nameof(msgValidDbVersion),                 "Database extension $1 $2 [$3]." },
                        { nameof(msgNoEstimationCellCollection),     "There is no collection with at least one estimation cell in the database." },
                        { nameof(msgLargeEstimationCellCollection),  "The number of estimation cells in the collection $1 $0exceeds the required limit $2 of estimation cells." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlEstimate),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </param>
        /// <param name="withEventHandlers">
        /// <para lang="cs">Inicializace včetně obsluhy událostí tlačítek</para>
        /// <para lang="en">Initialize with button event handlers</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting,
            bool withEventHandlers)
        {
            // Controls
            // Pozn: Při inicializaci objektu je třeba Controls nastavit na null,
            // tj. před vytvořením objektu připojení k databázi,
            // jinak v nich dochází k přístupu k databázi, ke které ještě neexistuje připojení
            ctrTargetVariable = null;
            ctrUnit = null;
            ctrVariable = null;
            ctrVariableRatio = null;
            ctrEstimateResult = null;
            pnlControls.Controls.Clear();

            // ControlOwner
            ControlOwner = controlOwner;

            // Database
            Disconnect();
            database = new NfiEstaDB();
            database.Postgres.StayConnected = true;

            Database.Postgres.ConnectionError +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.ConnectionErrorHandler(
                (sender, e) =>
                {
                    MessageBox.Show(
                        text: e.ToString(),
                        caption: e.MessageBoxCaption,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Error);
                });

            Database.Postgres.ExceptionReceived +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionReceivedHandler(
                (sender, e) =>
                {
                    if (!Setting.DBSuppressError)
                    {
                        MessageBox.Show(
                            text: e.ToString(),
                            caption: e.MessageBoxCaption,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Error);
                    }
                });

            Database.Postgres.NoticeReceived +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.NoticeReceivedHandler(
                (sender, e) =>
                {
                    if (!Setting.DBSuppressWarning)
                    {
                        MessageBox.Show(
                            text: $"{e.Notice.MessageText}",
                            caption: $"{e.Notice.Severity}",
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                });

            // LanguageVersion
            LanguageVersion = languageVersion;

            // LanguageFile
            LanguageFile = languageFile;

            // Setting
            Setting = setting;

            // StatusStrip
            ssrMain.Visible = Setting.StatusStripVisible;

            // Thread buttons
            Enable();

#if DEBUG
            tsmiSeparator.Visible = true;
            tsmiSDesignExtension.Visible = true;
            tsmiSResultsExtension.Visible = true;
            tsmiNfiEstaExtension.Visible = true;
#else
            tsmiSeparator.Visible = false;
            tsmiSDesignExtension.Visible = false;
            tsmiSResultsExtension.Visible = false;
            tsmiNfiEstaExtension.Visible = false;
#endif

            if (withEventHandlers)
            {
                tsmiConnect.Click += new EventHandler(
                    (sender, e) => { Connect(); });

                tsmiSDesignExtension.Click += new EventHandler(
                    (sender, e) => { SDesignExtension(); });

                tsmiSResultsExtension.Click += new EventHandler(
                   (sender, e) => { SResultsExtension(); });

                tsmiNfiEstaExtension.Click += new EventHandler(
                    (sender, e) => { NfiEstaExtension(); });

                btnStop.Click += new EventHandler(
                   (sender, e) =>
                   {
                       ctrEstimateResult?.CtrViewOLAP?.ControlThread?.Stop();
                   });
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing module control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            tsmiDatabase.Text =
                labels.TryGetValue(key: nameof(tsmiDatabase),
                out string tsmiDatabaseText)
                    ? tsmiDatabaseText
                    : String.Empty;

            tsmiConnect.Text =
                 labels.TryGetValue(key: nameof(tsmiConnect),
                 out string tsmiConnectText)
                     ? tsmiConnectText
                     : String.Empty;

            tsmiSDesignExtension.Text =
                 (labels.TryGetValue(key: nameof(tsmiSDesignExtension),
                 out string tsmiSDesignExtensionText)
                     ? tsmiSDesignExtensionText
                     : String.Empty)
                .Replace(
                     oldValue: "$1",
                     newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName);

            tsmiSResultsExtension.Text =
                 (labels.TryGetValue(key: nameof(tsmiSResultsExtension),
                 out string tsmiSResultsExtensionText)
                     ? tsmiSResultsExtensionText
                     : String.Empty)
                .Replace(
                     oldValue: "$1",
                     newValue: ZaJi.NfiEstaPg.Results.ResultsSchema.ExtensionName);

            tsmiNfiEstaExtension.Text =
                   (labels.TryGetValue(key: nameof(tsmiNfiEstaExtension),
                   out string tsmiNfiEstaExtensionText)
                       ? tsmiNfiEstaExtensionText
                       : String.Empty)
                    .Replace(
                       oldValue: "$1",
                       newValue: ZaJi.NfiEstaPg.Core.NfiEstaSchema.ExtensionName);

            btnStop.Text =
                 labels.TryGetValue(key: nameof(btnStop),
                 out string btnStopText)
                     ? btnStopText
                     : String.Empty;

            msgReset =
                 labels.TryGetValue(key: nameof(msgReset),
                 out msgReset)
                     ? msgReset
                     : String.Empty;

            msgSQLCommandsDisplayEnabled =
                 labels.TryGetValue(key: nameof(msgSQLCommandsDisplayEnabled),
                 out msgSQLCommandsDisplayEnabled)
                     ? msgSQLCommandsDisplayEnabled
                     : String.Empty;

            msgSQLCommandsDisplayDisabled =
                 labels.TryGetValue(key: nameof(msgSQLCommandsDisplayDisabled),
                 out msgSQLCommandsDisplayDisabled)
                     ? msgSQLCommandsDisplayDisabled
                     : String.Empty;

            msgNoDatabaseConnection =
                 labels.TryGetValue(key: nameof(msgNoDatabaseConnection),
                 out msgNoDatabaseConnection)
                     ? msgNoDatabaseConnection
                     : String.Empty;

            msgNoDbExtension =
                 labels.TryGetValue(key: nameof(msgNoDbExtension),
                 out msgNoDbExtension)
                     ? msgNoDbExtension
                     : String.Empty;

            msgNoDbSchema =
                 labels.TryGetValue(key: nameof(msgNoDbSchema),
                 out msgNoDbSchema)
                     ? msgNoDbSchema
                     : String.Empty;

            msgInvalidDbVersion =
                 labels.TryGetValue(key: nameof(msgInvalidDbVersion),
                 out msgInvalidDbVersion)
                     ? msgInvalidDbVersion
                     : String.Empty;

            msgValidDbVersion =
                 labels.TryGetValue(key: nameof(msgValidDbVersion),
                 out msgValidDbVersion)
                     ? msgValidDbVersion
                     : String.Empty;

            msgNoEstimationCellCollection =
                 labels.TryGetValue(key: nameof(msgNoEstimationCellCollection),
                 out msgNoEstimationCellCollection)
                     ? msgNoEstimationCellCollection
                     : String.Empty;

            msgLargeEstimationCellCollection =
                 labels.TryGetValue(key: nameof(msgLargeEstimationCellCollection),
                 out msgLargeEstimationCellCollection)
                     ? msgLargeEstimationCellCollection
                     : String.Empty;

            ctrTargetVariable?.InitializeLabels();

            ctrUnit?.InitializeLabels();

            ctrVariable?.InitializeLabels();

            ctrVariableRatio?.InitializeLabels();

            ctrEstimateResult?.InitializeLabels();

            DisplayNfiEstaExtensionStatus();

            DisplaySDesignExtensionStatus();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            LoadContent(verbose: false);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat zprávy</para>
        /// <para lang="en">Dislay messages</para>
        /// </param>
        public void LoadContent(bool verbose)
        {
            // Tabulky pro ControlTargetVariable:
            Database.SNfiEsta.CTopic.ReLoad();
            Database.SNfiEsta.CmTargetVariableToTopic.ReLoad();
            Database.SNfiEsta.CTargetVariable.ReLoad();
            Database.SNfiEsta.CTargetVariableMetadata.ReLoad(
                cTargetVariable: Database.SNfiEsta.CTargetVariable);

            // Tabulky pro ControlVariable a ControlVariableRatio:
            Database.SNfiEsta.CAreaDomain.ReLoad(extended: true);
            Database.SNfiEsta.CAreaDomainCategory.ReLoad(extended: true);
            Database.SNfiEsta.CAuxiliaryVariable.ReLoad(extended: true);
            Database.SNfiEsta.CAuxiliaryVariableCategory.ReLoad(extended: true);
            Database.SNfiEsta.CSubPopulation.ReLoad(extended: true);
            Database.SNfiEsta.CSubPopulationCategory.ReLoad(extended: true);
            Database.SNfiEsta.TVariable.ReLoad();
            Database.SNfiEsta.TVariableHierarchy.ReLoad();
            Database.SNfiEsta.VVariable.ReLoad(
                tVariable: Database.SNfiEsta.TVariable,
                cSubPopulationCategory: Database.SNfiEsta.CSubPopulationCategory,
                cSubPopulation: Database.SNfiEsta.CSubPopulation,
                cAreaDomainCategory: Database.SNfiEsta.CAreaDomainCategory,
                cAreaDomain: Database.SNfiEsta.CAreaDomain,
                cAuxiliaryVariableCategory: Database.SNfiEsta.CAuxiliaryVariableCategory,
                cAuxiliaryVariable: Database.SNfiEsta.CAuxiliaryVariable
                );

            // Tabulky pro ControlEstimateResult:
            Database.SNfiEsta.CEstimationPeriod.ReLoad();
            Database.SNfiEsta.CPhaseEstimateType.ReLoad();
            Database.SNfiEsta.CEstimateType.ReLoad();
            Database.SNfiEsta.CEstimateConfStatus.ReLoad();
            Database.SDesign.TStratum.ReLoad();
            Database.SDesign.TPanel.ReLoad();
            Database.SDesign.TReferenceYearSet.ReLoad();
            Database.SNfiEsta.VEstimationCellsCount =
                NfiEstaFunctions.FnGetEstimationCellsNumberInCollection.Execute(database: Database);
            Database.SNfiEsta.CEstimationCellCollection.ReLoad();

            ReloadEstimationCells(
                selectedEstimationCellCollections: null,
                verbose: verbose);
        }

        /// <summary>
        /// <para lang="cs">
        /// Nahraje znovu seznam výpočetních buněk pro vybrané kolekce
        /// </para>
        /// <para lang="en">
        /// Reloads the list of estimation cells for the selected collections</para>
        /// </summary>
        /// <param name="selectedEstimationCellCollections">
        /// <para lang="cs">
        /// Vybrané kolekce výpočetních buněk
        /// (jestliže null, pak se nahrávají výpočetní buňky z kolekce s nejmenším počtem výpočetních buněk)</para>
        /// <para lang="en">
        /// Selected estimation cell collections
        /// (if null, then the estimation cells from the collection with the smallest number of estimation cells are loaded)
        /// </para>
        /// </param>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat zprávy</para>
        /// <para lang="en">Dislay messages</para>
        /// </param>
        public void ReloadEstimationCells(
            EstimationCellCollectionList selectedEstimationCellCollections = null,
            bool verbose = false)
        {
            string strEstimationCellCollectionsIds;
            string strEstimationCellsIds;
            string strEstimationCellsInStratumIds;

            // Vybrané kolekce výpočetních buněk
            if (selectedEstimationCellCollections != null)
            {
                // Identifikátory vybraných kolekcí výpočetních buněk
                IEnumerable<int> estimationCellCollectionsIds =
                    selectedEstimationCellCollections.Items
                    .Select(a => a.Id);

                if (!estimationCellCollectionsIds.Any())
                {
                    Database.SNfiEsta.CEstimationCell.ReLoad(condition: $"false");
                    Database.SNfiEsta.TStratumInEstimationCell.ReLoad(condition: $"false");
                    return;
                }

                strEstimationCellCollectionsIds =
                    estimationCellCollectionsIds
                        .Select(a => a.ToString())
                        .Aggregate((a, b) => $"{a},{b}");
            }


            // Defaultní kolekce výpočetních buněk (s minimálním počtem výpočetních buněk)
            else
            {
                if ((DefaultEstimationCellCollectionCount == null) || (DefaultEstimationCellCollection == null))
                {
                    // "V databázi neexistuje kolekce s alespoň jednou výpočetní buňkou."
                    Database.SNfiEsta.CEstimationCell.ReLoad(condition: $"false");
                    Database.SNfiEsta.TStratumInEstimationCell.ReLoad(condition: $"false");

                    if (verbose)
                    {
                        MessageBox.Show(
                            text: msgNoEstimationCellCollection,
                            caption: String.Empty,
                            icon: MessageBoxIcon.Information,
                            buttons: MessageBoxButtons.OK);
                    }

                    return;
                }

                if (DefaultEstimationCellCollectionCount.EstimationCellsNumber > Setting.EstimationCellsCountLimit)
                {
                    // "Počet výpočetních buněk v kolekci přesahuje požadovaný limit výpočetních buněk."
                    Database.SNfiEsta.CEstimationCell.ReLoad(condition: $"false");
                    Database.SNfiEsta.TStratumInEstimationCell.ReLoad(condition: $"false");

                    if (verbose)
                    {
                        MessageBox.Show(
                        text: msgLargeEstimationCellCollection
                            .Replace(oldValue: "$1", newValue: DefaultEstimationCellCollectionCount.EstimationCellsNumber.ToString())
                            .Replace(oldValue: "$2", newValue: Setting.EstimationCellsCountLimit.ToString())
                            .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        icon: MessageBoxIcon.Information,
                        buttons: MessageBoxButtons.OK);
                    }

                    return;
                }

                // Omezení seznamu výpočetních buněk na kolekci s nejmenším počtem výpočetních buněk
                // Ostatní výpočetní buňky jsou načteny až po volbě kolekce uživatelem
                strEstimationCellCollectionsIds = DefaultEstimationCellCollectionCount.Id.ToString();
            }

            Database.SNfiEsta.CEstimationCell.ReLoad(
                           condition: $"{EstimationCellList.ColEstimationCellCollectionId.Name} IN ({strEstimationCellCollectionsIds})");

            // Identifikátory výpočetních buněk v kolekci
            IEnumerable<int> estimationCellsIds =
                Database.SNfiEsta.CEstimationCell.Data.AsEnumerable()
                .Select(a => a.Field<int>(columnName: EstimationCellList.ColId.Name));
            if (estimationCellsIds.Any())
            {
                strEstimationCellsIds =
                    estimationCellsIds
                        .Select(a => a.ToString())
                        .Aggregate((a, b) => $"{a},{b}");
                Database.SNfiEsta.TStratumInEstimationCell.ReLoad(
                    condition: $"{StratumInEstimationCellList.ColEstimationCellId.Name} IN({strEstimationCellsIds})");
            }
            else
            {
                Database.SNfiEsta.TStratumInEstimationCell.ReLoad(condition: $"false");
            }

            // Identifikátory výpočetních buněk v kolekci uvnitř některého ze strat
            IEnumerable<int> estimationCellsInStratumIds =
                Database.SNfiEsta.TStratumInEstimationCell.Data.AsEnumerable()
                .Select(a => a.Field<Nullable<int>>(columnName: StratumInEstimationCellList.ColEstimationCellId.Name))
                .Where(a => a != null)
                .Select(a => (int)a)
                .Distinct();

            // Omezení seznamu výpočetních buněk v kolekci na výpočetní buňky uvnitř strata
            if (estimationCellsInStratumIds.Any())
            {
                strEstimationCellsInStratumIds = estimationCellsInStratumIds
                    .Select(a => a.ToString())
                    .Aggregate((a, b) => $"{a},{b}");
                Database.SNfiEsta.CEstimationCell.ReLoad(
                       condition: $"{EstimationCellList.ColId.Name} IN ({strEstimationCellsInStratumIds})");
            }
            else
            {
                Database.SNfiEsta.CEstimationCell.ReLoad(condition: $"false");
            }
        }

        /// <summary>
        /// <para lang="cs">Resetuje nastavení modulu pro konfiguraci a výpočet odhadů na výchozí hodnoty</para>
        /// <para lang="en">Resets configuration and calculation estimates module settings to default values</para>
        /// </summary>
        /// <param name="displayMessage">
        /// <para lang="cs">Zobrazení zprávy o provedeném resetu nastavení modulu</para>
        /// <para lang="en">Display a message about the reset of the module settings</para>
        /// </param>
        public void Reset(bool displayMessage)
        {
            Setting = new Setting();

            if (displayMessage)
            {
                MessageBox.Show(
                    text: msgReset,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Připojit k databázi</para>
        /// <para lang="en">Connect to a database</para>
        /// </summary>
        private void Connect()
        {
            Initialize(
                controlOwner: ControlOwner,
                languageVersion: LanguageVersion,
                languageFile: LanguageFile,
                setting: Setting,
                withEventHandlers: false);

            Database.Postgres.Setting = Setting.DBSetting;
            Database.Connect(
                 applicationName: Setting.DBSetting.ApplicationName);
            Setting.DBSetting = Database.Postgres.Setting;

            pnlControls.Controls.Clear();

            // Pozn. je nutné spustit obě metody nelze použít zkrácené vyhodnocení logického výrazu
            // musí zde být operátor & (nelze &&)
            if (
                DisplayNfiEstaExtensionStatus() &
                DisplaySDesignExtensionStatus()
            )
            {
                ctrTargetVariable =
                    new ControlTargetVariable(controlOwner: this)
                    {
                        Dock = DockStyle.Fill
                    };

                ctrTargetVariable.NextClick +=
                    new EventHandler(
                        (sender, e) =>
                        {
                            ControlTargetVariable_ShowNext();
                        });

                LoadContent(verbose: true);
                ctrTargetVariable.LoadContent();

                pnlControls.Controls.Add(value: ctrTargetVariable);
            }
        }

        /// <summary>
        /// <para lang="cs">Odpojit od databáze</para>
        /// <para lang="en">Disconnect from database</para>
        /// </summary>
        public void Disconnect()
        {
            Database?.Postgres?.CloseConnection();
        }

        /// <summary>
        /// <para lang="cs">ThreadSave povolení přístupu k ovládacímu prvku</para>
        /// <para lang="en">ThreadSave enable access to the control</para>
        /// </summary>
        public void Enable()
        {
            if (InvokeRequired)
            {
                BeginInvoke(
                    method: (MethodInvoker)delegate ()
                    {
                        pnlMain.Enabled = true;
                        tlpMain.RowStyles[1].Height = 0;
                    });
            }
            else
            {
                pnlMain.Enabled = true;
                tlpMain.RowStyles[1].Height = 0;
            }
        }

        /// <summary>
        /// <para lang="cs">ThreadSave zakázání přístupu k ovládacímu prvku</para>
        /// <para lang="en">ThreadSave disable access to the control</para>
        /// </summary>
        public void Disable()
        {
            if (InvokeRequired)
            {
                BeginInvoke(
                    method: (MethodInvoker)delegate ()
                    {
                        pnlMain.Enabled = false;
                        tlpMain.RowStyles[1].Height = 40;
                    });
            }
            else
            {
                pnlMain.Enabled = false;
                tlpMain.RowStyles[1].Height = 40;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stav instalace extenze nfiesta_pg</para>
        /// <para lang="en">Method displays installation status of the database extension nfiesta_pg</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud v připojené databázi je nainstalovaná extenze nfiesta_pg
        /// v kompatibilní verzi s aplikací. Jinak false.</para>
        /// <para lang="en">It returns true, when there is installed database extension nfiesta_pg
        /// in compatible version with application.</para>
        /// </returns>
        private bool DisplayNfiEstaExtensionStatus()
        {
            lblStatus.Text = Database.Postgres.ConnectionInfo();

            // Není připojeno k databázi
            // No database connection
            if (!Database.Postgres.Initialized)
            {
                lblNfiestaExtension.Text = String.Empty;
                return false;
            }

            // Extenze nfiesta neexistuje
            // The nfiesta extension does not exist
            if (Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName] == null)
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text = msgNoDbExtension
                    .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName);
                return false;
            }

            // Extenze existuje
            ExtensionVersion dbExtensionVersion = new(
                version: Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Version);

            // Schéma neexistuje
            if (Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Schema == null)
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text = msgNoDbSchema
                    .Replace(oldValue: "$1", newValue: NfiEstaSchema.Name);
                return false;
            }

            // Neshoduje se minor verze
            if (!NfiEstaSchema.ExtensionVersion.MinorEquals(obj: dbExtensionVersion))
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text = msgInvalidDbVersion
                    .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: NfiEstaSchema.ExtensionVersion.ToString());
                return false;
            }

            // Neshoduje se patch verze
            if (!NfiEstaSchema.ExtensionVersion.Equals(obj: dbExtensionVersion))
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text = msgInvalidDbVersion
                     .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                     .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Version)
                     .Replace(oldValue: "$3", newValue: NfiEstaSchema.ExtensionVersion.ToString());
                return true;
            }

            // Extenze je OK
            lblNfiestaExtension.ForeColor = System.Drawing.SystemColors.ControlText;
            lblNfiestaExtension.Text = msgValidDbVersion
                .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                .Replace(oldValue: "$2", newValue: NfiEstaSchema.ExtensionVersion.ToString())
                .Replace(oldValue: "$3", newValue: NfiEstaSchema.Name);

            return true;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stav instalace extenze nfiesta_sdesign</para>
        /// <para lang="en">Method displays installation status of the database extension nfiesta_sdesign</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud v připojené databázi je nainstalovaná extenze nfiesta_sdesign
        /// v kompatibilní verzi s aplikací. Jinak false.</para>
        /// <para lang="en">It returns true, when there is installed database extension nfiesta_sdesign
        /// in compatible version with application.</para>
        /// </returns>
        private bool DisplaySDesignExtensionStatus()
        {
            lblStatus.Text = Database.Postgres.ConnectionInfo();

            // Není připojeno k databázi
            if (!Database.Postgres.Initialized)
            {
                lblSDesignExtension.Text = String.Empty;
                return false;
            }

            // Extenze v neexistuje
            if (Database.Postgres.Catalog.Extensions[ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName] == null)
            {
                lblSDesignExtension.ForeColor = System.Drawing.Color.Red;
                lblSDesignExtension.Text = msgNoDbExtension
                    .Replace(oldValue: "$1", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName);
                return false;
            }

            // Extenze existuje
            ExtensionVersion dbExtensionVersion = new(
                version: Database.Postgres.Catalog.Extensions[ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Version);

            // Schéma neexistuje
            if (Database.Postgres.Catalog.Extensions[ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Schema == null)
            {
                lblSDesignExtension.ForeColor = System.Drawing.Color.Red;
                lblSDesignExtension.Text = msgNoDbSchema
                    .Replace(oldValue: "$1", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.Name);
                return false;
            }

            // Neshoduje se minor verze
            if (!ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionVersion.MinorEquals(obj: dbExtensionVersion))
            {
                lblSDesignExtension.ForeColor = System.Drawing.Color.Red;
                lblSDesignExtension.Text = msgInvalidDbVersion
                    .Replace(oldValue: "$1", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionVersion.ToString());
                return false;
            }

            // Neshoduje se patch verze
            if (!ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionVersion.Equals(obj: dbExtensionVersion))
            {
                lblSDesignExtension.ForeColor = System.Drawing.Color.Red;
                lblSDesignExtension.Text = msgInvalidDbVersion
                     .Replace(oldValue: "$1", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName)
                     .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Version)
                     .Replace(oldValue: "$3", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionVersion.ToString());
                return true;
            }

            // Extenze je OK
            lblSDesignExtension.ForeColor = System.Drawing.SystemColors.ControlText;
            lblSDesignExtension.Text = msgValidDbVersion
                .Replace(oldValue: "$1", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionName)
                .Replace(oldValue: "$2", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.ExtensionVersion.ToString())
                .Replace(oldValue: "$3", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.Name);
            return true;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí nebo skryje stavový řádek</para>
        /// <para lang="en">Shows or hides status strip</para>
        /// </summary>
        public void DisplayStatusStrip()
        {
            if (Setting.StatusStripVisible)
            {
                ShowStatusStrip();
            }
            else
            {
                HideStatusStrip();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Show status strip</para>
        /// </summary>
        public void ShowStatusStrip()
        {
            Setting.StatusStripVisible = true;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Hide status strip</para>
        /// </summary>
        public void HideStatusStrip()
        {
            Setting.StatusStripVisible = false;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Povolí nebo zakáže zobrazování SQL příkazů</para>
        /// <para lang="en">Enables or disables the display of SQL statements</para>
        /// </summary>
        public void SwitchDisplaySQLCommands()
        {
            Setting.Verbose =
                !Setting.Verbose;

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: msgSQLCommandsDisplayEnabled,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    text: msgSQLCommandsDisplayDisabled,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_sdesign</para>
        /// <para lang="en">Displays data from extension nfiesta_sdesign"</para>
        /// </summary>
        private void SDesignExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgNoDatabaseConnection,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (Database.Postgres.Catalog.Schemas[ZaJi.NfiEstaPg.SDesign.SDesignSchema.Name] == null)
            {
                MessageBox.Show(
                   text: msgNoDbExtension.Replace(oldValue: "$1", newValue: ZaJi.NfiEstaPg.SDesign.SDesignSchema.Name),
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);
                return;
            }

            List<ZaJi.PostgreSQL.Catalog.DBStoredProcedure> storedProcedures =
                [.. Database.Postgres.Catalog
                    .Schemas[ZaJi.NfiEstaPg.SDesign.SDesignSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)];

            ZaJi.NfiEstaPg.SDesign.FormSDesign frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_results</para>
        /// <para lang="en">Displays data from extension nfiesta_results"</para>
        /// </summary>
        private void SResultsExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgNoDatabaseConnection,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (Database.Postgres.Catalog.Schemas[ZaJi.NfiEstaPg.Results.ResultsSchema.Name] == null)
            {
                MessageBox.Show(
                   text: msgNoDbExtension.Replace(oldValue: "$1", newValue: ZaJi.NfiEstaPg.Results.ResultsSchema.Name),
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);
                return;
            }

            List<ZaJi.PostgreSQL.Catalog.DBStoredProcedure> storedProcedures =
                [.. Database.Postgres.Catalog
                    .Schemas[ZaJi.NfiEstaPg.Results.ResultsSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)];

            ZaJi.NfiEstaPg.Results.FormSResults frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_target_data</para>
        /// <para lang="en">Displays data from extension nfiesta_target_data"</para>
        /// </summary>
        private void NfiEstaExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgNoDatabaseConnection,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (Database.Postgres.Catalog.Schemas[NfiEstaSchema.Name] == null)
            {
                MessageBox.Show(
                   text: msgNoDbExtension.Replace(oldValue: "$1", newValue: NfiEstaSchema.Name),
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);
                return;
            }

            List<ZaJi.PostgreSQL.Catalog.DBStoredProcedure> storedProcedures =
                [.. Database.Postgres.Catalog
                    .Schemas[NfiEstaSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)];

            FormNfiEsta frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba indikátoru"</para>
        /// <para lang="en">Displays the control following
        /// the control "Indicator selection"</para>
        /// </summary>
        private void ControlTargetVariable_ShowNext()
        {
            if (ctrTargetVariable == null)
            {
                return;
            }

            ctrUnit =
                new ControlUnit(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            CtrUnit.PreviousClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlUnit_ShowPrevious();
                    });

            CtrUnit.NextClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlUnit_ShowNext();
                    });

            CtrUnit.LoadContent();

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrUnit);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Volba jednotky a typu odhadu"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Unit and estimate type selection"</para>
        /// </summary>
        private void ControlUnit_ShowPrevious()
        {
            if (ctrTargetVariable == null)
            {
                return;
            }

            LoadContent(verbose: false);
            ctrTargetVariable.LoadContent();

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrTargetVariable);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba jednotky a typu odhadu"</para>
        /// <para lang="en">Displays the control following
        /// the control "Unit and estimate type selection"</para>
        /// </summary>
        private void ControlUnit_ShowNext()
        {
            if (ctrUnit == null)
            {
                return;
            }

            if (CtrUnit.SelectedUnitOfMeasure == null)
            {
                return;
            }

            if (CtrUnit.SelectedUnitOfMeasure.IsTotal)
            {
                // Konfigurace úhrnu
                ctrVariable = new ControlVariable(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

                CtrVariable.PreviousClick +=
                    new EventHandler(
                        (sender, e) =>
                        {
                            ControlVariable_ShowPrevious();
                        });

                CtrVariable.NextClick +=
                    new EventHandler(
                        (sender, e) =>
                        {
                            ControlVariable_ShowNext();
                        });

                CtrVariable.LoadContent();

                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrVariable);
            }

            else
            {
                // Konfigurace podílu
                ctrVariableRatio = new ControlVariableRatio(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

                CtrVariableRatio.PreviousClick +=
                    new EventHandler(
                        (sender, e) =>
                        {
                            ControlVariableRatio_ShowPrevious();
                        });

                CtrVariableRatio.NextClick +=
                    new EventHandler(
                        (sender, e) =>
                        {
                            ControlVariableRatio_ShowNext();
                        });

                CtrVariableRatio.LoadContent();

                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrVariableRatio);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Volba atributových kategorií pro odhad úhrnu"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Attribute categories selection for total estimate"</para>
        /// </summary>
        private void ControlVariable_ShowPrevious()
        {
            if (ctrUnit == null)
            {
                return;
            }

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrUnit);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba atributových kategorií pro odhad úhrnu"</para>
        /// <para lang="en">Displays the control following
        /// the control "Attribute categories selection for total estimate"</para>
        /// </summary>
        private void ControlVariable_ShowNext()
        {
            if (ctrVariable == null)
            {
                return;
            }

            ctrEstimateResult = new ControlEstimateResult(controlOwner: this)
            {
                Dock = DockStyle.Fill
            };

            CtrEstimateResult.PreviousClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlEstimateResult_ShowPrevious();
                    });

            CtrEstimateResult.LoadContent();

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrEstimateResult);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Volba atributových kategorií pro odhad podílu"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Attribute categories selection for ratio estimate"</para>
        /// </summary>
        private void ControlVariableRatio_ShowPrevious()
        {
            if (ctrUnit == null)
            {
                return;
            }

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrUnit);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba atributových kategorií pro odhad podílu"</para>
        /// <para lang="en">Displays the control following
        /// the control "Attribute categories selection for ratio estimate"</para>
        /// </summary>
        private void ControlVariableRatio_ShowNext()
        {
            if (ctrVariableRatio == null)
            {
                return;
            }

            ctrEstimateResult = new ControlEstimateResult(controlOwner: this)
            {
                Dock = DockStyle.Fill
            };

            CtrEstimateResult.PreviousClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlEstimateResult_ShowPrevious();
                    });

            CtrEstimateResult.LoadContent();

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrEstimateResult);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Konfigurace a výpočet odhadů"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Configuration and calculation estimates"</para>
        /// </summary>
        private void ControlEstimateResult_ShowPrevious()
        {
            if (ctrEstimateResult == null)
            {
                return;
            }

            if (CtrEstimateResult.SelectedUnitOfMeasure == null)
            {
                return;
            }

            if (CtrEstimateResult.SelectedUnitOfMeasure.IsTotal)
            {
                // Konfigurace úhrnu
                if (ctrVariable == null)
                {
                    return;
                }

                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrVariable);
            }

            else
            {
                // Konfigurace podílu
                if (ctrVariableRatio == null)
                {
                    return;
                }

                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrVariableRatio);
            }
        }

        #endregion Methods

    }

}