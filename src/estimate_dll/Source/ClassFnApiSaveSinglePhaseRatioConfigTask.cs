﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;

namespace ZaJi
{
    namespace ModuleEstimate
    {

        /// <summary>
        /// <para lang="cs">Úkol pracovního vlákna pro konfiguraci jednofázových odhadů podílu</para>
        /// <para lang="en">Task for worker thread for configuration one-phase estimates of ratios</para>
        /// </summary>
        internal class FnApiSaveSinglePhaseRatioConfigTask
            : ThreadTask<OLAPRatioEstimate>
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Agregovaná skupina výpočetních buněk</para>
            /// <para lang="en">Aggregated group of estimation cells</para>
            /// </summary>
            private AggregatedEstimationCellGroup aggregatedEstimationCellGroup;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="estimate">
            /// <para lang="cs">Konfigurovaný odhad podílu</para>
            /// <para lang="en">Configured ratio estimate</para>
            /// </param>
            /// <param name="aggregatedEstimationCellGroup">
            /// <para lang="cs">Agregovaná skupina výpočetních buněk</para>
            /// <para lang="en">Aggregated group of estimation cells</para>
            /// </param>
            public FnApiSaveSinglePhaseRatioConfigTask(
                OLAPRatioEstimate estimate,
                AggregatedEstimationCellGroup aggregatedEstimationCellGroup)
                : base(element: estimate, priority: 1)
            {
                AggregatedEstimationCellGroup = aggregatedEstimationCellGroup;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Agregovaná skupina výpočetních buněk</para>
            /// <para lang="en">Aggregated group of estimation cells</para>
            /// </summary>
            public AggregatedEstimationCellGroup AggregatedEstimationCellGroup
            {
                get
                {
                    return aggregatedEstimationCellGroup;
                }
                private set
                {
                    aggregatedEstimationCellGroup = value;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true|false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not FnApiSaveRatioConfigTask Type, return False
                if (obj is not FnApiSaveSinglePhaseRatioConfigTask)
                {
                    return false;
                }

                return
                    ((FnApiSaveSinglePhaseRatioConfigTask)obj).Element.Equals(obj: Element) &&
                    ((FnApiSaveSinglePhaseRatioConfigTask)obj).Priority.Equals(obj: Priority) &&
                    ((FnApiSaveSinglePhaseRatioConfigTask)obj).AggregatedEstimationCellGroup.Equals(obj: AggregatedEstimationCellGroup);
            }

            /// <summary>
            /// Returns a hash code for the current object
            /// </summary>
            /// <returns>Hash code for the current object</returns>
            public override int GetHashCode()
            {
                return
                    Element.GetHashCode() ^
                    Priority.GetHashCode() ^
                    AggregatedEstimationCellGroup.GetHashCode();
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>String that represents the current object</returns>
            public override string ToString()
            {
                return (Element?.Id == null)
                    ? $"{nameof(Element)}: {ZaJi.PostgreSQL.Functions.StrNull}"
                    : $"{nameof(Element)}: {Element.Id}";
            }

            #endregion Methods

        }

    }
}