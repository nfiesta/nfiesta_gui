﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleEstimate
{

    partial class ControlTargetVariable
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlRowsCount = new System.Windows.Forms.Panel();
            this.lblRowsCount = new System.Windows.Forms.Label();
            this.pnlNext = new System.Windows.Forms.Panel();
            this.btnNext = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splCaption = new System.Windows.Forms.SplitContainer();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.pnlInnerWorkSpace = new System.Windows.Forms.Panel();
            this.splTopic = new System.Windows.Forms.SplitContainer();
            this.grpTopic = new System.Windows.Forms.GroupBox();
            this.pnlTopic = new System.Windows.Forms.Panel();
            this.pnlTargetVariableSelector = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlRowsCount.SuspendLayout();
            this.pnlNext.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).BeginInit();
            this.splCaption.Panel1.SuspendLayout();
            this.splCaption.Panel2.SuspendLayout();
            this.splCaption.SuspendLayout();
            this.pnlInnerWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splTopic)).BeginInit();
            this.splTopic.Panel1.SuspendLayout();
            this.splTopic.Panel2.SuspendLayout();
            this.splTopic.SuspendLayout();
            this.grpTopic.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.SystemColors.Control;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 1);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(3, 3);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(954, 534);
            this.tlpMain.TabIndex = 7;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.Controls.Add(this.pnlRowsCount, 0, 0);
            this.tlpButtons.Controls.Add(this.pnlNext, 2, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 494);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(954, 40);
            this.tlpButtons.TabIndex = 10;
            // 
            // pnlRowsCount
            // 
            this.pnlRowsCount.Controls.Add(this.lblRowsCount);
            this.pnlRowsCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRowsCount.Location = new System.Drawing.Point(0, 0);
            this.pnlRowsCount.Margin = new System.Windows.Forms.Padding(0);
            this.pnlRowsCount.Name = "pnlRowsCount";
            this.pnlRowsCount.Padding = new System.Windows.Forms.Padding(5);
            this.pnlRowsCount.Size = new System.Drawing.Size(634, 40);
            this.pnlRowsCount.TabIndex = 17;
            // 
            // lblRowsCount
            // 
            this.lblRowsCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRowsCount.Location = new System.Drawing.Point(5, 5);
            this.lblRowsCount.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblRowsCount.Name = "lblRowsCount";
            this.lblRowsCount.Size = new System.Drawing.Size(624, 30);
            this.lblRowsCount.TabIndex = 0;
            this.lblRowsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlNext
            // 
            this.pnlNext.Controls.Add(this.btnNext);
            this.pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNext.Location = new System.Drawing.Point(794, 0);
            this.pnlNext.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNext.Name = "pnlNext";
            this.pnlNext.Padding = new System.Windows.Forms.Padding(5);
            this.pnlNext.Size = new System.Drawing.Size(160, 40);
            this.pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnNext.Location = new System.Drawing.Point(5, 5);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 15;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlWorkSpace.Controls.Add(this.splCaption);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 494);
            this.pnlWorkSpace.TabIndex = 9;
            // 
            // splCaption
            // 
            this.splCaption.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCaption.Location = new System.Drawing.Point(0, 0);
            this.splCaption.Margin = new System.Windows.Forms.Padding(0);
            this.splCaption.Name = "splCaption";
            this.splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            this.splCaption.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splCaption.Panel1.Controls.Add(this.pnlCaption);
            this.splCaption.Panel1MinSize = 0;
            // 
            // splCaption.Panel2
            // 
            this.splCaption.Panel2.Controls.Add(this.pnlInnerWorkSpace);
            this.splCaption.Panel2MinSize = 0;
            this.splCaption.Size = new System.Drawing.Size(954, 491);
            this.splCaption.SplitterDistance = 40;
            this.splCaption.SplitterWidth = 1;
            this.splCaption.TabIndex = 8;
            // 
            // pnlCaption
            // 
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Size = new System.Drawing.Size(954, 40);
            this.pnlCaption.TabIndex = 11;
            // 
            // pnlInnerWorkSpace
            // 
            this.pnlInnerWorkSpace.BackColor = System.Drawing.SystemColors.Control;
            this.pnlInnerWorkSpace.Controls.Add(this.splTopic);
            this.pnlInnerWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInnerWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.pnlInnerWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlInnerWorkSpace.Name = "pnlInnerWorkSpace";
            this.pnlInnerWorkSpace.Size = new System.Drawing.Size(954, 450);
            this.pnlInnerWorkSpace.TabIndex = 11;
            // 
            // splTopic
            // 
            this.splTopic.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splTopic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splTopic.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splTopic.Location = new System.Drawing.Point(0, 0);
            this.splTopic.Margin = new System.Windows.Forms.Padding(0);
            this.splTopic.Name = "splTopic";
            // 
            // splTopic.Panel1
            // 
            this.splTopic.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splTopic.Panel1.Controls.Add(this.grpTopic);
            this.splTopic.Panel1.Padding = new System.Windows.Forms.Padding(5);
            this.splTopic.Panel1MinSize = 0;
            // 
            // splTopic.Panel2
            // 
            this.splTopic.Panel2.Controls.Add(this.pnlTargetVariableSelector);
            this.splTopic.Panel2MinSize = 0;
            this.splTopic.Size = new System.Drawing.Size(954, 450);
            this.splTopic.SplitterDistance = 250;
            this.splTopic.SplitterWidth = 1;
            this.splTopic.TabIndex = 9;
            // 
            // grpTopic
            // 
            this.grpTopic.Controls.Add(this.pnlTopic);
            this.grpTopic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpTopic.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpTopic.Location = new System.Drawing.Point(5, 5);
            this.grpTopic.Margin = new System.Windows.Forms.Padding(5);
            this.grpTopic.Name = "grpTopic";
            this.grpTopic.Padding = new System.Windows.Forms.Padding(5);
            this.grpTopic.Size = new System.Drawing.Size(240, 440);
            this.grpTopic.TabIndex = 3;
            this.grpTopic.TabStop = false;
            this.grpTopic.Text = "grpTopic";
            // 
            // pnlTopic
            // 
            this.pnlTopic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlTopic.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlTopic.Location = new System.Drawing.Point(5, 20);
            this.pnlTopic.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTopic.Name = "pnlTopic";
            this.pnlTopic.Size = new System.Drawing.Size(230, 415);
            this.pnlTopic.TabIndex = 0;
            // 
            // pnlTargetVariableSelector
            // 
            this.pnlTargetVariableSelector.BackColor = System.Drawing.SystemColors.Control;
            this.pnlTargetVariableSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTargetVariableSelector.Location = new System.Drawing.Point(0, 0);
            this.pnlTargetVariableSelector.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTargetVariableSelector.Name = "pnlTargetVariableSelector";
            this.pnlTargetVariableSelector.Size = new System.Drawing.Size(703, 450);
            this.pnlTargetVariableSelector.TabIndex = 0;
            // 
            // ControlTargetVariable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlTargetVariable";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlRowsCount.ResumeLayout(false);
            this.pnlNext.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splCaption.Panel1.ResumeLayout(false);
            this.splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).EndInit();
            this.splCaption.ResumeLayout(false);
            this.pnlInnerWorkSpace.ResumeLayout(false);
            this.splTopic.Panel1.ResumeLayout(false);
            this.splTopic.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splTopic)).EndInit();
            this.splTopic.ResumeLayout(false);
            this.grpTopic.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.Panel pnlInnerWorkSpace;
        private System.Windows.Forms.SplitContainer splTopic;
        private System.Windows.Forms.GroupBox grpTopic;
        private System.Windows.Forms.Panel pnlTopic;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlRowsCount;
        private System.Windows.Forms.Panel pnlTargetVariableSelector;
        private System.Windows.Forms.Label lblRowsCount;
    }

}
