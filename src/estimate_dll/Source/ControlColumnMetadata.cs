﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení metadat sloupce tabulky</para>
    /// <para lang="en">Control for displaying table column metadata</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlColumnMetadata
            : UserControl, INfiEstaControl, IEstimateControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Barva pozadí ovládacího prvku</para>
        /// <para lang="en">Control back color</para>
        /// </summary>
        private static readonly Color ControlBackColor = Color.White;

        /// <summary>
        /// <para lang="cs">Barva pozadí vybraného ovládacího prvku</para>
        /// <para lang="en">Selected control back color</para>
        /// </summary>
        private static readonly Color SelectedControlBackColor = Color.DarkBlue;

        /// <summary>
        /// <para lang="cs">Barva písma ovládacího prvku</para>
        /// <para lang="en">Control text color</para>
        /// </summary>
        private static readonly Color ControlForeColor = Color.Black;

        /// <summary>
        /// <para lang="cs">Barva písma vybraného ovládacího prvku</para>
        /// <para lang="en">Selected control text color</para>
        /// </summary>
        private static readonly Color SelectedControlForeColor = Color.White;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Metadata sloupce tabulky</para>
        /// <para lang="en">Table column metadata</para>
        /// </summary>
        private ColumnMetadata metadata;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Stisknutí tlačítka myši"</para>
        /// <para lang="en">"Mouse Down" event</para>
        /// </summary>
        public event MouseEventHandler ControlMouseDown;

        /// <summary>
        /// <para lang="cs">Událost "Pohyb myši"</para>
        /// <para lang="en">"Mouse Move" event</para>
        /// </summary>
        public event MouseEventHandler ControlMouseMove;

        /// <summary>
        /// <para lang="cs">Událost "Uvolnění tlačítka myši"</para>
        /// <para lang="en">"Mouse Up" event</para>
        /// </summary>
        public event MouseEventHandler ControlMouseUp;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata sloupce tabulky</para>
        /// <para lang="en">Table column metadata</para>
        /// </param>
        public ControlColumnMetadata(
            Control controlOwner,
            ColumnMetadata metadata
            )
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                metadata: metadata);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Metadata sloupce tabulky</para>
        /// <para lang="en">Table column metadata</para>
        /// </summary>
        public ColumnMetadata Metadata
        {
            get
            {
                return metadata ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Metadata)} must not be null.",
                        paramName: nameof(Metadata));
            }
            set
            {
                metadata = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Metadata)} must not be null.",
                        paramName: nameof(Metadata));
            }
        }

        /// <summary>
        /// <para lang="cs">Je ovládací prvek vybraný?</para>
        /// <para lang="en">Is the control selected?</para>
        /// </summary>
        public bool Selected
        {
            get
            {
                return
                    pnlMain.BackColor == SelectedControlBackColor;
            }
            set
            {
                if (value)
                {
                    pnlMain.BackColor = SelectedControlBackColor;
                    pnlMain.ForeColor = SelectedControlForeColor;

                    lblDisplayIndex.BackColor = SelectedControlBackColor;
                    lblDisplayIndex.ForeColor = SelectedControlForeColor;

                    chkVisible.BackColor = SelectedControlBackColor;
                    chkVisible.ForeColor = SelectedControlForeColor;

                    lblColumnName.BackColor = SelectedControlBackColor;
                    lblColumnName.ForeColor = SelectedControlForeColor;

                    btnEdit.BackColor = SelectedControlBackColor;
                    btnEdit.ForeColor = SelectedControlForeColor;
                }
                else
                {
                    pnlMain.BackColor = ControlBackColor;
                    pnlMain.ForeColor = ControlForeColor;

                    lblDisplayIndex.BackColor = ControlBackColor;
                    lblDisplayIndex.ForeColor = ControlForeColor;

                    chkVisible.BackColor = ControlBackColor;
                    chkVisible.ForeColor = ControlForeColor;

                    lblColumnName.BackColor = ControlBackColor;
                    lblColumnName.ForeColor = ControlForeColor;

                    btnEdit.BackColor = ControlBackColor;
                    btnEdit.ForeColor = ControlForeColor;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Je ovládací prvek zaškrtnutý? (read-only)</para>
        /// <para lang="en">Is the control checked? (read-only)</para>
        /// </summary>
        public bool Checked
        {
            get
            {
                return
                   chkVisible.Checked;
            }
        }

        /// <summary>
        /// <para lang="cs">Umožní editovat popisky? (read-only)</para>
        /// <para lang="en">Enable edit labels? (read-only)</para>
        /// </summary>
        public bool EnableEditLabels
        {
            get
            {
                return btnEdit.Visible;
            }
            set
            {
                btnEdit.Visible = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnEdit),            String.Empty },
                        { nameof(chkVisible),         String.Empty },
                        { nameof(lblColumnName),      "$1" },
                        { nameof(lblDisplayIndex),    "$1." }
                    }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: nameof(ControlColumnMetadata),
                            out Dictionary<string, string> dictNational)
                                ? dictNational
                                : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnEdit),            String.Empty },
                        { nameof(chkVisible),         String.Empty },
                        { nameof(lblColumnName),      "$1" },
                        { nameof(lblDisplayIndex),    "$1." }
                    }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: nameof(ControlColumnMetadata),
                            out Dictionary<string, string> dictInternational)
                                ? dictInternational
                                : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata sloupce tabulky</para>
        /// <para lang="en">Table column metadata</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ColumnMetadata metadata)
        {
            ControlOwner = controlOwner;
            Metadata = metadata;
            Selected = false;
            chkVisible.Checked = Metadata.Visible;
            EnableEditLabels = false;

            InitializeLabels();

            lblDisplayIndex.MouseDown += (sender, e) =>
            {
                ControlMouseDown?.Invoke(
                    sender: this,
                    e: new MouseEventArgs(
                        button: e.Button,
                        clicks: e.Clicks,
                        x: e.X + lblDisplayIndex.Left,
                        y: e.Y + lblDisplayIndex.Top,
                        delta: e.Delta));
            };

            lblColumnName.MouseDown += (sender, e) =>
            {
                ControlMouseDown?.Invoke(
                     sender: this,
                     e: new MouseEventArgs(
                         button: e.Button,
                         clicks: e.Clicks,
                         x: e.X + lblColumnName.Left,
                         y: e.Y + lblColumnName.Top,
                         delta: e.Delta));
            };

            lblDisplayIndex.MouseMove += (sender, e) =>
            {
                ControlMouseMove?.Invoke(
                    sender: this,
                    e: new MouseEventArgs(
                        button: e.Button,
                        clicks: e.Clicks,
                        x: e.X + lblDisplayIndex.Left,
                        y: e.Y + lblDisplayIndex.Top,
                        delta: e.Delta));
            };

            lblColumnName.MouseMove += (sender, e) =>
            {
                ControlMouseMove?.Invoke(
                     sender: this,
                     e: new MouseEventArgs(
                         button: e.Button,
                         clicks: e.Clicks,
                         x: e.X + lblColumnName.Left,
                         y: e.Y + lblColumnName.Top,
                         delta: e.Delta));
            };

            lblDisplayIndex.MouseUp += (sender, e) =>
            {
                ControlMouseUp?.Invoke(
                    sender: this,
                    e: new MouseEventArgs(
                        button: e.Button,
                        clicks: e.Clicks,
                        x: e.X + lblDisplayIndex.Left,
                        y: e.Y + lblDisplayIndex.Top,
                        delta: e.Delta));
            };

            lblColumnName.MouseUp += (sender, e) =>
            {
                ControlMouseUp?.Invoke(
                     sender: this,
                     e: new MouseEventArgs(
                         button: e.Button,
                         clicks: e.Clicks,
                         x: e.X + lblColumnName.Left,
                         y: e.Y + lblColumnName.Top,
                         delta: e.Delta));
            };

            btnEdit.Click += (sender, e) =>
            {
                FormColumnEdit form = new(controlOwner: this);

                if (form.ShowDialog() == DialogResult.OK)
                {
                    InitializeLabels();
                }
            };
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            chkVisible.Text =
                labels.TryGetValue(key: nameof(chkVisible),
                out string chkVisibleText)
                    ? chkVisibleText
                    : String.Empty;


            lblColumnName.Text =
                (labels.TryGetValue(key: nameof(lblColumnName),
                out string lblColumnNameText)
                    ? lblColumnNameText
                    : String.Empty)
                .Replace(
                    oldValue: "$1",
                    newValue:
                        (LanguageVersion == LanguageVersion.National) ? Metadata.ToolTipTextCs :
                        (LanguageVersion == LanguageVersion.International) ? Metadata.ToolTipTextEn :
                        String.Empty);

            lblDisplayIndex.Text =
                (labels.TryGetValue(key: nameof(lblDisplayIndex),
                out string lblDisplayIndexText)
                    ? lblDisplayIndexText
                    : String.Empty)
              .Replace(
                  oldValue: "$1",
                  newValue: (Metadata.DisplayIndex + 1).ToString());

            btnEdit.Text =
                labels.TryGetValue(key: nameof(btnEdit),
                out string btnEditText)
                    ? btnEditText
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}