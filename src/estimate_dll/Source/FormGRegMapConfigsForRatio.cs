﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleEstimate
{

    /// <summary>
    /// <para lang="cs">
    /// Formulář pro konfiguraci regresních odhadů podílů:
    /// Výběr kombinace konfigurací regresních odhadů úhrnů,
    /// která může být použita pro konfiguraci regresního odhadu podílu
    /// </para>
    /// <para lang="en">
    /// Form for configuring regression estimates of ratios:
    /// Selecting combinations of configurations of GREG-map totals
    /// that can be considered for the ratio
    /// </para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormGRegMapConfigsForRatio
        : Form, INfiEstaControl, IEstimateControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">
        /// Seznam platných kombinací skupin panelů a referenčních období, typu parametrizační oblasti,
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů.
        /// </para>
        /// <para lang="en">
        /// List of valid combinations of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals.
        /// </para>
        /// </summary>
        private TFnApiGetGRegMapConfigsForRatioList combinations;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        #endregion Private Fields


        #region Construtor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormGRegMapConfigsForRatio(
            Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IEstimateControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IEstimateControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IEstimateControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IEstimateControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">
        /// Seznam platných kombinací skupin panelů a referenčních období, typu parametrizační oblasti,
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů.
        /// </para>
        /// <para lang="en">
        /// List of valid combinations of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals.
        /// </para>
        /// </summary>
        public TFnApiGetGRegMapConfigsForRatioList Combinations
        {
            get
            {
                return combinations ??
                    new TFnApiGetGRegMapConfigsForRatioList(database: Database);
            }
            set
            {
                combinations = value ??
                    new TFnApiGetGRegMapConfigsForRatioList(database: Database);

                OnEdit = true;
                pnlWorkSpace.Controls.Clear();

                TableLayoutPanel tableLayoutPanel = new()
                {
                    ColumnCount = 1,
                    Dock = DockStyle.Top,
                    Height = 0,
                    RowCount = Combinations.Items.Count + 1,
                };

                tableLayoutPanel.ColumnStyles.Add(
                    columnStyle: new ColumnStyle(
                        sizeType: SizeType.Percent,
                        width: 100F));

                // Požadované seřazení položek v seznamu
                IEnumerable<TFnApiGetGRegMapConfigsForRatio> items =
                    Combinations.Items
                            .OrderBy(a => (a.AllEstimatesConfigurable == null) ? 3 : (bool)a.AllEstimatesConfigurable ? 1 : 2)  // true first, false second, null third
                            .ThenBy(a => (LanguageVersion == LanguageVersion.National) ? a.PanelRefYearSetGroupLabelCs ?? String.Empty : a.PanelRefYearSetGroupLabelEn ?? String.Empty)
                            .ThenBy(a => (a.ForceSynthetic == null) ? 3 : (bool)a.ForceSynthetic ? 2 : 1)  // false first, true second, null third,
                            .ThenBy(a => (LanguageVersion == LanguageVersion.National) ? a.ParamAreaTypeLabelCs ?? String.Empty : a.ParamAreaTypeLabelEn ?? String.Empty)
                            .ThenBy(a => (LanguageVersion == LanguageVersion.National) ? a.NumeratorModelLabelCs ?? String.Empty : a.NumeratorModelLabelEn ?? String.Empty)
                            .ThenBy(a => (a.NumeratorModelSigma == null) ? 3 : (bool)a.NumeratorModelSigma ? 1 : 2) // true first, false second, null third
                            .ThenBy(a => (LanguageVersion == LanguageVersion.National) ? a.DenominatorModelLabelCs ?? String.Empty : a.DenominatorModelLabelEn ?? String.Empty)
                            .ThenBy(a => (a.DenominatorModelSigma == null) ? 3 : (bool)a.DenominatorModelSigma ? 1 : 2); // true first, false second, null third

                int rowNumber = 0;
                foreach (TFnApiGetGRegMapConfigsForRatio combination in items)
                {
                    ControlGRegMapConfigsForRatio ctrCombination = new(controlOwner: this)
                    {
                        Combination = combination,
                        Dock = DockStyle.Fill,
                        Id = rowNumber + 1
                    };
                    ctrCombination.OnSelect += new EventHandler(
                        (sender, e) =>
                        {
                            if (!OnEdit)
                            {
                                foreach (ControlGRegMapConfigsForRatio c in CombinationControls)
                                {
                                    if (c != (ControlGRegMapConfigsForRatio)sender)
                                    {
                                        c.Checked = false;
                                    }
                                }
                            }
                            EnableNext();
                        });
                    ctrCombination.CreateControl();

                    tableLayoutPanel.RowStyles.Add(
                        rowStyle: new RowStyle(
                            sizeType: SizeType.Absolute,
                            height: ctrCombination.Height));

                    tableLayoutPanel.Controls.Add(
                        control: ctrCombination,
                        column: 0,
                        row: rowNumber++);

                    tableLayoutPanel.Height += ctrCombination.Height;
                }

                tableLayoutPanel.RowStyles.Add(
                        rowStyle: new RowStyle(
                            sizeType: SizeType.Percent,
                            height: 100F));

                tableLayoutPanel.CreateControl();
                pnlWorkSpace.Controls.Add(value: tableLayoutPanel);

                ControlGRegMapConfigsForRatio ctrFirstConfigurableCombination = CombinationControls
                    .Where(a => a.Combination.AllEstimatesConfigurable ?? false)
                    .OrderBy(a => a.Id)
                    .FirstOrDefault<ControlGRegMapConfigsForRatio>();
                if (ctrFirstConfigurableCombination != null)
                {
                    ctrFirstConfigurableCombination.Checked = true;
                }

                OnEdit = false;

                EnableNext();
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Zobrazené ovládací prvky
        /// pro kombinace skupin panelů a referenčních období, typu parametrizační oblasti
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů
        /// </para>
        /// <para lang="en">
        /// Displayed controls
        /// for combinations of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals
        /// </para>
        /// </summary>
        private IEnumerable<ControlGRegMapConfigsForRatio> CombinationControls
        {
            get
            {
                if (pnlWorkSpace.Controls.Count == 0)
                {
                    return [];
                }
                if (pnlWorkSpace.Controls[0] is not TableLayoutPanel)
                {
                    return [];
                }

                return ((TableLayoutPanel)pnlWorkSpace.Controls[0])
                    .Controls.OfType<ControlGRegMapConfigsForRatio>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybraný ovládací prvek
        /// pro kombinace skupin panelů a referenčních období, typu parametrizační oblasti
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů
        /// </para>
        /// <para lang="en">
        /// Selected control
        /// for combinations of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals
        /// </para>
        /// </summary>
        private ControlGRegMapConfigsForRatio SelectedCombinationControl
        {
            get
            {
                return
                    CombinationControls
                    .Where(a => a.Checked)
                    .FirstOrDefault<ControlGRegMapConfigsForRatio>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybraná kombinace skupin panelů a referenčních období, typu parametrizační oblasti,
        /// syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů.
        /// </para>
        /// <para lang="en">
        /// Selected combination of panel-refyearset group, param. area type,
        /// force synthetic, model and sigma of already configured GREG-map totals.
        /// </para>
        /// </summary>
        public TFnApiGetGRegMapConfigsForRatio SelectedCombination
        {
            get
            {
                return
                    SelectedCombinationControl?.Combination;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegMapConfigsForRatio),
                            "Výběr kombinace konfigurací regresních odhadů úhrnů " },
                        { nameof(btnOK),
                            "Uložit " },
                        { nameof(btnCancel),
                            "Zrušit " },
                        { nameof(lblNoSelectedCombination), String.Concat(
                            "Nebyly nalezeny žádné odpovídající konfigurace regresního odhadu úhrnu pro čitatele a jmenovatele ",
                            "některých nebo všech kombinací výpočetních buněk a atributových členění. ",
                            "Nejprve prosím nakonfigurujte chybějící regresní odhady úhrnů nebo ",
                            "zredukujte seznam kombinací výpočetních buněk a atributových členění. ") },
                        { nameof(lblNoNumeratorConfigurations),
                            "Seznam konfigurací regresních odhadů úhrnu čitatele je prázdný. " },
                        { nameof(lblNoDenominatorConfigurations),
                            "Seznam konfigurací regresních odhadů úhrnu jmenovatele je prázdný. " },
                        { nameof(lblDifferentListLength),
                            "Délky seznamů konfigurací regresních odhadů úhrnu čitatele a jmenovatele jsou odlišné. " },
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormGRegMapConfigsForRatio),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormGRegMapConfigsForRatio),
                            "Selecting combinations of configurations of GREG-map totals " },
                        { nameof(btnOK),
                            "Save " },
                        { nameof(btnCancel),
                            "Cancel " },
                        { nameof(lblNoSelectedCombination), String.Concat(
                            "No matching GREG-map configurations found for numerator and denominator ",
                            "of some or all combinations of estimation cells and variables. ",
                            "Please configure the missing GREG-map total estimates first or ",
                            "reduce the list of estimation cell and variable combinations. ") },
                        { nameof(lblNoNumeratorConfigurations),
                            "The list of GREG-map configurations of totals for numerator is empty. " },
                        { nameof(lblNoDenominatorConfigurations),
                            "The list of GREG-map configurations of totals for denominator is empty. " },
                        { nameof(lblDifferentListLength),
                            "The lengths of the lists of GREG-map configurations of totals for numerator and denominator are different. " },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormGRegMapConfigsForRatio),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Combinations =
                new TFnApiGetGRegMapConfigsForRatioList(
                    database: Database);
            OnEdit = false;

            InitializeLabels();

            LoadContent();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
              languageVersion: LanguageVersion,
              languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormGRegMapConfigsForRatio),
                out string frmGRegMapConfigsForRatioText)
                    ? frmGRegMapConfigsForRatioText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            lblNoSelectedCombination.Text =
               labels.TryGetValue(key: nameof(lblNoSelectedCombination),
               out string lblNoSelectedCombinationText)
                   ? lblNoSelectedCombinationText
                   : String.Empty;

            lblNoNumeratorConfigurations.Text =
               labels.TryGetValue(key: nameof(lblNoNumeratorConfigurations),
               out string lblNoNumeratorConfigurationsText)
                   ? lblNoNumeratorConfigurationsText
                   : String.Empty;

            lblNoDenominatorConfigurations.Text =
               labels.TryGetValue(key: nameof(lblNoDenominatorConfigurations),
               out string lblNoDenominatorConfigurationsText)
                   ? lblNoDenominatorConfigurationsText
                   : String.Empty;

            lblDifferentListLength.Text =
               labels.TryGetValue(key: nameof(lblDifferentListLength),
               out string lblDifferentListLengthText)
                   ? lblDifferentListLengthText
                   : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            lblNoSelectedCombination.Visible = false;
            lblNoNumeratorConfigurations.Visible = false;
            lblNoDenominatorConfigurations.Visible = false;
            lblDifferentListLength.Visible = false;

            lblNoSelectedCombination.Dock = DockStyle.None;
            lblNoNumeratorConfigurations.Dock = DockStyle.None;
            lblNoDenominatorConfigurations.Dock = DockStyle.None;
            lblDifferentListLength.Dock = DockStyle.None;

            // Není vybraná žádná kombinace konfigurací
            if (SelectedCombination == null)
            {
                btnOK.Enabled = false;
                tlpWorkSpace.RowStyles[tlpWorkSpace.RowStyles.Count - 1].Height = 40;
                lblNoSelectedCombination.Dock = DockStyle.Fill;
                lblNoSelectedCombination.Visible = true;
                return;
            }

            // Pole konfigurací pro čitatel je prázdné
            if ((SelectedCombination.TotalEstimateConfNumeratorList == null) ||
                (SelectedCombination.TotalEstimateConfNumeratorList.Count == 0))
            {
                btnOK.Enabled = false;
                tlpWorkSpace.RowStyles[tlpWorkSpace.RowStyles.Count - 1].Height = 40;
                lblNoNumeratorConfigurations.Dock = DockStyle.Fill;
                lblNoNumeratorConfigurations.Visible = true;
                return;
            }

            // Pole konfigurací pro jmenovatel je prázdné
            if ((SelectedCombination.TotalEstimateConfDenominatorList == null) ||
                (SelectedCombination.TotalEstimateConfDenominatorList.Count == 0))
            {
                btnOK.Enabled = false;
                tlpWorkSpace.RowStyles[tlpWorkSpace.RowStyles.Count - 1].Height = 40;
                lblNoDenominatorConfigurations.Dock = DockStyle.Fill;
                lblNoDenominatorConfigurations.Visible = true;
                return;
            }

            // Délka polí konfigurací pro jmenovatel a čitatel se liší
            if (SelectedCombination.TotalEstimateConfNumeratorList.Count !=
                SelectedCombination.TotalEstimateConfDenominatorList.Count)
            {
                btnOK.Enabled = false;
                tlpWorkSpace.RowStyles[tlpWorkSpace.RowStyles.Count - 1].Height = 40;
                lblDifferentListLength.Dock = DockStyle.Fill;
                lblDifferentListLength.Visible = true;
                return;
            }

            tlpWorkSpace.RowStyles[tlpWorkSpace.RowStyles.Count - 1].Height = 0;
            btnOK.Enabled = true;
            return;
        }

        #endregion Methods

    }

}