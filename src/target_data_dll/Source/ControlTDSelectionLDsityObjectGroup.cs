﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba objektů lokálních hustot"</para>
    /// <para lang="en">Control "Selection of local density objects"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTDSelectionLDsityObjectGroup
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;


        // Datové položky získané z uložených procedur:
        // Data items obtained from stored procedures:

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot ve skupinách objektů</para>
        /// <para lang="en">Local density objects in groups</para>
        /// </summary>
        private Dictionary<TDLDsityObjectGroup, TDLDsityObjectList> dictFnGetLDsityObjectForLDsityObjectGroup;


        // Výsledkem výběru v této komponentě jsou
        // skupina objektů lokálních hustot a její objekty:
        // The selection results in this component are
        // local density object group and its objects:

        /// <summary>
        /// <para lang="cs">Vybraná skupina objektů lokálních hustot</para>
        /// <para lang="en">Selected group of local density objects</para>
        /// </summary>
        private TDLDsityObjectGroup selectedLocalDensityObjectGroup;

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot ve vybrané skupině</para>
        /// <para lang="en">Local density objects in the selected group</para>
        /// </summary>
        private TDLDsityObjectList selectedLocalDensityObjects;

        private string msgNone = String.Empty;
        private string msgNoneSelectedLDsityObjectGroup = String.Empty;
        private string msgCannotDeleteSelectedLDsityObjectGroup = String.Empty;
        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        /// <summary>
        /// <para lang="cs">Událost změny vybrané skupiny objektů lokálních hustot</para>
        /// <para lang="en">Change event for a selected group of local density objects</para>
        /// </summary>
        public event EventHandler SelectedLocalDensityObjectGroupChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTDSelectionLDsityObjectGroup(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        // Výsledkem výběru v této komponentě jsou
        // skupina objektů lokálních hustot a její objekty:
        // The selection results in this component are
        // local density object group and its objects:

        /// <summary>
        /// <para lang="cs">Vybraná skupina objektů lokálních hustot</para>
        /// <para lang="en">Selected group of local density objects</para>
        /// </summary>
        public TDLDsityObjectGroup SelectedLocalDensityObjectGroup
        {
            get
            {
                return selectedLocalDensityObjectGroup;
            }
            set
            {
                selectedLocalDensityObjectGroup = value;

                selectedLocalDensityObjects =
                    (selectedLocalDensityObjectGroup == null)
                        ? null
                        : dictFnGetLDsityObjectForLDsityObjectGroup.TryGetValue(
                                key: selectedLocalDensityObjectGroup,
                                out TDLDsityObjectList ldsityObjectForSelectedLocalDensityGroup)
                            ? ldsityObjectForSelectedLocalDensityGroup
                            : null;

                InitializeListBoxLDsityObject();
                SetCaption();
                SetControlTexts();
                EnableButtonNext();

                SelectedLocalDensityObjectGroupChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot ve vybrané skupině (read-only)</para>
        /// <para lang="en">Local density objects in the selected group (read-only)</para>
        /// </summary>
        public TDLDsityObjectList SelectedLocalDensityObjects
        {
            get
            {
                return selectedLocalDensityObjects;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnLDsityObjectEdit),                        "Editovat objekty lokálních hustot" },
                        { nameof(btnLDsityObjectGroupEdit),                   "Editovat skupinu objektů lokálních hustot" },
                        { nameof(btnLDsityObjectGroupAdd),                    "Vytvořit novou skupinu objektů lokálních hustot" },
                        { nameof(btnLDsityObjectGroupDelete),                 "Smazat vybranou skupinu objektů lokálních hustot" },
                        { nameof(btnNext),                                    "Další" },
                        { nameof(grpLDsityObject),                            "Objekty lokálních hustot:" },
                        { nameof(grpLDsityObjectGroup),                       "Skupiny objektů lokálních hustot:" },
                        { nameof(grpLDsityObjectGroupInfo),                   String.Empty },
                        { nameof(lblMainCaption),                             "Volba objektů lokálních hustot" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),        "Vybraná skupina objektů lokálních hustot:" },
                        { nameof(lblLDsityObjectGroupIsEmpty),                "Skupina objektů lokálních hustot je prázdná." },
                        { nameof(lblInfoIDCaption),                           "Identifikační číslo:"},
                        { nameof(lblInfoLabelCsCaption),                      "Zkratka (národní):" },
                        { nameof(lblInfoDescriptionCsCaption),                "Popis (národní):" },
                        { nameof(lblInfoLabelEnCaption),                      "Zkratka (anglická):" },
                        { nameof(lblInfoDescriptionEnCaption),                "Popis (anglický):" },
                        { nameof(lstLDsityObject),                            "ExtendedLabelCs" },
                        { nameof(lstLDsityObjectGroup),                       "ExtendedLabelCs" },
                        { nameof(tsrLDsityObject),                            String.Empty },
                        { nameof(tsrLDsityObjectGroup),                       String.Empty },
                        { nameof(msgNone),                                    "žádná" },
                        { nameof(msgNoneSelectedLDsityObjectGroup),           "Žádná skupina objektů lokálních hustot není vybraná." },
                        { nameof(msgCannotDeleteSelectedLDsityObjectGroup),   "Vybranou skupinu objektů lokálních hustot nelze smazat." },
                        { nameof(msgLabelCsIsEmpty),                          "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),                    "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),                          "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),                    "Popis (anglický) musí být vyplněn." },
                        { nameof(msgLabelCsExists),                           "Použitá zkratka (národní) již existuje pro jinou skupinu objektů lokálních hustot." },
                        { nameof(msgDescriptionCsExists),                     "Použitý popis (národní) již existuje pro jinou skupinu objektů lokálních hustot." },
                        { nameof(msgLabelEnExists),                           "Použitá zkratka (anglická) již existuje pro jinou skupinu objektů lokálních hustot." },
                        { nameof(msgDescriptionEnExists),                     "Použitý popis (anglický) již existuje pro jinou skupinu objektů lokálních hustot." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionLDsityObjectGroup),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnLDsityObjectEdit),                        "Edit local density objects" },
                        { nameof(btnLDsityObjectGroupEdit),                   "Edit groups of local density objects" },
                        { nameof(btnLDsityObjectGroupAdd),                    "Create a new group of local density objects" },
                        { nameof(btnLDsityObjectGroupDelete),                 "Delete the selected group of local density objects" },
                        { nameof(btnNext),                                    "Next" },
                        { nameof(grpLDsityObject),                            "Local density objects:" },
                        { nameof(grpLDsityObjectGroup),                       "Groups of local density objects:" },
                        { nameof(grpLDsityObjectGroupInfo),                   String.Empty },
                        { nameof(lblMainCaption),                             "Selection of local density objects" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),        "Selected local density object group:" },
                        { nameof(lblLDsityObjectGroupIsEmpty),                "Group of local density objects is empty." },
                        { nameof(lblInfoIDCaption),                           "ID:"},
                        { nameof(lblInfoLabelCsCaption),                      "Label (national):"},
                        { nameof(lblInfoDescriptionCsCaption),                "Description (national):" },
                        { nameof(lblInfoLabelEnCaption),                      "Label (English):" },
                        { nameof(lblInfoDescriptionEnCaption),                "Description (English):" },
                        { nameof(lstLDsityObject),                            "ExtendedLabelEn" },
                        { nameof(lstLDsityObjectGroup),                       "ExtendedLabelEn" },
                        { nameof(tsrLDsityObject),                            String.Empty },
                        { nameof(tsrLDsityObjectGroup),                       String.Empty },
                        { nameof(msgNone),                                    "none" },
                        { nameof(msgNoneSelectedLDsityObjectGroup),           "Group of local density objects is not selected." },
                        { nameof(msgCannotDeleteSelectedLDsityObjectGroup),   "Selected group of local density objects cannot be deleted." },
                        { nameof(msgLabelCsIsEmpty),                          "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),                    "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),                          "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),                    "Description (English) cannot be empty." },
                        { nameof(msgLabelCsExists),                           "This label (national) already exists for another group of local density objects." },
                        { nameof(msgDescriptionCsExists),                     "This description (national) already exists for another group of local density objects." },
                        { nameof(msgLabelEnExists),                           "This label (English) already exists for another group of local density objects." },
                        { nameof(msgDescriptionEnExists),                     "This description (English) already exists for another group of local density objects." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionLDsityObjectGroup),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            dictFnGetLDsityObjectForLDsityObjectGroup = [];

            selectedLocalDensityObjectGroup = null;
            selectedLocalDensityObjects = null;

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();
            SelectedLocalDensityObjectGroup = null;

            lstLDsityObjectGroup.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SelectLDsityObjectGroup();
                    }
                });

            btnLDsityObjectGroupEdit.Click += new EventHandler(
                (sender, e) => { UpdateLDsityObjectGroup(); });

            btnLDsityObjectGroupDelete.Click += new EventHandler(
                (sender, e) => { DeleteLDsityObjectGroup(); });

            btnLDsityObjectGroupAdd.Click += new EventHandler(
                (sender, e) => { InsertLDsityObjectGroup(); });

            btnLDsityObjectEdit.Click += new EventHandler(
                (sender, e) => { UpdateLDsityObject(); });

            btnNext.Click += new EventHandler(
                (sender, e) => { NextClick?.Invoke(sender: sender, e: e); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnLDsityObjectEdit.Text =
                labels.TryGetValue(key: nameof(btnLDsityObjectEdit),
                out string btnLDsityObjectEditText)
                    ? btnLDsityObjectEditText
                    : String.Empty;

            btnLDsityObjectGroupEdit.Text =
                labels.TryGetValue(key: nameof(btnLDsityObjectGroupEdit),
                out string btnLDsityObjectGroupEditText)
                    ? btnLDsityObjectGroupEditText
                    : String.Empty;

            btnLDsityObjectGroupAdd.Text =
                labels.TryGetValue(key: nameof(btnLDsityObjectGroupAdd),
                    out string btnLDsityObjectGroupAddText)
                        ? btnLDsityObjectGroupAddText
                        : String.Empty;

            btnLDsityObjectGroupDelete.Text =
                labels.TryGetValue(key: nameof(btnLDsityObjectGroupDelete),
                    out string btnLDsityObjectGroupDeleteText)
                        ? btnLDsityObjectGroupDeleteText
                        : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                    out string btnNextText)
                        ? btnNextText
                        : String.Empty;

            grpLDsityObject.Text =
                labels.TryGetValue(key: nameof(grpLDsityObject),
                    out string grpLDsityObjectText)
                        ? grpLDsityObjectText
                        : String.Empty;

            grpLDsityObjectGroup.Text =
                labels.TryGetValue(key: nameof(grpLDsityObjectGroup),
                    out string grpLDsityObjectGroupText)
                        ? grpLDsityObjectGroupText
                        : String.Empty;

            grpLDsityObjectGroupInfo.Text =
                labels.TryGetValue(key: nameof(grpLDsityObjectGroupInfo),
                    out string grpLDsityObjectGroupInfoText)
                        ? grpLDsityObjectGroupInfoText
                        : String.Empty;

            lblMainCaption.Text =
                labels.TryGetValue(key: nameof(lblMainCaption),
                    out string lblMainCaptionText)
                        ? lblMainCaptionText
                        : String.Empty;

            lblSelectedLDsityObjectGroupCaption.Text =
                labels.TryGetValue(key: nameof(lblSelectedLDsityObjectGroupCaption),
                    out string lblSelectedLDsityObjectGroupCaptionText)
                        ? lblSelectedLDsityObjectGroupCaptionText
                        : String.Empty;

            lblLDsityObjectGroupIsEmpty.Text =
                labels.TryGetValue(key: nameof(lblLDsityObjectGroupIsEmpty),
                    out string lblLDsityObjectGroupIsEmptyText)
                        ? lblLDsityObjectGroupIsEmptyText
                        : String.Empty;

            lblInfoIDCaption.Text =
                labels.TryGetValue(key: nameof(lblInfoIDCaption),
                    out string lblInfoIDCaptionText)
                        ? lblInfoIDCaptionText
                        : String.Empty;

            lblInfoLabelCsCaption.Text =
                labels.TryGetValue(key: nameof(lblInfoLabelCsCaption),
                    out string lblInfoLabelCsCaptionText)
                        ? lblInfoLabelCsCaptionText
                        : String.Empty;

            lblInfoDescriptionCsCaption.Text =
                labels.TryGetValue(key: nameof(lblInfoDescriptionCsCaption),
                    out string lblInfoDescriptionCsCaptionText)
                        ? lblInfoDescriptionCsCaptionText
                        : String.Empty;

            lblInfoLabelEnCaption.Text =
                labels.TryGetValue(key: nameof(lblInfoLabelEnCaption),
                    out string lblInfoLabelEnCaptionText)
                        ? lblInfoLabelEnCaptionText
                        : String.Empty;

            lblInfoDescriptionEnCaption.Text =
                labels.TryGetValue(key: nameof(lblInfoDescriptionEnCaption),
                    out string lblInfoDescriptionEnCaptionText)
                        ? lblInfoDescriptionEnCaptionText
                        : String.Empty;

            tsrLDsityObject.Text =
                labels.TryGetValue(key: nameof(tsrLDsityObject),
                    out string tsrLDsityObjectText)
                        ? tsrLDsityObjectText
                        : String.Empty;

            tsrLDsityObjectGroup.Text =
                labels.TryGetValue(key: nameof(tsrLDsityObjectGroup),
                    out string tsrLDsityObjectGroupText)
                        ? tsrLDsityObjectGroupText
                        : String.Empty;

            msgNone =
                labels.TryGetValue(key: nameof(msgNone),
                        out msgNone)
                            ? msgNone
                            : String.Empty;

            msgNoneSelectedLDsityObjectGroup =
                labels.TryGetValue(key: nameof(msgNoneSelectedLDsityObjectGroup),
                        out msgNoneSelectedLDsityObjectGroup)
                            ? msgNoneSelectedLDsityObjectGroup
                            : String.Empty;

            msgCannotDeleteSelectedLDsityObjectGroup =
                labels.TryGetValue(key: nameof(msgCannotDeleteSelectedLDsityObjectGroup),
                        out msgCannotDeleteSelectedLDsityObjectGroup)
                            ? msgCannotDeleteSelectedLDsityObjectGroup
                            : String.Empty;

            msgLabelCsIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                        out msgLabelCsIsEmpty)
                            ? msgLabelCsIsEmpty
                            : String.Empty;

            msgDescriptionCsIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                        out msgDescriptionCsIsEmpty)
                            ? msgDescriptionCsIsEmpty
                            : String.Empty;

            msgLabelEnIsEmpty =
                labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                        out msgLabelEnIsEmpty)
                            ? msgLabelEnIsEmpty
                            : String.Empty;

            msgDescriptionEnIsEmpty =
                labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                        out msgDescriptionEnIsEmpty)
                            ? msgDescriptionEnIsEmpty
                            : String.Empty;

            msgLabelCsExists =
                labels.TryGetValue(key: nameof(msgLabelCsExists),
                        out msgLabelCsExists)
                            ? msgLabelCsExists
                            : String.Empty;

            msgDescriptionCsExists =
                labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                        out msgDescriptionCsExists)
                            ? msgDescriptionCsExists
                            : String.Empty;

            msgLabelEnExists =
               labels.TryGetValue(key: nameof(msgLabelEnExists),
                       out msgLabelEnExists)
                           ? msgLabelEnExists
                           : String.Empty;

            msgDescriptionEnExists =
               labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                       out msgDescriptionEnExists)
                           ? msgDescriptionEnExists
                           : String.Empty;

            onEdit = true;

            lstLDsityObject.DisplayMember =
                labels.TryGetValue(key: nameof(lstLDsityObject),
                    out string lstLDsityObjectDisplayMember)
                        ? lstLDsityObjectDisplayMember
                        : ID;

            lstLDsityObjectGroup.DisplayMember =
                labels.TryGetValue(key: nameof(lstLDsityObjectGroup),
                    out string lstLDsityObjectGroupDisplayMember)
                        ? lstLDsityObjectGroupDisplayMember
                        : ID;

            onEdit = false;

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Database.STargetData.CLDsityObjectGroup
                = TDFunctions.FnGetLDsityObjectGroup.Execute(database: Database);

            Database.STargetData.CLDsityObject
                = TDFunctions.FnGetLDsityObject.Execute(database: Database);

            // Data se načítají pro dialog FormGroupOfObjectsNew
            Database.STargetData.CArealOrPopulation
                = TDFunctions.FnGetArealOrPopulation.Execute(database: Database);

            dictFnGetLDsityObjectForLDsityObjectGroup.Clear();

            foreach (TDLDsityObjectGroup group in
                Database.STargetData.CLDsityObjectGroup.Items)
            {
                dictFnGetLDsityObjectForLDsityObjectGroup.Add(
                    key: group,
                    value: TDFunctions.FnGetLDsityObjectForLDsityObjectGroup.Execute(
                        database: Database,
                        ldsityObjectGroupId: group.Id));
            }

            InitializeListBoxLDsityObjectGroup();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis ovládacího prvku</para>
        /// <para lang="en">Sets the control caption</para>
        /// </summary>
        private void SetCaption()
        {
            if (SelectedLocalDensityObjectGroup == null)
            {

                lblSelectedLDsityObjectGroupValue.Text = msgNone;
                return;
            }

            string selectedLocalDensityObjectGroupId = SelectedLocalDensityObjectGroup.Id.ToString();
            string selectedLocalDensityObjectGroupCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedLocalDensityObjectGroup.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedLocalDensityObjectGroup.LabelEn :
                    String.Empty;
            lblSelectedLDsityObjectGroupValue.Text =
                $"({selectedLocalDensityObjectGroupId}) {selectedLocalDensityObjectGroupCaption}";
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost tlačítka "Další"</para>
        /// <para lang="en">Enable or Disable button "Next"</para>
        /// </summary>
        private void EnableButtonNext()
        {
            btnNext.Enabled =
                (SelectedLocalDensityObjectGroup != null) &&
                (SelectedLocalDensityObjects != null) &&
                 SelectedLocalDensityObjects.Items.Count != 0;
        }

        /// <summary>
        /// <para lang="cs">Do ListBoxu vyplní seznam skupin objektů lokálních hustot</para>
        /// <para lang="en">Fills the ListBox with a list of local density object groups</para>
        /// </summary>
        private void InitializeListBoxLDsityObjectGroup()
        {
            Dictionary<string, string> labels = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

            onEdit = true;
            lstLDsityObjectGroup.DataSource = Database.STargetData.CLDsityObjectGroup.Items;
            lstLDsityObjectGroup.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLDsityObjectGroup),
                    out string lstLDsityObjectGroupDisplayMember)
                        ? lstLDsityObjectGroupDisplayMember
                        : ID;
            onEdit = false;

            SelectLDsityObjectGroup();
        }

        /// <summary>
        /// <para lang="cs">Do ListBoxu vyplní seznam objektů lokálních hustot ve vybrané skupině</para>
        /// <para lang="en">Fills the ListBox with the list of local density objects in the selected group</para>
        /// </summary>
        private void InitializeListBoxLDsityObject()
        {
            if (SelectedLocalDensityObjects == null)
            {
                lstLDsityObject.Visible = false;
                lblLDsityObjectGroupIsEmpty.Visible = true;
                return;
            }

            if (SelectedLocalDensityObjects.Items.Count == 0)
            {
                lstLDsityObject.Visible = false;
                lblLDsityObjectGroupIsEmpty.Visible = true;
                return;
            }

            Dictionary<string, string> labels = Dictionary(
                     languageVersion: LanguageVersion,
                     languageFile: LanguageFile);

            onEdit = true;
            lstLDsityObject.DataSource = SelectedLocalDensityObjects.Items;
            lstLDsityObject.DisplayMember =
                labels.TryGetValue(key: nameof(lstLDsityObject),
                    out string lstLDsityObjectDisplayMember)
                        ? lstLDsityObjectDisplayMember
                        : ID;
            onEdit = false;

            lstLDsityObject.Visible = true;
            lblLDsityObjectGroupIsEmpty.Visible = false;
        }

        /// <summary>
        /// <para lang="cs">Spouští se v případě, že byla v ListBoxu LDsityObjectGroup vybrána skupina objektů lokálních hustot</para>
        /// <para lang="en">Fires if a group of local density objects was selected in the LDsityObjectGroup ListBox</para>
        /// </summary>
        private void SelectLDsityObjectGroup()
        {
            if (lstLDsityObjectGroup.SelectedItem != null)
            {
                SelectedLocalDensityObjectGroup = (TDLDsityObjectGroup)lstLDsityObjectGroup.SelectedItem;
                ShowTsrLDsityObjectGroup();
            }
            else
            {
                SelectedLocalDensityObjectGroup = null;
            }
        }

        /// <summary>
        /// <para lang="cs">Provede výběr LDsityObjectGroup v ListBoxu</para>
        /// <para lang="en">It selects item in ListBox LDsityObjectGroup</para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikátor skupiny objektů lokálních hustot</para>
        /// <para lang="en">Local density object group identifier</para>
        /// </param>
        private void SelectLDsityObjectGroup(int id)
        {
            // Výběr editované položky po aktualizaci databáze
            // Selecting an edited item after updating the database
            if (lstLDsityObjectGroup.Items
                    .OfType<TDLDsityObjectGroup>()
                    .Where(a => a.Id == id)
                    .Any())
            {
                lstLDsityObjectGroup.SelectedItem =
                    lstLDsityObjectGroup.Items
                    .OfType<TDLDsityObjectGroup>()
                    .Where(a => a.Id == id)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí panel tlačítek pro ListBox se seznamem skupin objektů lokálních hustot</para>
        /// <para lang="en">Displays a button bar for ListBox with a list of local density object groups</para>
        /// </summary>
        private void ShowTsrLDsityObjectGroup()
        {
            tsrLDsityObjectGroup.Enabled = true;
            tsrLDsityObjectGroup.Visible = true;
            tlpLDsityObjectGroup.RowStyles[0].Height = 25;
            tlpLDsityObject.RowStyles[0].Height = 25;
        }

        /// <summary>
        /// <para lang="cs">Nastaví textové popisky vybrané skupiny objektů lokálních hustot</para>
        /// <para lang="en">Sets the text labels of the selected group of local density objects</para>
        /// </summary>
        private void SetControlTexts()
        {
            if (SelectedLocalDensityObjectGroup != null)
            {
                lblInfoIDValue.Text = SelectedLocalDensityObjectGroup.Id.ToString();
                lblLabelCsValue.Text = SelectedLocalDensityObjectGroup.LabelCs;
                lblDescriptionCsValue.Text = SelectedLocalDensityObjectGroup.DescriptionCs;
                lblLabelEnValue.Text = SelectedLocalDensityObjectGroup.LabelEn;
                lblDescriptionEnValue.Text = SelectedLocalDensityObjectGroup.DescriptionEn;

                tlpLDsityObjectGroupInfo.RowStyles[3].Height = 30 +
                    15 * (SelectedLocalDensityObjectGroup.DescriptionCs.Split('\n').Length - 1);

                tlpLDsityObjectGroupInfo.RowStyles[5].Height = 30 +
                    15 * (SelectedLocalDensityObjectGroup.DescriptionEn.Split('\n').Length - 1);
            }
            else
            {
                lblInfoIDValue.Text = String.Empty;
                lblLabelCsValue.Text = String.Empty;
                lblDescriptionCsValue.Text = String.Empty;
                lblLabelEnValue.Text = String.Empty;
                lblDescriptionEnValue.Text = String.Empty;
                tlpLDsityObjectGroupInfo.RowStyles[2].Height = 30;
                tlpLDsityObjectGroupInfo.RowStyles[4].Height = 30;
            }
        }

        /// <summary>
        /// <para lang="cs">Zapíše změnu popisků skupiny objektů lokálních hustot do databáze (pomocí formuláře pro editace číselníku)</para>
        /// <para lang="en">Writes a change to the labels of a group of local density objects to the database (by form for edit lookup tables)</para>
        /// </summary>
        private void UpdateLDsityObjectGroup()
        {
            if (SelectedLocalDensityObjectGroup == null)
            {
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CLDsityObjectGroup,
                displayedItemsIds: [SelectedLocalDensityObjectGroup.Id]);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                int id = SelectedLocalDensityObjectGroup.Id;
                LoadContent();
                SelectLDsityObjectGroup(id);
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové skupiny objektů lokálních hustot</para>
        /// <para lang="en">Create a new group of local density objects</para>
        /// </summary>
        private void InsertLDsityObjectGroup()
        {
            FormLDsityObjectGroupNew frm = new(
                controlOwner: this);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání existující skupiny objektů lokálních hustot</para>
        /// <para lang="en">Delete an existing group of local density objects</para>
        /// </summary>
        private void DeleteLDsityObjectGroup()
        {
            if (SelectedLocalDensityObjectGroup == null)
            {
                // Žádná skupina objektů lokálních hustot není vybraná
                // No group of local density objects is selected
                MessageBox.Show(
                    text: msgNoneSelectedLDsityObjectGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (!Database.STargetData.CLDsityObjectGroup.Items
                .Where(a => a.Id == SelectedLocalDensityObjectGroup.Id).Any())
            {
                // Vybraná skupina objektů lokálních hustot není v databázové tabulce (nemůže nastat)
                // The selected group of local density objects is not in the database table (cannot occur)
                MessageBox.Show(
                    text: msgNoneSelectedLDsityObjectGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (TDFunctions.FnTryDeleteLDsityObjectGroup.Execute(
                    database: Database,
                    ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id))
            {
                TDFunctions.FnDeleteLDsityObjectGroup.Execute(
                    database: Database,
                    ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnTryDeleteLDsityObjectGroup.CommandText,
                        caption: TDFunctions.FnTryDeleteLDsityObjectGroup.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    MessageBox.Show(
                        text: TDFunctions.FnDeleteLDsityObjectGroup.CommandText,
                        caption: TDFunctions.FnDeleteLDsityObjectGroup.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                LoadContent();
            }
            else
            {
                // Vybranou skupinu objektů lokálních hustot nelze smazat
                // The selected group of local density objects cannot be deleted
                MessageBox.Show(
                    text: msgCannotDeleteSelectedLDsityObjectGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnTryDeleteLDsityObjectGroup.CommandText,
                        caption: TDFunctions.FnTryDeleteLDsityObjectGroup.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Zapíše změnu objektu lokální hustoty do databáze</para>
        /// <para lang="en">Writes a local density object change to the database</para>
        /// </summary>
        private void UpdateLDsityObject()
        {
            if (!lstLDsityObject.Items.OfType<TDLDsityObject>().Any())
            {
                // Seznam je prázdný
                // List is empty
                return;
            }

            FormLookupTable frmLookupTable = new(
                    controlOwner: this,
                    lookupTable: Database.STargetData.CLDsityObject,
                    displayedItemsIds: lstLDsityObject.Items
                                        .OfType<TDLDsityObject>()
                                        .Select(a => (Nullable<int>)a.Id)
                                        .ToList<Nullable<int>>());

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                int id = (SelectedLocalDensityObjectGroup != null) ?
                    SelectedLocalDensityObjectGroup.Id : 0;
                LoadContent();
                SelectLDsityObjectGroup(id);
            }
        }

        #endregion Method

    }

}