﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Skupina klasifikačních pravidel pro jednu doménu</para>
        /// <para lang="en">Collection of classification rules for one domain</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        internal class ClassificationRuleCollection<TDomain, TCategory>
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Identifikační číslo domény</para>
            /// <para lang="en">Domain identifier</para>
            /// </summary>
            private int domainId;

            /// <summary>
            /// <para lang="cs">Popisek domény v národním jazyce</para>
            /// <para lang="en">Domain label in national language</para>
            /// </summary>
            private string domainLabelCs;

            /// <summary>
            /// <para lang="cs">Popis domény v národním jazyce</para>
            /// <para lang="en">Domain description in national language</para>
            /// </summary>
            private string domainDescriptionCs;

            /// <summary>
            /// <para lang="cs">Popisek domény anglický</para>
            /// <para lang="en">Domain label in English</para>
            /// </summary>
            private string domainLabelEn;

            /// <summary>
            /// <para lang="cs">Popis domény anglický</para>
            /// <para lang="en">Domain description in English</para>
            /// </summary>
            private string domainDescriptionEn;

            /// <summary>
            /// <para lang="cs">Seznam kategorií v doméně</para>
            /// <para lang="en">List of categories in domain</para>
            /// </summary>
            private List<CategoryJSON> categories;

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            private JsonSerializerOptions serializerOptions;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">
            /// Prázdný konstruktor
            /// (je potřeba pro deserializaci objektu z JSON souboru)</para>
            /// <para lang="en">
            /// Empty constructor
            /// (needed to deserialize an object from a JSON file)</para>
            /// </summary>
            public ClassificationRuleCollection()
            {
                SerializerOptions = null;
                DomainId = 0;
                DomainLabelCs = String.Empty;
                DomainDescriptionCs = String.Empty;
                DomainLabelEn = String.Empty;
                DomainDescriptionEn = String.Empty;
                Categories = [];
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="domain">
            /// <para lang="cs">Doména</para>
            /// <para lang="en">Domain</para>
            /// </param>
            /// <param name="categories">
            /// <para lang="cs">Seznam kategorií v doméně</para>
            /// <para lang="en">List of categories in domain</para>
            /// </param>
            /// <param name="ldsityObjectsForADSP">
            /// <para lang="cs">Seznam objektů lokálních hustot pro třídění</para>
            /// <para lang="en">List of local density objects for classification</para>
            /// </param>
            /// <param name="dtClassificationRules">
            /// <para lang="cs">Tabulka klasifikačních pravidel</para>
            /// <para lang="en">Classification rules data table</para>
            /// </param>
            public ClassificationRuleCollection(
                TDomain domain,
                List<TCategory> categories,
                List<TDLDsityObject> ldsityObjectsForADSP,
                DataTable dtClassificationRules)
            {
                SerializerOptions = null;
                Initialize(
                    domain: domain,
                    categories: categories,
                    ldsityObjectsForADSP: ldsityObjectsForADSP,
                    dtClassificationRules: dtClassificationRules);
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Identifikační číslo domény</para>
            /// <para lang="en">Domain identifier</para>
            /// </summary>
            public int DomainId
            {
                get
                {
                    return domainId;
                }
                set
                {
                    domainId = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Popisek domény v národním jazyce</para>
            /// <para lang="en">Domain label in national language</para>
            /// </summary>
            public string DomainLabelCs
            {
                get
                {
                    return domainLabelCs;
                }
                set
                {
                    domainLabelCs = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Popis domény v národním jazyce</para>
            /// <para lang="en">Domain description in national language</para>
            /// </summary>
            public string DomainDescriptionCs
            {
                get
                {
                    return domainDescriptionCs;
                }
                set
                {
                    domainDescriptionCs = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Rozšířený popisek domény v národním jazyce</para>
            /// <para lang="en">Domain extended label in national language</para>
            /// </summary>
            [JsonIgnore]
            public string DomainExtendedLabelCs
            {
                get
                {
                    return $"{DomainId} - {DomainLabelCs}";
                }
            }


            /// <summary>
            /// <para lang="cs">Popisek domény anglický</para>
            /// <para lang="en">Domain label in English</para>
            /// </summary>
            public string DomainLabelEn
            {
                get
                {
                    return domainLabelEn;
                }
                set
                {
                    domainLabelEn = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Popis domény anglický</para>
            /// <para lang="en">Domain description in English</para>
            /// </summary>
            public string DomainDescriptionEn
            {
                get
                {
                    return domainDescriptionEn;
                }
                set
                {
                    domainDescriptionEn = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Rozšířený popisek domény v angličtině</para>
            /// <para lang="en">Domain extended label in English</para>
            /// </summary>
            [JsonIgnore]
            public string DomainExtendedLabelEn
            {
                get
                {
                    return $"{DomainId} - {DomainLabelEn}";
                }
            }


            /// <summary>
            /// <para lang="cs">Seznam kategorií v doméně</para>
            /// <para lang="en">List of categories in domain</para>
            /// </summary>
            public List<CategoryJSON> Categories
            {
                get
                {
                    return categories;
                }
                set
                {
                    categories = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Seznam objektů lokálních hustot pro třídění z JSON souboru</para>
            /// <para lang="en">List of local density objects for classification from JSON file</para>
            /// </summary>
            [JsonIgnore]
            public List<LocalDensityObjectJSON> LDsityObjectsForADSP
            {
                get
                {
                    return Categories
                        .SelectMany(a => a.LDsityObjectsForADSP)
                        .Distinct()
                        .ToList<LocalDensityObjectJSON>();
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam objektů lokálních hustot pro třídění z JSON souboru</para>
            /// <para lang="en">List of local density objects for classification from JSON file</para>
            /// </summary>
            [JsonIgnore]
            public List<LocalDensityObjectJSON> LDsityObjectsForADSPExtended
            {
                get
                {
                    return [.. LDsityObjectsForADSP
                        .Concat(second:
                            [
                                new(
                                    id: 0,
                                    labelCs: "žádný",
                                    descriptionCs: "žádný",
                                    labelEn: "none",
                                    descriptionEn: "none",
                                    positiveClassificationRule: null,
                                    negativeClassificationRule: null)
                            ])
                        .Distinct()
                        .OrderBy(a => a.LocalDensityObjectId)];
                }
            }


            /// <summary>
            /// <para lang="cs">Je skupina klasifikačních pravidel prázdná</para>
            /// <para lang="en">Is collection of classification rules empty</para>
            /// </summary>
            [JsonIgnore]
            public bool IsEmpty
            {
                get
                {
                    return Categories.Count == 0;
                }
            }


            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            [JsonIgnore]
            private JsonSerializerOptions SerializerOptions
            {
                get
                {
                    return serializerOptions ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
                set
                {
                    serializerOptions = value ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
            }

            /// <summary>
            /// <para lang="cs">Serializace objektu do formátu JSON</para>
            /// <para lang="en">Object serialization to JSON format</para>
            /// </summary>
            [JsonIgnore]
            public string JSON
            {
                get
                {
                    return JsonSerializer.Serialize(
                        value: this,
                        options: SerializerOptions);
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Obnovení objektu z formátu JSON</para>
            /// <para lang="en">Restoring an object from JSON format</para>
            /// </summary>
            /// <param name="json">
            /// <para lang="cs">JSON text</para>
            /// <para lang="en">JSON text</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Skupina klasifikačních pravidel pro jednu doménu</para>
            /// <para lang="en">Collection of classification rules for one domain</para>
            /// </returns>
            public static ClassificationRuleCollection<TDomain, TCategory> Deserialize(string json)
            {
                try
                {
                    return
                        JsonSerializer.Deserialize<ClassificationRuleCollection<TDomain, TCategory>>(json: json);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Exclamation);

                    return null;
                }
            }

            /// <summary>
            /// <para lang="cs">Načte objekt ze souboru json</para>
            /// <para lang="en">Loads object from json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Skupina klasifikačních pravidel pro jednu doménu</para>
            /// <para lang="en">Collection of classification rules for one domain</para>
            /// </returns>
            public static ClassificationRuleCollection<TDomain, TCategory> LoadFromFile(string path)
            {
                try
                {
                    return Deserialize(
                        json: System.IO.File.ReadAllText(
                            path: path,
                            encoding: Encoding.UTF8));
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return null;
                }
            }

            /// <summary>
            /// <para lang="cs">Prázdná datová tabulka pro uložení klasifikačních pravidel</para>
            /// <para lang="en">Empty data table for classification rules</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Prázdná datová tabulka pro uložení klasifikačních pravidel</para>
            /// <para lang="en">Empty data table for classification rules</para>
            /// </returns>
            public static DataTable EmptyDataTable()
            {
                DataTable table = new();
                table.Columns.Add(columnName: "Category", type: Type.GetType(typeName: "System.Int32"));
                table.Columns.Add(columnName: "LDsityObjectADSP", type: Type.GetType(typeName: "System.Int32"));
                table.Columns.Add(columnName: "PositiveRule", type: Type.GetType(typeName: "System.String"));
                table.Columns.Add(columnName: "NegativeRule", type: Type.GetType(typeName: "System.String"));
                return table;
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Inicializace objektu</para>
            /// <para lang="en">Object inicialization</para>
            /// </summary>
            /// <param name="domain">
            /// <para lang="cs">Doména</para>
            /// <para lang="en">Domain</para>
            /// </param>
            /// <param name="categories">
            /// <para lang="cs">Seznam kategorií v doméně</para>
            /// <para lang="en">List of categories in domain</para>
            /// </param>
            /// <param name="ldsityObjectsForADSP">
            /// <para lang="cs">Seznam objektů lokálních hustot pro třídění</para>
            /// <para lang="en">List of local density objects for classification</para>
            /// </param>
            /// <param name="dtClassificationRules">
            /// <para lang="cs">Tabulka klasifikačních pravidel</para>
            /// <para lang="en">Classification rules data table</para>
            /// </param>
            private void Initialize(
                TDomain domain,
                List<TCategory> categories,
                List<TDLDsityObject> ldsityObjectsForADSP,
                DataTable dtClassificationRules)
            {
                DomainId = domain.Id;
                DomainLabelCs = domain.LabelCs;
                DomainDescriptionCs = domain.DescriptionCs;
                DomainLabelEn = domain.LabelEn;
                DomainDescriptionEn = domain.DescriptionEn;

                Categories = categories
                    .Select(a =>
                        new CategoryJSON(
                            category: a,
                            ldsityObjectsForADSP: ldsityObjectsForADSP,
                            dtClassificationRules: dtClassificationRules))
                    .ToList<CategoryJSON>();
            }

            /// <summary>
            /// <para lang="cs">Uloží objekt do souboru json</para>
            /// <para lang="en">Save object into json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            public void SaveToFile(string path)
            {
                try
                {
                    System.IO.File.WriteAllText(
                        path: path,
                        contents: JSON,
                        encoding: Encoding.UTF8);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            /// <summary>
            /// <para lang="cs">Určí, zda se zadaný objekt rovná aktuálnímu objektu</para>
            /// <para lang="en">Determines whether specified object is equal to the current object</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Zadaný objekt</para>
            /// <para lang="en">Speciefied object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not ClassificationRuleCollection<TDomain, TCategory> Type, return False
                if (obj is not ClassificationRuleCollection<TDomain, TCategory>)
                {
                    return false;
                }

                return
                     DomainId == ((ClassificationRuleCollection<TDomain, TCategory>)obj).DomainId;
            }

            /// <summary>
            /// <para lang="cs">Vrací hash kód</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Hash kód</para>
            /// <para lang="en">Hash code</para>
            /// </returns>
            public override int GetHashCode()
            {
                return
                    DomainId.GetHashCode();
            }

            /// <summary>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </returns>
            public override string ToString()
            {
                return DomainExtendedLabelEn;
            }

            #endregion Methods


            #region Internal Classes

            /// <summary>
            /// <para lang="cs">Skupina klasifikačních pravidel pro jednu kategorii</para>
            /// <para lang="en">Collection of classification rules for one category</para>
            /// </summary>
            internal class CategoryJSON
            {

                #region Private Fields

                /// <summary>
                /// <para lang="cs">Identifikační číslo kategorie</para>
                /// <para lang="en">Category identifier</para>
                /// </summary>
                private int categoryId;

                /// <summary>
                /// <para lang="cs">Popisek kategorie v národním jazyce</para>
                /// <para lang="en">Category label in national language</para>
                /// </summary>
                private string categoryLabelCs;

                /// <summary>
                /// <para lang="cs">Popis kategorie v národním jazyce</para>
                /// <para lang="en">Category description in national language</para>
                /// </summary>
                private string categoryDescriptionCs;

                /// <summary>
                /// <para lang="cs">Popisek kategorie anglický</para>
                /// <para lang="en">Category label in English</para>
                /// </summary>
                private string categoryLabelEn;

                /// <summary>
                /// <para lang="cs">Popis kategorie anglický</para>
                /// <para lang="en">Category description in English</para>
                /// </summary>
                private string categoryDescriptionEn;

                /// <summary>
                /// <para lang="cs">Seznam objektů lokálních hustot pro třídění</para>
                /// <para lang="en">List of local density objects for classification</para>
                /// </summary>
                private List<LocalDensityObjectJSON> ldsityObjectsForADSP;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// <para lang="cs">
                /// Prázdný konstruktor
                /// (je potřeba pro deserializaci objektu z JSON souboru)</para>
                /// <para lang="en">
                /// Empty constructor
                /// (needed to deserialize an object from a JSON file)</para>
                /// </summary>
                public CategoryJSON()
                {
                    CategoryId = 0;
                    CategoryLabelCs = String.Empty;
                    CategoryDescriptionCs = String.Empty;
                    CategoryLabelEn = String.Empty;
                    CategoryDescriptionEn = String.Empty;
                    LDsityObjectsForADSP = [];
                }

                /// <summary>
                /// <para lang="cs">Konstruktor</para>
                /// <para lang="en">Constructor</para>
                /// </summary>
                /// <param name="category">
                /// <para lang="cs">Kategorie</para>
                /// <para lang="en">Category</para>
                /// </param>
                /// <param name="ldsityObjectsForADSP">
                /// <para lang="cs">Seznam objektů lokálních hustot pro třídění</para>
                /// <para lang="en">List of local density objects for classification</para>
                /// </param>
                /// <param name="dtClassificationRules">
                /// <para lang="cs">Tabulka klasifikačních pravidel</para>
                /// <para lang="en">Classification rules data table</para>
                /// </param>
                public CategoryJSON(
                    TCategory category,
                    List<TDLDsityObject> ldsityObjectsForADSP,
                    DataTable dtClassificationRules)
                {
                    Initialize(
                        category: category,
                        ldsityObjectsForADSP: ldsityObjectsForADSP,
                        dtClassificationRules: dtClassificationRules);
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// <para lang="cs">Identifikační číslo kategorie</para>
                /// <para lang="en">Category identifier</para>
                /// </summary>
                public int CategoryId
                {
                    get
                    {
                        return categoryId;
                    }
                    set
                    {
                        categoryId = value;
                    }
                }


                /// <summary>
                /// <para lang="cs">Popisek kategorie v národním jazyce</para>
                /// <para lang="en">Category label in national language</para>
                /// </summary>
                public string CategoryLabelCs
                {
                    get
                    {
                        return categoryLabelCs;
                    }
                    set
                    {
                        categoryLabelCs = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Popis kategorie v národním jazyce</para>
                /// <para lang="en">Category description in national language</para>
                /// </summary>
                public string CategoryDescriptionCs
                {
                    get
                    {
                        return categoryDescriptionCs;
                    }
                    set
                    {
                        categoryDescriptionCs = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Rozšířený popisek kategorie v národním jazyce</para>
                /// <para lang="en">Category extended label in national language</para>
                /// </summary>
                [JsonIgnore]
                public string CategoryExtendedLabelCs
                {
                    get
                    {
                        return $"{CategoryId} - {CategoryLabelCs}";
                    }
                }


                /// <summary>
                /// <para lang="cs">Popisek kategorie anglický</para>
                /// <para lang="en">Category label in English</para>
                /// </summary>
                public string CategoryLabelEn
                {
                    get
                    {
                        return categoryLabelEn;
                    }
                    set
                    {
                        categoryLabelEn = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Popis kategorie anglický</para>
                /// <para lang="en">Category description in English</para>
                /// </summary>
                public string CategoryDescriptionEn
                {
                    get
                    {
                        return categoryDescriptionEn;
                    }
                    set
                    {
                        categoryDescriptionEn = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Rozšířený popisek kategorie v angličtině</para>
                /// <para lang="en">Category extended label in English</para>
                /// </summary>
                [JsonIgnore]
                public string CategoryExtendedLabelEn
                {
                    get
                    {
                        return $"{CategoryId} - {CategoryLabelEn}";
                    }
                }


                /// <summary>
                /// <para lang="cs">Seznam objektů lokálních hustot pro třídění</para>
                /// <para lang="en">List of local density objects for classification</para>
                /// </summary>
                public List<LocalDensityObjectJSON> LDsityObjectsForADSP
                {
                    get
                    {
                        return ldsityObjectsForADSP;
                    }
                    set
                    {
                        ldsityObjectsForADSP = value;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// <para lang="cs">Inicializace objektu</para>
                /// <para lang="en">Object inicialization</para>
                /// </summary>
                /// <param name="category">
                /// <para lang="cs">Kategorie</para>
                /// <para lang="en">Category</para>
                /// </param>
                /// <param name="ldsityObjectsForADSP">
                /// <para lang="cs">Seznam objektů lokálních hustot pro třídění</para>
                /// <para lang="en">List of local density objects for classification</para>
                /// </param>
                /// <param name="dtClassificationRules">
                /// <para lang="cs">Tabulka klasifikačních pravidel</para>
                /// <para lang="en">Classification rules data table</para>
                /// </param>
                private void Initialize(
                    TCategory category,
                    List<TDLDsityObject> ldsityObjectsForADSP,
                    DataTable dtClassificationRules)
                {
                    CategoryId = category.Id;
                    CategoryLabelCs = category.LabelCs;
                    CategoryDescriptionCs = category.DescriptionCs;
                    CategoryLabelEn = category.LabelEn;
                    CategoryDescriptionEn = category.DescriptionEn;

                    LDsityObjectsForADSP = ldsityObjectsForADSP
                        .Select(a =>
                            new LocalDensityObjectJSON(
                                category: category,
                                ldsityObjectForADSP: a,
                                dtClassificationRules: dtClassificationRules))
                        .ToList<LocalDensityObjectJSON>();
                }

                /// <summary>
                /// <para lang="cs">Určí, zda se zadaný objekt rovná aktuálnímu objektu</para>
                /// <para lang="en">Determines whether specified object is equal to the current object</para>
                /// </summary>
                /// <param name="obj">
                /// <para lang="cs">Zadaný objekt</para>
                /// <para lang="en">Speciefied object</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">ano/ne</para>
                /// <para lang="en">true/false</para>
                /// </returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not CategoryJSON Type, return False
                    if (obj is not CategoryJSON)
                    {
                        return false;
                    }

                    return
                        CategoryId == ((CategoryJSON)obj).CategoryId;
                }

                /// <summary>
                /// <para lang="cs">Vrací hash kód</para>
                /// <para lang="en">Returns the hash code</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Hash kód</para>
                /// <para lang="en">Hash code</para>
                /// </returns>
                public override int GetHashCode()
                {
                    return
                        CategoryId.GetHashCode();
                }

                /// <summary>
                /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
                /// <para lang="en">Returns the string that represents current object</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
                /// <para lang="en">Returns the string that represents current object</para>
                /// </returns>
                public override string ToString()
                {
                    return CategoryExtendedLabelEn;
                }

                #endregion Methods

            }

            /// <summary>
            /// <para lang="cs">Skupina klasifikačních pravidel pro jednu kategorii a jeden objekt lokální hustoty</para>
            /// <para lang="en">Collection of classification rules for one category and one local density object</para>
            /// </summary>
            internal class LocalDensityObjectJSON
            {

                #region Private Fields

                /// <summary>
                /// <para lang="cs">Identifikační číslo objektu lokální hustoty</para>
                /// <para lang="en">Local density object identifier</para>
                /// </summary>
                private int localDensityObjectId;

                /// <summary>
                /// <para lang="cs">Popisek objektu lokální hustoty v národním jazyce</para>
                /// <para lang="en">Local density object label in national language</para>
                /// </summary>
                private string localDensityObjectLabelCs;

                /// <summary>
                /// <para lang="cs">Popis objektu lokální hustoty v národním jazyce</para>
                /// <para lang="en">Local density object description in national language</para>
                /// </summary>
                private string localDensityObjectDescriptionCs;

                /// <summary>
                /// <para lang="cs">Popisek objektu lokální hustoty anglický</para>
                /// <para lang="en">Local density object label in English</para>
                /// </summary>
                private string localDensityObjectLabelEn;

                /// <summary>
                /// <para lang="cs">Popis objektu lokální hustoty anglický</para>
                /// <para lang="en">Local density object description in English</para>
                /// </summary>
                private string localDensityObjectDescriptionEn;

                /// <summary>
                /// <para lang="cs">Kladné klasifikační pravidlo</para>
                /// <para lang="en">Positive classification rule</para>
                /// </summary>
                private string positiveClassificationRule;

                /// <summary>
                /// <para lang="cs">Záporné klasifikační pravidlo</para>
                /// <para lang="en">Negative classification rule</para>
                /// </summary>
                private string negativeClassificationRule;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// <para lang="cs">
                /// Prázdný konstruktor
                /// (je potřeba pro deserializaci objektu z JSON souboru)</para>
                /// <para lang="en">
                /// Empty constructor
                /// (needed to deserialize an object from a JSON file)</para>
                /// </summary>
                public LocalDensityObjectJSON()
                {
                    LocalDensityObjectId = 0;
                    LocalDensityObjectLabelCs = String.Empty;
                    LocalDensityObjectDescriptionCs = String.Empty;
                    LocalDensityObjectLabelEn = String.Empty;
                    LocalDensityObjectDescriptionEn = String.Empty;
                    PositiveClassificationRule = String.Empty;
                    NegativeClassificationRule = String.Empty;
                }

                /// <summary>
                /// <para lang="cs">Konstruktor</para>
                /// <para lang="en">Constructor</para>
                /// </summary>
                /// <param name="category">
                /// <para lang="cs">Kategorie</para>
                /// <para lang="en">Category</para>
                /// </param>
                /// <param name="ldsityObjectForADSP">
                /// <para lang="cs">Objekt lokálních hustot pro třídění</para>
                /// <para lang="en">Local density object for classification</para>
                /// </param>
                /// <param name="dtClassificationRules">
                /// <para lang="cs">Tabulka klasifikačních pravidel</para>
                /// <para lang="en">Classification rules data table</para>
                /// </param>
                public LocalDensityObjectJSON(
                    TCategory category,
                    TDLDsityObject ldsityObjectForADSP,
                    DataTable dtClassificationRules)
                {
                    Initialize(
                        category: category,
                        ldsityObjectForADSP: ldsityObjectForADSP,
                        dtClassificationRules: dtClassificationRules);
                }

                /// <summary>
                /// <para lang="cs">Konstruktor</para>
                /// <para lang="en">Constructor</para>
                /// </summary>
                /// <param name="id">
                /// <para lang="cs">Identifikační číslo objektu lokální hustoty</para>
                /// <para lang="en">Local density object identifier</para>
                /// </param>
                /// <param name="labelCs">
                /// <para lang="cs">Popisek objektu lokální hustoty v národním jazyce</para>
                /// <para lang="en">Local density object label in national language</para>
                /// </param>
                /// <param name="descriptionCs">
                /// <para lang="cs">Popis objektu lokální hustoty v národním jazyce</para>
                /// <para lang="en">Local density object description in national language</para>
                /// </param>
                /// <param name="labelEn">
                /// <para lang="cs">Popisek objektu lokální hustoty anglický</para>
                /// <para lang="en">Local density object label in English</para>
                /// </param>
                /// <param name="descriptionEn">
                /// <para lang="cs">Popis objektu lokální hustoty anglický</para>
                /// <para lang="en">Local density object description in English</para>
                /// </param>
                /// <param name="positiveClassificationRule">
                /// <para lang="cs">Kladné klasifikační pravidlo</para>
                /// <para lang="en">Positive classification rule</para>
                /// </param>
                /// <param name="negativeClassificationRule">
                /// <para lang="cs">Záporné klasifikační pravidlo</para>
                /// <para lang="en">Negative classification rule</para>
                /// </param>
                public LocalDensityObjectJSON(
                    int id,
                    string labelCs = "",
                    string descriptionCs = "",
                    string labelEn = "",
                    string descriptionEn = "",
                    string positiveClassificationRule = "",
                    string negativeClassificationRule = "")
                {
                    LocalDensityObjectId = id;
                    LocalDensityObjectLabelCs = labelCs;
                    LocalDensityObjectDescriptionCs = descriptionCs;
                    LocalDensityObjectLabelEn = labelEn;
                    LocalDensityObjectDescriptionEn = descriptionEn;
                    PositiveClassificationRule = positiveClassificationRule;
                    NegativeClassificationRule = negativeClassificationRule;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// <para lang="cs">Identifikační číslo objektu lokální hustoty</para>
                /// <para lang="en">Local density object identifier</para>
                /// </summary>
                public int LocalDensityObjectId
                {
                    get
                    {
                        return localDensityObjectId;
                    }
                    set
                    {
                        localDensityObjectId = value;
                    }
                }


                /// <summary>
                /// <para lang="cs">Popisek objektu lokální hustoty v národním jazyce</para>
                /// <para lang="en">Local density object label in national language</para>
                /// </summary>
                public string LocalDensityObjectLabelCs
                {
                    get
                    {
                        return localDensityObjectLabelCs;
                    }
                    set
                    {
                        localDensityObjectLabelCs = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Popis objektu lokální hustoty v národním jazyce</para>
                /// <para lang="en">Local density object description in national language</para>
                /// </summary>
                public string LocalDensityObjectDescriptionCs
                {
                    get
                    {
                        return localDensityObjectDescriptionCs;
                    }
                    set
                    {
                        localDensityObjectDescriptionCs = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Rozšířený popisek objektu lokální hustoty v národním jazyce</para>
                /// <para lang="en">Local density object extended label in national language</para>
                /// </summary>
                [JsonIgnore]
                public string LocalDensityObjectExtendedLabelCs
                {
                    get
                    {
                        return $"{LocalDensityObjectId} - {LocalDensityObjectLabelCs}";
                    }
                }


                /// <summary>
                /// <para lang="cs">Popisek objektu lokální hustoty anglický</para>
                /// <para lang="en">Local density object label in English</para>
                /// </summary>
                public string LocalDensityObjectLabelEn
                {
                    get
                    {
                        return localDensityObjectLabelEn;
                    }
                    set
                    {
                        localDensityObjectLabelEn = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Popis objektu lokální hustoty anglický</para>
                /// <para lang="en">Local density object description in English</para>
                /// </summary>
                public string LocalDensityObjectDescriptionEn
                {
                    get
                    {
                        return localDensityObjectDescriptionEn;
                    }
                    set
                    {
                        localDensityObjectDescriptionEn = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Rozšířený popisek objektu lokální hustoty v angličtině</para>
                /// <para lang="en">Local density object extended label in English</para>
                /// </summary>
                [JsonIgnore]
                public string LocalDensityObjectExtendedLabelEn
                {
                    get
                    {
                        return $"{LocalDensityObjectId} - {LocalDensityObjectLabelEn}";
                    }
                }


                /// <summary>
                /// <para lang="cs">Kladné klasifikační pravidlo</para>
                /// <para lang="en">Positive classification rule</para>
                /// </summary>
                public string PositiveClassificationRule
                {
                    get
                    {
                        return positiveClassificationRule;
                    }
                    set
                    {
                        positiveClassificationRule = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Záporné klasifikační pravidlo</para>
                /// <para lang="en">Negative classification rule</para>
                /// </summary>
                public string NegativeClassificationRule
                {
                    get
                    {
                        return negativeClassificationRule;
                    }
                    set
                    {
                        negativeClassificationRule = value;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// <para lang="cs">Inicializace objektu</para>
                /// <para lang="en">Object inicialization</para>
                /// </summary>
                /// <param name="category">
                /// <para lang="cs">Kategorie</para>
                /// <para lang="en">Category</para>
                /// </param>
                /// <param name="ldsityObjectForADSP">
                /// <para lang="cs">Objekt lokálních hustot pro třídění</para>
                /// <para lang="en">Local density object for classification</para>
                /// </param>
                /// <param name="dtClassificationRules">
                /// <para lang="cs">Tabulka klasifikačních pravidel</para>
                /// <para lang="en">Classification rules data table</para>
                /// </param>
                private void Initialize(
                    TCategory category,
                    TDLDsityObject ldsityObjectForADSP,
                    DataTable dtClassificationRules)
                {
                    LocalDensityObjectId = ldsityObjectForADSP.Id;
                    LocalDensityObjectLabelCs = ldsityObjectForADSP.LabelCs;
                    LocalDensityObjectDescriptionCs = ldsityObjectForADSP.DescriptionCs;
                    LocalDensityObjectLabelEn = ldsityObjectForADSP.LabelEn;
                    LocalDensityObjectDescriptionEn = ldsityObjectForADSP.DescriptionEn;

                    DataRow row =
                        dtClassificationRules.AsEnumerable()
                                .Where(a => a.Field<int>(columnName: "Category") == category.Id)
                                .Where(a => a.Field<int>(columnName: "LDsityObjectADSP") == ldsityObjectForADSP.Id)
                                .FirstOrDefault<DataRow>();

                    PositiveClassificationRule = (row == null)
                        ? null
                        : Functions.GetStringArg(row: row, name: "PositiveRule", defaultValue: null);

                    NegativeClassificationRule = (row == null)
                        ? null
                        : Functions.GetStringArg(row: row, name: "NegativeRule", defaultValue: null);
                }


                /// <summary>
                /// <para lang="cs">Určí, zda se zadaný objekt rovná aktuálnímu objektu</para>
                /// <para lang="en">Determines whether specified object is equal to the current object</para>
                /// </summary>
                /// <param name="obj">
                /// <para lang="cs">Zadaný objekt</para>
                /// <para lang="en">Speciefied object</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">ano/ne</para>
                /// <para lang="en">true/false</para>
                /// </returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not LocalDensityObjectJSON Type, return False
                    if (obj is not LocalDensityObjectJSON)
                    {
                        return false;
                    }

                    return
                        LocalDensityObjectId == ((LocalDensityObjectJSON)obj).LocalDensityObjectId;
                }

                /// <summary>
                /// <para lang="cs">Vrací hash kód</para>
                /// <para lang="en">Returns the hash code</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Hash kód</para>
                /// <para lang="en">Hash code</para>
                /// </returns>
                public override int GetHashCode()
                {
                    return
                        LocalDensityObjectId.GetHashCode();
                }

                /// <summary>
                /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
                /// <para lang="en">Returns the string that represents current object</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
                /// <para lang="en">Returns the string that represents current object</para>
                /// </returns>
                public override string ToString()
                {
                    return $"{LocalDensityObjectId}: {LocalDensityObjectLabelCs}";
                }

                #endregion Methods

            }

            #endregion Internal Classes

        }

    }
}