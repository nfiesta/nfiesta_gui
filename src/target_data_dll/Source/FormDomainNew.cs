﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nové plošné domény nebo subpopulace - třída pro Designer</para>
    /// <para lang="en">Form to create a new area domain or subpopulation - class for Designer</para>
    /// </summary>
    internal partial class FormDomainNewDesign
        : Form
    {
        protected FormDomainNewDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Button BtnCancel => btnCancel;
        protected System.Windows.Forms.Button BtnOK => btnOK;
        protected System.Windows.Forms.ComboBox CboClassificationType => cboClassificationType;
        protected System.Windows.Forms.GroupBox GrpDomain => grpDomain;
        protected System.Windows.Forms.Label LblClassificationTypeCaption => lblClassificationTypeCaption;
        protected System.Windows.Forms.Label LblDescriptionCsCaption => lblDescriptionCsCaption;
        protected System.Windows.Forms.Label LblDescriptionEnCaption => lblDescriptionEnCaption;
        protected System.Windows.Forms.Label LblLabelCsCaption => lblLabelCsCaption;
        protected System.Windows.Forms.Label LblLabelEnCaption => lblLabelEnCaption;
        protected System.Windows.Forms.TableLayoutPanel TlpDomain => tlpDomain;
        protected System.Windows.Forms.TextBox TxtDescriptionCsValue => txtDescriptionCsValue;
        protected System.Windows.Forms.TextBox TxtDescriptionEnValue => txtDescriptionEnValue;
        protected System.Windows.Forms.TextBox TxtLabelCsValue => txtLabelCsValue;
        protected System.Windows.Forms.TextBox TxtLabelEnValue => txtLabelEnValue;
    }

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nové plošné domény nebo subpopulace</para>
    /// <para lang="en">Form to create a new area domain or subpopulation</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class FormDomainNew<TDomain, TCategory>
            : FormDomainNewDesign, INfiEstaControl, ITargetDataControl
             where TDomain : IArealOrPopulationDomain
             where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Skupina klasifikačních pravidel pro jednu doménu, načtená z json souboru</para>
        /// <para lang="en">Collection of classification rules for one domain, loaded from json file</para>
        /// </summary>
        private ClassificationRuleCollection<TDomain, TCategory> collection;

        /// <summary>
        /// <para lang="cs">
        /// Párování objektů lokálních hustot pro třídění,
        /// klíč: objekt lokální hustoty pro třídění z databáze,
        /// hodnota: objekt lokální hustoty pro třídění z json souboru</para>
        /// <para lang="en">
        /// Pairing local density objects for classification,
        /// key: local density object for classification from database,
        /// value: local density object for classification from json file</para>
        /// </summary>
        public ReplacingLocalDensityObject<TDomain, TCategory> pairing;

        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgClassificationTypeIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormDomainNew(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná (vybraná na 2. nebo 3. formuláři) (read-only)</para>
        /// <para lang="en">Selected target variable (selected on the 2nd or 3rd form) (read-only)</para>
        /// </summary>
        public TDTargetVariable SelectedTargetVariable
        {
            get
            {
                return
                    ((ControlTDSelectionAttributeCategory)ControlOwner)
                        .SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Skupina klasifikačních pravidel pro jednu doménu, načtená z json souboru</para>
        /// <para lang="en">Collection of classification rules for one domain, loaded from json file</para>
        /// </summary>
        public ClassificationRuleCollection<TDomain, TCategory> Collection
        {
            get
            {
                return collection;
            }
            set
            {
                collection = value;

                if (collection == null)
                {
                    TxtLabelCsValue.Text = String.Empty;
                    TxtDescriptionCsValue.Text = String.Empty;
                    TxtLabelEnValue.Text = String.Empty;
                    TxtLabelEnValue.Text = String.Empty;
                    return;
                }

                if (collection.IsEmpty)
                {
                    TxtLabelCsValue.Text = String.Empty;
                    TxtDescriptionCsValue.Text = String.Empty;
                    TxtLabelEnValue.Text = String.Empty;
                    TxtLabelEnValue.Text = String.Empty;
                    return;
                }

                TxtLabelCsValue.Text = collection.DomainLabelCs ?? String.Empty;
                TxtLabelEnValue.Text = collection.DomainLabelEn ?? String.Empty;
                TxtDescriptionCsValue.Text = collection.DomainDescriptionCs ?? String.Empty;
                TxtDescriptionEnValue.Text = collection.DomainDescriptionEn ?? String.Empty;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Párování objektů lokálních hustot pro třídění,
        /// klíč: objekt lokální hustoty pro třídění z databáze,
        /// hodnota: objekt lokální hustoty pro třídění z json souboru</para>
        /// <para lang="en">
        /// Pairing local density objects for classification,
        /// key: local density object for classification from database,
        /// value: local density object for classification from json file</para>
        /// </summary>
        public ReplacingLocalDensityObject<TDomain, TCategory> Pairing
        {
            get
            {
                return pairing;
            }
            set
            {
                pairing = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormDomainNew<TDomain, TCategory>)}",   "Nová plošná doména" },
                        { nameof(BtnOK),                                    "Další" },
                        { nameof(BtnCancel),                                "Zrušit" },
                        { nameof(CboClassificationType),                    "ExtendedLabelCs" },
                        { nameof(GrpDomain),                                "Plošná doména:" },
                        { nameof(LblLabelCsCaption),                        "Zkratka (národní):" },
                        { nameof(LblDescriptionCsCaption),                  "Popis (národní):" },
                        { nameof(LblLabelEnCaption),                        "Zkratka (anglicky):" },
                        { nameof(LblDescriptionEnCaption),                  "Popis (anglicky):" },
                        { nameof(LblClassificationTypeCaption),             "Klasifikační typ:" },
                        { nameof(msgLabelCsIsEmpty),                        "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),                  "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),                        "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),                  "Popis (anglický) musí být vyplněn." },
                        { nameof(msgClassificationTypeIsEmpty),             "Není vybraný žádný klasifikační typ." },
                        { nameof(msgLabelCsExists),                         "Použitá zkratka (národní) již existuje pro jinou plošnou doménu." },
                        { nameof(msgDescriptionCsExists),                   "Použitý popis (národní) již existuje pro jinou plošnou doménu." },
                        { nameof(msgLabelEnExists),                         "Použitá zkratka (anglická) již existuje pro jinou plošnou doménu." },
                        { nameof(msgDescriptionEnExists),                   "Použitý popis (anglický) již existuje pro jinou plošnou doménu." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: $"{nameof(FormDomainNew<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                        out Dictionary<string, string> dictNationalAreaDomain)
                            ? dictNationalAreaDomain
                            : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormDomainNew<TDomain, TCategory>)}",   "Nová subpopulace" },
                        { nameof(BtnOK),                                    "Další" },
                        { nameof(BtnCancel),                                "Zrušit" },
                        { nameof(CboClassificationType),                    "ExtendedLabelCs" },
                        { nameof(GrpDomain),                                "Subpopulace:"  },
                        { nameof(LblLabelCsCaption),                        "Zkratka (národní):" },
                        { nameof(LblDescriptionCsCaption),                  "Popis (národní):" },
                        { nameof(LblLabelEnCaption),                        "Zkratka (anglicky):" },
                        { nameof(LblDescriptionEnCaption),                  "Popis (anglicky):" },
                        { nameof(LblClassificationTypeCaption),             "Klasifikační typ:" },
                        { nameof(msgLabelCsIsEmpty),                        "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),                  "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),                        "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),                  "Popis (anglický) musí být vyplněn." },
                        { nameof(msgClassificationTypeIsEmpty),             "Není vybraný žádný klasifikační typ." },
                        { nameof(msgLabelCsExists),                         "Použitá zkratka (národní) již existuje pro jinou subpopulaci." },
                        { nameof(msgDescriptionCsExists),                   "Použitý popis (národní) již existuje pro jinou subpopulaci." },
                        { nameof(msgLabelEnExists),                         "Použitá zkratka (anglická) již existuje pro jinou subpopulaci." },
                        { nameof(msgDescriptionEnExists),                   "Použitý popis (anglický) již existuje pro jinou subpopulaci." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: $"{nameof(FormDomainNew<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                        out Dictionary<string, string> dictNationalSubPopulation)
                            ? dictNationalSubPopulation
                            : [],

                    _ => [],
                },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormDomainNew<TDomain, TCategory>)}",   "New area domain"},
                        { nameof(BtnOK),                                    "Next" },
                        { nameof(BtnCancel),                                "Cancel" },
                        { nameof(CboClassificationType),                    "ExtendedLabelEn"},
                        { nameof(GrpDomain),                                "Area domain:"  },
                        { nameof(LblLabelCsCaption),                        "Label (national):"},
                        { nameof(LblDescriptionCsCaption),                  "Description (national):"},
                        { nameof(LblLabelEnCaption),                        "Label (English):"},
                        { nameof(LblDescriptionEnCaption),                  "Description (English):"},
                        { nameof(LblClassificationTypeCaption),             "Classification type:"},
                        { nameof(msgLabelCsIsEmpty),                        "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),                  "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),                        "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),                  "Description (English) cannot be empty." },
                        { nameof(msgClassificationTypeIsEmpty),             "Classification type is not selected." },
                        { nameof(msgLabelCsExists),                         "This label (national) already exists for another area domain." },
                        { nameof(msgDescriptionCsExists),                   "This description (national) already exists for another area domain." },
                        { nameof(msgLabelEnExists),                         "This label (English) already exists for another area domain." },
                        { nameof(msgDescriptionEnExists),                   "This description (English) already exists for another area domain." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: $"{nameof(FormDomainNew<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                        out Dictionary<string, string> dictInternationalAreaDomain)
                            ? dictInternationalAreaDomain
                            : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormDomainNew<TDomain, TCategory>)}",   "New subpopulation"},
                        { nameof(BtnOK),                                    "Next" },
                        { nameof(BtnCancel),                                "Cancel" },
                        { nameof(CboClassificationType),                    "ExtendedLabelEn"},
                        { nameof(GrpDomain),                                "Subpopulation:"  },
                        { nameof(LblLabelCsCaption),                        "Label (national):"},
                        { nameof(LblDescriptionCsCaption),                  "Description (national):"},
                        { nameof(LblLabelEnCaption),                        "Label (English):"},
                        { nameof(LblDescriptionEnCaption),                  "Description (English):"},
                        { nameof(LblClassificationTypeCaption),             "Classification type:"},
                        { nameof(msgLabelCsIsEmpty),                        "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),                  "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),                        "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),                  "Description (English) cannot be empty." },
                        { nameof(msgClassificationTypeIsEmpty),             "Classification type is not selected." },
                        { nameof(msgLabelCsExists),                         "This label (national) already exists for another subpopulation." },
                        { nameof(msgDescriptionCsExists),                   "This description (national) already exists for another subpopulation." },
                        { nameof(msgLabelEnExists),                         "This label (English) already exists for another subpopulation." },
                        { nameof(msgDescriptionEnExists),                   "This description (English) already exists for another subpopulation." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                       key: $"{nameof(FormDomainNew<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                        out Dictionary<string, string> dictInternationalSubPopulation)
                            ? dictInternationalSubPopulation
                            : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Collection = null;
            Pairing = null;

            CboClassificationType.DataSource = null;
            CboClassificationType.DataSource =
                TDFunctions.FnGetClassificationType.Execute(
                    database: Database,
                    targetVariableId: SelectedTargetVariable.Id).Items;

            InitializeLabels();
            SetStyle();

            BtnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            BtnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !DisplayFormCategoryNew();
                    }
                    else
                    {
                        // Formulář ukončen jiným tlačítkem než OK.
                        // Form terminates by a button other than OK.
                        e.Cancel = false;
                    }
                });

            BtnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            TableLayoutPanelCellPosition pos;

            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            BtnOK.Font = Setting.ButtonFont;
            BtnOK.ForeColor = Setting.ButtonForeColor;

            BtnCancel.Font = Setting.ButtonFont;
            BtnCancel.ForeColor = Setting.ButtonForeColor;

            CboClassificationType.Font = Setting.ComboBoxFont;
            CboClassificationType.ForeColor = Setting.ComboBoxForeColor;
            pos = TlpDomain.GetCellPosition(control: CboClassificationType);
            CboClassificationType.Margin = new Padding(
                left: 0, top: (int)((TlpDomain.GetRowHeights()[pos.Row] - CboClassificationType.Height) / 2), right: 0, bottom: 0);

            GrpDomain.Font = Setting.GroupBoxFont;
            GrpDomain.ForeColor = Setting.GroupBoxForeColor;

            LblLabelCsCaption.Font = Setting.LabelFont;
            LblLabelCsCaption.ForeColor = Setting.LabelForeColor;

            LblDescriptionCsCaption.Font = Setting.LabelFont;
            LblDescriptionCsCaption.ForeColor = Setting.LabelForeColor;

            LblLabelEnCaption.Font = Setting.LabelFont;
            LblLabelEnCaption.ForeColor = Setting.LabelForeColor;

            LblDescriptionEnCaption.Font = Setting.LabelFont;
            LblDescriptionEnCaption.ForeColor = Setting.LabelForeColor;

            LblClassificationTypeCaption.Font = Setting.LabelFont;
            LblClassificationTypeCaption.ForeColor = Setting.LabelForeColor;

            TxtLabelCsValue.Font = Setting.TextBoxFont;
            TxtLabelCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpDomain.GetCellPosition(control: TxtLabelCsValue);
            TxtLabelCsValue.Margin = new Padding(
                left: 0, top: (int)((TlpDomain.GetRowHeights()[pos.Row] - TxtLabelCsValue.Height) / 2), right: 0, bottom: 0);

            TxtDescriptionCsValue.Font = Setting.TextBoxFont;
            TxtDescriptionCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpDomain.GetCellPosition(control: TxtDescriptionCsValue);
            TxtDescriptionCsValue.Margin = new Padding(
                left: 0, top: (int)((TlpDomain.GetRowHeights()[pos.Row] - TxtDescriptionCsValue.Height) / 2), right: 0, bottom: 0);

            TxtLabelEnValue.Font = Setting.TextBoxFont;
            TxtLabelEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpDomain.GetCellPosition(control: TxtLabelEnValue);
            TxtLabelEnValue.Margin = new Padding(
                left: 0, top: (int)((TlpDomain.GetRowHeights()[pos.Row] - TxtLabelEnValue.Height) / 2), right: 0, bottom: 0);

            TxtDescriptionEnValue.Font = Setting.TextBoxFont;
            TxtDescriptionEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpDomain.GetCellPosition(control: TxtDescriptionEnValue);
            TxtDescriptionEnValue.Margin = new Padding(
                left: 0, top: (int)((TlpDomain.GetRowHeights()[pos.Row] - TxtDescriptionEnValue.Height) / 2), right: 0, bottom: 0);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: $"{nameof(FormDomainNew<TDomain, TCategory>)}",
                    out string frmDomainNew)
                        ? frmDomainNew
                        : String.Empty;

            BtnOK.Text =
                labels.TryGetValue(
                    key: nameof(BtnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            BtnCancel.Text =
                labels.TryGetValue(
                    key: nameof(BtnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            CboClassificationType.DisplayMember =
                labels.TryGetValue(
                    key: nameof(CboClassificationType),
                    out string cboClassificationTypeDisplayMember)
                        ? cboClassificationTypeDisplayMember
                        : String.Empty;

            GrpDomain.Text =
                labels.TryGetValue(
                    key: nameof(GrpDomain),
                    out string grpDomainText)
                        ? grpDomainText
                        : String.Empty;

            LblLabelCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(LblLabelCsCaption),
                    out string lblLabelCsCaptionText)
                        ? lblLabelCsCaptionText
                        : String.Empty;

            LblDescriptionCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(LblDescriptionCsCaption),
                    out string lblDescriptionCsCaptionText)
                        ? lblDescriptionCsCaptionText
                        : String.Empty;

            LblLabelEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(LblLabelEnCaption),
                    out string lblLabelEnCaptionText)
                        ? lblLabelEnCaptionText
                        : String.Empty;

            LblDescriptionEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(LblDescriptionEnCaption),
                    out string lblDescriptionEnCaptionText)
                        ? lblDescriptionEnCaptionText
                        : String.Empty;

            LblClassificationTypeCaption.Text =
                labels.TryGetValue(
                    key: nameof(LblClassificationTypeCaption),
                    out string lblClassificationTypeCaptionText)
                        ? lblClassificationTypeCaptionText
                        : String.Empty;

            msgLabelCsIsEmpty =
               labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                       out msgLabelCsIsEmpty)
                           ? msgLabelCsIsEmpty
                           : String.Empty;

            msgDescriptionCsIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                      out msgDescriptionCsIsEmpty)
                          ? msgDescriptionCsIsEmpty
                          : String.Empty;

            msgLabelEnIsEmpty =
              labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                      out msgLabelEnIsEmpty)
                          ? msgLabelEnIsEmpty
                          : String.Empty;

            msgDescriptionEnIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                      out msgDescriptionEnIsEmpty)
                          ? msgDescriptionEnIsEmpty
                          : String.Empty;

            msgClassificationTypeIsEmpty =
              labels.TryGetValue(key: nameof(msgClassificationTypeIsEmpty),
                      out msgClassificationTypeIsEmpty)
                          ? msgClassificationTypeIsEmpty
                          : String.Empty;

            msgLabelCsExists =
              labels.TryGetValue(key: nameof(msgLabelCsExists),
                      out msgLabelCsExists)
                          ? msgLabelCsExists
                          : String.Empty;

            msgDescriptionCsExists =
              labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                      out msgDescriptionCsExists)
                          ? msgDescriptionCsExists
                          : String.Empty;

            msgLabelEnExists =
              labels.TryGetValue(key: nameof(msgLabelEnExists),
                      out msgLabelEnExists)
                          ? msgLabelEnExists
                          : String.Empty;

            msgDescriptionEnExists =
              labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                      out msgDescriptionEnExists)
                          ? msgDescriptionEnExists
                          : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazí formulář pro vytvoření nových kategorií plošné domény nebo subpopulace</para>
        /// <para lang="en">Method displays form to create new categories of area domain or subpopulation </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Výsledek kontroly zadaných hodnot určuje, zda je možné zobrazit formulář</para>
        /// <para lang="en">Result of validation given values determines whether is possible to display form</para>
        /// </returns>
        private bool DisplayFormCategoryNew()
        {
            string labelCs = TxtLabelCsValue.Text.Trim();
            string descriptionCs = TxtDescriptionCsValue.Text.Trim();
            string labelEn = TxtLabelEnValue.Text.Trim();
            string descriptionEn = TxtDescriptionEnValue.Text.Trim();

            if (String.IsNullOrEmpty(value: labelCs))
            {
                // LabelCs - prázdné
                // LabelCs - empty
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                // DescriptionCs - prázdné
                // DescriptionCs - empty
                MessageBox.Show(
                    text: msgDescriptionCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                // LabelEn - prázdné
                // LabelEn - empty
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                // DescriptionEn - prázdné
                // DescriptionEn - empty
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (CboClassificationType.SelectedItem == null)
            {
                // Není vybraný klasifikační typ
                // No classification type is selected
                MessageBox.Show(
                    text: msgClassificationTypeIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            switch (ArealOrPopulation)
            {

                case TDArealOrPopulationEnum.AreaDomain:
                    // Formulář pro plošné domény
                    // Form for area domains

                    TDAreaDomainList areaDomains =
                        TDFunctions.FnGetAreaDomain.Execute(
                            database: Database,
                            areaDomainId: null,
                            targetVariableId: null);

                    if (areaDomains.Items.Where(a => a.LabelCs == labelCs).Any())
                    {
                        // LabelCs - duplicitní
                        // LabelCs - duplicated
                        MessageBox.Show(
                            text: msgLabelCsExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if (areaDomains.Items.Where(a => a.DescriptionCs == descriptionCs).Any())
                    {
                        // DescriptionCs - duplicitní
                        // DescriptionCs - duplicated
                        MessageBox.Show(
                            text: msgDescriptionCsExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if (areaDomains.Items.Where(a => a.LabelEn == labelEn).Any())
                    {
                        // LabelEn - duplicitní
                        // LabelEn - duplicated
                        MessageBox.Show(
                            text: msgLabelEnExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if (areaDomains.Items.Where(a => a.DescriptionEn == descriptionEn).Any())
                    {
                        // DescriptionEn - duplicitní
                        // DescriptionEn - duplicated
                        MessageBox.Show(
                            text: msgDescriptionEnExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    TDAreaDomain newAreaDomain = new()
                    {
                        Id = 1,
                        LabelCs = labelCs,
                        DescriptionCs = descriptionCs,
                        LabelEn = labelEn,
                        DescriptionEn = descriptionEn,
                        ClassificationTypeId = ((TDClassificationType)CboClassificationType.SelectedItem).Id
                    };

                    FormCategoryNew<TDAreaDomain, TDAreaDomainCategory> formAreaDomainCategoryNew =
                        new(
                            controlOwner: ControlOwner,
                            domain: newAreaDomain)
                        {
                            Collection =
                                (ClassificationRuleCollection<TDAreaDomain, TDAreaDomainCategory>)
                                Convert.ChangeType(
                                    value: Collection,
                                    conversionType: typeof(ClassificationRuleCollection<TDAreaDomain, TDAreaDomainCategory>)),
                            Pairing =
                                (ReplacingLocalDensityObject<TDAreaDomain, TDAreaDomainCategory>)
                                Convert.ChangeType(
                                    value: Pairing,
                                    conversionType: typeof(ReplacingLocalDensityObject<TDAreaDomain, TDAreaDomainCategory>)),
                        };

                    if (formAreaDomainCategoryNew.ShowDialog() == DialogResult.OK) { }

                    return true;

                case TDArealOrPopulationEnum.Population:
                    // Formulář pro subpopulace
                    // Form for subpopulations

                    TDSubPopulationList subPopulations =
                        TDFunctions.FnGetSubPopulation.Execute(
                            database: Database,
                            subPopulationId: null,
                            targetVariableId: null);

                    if (subPopulations.Items.Where(a => a.LabelCs == labelCs).Any())
                    {
                        // LabelCs - duplicitní
                        // LabelCs - duplicated
                        MessageBox.Show(
                            text: msgLabelCsExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if (subPopulations.Items.Where(a => a.DescriptionCs == descriptionCs).Any())
                    {
                        // DescriptionCs - duplicitní
                        // Description Cs - duplicated
                        MessageBox.Show(
                            text: msgDescriptionCsExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if (subPopulations.Items.Where(a => a.LabelEn == labelEn).Any())
                    {
                        // LabelEn - duplicitní
                        // LabelEn - duplicated
                        MessageBox.Show(
                            text: msgLabelEnExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if (subPopulations.Items.Where(a => a.DescriptionEn == descriptionEn).Any())
                    {
                        // DescriptionEn - duplicitní
                        // DescriptionEn - duplicated
                        MessageBox.Show(
                            text: msgDescriptionEnExists,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    TDSubPopulation newSubPopulation = new()
                    {
                        Id = 1,
                        LabelCs = labelCs,
                        DescriptionCs = descriptionCs,
                        LabelEn = labelEn,
                        DescriptionEn = descriptionEn,
                        ClassificationTypeId = ((TDClassificationType)CboClassificationType.SelectedItem).Id
                    };

                    FormCategoryNew<TDSubPopulation, TDSubPopulationCategory> formSubPopulationCategoryNew =
                        new(
                            controlOwner: ControlOwner,
                            domain: newSubPopulation)
                        {
                            Collection =
                                (ClassificationRuleCollection<TDSubPopulation, TDSubPopulationCategory>)
                                Convert.ChangeType(
                                    value: Collection,
                                    conversionType: typeof(ClassificationRuleCollection<TDSubPopulation, TDSubPopulationCategory>)),
                            Pairing =
                                (ReplacingLocalDensityObject<TDSubPopulation, TDSubPopulationCategory>)
                                Convert.ChangeType(
                                    value: Pairing,
                                    conversionType: typeof(ReplacingLocalDensityObject<TDSubPopulation, TDSubPopulationCategory>)),
                        };

                    if (formSubPopulationCategoryNew.ShowDialog() == DialogResult.OK) { }

                    return true;

                default:
                    return false;
            }
        }

        #endregion Methods

    }

}