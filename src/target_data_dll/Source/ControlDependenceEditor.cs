﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro definici závislostí
    /// mezi kategoriemi plošné domény nebo subpopulace - třída pro Designer</para>
    /// <para lang="en">Control for definition dependencies
    /// among area domain or subpopulation categories - class for Designer</para>
    /// </summary>
    internal partial class ControlDependenceEditorDesign
        : UserControl
    {
        protected ControlDependenceEditorDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.ToolStripButton BtnClose => btnClose;
        protected System.Windows.Forms.ToolStripButton BtnDependenceCancel => btnDependenceCancel;
        protected System.Windows.Forms.ToolStripButton BtnDependenceSelect => btnDependenceSelect;
        protected System.Windows.Forms.ToolStripButton BtnHierarchy => btnHierarchy;
        protected System.Windows.Forms.GroupBox GrpDependence => grpDependence;
        protected System.Windows.Forms.GroupBox GrpDependencePossible => grpDependencePossible;
        protected System.Windows.Forms.GroupBox GrpDependenceSelected => grpDependenceSelected;
        protected System.Windows.Forms.ListBox LstDependencePossible => lstDependencePossible;
        protected System.Windows.Forms.ListBox LstDependenceSelected => lstDependenceSelected;
        protected System.Windows.Forms.ToolStrip TsrDependencePossible => tsrDependencePossible;
        protected System.Windows.Forms.ToolStrip TsrDependenceSelected => tsrDependenceSelected;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro definici závislostí
    /// mezi kategoriemi plošné domény nebo subpopulace</para>
    /// <para lang="en">Control for definition dependencies
    /// among area domain or subpopulation categories</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlDependenceEditor<TDomain, TCategory>
            : ControlDependenceEditorDesign, INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Závislosti kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">Dependences of the area domain or subpopulation categories</para>
        /// </summary>
        private Dictionary<TDLDsityObject, SuperiorDomain<TDomain, TCategory>> dependences;

        private string msgNoneDomainSelectedForAddition = String.Empty;
        private string msgNoneDomainSelectedForRemoval = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlDependenceEditor(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Zavření ovládacího prvku"</para>
        /// <para lang="en">"Close user control" event</para>
        /// </summary>
        public event EventHandler DependenceEditorClosed;

        #endregion Events


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not IArealOrPopulationForm<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(IArealOrPopulationForm<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not IArealOrPopulationForm<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(IArealOrPopulationForm<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Základní formulář</para>
        /// <para lang="en">Base form</para>
        /// </summary>
        public IArealOrPopulationForm<TDomain, TCategory> Base
        {
            get
            {
                return
                    (IArealOrPopulationForm<TDomain, TCategory>)ControlOwner;
            }
        }

        /// <summary>
        /// <para lang="cs">Pár klasifikačních pravidel pro kladný a záporný příspěvek (read-only)</para>
        /// <para lang="en">Classification rule pair for positive and negative contribution (read-only)</para>
        /// </summary>
        private ClassificationRulePair<TDomain, TCategory> ClassificationRulePair
        {
            get
            {
                return Base.SelectedClassificationRulePair;
            }
        }

        /// <summary>
        /// <para lang="cs">Závislosti kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">Dependences of the area domain or subpopulation categories</para>
        /// </summary>
        public Dictionary<TDLDsityObject, SuperiorDomain<TDomain, TCategory>> Dependences
        {
            get
            {
                return dependences ?? [];
            }
            set
            {
                dependences = value ?? [];
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnDependenceSelect),              "Přidat vybranou závislost" },
                            { nameof(BtnDependenceCancel),              "Odebrat vybranou závislost" },
                            { nameof(BtnHierarchy),                     "Definovat hierarchii kategorií plošné domény" },
                            { nameof(GrpDependence),                    "Závislosti" },
                            { nameof(GrpDependencePossible),            "Plošné domény" },
                            { nameof(GrpDependenceSelected),            "Vybrané nadřazené plošné domény"},
                            { nameof(LstDependencePossible),            "ExtendedLabelCs" },
                            { nameof(LstDependenceSelected),            "ExtendedLabelCs" },
                            { nameof(TsrDependencePossible),            String.Empty },
                            { nameof(TsrDependenceSelected),            String.Empty },
                            { nameof(msgNoneDomainSelectedForAddition), "Není vybraná žádná plošná doména při přidání mezi nadřazené plošné domény." },
                            { nameof(msgNoneDomainSelectedForRemoval),  "Není vybraná žádná plošná doména pro odstranění z nadřazených plošných domén." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlDependenceEditor<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnDependenceSelect),              "Přidat vybranou závislost" },
                            { nameof(BtnDependenceCancel),              "Odebrat vybranou závislost" },
                            { nameof(BtnHierarchy),                     "Definovat hierarchii kategorií subpopulace" },
                            { nameof(GrpDependence),                    "Závislosti" },
                            { nameof(GrpDependencePossible),            "Subpopulace" },
                            { nameof(GrpDependenceSelected),            "Vybrané nadřazené subpopulace" },
                            { nameof(LstDependencePossible),            "ExtendedLabelCs" },
                            { nameof(LstDependenceSelected),            "ExtendedLabelCs" },
                            { nameof(TsrDependencePossible),            String.Empty },
                            { nameof(TsrDependenceSelected),            String.Empty },
                            { nameof(msgNoneDomainSelectedForAddition), "Není vybraná žádná subpopulace při přidání mezi nadřazené subpopulace." },
                            { nameof(msgNoneDomainSelectedForRemoval),  "Není vybraná žádná subpopulace pro odstranění z nadřazených subpopulací." }
                        }
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(ControlDependenceEditor<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                out Dictionary<string, string> dictNationalSubPopulation)
                                    ? dictNationalSubPopulation
                                    : [],

                    _ => [],
                },

                LanguageVersion.International =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnDependenceSelect),              "Add selected dependence" },
                            { nameof(BtnDependenceCancel),              "Remove selected dependence" },
                            { nameof(BtnHierarchy),                     "Define hierarchy of the area domain categories" },
                            { nameof(GrpDependence),                    "Dependencies" },
                            { nameof(GrpDependencePossible),            "Area domains"  },
                            { nameof(GrpDependenceSelected),            "Selected superior area domains" },
                            { nameof(LstDependencePossible),            "ExtendedLabelEn" },
                            { nameof(LstDependenceSelected),            "ExtendedLabelEn" },
                            { nameof(TsrDependencePossible),            String.Empty },
                            { nameof(TsrDependenceSelected),            String.Empty  },
                            { nameof(msgNoneDomainSelectedForAddition), "There is no selected area domain for addition into superior area domains." },
                            { nameof(msgNoneDomainSelectedForRemoval),  "There is no selected area domain for removal from superior area domains." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlDependenceEditor<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnDependenceSelect),              "Add selected dependence" },
                            { nameof(BtnDependenceCancel),              "Remove selected dependence" },
                            { nameof(BtnHierarchy),                     "Define hierarchy of the subpopulation categories" },
                            { nameof(GrpDependence),                    "Dependencies" },
                            { nameof(GrpDependencePossible),            "Subpopulations"  },
                            { nameof(GrpDependenceSelected),            "Selected superior subpopulations" },
                            { nameof(LstDependencePossible),            "ExtendedLabelEn" },
                            { nameof(LstDependenceSelected),            "ExtendedLabelEn" },
                            { nameof(TsrDependencePossible),            String.Empty },
                            { nameof(TsrDependenceSelected),            String.Empty },
                            { nameof(msgNoneDomainSelectedForAddition), "There is no selected subpopulation for addition into superior subpopulations." },
                            { nameof(msgNoneDomainSelectedForRemoval),  "There is no selected subpopulation for removal from superior subpopulations." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlDependenceEditor<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BackColor = System.Drawing.SystemColors.Control;

            ControlOwner = controlOwner;

            Dependences = null;

            LoadContent();

            InitializeToolStripIcons();

            InitializeListBoxDependencePossible();

            InitializeListBoxDependenceSelected();

            InitializeLabels();

            BtnClose.Click += new EventHandler(
                (sender, e) => { DependenceEditorClosed?.Invoke(sender: this, e: new EventArgs()); });

            BtnDependenceSelect.Click += new EventHandler(
                (sender, e) => { SelectDependence(); });

            BtnDependenceCancel.Click += new EventHandler(
                (sender, e) => { CancelDependence(); });

            BtnHierarchy.Click += new EventHandler(
                (sender, e) => { DefineHierarchy(); });

            BtnDependenceSelect.Enabled = !LstDependenceSelected.Items.OfType<TDomain>().Any();
            BtnDependenceCancel.Enabled = !BtnDependenceSelect.Enabled;
            BtnHierarchy.Enabled = !BtnDependenceSelect.Enabled;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace ikon tlačítek</para>
        /// <para lang="en">Initializing the button icons</para>
        /// </summary>
        private void InitializeToolStripIcons()
        {
            ControlResources resources = new();

            BtnDependenceSelect.Image = resources.IconArrowRight.Image;
            BtnDependenceSelect.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnDependenceCancel.Image = resources.IconArrowLeft.Image;
            BtnDependenceCancel.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnHierarchy.Image = resources.IconSchema.Image;
            BtnHierarchy.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnClose.Image = resources.IconRedCross.Image;
            BtnClose.DisplayStyle = ToolStripItemDisplayStyle.Image;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing user control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            BtnDependenceSelect.Text =
                labels.TryGetValue(
                    key: nameof(BtnDependenceSelect),
                    out string btnDependenceSelectText)
                        ? btnDependenceSelectText
                        : String.Empty;

            BtnDependenceCancel.Text =
                labels.TryGetValue(
                    key: nameof(BtnDependenceCancel),
                    out string btnDependenceCancelText)
                        ? btnDependenceCancelText
                        : String.Empty;

            BtnHierarchy.Text =
                labels.TryGetValue(
                    key: nameof(BtnHierarchy),
                    out string btnHierarchyText)
                        ? btnHierarchyText
                        : String.Empty;

            GrpDependence.Text =
                labels.TryGetValue(
                    key: nameof(GrpDependence),
                    out string grpDependenceText)
                        ? grpDependenceText
                        : String.Empty;

            GrpDependencePossible.Text =
                labels.TryGetValue(
                    key: nameof(GrpDependencePossible),
                    out string grpDependencePossibleText)
                        ? grpDependencePossibleText
                        : String.Empty;

            GrpDependenceSelected.Text =
                labels.TryGetValue(
                    key: nameof(GrpDependenceSelected),
                    out string grpDependenceSelectedText)
                        ? grpDependenceSelectedText
                        : String.Empty;

            LstDependencePossible.DisplayMember =
                labels.TryGetValue(
                    key: nameof(LstDependencePossible),
                    out string lstDependencePossibleText)
                        ? lstDependencePossibleText
                        : String.Empty;

            LstDependenceSelected.DisplayMember =
                labels.TryGetValue(
                    key: nameof(LstDependenceSelected),
                    out string lstDependenceSelectedText)
                        ? lstDependenceSelectedText
                        : String.Empty;

            TsrDependencePossible.Text =
                labels.TryGetValue(
                    key: nameof(TsrDependencePossible),
                    out string tsrDependencePossibleText)
                        ? tsrDependencePossibleText
                        : String.Empty;

            TsrDependenceSelected.Text =
                labels.TryGetValue(
                    key: nameof(TsrDependenceSelected),
                    out string tsrDependenceSelectedText)
                        ? tsrDependenceSelectedText
                        : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ListBox "Possible Dependencies"</para>
        /// <para lang="en">Initializing items in ListBox "Possible Dependencies"</para>
        /// </summary>
        private void InitializeListBoxDependencePossible()
        {
            LstDependencePossible.DataSource = null;
            LstDependencePossible.DataSource =
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain =>
                        Database.STargetData.CAreaDomain.Items
                            .Where(a => !a.Equals(obj: Base.Domain))
                            .ToList<TDAreaDomain>(),
                    TDArealOrPopulationEnum.Population =>
                        Database.STargetData.CSubPopulation.Items
                            .Where(a => !a.Equals(obj: Base.Domain))
                            .ToList<TDSubPopulation>(),
                    _ => null,
                };

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            LstDependencePossible.DisplayMember =
                labels.TryGetValue(
                    key: nameof(LstDependencePossible),
                    out string lstDependencePossibleText)
                        ? lstDependencePossibleText
                        : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ListBox "Selected Dependencies"</para>
        /// <para lang="en">Initializing items in ListBox "Selected Dependencies"</para>
        /// </summary>
        private void InitializeListBoxDependenceSelected()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            LstDependenceSelected.DataSource = null;

            LstDependenceSelected.DisplayMember =
                labels.TryGetValue(
                    key: nameof(LstDependenceSelected),
                    out string lstDependenceSelectedText)
                        ? lstDependenceSelectedText
                        : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Přidá vybranou závislost (nadřazenou doménu)</para>
        /// <para lang="en">Adds selected dependence (superior domain)</para>
        /// </summary>
        private void SelectDependence()
        {
            if (LstDependenceSelected.Items.OfType<TDomain>().Any())
            {
                // Seznam vybraných nadřazených domén již něco obsahuje.
                // Vybraná nadřazená doména může být pouze jedna.
                // Nelze přidávat další.
                return;
            }

            if (LstDependencePossible.SelectedItem == null)
            {
                // Není vybraná žádná plošná doména nebo subpopulace při přidání mezi nadřazené plošné domény nebo subpopulace
                // There is none selected area domain or subpopulation for addition into superior area domains or subpopulations

                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: nameof(msgNoneDomainSelectedForAddition),
                            out msgNoneDomainSelectedForAddition)
                                ? msgNoneDomainSelectedForAddition
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            TDomain selectedDomain =
                (TDomain)LstDependencePossible.SelectedItem;

            if (selectedDomain == null)
            {
                // Není vybraná žádná plošná doména nebo subpopulace při přidání mezi nadřazené plošné domény nebo subpopulace
                // There is none selected area domain or subpopulation for addition into superior area domains or subpopulations

                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: nameof(msgNoneDomainSelectedForAddition),
                            out msgNoneDomainSelectedForAddition)
                                ? msgNoneDomainSelectedForAddition
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            LstDependenceSelected.Items.Add(
                item: selectedDomain);

            BtnDependenceSelect.Enabled = !LstDependenceSelected.Items.OfType<TDomain>().Any();
            BtnDependenceCancel.Enabled = !BtnDependenceSelect.Enabled;
            BtnHierarchy.Enabled = !BtnDependenceSelect.Enabled;

            LstDependenceSelected.SelectedIndex = 0;
            Dependences = null;

            foreach (TDLDsityObject ldsityObjectForADSP in Base.LDsityObjectsForADSP)
            {
                Dependences.Add(
                    key: ldsityObjectForADSP,
                    value:
                        new SuperiorDomain<TDomain, TCategory>(
                            owner: this,
                            domain: selectedDomain,
                            inferiorDomain: Base.Domain,
                            inferiorCategories: Base.Categories));
            }
        }

        /// <summary>
        /// <para lang="cs">Odebere vybranou závislost (nadřazenou doménu)</para>
        /// <para lang="en">Removes selected dependence (superior domain)</para>
        /// </summary>
        private void CancelDependence()
        {
            if (!LstDependenceSelected.Items.OfType<TDomain>().Any())
            {
                // Seznam vybraných nadřazených domén nic neobsahuje.
                // Není co odebírat
                return;
            }

            LstDependenceSelected.SelectedIndex = 0;

            if (LstDependenceSelected.SelectedItem == null)
            {
                // Není vybraná žádná plošná doména nebo subpopulace pro odstranění z nadřazených plošných domén nebo subpopulací
                // There is none selected area domain or subpopulation for removal from superior area domains or subpopulations."

                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: nameof(msgNoneDomainSelectedForRemoval),
                                out msgNoneDomainSelectedForRemoval)
                                    ? msgNoneDomainSelectedForRemoval
                                    : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            TDomain selectedDomain =
                (TDomain)LstDependenceSelected.SelectedItem;

            if (selectedDomain == null)
            {
                // Není vybraná žádná plošná doména nebo subpopulace pro odstranění z nadřazených plošných domén nebo subpopulací
                // There is none selected area domain or subpopulation for removal from superior area domains or subpopulations."

                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: nameof(msgNoneDomainSelectedForRemoval),
                            out msgNoneDomainSelectedForRemoval)
                                ? msgNoneDomainSelectedForRemoval
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            LstDependenceSelected.Items.Remove(
                value: selectedDomain);

            BtnDependenceSelect.Enabled = !LstDependenceSelected.Items.OfType<TDomain>().Any();
            BtnDependenceCancel.Enabled = !BtnDependenceSelect.Enabled;
            BtnHierarchy.Enabled = !BtnDependenceSelect.Enabled;

            Dependences = null;
        }

        /// <summary>
        /// <para lang="cs">Definuje novou hierarchii kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">Defines new hierarchy of area domain or subpopulation categories</para>
        /// </summary>
        private void DefineHierarchy()
        {
            if (ClassificationRulePair == null)
            {
                return;
            }

            if (ClassificationRulePair.LDsityObjectForADSP == null)
            {
                return;
            }

            if (!Dependences.TryGetValue(
                key: ClassificationRulePair.LDsityObjectForADSP,
                out SuperiorDomain<TDomain, TCategory> value))
            {
                return;
            }

            FormHierarchy<TDomain, TCategory> form
                = new(
                    controlOwner: ControlOwner,
                    domain: value);

            if (form.ShowDialog() == DialogResult.OK)
            {
                value.ConfirmChanges();
            }
            else
            {
                value.CancelChanges();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení ovládacího prvku</para>
        /// <para lang="en">Reset control</para>
        /// </summary>
        public void Reset()
        {
            DependenceEditorClosed?.Invoke(sender: this, e: new EventArgs());
        }

        #endregion Methods

    }

}