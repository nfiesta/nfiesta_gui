﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení párů panel a referenčního období</para>
    /// <para lang="en">Control for displaying panel and referenece year set pairs</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlPanelRefYearSetList
             : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Private Classes

        /// <summary>
        /// <para lang="cs">Objekt štítku pro Label</para>
        /// <para lang="en">Tag object for Label</para>
        /// </summary>
        private class LabelTag
        {
            /// <summary>
            /// <para lang="cs">Číslo řádku (hlavička má 0)</para>
            /// <para lang="en">Line number (header has 0)</para>
            /// </summary>
            public int Line { get; set; }

            /// <summary>
            /// <para lang="cs">Identifikační číslo panelu a referenčního období</para>
            /// <para lang="en">Panel reference year set identifier</para>
            /// </summary>
            public Nullable<int> PanelReferenceYearSetId { get; set; }

            /// <summary>
            /// <para lang="cs">Stav přípravy lokálních hustot</para>
            /// <para lang="en">Local density calculation state</para>
            /// </summary>
            public LocalDensityCalculationState State { get; set; }
        }

        #endregion Private Classes


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Seznam kombinací panelů a referenčích období patřících do jedné skupiny</para>
        /// <para lang="en">List of panel reference year set combinations belonging to a particuliar group</para>
        /// </summary>
        private TFnGetRefYearSetToPanelMappingForGroupList refYearSetToPanelMappings;

        /// <summary>
        /// <para lang="cs">Řídící vlákno</para>
        /// <para lang="en">Control thread</para>
        /// </summary>
        private IControlThread controlThread;

        private string msgPanelsNoneSelected = String.Empty;
        private string msgFnSystemUtilizationIsNull = String.Empty;
        private string msgFnSystemUtilizationLoadMinIsNull = String.Empty;
        private string msgFnSystemUtilizationCPUCountIsNull = String.Empty;
        private string msgFnSystemUtilizationHighLoad = String.Empty;
        private string msgCheckSystemResourcesAvailability = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Popisek sloupečku s identifikátorem</para>
        /// <para lang="en">Label of the column with identifier</para>
        /// </summary>
        private Label lblIdentifierHeader;

        /// <summary>
        /// <para lang="cs">Popisek sloupečku s panelem</para>
        /// <para lang="en">Label of the column with panel</para>
        /// </summary>
        private Label lblPanelHeader;

        /// <summary>
        /// <para lang="cs">Popisek sloupečku s referenčním obdobím</para>
        /// <para lang="en">Label of the column with reference year set</para>
        /// </summary>
        private Label lblReferenceYearSetHeader;

        /// <summary>
        /// <para lang="cs">Popisek sloupečku se stavem přípravy lokálních hustot</para>
        /// <para lang="en">Label of the column with state of local density calculation</para>
        /// </summary>
        private Label lblStatusHeader;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro cílová data"</para>
        /// <para lang="en">Control "Module for target data"</para>
        /// </summary>
        public ControlTargetData CtrTargetData
        {
            get
            {
                return (ControlTargetData)
                    ((ControlTDSelectionPanelRefYearSetGroup)ControlOwner)
                    .ControlOwner;
            }
        }

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlPanelRefYearSetList(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionPanelRefYearSetGroup)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTDSelectionPanelRefYearSetGroup)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionPanelRefYearSetGroup)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTDSelectionPanelRefYearSetGroup)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Seznam kombinací panelů a referenčích období patřících do jedné skupiny</para>
        /// <para lang="en">List of panel reference year set combinations belonging to a particuliar group</para>
        /// </summary>
        public TFnGetRefYearSetToPanelMappingForGroupList RefYearSetToPanelMappings
        {
            get
            {
                return refYearSetToPanelMappings;
            }
            set
            {
                refYearSetToPanelMappings = value;

                InitializeTableHeader();
                InitializeTableRows();
            }
        }

        /// <summary>
        /// <para lang="cs">Skrýt nebo zobrazit sloupec s identifikátorem</para>
        /// <para lang="en">Hide or display identifier column</para>
        /// </summary>
        public bool IdentifierVisible
        {
            get
            {
                return !splIdentifier.Panel1Collapsed;
            }
            set
            {
                splIdentifier.Panel1Collapsed = !value;
            }
        }

        /// <summary>
        /// <para lang="cs">Skrýt nebo zobrazit sloupec se stavem přípravy lokálních hustot</para>
        /// <para lang="en">Hide or display column with state of local density calculation</para>
        /// </summary>
        public bool StatusVisible
        {
            get
            {
                return !splStatus.Panel1Collapsed;
            }
            set
            {
                splStatus.Panel1Collapsed = !value;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná (vybraná na 2. nebo 3. formuláři) (read-only)</para>
        /// <para lang="en">Selected target variable (selected on the 2nd or 3rd form) (read-only)</para>
        /// </summary>
        public TDTargetVariable SelectedTargetVariable
        {
            get
            {
                return
                    ((ControlTDSelectionPanelRefYearSetGroup)ControlOwner)
                    .SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Řídící vlákno</para>
        /// <para lang="en">Control thread</para>
        /// </summary>
        public IControlThread ControlThread
        {
            get
            {
                return controlThread;
            }
            private set
            {
                controlThread = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(lblIdentifierHeader),                      "ID" },
                        { nameof(lblPanelHeader),                           "Panel" },
                        { nameof(lblReferenceYearSetHeader),                "Referenční období" },
                        { nameof(lblStatusHeader),                          "Stav lokálních hustot" },
                        { nameof(msgPanelsNoneSelected),                    "Není zvolen žádný panel pro výpočet." },
                        { nameof(msgFnSystemUtilizationIsNull),             "Uložená procedure fn_system_utilization vrátila NULL." },
                        { nameof(msgFnSystemUtilizationLoadMinIsNull),      "Hodnotu load_1_min se nepodařilo získat z výsledku uložené procedury fn_system_utilization." },
                        { nameof(msgFnSystemUtilizationCPUCountIsNull),     "Hodnotu cpu_count se nepodařilo získat z výsledku uložené procedury fn_system_utilization." },
                        { nameof(msgFnSystemUtilizationHighLoad),           "Vysoké zatížení systému." },
                        { nameof(msgCheckSystemResourcesAvailability),      String.Concat(
                                                                                "Požadovaný počet připojení $1 ",
                                                                                "přesahuje počet dostupných připojení. $0",
                                                                                "Výpočet bude spuštěn na dostupných $2 připojeních.") }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlPanelRefYearSetList),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                    (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(lblIdentifierHeader),                  "ID" },
                            { nameof(lblPanelHeader),                       "Panel" },
                            { nameof(lblReferenceYearSetHeader),            "Reference year set" },
                            { nameof(lblStatusHeader),                      "Local densities state" },
                            { nameof(msgPanelsNoneSelected),                "No panel is selected for calculation." },
                            { nameof(msgFnSystemUtilizationIsNull),         "Stored procedure fn_system_utilization returned NULL." },
                            { nameof(msgFnSystemUtilizationLoadMinIsNull),  "The load_1_min value could not be obtained from the result of the fn_system_utilization stored procedure." },
                            { nameof(msgFnSystemUtilizationCPUCountIsNull), "The cpu_count value could not be obtained from the result of the fn_system_utilization stored procedure." },
                            { nameof(msgFnSystemUtilizationHighLoad),       "High system load." },
                            { nameof(msgCheckSystemResourcesAvailability),  String.Concat(
                                                                                "Required number of connections $1 ",
                                                                                "exceeds the number of available connections. $0",
                                                                                "The calculation will be run on the available $2 connections.") }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: nameof(ControlPanelRefYearSetList),
                            out Dictionary<string, string> dictInternational)
                                ? dictInternational
                                : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            RefYearSetToPanelMappings =
                new TFnGetRefYearSetToPanelMappingForGroupList(
                    database: Database);
            ControlThread = null;

            pnlMain.Resize += new EventHandler(
                (sender, e) => { PanelMainResize(); });
            PanelMainResize();

            splIdentifier.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    Setting.ColPanelRefYearSetListIdentifierWidth = splIdentifier.SplitterDistance;
                });

            splPanel.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    Setting.ColPanelWidth = splPanel.SplitterDistance;
                });

            splReferenceYearSet.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    Setting.ColReferenceYearSetWidth = splReferenceYearSet.SplitterDistance;
                });

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Nastavení šířky sloupců tabulky při změně velikosti hlavního panelu</para>
        /// <para lang="en">Setting the width of table columns when resizing the main panel</para>
        /// </summary>
        private void PanelMainResize()
        {
            int distance = Setting.ColPanelRefYearSetListIdentifierWidth;
            if ((distance > splIdentifier.Panel1MinSize) &&
                (distance < (splIdentifier.Width - splIdentifier.Panel2MinSize)))
            {
                splIdentifier.SplitterDistance = distance;
            }

            distance = Setting.ColPanelWidth;
            if ((distance > splPanel.Panel1MinSize) &&
                (distance < (splPanel.Width - splPanel.Panel2MinSize)))
            {
                splPanel.SplitterDistance = distance;
            }

            distance = Setting.ColReferenceYearSetWidth;
            if ((distance > splReferenceYearSet.Panel1MinSize) &&
                (distance < (splReferenceYearSet.Width - splReferenceYearSet.Panel2MinSize)))
            {
                splReferenceYearSet.SplitterDistance = distance;
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: CtrTargetData.LanguageFile);

            msgPanelsNoneSelected =
                labels.TryGetValue(key: nameof(msgPanelsNoneSelected),
                        out msgPanelsNoneSelected)
                            ? msgPanelsNoneSelected
                            : String.Empty;

            msgFnSystemUtilizationIsNull =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationIsNull),
                       out msgFnSystemUtilizationIsNull)
                           ? msgFnSystemUtilizationIsNull
                           : String.Empty;

            msgFnSystemUtilizationLoadMinIsNull =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationLoadMinIsNull),
                       out msgFnSystemUtilizationLoadMinIsNull)
                           ? msgFnSystemUtilizationLoadMinIsNull
                           : String.Empty;

            msgFnSystemUtilizationCPUCountIsNull =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationCPUCountIsNull),
                       out msgFnSystemUtilizationCPUCountIsNull)
                           ? msgFnSystemUtilizationCPUCountIsNull
                           : String.Empty;

            msgFnSystemUtilizationHighLoad =
               labels.TryGetValue(key: nameof(msgFnSystemUtilizationHighLoad),
                       out msgFnSystemUtilizationHighLoad)
                           ? msgFnSystemUtilizationHighLoad
                           : String.Empty;

            msgCheckSystemResourcesAvailability =
              labels.TryGetValue(key: nameof(msgCheckSystemResourcesAvailability),
                      out msgCheckSystemResourcesAvailability)
                          ? msgCheckSystemResourcesAvailability
                          : String.Empty;

            SetLabelText(
                label: lblIdentifierHeader,
                toolTip: toolTip,
                text:
                    labels.TryGetValue(
                        key: nameof(lblIdentifierHeader),
                        out string lblIdentifierHeaderText)
                            ? lblIdentifierHeaderText
                            : String.Empty,
                reset: true);

            SetLabelText(
                label: lblPanelHeader,
                toolTip: toolTip,
                text:
                    labels.TryGetValue(
                        key: nameof(lblPanelHeader),
                        out string lblPanelHeaderText)
                            ? lblPanelHeaderText
                            : String.Empty,
                reset: true);

            SetLabelText(
                label: lblReferenceYearSetHeader,
                toolTip: toolTip,
                text:
                    labels.TryGetValue(
                        key: nameof(lblReferenceYearSetHeader),
                        out string lblReferenceYearSetHeaderText)
                            ? lblReferenceYearSetHeaderText
                            : String.Empty,
                reset: true);

            SetLabelText(
                label: lblStatusHeader,
                toolTip: toolTip,
                text:
                    labels.TryGetValue(
                        key: nameof(lblStatusHeader),
                        out string lblStatusHeaderText)
                            ? lblStatusHeaderText
                            : String.Empty,
                reset: true);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace hlavičky tabulky</para>
        /// <para lang="en">Initializing table header</para>
        /// </summary>
        private void InitializeTableHeader()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: CtrTargetData.LanguageFile);

            int line = 0;

            pnlIdentifier.Controls.Clear();
            lblIdentifierHeader = new Label()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                AutoSize = false,
                BackColor = Color.DarkBlue,
                BorderStyle = BorderStyle.FixedSingle,
                Dock = DockStyle.None,
                Font = new Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9.75F,
                    style: FontStyle.Bold,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                ForeColor = Color.White,
                Height = Setting.PanelRefYearSetListHeaderHeight + 1,
                Left = 0,
                Name = nameof(lblIdentifierHeader),
                Tag = new LabelTag
                {
                    Line = 0,
                    PanelReferenceYearSetId = null,
                    State = LocalDensityCalculationState.None
                },
                Top = line * Setting.PanelRefYearSetListHeaderHeight,
                Width = pnlIdentifier.Width
            };
            lblIdentifierHeader.CreateControl();
            SetLabelText(
                label: lblIdentifierHeader,
                toolTip: toolTip,
                text:
                    labels.TryGetValue(
                        key: nameof(lblIdentifierHeader),
                        out string lblIdentifierHeaderText)
                            ? lblIdentifierHeaderText
                            : String.Empty,
                reset: true);
            pnlIdentifier.Controls.Add(value: lblIdentifierHeader);

            pnlPanel.Controls.Clear();
            lblPanelHeader = new Label()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                AutoSize = false,
                BackColor = Color.DarkBlue,
                BorderStyle = BorderStyle.FixedSingle,
                Dock = DockStyle.None,
                Font = new Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9.75F,
                    style: FontStyle.Bold,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                ForeColor = Color.White,
                Height = Setting.PanelRefYearSetListHeaderHeight + 1,
                Left = 0,
                Name = nameof(lblPanelHeader),
                Tag = new LabelTag
                {
                    Line = 0,
                    PanelReferenceYearSetId = null,
                    State = LocalDensityCalculationState.None
                },
                Top = line * Setting.PanelRefYearSetListHeaderHeight,
                Width = pnlPanel.Width
            };
            lblPanelHeader.CreateControl();
            SetLabelText(
                label: lblPanelHeader,
                toolTip: toolTip,
                text:
                    labels.TryGetValue(
                        key: nameof(lblPanelHeader),
                        out string lblPanelHeaderText)
                            ? lblPanelHeaderText
                            : String.Empty,
                reset: true);
            pnlPanel.Controls.Add(value: lblPanelHeader);

            pnlReferenceYearSet.Controls.Clear();
            lblReferenceYearSetHeader = new Label()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                AutoSize = false,
                BackColor = Color.DarkBlue,
                BorderStyle = BorderStyle.FixedSingle,
                Dock = DockStyle.None,
                Font = new Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9.75F,
                    style: FontStyle.Bold,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                ForeColor = Color.White,
                Height = Setting.PanelRefYearSetListHeaderHeight + 1,
                Left = 0,
                Name = nameof(lblReferenceYearSetHeader),
                Tag = new LabelTag
                {
                    Line = 0,
                    PanelReferenceYearSetId = null,
                    State = LocalDensityCalculationState.None
                },
                Top = line * Setting.PanelRefYearSetListHeaderHeight,
                Width = pnlReferenceYearSet.Width
            };
            lblReferenceYearSetHeader.CreateControl();
            SetLabelText(
                label: lblReferenceYearSetHeader,
                toolTip: toolTip,
                text:
                    labels.TryGetValue(
                        key: nameof(lblReferenceYearSetHeader),
                        out string lblReferenceYearSetHeaderText)
                            ? lblReferenceYearSetHeaderText
                            : String.Empty,
                reset: true);
            pnlReferenceYearSet.Controls.Add(value: lblReferenceYearSetHeader);

            pnlStatus.Controls.Clear();
            lblStatusHeader = new Label()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                AutoSize = false,
                BackColor = Color.DarkBlue,
                BorderStyle = BorderStyle.FixedSingle,
                Dock = DockStyle.None,
                Font = new Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9.75F,
                    style: FontStyle.Bold,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                ForeColor = Color.White,
                Height = Setting.PanelRefYearSetListHeaderHeight + 1,
                Left = 0,
                Name = nameof(lblStatusHeader),
                Tag = new LabelTag
                {
                    Line = 0,
                    PanelReferenceYearSetId = null,
                    State = LocalDensityCalculationState.None
                },
                Top = line * Setting.PanelRefYearSetListHeaderHeight,
                Width = pnlStatus.Width
            };
            lblStatusHeader.CreateControl();
            SetLabelText(
               label: lblStatusHeader,
               toolTip: toolTip,
               text:
                   labels.TryGetValue(
                        key: nameof(lblStatusHeader),
                        out string lblStatusHeaderText)
                            ? lblStatusHeaderText
                            : String.Empty,
               reset: true);
            pnlStatus.Controls.Add(value: lblStatusHeader);
        }

        /// <summary>
        /// <para lang="cs">Inicializace řádků tabulky</para>
        /// <para lang="en">Initializing table rows</para>
        /// </summary>
        private void InitializeTableRows()
        {
            if (RefYearSetToPanelMappings == null)
            {
                return;
            }

            int line = -1; // odpočítává hlavičku

            foreach (TFnGetRefYearSetToPanelMappingForGroup refYearSetToPanelMapping in
                RefYearSetToPanelMappings.Items
                    .OrderBy(a => a.PanelDescription)
                    .ThenBy(a => a.RefYearSetDescription))
            {
                line++;

                string strIdentifier = refYearSetToPanelMapping.Id.ToString();

                Label lblIdentifierValue = new()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                    AutoSize = false,
                    BackColor = Color.White,
                    BorderStyle = BorderStyle.FixedSingle,
                    Dock = DockStyle.None,
                    Font = new Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: FontStyle.Regular,
                        unit: GraphicsUnit.Point,
                        gdiCharSet: (byte)238),
                    ForeColor = Color.Black,
                    Height = Setting.PanelRefYearSetListRowHeight + 1,
                    Left = 0,
                    Name = $"lblIdentifier{strIdentifier}",
                    Tag = new LabelTag
                    {
                        Line = line,
                        PanelReferenceYearSetId = refYearSetToPanelMapping.Id,
                        State = LocalDensityCalculationState.Waiting,
                    },
                    Top = Setting.PanelRefYearSetListHeaderHeight + line * Setting.PanelRefYearSetListRowHeight,
                    Width = pnlIdentifier.Width
                };
                lblIdentifierValue.CreateControl();
                SetLabelText(
                    label: lblIdentifierValue,
                    toolTip: toolTip,
                    text: strIdentifier,
                    reset: true);
                pnlIdentifier.Controls.Add(value: lblIdentifierValue);

                Label lblPanelValue = new()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                    AutoSize = false,
                    BackColor = Color.White,
                    BorderStyle = BorderStyle.FixedSingle,
                    Dock = DockStyle.None,
                    Font = new Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: FontStyle.Regular,
                        unit: GraphicsUnit.Point,
                        gdiCharSet: (byte)238),
                    ForeColor = Color.Black,
                    Height = Setting.PanelRefYearSetListRowHeight + 1,
                    Left = 0,
                    Name = $"lblPanel{strIdentifier}",
                    Tag = new LabelTag
                    {
                        Line = line,
                        PanelReferenceYearSetId = refYearSetToPanelMapping.Id,
                        State = LocalDensityCalculationState.Waiting,
                    },
                    Top = Setting.PanelRefYearSetListHeaderHeight + line * Setting.PanelRefYearSetListRowHeight,
                    Width = pnlPanel.Width
                };
                lblPanelValue.CreateControl();
                SetLabelText(
                    label: lblPanelValue,
                    toolTip: toolTip,
                    text: refYearSetToPanelMapping.PanelLabel,
                    reset: true);
                pnlPanel.Controls.Add(value: lblPanelValue);

                Label lblReferenceYearSetValue = new()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                    AutoSize = false,
                    BackColor = Color.White,
                    BorderStyle = BorderStyle.FixedSingle,
                    Dock = DockStyle.None,
                    Font = new Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: FontStyle.Regular,
                        unit: GraphicsUnit.Point,
                        gdiCharSet: (byte)238),
                    ForeColor = Color.Black,
                    Height = Setting.PanelRefYearSetListRowHeight + 1,
                    Left = 0,
                    Name = $"lblReferenceYearSet{strIdentifier}",
                    Tag = new LabelTag
                    {
                        Line = line,
                        PanelReferenceYearSetId = refYearSetToPanelMapping.Id,
                        State = LocalDensityCalculationState.Waiting,
                    },
                    Top = Setting.PanelRefYearSetListHeaderHeight + line * Setting.PanelRefYearSetListRowHeight,
                    Width = pnlReferenceYearSet.Width
                };
                lblReferenceYearSetValue.CreateControl();
                SetLabelText(
                    label: lblReferenceYearSetValue,
                    toolTip: toolTip,
                    text: refYearSetToPanelMapping.RefYearSetLabel,
                    reset: true);
                pnlReferenceYearSet.Controls.Add(value: lblReferenceYearSetValue);

                Label lblStatusValue = new()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                    AutoSize = false,
                    BackColor = Color.White,
                    BorderStyle = BorderStyle.FixedSingle,
                    Dock = DockStyle.None,
                    Font = new Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: FontStyle.Regular,
                        unit: GraphicsUnit.Point,
                        gdiCharSet: (byte)238),
                    ForeColor = Color.Black,
                    Height = Setting.PanelRefYearSetListRowHeight + 1,
                    Left = 0,
                    Name = $"lblStatus{strIdentifier}",
                    Tag = new LabelTag
                    {
                        Line = line,
                        PanelReferenceYearSetId = refYearSetToPanelMapping.Id,
                        State = LocalDensityCalculationState.Waiting,
                    },
                    Text = String.Empty,
                    Top = Setting.PanelRefYearSetListHeaderHeight + line * Setting.PanelRefYearSetListRowHeight,
                    Width = pnlStatus.Width
                };
                lblStatusValue.CreateControl();
                SetLabelText(
                    label: lblStatusValue,
                    toolTip: toolTip,
                    text: String.Empty,
                    reset: true);
                pnlStatus.Controls.Add(value: lblStatusValue);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// ThreadSave nastavení popisku ovládacího prvku Label
        /// z hlavního i pracovních vláken</para>
        /// <para lang="en">
        /// ThreadSave Label control text settings
        /// from main and worker threads</para>
        /// </summary>
        /// <param name="label">
        /// <para lang="cs">Ovládací prvek Label</para>
        /// <para lang="en">Label control</para>
        /// </param>
        /// <param name="toolTip">
        /// <para lang="cs">Ovládací prvek ToolTip</para>
        /// <para lang="en">ToolTip control</para>
        /// </param>
        /// <param name="text">
        /// <para lang="cs">Text pro zobrazení v ovládacím prvku Label</para>
        /// <para lang="en">Text for display in Label control</para>
        /// </param>
        /// <param name="reset">
        /// <para lang="cs">
        /// reset false: připojí k popisku další text,
        /// reset true: nahradí popisek novým textem
        /// </para>
        /// <para lang="en">
        /// reset false: adds additional text to the label,
        /// reset true: replaces the text in label with new text
        /// </para>
        /// </param>
        /// <param name="maxLength">
        /// <para lang="cs">Maximální počet znaků</para>
        /// <para lang="en">Text for display in Label control</para>
        /// </param>
        private static void SetLabelText(
            Label label,
            ToolTip toolTip = null,
            string text = null,
            bool reset = true,
            int maxLength = 1000)
        {
            if (label == null)
            {
                return;
            }

            if (label.InvokeRequired)
            {
                label.BeginInvoke(
                    method: (MethodInvoker)delegate ()
                    {
                        setLabelText();
                        setToolTipText();
                    });
            }
            else
            {
                setLabelText();
                setToolTipText();
            }

            void setLabelText()
            {
                text ??= String.Empty;

                string briefText =
                    (text.Length < maxLength)
                        ? text
                        : String.Concat(text[..maxLength], " ... ");

                if (reset)
                {
                    // reset true: nahradí popisek novým textem
                    label.Text = briefText ?? String.Empty;
                    return;
                }

                // reset false: připojí k popisku další text
                if (!String.IsNullOrEmpty(value: briefText))
                {
                    if (!String.IsNullOrEmpty(value: label.Text))
                    {
                        label.Text = String.Concat(label.Text, "; ", briefText);
                    }
                    else
                    {
                        label.Text = briefText;
                    }
                }
            }

            void setToolTipText()
            {
                if (label == null)
                {
                    return;
                }

                if (toolTip == null)
                {
                    return;
                }

                toolTip.SetToolTip(
                       control: label,
                       caption: label.Text);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// ThreadSave nastavení barvy ovládacího prvku Label
        /// z hlavního nebo pracovních vláken</para>
        /// <para lang="en">
        /// ThreadSave Label control color settings
        /// from main or worker threads</para>
        /// </summary>
        /// <param name="label">
        /// <para lang="cs">Ovládací prvek Label</para>
        /// <para lang="en">Label control</para>
        /// </param>
        /// <param name="state">
        /// <para lang="cs">Stav přípravy lokálních hustot</para>
        /// <para lang="en">Local density calculation state</para>
        /// </param>
        /// <param name="reset">
        /// <para lang="cs">Počáteční nastevení</para>
        /// <para lang="en">Initial setup</para>
        /// </param>
        private static void SetLabelColor(
            Label label,
            LocalDensityCalculationState state,
            bool reset)
        {
            if (label == null)
            {
                return;
            }

            if (label.InvokeRequired)
            {
                label.BeginInvoke(
                    method: (MethodInvoker)delegate () { setLabelColor(); });
            }
            else
            {
                setLabelColor();
            }

            void setLabelColor()
            {
                LabelTag tag = (LabelTag)label.Tag;

                if (reset)
                {
                    // reset nastaví barvu na počáteční
                    label.BackColor = Color.White;
                    label.ForeColor = Color.Black;
                    label.Font = new Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: FontStyle.Regular,
                        unit: GraphicsUnit.Point,
                        gdiCharSet: (byte)238);
                    label.Tag = new LabelTag
                    {
                        Line = tag.Line,
                        PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                        State = LocalDensityCalculationState.Waiting
                    };
                }

                else if (tag.State == LocalDensityCalculationState.DoneWithError)
                {
                    // Panely, které dříve skončily chybou,
                    // zůstávají stále označené, že jsou s chybou.
                    label.BackColor = Color.Bisque;
                    label.ForeColor = Color.Black;
                    label.Font = new Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9.75F,
                        style: FontStyle.Regular,
                        unit: GraphicsUnit.Point,
                        gdiCharSet: (byte)238);
                    label.Tag = new LabelTag
                    {
                        Line = tag.Line,
                        PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                        State = LocalDensityCalculationState.DoneWithError
                    };
                }

                else
                {
                    switch (state)
                    {
                        case LocalDensityCalculationState.Waiting:
                            label.BackColor = Color.White;
                            label.ForeColor = Color.Black;
                            label.Font = new Font(
                              familyName: "Microsoft Sans Serif",
                              emSize: 9.75F,
                              style: FontStyle.Regular,
                              unit: GraphicsUnit.Point,
                              gdiCharSet: (byte)238);
                            label.Tag = new LabelTag
                            {
                                Line = tag.Line,
                                PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                                State = LocalDensityCalculationState.Waiting
                            };
                            break;

                        case LocalDensityCalculationState.Active:
                            label.BackColor = Color.LightBlue;
                            label.ForeColor = Color.Black;
                            label.Font = new Font(
                              familyName: "Microsoft Sans Serif",
                              emSize: 9.75F,
                              style: FontStyle.Regular,
                              unit: GraphicsUnit.Point,
                              gdiCharSet: (byte)238);
                            label.Tag = new LabelTag
                            {
                                Line = tag.Line,
                                PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                                State = LocalDensityCalculationState.Active
                            };
                            break;

                        case LocalDensityCalculationState.DoneWithError:
                            label.BackColor = Color.Bisque;
                            label.ForeColor = Color.Black;
                            label.Font = new Font(
                              familyName: "Microsoft Sans Serif",
                              emSize: 9.75F,
                              style: FontStyle.Regular,
                              unit: GraphicsUnit.Point,
                              gdiCharSet: (byte)238);
                            label.Tag = new LabelTag
                            {
                                Line = tag.Line,
                                PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                                State = LocalDensityCalculationState.DoneWithError
                            };
                            break;

                        case LocalDensityCalculationState.Done:
                            label.BackColor = Color.Beige;
                            label.ForeColor = Color.Black;
                            label.Font = new Font(
                              familyName: "Microsoft Sans Serif",
                              emSize: 9.75F,
                              style: FontStyle.Regular,
                              unit: GraphicsUnit.Point,
                              gdiCharSet: (byte)238);
                            label.Tag = new LabelTag
                            {
                                Line = tag.Line,
                                PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                                State = LocalDensityCalculationState.Done
                            };
                            break;

                        case LocalDensityCalculationState.EnabledHeader:
                            label.BackColor = Color.DarkBlue;
                            label.ForeColor = Color.White;
                            label.Font = new Font(
                                familyName: "Microsoft Sans Serif",
                                emSize: 9.75F,
                                style: FontStyle.Bold,
                                unit: GraphicsUnit.Point,
                                gdiCharSet: (byte)238);
                            label.Tag = new LabelTag
                            {
                                Line = tag.Line,
                                PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                                State = LocalDensityCalculationState.EnabledHeader
                            };
                            break;

                        case LocalDensityCalculationState.DisabledHeader:
                            label.BackColor = Color.LightBlue;
                            label.ForeColor = Color.Black;
                            label.Font = new Font(
                                familyName: "Microsoft Sans Serif",
                                emSize: 9.75F,
                                style: FontStyle.Bold,
                                unit: GraphicsUnit.Point,
                                gdiCharSet: (byte)238);
                            label.Tag = new LabelTag
                            {
                                Line = tag.Line,
                                PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                                State = LocalDensityCalculationState.DisabledHeader,
                            };
                            break;

                        default:
                            label.BackColor = Color.White;
                            label.ForeColor = Color.Black;
                            label.Font = new Font(
                              familyName: "Microsoft Sans Serif",
                              emSize: 9.75F,
                              style: FontStyle.Regular,
                              unit: GraphicsUnit.Point,
                              gdiCharSet: (byte)238);
                            label.Tag = new LabelTag
                            {
                                Line = tag.Line,
                                PanelReferenceYearSetId = tag.PanelReferenceYearSetId,
                                State = LocalDensityCalculationState.None
                            };
                            break;
                    }
                }
            };
        }

        /// <summary>
        /// <para lang="cs">Nastaví stav přípravy lokálních hustot páru panel a referenčního období</para>
        /// <para lang="en">Sets state of local density calculation for panel and reference year set</para>
        /// </summary>
        /// <param name="panelRefYearSetId">
        /// <para lang="cs">Identifikační číslo páru panel a referenčního období</para>
        /// <para lang="en">Panel reference year set identifier</para>
        /// </param>
        /// <param name="state">
        /// <para lang="cs">Stav přípravy lokálních hustot</para>
        /// <para lang="en">Local density calculation state</para>
        /// </param>
        /// <param name="message">
        /// <para lang="cs">Zpráva o přípavě lokálních hustot</para>
        /// <para lang="en">Message about local density calculation</para>
        /// </param>
        public void SetLocalDensityCalculationState(
            Nullable<int> panelRefYearSetId,
            LocalDensityCalculationState state,
            string message)
        {
            Label lblIdentifier =
                pnlIdentifier.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId == panelRefYearSetId)
                    .FirstOrDefault<Label>();

            Label lblPanel =
                pnlPanel.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId == panelRefYearSetId)
                    .FirstOrDefault<Label>();

            Label lblReferenceYearSet =
                 pnlReferenceYearSet.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId == panelRefYearSetId)
                    .FirstOrDefault<Label>();

            Label lblStatus =
                pnlStatus.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId == panelRefYearSetId)
                    .FirstOrDefault<Label>();

            SetLabelColor(
                label: lblIdentifier,
                state: state,
                reset: false);

            SetLabelColor(
                label: lblPanel,
                state: state,
                reset: false);

            SetLabelColor(
                label: lblReferenceYearSet,
                state: state,
                reset: false);

            SetLabelColor(
                label: lblStatus,
                state: state,
                reset: false);

            if (panelRefYearSetId != null)
            {
                SetLabelText(
                    label: lblStatus,
                    toolTip: toolTip,
                    text: message,
                    reset: false);
            }
        }

        /// <summary>
        /// <para lang="cs">Resetuje stav výpočtu lokálních hustot všech páru panel a referenčního období</para>
        /// <para lang="en">Resets state of local density calculation for all panel and reference year sets</para>
        /// </summary>
        public void ResetLocalDensityCalculationState()
        {
            foreach (Label lblIdentifier in
                pnlIdentifier.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId != null)
                    .Select(a => ((Label)a)))
            {
                SetLabelColor(
                    label: lblIdentifier,
                    state: LocalDensityCalculationState.Waiting,
                    reset: true);
            }

            foreach (Label lblPanel in
                pnlPanel.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId != null)
                    .Select(a => ((Label)a)))
            {
                SetLabelColor(
                    label: lblPanel,
                    state: LocalDensityCalculationState.Waiting,
                    reset: true);
            }

            foreach (Label lblReferenceYearSet in
                pnlReferenceYearSet.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId != null)
                    .Select(a => ((Label)a)))
            {
                SetLabelColor(
                    label: lblReferenceYearSet,
                    state: LocalDensityCalculationState.Waiting,
                    reset: true);
            }

            foreach (Label lblStatus in
                 pnlStatus.Controls.OfType<Label>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is LabelTag)
                    .Where(a => ((LabelTag)a.Tag).PanelReferenceYearSetId != null)
                    .Select(a => ((Label)a)))
            {
                SetLabelColor(
                    label: lblStatus,
                    state: LocalDensityCalculationState.Waiting,
                    reset: true);

                SetLabelText(
                    label: lblStatus,
                    toolTip: toolTip,
                    text: String.Empty,
                    reset: true);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Provede kontrolu systémových prostředů před spuštěním výpočtu
        /// </para>
        /// <para lang="en">
        /// Performs a check of system resources before running calculation
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Vrací true: je možné spustit výpočet,
        /// false: není možné spustit výpočet
        /// </para>
        /// <para lang="en">
        /// Returns true: it is possible to run the calculation,
        /// false: it is not possible to run the calculation
        /// </para>
        /// </returns>
        private bool CheckSystemResourcesAvailability()
        {
            FnSystemUtilizationJson systemUtilization =
                TDFunctions.FnSystemUtilization.Execute(database: Database);

            if (systemUtilization == null)
            {
                Setting.LocalDensityAvailableThreadsNumber =
                    Setting.LocalDensityRequiredThreadsNumber;

                MessageBox.Show(
                    text: msgFnSystemUtilizationIsNull,
                    caption: String.Empty,
                    icon: MessageBoxIcon.Information,
                    buttons: MessageBoxButtons.OK);
            }

            else if (systemUtilization.Load1Min == null)
            {
                Setting.LocalDensityAvailableThreadsNumber =
                    Setting.LocalDensityRequiredThreadsNumber;

                MessageBox.Show(
                    text: msgFnSystemUtilizationLoadMinIsNull,
                    caption: String.Empty,
                    icon: MessageBoxIcon.Information,
                    buttons: MessageBoxButtons.OK);
            }

            else if (systemUtilization.CPUCount == null)
            {
                Setting.LocalDensityAvailableThreadsNumber =
                    Setting.LocalDensityRequiredThreadsNumber;

                MessageBox.Show(
                    text: msgFnSystemUtilizationCPUCountIsNull,
                    caption: String.Empty,
                    icon: MessageBoxIcon.Information,
                    buttons: MessageBoxButtons.OK);
            }

            else
            {
                int limit = (int)((double)systemUtilization.CPUCount - (double)systemUtilization.Load1Min);

                if (limit < 1)
                {
                    limit = 1;

                    MessageBox.Show(
                        text: msgFnSystemUtilizationHighLoad,
                        caption: String.Empty,
                        icon: MessageBoxIcon.Information,
                        buttons: MessageBoxButtons.OK);
                }

                Setting.LocalDensityAvailableThreadsNumber =
                    (limit >= Setting.LocalDensityRequiredThreadsNumber)
                        ? Setting.LocalDensityRequiredThreadsNumber
                        : limit;
            }


            if (Setting.LocalDensityAvailableThreadsNumber < Setting.LocalDensityRequiredThreadsNumber)
            {
                string messageText =
                    msgCheckSystemResourcesAvailability
                        .Replace("$0", Environment.NewLine)
                        .Replace("$1", Setting.LocalDensityRequiredThreadsNumber.ToString())
                        .Replace("$2", Setting.LocalDensityAvailableThreadsNumber.ToString());

                return
                    DialogResult.OK ==
                        MessageBox.Show(
                            text: messageText,
                            caption: String.Empty,
                            icon: MessageBoxIcon.Information,
                            buttons: MessageBoxButtons.OKCancel);
            }

            else
            {
                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Spuštění výpočtu lokálních hustot paralelně ve více pracovních vláknech</para>
        /// <para lang="en">Start local density calculation in parallel in multiple worker threads</para>
        /// </summary>
        public void CalculateLocalDensities()
        {
            if (RefYearSetToPanelMappings == null)
            {
                return;
            }

            if (RefYearSetToPanelMappings.Items.Count == 0)
            {
                // Žádné panely pro výpočet lokálních hustot
                // No selected panels for calculation
                MessageBox.Show(
                    text: msgPanelsNoneSelected,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            StartThreads(panelReferenceYearSets: RefYearSetToPanelMappings.Items);
        }

        /// <summary>
        /// <para lang="cs">Spuštění vláken provádějících výpočet lokálních hustot</para>
        /// <para lang="en">Starts threads that executes local densities calculation</para>
        /// </summary>
        /// <param name="panelReferenceYearSets">
        /// <para lang="cs">Seznam kombinací panelů a referenčních období pro výpočet</para>
        /// <para lang="en">List of panel reference year set combination for calculation</para>
        /// </param>
        private void StartThreads(List<TFnGetRefYearSetToPanelMappingForGroup> panelReferenceYearSets)
        {
            if (!CheckSystemResourcesAvailability()) { return; }

            ResetLocalDensityCalculationState();

            ControlThread =
                new ControlThread<
                    FnApiSaveLDsityValuesWorker,
                    FnApiSaveLDsityValuesTask,
                    TFnGetRefYearSetToPanelMappingForGroup>();

            ThreadSafeQueue<FnApiSaveLDsityValuesTask> tasksToDo = new();
            tasksToDo.EnqueueRange(items: panelReferenceYearSets
                .Select(a => new FnApiSaveLDsityValuesTask(
                    refYearSetToPanelMapping: a,
                    targetVariable: SelectedTargetVariable,
                    localDensityThreshold: Setting.LocalDensityThreshold)));

            ControlThread.OnControlStart += new EventHandler(
               (sender, e) =>
               {
                   ThreadEventArgs args = (ThreadEventArgs)e;

                   BeginInvoke(new Action(() =>
                   {
                       CtrTargetData?.Disable();

                       SetLocalDensityCalculationState(
                           panelRefYearSetId: null,
                           state: LocalDensityCalculationState.DisabledHeader,
                           message: null);
                   }));
               });

            ControlThread.OnTaskStart += new EventHandler(
                (sender, e) =>
                {
                    ThreadEventArgs args = (ThreadEventArgs)e;

                    BeginInvoke(new Action(() =>
                    {
                        SetLocalDensityCalculationState(
                            panelRefYearSetId: ((FnApiSaveLDsityValuesTask)args.Task).Element.Id,
                            state: LocalDensityCalculationState.Active,
                            message: String.Empty);
                    }));
                });

            ControlThread.OnTaskException += new EventHandler(
                (sender, e) =>
                {
                    ThreadEventArgs args = (ThreadEventArgs)e;

                    BeginInvoke(new Action(() =>
                    {
                        SetLocalDensityCalculationState(
                            panelRefYearSetId: ((FnApiSaveLDsityValuesTask)args.Task).Element.Id,
                            state: LocalDensityCalculationState.DoneWithError,
                            message: args?.ExceptionEventArgs?.MessageBoxText);
                    }));
                });

            ControlThread.OnTaskComplete += new EventHandler(
                (sender, e) =>
                {
                    ThreadEventArgs args = (ThreadEventArgs)e;

                    BeginInvoke(new Action(() =>
                    {
                        SetLocalDensityCalculationState(
                            panelRefYearSetId: ((FnApiSaveLDsityValuesTask)args.Task).Element.Id,
                            state: LocalDensityCalculationState.Done,
                            message: String.Empty);
                    }));
                });

            ControlThread.OnControlComplete += new EventHandler(
                (sender, e) =>
                {
                    ThreadEventArgs args = (ThreadEventArgs)e;

                    BeginInvoke(new Action(() =>
                    {
                        CtrTargetData?.Enable();

                        SetLocalDensityCalculationState(
                            panelRefYearSetId: null,
                            state: LocalDensityCalculationState.EnabledHeader,
                            message: null);

                        if (ControlThread.Log.Any())
                        {
                            WriteErrorLogIntoFile(errorLog: ControlThread.Log);
                            FormErrorLog form = new(controlOwner: this)
                            {
                                ErrorLog = ControlThread.Log
                            };
                            if (form.ShowDialog() == DialogResult.OK) { };
                        }
                    }));
                });

            ((ControlThread<
                FnApiSaveLDsityValuesWorker,
                FnApiSaveLDsityValuesTask,
                TFnGetRefYearSetToPanelMappingForGroup>)
                ControlThread).Start(
                workersCount: Setting.LocalDensityAvailableThreadsNumber,
                tasksToDo: tasksToDo,
                connection: Database.Postgres);
        }

        /// <summary>
        /// <para lang="cs">Provede zápis logu do textového souboru</para>
        /// <para lang="en">Writes the log to a text file</para>
        /// </summary>
        /// <param name="errorLog">
        /// <para lang="cs">Seznam chybových zpráv z pracovních vláken</para>
        /// <para lang="en">List of error messages from worker threads</para>
        /// </param>
        private void WriteErrorLogIntoFile(
            ThreadSafeList<string> errorLog)
        {
            string folderName = @"log";
            string fileName = String.Concat(folderName, @"\module_target_data_log.txt");
            System.IO.StreamWriter sw = null;

            try
            {
                // Pokud neexistuje adresář, pak je vytvořen
                if (!System.IO.Directory.Exists(path: folderName))
                {
                    System.IO.Directory.CreateDirectory(path: folderName);
                }

                // Vytvoří StreamWriter
                if (System.IO.File.Exists(path: fileName))
                {
                    if (CtrTargetData.ErrorLogEntryNumber == 0)
                    {
                        // Pokud při daném spuštění aplikace ještě žádná chyba nebyla zaznamenána
                        // přepíše staré záznamy z předchozího spuštění
                        sw = new System.IO.StreamWriter(path: fileName, append: false);
                    }
                    else
                    {
                        // Pokud při daném spuštění aplikace již chyby zaznamenány byly
                        // přidá nové záznamy
                        sw = new System.IO.StreamWriter(path: fileName, append: true);
                    }
                }
                else
                {
                    sw = System.IO.File.CreateText(path: fileName);
                }

                // Zápis řádků do souboru
                sw.WriteLine(value: DateTime.Now.ToString());
                foreach (string line in errorLog.Items)
                {
                    sw.WriteLine(value: line);
                }
            }
            finally
            {
                CtrTargetData.ErrorLogEntryNumber++;
                sw?.Close();
            }
        }

        #endregion Methods

    }

    /// <summary>
    /// <para lang="cs">Stav přípravy lokálních hustot</para>
    /// <para lang="en">Local density calculation state</para>
    /// </summary>
    internal enum LocalDensityCalculationState
    {
        /// <summary>
        /// <para lang="cs">Žádný</para>
        /// <para lang="en">None</para>
        /// </summary>
        None = 0,

        /// <summary>
        /// <para lang="cs">Čeká na přípravu lokálních hustot</para>
        /// <para lang="en">Waiting for local density calculation</para>
        /// </summary>
        Waiting = 1,

        /// <summary>
        /// <para lang="cs">Probíhá přípava lokálních hustot</para>
        /// <para lang="en">Local density calculation in process</para>
        /// </summary>
        Active = 2,

        /// <summary>
        /// <para lang="cs">Přípava lokálních hustot skončila s chybou</para>
        /// <para lang="en">Local density calculation done with errors</para>
        /// </summary>
        DoneWithError = 3,

        /// <summary>
        /// <para lang="cs">Přípava lokálních hustot dokončena</para>
        /// <para lang="en">Local density calculation done</para>
        /// </summary>
        Done = 4,

        /// <summary>
        /// <para lang="cs">Povolený nadpis</para>
        /// <para lang="en">Enabled header</para>
        /// </summary>
        EnabledHeader = 5,

        /// <summary>
        /// <para lang="cs">Nepovolený nadpis</para>
        /// <para lang="en">Disabled header</para>
        /// </summary>
        DisabledHeader = 6,

    }

}