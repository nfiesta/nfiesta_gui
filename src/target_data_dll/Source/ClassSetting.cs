﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Drawing;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data</para>
        /// <para lang="en">Module for target data setting</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class Setting
        {

            #region Constants

            #region General

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Default value - Database server connection parameters</para>
            /// </summary>
            private static readonly PostgreSQL.Setting DefaultDBSetting
                = new()
                {
                    ApplicationName = "NfiEstaGUI: Module Target Data",
                    LanguageVersion = LanguageVersion.International,
                    Host = "localhost",
                    Hosts = ["bran-pga", "bran-pga-dev", "localhost"],
                    Port = 5432,
                    Ports = [5432, 5433],
                    Database = "nfi_analytical",
                    Databases = ["nfi_analytical", "contrib_regression_target_data_user"],
                    UserName = "vagrant",
                    UserNames = ["vagrant"],
                    CommandTimeout = 0,         // SQL příkazy mají neomezenou dobu na vykonání
                    KeepAlive = 180,            // keep alive je posíláno 1x za 3 minuty
                    TcpKeepAlive = true,        // tcp keep alive zapnuto
                    TcpKeepAliveTime = 60,      // tcp keep alive je posíláno 1x za 1 minutu
                    TcpKeepAliveInterval = 5
                };

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Požadovaný počet vláken pro výpočet lokálních hustot</para>
            /// <para lang="en">Default value - Required number of threads to calculate local densities</para>
            /// </summary>
            public const int DefaultLocalDensityRequiredThreadsNumber = 10;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Dostupný počet vláken pro výpočet lokálních hustot</para>
            /// <para lang="en">Default value - Available number of threads to calculate local densities</para>
            /// </summary>
            private const int DefaultLocalDensityAvailableThreadsNumber = 10;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Mezní hodnota pro výpočet lokálních hustot</para>
            /// <para lang="en">Default value - Local density threshold</para>
            /// </summary>
            public const double DefaultLocalDensityThreshold = 0.00000001;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazit stavový řádek</para>
            /// <para lang="en">Default value - Display status strip</para>
            /// </summary>
            private const bool DefaultStatusStripVisible = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Default value - Display executed SQL commands when debugging module</para>
            /// </summary>
            private const bool DefaultVerbose = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Default value - Suppressing the display of error message from SQL statement</para>
            /// </summary>
            private const bool DefaultDBSuppressError = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Default value - Suppressing the display of messages from SQL statement</para>
            /// </summary>
            private const bool DefaultDBSuppressWarning = true;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Umístění složky pro export a import souborů klasifikačních providel plošné domény</para>
            /// <para lang="en">Default value - Folder for export and import files of area domain classification rules</para>
            /// </summary>
            private static readonly string DefaultAreaDomainJSONFolder =
                Environment.GetFolderPath(folder: Environment.SpecialFolder.MyDocuments);

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Umístění složky pro export a import souborů klasifikačních providel subpopulace</para>
            /// <para lang="en">Default value - Folder for export and import files of subpopulation classification rules</para>
            /// </summary>
            private static readonly string DefaultSubPopulationJSONFolder =
               Environment.GetFolderPath(folder: Environment.SpecialFolder.MyDocuments);

            #endregion General


            #region ControlPanelRefYearSetList

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Výška řádku v seznamu panelů a roků měření</para>
            /// <para lang="en">Default value - Row height in list of panels and reference year set</para>
            /// </summary>
            private const int DefaultPanelRefYearSetListRowHeight = 20;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Výška hlavičky v seznamu panelů a roků měření</para>
            /// <para lang="en">Default value - Header height in list of panels and reference year set</para>
            /// </summary>
            private const int DefaultPanelRefYearSetListHeaderHeight = 20;


            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Šířka sloupce "identifikátor" v seznamu panelů a roků měření</para>
            /// <para lang="en">Default value - Column width "identifier" in the list of panels and ref year set</para>
            /// </summary>
            private const int DefaultColPanelRefYearSetListIdentifierWidth = 50;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Šířka sloupce "panel" v seznamu panelů a roků měření</para>
            /// <para lang="en">Default value - Column width "panel" in the list of panels and ref year set</para>
            /// </summary>
            private const int DefaultColPanelWidth = 300;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Šířka sloupce "rok měření" v seznamu panelů a roků měření</para>
            /// <para lang="en">Default value - Column width "reference year set" in the list of panels and ref year set</para>
            /// </summary>
            private const int DefaultColReferenceYearSetWidth = 200;


            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva hlavičky v seznamu panelů a roků měření</para>
            /// <para lang="en">Default value - Color for header in the list of panels and ref year set</para>
            /// </summary>
            private static readonly Color DefaultBackColorOfColPanelRefYearSetListHeader = Color.DarkBlue;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Default value - Font color for panel without calculated local densities</para>
            /// </summary>
            private static readonly Color DefaultForeColorOfColPanelRefYearSetListHeader = Color.White;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Styl písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Default value - Font style for panel without calculated local densities</para>
            /// </summary>
            private static readonly Font DefaultFontStyleOfColPanelRefYearSetListHeader =
                new(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9.75F,
                    style: FontStyle.Bold,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: (byte)238);


            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Default value - Color for panel without calculated local densities</para>
            /// </summary>
            private static readonly Color DefaultBackColorOfWaitingPanel = Color.White;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Default value - Font color for panel without calculated local densities</para>
            /// </summary>
            private static readonly Color DefaultForeColorOfWaitingPanel = Color.Black;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Styl písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Default value - Font style for panel without calculated local densities</para>
            /// </summary>
            private static readonly Font DefaultFontStyleOfWaitingPanel =
                new(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9.75F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: (byte)238);


            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Default value - Color for panel with ongoing local density calculation</para>
            /// </summary>
            private static readonly Color DefaultBackColorForActivePanel = Color.LightBlue;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva písma pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Default value - Font color for panel with ongoing local density calculation</para>
            /// </summary>
            private static readonly Color DefaultForeColorForActivePanel = Color.Black;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Styl písma pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Default value - Font style for panel with ongoing local density calculation</para>
            /// </summary>
            private static readonly Font DefaultFontStyleForActivePanel =
                 new(
                     familyName: "Microsoft Sans Serif",
                     emSize: 9.75F,
                     style: FontStyle.Regular,
                     unit: GraphicsUnit.Point,
                     gdiCharSet: (byte)238);


            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Default value - Color for panel with local density calculation error</para>
            /// </summary>
            private static readonly Color DefaultBackColorForDoneWithErrorPanel = Color.Bisque;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva písma pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Default value - Font color for panel with local density calculation error</para>
            /// </summary>
            private static readonly Color DefaultForeColorForDoneWithErrorPanel = Color.Black;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Styl písma pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Default value - Font style for panel with local density calculation error</para>
            /// </summary>
            private static readonly Font DefaultFontStyleForDoneWithErrorPanel =
                  new(
                      familyName: "Microsoft Sans Serif",
                      emSize: 9.75F,
                      style: FontStyle.Regular,
                      unit: GraphicsUnit.Point,
                      gdiCharSet: (byte)238);


            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Default value - Color for panel with calculated local densities</para>
            /// </summary>
            private static readonly Color DefaultBackColorForDonePanel = Color.Beige;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Barva písma pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Default value - Font color for panel with calculated local densities</para>
            /// </summary>
            private static readonly Color DefaultForeColorForDonePanel = Color.Black;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Styl písma pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Default value - Font style for panel with calculated local densities</para>
            /// </summary>
            private static readonly Font DefaultFontStyleForDonePanel =
                  new(
                      familyName: "Microsoft Sans Serif",
                      emSize: 9.75F,
                      style: FontStyle.Regular,
                      unit: GraphicsUnit.Point,
                      gdiCharSet: (byte)238);

            #endregion ControlPanelRefYearSetList


            #endregion Constants


            #region Private Fields

            #region General

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            private PostgreSQL.Setting dbSetting;

            /// <summary>
            /// <para lang="cs">Požadovaný počet vláken pro výpočet lokálních hustot</para>
            /// <para lang="en">Required number of threads to calculate local densities</para>
            /// </summary>
            private Nullable<int> localDensityRequiredThreadsNumber;

            /// <summary>
            /// <para lang="cs">Dostupný počet vláken pro výpočet lokálních hustot</para>
            /// <para lang="en">Available number of threads to calculate local densities</para>
            /// </summary>
            private Nullable<int> localDensityAvailableThreadsNumber;

            /// <summary>
            /// <para lang="cs">Mezní hodnota pro výpočet lokálních hustot</para>
            /// <para lang="en">Local density threshold</para>
            /// </summary>
            public Nullable<double> localDensityThreshold;

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            private Nullable<bool> statusStripVisible;

            /// <summary>
            /// <para lang="cs">Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Display executed SQL commands when debugging module</para>
            /// </summary>
            private Nullable<bool> verbose;

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of error message from SQL statement</para>
            /// </summary>
            private Nullable<bool> dbSuppressError;

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of messages from SQL statement</para>
            /// </summary>
            private Nullable<bool> dbSuppressWarning;

            /// <summary>
            /// <para lang="cs">Umístění složky pro export a import souborů klasifikačních providel plošné domény</para>
            /// <para lang="en">Folder for export and import files of area domain classification rules</para>
            /// </summary>
            private string areaDomainJSONFolder;

            /// <summary>
            /// <para lang="cs">Umístění složky pro export a import souborů klasifikačních providel subpopulace</para>
            /// <para lang="en">Folder for export and import files of subpopulation classification rules</para>
            /// </summary>
            private string subPopulationJSONFolder;

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            private JsonSerializerOptions serializerOptions;

            #endregion General

            #region ControlPanelRefYearSetList

            /// <summary>
            /// <para lang="cs">Výška řádku v seznamu panelů a roků měření</para>
            /// <para lang="en">Row height in list of panels and reference year set</para>
            /// </summary>
            private Nullable<int> panelRefYearSetListRowHeight;

            /// <summary>
            /// <para lang="cs">Výška hlavičky v seznamu panelů a roků měření</para>
            /// <para lang="en">Header height in list of panels and reference year set</para>
            /// </summary>
            private Nullable<int> panelRefYearSetListHeaderHeight;


            /// <summary>
            /// <para lang="cs">Šířka sloupce "identifikátor" v seznamu panelů a roků měření</para>
            /// <para lang="en">Column width "identifier" in the list of panels and ref year set</para>
            /// </summary>
            private Nullable<int> colPanelRefYearSetListIdentifierWidth;

            /// <summary>
            /// <para lang="cs">Šířka sloupce "panel" v seznamu panelů a roků měření</para>
            /// <para lang="en">Column width "panel" in the list of panels and ref year set</para>
            /// </summary>
            private Nullable<int> colPanelWidth;

            /// <summary>
            /// <para lang="cs">Šířka sloupce "rok měření" v seznamu panelů a roků měření</para>
            /// <para lang="en">Column width "reference year set" in the list of panels and ref year set</para>
            /// </summary>
            private Nullable<int> colReferenceYearSetWidth;


            /// <summary>
            /// <para lang="cs">Barva hlavičky v seznamu panelů a roků měření</para>
            /// <para lang="en">Color for header in the list of panels and ref year set</para>
            /// </summary>
            private Color backColorOfColPanelRefYearSetListHeader;

            /// <summary>
            /// <para lang="cs">Barva písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font color for panel without calculated local densities</para>
            /// </summary>
            private Color foreColorOfColPanelRefYearSetListHeader;

            /// <summary>
            /// <para lang="cs">Styl písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font style for panel without calculated local densities</para>
            /// </summary>
            private Font fontStyleOfColPanelRefYearSetListHeader;


            /// <summary>
            /// <para lang="cs">Barva pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Color for panel without calculated local densities</para>
            /// </summary>
            private Color backColorOfWaitingPanel;

            /// <summary>
            /// <para lang="cs">Barva písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font color for panel without calculated local densities</para>
            /// </summary>
            private Color foreColorOfWaitingPanel;

            /// <summary>
            /// <para lang="cs">Styl písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font style for panel without calculated local densities</para>
            /// </summary>
            private Font fontStyleOfWaitingPanel;


            /// <summary>
            /// <para lang="cs">Barva pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Color for panel with ongoing local density calculation</para>
            /// </summary>
            private Color backColorForActivePanel;

            /// <summary>
            /// <para lang="cs">Barva písma pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Font color for panel with ongoing local density calculation</para>
            /// </summary>
            private Color foreColorForActivePanel;

            /// <summary>
            /// <para lang="cs">Styl písma pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Font style for panel with ongoing local density calculation</para>
            /// </summary>
            private Font fontStyleForActivePanel;


            /// <summary>
            /// <para lang="cs">Barva pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Color for panel with local density calculation error</para>
            /// </summary>
            private Color backColorForDoneWithErrorPanel;

            /// <summary>
            /// <para lang="cs">Barva písma pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Font color for panel with local density calculation error</para>
            /// </summary>
            private Color foreColorForDoneWithErrorPanel;

            /// <summary>
            /// <para lang="cs">Styl písma pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Font style for panel with local density calculation error</para>
            /// </summary>
            private Font fontStyleForDoneWithErrorPanel;


            /// <summary>
            /// <para lang="cs">Barva pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Color for panel with calculated local densities</para>
            /// </summary>
            private Color backColorForDonePanel;

            /// <summary>
            /// <para lang="cs">Barva písma pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Font color for panel with calculated local densities</para>
            /// </summary>
            private Color foreColorForDonePanel;

            /// <summary>
            /// <para lang="cs">Styl písma pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Font style for panel with calculated local densities</para>
            /// </summary>
            private Font fontStyleForDonePanel;

            #endregion ControlPanelRefYearSetList

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu</para>
            /// <para lang="en">Object constructor</para>
            /// </summary>
            public Setting()
            {

                #region General

                SerializerOptions = null;
                DBSetting = DefaultDBSetting;
                LocalDensityRequiredThreadsNumber = DefaultLocalDensityRequiredThreadsNumber;
                LocalDensityAvailableThreadsNumber = DefaultLocalDensityAvailableThreadsNumber;
                LocalDensityThreshold = DefaultLocalDensityThreshold;
                StatusStripVisible = DefaultStatusStripVisible;
                Verbose = DefaultVerbose;
                DBSuppressError = DefaultDBSuppressError;
                DBSuppressWarning = DefaultDBSuppressWarning;
                AreaDomainJSONFolder = DefaultAreaDomainJSONFolder;
                SubPopulationJSONFolder = DefaultSubPopulationJSONFolder;

                #endregion General

                #region ControlPanelRefYearSetList

                PanelRefYearSetListRowHeight = DefaultPanelRefYearSetListRowHeight;
                PanelRefYearSetListHeaderHeight = DefaultPanelRefYearSetListHeaderHeight;

                ColPanelRefYearSetListIdentifierWidth = DefaultColPanelRefYearSetListIdentifierWidth;
                ColPanelWidth = DefaultColPanelWidth;
                ColReferenceYearSetWidth = DefaultColReferenceYearSetWidth;

                BackColorOfColPanelRefYearSetListHeader = DefaultBackColorOfColPanelRefYearSetListHeader;
                ForeColorOfColPanelRefYearSetListHeader = DefaultForeColorOfColPanelRefYearSetListHeader;
                FontStyleOfColPanelRefYearSetListHeader = DefaultFontStyleOfColPanelRefYearSetListHeader;

                BackColorOfWaitingPanel = DefaultBackColorOfWaitingPanel;
                ForeColorOfWaitingPanel = DefaultForeColorOfWaitingPanel;
                FontStyleOfWaitingPanel = DefaultFontStyleOfWaitingPanel;

                BackColorForActivePanel = DefaultBackColorForActivePanel;
                ForeColorForActivePanel = DefaultForeColorForActivePanel;
                FontStyleForActivePanel = DefaultFontStyleForActivePanel;

                BackColorForDoneWithErrorPanel = DefaultBackColorForDoneWithErrorPanel;
                ForeColorForDoneWithErrorPanel = DefaultForeColorForDoneWithErrorPanel;
                FontStyleForDoneWithErrorPanel = DefaultFontStyleForDoneWithErrorPanel;

                BackColorForDonePanel = DefaultBackColorForDonePanel;
                ForeColorForDonePanel = DefaultForeColorForDonePanel;
                FontStyleForDonePanel = DefaultFontStyleForDonePanel;

                #endregion ControlPanelRefYearSetList

            }

            #endregion Constructor


            #region Properties

            #region General

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            public PostgreSQL.Setting DBSetting
            {
                get
                {
                    return dbSetting ?? DefaultDBSetting;
                }
                set
                {
                    dbSetting = value ?? DefaultDBSetting;
                }
            }

            /// <summary>
            /// <para lang="cs">Požadovaný počet vláken pro výpočet lokálních hustot</para>
            /// <para lang="en">Required number of threads to calculate local densities</para>
            /// </summary>
            public int LocalDensityRequiredThreadsNumber
            {
                get
                {
                    return localDensityRequiredThreadsNumber ?? DefaultLocalDensityRequiredThreadsNumber;
                }
                set
                {
                    localDensityRequiredThreadsNumber = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Dostupný počet vláken pro výpočet lokálních hustot</para>
            /// <para lang="en">Available number of threads to calculate local densities</para>
            /// </summary>
            public int LocalDensityAvailableThreadsNumber
            {
                get
                {
                    return localDensityAvailableThreadsNumber ?? DefaultLocalDensityAvailableThreadsNumber;
                }
                set
                {
                    localDensityAvailableThreadsNumber = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Mezní hodnota pro výpočet lokálních hustot</para>
            /// <para lang="en">Local density threshold</para>
            /// </summary>
            public double LocalDensityThreshold
            {
                get
                {
                    return localDensityThreshold ?? DefaultLocalDensityThreshold;
                }
                set
                {
                    localDensityThreshold = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            public bool StatusStripVisible
            {
                get
                {
                    return statusStripVisible ?? DefaultStatusStripVisible;
                }
                set
                {
                    statusStripVisible = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Display executed SQL commands when debugging module</para>
            /// </summary>
            public bool Verbose
            {
                get
                {
                    return verbose ?? DefaultVerbose;
                }
                set
                {
                    verbose = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of error message from SQL statement</para>
            /// </summary>
            public bool DBSuppressError
            {
                get
                {
                    return dbSuppressError ?? DefaultDBSuppressError;
                }
                set
                {
                    dbSuppressError = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of messages from SQL statement</para>
            /// </summary>
            public bool DBSuppressWarning
            {
                get
                {
                    return dbSuppressWarning ?? DefaultDBSuppressWarning;
                }
                set
                {
                    dbSuppressWarning = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Umístění složky pro export a import souborů klasifikačních providel plošné domény</para>
            /// <para lang="en">Folder for export and import files of area domain classification rules</para>
            /// </summary>
            public string AreaDomainJSONFolder
            {
                get
                {

                    if (String.IsNullOrEmpty(value: areaDomainJSONFolder))
                    {
                        return DefaultAreaDomainJSONFolder;
                    }

                    if (!Directory.Exists(path: areaDomainJSONFolder))
                    {
                        return DefaultAreaDomainJSONFolder;
                    }

                    return areaDomainJSONFolder;
                }
                set
                {
                    if (String.IsNullOrEmpty(value: value))
                    {
                        areaDomainJSONFolder = DefaultAreaDomainJSONFolder;
                    }

                    if (!Directory.Exists(path: value))
                    {
                        areaDomainJSONFolder = DefaultAreaDomainJSONFolder;
                    }

                    areaDomainJSONFolder = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Umístění složky pro export a import souborů klasifikačních providel subpopulace</para>
            /// <para lang="en">Folder for export and import files of subpopulation classification rules</para>
            /// </summary>
            public string SubPopulationJSONFolder
            {
                get
                {

                    if (String.IsNullOrEmpty(value: subPopulationJSONFolder))
                    {
                        return DefaultSubPopulationJSONFolder;
                    }

                    if (!Directory.Exists(path: subPopulationJSONFolder))
                    {
                        return DefaultSubPopulationJSONFolder;
                    }

                    return subPopulationJSONFolder;
                }
                set
                {
                    if (String.IsNullOrEmpty(value: value))
                    {
                        subPopulationJSONFolder = DefaultSubPopulationJSONFolder;
                    }

                    if (!Directory.Exists(path: value))
                    {
                        subPopulationJSONFolder = DefaultSubPopulationJSONFolder;
                    }

                    subPopulationJSONFolder = value;
                }
            }

            #endregion General


            #region ControlPanelRefYearSetList

            /// <summary>
            /// <para lang="cs">Výška řádku v seznamu panelů a roků měření</para>
            /// <para lang="en">Row height in list of panels and reference year set</para>
            /// </summary>
            public int PanelRefYearSetListRowHeight
            {
                get
                {
                    return panelRefYearSetListRowHeight ?? DefaultPanelRefYearSetListRowHeight;
                }
                set
                {
                    panelRefYearSetListRowHeight = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Výška hlavičky v seznamu panelů a roků měření</para>
            /// <para lang="en">Header height in list of panels and reference year set</para>
            /// </summary>
            public int PanelRefYearSetListHeaderHeight
            {
                get
                {
                    return panelRefYearSetListHeaderHeight ?? DefaultPanelRefYearSetListHeaderHeight;
                }
                set
                {
                    panelRefYearSetListHeaderHeight = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Šířka sloupce "identifikátor" v seznamu panelů a roků měření</para>
            /// <para lang="en">Column width "identifier" in the list of panels and ref year set</para>
            /// </summary>
            public int ColPanelRefYearSetListIdentifierWidth
            {
                get
                {
                    return colPanelRefYearSetListIdentifierWidth ?? DefaultColPanelRefYearSetListIdentifierWidth;
                }
                set
                {
                    colPanelRefYearSetListIdentifierWidth = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Šířka sloupce "panel" v seznamu panelů a roků měření</para>
            /// <para lang="en">Column width "panel" in the list of panels and ref year set</para>
            /// </summary>
            public int ColPanelWidth
            {
                get
                {
                    return colPanelWidth ?? DefaultColPanelWidth;
                }
                set
                {
                    colPanelWidth = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Šířka sloupce "rok měření" v seznamu panelů a roků měření</para>
            /// <para lang="en">Column width "reference year set" in the list of panels and ref year set</para>
            /// </summary>
            public int ColReferenceYearSetWidth
            {
                get
                {
                    return colReferenceYearSetWidth ?? DefaultColReferenceYearSetWidth;
                }
                set
                {
                    colReferenceYearSetWidth = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Barva hlavičky v seznamu panelů a roků měření</para>
            /// <para lang="en">Color for header in the list of panels and ref year set</para>
            /// </summary>
            public Color BackColorOfColPanelRefYearSetListHeader
            {
                get
                {
                    return backColorOfColPanelRefYearSetListHeader;
                }
                set
                {
                    backColorOfColPanelRefYearSetListHeader = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Barva písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font color for panel without calculated local densities</para>
            /// </summary>
            public Color ForeColorOfColPanelRefYearSetListHeader
            {
                get
                {
                    return foreColorOfColPanelRefYearSetListHeader;
                }
                set
                {
                    foreColorOfColPanelRefYearSetListHeader = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Styl písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font style for panel without calculated local densities</para>
            /// </summary>
            [JsonIgnore]
            public Font FontStyleOfColPanelRefYearSetListHeader
            {
                get
                {
                    return fontStyleOfColPanelRefYearSetListHeader;
                }
                set
                {
                    fontStyleOfColPanelRefYearSetListHeader = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Barva pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Color for panel without calculated local densities</para>
            /// </summary>
            public Color BackColorOfWaitingPanel
            {
                get
                {
                    return backColorOfWaitingPanel;
                }
                set
                {
                    backColorOfWaitingPanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Barva písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font color for panel without calculated local densities</para>
            /// </summary>
            public Color ForeColorOfWaitingPanel
            {
                get
                {
                    return foreColorOfWaitingPanel;
                }
                set
                {
                    foreColorOfWaitingPanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Styl písma pro panel bez vypočtných lokálních hustot</para>
            /// <para lang="en">Font style for panel without calculated local densities</para>
            /// </summary>
            [JsonIgnore]
            public Font FontStyleOfWaitingPanel
            {
                get
                {
                    return fontStyleOfWaitingPanel;
                }
                set
                {
                    fontStyleOfWaitingPanel = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Barva pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Color for panel with ongoing local density calculation</para>
            /// </summary>
            public Color BackColorForActivePanel
            {
                get
                {
                    return backColorForActivePanel;
                }
                set
                {
                    backColorForActivePanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Barva písma pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Font color for panel with ongoing local density calculation</para>
            /// </summary>
            public Color ForeColorForActivePanel
            {
                get
                {
                    return foreColorForActivePanel;
                }
                set
                {
                    foreColorForActivePanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Styl písma pro panel s probíhajícím výpočtem lokálních hustot</para>
            /// <para lang="en">Font style for panel with ongoing local density calculation</para>
            /// </summary>
            [JsonIgnore]
            public Font FontStyleForActivePanel
            {
                get
                {
                    return fontStyleForActivePanel;
                }
                set
                {
                    fontStyleForActivePanel = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Barva pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Color for panel with local density calculation error</para>
            /// </summary>
            public Color BackColorForDoneWithErrorPanel
            {
                get
                {
                    return backColorForDoneWithErrorPanel;
                }
                set
                {
                    backColorForDoneWithErrorPanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Barva písma pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Font color for panel with local density calculation error</para>
            /// </summary>
            public Color ForeColorForDoneWithErrorPanel
            {
                get
                {
                    return foreColorForDoneWithErrorPanel;
                }
                set
                {
                    foreColorForDoneWithErrorPanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Styl písma pro panel s chybou výpočtu lokálních hustot</para>
            /// <para lang="en">Font style for panel with local density calculation error</para>
            /// </summary>
            [JsonIgnore]
            public Font FontStyleForDoneWithErrorPanel
            {
                get
                {
                    return fontStyleForDoneWithErrorPanel;
                }
                set
                {
                    fontStyleForDoneWithErrorPanel = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Barva pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Color for panel with calculated local densities</para>
            /// </summary>
            public Color BackColorForDonePanel
            {
                get
                {
                    return backColorForDonePanel;
                }
                set
                {
                    backColorForDonePanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Barva písma pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Font color for panel with calculated local densities</para>
            /// </summary>
            public Color ForeColorForDonePanel
            {
                get
                {
                    return foreColorForDonePanel;
                }
                set
                {
                    foreColorForDonePanel = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Styl písma pro panel s vypočtenými lokálními hustotami</para>
            /// <para lang="en">Font style for panel with calculated local densities</para>
            /// </summary>
            [JsonIgnore]
            public Font FontStyleForDonePanel
            {
                get
                {
                    return fontStyleForDonePanel;
                }
                set
                {
                    fontStyleForDonePanel = value;
                }
            }

            #endregion ControlPanelRefYearSetList


            #region JSON

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            [JsonIgnore]
            private JsonSerializerOptions SerializerOptions
            {
                get
                {
                    return serializerOptions ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
                set
                {
                    serializerOptions = value ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
            }

            /// <summary>
            /// <para lang="cs">Serializace objektu do formátu JSON</para>
            /// <para lang="en">Object serialization to JSON format</para>
            /// </summary>
            [JsonIgnore]
            public string JSON
            {
                get
                {
                    return JsonSerializer.Serialize(
                        value: this,
                        options: SerializerOptions);
                }
            }

            #endregion JSON

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Obnovení objektu z formátu JSON</para>
            /// <para lang="en">Restoring an object from JSON format</para>
            /// </summary>
            /// <param name="json">
            /// <para lang="cs">JSON text</para>
            /// <para lang="en">JSON text</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Nastavení modulu</para>
            /// <para lang="en">Module setting</para>
            /// </returns>
            public static Setting Deserialize(string json)
            {
                try
                {
                    return
                        JsonSerializer.Deserialize<Setting>(json: json);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Exclamation);

                    return new Setting();
                }
            }

            /// <summary>
            /// <para lang="cs">Načte objekt nastavení ze souboru json</para>
            /// <para lang="en">Loads setting object from json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací načtený objekt nastavení</para>
            /// <para lang="en">Return loaded setting object</para>
            /// </returns>
            public static Setting LoadFromFile(string path)
            {
                try
                {
                    return Deserialize(
                        json: File.ReadAllText(
                            path: path,
                            encoding: Encoding.UTF8));
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return new Setting();
                }
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Uloží objekt nastavení do souboru json</para>
            /// <para lang="en">Save setting object into json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            public void SaveToFile(string path)
            {
                try
                {
                    File.WriteAllText(
                        path: path,
                        contents: JSON,
                        encoding: Encoding.UTF8);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            #endregion Methods


            #region Style

            #region Button

            /// <summary>
            /// <para lang="cs">Styl písma pro tlačítko</para>
            /// <para lang="en">Font style for button</para>
            /// </summary>
            public static readonly Font ButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro tlačítko</para>
            /// <para lang="en">Font color for button</para>
            /// </summary>
            public static readonly Color ButtonForeColor =
                Color.Black;

            #endregion Button


            #region ComboBox

            /// <summary>
            /// <para lang="cs">Styl písma pro rozbalovací seznam</para>
            /// <para lang="en">Font style for combo box</para>
            /// </summary>
            public static readonly Font ComboBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro rozbalovací seznam</para>
            /// <para lang="en">Font color for combo box</para>
            /// </summary>
            public static readonly Color ComboBoxForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro rozbalovací seznam</para>
            /// <para lang="en">Back color for combo box</para>
            /// </summary>
            public static readonly Color ComboBoxBackColor =
                Color.White;

            #endregion ComboBox


            #region CheckBox

            /// <summary>
            /// <para lang="cs">Styl písma pro zaškrtávací políčko</para>
            /// <para lang="en">Font style for check box</para>
            /// </summary>
            public static readonly Font CheckBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro zaškrtávací políčko</para>
            /// <para lang="en">Font color for check box</para>
            /// </summary>
            public static readonly Color CheckBoxForeColor =
                Color.Black;

            #endregion CheckBox


            #region CheckedListBox

            /// <summary>
            /// <para lang="cs">Styl písma pro zaškrtávací seznam</para>
            /// <para lang="en">Font style for checked list box</para>
            /// </summary>
            public static readonly Font CheckedListBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro zaškrtávací seznam</para>
            /// <para lang="en">Font color for checked list box</para>
            /// </summary>
            public static readonly Color CheckedListBoxForeColor =
                Color.Black;

            #endregion CheckedListBox


            #region Form

            /// <summary>
            /// <para lang="cs">Styl písma pro formulář</para>
            /// <para lang="en">Font style for form</para>
            /// </summary>
            public static readonly Font FormFont = new(
                    familyName: "Segoe UI",
                    emSize: 9F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro formulář</para>
            /// <para lang="en">Font color for form</para>
            /// </summary>
            public static readonly Color FormForeColor =
                Color.Black;

            #endregion Form


            #region GroupBox

            /// <summary>
            /// <para lang="cs">Styl písma pro skupinu</para>
            /// <para lang="en">Font style for group box</para>
            /// </summary>
            public static readonly Font GroupBoxFont = new(
                    familyName: "Segoe UI",
                    emSize: 11.25F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro skupinu</para>
            /// <para lang="en">Font color for group box</para>
            /// </summary>
            public static readonly Color GroupBoxForeColor =
                Color.MediumBlue;

            #endregion GroupBox


            #region Label

            /// <summary>
            /// <para lang="cs">Styl písma pro popisek</para>
            /// <para lang="en">Font style for label</para>
            /// </summary>
            public static readonly Font LabelFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek</para>
            /// <para lang="en">Font color for label</para>
            /// </summary>
            public static readonly Color LabelForeColor =
                Color.MediumBlue;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s hodnotou</para>
            /// <para lang="en">Font style for label with value</para>
            /// </summary>
            public static readonly Font LabelValueFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s hodnotou</para>
            /// <para lang="en">Font color for label with value</para>
            /// </summary>
            public static readonly Color LabelValueForeColor =
                Color.Black;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s chybovou hláškou</para>
            /// <para lang="en">Font style for label with error message</para>
            /// </summary>
            public static readonly Font LabelErrorFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s chybovou hláškou</para>
            /// <para lang="en">Font color for label with error message</para>
            /// </summary>
            public static readonly Color LabelErrorForeColor =
                Color.Red;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s nadpisem</para>
            /// <para lang="en">Font style for label with main caption</para>
            /// </summary>
            public static readonly Font LabelMainFont = new(
                   familyName: "Segoe UI",
                   emSize: 11.25F,
                   style: FontStyle.Bold,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s nadpisem</para>
            /// <para lang="en">Font color for label with main caption</para>
            /// </summary>
            public static readonly Color LabelMainForeColor =
                Color.Black;

            #endregion Label


            #region ListBox

            /// <summary>
            /// <para lang="cs">Styl písma pro seznam</para>
            /// <para lang="en">Font style for list box</para>
            /// </summary>
            public static readonly Font ListBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro seznam</para>
            /// <para lang="en">Font color for list box</para>
            /// </summary>
            public static readonly Color ListBoxForeColor =
                Color.Black;

            #endregion ListBox


            #region RadioButton

            /// <summary>
            /// <para lang="cs">Styl písma pro přepínací tlačítko</para>
            /// <para lang="en">Font style for radio button</para>
            /// </summary>
            public static readonly Font RadioButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro přepínací tlačítko</para>
            /// <para lang="en">Font color for radio button</para>
            /// </summary>
            public static readonly Color RadioButtonForeColor =
                Color.Black;

            #endregion RadioButton


            #region Table

            /// <summary>
            /// <para lang="cs">Styl písma v hlavičce tabulky</para>
            /// <para lang="en">Font style in table header</para>
            /// </summary>
            public static readonly Font TableHeaderFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma v hlavičce tabulky</para>
            /// <para lang="en">Font color in table header</para>
            /// </summary>
            public static readonly Color TableHeaderForeColor =
                Color.White;

            /// <summary>
            /// <para lang="cs">Barva pozadí hlavičky tabulky</para>
            /// <para lang="en">Table header back color</para>
            /// </summary>
            public static readonly Color TableHeaderBackColor =
                Color.DarkBlue;


            /// <summary>
            /// <para lang="cs">Styl písma pro řádek tabulky</para>
            /// <para lang="en">Font style for table row</para>
            /// </summary>
            public static readonly Font TableRowFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro řádek tabulky</para>
            /// <para lang="en">Font color for table row</para>
            /// </summary>
            public static readonly Color TableRowForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro řádek tabulky</para>
            /// <para lang="en">Table row back color</para>
            /// </summary>
            public static readonly Color TableRowBackColor =
                Color.White;


            /// <summary>
            /// <para lang="cs">Styl písma pro vybraný řádek tabulky</para>
            /// <para lang="en">Font style for selected table row</para>
            /// </summary>
            public static readonly Font TableSelectedRowFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro vzbrařádek tabulky</para>
            /// <para lang="en">Font color for table row</para>
            /// </summary>
            public static readonly Color TableSelectedRowForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro řádek tabulky</para>
            /// <para lang="en">Table row back color</para>
            /// </summary>
            public static readonly Color TableSelectedRowBackColor =
                Color.Bisque;

            #endregion Table


            #region TextBox

            /// <summary>
            /// <para lang="cs">Styl písma pro textové políčko</para>
            /// <para lang="en">Font style for text box</para>
            /// </summary>
            public static readonly Font TextBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro textové políčko</para>
            /// <para lang="en">Font color for text box</para>
            /// </summary>
            public static readonly Color TextBoxForeColor =
                Color.Black;


            /// <summary>
            /// <para lang="cs">Styl písma pro textové políčko s kódem</para>
            /// <para lang="en">Font style for text box</para>
            /// </summary>
            public static readonly Font TextBoxCodeFont = new(
                   familyName: "Courier New",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro textové políčko s kódem</para>
            /// <para lang="en">Font color for text box with code</para>
            /// </summary>
            public static readonly Color TextBoxCodeForeColor =
                Color.Black;

            #endregion TextBox


            #region ToolStripButton

            /// <summary>
            /// <para lang="cs">Styl písma pro tlačítko</para>
            /// <para lang="en">Font style for tool strip button</para>
            /// </summary>
            public static readonly Font ToolStripButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro tlačítko</para>
            /// <para lang="en">Font color for button</para>
            /// </summary>
            public static readonly Color ToolStripButtonForeColor =
                Color.Black;

            #endregion ToolStripButton

            #endregion Style

        }

    }
}