﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Pár nadřazené a podřízené kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Pair of superior and inferior area domain category or subpopulation category</para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        internal class CategoryPair<TDomain, TCategory>
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Nadřazená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Superior area domain category or subpopulation category</para>
            /// </summary>
            private TCategory superior;

            /// <summary>
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain category or subpopulation category</para>
            /// </summary>
            private TCategory inferior;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="superior">
            /// <para lang="cs">Nadřazená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Superior area domain category or subpopulation category</para>
            /// </param>
            /// <param name="inferior">
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain category or subpopulation category</para>
            /// </param>
            public CategoryPair(
                TCategory superior,
                TCategory inferior)
            {
                Initialize(
                    superior: superior,
                    inferior: inferior);
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
            /// <para lang="en">Domain and its categories type (read-only)</para>
            /// </summary>
            public static TDArealOrPopulationEnum ArealOrPopulation
            {
                get
                {
                    return typeof(TDomain).FullName switch
                    {
                        "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    TDArealOrPopulationEnum.AreaDomain,

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                        paramName: nameof(TCategory)),

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    TDArealOrPopulationEnum.Population,

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        _ =>
                            throw new ArgumentException(
                                message: String.Concat(
                                    $"Argument {nameof(TDomain)} must be type of ",
                                    $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                                paramName: nameof(TDomain))
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Nadřazená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Superior area domain category or subpopulation category</para>
            /// </summary>
            public TCategory Superior
            {
                get
                {
                    if (superior != null)
                    {
                        return superior;
                    }
                    else
                    {
                        throw new ArgumentException(
                        message: $"Argument {nameof(Superior)} must not be null.",
                        paramName: nameof(Superior));
                    }
                }
                set
                {
                    if (value != null)
                    {
                        superior = value;
                    }
                    else
                    {
                        throw new ArgumentException(
                        message: $"Argument {nameof(Superior)} must not be null.",
                        paramName: nameof(Superior));
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain category or subpopulation category</para>
            /// </summary>
            public TCategory Inferior
            {
                get
                {
                    if (inferior != null)
                    {
                        return inferior;
                    }
                    else
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Inferior)} must not be null.",
                            paramName: nameof(Inferior));
                    }
                }
                set
                {
                    if (value != null)
                    {
                        inferior = value;
                    }
                    else
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Inferior)} must not be null.",
                            paramName: nameof(Inferior));
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Nadřazená kategorie plošné domény (read-only)</para>
            /// <para lang="en">Superior area domain category (read-only)</para>
            /// </summary>
            public TDAreaDomainCategory SuperiorAreaDomainCategory
            {
                get
                {
                    return ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain =>
                            (TDAreaDomainCategory)Convert.ChangeType(
                                value: Superior,
                                conversionType: typeof(TDAreaDomainCategory)),

                        TDArealOrPopulationEnum.Population => null,

                        TDArealOrPopulationEnum.Unknown => null,

                        _ => null,
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Nadřazená kategorie subpopulace (read-only)</para>
            /// <para lang="en">Superior subpopulation category (read-only)</para>
            /// </summary>
            public TDSubPopulationCategory SuperiorSubPopulationCategory
            {
                get
                {
                    return ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => null,

                        TDArealOrPopulationEnum.Population =>
                            (TDSubPopulationCategory)Convert.ChangeType(
                                    value: Superior,
                                    conversionType: typeof(TDSubPopulationCategory)),

                        TDArealOrPopulationEnum.Unknown => null,

                        _ => null,
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Podřízená kategorie plošné domény (read-only)</para>
            /// <para lang="en">Inferior area domain category (read-only)</para>
            /// </summary>
            public TDAreaDomainCategory InferiorAreaDomainCategory
            {
                get
                {
                    return ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain =>
                            (TDAreaDomainCategory)Convert.ChangeType(
                                value: Inferior,
                                conversionType: typeof(TDAreaDomainCategory)),

                        TDArealOrPopulationEnum.Population => null,

                        TDArealOrPopulationEnum.Unknown => null,

                        _ => null,
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Podřízená kategorie subpopulace (read-only)</para>
            /// <para lang="en">Inferior subpopulation category (read-only)</para>
            /// </summary>
            public TDSubPopulationCategory InferiorSubPopulationCategory
            {
                get
                {
                    return ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => null,

                        TDArealOrPopulationEnum.Population =>
                            (TDSubPopulationCategory)Convert.ChangeType(
                                value: Inferior,
                                conversionType: typeof(TDSubPopulationCategory)),

                        TDArealOrPopulationEnum.Unknown => null,

                        _ => null,
                    };
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Inicializace objektu</para>
            /// <para lang="en">Initializing the object</para>
            /// </summary>
            /// <param name="superior">
            /// <para lang="cs">Nadřazená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Superior area domain category or subpopulation category</para>
            /// </param>
            /// <param name="inferior">
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain category or subpopulation category</para>
            /// </param>
            private void Initialize(
                TCategory superior,
                TCategory inferior)
            {
                Superior = superior;
                Inferior = inferior;
            }

            /// <summary>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </returns>
            public override string ToString()
            {
                return String.Concat(
                    $"Superior category: ",
                    $"{Functions.PrepStringArg(arg: Superior.ToString())},{Environment.NewLine}",
                    $"Inferior category: ",
                    $"{Functions.PrepStringArg(arg: Inferior.ToString())}.{Environment.NewLine}"
                );
            }

            /// <summary>
            /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
            /// <para lang="en">Determines whether specified object is equal to the current object</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Zadaný objekt</para>
            /// <para lang="en">Speicified object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not CategoryPair<TDomain, TCategory> Type, return False
                if (obj is not CategoryPair<TDomain, TCategory>)
                {
                    return false;
                }

                return

                    Superior.Equals(obj: ((CategoryPair<TDomain, TCategory>)obj).Superior) &&

                    Inferior.Equals(obj: ((CategoryPair<TDomain, TCategory>)obj).Inferior);
            }

            /// <summary>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </returns>
            public override int GetHashCode()
            {
                return
                    Superior.GetHashCode() ^
                    Inferior.GetHashCode();
            }

            #endregion Methods

        }

    }
}