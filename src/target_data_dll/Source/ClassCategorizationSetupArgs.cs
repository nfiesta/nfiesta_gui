﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Vstupní argumenty uložených procedur
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input a fn_save_constrained_target_variable
        /// </para>
        /// <para lang="en">Input arguments for stored procedures
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input and fn_save_constrained_target_variable
        /// </para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        internal class CategorizationSetupArgs
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            private ControlTDSelectionAttributeCategory owner;

            /// <summary>
            /// <para lang="cs">Formulář pro "Omezení cílové veličiny na úrovni bez rozlišení"</para>
            /// <para lang="en">Form for "Limit target variable at level of the altogether category"</para>
            /// </summary>
            private FormTargetVariableLimit formTargetVariableLimit;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            public CategorizationSetupArgs(ControlTDSelectionAttributeCategory owner)
            {
                Owner = owner;
                FormTargetVariableLimit = null;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            public ControlTDSelectionAttributeCategory Owner
            {
                get
                {
                    return owner ??
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                }
                private set
                {
                    owner = value ??
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                }
            }

            /// <summary>
            /// <para lang="cs">Formulář pro "Omezení cílové veličiny na úrovni bez rozlišení"</para>
            /// <para lang="en">Form for "Limit target variable at level of the altogether category"</para>
            /// </summary>
            public FormTargetVariableLimit FormTargetVariableLimit
            {
                get
                {
                    return formTargetVariableLimit;
                }
                set
                {
                    formTargetVariableLimit = value;
                }
            }


            /// <summary>
            /// <para lang="cs">Vybraná cílová proměnná (vybraná na 2. nebo 3. formuláři) (read-only)</para>
            /// <para lang="en">Selected target variable (selected on the 2nd or 3rd form) (read-only)</para>
            /// </summary>
            public TDTargetVariable SelectedTargetVariable
            {
                get
                {
                    return Owner.SelectedTargetVariable;
                }
            }


            /// <summary>
            /// <para lang="cs">
            /// Objekty lokálních hustot pro vybranou cílovou proměnnou (read-only)
            /// (všechny Core + Division)
            /// (setříděno podle klíče)
            /// </para>
            /// <para lang="en">
            /// Local density objects for selected target variable (read-only)
            /// (all Core + Division)
            /// (sorted by key)
            /// </para>
            /// </summary>
            public Dictionary<
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
                LDsityObjectsForTargetVariable
            {
                get
                {
                    return Owner.LDsityObjectsForTargetVariable;
                }
            }


            /// <summary>
            /// <para lang="cs">Identifikační číslo vybrané cílová proměnná (read-only)</para>
            /// <para lang="en">Selected target variable identifier (read-only)</para>
            /// </summary>
            public Nullable<int> TargetVariableId
            {
                get
                {
                    if (SelectedTargetVariable != null)
                    {
                        return SelectedTargetVariable.Id;
                    }
                    else
                    {
                        return null;
                    }
                }
            }


            /// <summary>
            /// <para lang="cs">Seznam identifikačních čísel z mapovací tabulky cm_ldsity2target_variable
            /// pro vybranou cílovou proměnnou (read-only)</para>
            /// <para lang="en">List of records identifiers from mapping table cm_ldsity2target_variable
            /// for selected target variable (read-only)</para>
            /// </summary>
            public List<Nullable<int>> CMTargetVariableIds
            {
                get
                {
                    if (LDsityObjectsForTargetVariable != null)
                    {
                        // Seznam musí být seřazen podle identifikátoru lokální hustoty
                        // a use negative (podle klíče slovníku)
                        return
                            LDsityObjectsForTargetVariable
                                .OrderBy(a => a.Key)
                                .ToDictionary(a => a.Key, a => a.Value)
                                .Values
                                .Select(a => (Nullable<int>)a.CmLDsityToTargetVariableId)
                                .ToList<Nullable<int>>();
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam identifikačních čísel z mapovací tabulky cm_ldsity2target_variable
            /// pro vybranou cílovou proměnnou (read-only)</para>
            /// <para lang="en">List of records identifiers from mapping table cm_ldsity2target_variable
            /// for selected target variable (read-only)</para>
            /// </summary>
            public string CMTargetVariableIdsText
            {
                get
                {
                    return
                        Functions.PrepNIntArrayArg(args: CMTargetVariableIds, dbType: "int4");
                }
            }


            /// <summary>
            /// <para lang="cs">Seznam identifikačních čísel plošných domén (read-only)</para>
            /// <para lang="en">List area domain identifiers (read-only)</para>
            /// </summary>
            public List<Nullable<int>> AreaDomainIds
            {
                get
                {
                    return
                        Owner.LstSelectedAreaDomains?.GetDomainIdentifiers();
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam identifikačních čísel plošných domén (read-only)</para>
            /// <para lang="en">List area domain identifiers (read-only)</para>
            /// </summary>
            public string AreaDomainIdsText
            {
                get
                {
                    return
                        Functions.PrepNIntArrayArg(args: AreaDomainIds, dbType: "int4");
                }
            }


            /// <summary>
            /// <para lang="cs">Seznam identifikačních čísel subpopulací (read-only)</para>
            /// <para lang="en">List subpopulation identifiers (read-only)</para>
            /// </summary>
            public List<Nullable<int>> SubPopulationIds
            {
                get
                {
                    return
                        Owner.LstSelectedSubPopulations?.GetDomainIdentifiers();
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam identifikačních čísel subpopulací (read-only)</para>
            /// <para lang="en">List subpopulation identifiers (read-only)</para>
            /// </summary>
            public string SubPopulationIdsText
            {
                get
                {
                    return
                        Functions.PrepNIntArrayArg(args: SubPopulationIds, dbType: "int4");
                }
            }


            /// <summary>
            /// <para lang="cs">Seznam se seznamy identifikačních čísel objektů lokálních hustot pro plošné domény (read-only)</para>
            /// <para lang="en">List of lists of local density object identifiers for area domains (read-only)</para>
            /// </summary>
            public List<List<Nullable<int>>> AreaDomainObjectIds
            {
                get
                {
                    // Vnější pole musí mít stejné pořadí prvků jako CMTargetVariableIds
                    // Vnitřní pole musí mít stejné pořadí prvků jako AreaDomainIds
                    return
                        Owner.LstSelectedAreaDomains?.GetSelectedLDsityObjectForADSPIdentifiers();
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam se seznamy identifikačních čísel objektů lokálních hustot pro plošné domény (read-only)</para>
            /// <para lang="en">List of lists of local density object identifiers for area domains (read-only)</para>
            /// </summary>
            public string AreaDomainObjectIdsText
            {
                get
                {
                    return
                        Functions.PrepNIntArrayOfArrayArg(args: AreaDomainObjectIds, dbType: "int4");
                }
            }


            /// <summary>
            /// <para lang="cs">Seznam se seznamy identifikačních čísel objektů lokálních hustot pro subpopulace (read-only)</para>
            /// <para lang="en">List of lists of local density object identifiers for subpopulations (read-only)</para>
            /// </summary>
            public List<List<Nullable<int>>> SubPopulationObjectIds
            {
                get
                {
                    // Vnější pole musí mít stejné pořadí prvků jako CMTargetVariableIds
                    // Vnitřní pole musí mít stejné pořadí prvků jako SubPopulationIds
                    return
                        Owner.LstSelectedSubPopulations?.GetSelectedLDsityObjectForADSPIdentifiers();
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam se seznamy identifikačních čísel objektů lokálních hustot pro subpopulace (read-only)</para>
            /// <para lang="en">List of lists of local density object identifiers for subpopulations (read-only)</para>
            /// </summary>
            public string SubPopulationObjectIdsText
            {
                get
                {
                    return
                        Functions.PrepNIntArrayOfArrayArg(args: SubPopulationObjectIds, dbType: "int4");
                }
            }


            /// <summary>
            /// <para lang="cs">
            /// Seznam identifikačních čísel kategorií plošné domény, které tvoří atributovou kategorii
            /// </para>
            /// <para lang="en">
            /// List of area domain category identification numbers that make up an attribute category
            /// </para>
            /// </summary>
            public List<List<Nullable<int>>> AreaDomainCategoryIds
            {
                get
                {
                    return
                        FormTargetVariableLimit?.AreaDomainCategoryIds;
                }
            }

            /// <summary>
            /// <para lang="cs">
            /// Seznam identifikačních čísel kategorií plošné domény, které tvoří atributovou kategorii
            /// </para>
            /// <para lang="en">
            /// List of area domain category identification numbers that make up an attribute category
            /// </para>
            /// </summary>
            public string AreaDomainCategoryIdsText
            {
                get
                {
                    return
                        Functions.PrepNIntArrayOfArrayArg(args: AreaDomainCategoryIds, dbType: "int4");
                }
            }


            /// <summary>
            /// <para lang="cs">
            /// Seznam identifikačních čísel kategorií subpopulace, které tvoří atributovou kategorii
            /// </para>
            /// <para lang="en">
            /// List of subpopulation category identification numbers that make up an attribute category
            /// </para>
            /// </summary>
            public List<List<Nullable<int>>> SubPopulationCategoryIds
            {
                get
                {
                    return
                        FormTargetVariableLimit?.SubPopulationCategoryIds;
                }
            }

            /// <summary>
            /// <para lang="cs">
            /// Seznam identifikačních čísel kategorií subpopulace, které tvoří atributovou kategorii
            /// </para>
            /// <para lang="en">
            /// List of subpopulation category identification numbers that make up an attribute category
            /// </para>
            /// </summary>
            public string SubPopulationCategoryIdsText
            {
                get
                {
                    return
                        Functions.PrepNIntArrayOfArrayArg(args: SubPopulationCategoryIds, dbType: "int4");
                }
            }

            /// <summary>
            /// <para lang="cs">Skupina plošná doména s objektem lokální hustoty</para>
            /// <para lang="en">Area domain with local density object group</para>
            /// </summary>
            public class AreaDomainLDsityObject
            {
                /// <summary>
                /// <para lang="cs">Plošná doména</para>
                /// <para lang="en">Area domain</para>
                /// </summary>
                public TDAreaDomain AreaDomain { get; set; }

                /// <summary>
                /// <para lang="cs">Objekt lokální hustoty</para>
                /// <para lang="en">Local density object</para>
                /// </summary>
                public TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier LDSityObject { get; set; }

                /// <summary>
                /// <para lang="cs">Skupina obsahuje klasifikační pravidla</para>
                /// <para lang="en">Group contains classification rules</para>
                /// </summary>
                public bool WithClassificationRules { get; set; }
            }

            /// <summary>
            /// <para lang="cs">Seznam skupin plošná doména s objektem lokální hustoty</para>
            /// <para lang="en">List of area domain with local density object groups</para>
            /// </summary>
            public List<AreaDomainLDsityObject> ClassificationRulesForADLDObjects
            {
                get
                {
                    List<AreaDomainLDsityObject> result = [];

                    if (Owner.LstSelectedAreaDomains != null)
                    {
                        foreach (var areaDomainItem in
                            Owner.LstSelectedAreaDomains.Items
                                .Select(a => new
                                {
                                    Key = (TDAreaDomain)a.Key,
                                    Val = a.Value
                                }))
                        {
                            foreach (KeyValuePair<TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier, TDLDsityObject>
                                pair in areaDomainItem.Val)
                            {
                                if (pair.Value != null)
                                {
                                    result.Add(item: new AreaDomainLDsityObject()
                                    {
                                        AreaDomain = areaDomainItem.Key,
                                        LDSityObject = pair.Key,
                                        WithClassificationRules = pair.Value.ClassificationRule ?? false
                                    });
                                }
                                else
                                {
                                    result.Add(item: new AreaDomainLDsityObject()
                                    {
                                        AreaDomain = areaDomainItem.Key,
                                        LDSityObject = pair.Key,
                                        WithClassificationRules = false
                                    });
                                }
                            }
                        }
                    }

                    return result;
                }
            }

            /// <summary>
            /// <para lang="cs">Skupina subpopulace s objektem lokální hustoty</para>
            /// <para lang="en">Subpopulation with local density object group</para>
            /// </summary>
            public class SubPopulationLDsityObject
            {
                /// <summary>
                /// <para lang="cs">Subpopulace</para>
                /// <para lang="en">Subpopulation</para>
                /// </summary>
                public TDSubPopulation SubPopulation { get; set; }

                /// <summary>
                /// <para lang="cs">Objekt lokální hustoty</para>
                /// <para lang="en">Local density object</para>
                /// </summary>
                public TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier LDSityObject { get; set; }

                /// <summary>
                /// <para lang="cs">Skupina obsahuje klasifikační pravidla</para>
                /// <para lang="en">Group contains classification rules</para>
                /// </summary>
                public bool WithClassificationRules { get; set; }
            }

            /// <summary>
            /// <para lang="cs">Seznam skupin subpopulace s objektem lokální hustoty</para>
            /// <para lang="en">List of Subpopulation with local density object groups</para>
            /// </summary>
            public List<SubPopulationLDsityObject> ClassificationRulesForSPLDObjects
            {
                get
                {
                    List<SubPopulationLDsityObject> result = [];

                    if (Owner.LstSelectedSubPopulations != null)
                    {
                        foreach (var subpopulationItem in
                            Owner.LstSelectedSubPopulations.Items
                                .Select(a => new
                                {
                                    Key = (TDSubPopulation)a.Key,
                                    Val = a.Value
                                }))
                        {
                            foreach (KeyValuePair<TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier, TDLDsityObject>
                                pair in subpopulationItem.Val)
                            {
                                if (pair.Value != null)
                                {
                                    result.Add(item: new SubPopulationLDsityObject()
                                    {
                                        SubPopulation = subpopulationItem.Key,
                                        LDSityObject = pair.Key,
                                        WithClassificationRules = pair.Value.ClassificationRule ?? false
                                    });
                                }
                                else
                                {
                                    result.Add(item: new SubPopulationLDsityObject()
                                    {
                                        SubPopulation = subpopulationItem.Key,
                                        LDSityObject = pair.Key,
                                        WithClassificationRules = false
                                    });
                                }
                            }
                        }
                    }

                    return result;
                }
            }

            #endregion Properties

        }

    }
}