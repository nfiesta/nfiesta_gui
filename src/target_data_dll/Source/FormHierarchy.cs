﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro definici hierarchie kategorií plošných domén nebo subpopulací - třída pro Designer</para>
    /// <para lang="en">Form for defining a hierarchy of area domain or subpopulation categories - class for Designer</para>
    /// </summary>
    internal partial class FormHierarchyDesign
        : Form
    {
        protected FormHierarchyDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Button BtnCancel => btnCancel;
        protected System.Windows.Forms.Button BtnOK => btnOK;
        protected System.Windows.Forms.ComboBox CboInferior => cboInferior;
        protected System.Windows.Forms.ComboBox CboSuperior => cboSuperior;
        protected System.Windows.Forms.Label LblInferiorCaption => lblInferiorCaption;
        protected System.Windows.Forms.Label LblInferiorValue => lblInferiorValue;
        protected System.Windows.Forms.Label LblSuperiorCaption => lblSuperiorCaption;
        protected System.Windows.Forms.Label LblSuperiorValue => lblSuperiorValue;
        protected System.Windows.Forms.Panel PnlWorkSpace => pnlWorkSpace;
    }

    /// <summary>
    /// <para lang="cs">Formulář pro definici hierarchie kategorií plošných domén nebo subpopulací</para>
    /// <para lang="en">Form for defining a hierarchy of area domain or subpopulation categories</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class FormHierarchy<TDomain, TCategory>
            : FormHierarchyDesign, INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Příznak pro definici nové hierarchie</para>
        /// <para lang="en">Flag for defining a new hierarchy</para>
        /// </summary>
        private bool flagNewHierarchy;

        /// <summary>
        /// <para lang="cs">Seznam plošných domén nebo subpopulací</para>
        /// <para lang="en">List of area domains or subpopulations</para>
        /// </summary>
        private List<SuperiorDomain<TDomain, TCategory>> domains;

        /// <summary>
        /// <para lang="cs">Seznam zobrazených ovládacích prvků</para>
        /// <para lang="en">List of displayed control</para>
        /// </summary>
        private List<ControlTDCategory<TDomain, TCategory>> controls;

        private string strWrite = String.Empty;
        private string msgDeleteCopy = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře
        /// (pro existující podřízené plošné domény nebo subpopulace)</para>
        /// <para lang="en">Form constructor
        /// (for existing inferior area domains or subpopulations)</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domains">
        /// <para lang="cs">Vybrané existující podřízené plošné domény nebo subpopulace</para>
        /// <para lang="en">Selected, existing, inferior area domains or subpopulations</para>
        /// </param>
        public FormHierarchy(
            Control controlOwner,
            List<TDomain> domains)
            : base()
        {
            Initialize(
                controlOwner: controlOwner,
                domains: domains);
        }

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře
        /// (pro novou podřízenou plošnou doménu nebo subpopulaci)</para>
        /// <para lang="en">Form constructor
        /// (for a new inferior area domain or subpopulation)</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Nadřazená plošná doména nebo subpopulace
        /// s novou podřízenou plošnou doménou nebo subpopulací</para>
        /// <para lang="en">Superior area domain or subpopulation
        /// with a new, inferior, area domain or subpopulation</para>
        /// </param>
        public FormHierarchy(
            Control controlOwner,
            SuperiorDomain<TDomain, TCategory> domain)
            : base()
        {
            Initialize(
                controlOwner: controlOwner,
                domain: domain);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam plošných domén nebo subpopulací (read-only)</para>
        /// <para lang="en">List of area domains or subpopulations (read-only)</para>
        /// </summary>
        public List<SuperiorDomain<TDomain, TCategory>> Domains
        {
            get
            {
                return domains;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(FormHierarchy<TDomain, TCategory>),    "Hierarchie kategorií plošných domén" },
                            { nameof(BtnCancel),                            "Zrušit" },
                            { nameof(BtnOK),                                "OK" },
                            { nameof(strWrite),                             "Zapsat do databáze" },
                            { nameof(CboSuperior),                          "LabelCs" },
                            { nameof(CboInferior),                          "LabelCs" },
                            { nameof(LblSuperiorCaption),                   "Nadřazená plošná doména:" },
                            { nameof(LblInferiorCaption),                   "Podřízená plošná doména:" },
                            { nameof(msgDeleteCopy),                        "Pouze kopie kategorie plošné domény mohou být smazány." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormHierarchy<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(FormHierarchy<TDomain, TCategory>),    "Hierarchie kategorií subpopulací" },
                            { nameof(BtnCancel),                            "Zrušit" },
                            { nameof(BtnOK),                                "OK" },
                            { nameof(strWrite),                             "Zapsat do databáze" },
                            { nameof(CboSuperior),                          "LabelCs" },
                            { nameof(CboInferior),                          "LabelCs" },
                            { nameof(LblSuperiorCaption),                   "Nadřazená subpopulace:" },
                            { nameof(LblInferiorCaption),                   "Podřízená subpopulace:" },
                            { nameof(msgDeleteCopy),                        "Pouze kopie kategorie subpopulace mohou být smazány." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormHierarchy<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    _ => [],
                },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(FormHierarchy<TDomain, TCategory>),    "Hierarchy of the area domain categories" },
                            { nameof(BtnCancel),                            "Cancel" },
                            { nameof(BtnOK),                                "OK" },
                            { nameof(strWrite),                             "Write to database" },
                            { nameof(CboSuperior),                          "LabelEn" },
                            { nameof(CboInferior),                          "LabelEn" },
                            { nameof(LblSuperiorCaption),                   "Superior area domain:" },
                            { nameof(LblInferiorCaption),                   "Inferior area domain:" },
                            { nameof(msgDeleteCopy),                        "Only copies of area domain category can be deleted." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormHierarchy<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(FormHierarchy<TDomain, TCategory>),    "Hierarchy of the subpopulation categories" },
                            { nameof(BtnCancel),                            "Cancel" },
                            { nameof(BtnOK),                                "OK" },
                            { nameof(strWrite),                             "Write to database" },
                            { nameof(CboSuperior),                          "LabelEn" },
                            { nameof(CboInferior),                          "LabelEn" },
                            { nameof(LblSuperiorCaption),                   "Superior subpopulation:" },
                            { nameof(LblInferiorCaption),                   "Inferior subpopulation:" },
                            { nameof(msgDeleteCopy),                        "Only copies of subpopulation category can be deleted." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormHierarchy<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře
        /// (pro existující podřízené plošné domény nebo subpopulace)</para>
        /// <para lang="en">Initializing the form
        /// (for existing inferior area domains or subpopulations)</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domains">
        /// <para lang="cs">Vybrané existující podřízené plošné domény nebo subpopulace</para>
        /// <para lang="en">Selected, existing, inferior area domains or subpopulations</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            List<TDomain> domains)
        {
            SetEventHandlers();

            ControlOwner = controlOwner;

            onEdit = false;
            flagNewHierarchy = false;

            if (domains != null)
            {
                this.domains = domains
                    .Select(a => new SuperiorDomain<TDomain, TCategory>(
                        owner: this,
                        domain: a,
                        inferiorDomains: domains))
                    .ToList<SuperiorDomain<TDomain, TCategory>>();
            }
            else
            {
                throw new ArgumentException(
                    message: $"Argument {nameof(domains)} must not be null.",
                    paramName: nameof(domains));
            }

            controls = [];

            SetHeader();

            onEdit = true;
            CboSuperior.DataSource = this.domains;
            onEdit = false;

            SuperiorChanged();

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace formuláře
        /// (pro novou podřízenou plošnou doménu nebo subpopulaci)</para>
        /// <para lang="en">Initializing the form
        /// (for a new inferior area domain or subpopulation)</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Nadřazená plošná doména nebo subpopulace
        /// s novou podřízenou plošnou doménou nebo subpopulací</para>
        /// <para lang="en">Superior area domain or subpopulation
        /// with a new, inferior, area domain or subpopulation</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            SuperiorDomain<TDomain, TCategory> domain)
        {
            SetEventHandlers();

            ControlOwner = controlOwner;

            onEdit = false;
            flagNewHierarchy = true;

            if (domain != null)
            {
                domains = [domain];
            }
            else
            {
                throw new ArgumentException(
                    message: $"Argument {nameof(domain)} must not be null.",
                    paramName: nameof(domain));
            }

            controls = [];

            SetHeader();

            LblSuperiorValue.Text =
                (LanguageVersion == LanguageVersion.National) ? domain.Domain.LabelCs :
                (LanguageVersion == LanguageVersion.International) ? domain.Domain.LabelEn :
                String.Empty;
            LblSuperiorValue.Tag = domain;

            LblInferiorValue.Text =
                (LanguageVersion == LanguageVersion.National) ? domain.InferiorCategories[0].LabelCs :
                (LanguageVersion == LanguageVersion.International) ? domain.InferiorCategories[0].LabelEn :
                String.Empty;
            LblInferiorValue.Tag = domain.InferiorCategories[0];

            DisplayChart(
                sup: domain,
                inf: domain.InferiorDomains[0]);

            InitializeLabels();

            SetStyle();

            BtnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Nastaví obsluhu událostí</para>
        /// <para lang="en">Set event handlers</para>
        /// </summary>
        private void SetEventHandlers()
        {
            BtnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            BtnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            CboSuperior.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SuperiorChanged();
                    }
                });

            CboInferior.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        InferiorChanged();
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Nastavení viditelnosti položek v hlavičce, pode typu formuláře</para>
        /// <para lang="en">Setting the visibility of items in the header, by form type</para>
        /// </summary>
        private void SetHeader()
        {
            if (flagNewHierarchy)
            {
                CboSuperior.Visible = false;
                CboInferior.Visible = false;
                LblSuperiorValue.Visible = true;
                LblInferiorValue.Visible = true;
                CboSuperior.Dock = DockStyle.None;
                CboInferior.Dock = DockStyle.None;
                LblSuperiorValue.Dock = DockStyle.Fill;
                LblInferiorValue.Dock = DockStyle.Fill;
                LblSuperiorValue.Text = String.Empty;
                LblInferiorValue.Text = String.Empty;
            }
            else
            {
                LblSuperiorValue.Visible = false;
                LblInferiorValue.Visible = false;
                CboSuperior.Visible = true;
                CboInferior.Visible = true;
                LblSuperiorValue.Dock = DockStyle.None;
                LblInferiorValue.Dock = DockStyle.None;
                CboSuperior.Dock = DockStyle.Fill;
                CboInferior.Dock = DockStyle.Fill;
                LblSuperiorValue.Text = String.Empty;
                LblInferiorValue.Text = String.Empty;
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing the form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: nameof(FormHierarchy<TDomain, TCategory>),
                    out string frmHierarchy)
                        ? frmHierarchy
                        : String.Empty;

            BtnCancel.Text =
                labels.TryGetValue(
                    key: nameof(BtnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            strWrite =
              labels.TryGetValue(key: nameof(strWrite),
                      out strWrite)
                          ? strWrite
                          : String.Empty;
            BtnOK.Text =
                flagNewHierarchy
                ? labels.TryGetValue(
                    key: nameof(BtnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty
                : strWrite;

            LblSuperiorCaption.Text =
                labels.TryGetValue(
                    key: nameof(LblSuperiorCaption),
                    out string lblSuperiorCaptionText)
                        ? lblSuperiorCaptionText
                        : String.Empty;

            LblInferiorCaption.Text =
                labels.TryGetValue(
                    key: nameof(LblInferiorCaption),
                    out string lblInferiorCaptionText)
                        ? lblInferiorCaptionText
                        : String.Empty;

            msgDeleteCopy =
              labels.TryGetValue(key: nameof(msgDeleteCopy),
                      out msgDeleteCopy)
                          ? msgDeleteCopy
                          : String.Empty;

            onEdit = true;

            CboSuperior.DisplayMember =
                labels.TryGetValue(
                    key: nameof(CboSuperior),
                    out string cboSuperiorDisplayMember)
                        ? cboSuperiorDisplayMember
                        : String.Empty;

            CboInferior.DisplayMember =
                labels.TryGetValue(
                    key: nameof(CboInferior),
                    out string cboInferiorDisplayMember)
                        ? cboInferiorDisplayMember
                        : String.Empty;

            onEdit = false;

            foreach (ControlTDCategory<TDomain, TCategory> control in controls)
            {
                control.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            BtnOK.Font = Setting.ButtonFont;
            BtnOK.ForeColor = Setting.ButtonForeColor;

            BtnCancel.Font = Setting.ButtonFont;
            BtnOK.ForeColor = Setting.ButtonForeColor;

            LblInferiorCaption.Font = Setting.LabelFont;
            LblInferiorCaption.ForeColor = Setting.LabelForeColor;

            LblSuperiorCaption.Font = Setting.LabelFont;
            LblSuperiorCaption.ForeColor = Setting.LabelForeColor;

            LblInferiorValue.Font = Setting.LabelValueFont;
            LblInferiorValue.ForeColor = Setting.LabelValueForeColor;

            LblSuperiorValue.Font = Setting.LabelValueFont;
            LblSuperiorValue.ForeColor = Setting.LabelValueForeColor;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Změna výběru nadřazené plošné domény nebo subpopulace</para>
        /// <para lang="en">Changing the selection of a superior area domain or subpopulation</para>
        /// </summary>
        private void SuperiorChanged()
        {
            if (CboSuperior.SelectedItem == null)
            {
                ClearWorkSpace();
                return;
            }

            SuperiorDomain<TDomain, TCategory> sup =
                (SuperiorDomain<TDomain, TCategory>)CboSuperior.SelectedItem;

            onEdit = true;
            CboInferior.DataSource = sup.InferiorDomains;
            onEdit = false;

            InferiorChanged();
        }

        /// <summary>
        /// <para lang="cs">Změna výběru podřízené plošné domény nebo subpopulace</para>
        /// <para lang="en">Changing the selection of an inferior area domain or subpopulation</para>
        /// </summary>
        private void InferiorChanged()
        {
            if (CboSuperior.SelectedItem == null)
            {
                ClearWorkSpace();
                return;

            }

            if (CboInferior.SelectedItem == null)
            {
                ClearWorkSpace();
                return;
            }

            SuperiorDomain<TDomain, TCategory> sup =
                (SuperiorDomain<TDomain, TCategory>)CboSuperior.SelectedItem;

            TDomain inf = (TDomain)CboInferior.SelectedItem;

            DisplayChart(
                sup: sup,
                inf: inf);
        }

        /// <summary>
        /// <para lang="cs">Vyčistí pracovní plochu</para>
        /// <para lang="en">It clears the workspace</para>
        /// </summary>
        private void ClearWorkSpace()
        {
            PnlWorkSpace.Controls.Clear();
            controls.Clear();
        }

        /// <summary>
        /// <para lang="cs">Grafické zobrazení nadřazených a podřízených kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">Graphic display of superior and inferior categories of area domains or subpopulations</para>
        /// </summary>
        /// <param name="sup">
        /// <para lang="cs">Nadřazená plošná doména nebo subpopulace</para>
        /// <para lang="en">Superior area domain or subpopulation</para>
        /// </param>
        /// <param name="inf">
        /// <para lang="cs">Podřízená plošná doména nebo subpopulace</para>
        /// <para lang="en">Inferior area domain or subpopulation</para>
        /// </param>
        private void DisplayChart(
            SuperiorDomain<TDomain, TCategory> sup,
            TDomain inf)
        {
            ClearWorkSpace();

            // Nezařazené podřízené kategorie
            // Unclassified inferior categories
            int top = 5;
            int left = 250;
            List<TCategory> notClassified =
                sup.GetUnClassifiedInferiorCategories(inferiorDomain: inf);

            foreach (TCategory cat in notClassified)
            {
                ControlTDCategory<TDomain, TCategory> control =
                    new(
                        controlOwner: this,
                        superiorControl: null,
                        category: cat,
                        isSuperior: false)
                    {
                        Left = left,
                        Top = top,
                    };
                control.Relocate += Control_Relocate;
                control.Duplicate += Control_Duplicate;
                control.Remove += Control_Remove;

                controls.Add(item: control);
                PnlWorkSpace.Controls.Add(value: control);
                top = top + control.Height + 5;
            }

            // Nadřazené kategorie
            // Superior categories
            top = 5;
            left = 5;
            foreach (TCategory supCat in sup.Categories)
            {
                ControlTDCategory<TDomain, TCategory> superiorControl =
                    new(
                        controlOwner: this,
                        superiorControl: null,
                        category: supCat,
                        isSuperior: true)
                    {
                        Left = left,
                        Top = top
                    };

                // Zařazené podřízené kategorie
                // Classified inferior categories
                List<TCategory> classified = sup.GetClassifiedInferiorCategories(
                    inferiorDomain: inf,
                    superiorCategory: supCat);
                foreach (TCategory cat in classified)
                {
                    ControlTDCategory<TDomain, TCategory> inferiorControl =
                        new(
                            controlOwner: this,
                            superiorControl: null,
                            category: cat,
                            isSuperior: false);
                    inferiorControl.Duplicate += Control_Duplicate;
                    inferiorControl.Remove += Control_Remove;

                    superiorControl.AddInferior(inferior: inferiorControl);
                }

                controls.Add(item: superiorControl);
                PnlWorkSpace.Controls.Add(value: superiorControl);
                top = top + superiorControl.Height + 5;
            }
        }

        /// <summary>
        /// <para lang="cs">Vrací true, pokud ovládací prvek pro zvolenou kategorii
        /// plošné domény nebo subpopulace existuje v kopii</para>
        /// <para lang="en">It returns true if the control for the selected
        /// area domain or subpopulation category exists in the copy</para>
        /// </summary>
        /// <param name="control">
        /// <para lang="cs">Ovládací prvek pro zvolenou kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en">Control for the selected category of area domain or subpopulation</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud ovládací prvek pro zvolenou kategorii
        /// plošné domény nebo subpopulace existuje v kopii</para>
        /// <para lang="en">It returns true if the control for the selected
        /// area domain or subpopulation category exists in the copy</para>
        /// </returns>
        private bool ControlExists(
            ControlTDCategory<TDomain, TCategory> control)
        {
            foreach (ControlTDCategory<TDomain, TCategory> a in controls)
            {
                if (a != control)
                {
                    // Hledání kopie ovládacího prvku v nezařazených kategoriích
                    // Search for a copy of a control in unclassified categories
                    if (a.Category.Id == control.Category.Id)
                    {
                        return true;
                    }
                    else
                    {
                        // Hledání kopie ovládacího prvku v zařazených kategoriích
                        // Search for a copy of a control in classified categories
                        if (a.IsSuperior)
                        {
                            foreach (ControlTDCategory<TDomain, TCategory> b in a.InferiorControls)
                            {
                                if (b != control)
                                {
                                    if (b.Category.Id == control.Category.Id)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    // Zde se nejedná o kopii ovládacího prvku, ale o jeho originál
                    // Here it is not a copy of the control, but its original
                }
            }
            return false;
        }

        /// <summary>
        /// <para lang="cs">Duplikace ovládacího prvku</para>
        /// <para lang="en">Control duplication</para>
        /// </summary>
        /// <param name="control">
        /// <para lang="cs">Ovládací prvek</para>
        /// <para lang="en">Control</para>
        /// </param>
        private void DuplicateControl(
            ControlTDCategory<TDomain, TCategory> control)
        {
            if (control == null)
            {
                // Objekt control je null
                // Object control is null
                return;
            }

            if (control.IsSuperior)
            {
                // Nadřazené kategorie nemohou být duplikovány
                // Superior categories cannot be duplicated
                return;
            }

            int left = (control.IsCategorized) ?
                ((ControlTDCategory<TDomain, TCategory>)control.SuperiorControl).Left +
                ((ControlTDCategory<TDomain, TCategory>)control.SuperiorControl).Width + 20 :
                control.Left + control.Width + 20;

            int top = (control.IsCategorized) ?
                 ((ControlTDCategory<TDomain, TCategory>)control.SuperiorControl).Top +
                 control.Top :
                 control.Top;

            ControlTDCategory<TDomain, TCategory> duplicate =
                new(
                    controlOwner: this,
                    superiorControl: null,
                    category: control.Category,
                    isSuperior: false)
                {
                    Left = left,
                    Top = top,
                };

            duplicate.Relocate += Control_Relocate;
            duplicate.Duplicate += Control_Duplicate;
            duplicate.Remove += Control_Remove;

            controls.Insert(index: 0, item: duplicate);
            PnlWorkSpace.Controls.Clear();
            foreach (ControlTDCategory<TDomain, TCategory> item in controls)
            {
                PnlWorkSpace.Controls.Add(value: item);
            }
        }

        /// <summary>
        /// <para lang="cs">Odstranění ovládacího prvku</para>
        /// <para lang="en">Remove of the control</para>
        /// </summary>
        /// <param name="control">
        /// <para lang="cs">Ovládací prvek</para>
        /// <para lang="en">Control</para>
        /// </param>
        private void RemoveControl(
            ControlTDCategory<TDomain, TCategory> control)
        {
            object selectedItem =
                (flagNewHierarchy) ?
                LblSuperiorValue.Tag :
                CboSuperior.SelectedItem;

            if (selectedItem == null)
            {
                // Není vybraná nadřazená plošná doména nebo subpopulace
                // No superior area domain or subpopulation is selected
                return;
            }

            if (control == null)
            {
                // Objekt je null
                // Object is null
                return;
            }

            if (control.IsSuperior)
            {
                // Nadřazené prvky nelze odstraňovat
                // Superior items cannot be removed
                return;
            }

            if (control.IsCategorized)
            {
                SuperiorDomain<TDomain, TCategory> sup =
                    (SuperiorDomain<TDomain, TCategory>)selectedItem;

                ControlTDCategory<TDomain, TCategory> supCtrl =
                    control.SuperiorControl;

                // Přesun zařazeného prvku mezi nezařazené
                // Move of a classified item to unclassified
                sup.RemovePair(
                    superior: supCtrl.Category,
                    inferior: control.Category);
                supCtrl.RemoveInferior(inferior: control);
                DuplicateControl(control: control);
            }
            else
            {
                // Odstranění kopie nezařazeného prvku
                // Delete of a copy of an unclassified item
                if (ControlExists(control: control))
                {
                    controls.Remove(item: control);
                    PnlWorkSpace.Controls.Remove(value: control);
                }
                else
                {
                    MessageBox.Show(
                        text: msgDeleteCopy,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Přesunutí podřízeného ovládacího prvku do nadřazeného ovládacího prvku</para>
        /// <para lang="en">Relocation of an inferior control to a superior control</para>
        /// </summary>
        /// <param name="inferiorControl">
        /// <para lang="cs">Podřízený ovládací prvek</para>
        /// <para lang="en">Inferior control</para>
        /// </param>
        private void RelocateControl(
            ControlTDCategory<TDomain, TCategory> inferiorControl)
        {
            object selectedItem =
                (flagNewHierarchy) ?
                LblSuperiorValue.Tag :
                CboSuperior.SelectedItem;

            if (selectedItem == null)
            {
                // Není vybraná nadřazená plošná doména nebo subpopulace
                // No superior area domain or subpopulation is selected
                return;
            }

            if (inferiorControl.IsSuperior)
            {
                // Nadřazené kategorie nemohou být zařazeny.
                // Superior categories cannot be assigned.
                return;
            }

            if (inferiorControl.IsCategorized)
            {
                // Zařazené kategorie nemohou být znovu zařazeny
                // Assigned categories cannot be assigned again
                return;
            }

            SuperiorDomain<TDomain, TCategory> sup =
                (SuperiorDomain<TDomain, TCategory>)selectedItem;

            foreach (ControlTDCategory<TDomain, TCategory> superiorControl in PnlWorkSpace.Controls)
            {
                if (!superiorControl.IsSuperior)
                {
                    // Do podřízených kategorií nelze nic zařazovat.
                    // Nothing can be assigned to inferior categories.
                    continue;
                }

                if (!inferiorControl.IsInside(superiorControl))
                {
                    // Podřízená kategorie není uvnitř nadřazené.
                    //(Podmínka pro zařazení podřízené k nadřazené)
                    // Inferior category in not inside the superior one.
                    //(Condition for assigning the inferior to the superior)
                    continue;
                }

                if (!superiorControl.InferiorControls
                    .Where(a => a.Category.Id == inferiorControl.Category.Id)
                    .Any())
                {
                    sup.AddPair(
                        superior: superiorControl.Category,
                        inferior: inferiorControl.Category);

                    superiorControl.AddInferior(inferior: inferiorControl);
                    controls.Remove(item: inferiorControl);
                    PnlWorkSpace.Controls.Remove(value: inferiorControl);
                }
                else
                {
                    // Nadřazená kategorie již tuto podřízenou kategorii obsahuje
                    // Superior category already contains this inferior category
                    controls.Remove(item: inferiorControl);
                    PnlWorkSpace.Controls.Remove(value: inferiorControl);
                }
            }
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události "Duplikace ovládacího prvku"</para>
        /// <para lang="en">Handling the "Control duplication" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ControlTDCategory)</para>
        /// <para lang="en">Object that sends the event (ControlTDCategory)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Control_Duplicate(object sender, EventArgs e)
        {
            ControlTDCategory<TDomain, TCategory> control =
                (ControlTDCategory<TDomain, TCategory>)sender;
            DuplicateControl(control: control);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Odstranění ovládacího prvku"</para>
        /// <para lang="en">Handling the "Remove of the control" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ControlTDCategory)</para>
        /// <para lang="en">Object that sends the event (ControlTDCategory)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Control_Remove(object sender, EventArgs e)
        {
            ControlTDCategory<TDomain, TCategory> control =
                (ControlTDCategory<TDomain, TCategory>)sender;
            RemoveControl(control: control);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Přemístění ovládacího prvku"</para>
        /// <para lang="en">Handling the "Relocation of the control" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ControlTDCategory)</para>
        /// <para lang="en">Object that sends the event (ControlTDCategory)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Control_Relocate(object sender, EventArgs e)
        {
            ControlTDCategory<TDomain, TCategory> inferiorControl =
                (ControlTDCategory<TDomain, TCategory>)sender;
            RelocateControl(inferiorControl: inferiorControl);
        }

        #endregion Event Handlers

    }

}