﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class FormTargetVariableDivisionNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            splMain = new System.Windows.Forms.SplitContainer();
            grpTargetVariable = new System.Windows.Forms.GroupBox();
            pnlTargetVariable = new System.Windows.Forms.Panel();
            tlpTargetVariable = new System.Windows.Forms.TableLayoutPanel();
            cboUnitOfMeasure = new System.Windows.Forms.ComboBox();
            txtDescriptionEnValue = new System.Windows.Forms.TextBox();
            txtLabelEnValue = new System.Windows.Forms.TextBox();
            txtDescriptionCsValue = new System.Windows.Forms.TextBox();
            lblLabelCsCaption = new System.Windows.Forms.Label();
            lblDescriptionCsCaption = new System.Windows.Forms.Label();
            lblLabelEnCaption = new System.Windows.Forms.Label();
            lblDescriptionEnCaption = new System.Windows.Forms.Label();
            txtLabelCsValue = new System.Windows.Forms.TextBox();
            lblVersion = new System.Windows.Forms.Label();
            cboVersion = new System.Windows.Forms.ComboBox();
            lblUnitOfMeasure = new System.Windows.Forms.Label();
            grpLDsityContributions = new System.Windows.Forms.GroupBox();
            pnlLDsityContributions = new System.Windows.Forms.Panel();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            grpTargetVariable.SuspendLayout();
            pnlTargetVariable.SuspendLayout();
            tlpTargetVariable.SuspendLayout();
            grpLDsityContributions.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(944, 501);
            pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(splMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 15;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += BtnCancel_Click;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += BtnOK_Click;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splMain.Location = new System.Drawing.Point(0, 0);
            splMain.Margin = new System.Windows.Forms.Padding(0);
            splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(grpTargetVariable);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(grpLDsityContributions);
            splMain.Size = new System.Drawing.Size(944, 461);
            splMain.SplitterDistance = 500;
            splMain.TabIndex = 5;
            // 
            // grpTargetVariable
            // 
            grpTargetVariable.Controls.Add(pnlTargetVariable);
            grpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            grpTargetVariable.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpTargetVariable.ForeColor = System.Drawing.Color.MediumBlue;
            grpTargetVariable.Location = new System.Drawing.Point(0, 0);
            grpTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            grpTargetVariable.Name = "grpTargetVariable";
            grpTargetVariable.Padding = new System.Windows.Forms.Padding(5);
            grpTargetVariable.Size = new System.Drawing.Size(500, 461);
            grpTargetVariable.TabIndex = 3;
            grpTargetVariable.TabStop = false;
            grpTargetVariable.Text = "grpTargetVariable";
            // 
            // pnlTargetVariable
            // 
            pnlTargetVariable.Controls.Add(tlpTargetVariable);
            pnlTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTargetVariable.Location = new System.Drawing.Point(5, 25);
            pnlTargetVariable.Name = "pnlTargetVariable";
            pnlTargetVariable.Size = new System.Drawing.Size(490, 431);
            pnlTargetVariable.TabIndex = 0;
            // 
            // tlpTargetVariable
            // 
            tlpTargetVariable.ColumnCount = 2;
            tlpTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpTargetVariable.Controls.Add(cboUnitOfMeasure, 1, 6);
            tlpTargetVariable.Controls.Add(txtDescriptionEnValue, 1, 4);
            tlpTargetVariable.Controls.Add(txtLabelEnValue, 1, 3);
            tlpTargetVariable.Controls.Add(txtDescriptionCsValue, 1, 1);
            tlpTargetVariable.Controls.Add(lblLabelCsCaption, 0, 0);
            tlpTargetVariable.Controls.Add(lblDescriptionCsCaption, 0, 1);
            tlpTargetVariable.Controls.Add(lblLabelEnCaption, 0, 3);
            tlpTargetVariable.Controls.Add(lblDescriptionEnCaption, 0, 4);
            tlpTargetVariable.Controls.Add(txtLabelCsValue, 1, 0);
            tlpTargetVariable.Controls.Add(lblVersion, 0, 7);
            tlpTargetVariable.Controls.Add(cboVersion, 1, 7);
            tlpTargetVariable.Controls.Add(lblUnitOfMeasure, 0, 6);
            tlpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpTargetVariable.Font = new System.Drawing.Font("Segoe UI", 9F);
            tlpTargetVariable.ForeColor = System.Drawing.Color.Black;
            tlpTargetVariable.Location = new System.Drawing.Point(0, 0);
            tlpTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            tlpTargetVariable.Name = "tlpTargetVariable";
            tlpTargetVariable.RowCount = 8;
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.Size = new System.Drawing.Size(490, 431);
            tlpTargetVariable.TabIndex = 3;
            // 
            // cboUnitOfMeasure
            // 
            cboUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            cboUnitOfMeasure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboUnitOfMeasure.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            cboUnitOfMeasure.ForeColor = System.Drawing.Color.Black;
            cboUnitOfMeasure.FormattingEnabled = true;
            cboUnitOfMeasure.Location = new System.Drawing.Point(153, 373);
            cboUnitOfMeasure.Name = "cboUnitOfMeasure";
            cboUnitOfMeasure.Size = new System.Drawing.Size(334, 23);
            cboUnitOfMeasure.TabIndex = 15;
            cboUnitOfMeasure.SelectedIndexChanged += CboUnitOfMeasure_SelectedIndexChanged;
            // 
            // txtDescriptionEnValue
            // 
            txtDescriptionEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEnValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtDescriptionEnValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionEnValue.Location = new System.Drawing.Point(150, 215);
            txtDescriptionEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEnValue.Multiline = true;
            txtDescriptionEnValue.Name = "txtDescriptionEnValue";
            txtDescriptionEnValue.Size = new System.Drawing.Size(340, 140);
            txtDescriptionEnValue.TabIndex = 7;
            // 
            // txtLabelEnValue
            // 
            txtLabelEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtLabelEnValue.ForeColor = System.Drawing.Color.Black;
            txtLabelEnValue.Location = new System.Drawing.Point(150, 185);
            txtLabelEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEnValue.Name = "txtLabelEnValue";
            txtLabelEnValue.Size = new System.Drawing.Size(340, 16);
            txtLabelEnValue.TabIndex = 6;
            // 
            // txtDescriptionCsValue
            // 
            txtDescriptionCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCsValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtDescriptionCsValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionCsValue.Location = new System.Drawing.Point(150, 30);
            txtDescriptionCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCsValue.Multiline = true;
            txtDescriptionCsValue.Name = "txtDescriptionCsValue";
            txtDescriptionCsValue.Size = new System.Drawing.Size(340, 140);
            txtDescriptionCsValue.TabIndex = 5;
            // 
            // lblLabelCsCaption
            // 
            lblLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblLabelCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCsCaption.Location = new System.Drawing.Point(0, 0);
            lblLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCsCaption.Name = "lblLabelCsCaption";
            lblLabelCsCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelCsCaption.TabIndex = 0;
            lblLabelCsCaption.Text = "lblLabelCsCaption";
            lblLabelCsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCsCaption
            // 
            lblDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblDescriptionCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCsCaption.Location = new System.Drawing.Point(0, 30);
            lblDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCsCaption.Name = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.Size = new System.Drawing.Size(150, 140);
            lblDescriptionCsCaption.TabIndex = 1;
            lblDescriptionCsCaption.Text = "lblDescriptionCsCaption";
            // 
            // lblLabelEnCaption
            // 
            lblLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblLabelEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelEnCaption.Location = new System.Drawing.Point(0, 185);
            lblLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEnCaption.Name = "lblLabelEnCaption";
            lblLabelEnCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelEnCaption.TabIndex = 2;
            lblLabelEnCaption.Text = "lblLabelEnCaption";
            lblLabelEnCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionEnCaption
            // 
            lblDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblDescriptionEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionEnCaption.Location = new System.Drawing.Point(0, 215);
            lblDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEnCaption.Name = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.Size = new System.Drawing.Size(150, 140);
            lblDescriptionEnCaption.TabIndex = 3;
            lblDescriptionEnCaption.Text = "lblDescriptionEnCaption";
            // 
            // txtLabelCsValue
            // 
            txtLabelCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCsValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtLabelCsValue.ForeColor = System.Drawing.Color.Black;
            txtLabelCsValue.Location = new System.Drawing.Point(150, 0);
            txtLabelCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCsValue.Name = "txtLabelCsValue";
            txtLabelCsValue.Size = new System.Drawing.Size(340, 16);
            txtLabelCsValue.TabIndex = 4;
            // 
            // lblVersion
            // 
            lblVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            lblVersion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblVersion.ForeColor = System.Drawing.Color.MediumBlue;
            lblVersion.Location = new System.Drawing.Point(0, 400);
            lblVersion.Margin = new System.Windows.Forms.Padding(0);
            lblVersion.Name = "lblVersion";
            lblVersion.Size = new System.Drawing.Size(150, 31);
            lblVersion.TabIndex = 10;
            lblVersion.Text = "lblVersion";
            lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboVersion
            // 
            cboVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            cboVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboVersion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            cboVersion.ForeColor = System.Drawing.Color.Black;
            cboVersion.FormattingEnabled = true;
            cboVersion.Location = new System.Drawing.Point(153, 403);
            cboVersion.Name = "cboVersion";
            cboVersion.Size = new System.Drawing.Size(334, 23);
            cboVersion.TabIndex = 12;
            cboVersion.SelectedIndexChanged += CboVersion_SelectedIndexChanged;
            // 
            // lblUnitOfMeasure
            // 
            lblUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            lblUnitOfMeasure.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblUnitOfMeasure.ForeColor = System.Drawing.Color.MediumBlue;
            lblUnitOfMeasure.Location = new System.Drawing.Point(0, 370);
            lblUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            lblUnitOfMeasure.Name = "lblUnitOfMeasure";
            lblUnitOfMeasure.Size = new System.Drawing.Size(150, 30);
            lblUnitOfMeasure.TabIndex = 14;
            lblUnitOfMeasure.Text = "lblUnitOfMeasure";
            lblUnitOfMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpLDsityContributions
            // 
            grpLDsityContributions.Controls.Add(pnlLDsityContributions);
            grpLDsityContributions.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLDsityContributions.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpLDsityContributions.ForeColor = System.Drawing.Color.MediumBlue;
            grpLDsityContributions.Location = new System.Drawing.Point(0, 0);
            grpLDsityContributions.Margin = new System.Windows.Forms.Padding(0);
            grpLDsityContributions.Name = "grpLDsityContributions";
            grpLDsityContributions.Padding = new System.Windows.Forms.Padding(5);
            grpLDsityContributions.Size = new System.Drawing.Size(440, 461);
            grpLDsityContributions.TabIndex = 2;
            grpLDsityContributions.TabStop = false;
            grpLDsityContributions.Text = "grpLDsityContributions";
            // 
            // pnlLDsityContributions
            // 
            pnlLDsityContributions.AutoScroll = true;
            pnlLDsityContributions.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLDsityContributions.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            pnlLDsityContributions.ForeColor = System.Drawing.Color.Black;
            pnlLDsityContributions.Location = new System.Drawing.Point(5, 25);
            pnlLDsityContributions.Margin = new System.Windows.Forms.Padding(0);
            pnlLDsityContributions.Name = "pnlLDsityContributions";
            pnlLDsityContributions.Size = new System.Drawing.Size(430, 431);
            pnlLDsityContributions.TabIndex = 7;
            pnlLDsityContributions.Resize += PnlLDsityContributions_Resize;
            // 
            // FormTargetVariableDivisionNew
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(pnlMain);
            Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormTargetVariableDivisionNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormTargetVariableDivisionNew";
            FormClosing += FormTargetVariableDivisionNew_FormClosing;
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            grpTargetVariable.ResumeLayout(false);
            pnlTargetVariable.ResumeLayout(false);
            tlpTargetVariable.ResumeLayout(false);
            tlpTargetVariable.PerformLayout();
            grpLDsityContributions.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.GroupBox grpTargetVariable;
        private System.Windows.Forms.Panel pnlTargetVariable;
        private System.Windows.Forms.TableLayoutPanel tlpTargetVariable;
        private System.Windows.Forms.ComboBox cboUnitOfMeasure;
        private System.Windows.Forms.TextBox txtDescriptionEnValue;
        private System.Windows.Forms.TextBox txtLabelEnValue;
        private System.Windows.Forms.TextBox txtDescriptionCsValue;
        private System.Windows.Forms.Label lblLabelCsCaption;
        private System.Windows.Forms.Label lblDescriptionCsCaption;
        private System.Windows.Forms.Label lblLabelEnCaption;
        private System.Windows.Forms.Label lblDescriptionEnCaption;
        private System.Windows.Forms.TextBox txtLabelCsValue;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.ComboBox cboVersion;
        private System.Windows.Forms.Label lblUnitOfMeasure;
        private System.Windows.Forms.GroupBox grpLDsityContributions;
        private System.Windows.Forms.Panel pnlLDsityContributions;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
    }

}