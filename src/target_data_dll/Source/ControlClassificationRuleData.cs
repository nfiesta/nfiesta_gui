﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek - data pro klasifikační pravidla - třída pro Designer</para>
    /// <para lang="en">Control - data for classification rules - class for Designer</para>
    /// </summary>
    internal partial class ControlClassificationRuleDataDesign
        : UserControl
    {
        protected ControlClassificationRuleDataDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.ToolStripButton BtnValidateClassificationRule => btnValidateClassificationRule;
        protected System.Windows.Forms.ToolStripButton BtnValidateClassificationRuleClose => btnValidateClassificationRuleClose;
        protected System.Windows.Forms.ToolStripButton BtnValidateClassificationRuleSet => btnValidateClassificationRuleSet;
        protected System.Windows.Forms.ToolStripButton BtnValidateClassificationRuleSetClose => btnValidateClassificationRuleSetClose;
        protected System.Windows.Forms.ToolStripButton BtnValidateSyntax => btnValidateSyntax;
        public System.Windows.Forms.ToolStripButton BtnExportJson => btnExportJson;
        public System.Windows.Forms.ToolStripButton BtnImportJson => btnImportJson;
        protected System.Windows.Forms.GroupBox GrpMain => grpMain;
        protected System.Windows.Forms.GroupBox GrpValidateClassificationRule => grpValidateClassificationRule;
        protected System.Windows.Forms.GroupBox GrpValidateClassificationRuleSet => grpValidateClassificationRuleSet;
        protected System.Windows.Forms.Panel PnlClassificationRuleEditor => pnlClassificationRuleEditor;
        protected System.Windows.Forms.Panel PnlValidateClassificationRule => pnlValidateClassificationRule;
        protected System.Windows.Forms.Panel PnlValidateClassificationRuleSet => pnlValidateClassificationRuleSet;
        protected System.Windows.Forms.ToolStrip TsrMain => tsrMain;
        protected System.Windows.Forms.ToolStrip TsrValidateClassificationRule => tsrValidateClassificationRule;
        protected System.Windows.Forms.ToolStrip TsrValidateClassificationRuleSet => tsrValidateClassificationRuleSet;
        protected System.Windows.Forms.SplitContainer SplData => splData;
        protected System.Windows.Forms.SplitContainer SplMain => splMain;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek - data pro klasifikační pravidla</para>
    /// <para lang="en">Control - data for classification rules</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlClassificationRuleData<TDomain, TCategory>
            : ControlClassificationRuleDataDesign, INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Stav zobrazení dat klasifikačních pravidel</para>
        /// <para lang="en">Classification rule data display status</para>
        /// </summary>
        private ClassificationRuleDataDisplayStatus displayStatus;

        private string msgClassificationRuleInvalidSyntax = String.Empty;
        private string colClassificationResult = String.Empty;
        private string colNumberOfObjects = String.Empty;
        private string rowObjectClassified = String.Empty;
        private string rowObjectUnClassified = String.Empty;
        private string rowRuleNotApplied = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Editor klasifikačních pravidel</para>
        /// <para lang="en">Editor of the classification rules</para>
        /// </summary>
        private ControlClassificationRuleEditor<TDomain, TCategory>
            ctrClassificationRuleEditor;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna textu klasifikačního pravidla"</para>
        /// <para lang="en">Event of the
        /// "Classification rule text change"</para>
        /// </summary>
        public event EventHandler ClassificationRuleTextChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlClassificationRuleData(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlClassificationRuleSwitch<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlClassificationRuleSwitch<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlClassificationRuleSwitch<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlClassificationRuleSwitch<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>s
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }


        /// <summary>
        /// <para lang="cs">Základní formulář</para>
        /// <para lang="en">Base form</para>
        /// </summary>
        public IArealOrPopulationForm<TDomain, TCategory> Base
        {
            get
            {
                return ((ControlClassificationRuleSwitch<TDomain, TCategory>)ControlOwner)
                    .Base;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané klasifikační pravidlo(read-only)</para>
        /// <para lang="en">Selected classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> ClassificationRule
        {
            get
            {
                return
                    ((ControlClassificationRuleSwitch<TDomain, TCategory>)ControlOwner)
                    .ClassificationRule;
            }
        }

        /// <summary>
        /// <para lang="cs">Kladné (true) nebo záporné (false) klasifikační pravidlo je zobrazeno</para>
        /// <para lang="en">Positive (true) or negative (false) classification rule is displayed</para>
        /// </summary>
        public bool Positive
        {
            get
            {
                return
                    ((ControlClassificationRuleSwitch<TDomain, TCategory>)ControlOwner)
                    .Positive;
            }
        }

        /// <summary>
        /// <para lang="cs">Stav zobrazení dat klasifikačních pravidel</para>
        /// <para lang="en">Classification rule data display status</para>
        /// </summary>
        private ClassificationRuleDataDisplayStatus DisplayStatus
        {
            get
            {
                return displayStatus;
            }
            set
            {
                displayStatus = value;
                switch (DisplayStatus)
                {
                    case ClassificationRuleDataDisplayStatus.ClassificationRuleEditor:
                        SplMain.Panel1Collapsed = false;
                        SplMain.Panel2Collapsed = true;
                        SplData.Panel1Collapsed = false;
                        SplData.Panel2Collapsed = true;
                        break;

                    case ClassificationRuleDataDisplayStatus.ObjectsClassifiedByRule:
                        SplMain.Panel1Collapsed = false;
                        SplMain.Panel2Collapsed = false;
                        SplData.Panel1Collapsed = false;
                        SplData.Panel2Collapsed = true;
                        break;

                    case ClassificationRuleDataDisplayStatus.PanelsAndReferenceYearSets:
                        SplMain.Panel1Collapsed = false;
                        SplMain.Panel2Collapsed = false;
                        SplData.Panel1Collapsed = true;
                        SplData.Panel2Collapsed = false;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(
                        message: $"Unknown value of the {nameof(DisplayStatus)}.",
                        paramName: nameof(DisplayStatus));
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnValidateClassificationRule),          "Validovat vybrané klasifikační pravidlo" },
                            { nameof(BtnValidateClassificationRuleClose),     "Skrýt počet objektů klasifikovaných pravidlem" },
                            { nameof(BtnValidateClassificationRuleSet),       "Validovat sadu klasifikačních pravidel" },
                            { nameof(BtnValidateClassificationRuleSetClose),  "Skrýt panely a roky měření" },
                            { nameof(BtnValidateSyntax),                      "Zkontrolovat syntaxi vybraného klasifikačního pravidla" },
                            { nameof(BtnExportJson),                          "Export klasifikačních pravidel plošné domény do JSON souboru." },
                            { nameof(BtnImportJson),                          "Import klasifikačních pravidel plošné domény z JSON souboru." },
                            { nameof(GrpMain),                                "Klasifikační pravidlo" },
                            { nameof(GrpValidateClassificationRule),          "Počet objektů klasifikovaných pravidlem" },
                            { nameof(GrpValidateClassificationRuleSet),       "Panely a roky měření" },
                            { nameof(colClassificationResult),                "výsledek třídění" },
                            { nameof(colNumberOfObjects),                     "počet objektů" },
                            { nameof(rowObjectClassified),                    "objekt zařazen" },
                            { nameof(rowObjectUnClassified),                  "objekt nezařazen" },
                            { nameof(rowRuleNotApplied),                      "pravidlo nelze použít" },
                            { nameof(TsrMain),                                String.Empty },
                            { nameof(TsrValidateClassificationRule),          String.Empty },
                            { nameof(TsrValidateClassificationRuleSet),       String.Empty },
                            { nameof(msgClassificationRuleInvalidSyntax),     "Klasifikační pravidlo nemá validní syntaxi." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleData<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnValidateClassificationRule),          "Validovat vybrané klasifikační pravidlo" },
                            { nameof(BtnValidateClassificationRuleClose),     "Skrýt počet objektů klasifikovaných pravidlem" },
                            { nameof(BtnValidateClassificationRuleSet),       "Validovat sadu klasifikačních pravidel" },
                            { nameof(BtnValidateClassificationRuleSetClose),  "Skrýt panely a roky měření" },
                            { nameof(BtnValidateSyntax),                      "Zkontrolovat syntaxi vybraného klasifikačního pravidla" },
                            { nameof(BtnExportJson),                          "Export klasifikačních pravidel subpopulace do JSON souboru." },
                            { nameof(BtnImportJson),                          "Import klasifikačních pravidel subpopulace z JSON souboru." },
                            { nameof(GrpMain),                                "Klasifikační pravidlo" },
                            { nameof(GrpValidateClassificationRule),          "Počet objektů klasifikovaných pravidlem" },
                            { nameof(GrpValidateClassificationRuleSet),       "Panely a roky měření" },
                            { nameof(colClassificationResult),                "výsledek třídění" },
                            { nameof(colNumberOfObjects),                     "počet objektů" },
                            { nameof(rowObjectClassified),                    "objekt zařazen" },
                            { nameof(rowObjectUnClassified),                  "objekt nezařazen" },
                            { nameof(rowRuleNotApplied),                      "pravidlo nelze použít" },
                            { nameof(TsrMain),                                String.Empty },
                            { nameof(TsrValidateClassificationRule),          String.Empty },
                            { nameof(TsrValidateClassificationRuleSet),       String.Empty },
                            { nameof(msgClassificationRuleInvalidSyntax),     "Klasifikační pravidlo nemá validní syntaxi." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleData<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictNationalSubPopulation)
                                ? dictNationalSubPopulation
                                : [],
                    _ => [],
                },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnValidateClassificationRule),          "Validate the selected classification rule" },
                            { nameof(BtnValidateClassificationRuleClose),     "Hide number of objects classified by the rule" },
                            { nameof(BtnValidateClassificationRuleSet),       "Validate the set of classification rules" },
                            { nameof(BtnValidateClassificationRuleSetClose),  "Hide panels and reference year sets" },
                            { nameof(BtnValidateSyntax),                      "Check the syntax of the selected classification rule" },
                            { nameof(BtnExportJson),                          "Export area domain classification rules into JSON file." },
                            { nameof(BtnImportJson),                          "Import area domain classification rules from JSON file." },
                            { nameof(GrpMain),                                "Classification rule" },
                            { nameof(GrpValidateClassificationRule),          "Number of objects classified by the rule" },
                            { nameof(GrpValidateClassificationRuleSet),       "Panels and reference year sets" },
                            { nameof(colClassificationResult),                "classification result" },
                            { nameof(colNumberOfObjects),                     "number of objects" },
                            { nameof(rowObjectClassified),                    "object classified" },
                            { nameof(rowObjectUnClassified),                  "object unclassified" },
                            { nameof(rowRuleNotApplied),                      "rule cannot be applied" },
                            { nameof(TsrMain),                                String.Empty },
                            { nameof(TsrValidateClassificationRule),          String.Empty },
                            { nameof(TsrValidateClassificationRuleSet),       String.Empty },
                            { nameof(msgClassificationRuleInvalidSyntax),     "Syntax of the classification rule is not valid." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleData<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnValidateClassificationRule),          "Validate the selected classification rule" },
                            { nameof(BtnValidateClassificationRuleClose),     "Hide number of objects classified by the rule" },
                            { nameof(BtnValidateClassificationRuleSet),       "Validate the set of classification rules" },
                            { nameof(BtnValidateClassificationRuleSetClose),  "Hide panels and reference year sets" },
                            { nameof(BtnValidateSyntax),                      "Check the syntax of the selected classification rule" },
                            { nameof(BtnExportJson),                          "Export subpopulation classification rules into JSON file." },
                            { nameof(BtnImportJson),                          "Import subpopulation classification rules from JSON file." },
                            { nameof(GrpMain),                                "Classification rule" },
                            { nameof(GrpValidateClassificationRule),          "Number of objects classified by the rule" },
                            { nameof(GrpValidateClassificationRuleSet),       "Panels and reference year sets" },
                            { nameof(colClassificationResult),                "classification result" },
                            { nameof(colNumberOfObjects),                     "number of objects" },
                            { nameof(rowObjectClassified),                    "object classified" },
                            { nameof(rowObjectUnClassified),                  "object unclassified" },
                            { nameof(rowRuleNotApplied),                      "rule cannot be applied" },
                            { nameof(TsrMain),                                String.Empty },
                            { nameof(TsrValidateClassificationRule),          String.Empty },
                            { nameof(TsrValidateClassificationRuleSet),       String.Empty },
                            { nameof(msgClassificationRuleInvalidSyntax),     "Syntax of the classification rule is not valid." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleData<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BackColor = System.Drawing.SystemColors.Control;

            ControlOwner = controlOwner;

            DisplayStatus = ClassificationRuleDataDisplayStatus.ClassificationRuleEditor;

            LoadContent();

            InitializeControls();

            InitializeToolStripIcons();

            InitializeLabels();

            Reset();

            BtnValidateClassificationRule.Click += new EventHandler(
                (sender, e) => { ValidateClassificationRule(); });

            BtnValidateClassificationRuleSet.Click += new EventHandler(
                (sender, e) => { ValidateClassificationRuleSet(); });

            BtnValidateClassificationRuleClose.Click += new EventHandler(
                (sender, e) => { DisplayStatus = ClassificationRuleDataDisplayStatus.ClassificationRuleEditor; });

            BtnValidateClassificationRuleSetClose.Click += new EventHandler(
                (sender, e) => { DisplayStatus = ClassificationRuleDataDisplayStatus.ClassificationRuleEditor; });

            BtnValidateSyntax.Click += new EventHandler(
                (sender, e) => { ValidateSyntax(verbose: true); });

            BtnExportJson.Click += new EventHandler(
                (sender, e) => { ExportJSON(); });

            BtnImportJson.Click += new EventHandler(
                (sender, e) => { ImportJSON(); });
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacích prvků</para>
        /// <para lang="en">Initializing user controls</para>
        /// </summary>
        private void InitializeControls()
        {
            // ControlClassificationRuleEditor
            PnlClassificationRuleEditor.Controls.Clear();
            ctrClassificationRuleEditor =
                new ControlClassificationRuleEditor<TDomain, TCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrClassificationRuleEditor.ClassificationRuleTextChanged +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ClassificationRuleTextChanged?.Invoke(
                            sender: sender, e: e);
                    });
            PnlClassificationRuleEditor.Controls.Add(
                value: ctrClassificationRuleEditor);
        }

        /// <summary>
        /// <para lang="cs">Inicializace ikon tlačítek</para>
        /// <para lang="en">Initializing the button icons</para>
        /// </summary>
        private void InitializeToolStripIcons()
        {
            ControlResources resources = new();

            BtnValidateClassificationRule.Image = resources.IconValidateBlack.Image;
            BtnValidateClassificationRule.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnValidateClassificationRuleClose.Image = resources.IconRedCross.Image;
            BtnValidateClassificationRuleClose.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnValidateClassificationRuleSet.Image = resources.IconValidateSet.Image;
            BtnValidateClassificationRuleSet.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnValidateClassificationRuleSetClose.Image = resources.IconRedCross.Image;
            BtnValidateClassificationRuleSetClose.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnValidateSyntax.Image = resources.IconValidateGreen.Image;
            BtnValidateSyntax.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnExportJson.Image = resources.IconExport.Image;
            BtnExportJson.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnImportJson.Image = resources.IconImport.Image;
            BtnImportJson.DisplayStyle = ToolStripItemDisplayStyle.Image;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing user control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            BtnValidateClassificationRule.Text =
                labels.TryGetValue(key: nameof(BtnValidateClassificationRule),
                out string btnValidateClassificationRuleText)
                    ? btnValidateClassificationRuleText
                    : String.Empty;

            BtnValidateClassificationRuleClose.Text =
                labels.TryGetValue(key: nameof(BtnValidateClassificationRuleClose),
                out string btnValidateClassificationRuleCloseText)
                    ? btnValidateClassificationRuleCloseText
                    : String.Empty;

            BtnValidateClassificationRuleSet.Text =
                labels.TryGetValue(key: nameof(BtnValidateClassificationRuleSet),
                out string btnValidateClassificationRuleSetText)
                    ? btnValidateClassificationRuleSetText
                    : String.Empty;

            BtnValidateClassificationRuleSetClose.Text =
                labels.TryGetValue(key: nameof(BtnValidateClassificationRuleSetClose),
                out string btnValidateClassificationRuleSetClose)
                    ? btnValidateClassificationRuleSetClose
                    : String.Empty;

            BtnValidateSyntax.Text =
                labels.TryGetValue(key: nameof(BtnValidateSyntax),
                out string btnValidateSyntaxText)
                    ? btnValidateSyntaxText
                    : String.Empty;

            BtnExportJson.Text =
                labels.TryGetValue(key: nameof(BtnExportJson),
                out string btnExportJsonText)
                    ? btnExportJsonText
                    : String.Empty;

            BtnImportJson.Text =
                labels.TryGetValue(key: nameof(BtnImportJson),
                out string btnImportJsonText)
                    ? btnImportJsonText
                    : String.Empty;

            GrpMain.Text =
               labels.TryGetValue(key: nameof(GrpMain),
               out string grpMainText)
                   ? grpMainText
                   : String.Empty;

            GrpValidateClassificationRule.Text =
              labels.TryGetValue(key: nameof(GrpValidateClassificationRule),
              out string grpValidateClassificationRuleText)
                  ? grpValidateClassificationRuleText
                  : String.Empty;

            GrpValidateClassificationRuleSet.Text =
              labels.TryGetValue(key: nameof(GrpValidateClassificationRuleSet),
              out string grpValidateClassificationRuleSetText)
                  ? grpValidateClassificationRuleSetText
                  : String.Empty;

            TsrMain.Text =
              labels.TryGetValue(key: nameof(TsrMain),
              out string tsrMainText)
                  ? tsrMainText
                  : String.Empty;

            TsrValidateClassificationRule.Text =
              labels.TryGetValue(key: nameof(TsrValidateClassificationRule),
              out string tsrValidateClassificationRuleText)
                  ? tsrValidateClassificationRuleText
                  : String.Empty;

            TsrValidateClassificationRuleSet.Text =
              labels.TryGetValue(key: nameof(TsrValidateClassificationRuleSet),
              out string tsrValidateClassificationRuleSetText)
                  ? tsrValidateClassificationRuleSetText
                  : String.Empty;

            msgClassificationRuleInvalidSyntax =
                labels.TryGetValue(key: nameof(msgClassificationRuleInvalidSyntax),
                        out msgClassificationRuleInvalidSyntax)
                            ? msgClassificationRuleInvalidSyntax
                            : String.Empty;

            colClassificationResult =
                labels.TryGetValue(key: nameof(colClassificationResult),
                        out colClassificationResult)
                            ? colClassificationResult
                            : String.Empty;

            colNumberOfObjects =
                labels.TryGetValue(key: nameof(colNumberOfObjects),
                        out colNumberOfObjects)
                            ? colNumberOfObjects
                            : String.Empty;

            rowObjectClassified =
                labels.TryGetValue(key: nameof(rowObjectClassified),
                        out rowObjectClassified)
                            ? rowObjectClassified
                            : String.Empty;

            rowObjectUnClassified =
                labels.TryGetValue(key: nameof(rowObjectUnClassified),
                        out rowObjectUnClassified)
                            ? rowObjectUnClassified
                            : String.Empty;

            rowRuleNotApplied =
                labels.TryGetValue(key: nameof(rowRuleNotApplied),
                        out rowRuleNotApplied)
                            ? rowRuleNotApplied
                            : String.Empty;

            ctrClassificationRuleEditor?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Kontroluje syntaxi vybraného klasifikačního pravidla</para>
        /// <para lang="en">Method checks the syntax of the selected classification rule</para>
        /// </summary>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazení hlášení o validitě klasifikačního pravidla</para>
        /// <para lang="en">To display a classification rule validity report</para>
        /// </param>
        private void ValidateSyntax(bool verbose)
        {
            if (ClassificationRule == null)
            {
                return;
            }

            ClassificationRule.ValidateSyntax(
                verbose: verbose);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí počet objektů klasifikovaných pravidlem</para>
        /// <para lang="en">Method displays number of objects classified by the rule</para>
        /// </summary>
        private void ValidateClassificationRule()
        {
            DisplayStatus = ClassificationRuleDataDisplayStatus.ClassificationRuleEditor;

            if (ClassificationRule == null)
            {
                return;
            }

            ValidateSyntax(verbose: false);

            if (!ClassificationRule.IsValid)
            {
                MessageBox.Show(
                    text: msgClassificationRuleInvalidSyntax,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            // Příspěvek lokální hustoty pro vybraný objekt lokální hustoty
            // Pro každný objekt lokální hustoty by měl existovat právě jeden příspěvek lokální hustoty
            // Local density contribution for selected local density object
            // For each local density object should exist only and just one local density contribution
            List<TDLDsity> ldsities =
                Base.SelectedLDsities.Items
                    .Where(a => a.LDsityObjectId == ClassificationRule.LDsityObject.Id)
                    .Where(a => a.UseNegative == !Positive)
                    .ToList<TDLDsity>();
            TDLDsity ldsity = ldsities.FirstOrDefault();

            if (!Base.LDsityCountIsValid(ldsities: ldsities))
            {
                return;
            }

            List<List<Nullable<int>>> paramADC =
                ((IArealOrPopulationForm<TDomain, TCategory>)
                ((ControlClassificationRuleSwitch<TDomain, TCategory>)ControlOwner).ControlOwner).
                PrepareParamADC(
                    ldsityObjectForADSP: ClassificationRule.LDsityObjectForADSP);

            List<List<Nullable<int>>> paramSPC =
                ((IArealOrPopulationForm<TDomain, TCategory>)
                ((ControlClassificationRuleSwitch<TDomain, TCategory>)ControlOwner).ControlOwner).
                PrepareParamSPC(
                    ldsityObjectForADSP: ClassificationRule.LDsityObjectForADSP);

            // fn_check_classification_rule
            TDFnCheckClassificationRuleTypeList result =
                TDFunctions.FnCheckClassificationRule.Execute(
                    database: Database,
                    ldsityId: ldsity.Id,
                    ldsityObjectId: ClassificationRule.LDsityObjectForADSP.Id,
                    rule: ClassificationRule.Text,
                    panelRefYearSetId: null,
                    adc: paramADC,
                    spc: paramSPC,
                    useNegative: !Positive);

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnCheckClassificationRule.CommandText,
                    caption: TDFunctions.FnCheckClassificationRule.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            TDFnCheckClassificationRuleTypeList.SetColumnsVisibility(
                TDFnCheckClassificationRuleTypeList.Cols
                    .Where(a => !a.Value.PKey)
                    .OrderBy(a => a.Value.DisplayIndex)
                    .Select(a => a.Value.Name)
                    .ToArray<string>());

            Dictionary<string, ColumnMetadata> metadata = new()
            {
                { nameof(colClassificationResult), new ColumnMetadata()
                {
                    Name = nameof(colClassificationResult),
                    DbName = nameof(colClassificationResult),
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = true,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = ZaJi.PostgreSQL.DataModel.DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = colClassificationResult,
                    HeaderTextEn = colClassificationResult,
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 200,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { nameof(colNumberOfObjects), new ColumnMetadata()
                {
                    Name = nameof(colNumberOfObjects),
                    DbName = nameof(colNumberOfObjects),
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = true,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = ZaJi.PostgreSQL.DataModel.DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = colNumberOfObjects,
                    HeaderTextEn = colNumberOfObjects,
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 200,
                    Elemental = true,
                    DisplayIndex = 1
                }
                }
            };

            DataTable newDataTable = new();
            newDataTable.Columns.Add(
                columnName: metadata[nameof(colClassificationResult)].Name,
                type: Type.GetType(typeName: metadata[nameof(colClassificationResult)].DataType));
            newDataTable.Columns.Add(
                columnName: metadata[nameof(colNumberOfObjects)].Name,
                type: Type.GetType(typeName: metadata[nameof(colNumberOfObjects)].DataType));

            foreach (DataRow row in result.Data.Rows)
            {
                DataRow newRow = newDataTable.NewRow();

                Nullable<bool> complianceStatus =
                    Functions.GetNBoolArg(
                        row: row,
                        name: TDFnCheckClassificationRuleTypeList.ColComplianceStatus.Name,
                        defaultValue: null);

                Nullable<int> numberOfObjects =
                    Functions.GetNIntArg(
                        row: row,
                        name: TDFnCheckClassificationRuleTypeList.ColNumberOfObjects.Name,
                        defaultValue: null);

                if (complianceStatus == null)
                {
                    Functions.SetStringArg(
                        row: newRow,
                        name: metadata[nameof(colClassificationResult)].Name,
                        val: rowRuleNotApplied);
                }
                else if ((bool)complianceStatus)
                {
                    Functions.SetStringArg(
                        row: newRow,
                        name: metadata[nameof(colClassificationResult)].Name,
                        val: rowObjectClassified);
                }
                else
                {
                    Functions.SetStringArg(
                        row: newRow,
                        name: metadata[nameof(colClassificationResult)].Name,
                        val: rowObjectUnClassified);
                }

                Functions.SetNIntArg(
                    row: newRow,
                    name: metadata[nameof(colNumberOfObjects)].Name,
                    val: numberOfObjects);

                newDataTable.Rows.Add(row: newRow);
            }

            // Vytvoření DataGridView pro zobrazení výsledku
            // Creates DataGridView for display result
            PnlValidateClassificationRule.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (sender, e) => { dgvData.ClearSelection(); };
            PnlValidateClassificationRule.Controls.Add(value: dgvData);

            ADatabaseTable.SetDataGridViewDataSource(
                data: newDataTable,
                dataGridView: dgvData);

            ADatabaseTable.FormatDataGridView(
                columns: metadata,
                dataGridView: dgvData,
                languageVersion: LanguageVersion);

            DisplayStatus = ClassificationRuleDataDisplayStatus.ObjectsClassifiedByRule;
        }

        /// <summary>
        /// <para lang="cs">Validuje celou sadu klasifikačních pravidel</para>
        /// <para lang="en">Method validates whole set of classification rules</para>
        /// </summary>
        private void ValidateClassificationRuleSet()
        {
            DisplayStatus = ClassificationRuleDataDisplayStatus.ClassificationRuleEditor;

            if (ClassificationRule == null)
            {
                return;
            }

            TDVwClassificationRuleCheckList vwClassificationRuleChecks =
                Base.GetClassificationRuleCheckList(
                    classificationRule: ClassificationRule,
                    useNegative: !Positive);

            if (vwClassificationRuleChecks == null)
            {
                return;
            }

            TDVwClassificationRuleCheckList.SetColumnsVisibility(
                     TDVwClassificationRuleCheckList.ColId.Name,
                     TDVwClassificationRuleCheckList.ColPanelCode.Name,
                     TDVwClassificationRuleCheckList.ColRefYearSetCode.Name,
                     TDVwClassificationRuleCheckList.ColResult.Name,
                     TDVwClassificationRuleCheckList.ColMessageId.Name,
                     TDVwClassificationRuleCheckList.ColMessage.Name);

            // Vytvoření DataGridView pro zobrazení výsledku
            // Creates DataGridView for display result
            PnlValidateClassificationRuleSet.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (sender, e) => { dgvData.ClearSelection(); };
            PnlValidateClassificationRuleSet.Controls.Add(value: dgvData);

            vwClassificationRuleChecks.SetDataGridViewDataSource(dataGridView: dgvData);
            vwClassificationRuleChecks.FormatDataGridView(dataGridView: dgvData);

            DisplayStatus = ClassificationRuleDataDisplayStatus.PanelsAndReferenceYearSets;
        }

        /// <summary>
        /// <para lang="cs">Export klasifikačních pravidel plošné domény nebo subpopulace do JSON souboru</para>
        /// <para lang="en">Export area domain or subpopulation classification rules into JSON file</para>
        /// </summary>
        private void ExportJSON()
        {
            TDomain selectedDomain = Base.Domain;

            FormCategoryNew<TDomain, TCategory> formCategoryNew = Base as FormCategoryNew<TDomain, TCategory>;

            List<TCategory> domainCategories = formCategoryNew.Categories;

            TDClassificationTypeEnum classificationType = selectedDomain.ClassificationTypeValue;

            List<TDLDsityObject> lDsityObjectsForADSP =
                formCategoryNew?.LDsityObjectsForADSP
                ?? new List<TDLDsityObject>();

            DataTable dtClassificationRules =
                ClassificationRuleCollection<TDomain, TCategory>.EmptyDataTable();

            if (formCategoryNew.ClassificationRulePairs != null)
            {
                foreach (ClassificationRulePair<TDomain, TCategory> crp in formCategoryNew.ClassificationRulePairs)
                {
                    DataRow row = dtClassificationRules.NewRow();

                    row["Category"] = crp.Category.Id;
                    row["LDsityObjectADSP"] = crp.LDsityObjectForADSP?.Id ?? 0;

                    string positiveText = crp.Positive?.Text;
                    row["PositiveRule"] =
                        string.IsNullOrEmpty(positiveText) ? null : positiveText;
                    if (classificationType == TDClassificationTypeEnum.ChangeOrDynamic)
                    {
                        string negativeText = crp.Negative?.Text;
                        row["NegativeRule"] =
                            string.IsNullOrEmpty(negativeText) ? null : negativeText;
                    }
                    else
                    {
                        row["NegativeRule"] = null;
                    }

                    dtClassificationRules.Rows.Add(row);
                }
            }

            ClassificationRuleCollection<TDomain, TCategory> collection =
                new ClassificationRuleCollection<TDomain, TCategory>(
                    domain: selectedDomain,
                    categories: domainCategories,
                    ldsityObjectsForADSP: lDsityObjectsForADSP,
                    dtClassificationRules: dtClassificationRules
                );

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.DefaultExt = "json";
                sfd.Filter = "Json files (*.json)|*.json";
                sfd.FileName = $"{selectedDomain.Id}.json";
                sfd.RestoreDirectory = true;
                sfd.Title = labels.TryGetValue(
                                key: nameof(BtnExportJson),
                                out string BtnExportJsonText)
                                ? BtnExportJsonText
                                : String.Empty;

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    string path = Path.GetFullPath(sfd.FileName);
                    collection.SaveToFile(path);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Import klasifikačních pravidel plošné domény nebo subpopulace z JSON souboru</para>
        /// <para lang="en">Import area domain or subpopulation classification rules from JSON file</para>
        /// </summary>
        private void ImportJSON()
        {
            Dictionary<string, string> labels = Dictionary(LanguageVersion, LanguageFile);

            OpenFileDialog openFileDialog = new()
            {
                DefaultExt = "json",
                Filter = "Json files (*.json)|*.json",
                InitialDirectory = (FormClassificationRule<TDomain, TCategory>.ArealOrPopulation
                                    == TDArealOrPopulationEnum.AreaDomain)
                                        ? Setting.AreaDomainJSONFolder
                                        : Setting.SubPopulationJSONFolder,
                RestoreDirectory = true,
                Title = labels.TryGetValue(nameof(BtnImportJson), out string btnImportJsonText)
                        ? btnImportJsonText
                        : "Import classification rules from JSON"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = Path.GetFullPath(openFileDialog.FileName);

                // Uložíme cestu do nastavení
                // Saving the path to the setting
                if (FormClassificationRule<TDomain, TCategory>.ArealOrPopulation == TDArealOrPopulationEnum.AreaDomain)
                    Setting.AreaDomainJSONFolder = Path.GetDirectoryName(path);
                else
                    Setting.SubPopulationJSONFolder = Path.GetDirectoryName(path);

                // 1) Načteme JSON
                // 1) Loading JSON
                ClassificationRuleCollection<TDomain, TCategory> collection =
                    ClassificationRuleCollection<TDomain, TCategory>.LoadFromFile(path);
                if (collection == null)
                {
                    MessageBox.Show("JSON file is empty or invalid.");
                    return;
                }

                // 2) Získáme rodičovský formulář a local density object for classification, který uživatel vybral
                // 2) Obtaining the parent form and the local density object for classification selected by a user
                var formClassificationRule = (FormClassificationRule<TDomain, TCategory>)this.ParentForm;
                if (formClassificationRule == null)
                {
                    MessageBox.Show("Parent form is not FormClassificationRule.");
                    return;
                }

                // Vybraný local density object for classification, na který chceme navázat JSON
                // Selected local density object for classification, to which we want to bind JSON
                TDLDsityObject selectedLdsityObjectForADSP = formClassificationRule.LDsityObjectForADSP;
                if (selectedLdsityObjectForADSP == null)
                {
                    MessageBox.Show("No LocalDensityObjectForADSP selected.");
                    return;
                }

                // 3) Otevřeme FormDomainFromJson v režimu SingleRowMode
                // 3) Opening FormDomainFromJson in the SingleRowMode mode
                FormDomainFromJson<TDomain, TCategory> frmPairing = new(controlOwner: this)
                {
                    // Předáme JSON
                    // Passing JSON
                    Collection = collection,

                    SingleRowMode = true,

                    LDsityObjects = new List<TDLDsityObject> { selectedLdsityObjectForADSP }
                };

                if (frmPairing.ShowDialog() == DialogResult.OK)
                {
                    // 4) Získáme Pairing
                    // 4) Obtaining Pairing
                    var pairing = frmPairing.Pairing;
                    if (pairing == null || pairing.IsEmpty)
                    {
                        // Uživatel nic nenamapoval
                        // A user did not map anything
                        return;
                    }

                    // 5) Sloučíme vybraná pravidla do paměti
                    // 5) Merging selected rules in the memory
                    MergeClassificationRulesFromJson(collection, pairing);

                    RefreshUIAfterImport();
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Projde JSON kategoriemi, pro každou kategorii a Local Density Object z JSON zjistí, zda je v `pairing` namapovaný reálný Local Density Object (mappedLDsityObject). Pokud ano, založí/aktualizuje ClassificationRulePair.</para>
        /// <para lang="en">It will go through the JSON categories, for each category and Local Density Object from the JSON to see if there is a real Local Density Object (mappedLDsityObject) mapped in the `pairing`. If so, it will create/update a ClassificationRulePair.</para>
        /// </summary>
        /// <param name="collection">
        /// <para lang="cs">Kolekce pravidel klasifikace, která obsahuje kategorie a Local Density Objects definované v JSON.</para>
        /// <para lang="en">A collection of classification rules containing categories and Local Density Objects defined in JSON.</para>
        /// </param>
        /// <param name="pairing">
        /// <para lang="cs">Objekt, který zastupuje mapování mezi JSON Local Density Objects a reálnými Local Density Objects v databázi.</para>
        /// <para lang="en">An object representing the mapping between JSON Local Density Objects and actual Local Density Objects in the database.</para>
        /// </param>
        private void MergeClassificationRulesFromJson(
            ClassificationRuleCollection<TDomain, TCategory> collection,
            ReplacingLocalDensityObject<TDomain, TCategory> pairing
        )
        {
            var formClassificationRule = (FormClassificationRule<TDomain, TCategory>)this.ParentForm;
            if (formClassificationRule == null) return;

            // 1) Procházet JSON kategorie
            // 1) Iterating JSON categories
            foreach (var catJson in collection.Categories)
            {
                // Najdeme reálnou kategorii v DB
                // Finding the actual category in DB
                TCategory dbCategory = FindCategoryInDB(catJson.CategoryId);
                if (dbCategory == null)
                    continue;

                // 2) Projdeme Local Density Objects definované v JSON
                // 2) Iterating Local Density Objects defined in JSON
                foreach (var ldsityObjectJson in catJson.LDsityObjectsForADSP)
                {
                    // Zkusíme v pairing najít reálný Local Density Object v DB
                    // Let's try in pairing to find a real Local Density Object in DB
                    TDLDsityObject mappedLDsityObject = pairing.FindLdoInDbByJson(ldsityObjectJson.LocalDensityObjectId);
                    if (mappedLDsityObject == null)
                    {
                        // Znamená to, že uživatel to buď ignoruje, nebo nespároval ID=ldsityObjectJson.LocalDensityObjectId s ničím v DB
                        // It means that the user either ignores it or did not pair ID=ldsityObjectJson.LocalDensityObjectId with anything in DB
                        continue;
                    }

                    string positiveRule = ldsityObjectJson.PositiveClassificationRule;
                    string negativeRule = ldsityObjectJson.NegativeClassificationRule;

                    // Najdeme existující pár
                    // Finding the existing pair
                    var pair = formClassificationRule.ClassificationRulePairs
                        .FirstOrDefault(p => p.Category.Id == dbCategory.Id
                                          && p.LDsityObjectForADSP.Id == mappedLDsityObject.Id);

                    if (pair == null)
                    {
                        // Vytvoříme nový TreeNode
                        // Creating a new TreeNode
                        TreeNode newNode = new TreeNode
                        {
                            Text = (formClassificationRule.LanguageVersion == LanguageVersion.National)
                               ? dbCategory.ExtendedLabelCs
                               : dbCategory.ExtendedLabelEn
                        };
                        formClassificationRule.TvwCategory.Nodes.Add(newNode);

                        pair = new ClassificationRulePair<TDomain, TCategory>(
                            owner: this,
                            node: newNode,
                            flat: true,
                            classificationType: formClassificationRule.ClassificationType,
                            category: dbCategory,
                            ldsityObject: formClassificationRule.LDsityObject,
                            ldsityObjectForADSP: mappedLDsityObject
                        );
                        formClassificationRule.AddClassificationRulePairToTreeView(pair);
                    }

                    // Naplníme kladné/záporné pravidlo
                    // Filling the positive/negative rule
                    if (!string.IsNullOrEmpty(positiveRule))
                    {
                        pair.Positive.Text = positiveRule;
                    }
                    if (!string.IsNullOrEmpty(negativeRule)
                        && formClassificationRule.ClassificationType == TDClassificationTypeEnum.ChangeOrDynamic)
                    {
                        if (pair.Negative == null)
                        {
                            pair.Negative = new ClassificationRule<TDomain, TCategory>(owner: pair)
                            {
                                Text = negativeRule
                            };
                        }
                        else
                        {
                            pair.Negative.Text = negativeRule;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Najde reálnou kategorii v databázi (CAreaDomainCategory nebo CSubPopulationCategory) podle ID.</para>
        /// <para lang="en">Finds the real category in the database (CAreaDomainCategory or CSubPopulationCategory) by ID.</para>
        /// </summary>
        /// <param name="categoryId">
        /// <para lang="cs">ID kategorie, která má být nalezena.</para>
        /// <para lang="en">The ID of the category to be found.</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací nalezenou kategorii (CAreaDomainCategory nebo CSubPopulationCategory) nebo výchozí hodnotu, pokud kategorie nebyla nalezena.</para>
        /// <para lang="en">Returns the found category (CAreaDomainCategory or CSubPopulationCategory) or the default value if the category was not found.</para>
        /// </returns>
        private TCategory FindCategoryInDB(int categoryId)
        {
            if (FormClassificationRule<TDomain, TCategory>.ArealOrPopulation == TDArealOrPopulationEnum.AreaDomain)
            {
                var catArea = Database.STargetData.CAreaDomainCategory.Items
                    .FirstOrDefault(x => x.Id == categoryId);
                if (catArea == null) return default;
                return (TCategory)(object)catArea; // double cast
            }
            else
            {
                var catSub = Database.STargetData.CSubPopulationCategory.Items
                    .FirstOrDefault(x => x.Id == categoryId);
                if (catSub == null) return default;
                return (TCategory)(object)catSub;
            }
        }

        /// <summary>
        /// <para lang="cs">Refresh UI</para>
        /// <para lang="en">Refresh UI</para>
        /// </summary>
        private void RefreshUIAfterImport()
        {
            var frmClassificationRule = (FormClassificationRule<TDomain, TCategory>)this.ParentForm;
            if (frmClassificationRule != null)
            {
                frmClassificationRule.ColorTreeNodes();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení ovládacího prvku</para>
        /// <para lang="en">Reset control</para>
        /// </summary>
        public void Reset()
        {
            DisplayStatus = ClassificationRuleDataDisplayStatus.ClassificationRuleEditor;
            ctrClassificationRuleEditor?.Reset();
        }

        #endregion Methods

    }

    /// <summary>
    /// <para lang="cs">Stav zobrazení dat klasifikačních pravidel</para>
    /// <para lang="en">Classification rule data display status</para>
    /// </summary>
    internal enum ClassificationRuleDataDisplayStatus
    {
        /// <summary>
        /// <para lang="cs">Zobrazen pouze editor klasifikačních pravidel</para>
        /// <para lang="en">Only the classification rules editor is displayed</para>
        /// </summary>
        ClassificationRuleEditor = 0,

        /// <summary>
        /// <para lang="cs">Zobrazen počet objektů klasifikovaných pravidlem</para>
        /// <para lang="en">Number of objects classified by the rule is displayed</para>
        /// </summary>
        ObjectsClassifiedByRule = 100,

        /// <summary>
        /// <para lang="cs">Zobrazen je seznam panelů a roků měření</para>
        /// <para lang="en">A list of panels and reference year sets is displayed</para>
        /// </summary>
        PanelsAndReferenceYearSets = 200

    }

}