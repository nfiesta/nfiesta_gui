﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Rozhodnutí o třídění subpopulacemi"</para>
    /// <para lang="en">Control "Decision about classification by subpopulations"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTDSelectionTargetVariableDivision
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        public bool onEdit;

        private string msgNone = String.Empty;
        private string msgNoneSelectedTargetVariable = String.Empty;
        private string msgSelectedTargetVariableNotExist = String.Empty;
        private string msgCannotDeleteSelectedTargetVariable = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Informace o příspěvku lokální hustoty typu Core"</para>
        /// <para lang="en">Control "Information about local density contribution Core type"</para>
        /// </summary>
        private ControlTDLDsityInformation ctrInformationCore;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Informace o příspěvku lokální hustoty typu Division"</para>
        /// <para lang="en">Control "Information about local density contribution Division type"</para>
        /// </summary>
        private ControlTDLDsityInformation ctrInformationDivision;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event EventHandler NextClick;

        /// <summary>
        /// <para lang="cs">Událost změny vybrané cílové proměnné</para>
        /// <para lang="en">Event of the selected target variable change</para>
        /// </summary>
        public event EventHandler SelectedTargetVariableChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTDSelectionTargetVariableDivision(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// <para lang="cs">Slovník s texty hlášení (read-only)</para>
        /// <para lang="en">Dictionary with message texts (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraná skupina objektů lokálních hustot (vybraná na 1.formuláři) (read-only)</para>
        /// <para lang="en">Selected group of local density objects (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectGroup SelectedLocalDensityObjectGroup
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionLDsityObjectGroup
                    .SelectedLocalDensityObjectGroup;
            }
        }

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot ve vybrané skupině (vybrané na 1.formuláři) (read-only)</para>
        /// <para lang="en">Local density objects in the selected group (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectList SelectedLocalDensityObjects
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionLDsityObjectGroup
                    .SelectedLocalDensityObjects;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina cílových proměnných (vybraná na 2.formuláři)</para>
        /// <para lang="en">Selected target variable group (selected on the 2nd form)</para>
        /// </summary>
        public TDFnGetTargetVariableType SelectedTargetVariableGroup
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionTargetVariableCore
                    .SelectedTargetVariableGroup;
            }
            set
            {
                ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionTargetVariableCore
                    .SelectedTargetVariableGroup = value;

                SelectClassifyBySubPopulation();
            }
        }


        // Výsledkem výběru v této komponentě je
        // rozhodnutí o třídění subpopulacemi,
        // a cílová proměnná
        // The result of the selection in this component are
        // decision about classification by subpopulations
        // and selected target variable

        /// <summary>
        /// <para lang="cs">Třídit subpopulacemi?</para>
        /// <para lang="en">Classify by subPopulations?</para>
        /// </summary>
        public bool ClassifyBySubPopulations
        {
            get
            {
                return rdoDecisionYes.Checked;
            }
            set
            {
                onEdit = true;
                if (value)
                {
                    rdoDecisionNo.Checked = false;
                    rdoDecisionYes.Checked = true;
                }
                else
                {
                    rdoDecisionNo.Checked = true;
                    rdoDecisionYes.Checked = false;
                }
                onEdit = false;

                SelectClassifyBySubPopulation();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná (read</para>
        /// <para lang="en">Selected target variable</para>
        /// </summary>
        public TDTargetVariable SelectedTargetVariable
        {
            get
            {
                // Klasifikovat subpopulacemi - NE
                if (!ClassifyBySubPopulations)
                {
                    return (lstTargetVariableCore.SelectedItem == null) ? null :
                        (TDTargetVariable)lstTargetVariableCore.SelectedItem;
                }

                // Klasifikovat subpopulacemi - ANO
                else
                {
                    return (lstTargetVariableDivision.SelectedItem == null) ? null :
                        (TDTargetVariable)lstTargetVariableDivision.SelectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný objekt lokální hustoty (read-only)</para>
        /// <para lang="en">Selected local density object (read-only)</para>
        /// </summary>
        private TDLDsityObject SelectedLocalDensityObject
        {
            get
            {
                // Klasifikovat subpopulacemi - NE
                if (!ClassifyBySubPopulations)
                {
                    return (lstLDsityObjectCore.SelectedItem == null) ? null :
                        (TDLDsityObject)lstLDsityObjectCore.SelectedItem;
                }

                // Klasifikovat subpopulacemi - ANO
                else
                {
                    return (lstLDsityObjectDivision.SelectedItem == null) ? null :
                        (TDLDsityObject)lstLDsityObjectDivision.SelectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný příspěvek lokální hustoty (read-only)</para>
        /// <para lang="en">Selected local density contribution (read-only)</para>
        /// </summary>
        private TDLDsity SelectedLocalDensity
        {
            get
            {
                // Klasifikovat subpopulacemi - NE
                if (!ClassifyBySubPopulations)
                {
                    return (lstLDsityCore.SelectedItem == null) ? null :
                        (TDLDsity)lstLDsityCore.SelectedItem;
                }

                // Klasifikovat subpopulacemi - ANO
                else
                {
                    return (lstLDsityDivision.SelectedItem == null) ? null :
                        (TDLDsity)lstLDsityDivision.SelectedItem;
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnTargetVariableCopy),                  "Vytvořit kopii vybrané cílové proměnné" },
                        { nameof(btnTargetVariableAdd),                   "Vytvořit novou cílovou proměnnou" },
                        { nameof(btnTargetVariableDelete),                "Smazat cílovou proměnnou" },
                        { nameof(btnLDsityObjectEdit),                    "Editovat objekty lokálních hustot" },
                        { nameof(btnLDsityEdit),                          "Editovat příspěvky lokálních hustot" },
                        { nameof(btnPrevious),                            "Předchozí" },
                        { nameof(btnNext),                                "Další" },
                        { nameof(grpDecision),                            "Budeme třídit subpopulacemi?" },
                        { nameof(grpTargetVariable),                      "Cílová proměnná:" },
                        { nameof(grpLDsityObject),                        "Objekt lokální hustoty:" },
                        { nameof(grpLDsity),                              "Lokální hustota:" },
                        { nameof(lblMainCaption),                         "Rozhodnutí o třídění subpopulacemi" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),    "Vybraná skupina objektů lokálních hustot:" },
                        { nameof(lblSelectedTargetVariableCaption),       "Vybraná cílová proměnná:" },
                        { nameof(lstTargetVariableCore),                  "ExtendedLabelCs" },
                        { nameof(lstTargetVariableDivision),              "ExtendedLabelCs" },
                        { nameof(lstLDsityObjectCore),                    "ExtendedLabelCs" },
                        { nameof(lstLDsityObjectDivision),                "ExtendedLabelCs" },
                        { nameof(lstLDsityCore),                          "ExtendedLabelCs" },
                        { nameof(lstLDsityDivision),                      "ExtendedLabelCs" },
                        { nameof(rdoDecisionYes),                         "ano" },
                        { nameof(rdoDecisionNo),                          "ne" },
                        { nameof(tsrTargetVariable),                      String.Empty },
                        { nameof(tsrLDsityObject),                        String.Empty },
                        { nameof(tsrLDsity),                              String.Empty },
                        { nameof(msgNone),                                "žádná" },
                        { nameof(msgNoneSelectedTargetVariable),          "Žádná cílová proměnná není vybraná." },
                        { nameof(msgSelectedTargetVariableNotExist),      "Cílová proměnná $1 neexistuje v databázové tabulce." },
                        { nameof(msgCannotDeleteSelectedTargetVariable),  "Vybranou cílovou proměnnou $1 nelze smazat." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionTargetVariableDivision),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnTargetVariableCopy),                  "Create a copy of the selected target variable" },
                        { nameof(btnTargetVariableAdd),                   "Create new target variable" },
                        { nameof(btnTargetVariableDelete),                "Delete selected target variable" },
                        { nameof(btnLDsityObjectEdit),                    "Edit local density objects" },
                        { nameof(btnLDsityEdit),                          "Edit local density contributions" },
                        { nameof(btnPrevious),                            "Previous" },
                        { nameof(btnNext),                                "Next" },
                        { nameof(grpDecision),                            "Shall we classify by subpopulations?" },
                        { nameof(grpTargetVariable),                      "Target variable:" },
                        { nameof(grpLDsityObject),                        "Local density object:" },
                        { nameof(grpLDsity),                              "Local density:" },
                        { nameof(lblMainCaption),                         "Decision about classification by subpopulations" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),    "Selected local density object group:" },
                        { nameof(lblSelectedTargetVariableCaption),       "Selected target variable:" },
                        { nameof(lstTargetVariableCore),                  "ExtendedLabelEn" },
                        { nameof(lstTargetVariableDivision),              "ExtendedLabelEn" },
                        { nameof(lstLDsityObjectCore),                    "ExtendedLabelEn" },
                        { nameof(lstLDsityObjectDivision),                "ExtendedLabelEn" },
                        { nameof(lstLDsityCore),                          "ExtendedLabelEn" },
                        { nameof(lstLDsityDivision),                      "ExtendedLabelEn" },
                        { nameof(rdoDecisionYes),                         "yes" },
                        { nameof(rdoDecisionNo),                          "no" },
                        { nameof(tsrTargetVariable),                      String.Empty },
                        { nameof(tsrLDsityObject),                        String.Empty },
                        { nameof(tsrLDsity),                              String.Empty },
                        { nameof(msgNone),                                "none" },
                        { nameof(msgNoneSelectedTargetVariable),          "Target variable is not selected." },
                        { nameof(msgSelectedTargetVariableNotExist),      "Selected target variable $1 doesn't exist in database table." },
                        { nameof(msgCannotDeleteSelectedTargetVariable),  "Selected target variable $1 cannot be deleted."  }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionTargetVariableDivision),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            onEdit = false;

            Visible = (Database != null) &&
                        (Database.Postgres != null) &&
                         Database.Postgres.Initialized;

            pnlInformationCore.Controls.Clear();
            ctrInformationCore =
                new ControlTDLDsityInformation(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            pnlInformationCore.Controls.Add(
                value: ctrInformationCore);

            pnlInformationDivision.Controls.Clear();
            ctrInformationDivision =
                new ControlTDLDsityInformation(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            pnlInformationDivision.Controls.Add(
                value: ctrInformationDivision);

            InitializeLabels();

            ClassifyBySubPopulations = false;

            rdoDecisionYes.CheckedChanged += new EventHandler(
                (sender, e) => { SelectClassifyBySubPopulation(); });

            btnTargetVariableCopy.Click += new EventHandler(
               (sender, e) => { InsertTargetVariable(useCopy: true); });

            btnTargetVariableAdd.Click += new EventHandler(
                (sender, e) => { InsertTargetVariable(useCopy: false); });

            btnTargetVariableDelete.Click += new EventHandler(
                (sender, e) => { DeleteTargetVariable(); });

            btnLDsityObjectEdit.Click += new EventHandler(
                (sender, e) => { UpdateLocalDensityObject(); });

            btnLDsityEdit.Click += new EventHandler(
                (sender, e) => { UpdateLocalDensity(); });

            btnPrevious.Click += new EventHandler(
                (sender, e) => { PreviousClick?.Invoke(sender: sender, e: e); });

            btnNext.Click += new EventHandler(
                (sender, e) => { NextClick?.Invoke(sender: sender, e: e); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnTargetVariableCopy.Text =
                labels.TryGetValue(
                    key: nameof(btnTargetVariableCopy),
                    out string btnTargetVariableCopyText)
                        ? btnTargetVariableCopyText
                        : String.Empty;

            btnTargetVariableAdd.Text =
                labels.TryGetValue(
                    key: nameof(btnTargetVariableAdd),
                    out string btnTargetVariableAddText)
                        ? btnTargetVariableAddText
                        : String.Empty;

            btnTargetVariableDelete.Text =
                labels.TryGetValue(
                    key: nameof(btnTargetVariableDelete),
                    out string btnTargetVariableDeleteText)
                        ? btnTargetVariableDeleteText
                        : String.Empty;

            btnLDsityObjectEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnLDsityObjectEdit),
                    out string btnLDsityObjectEditText)
                        ? btnLDsityObjectEditText
                        : String.Empty;

            btnLDsityEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnLDsityEdit),
                    out string btnLDsityEditText)
                        ? btnLDsityEditText
                        : String.Empty;

            btnPrevious.Text =
                labels.TryGetValue(
                    key: nameof(btnPrevious),
                    out string btnPreviousText)
                        ? btnPreviousText
                        : String.Empty;

            btnNext.Text =
                labels.TryGetValue(
                    key: nameof(btnNext),
                    out string btnNextText)
                        ? btnNextText
                        : String.Empty;

            grpDecision.Text =
                labels.TryGetValue(
                    key: nameof(grpDecision),
                    out string grpDecisionText)
                        ? grpDecisionText
                        : String.Empty;

            grpTargetVariable.Text =
                labels.TryGetValue(
                    key: nameof(grpTargetVariable),
                    out string grpTargetVariableText)
                        ? grpTargetVariableText
                        : String.Empty;

            grpLDsityObject.Text =
                labels.TryGetValue(
                    key: nameof(grpLDsityObject),
                    out string grpLDsityObjectText)
                        ? grpLDsityObjectText
                        : String.Empty;

            grpLDsity.Text =
                labels.TryGetValue(
                    key: nameof(grpLDsity),
                    out string grpLDsityText)
                        ? grpLDsityText
                        : String.Empty;

            lblMainCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblMainCaption),
                    out string lblMainCaptionText)
                        ? lblMainCaptionText
                        : String.Empty;

            lblSelectedLDsityObjectGroupCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblSelectedLDsityObjectGroupCaption),
                    out string lblSelectedLDsityObjectGroupCaptionText)
                        ? lblSelectedLDsityObjectGroupCaptionText
                        : String.Empty;

            lblSelectedTargetVariableCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblSelectedTargetVariableCaption),
                    out string lblSelectedTargetVariableCaptionText)
                        ? lblSelectedTargetVariableCaptionText
                        : String.Empty;

            rdoDecisionYes.Text =
                labels.TryGetValue(
                    key: nameof(rdoDecisionYes),
                    out string rdoDecisionYesText)
                        ? rdoDecisionYesText
                        : String.Empty;

            rdoDecisionNo.Text =
                labels.TryGetValue(
                    key: nameof(rdoDecisionNo),
                    out string rdoDecisionNoText)
                        ? rdoDecisionNoText
                        : String.Empty;

            tsrTargetVariable.Text =
                labels.TryGetValue(
                    key: nameof(tsrTargetVariable),
                    out string tsrTargetVariableText)
                        ? tsrTargetVariableText
                        : String.Empty;

            tsrLDsityObject.Text =
                labels.TryGetValue(
                    key: nameof(tsrLDsityObject),
                    out string tsrLDsityObjectText)
                        ? tsrLDsityObjectText
                        : String.Empty;

            tsrLDsity.Text =
                labels.TryGetValue(
                    key: nameof(tsrLDsity),
                    out string tsrLDsityText)
                        ? tsrLDsityText
                        : String.Empty;

            msgNone =
                labels.TryGetValue(key: nameof(msgNone),
                        out msgNone)
                            ? msgNone
                            : String.Empty;

            msgNoneSelectedTargetVariable =
                labels.TryGetValue(key: nameof(msgNoneSelectedTargetVariable),
                        out msgNoneSelectedTargetVariable)
                            ? msgNoneSelectedTargetVariable
                            : String.Empty;

            msgSelectedTargetVariableNotExist =
                labels.TryGetValue(key: nameof(msgSelectedTargetVariableNotExist),
                        out msgSelectedTargetVariableNotExist)
                            ? msgSelectedTargetVariableNotExist
                            : String.Empty;

            msgCannotDeleteSelectedTargetVariable =
                labels.TryGetValue(key: nameof(msgCannotDeleteSelectedTargetVariable),
                        out msgCannotDeleteSelectedTargetVariable)
                            ? msgCannotDeleteSelectedTargetVariable
                            : String.Empty;

            onEdit = true;

            lstTargetVariableCore.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstTargetVariableCore),
                    out string lstTargetVariableCoreDisplayMember)
                        ? lstTargetVariableCoreDisplayMember
                        : ID;

            lstTargetVariableDivision.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstTargetVariableDivision),
                    out string lstTargetVariableDivisionDisplayMember)
                        ? lstTargetVariableDivisionDisplayMember
                        : ID;

            lstLDsityObjectCore.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLDsityObjectCore),
                    out string lstLDsityObjectCoreDisplayMember)
                        ? lstLDsityObjectCoreDisplayMember
                        : ID;

            lstLDsityObjectDivision.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLDsityObjectDivision),
                    out string lstLDsityObjectDivisionDisplayMember)
                        ? lstLDsityObjectDivisionDisplayMember
                        : ID;

            lstLDsityCore.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLDsityCore),
                    out string lstLDsityCoreDisplayMember)
                        ? lstLDsityCoreDisplayMember
                        : ID;

            lstLDsityDivision.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLDsityDivision),
                    out string lstLDsityDivisionDisplayMember)
                        ? lstLDsityDivisionDisplayMember
                        : ID;

            onEdit = false;

            ctrInformationCore
                .InitializeLabels();

            ctrInformationDivision
                .InitializeLabels();

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis ovládacího prvku</para>
        /// <para lang="en">Sets the control caption</para>
        /// </summary>
        private void SetCaption()
        {
            string selectedLocalDensityObjectGroupId;
            string selectedLocalDensityObjectGroupCaption;
            string selectedTargetVariableId;
            string selectedTargetVariableCaption;

            if (SelectedLocalDensityObjectGroup != null)
            {
                selectedLocalDensityObjectGroupId =
                    SelectedLocalDensityObjectGroup.Id.ToString();
                selectedLocalDensityObjectGroupCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedLocalDensityObjectGroup.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedLocalDensityObjectGroup.LabelEn :
                    String.Empty;
                lblSelectedLDsityObjectGroupValue.Text =
                    $"({selectedLocalDensityObjectGroupId}) {selectedLocalDensityObjectGroupCaption}";
            }
            else
            {
                lblSelectedLDsityObjectGroupValue.Text = msgNone;
            }

            if (SelectedTargetVariable != null)
            {
                selectedTargetVariableId =
                    SelectedTargetVariable.Id.ToString();
                selectedTargetVariableCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedTargetVariable.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedTargetVariable.LabelEn : String.Empty;

                lblSelectedTargetVariableValue.Text =
                    $"({selectedTargetVariableId}) {selectedTargetVariableCaption}";
            }
            else
            {
                lblSelectedTargetVariableValue.Text = msgNone;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Database.STargetData.CTargetVariable.ReLoad();
            Database.STargetData.CLDsityObject.ReLoad();
            Database.STargetData.CLDsity.ReLoad();
        }


        /// <summary>
        /// <para lang="cs">Volba třídění subpopulacemi</para>
        /// <para lang="en">Selection of classify by subpopulations</para>
        /// </summary>
        private void SelectClassifyBySubPopulation()
        {
            if (onEdit) { return; }

            // Klasifikovat subpopulacemi - NE
            if (!ClassifyBySubPopulations)
            {
                splTargetVariableList.Panel2Collapsed = true;
                splLDsityObjectList.Panel2Collapsed = true;
                splLDsityList.Panel2Collapsed = true;
                splInformationList.Panel2Collapsed = true;
                tsrTargetVariable.Visible = false;
                tsrLDsityObject.Visible = false;
                tsrLDsity.Visible = false;
                tlpTargetVariable.RowStyles[0].Height = 0;
                tlpLDsityObject.RowStyles[0].Height = 0;
                tlpLDsity.RowStyles[0].Height = 0;

                InitializeListBoxTargetVariableCore();
            }

            // Klasifikovat subpopulacemi - ANO
            else
            {
                splTargetVariableList.Panel1Collapsed = true;
                splLDsityObjectList.Panel1Collapsed = true;
                splLDsityList.Panel1Collapsed = true;
                splInformationList.Panel1Collapsed = true;
                tsrTargetVariable.Visible = true;
                tsrLDsityObject.Visible = true;
                tsrLDsity.Visible = true;
                tlpTargetVariable.RowStyles[0].Height = 25;
                tlpLDsityObject.RowStyles[0].Height = 25;
                tlpLDsity.RowStyles[0].Height = 25;

                InitializeListBoxTargetVariableDivision();
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu cílových proměnných typu Core</para>
        /// <para lang="en">Initialization of list of target variables type of Core</para>
        /// </summary>
        private void InitializeListBoxTargetVariableCore()
        {
            TDTargetVariableList targetVariablesCore =
                GetTargetVariableCore();

            onEdit = true;
            lstTargetVariableCore = new ListBox()
            {
                DataSource = targetVariablesCore?.Items,
                DisplayMember =
                     Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                        .TryGetValue(
                             key: nameof(lstTargetVariableCore),
                             out string lstTargetVariableCoreDisplayMember)
                                ? lstTargetVariableCoreDisplayMember
                                : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = targetVariablesCore
            };
            lstTargetVariableCore.CreateControl();

            lstTargetVariableCore.SelectedIndexChanged += (sender, e) =>
            {
                SelectTargetVariable();
            };
            onEdit = false;

            pnlTargetVariableCore.Controls.Clear();
            pnlTargetVariableCore.Controls.Add(
                value: lstTargetVariableCore);

            SelectTargetVariable();
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu cílových proměnných typu Division</para>
        /// <para lang="en">Initialization of list of target variables type of Division</para>
        /// </summary>
        private void InitializeListBoxTargetVariableDivision()
        {
            TDTargetVariableList targetVariablesDivision =
                GetTargetVariableDivision();

            if (targetVariablesDivision == null)
            {
                return;
            }

            onEdit = true;

            lstTargetVariableDivision = new ListBox()
            {
                DataSource = targetVariablesDivision.Items,
                DisplayMember = Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                                .TryGetValue(
                                     key: nameof(lstTargetVariableDivision),
                                     out string lstTargetVariableDivisionDisplayMember)
                                        ? lstTargetVariableDivisionDisplayMember
                                        : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = targetVariablesDivision
            };
            lstTargetVariableDivision.CreateControl();

            lstTargetVariableDivision.SelectedIndexChanged += (sender, e) =>
            {
                SelectTargetVariable();
            };

            onEdit = false;

            pnlTargetVariableDivision.Controls.Clear();
            pnlTargetVariableDivision.Controls.Add(
                value: lstTargetVariableDivision);

            SelectTargetVariable();
        }

        /// <summary>
        /// <para lang="cs">Načtení seznamu cílových proměnných typu Core z databáze</para>
        /// <para lang="en">Loads list of target variables type of Core from database</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Seznam cílových proměnných typu Core</para>
        /// <para lang="en">List of target variables type of Core</para>
        /// </returns>
        private TDTargetVariableList GetTargetVariableCore()
        {
            // Všechny cílové proměnné:
            TDTargetVariableList targetVariablesAll =
                TDFunctions.FnGetTargetVariable.Execute(
                    database: Database,
                    ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id)
                    .ConvertToTargetVariableList();

            // Cílové proměnné typu Core
            TDTargetVariableList targetVariablesCore =
                new(
                    database: Database,
                    rows: targetVariablesAll.Data.AsEnumerable()
                        .Where(a => a.Field<Nullable<bool>>(columnName: TDTargetVariableList.ColCore.Name) == true)
                        .Where(a => SelectedTargetVariableGroup.TargetVariableIdList.Contains(
                            item: a.Field<Nullable<int>>(columnName: TDTargetVariableList.ColId.Name))));

            return targetVariablesCore;
        }

        /// <summary>
        /// <para lang="cs">Načtení seznamu cílových proměnných typu Division z databáze</para>
        /// <para lang="en">Loads list of target variables type of Division from database</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Seznam cílových proměnných typu Division</para>
        /// <para lang="en">List of target variables type of Division</para>
        /// </returns>
        private TDTargetVariableList GetTargetVariableDivision()
        {
            // Všechny cílové proměnné:
            TDTargetVariableList targetVariablesAll =
                TDFunctions.FnGetTargetVariable.Execute(
                    database: Database,
                    ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id)
                    .ConvertToTargetVariableList();

            // Cílové proměnné typu Division
            TDTargetVariableList targetVariablesDivision =
                new(
                database: Database,
                rows: targetVariablesAll.Data.AsEnumerable()
                        .Where(a => a.Field<Nullable<bool>>(columnName: TDTargetVariableList.ColCore.Name) == false)
                        .Where(a => SelectedTargetVariableGroup.TargetVariableIdList.Contains(
                            item: a.Field<Nullable<int>>(columnName: TDTargetVariableList.ColId.Name))));

            return targetVariablesDivision;
        }

        /// <summary>
        /// <para lang="cs">Načtení seznamu příspěvků lokálních hustot typu Core z databáze</para>
        /// <para lang="en">Loads list of local density contributions type of Core from database</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Seznam příspěvků lokálních hustot typu Core</para>
        /// <para lang="en">List of local density contributions type of Core</para>
        /// </returns>
        private TDLDsityList GetLocalDensityCore()
        {
            if (SelectedTargetVariableGroup == null)
            {
                return null;
            }

            // Příspěvky lokálních hustot typu Core
            TDLDsityList ldsitiesCore =
                   TDFunctions.FnGetLDsity.Execute(
                       database: Database,
                       ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Core,
                       targetVariableIds: SelectedTargetVariableGroup.TargetVariableIdList);

            return ldsitiesCore;
        }

        /// <summary>
        /// <para lang="cs">Načtení seznamu příspěvků lokálních hustot typu Division z databáze</para>
        /// <para lang="en">Loads list of local density contributions type of Division from database</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Seznam příspěvků lokálních hustot typu Division</para>
        /// <para lang="en">List of local density contributions type of Division</para>
        /// </returns>
        private TDLDsityList GetLocalDensityDivision()
        {
            if (SelectedTargetVariableGroup == null)
            {
                return null;
            }

            // Příspěvky lokálních hustot typu Division
            TDLDsityList ldsitiesDivision =
                    TDFunctions.FnGetLDsity.Execute(
                        database: Database,
                        ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Division,
                        targetVariableIds: SelectedTargetVariableGroup.TargetVariableIdList);

            return ldsitiesDivision;
        }


        /// <summary>
        /// <para lang="cs">Volba cílové proměnné</para>
        /// <para lang="en">Selection of target variable</para>
        /// </summary>
        private void SelectTargetVariable()
        {
            if (onEdit) { return; }

            // Klasifikovat subpopulacemi - NE
            if (!ClassifyBySubPopulations)
            {
                InitializeListBoxLocalDensityObjectCore();
            }

            // Klasifikovat subpopulacemi - ANO
            else
            {
                InitializeListBoxLocalDensityObjectDivision();
            }

            SetCaption();

            SelectedTargetVariableChanged?.Invoke(sender: this, e: new EventArgs());
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu objektů lokálních hustot typu Core</para>
        /// <para lang="en">Initialization of list of local density objects type of Core</para>
        /// </summary>
        private void InitializeListBoxLocalDensityObjectCore()
        {
            TDLDsityObjectList ldsityObjectsCore =
                GetLocalDensityObjectForTargetVariableCore();

            onEdit = true;
            lstLDsityObjectCore = new ListBox()
            {
                DataSource = ldsityObjectsCore?.Items,
                DisplayMember = Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                                 .TryGetValue(
                                     key: nameof(lstLDsityObjectCore),
                                     out string lstLDsityObjectCoreDisplayMember)
                                        ? lstLDsityObjectCoreDisplayMember
                                        : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = ldsityObjectsCore
            };
            lstLDsityObjectCore.CreateControl();

            lstLDsityObjectCore.SelectedIndexChanged += (sender, e) =>
            {
                SelectLocalDensityObject();
            };
            onEdit = false;

            pnlLDsityObjectCore.Controls.Clear();
            pnlLDsityObjectCore.Controls.Add(
                value: lstLDsityObjectCore);

            SelectLocalDensityObject();
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu objektů lokálních hustot typu Division</para>
        /// <para lang="en">Initialization of list of local density objects type of Division</para>
        /// </summary>
        private void InitializeListBoxLocalDensityObjectDivision()
        {
            TDLDsityObjectList ldsityObjectsDivision =
                GetLocalDensityObjectForTargetVariableDivision();

            onEdit = true;

            lstLDsityObjectDivision = new ListBox()
            {
                DataSource = ldsityObjectsDivision?.Items,
                DisplayMember = Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                                 .TryGetValue(
                                     key: nameof(lstLDsityObjectDivision),
                                     out string lstLDsityObjectDivisionDisplayMember)
                                        ? lstLDsityObjectDivisionDisplayMember
                                        : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = ldsityObjectsDivision
            };
            lstLDsityObjectDivision.CreateControl();

            lstLDsityObjectDivision.SelectedIndexChanged += (sender, e) =>
            {
                SelectLocalDensityObject();
            };

            onEdit = false;

            pnlLDsityObjectDivision.Controls.Clear();
            pnlLDsityObjectDivision.Controls.Add(
                value: lstLDsityObjectDivision);

            SelectLocalDensityObject();
        }

        /// <summary>
        /// <para lang="cs">
        /// Načtení seznamu objektů lokálních hustot typu Core z databáze
        /// pro vybranou cílovou proměnnou
        /// </para>
        /// <para lang="en">
        /// Loads list of local density objects type of Core from database
        /// for selected target variable
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Seznam objektů lokálních hustot typu Core
        /// pro vybranou cílovou proměnnou
        /// </para>
        /// <para lang="en">
        /// List of local density objects type of Core
        /// for selected target variable
        /// </para>
        /// </returns>
        private TDLDsityObjectList GetLocalDensityObjectForTargetVariableCore()
        {
            TDLDsityList ldsitiesCore =
                GetLocalDensityCore();

            if (ldsitiesCore == null)
            {
                return null;
            }

            if (SelectedTargetVariable == null)
            {
                return null;
            }

            // Všechny objekty lokálních hustot
            TDLDsityObjectList ldsityObjectsAll =
                TDFunctions.FnGetLDsityObject.Execute(
                    database: Database);

            // Příspěvky lokálních hustot typu Core pro zvolenou cílovou proměnnou
            TDLDsityList ldsitiesCoreForTargetVariable =
                new(
                    database: Database,
                    rows: ldsitiesCore.Data.AsEnumerable()
                            .Where(a => a.Field<int>(TDLDsityList.ColTargetVariableId.Name)
                                == SelectedTargetVariable.Id));

            TDLDsityObjectList ldsityObjectsCore =
                new(
                    database: Database,
                    rows: ldsityObjectsAll.Data.AsEnumerable()
                        .Where(a =>
                            ldsitiesCoreForTargetVariable.Items
                                .Select(b => b.LDsityObjectId)
                                .Distinct()
                            .Contains(value: a.Field<int>(TDLDsityObjectList.ColId.Name))));

            return ldsityObjectsCore;
        }

        /// <summary>
        /// <para lang="cs">
        /// Načtení seznamu objektů lokálních hustot typu Division z databáze
        /// pro vybranou cílovou proměnnou
        /// </para>
        /// <para lang="en">
        /// Loads list of local density objects type of Division from database
        /// for selected target variable
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Seznam objektů lokálních hustot typu Division
        /// pro vybranou cílovou proměnnou
        /// </para>
        /// <para lang="en">
        /// List of local density objects type of Division
        /// for selected target variable
        /// </para>
        /// </returns>
        private TDLDsityObjectList GetLocalDensityObjectForTargetVariableDivision()
        {
            TDLDsityList ldsitiesDivision =
               GetLocalDensityDivision();

            if (ldsitiesDivision == null)
            {
                return null;
            }

            if (SelectedTargetVariable == null)
            {
                return null;
            }

            // Všechny objekty lokálních hustot
            TDLDsityObjectList ldsityObjectsAll =
                TDFunctions.FnGetLDsityObject.Execute(
                    database: Database);

            // Příspěvky lokálních hustot typu Core pro zvolenou cílovou proměnnou
            TDLDsityList ldsitiesDivisionForTargetVariable =
                new(
                    database: Database,
                    rows: ldsitiesDivision.Data.AsEnumerable()
                            .Where(a => a.Field<int>(TDLDsityList.ColTargetVariableId.Name)
                                == SelectedTargetVariable.Id));

            TDLDsityObjectList ldsityObjectsDivision =
                new(
                    database: Database,
                    rows: ldsityObjectsAll.Data.AsEnumerable()
                        .Where(a =>
                            ldsitiesDivisionForTargetVariable.Items
                                .Select(b => b.LDsityObjectId)
                                .Distinct()
                            .Contains(value: a.Field<int>(TDLDsityObjectList.ColId.Name))));

            return ldsityObjectsDivision;
        }


        /// <summary>
        /// <para lang="cs">Volba objektu lokální hustoty</para>
        /// <para lang="en">Selection of local density object</para>
        /// </summary>
        private void SelectLocalDensityObject()
        {
            if (onEdit) { return; }

            // Klasifikovat subpopulacemi - NE
            if (!ClassifyBySubPopulations)
            {
                InitializeListBoxLocalDensityCore();
            }

            // Klasifikovat subpopulacemi - ANO
            else
            {
                InitializeListBoxLocalDensityDivision();
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu příspěvků lokálních hustot typu Core</para>
        /// <para lang="en">Initialization of list of local density contributions type of Core</para>
        /// </summary>
        private void InitializeListBoxLocalDensityCore()
        {
            TDLDsityList ldsitiesCore =
                GetLocalDensityForTargetVariableAndObjectCore();

            onEdit = true;
            lstLDsityCore = new ListBox()
            {
                DataSource = ldsitiesCore?.Items,
                DisplayMember = Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                                 .TryGetValue(
                                     key: nameof(lstLDsityCore),
                                     out string lstLDsityCoreDisplayMember)
                                        ? lstLDsityCoreDisplayMember
                                        : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = ldsitiesCore
            };
            lstLDsityCore.CreateControl();

            lstLDsityCore.SelectedIndexChanged += (sender, e) =>
            {
                SelectLocalDensity();
            };
            onEdit = false;

            pnlLDsityCore.Controls.Clear();
            pnlLDsityCore.Controls.Add(
                value: lstLDsityCore);

            SelectLocalDensity();
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu příspěvků lokálních hustot typu Division</para>
        /// <para lang="en">Initialization of list of local density contributions type of Division</para>
        /// </summary>
        private void InitializeListBoxLocalDensityDivision()
        {
            TDLDsityList ldsitiesDivision =
                GetLocalDensityForTargetVariableAndObjectDivision();

            onEdit = true;

            lstLDsityDivision = new ListBox()
            {
                DataSource = ldsitiesDivision?.Items,
                DisplayMember = Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                     .TryGetValue(
                         key: nameof(lstLDsityDivision),
                         out string lstLDsityDivisionDisplayMember)
                            ? lstLDsityDivisionDisplayMember
                            : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = ldsitiesDivision
            };
            lstLDsityDivision.CreateControl();

            lstLDsityDivision.SelectedIndexChanged += (sender, e) =>
            {
                SelectLocalDensity();
            };

            onEdit = false;

            pnlLDsityDivision.Controls.Clear();
            pnlLDsityDivision.Controls.Add(
                value: lstLDsityDivision);

            SelectLocalDensity();
        }

        /// <summary>
        /// <para lang="cs">
        /// Načtení seznamu příspěvků lokálních hustot typu Core z databáze
        /// pro vybranou cílovou proměnnou a objekt lokální hustoty
        /// </para>
        /// <para lang="en">
        /// Loads list of local density contributions type of Core from database
        /// for selected target variable and local density object
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Seznam příspěvků lokálních hustot typu Core
        /// pro vybranou cílovou proměnnou a objekt lokální hustoty
        /// </para>
        /// <para lang="en">
        /// List of local density contributions type of Core
        /// for selected target variable and local density object
        /// </para>
        /// </returns>
        private TDLDsityList GetLocalDensityForTargetVariableAndObjectCore()
        {
            TDLDsityList ldsitiesCore =
                GetLocalDensityCore();

            if (ldsitiesCore == null)
            {
                return null;
            }

            if (SelectedTargetVariable == null)
            {
                return null;
            }

            if (SelectedLocalDensityObject == null)
            {
                return null;
            }

            // Příspěvky lokálních hustot typu Core
            // pro zvolenou cílovou proměnnou a zvolený objekt lokální hustoty
            TDLDsityList ldsitiesCoreForTargetVariableObject =
                new(
                    database: Database,
                    rows: ldsitiesCore.Data.AsEnumerable()
                            .Where(a => a.Field<int>(TDLDsityList.ColTargetVariableId.Name)
                                == SelectedTargetVariable.Id)
                            .Where(a => a.Field<int>(TDLDsityList.ColLDsityObjectId.Name)
                                == SelectedLocalDensityObject.Id));

            return ldsitiesCoreForTargetVariableObject;
        }

        /// <summary>
        /// <para lang="cs">
        /// Načtení seznamu příspěvků lokálních hustot typu Division z databáze
        /// pro vybranou cílovou proměnnou a objekt lokální hustoty
        /// </para>
        /// <para lang="en">
        /// Loads list of local density contributions type of Division from database
        /// for selected target variable and local density object
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Seznam příspěvků lokálních hustot typu Division
        /// pro vybranou cílovou proměnnou a objekt lokální hustoty
        /// </para>
        /// <para lang="en">
        /// List of local density contributions type of Division
        /// for selected target variable and local density object
        /// </para>
        /// </returns>
        private TDLDsityList GetLocalDensityForTargetVariableAndObjectDivision()
        {
            TDLDsityList ldsitiesDivision =
              GetLocalDensityDivision();

            if (ldsitiesDivision == null)
            {
                return null;
            }

            if (SelectedTargetVariable == null)
            {
                return null;
            }

            if (SelectedLocalDensityObject == null)
            {
                return null;
            }

            // Příspěvky lokálních hustot typu Division
            // pro zvolenou cílovou proměnnou a zvolený objekt lokální hustoty
            TDLDsityList ldsitiesDivisionForTargetVariableObject =
                new(
                    database: Database,
                    rows: ldsitiesDivision.Data.AsEnumerable()
                            .Where(a => a.Field<int>(TDLDsityList.ColTargetVariableId.Name)
                                == SelectedTargetVariable.Id)
                            .Where(a => a.Field<int>(TDLDsityList.ColLDsityObjectId.Name)
                                == SelectedLocalDensityObject.Id));

            return ldsitiesDivisionForTargetVariableObject;
        }

        /// <summary>
        /// <para lang="cs">
        /// Načtení seznamu příspěvků lokálních hustot typu Division z databáze
        /// pro vybranou cílovou proměnnou
        /// </para>
        /// <para lang="en">
        /// Loads list of local density contributions type of Division from database
        /// for selected target variable
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Seznam příspěvků lokálních hustot typu Division
        /// pro vybranou cílovou proměnnou
        /// </para>
        /// <para lang="en">
        /// List of local density contributions type of Division
        /// for selected target variable
        /// </para>
        /// </returns>
        public TDLDsityList GetLocalDensityForTargetVariableDivision()
        {
            TDLDsityList ldsitiesDivision =
             GetLocalDensityDivision();

            if (ldsitiesDivision == null)
            {
                return null;
            }

            if (SelectedTargetVariable == null)
            {
                return null;
            }

            // Příspěvky lokálních hustot typu Division
            // pro zvolenou cílovou proměnnou a zvolený objekt lokální hustoty
            TDLDsityList ldsitiesDivisionForTargetVariable =
                new(
                    database: Database,
                    rows: ldsitiesDivision.Data.AsEnumerable()
                            .Where(a => a.Field<int>(TDLDsityList.ColTargetVariableId.Name)
                                == SelectedTargetVariable.Id));

            return ldsitiesDivisionForTargetVariable;
        }

        /// <summary>
        /// <para lang="cs">Volba příspěvků lokální hustoty</para>
        /// <para lang="en">Selection of local density contribution</para>
        /// </summary>
        private void SelectLocalDensity()
        {
            EnableButtonNext();

            // Klasifikovat subpopulacemi - NE
            if (!ClassifyBySubPopulations)
            {
                ctrInformationCore.Visible = true;

                if (SelectedTargetVariable == null)
                {
                    ctrInformationCore.Visible = false;
                    return;
                }

                if (SelectedLocalDensity == null)
                {
                    ctrInformationCore.Visible = false;
                    return;
                }

                ctrInformationDivision.ShowInformation(
                    targetVariable: SelectedTargetVariable,
                    ldsity: SelectedLocalDensity);
            }

            // Klasifikovat subpopulacemi - ANO
            else
            {
                ctrInformationDivision.Visible = true;

                if (SelectedTargetVariable == null)
                {
                    ctrInformationDivision.Visible = false;
                    return;
                }

                if (SelectedLocalDensity == null)
                {
                    ctrInformationDivision.Visible = false;
                    return;
                }

                ctrInformationDivision.ShowInformation(
                    targetVariable: SelectedTargetVariable,
                    ldsity: SelectedLocalDensity);
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost tlačítka "Další"</para>
        /// <para lang="en">Enable or Disable button "Next"</para>
        /// </summary>
        private void EnableButtonNext()
        {
            btnNext.Enabled =
                (SelectedTargetVariableGroup != null) &&
                (SelectedTargetVariable != null) &&
                (SelectedLocalDensityObject != null) &&
                (SelectedLocalDensity != null);
        }


        /// <summary>
        /// <para lang="cs">Vložení nové cílové proměnné typu Division</para>
        /// <para lang="en">Inserting a new target variable type of Division</para>
        /// </summary>
        /// <param name="useCopy">
        /// <para lang="cs">Použít kopii vybrané cílové proměnné</para>
        /// <para lang="en">Use copy of selected target variable</para>
        /// </param>
        private void InsertTargetVariable(bool useCopy)
        {
            if (!ClassifyBySubPopulations)
            {
                // Vložení nové cílové proměnné typu Core se provádí na předchozím formuláři
                // Na tomto formuláři se vkládají pouze nové cílové proměnné typu Division
                // a tlačítko pro vložení nové cílové proměnné tedy není dostupné.
                return;
            }

            if (SelectedLocalDensityObjectGroup == null)
            {
                return;
            }

            FormTargetVariableDivisionNew form = null;
            try
            {
                form = new FormTargetVariableDivisionNew(
                    controlOwner: this,
                    targetVariablesCore: GetTargetVariableCore(),
                    ldsityContributionsCore: GetLocalDensityCore(),
                    useCopy: useCopy);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    text: e.Message,
                    caption: String.Empty,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }

            if (form != null)
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    if (form.NewTargetVariableId != null)
                    {
                        TDFnGetTargetVariableTypeList fnTargetVariable
                            = TDFunctions.FnGetTargetVariable.Execute(
                                database: Database,
                                ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id);

                        if (fnTargetVariable != null)
                        {
                            if (
                                fnTargetVariable.Items
                                    .Where(a => a.TargetVariableIdList.Contains(
                                            item: (int)form.NewTargetVariableId))
                                    .Any())
                            {
                                SelectedTargetVariableGroup =
                                    fnTargetVariable.Items
                                        .Where(a => a.TargetVariableIdList.Contains(
                                            item: (int)form.NewTargetVariableId))
                                        .FirstOrDefault();
                            }
                        }
                    }

                    LoadContent();
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání vybrané cílové proměnné typu Division</para>
        /// <para lang="en">Delete selected target variable type of Division</para>
        /// </summary>
        private void DeleteTargetVariable()
        {
            if (!ClassifyBySubPopulations)
            {
                // Smazání cílové proměnné typu Core a tedy celé skupiny se provádí na předchozím formuláři
                // Na tomto formuláři lze pouze smazat cílové proměnné typu Division ze skupiny
                // a tlačítko pro smazání cílové proměnné tedy není dostupné.
                return;
            }

            if (SelectedTargetVariable == null)
            {
                // Žádná cílová proměnná není vybraná
                // No target variable selected
                MessageBox.Show(
                    text: msgNoneSelectedTargetVariable,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (!Database.STargetData.CTargetVariable.Items
                    .Where(a => a.Id == SelectedTargetVariable.Id)
                    .Any())
            {
                // Vybraná cílová proměnná není v databázové tabulce (nemůže nastat)
                // The selected target variable is not in the database table (cannot occur)
                MessageBox.Show(
                    text: msgSelectedTargetVariableNotExist
                         .Replace(oldValue: "$1", newValue: SelectedTargetVariable.Id.ToString()),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (TDFunctions.FnTryDeleteTargetVariable.Execute(
                    database: Database,
                    targetVariableId: SelectedTargetVariable.Id))
            {
                TDFunctions.FnDeleteTargetVariable.Execute(
                    database: Database,
                    targetVariableId: SelectedTargetVariable.Id);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnTryDeleteTargetVariable.CommandText,
                        caption: TDFunctions.FnTryDeleteTargetVariable.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    MessageBox.Show(
                        text: TDFunctions.FnDeleteTargetVariable.CommandText,
                        caption: TDFunctions.FnDeleteTargetVariable.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                LoadContent();
                SelectClassifyBySubPopulation();
            }
            else
            {
                // Vybranou cílovou proměnnou nelze smazat
                // The selected target variable cannot be deleted
                MessageBox.Show(
                    text: msgCannotDeleteSelectedTargetVariable
                        .Replace(oldValue: "$1", newValue: SelectedTargetVariable.Id.ToString()),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků objektů lokální hustoty</para>
        /// <para lang="en">Editing local density object labels</para>
        /// </summary>
        private void UpdateLocalDensityObject()
        {
            if (!ClassifyBySubPopulations)
            {
                // Editace objektů lokálních hustot cílové proměnné typu Core se provádí na předchozím formuláři
                // Na tomto formuláři se editují pouze objekty lokálních hustot cílové proměnné typu Division
                // a tlačítko pro editaci tedy není dostupné.

                // Changes in local density object of target variable type Core has been done on previous form
                // On this form only changes in local density objects of target variable type Division are possible
                // button for that is not accessible.
                return;
            }

            if (!lstLDsityObjectDivision.Items.OfType<TDLDsityObject>().Any())
            {
                // Seznam je prázdný
                // List is empty
                return;
            }

            FormLookupTable frmLookupTable =
                new(
                    controlOwner: this,
                    lookupTable: Database.STargetData.CLDsityObject,
                    displayedItemsIds: lstLDsityObjectDivision.Items
                                        .OfType<TDLDsityObject>()
                                        .Select(a => (Nullable<int>)a.Id)
                                        .ToList());

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
                SelectTargetVariable();
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků příspěvků lokální hustoty</para>
        /// <para lang="en">Editing local density contribution labels</para>
        /// </summary>
        private void UpdateLocalDensity()
        {
            if (!ClassifyBySubPopulations)
            {
                // Editace příspěvků lokálních hustot cílové proměnné typu Core se provádí na předchozím formuláři
                // Na tomto formuláři se editují pouze příspěvky lokálních hustot cílové proměnné typu Division
                // a tlačítko pro editaci tedy není dostupné.

                // Changes in local density contributions of target variable type Core has been done on previous form
                // On this form only changes in local density contribitions of target variable type Division are possible
                // button for that is not accessible.
                return;
            }

            if (!lstLDsityDivision.Items.OfType<TDLDsity>().Any())
            {
                // Seznam je prázdný
                // List is empty
                return;
            }

            FormLookupTable frmLookupTable =
                new(
                    controlOwner: this,
                    lookupTable: Database.STargetData.CLDsity,
                    displayedItemsIds: lstLDsityDivision.Items
                                        .OfType<TDLDsity>()
                                        .Select(a => (Nullable<int>)a.Id)
                                        .ToList());

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
                SelectLocalDensityObject();
            }
        }

        #endregion Methods

    }

}