﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlClassificationRuleSwitchDesign
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splMain = new System.Windows.Forms.SplitContainer();
            this.tsrMain = new System.Windows.Forms.ToolStrip();
            this.btnPositive = new System.Windows.Forms.ToolStripButton();
            this.btnNegative = new System.Windows.Forms.ToolStripButton();
            this.splClassificationRule = new System.Windows.Forms.SplitContainer();
            this.pnlPositive = new System.Windows.Forms.Panel();
            this.pnlNegative = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).BeginInit();
            this.splMain.Panel1.SuspendLayout();
            this.splMain.Panel2.SuspendLayout();
            this.splMain.SuspendLayout();
            this.tsrMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splClassificationRule)).BeginInit();
            this.splClassificationRule.Panel1.SuspendLayout();
            this.splClassificationRule.Panel2.SuspendLayout();
            this.splClassificationRule.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.Controls.Add(this.splMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(598, 298);
            this.pnlMain.TabIndex = 1;
            // 
            // splMain
            // 
            this.splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splMain.IsSplitterFixed = true;
            this.splMain.Location = new System.Drawing.Point(0, 0);
            this.splMain.Margin = new System.Windows.Forms.Padding(0);
            this.splMain.Name = "splMain";
            this.splMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMain.Panel1
            // 
            this.splMain.Panel1.Controls.Add(this.tsrMain);
            // 
            // splMain.Panel2
            // 
            this.splMain.Panel2.Controls.Add(this.splClassificationRule);
            this.splMain.Size = new System.Drawing.Size(598, 298);
            this.splMain.SplitterDistance = 25;
            this.splMain.SplitterWidth = 1;
            this.splMain.TabIndex = 0;
            // 
            // tsrMain
            // 
            this.tsrMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPositive,
            this.btnNegative});
            this.tsrMain.Location = new System.Drawing.Point(0, 0);
            this.tsrMain.Name = "tsrMain";
            this.tsrMain.Size = new System.Drawing.Size(598, 25);
            this.tsrMain.TabIndex = 3;
            this.tsrMain.Text = "toolStrip1";
            // 
            // btnPositive
            // 
            this.btnPositive.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPositive.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPositive.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPositive.Name = "btnPositive";
            this.btnPositive.Size = new System.Drawing.Size(70, 22);
            this.btnPositive.Text = "btnPositive";
            // 
            // btnNegative
            // 
            this.btnNegative.BackColor = System.Drawing.SystemColors.Control;
            this.btnNegative.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnNegative.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnNegative.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNegative.Name = "btnNegative";
            this.btnNegative.Size = new System.Drawing.Size(76, 22);
            this.btnNegative.Text = "btnNegative";
            // 
            // splClassificationRule
            // 
            this.splClassificationRule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splClassificationRule.Location = new System.Drawing.Point(0, 0);
            this.splClassificationRule.Name = "splClassificationRule";
            // 
            // splClassificationRule.Panel1
            // 
            this.splClassificationRule.Panel1.Controls.Add(this.pnlPositive);
            // 
            // splClassificationRule.Panel2
            // 
            this.splClassificationRule.Panel2.Controls.Add(this.pnlNegative);
            this.splClassificationRule.Size = new System.Drawing.Size(598, 272);
            this.splClassificationRule.SplitterDistance = 300;
            this.splClassificationRule.TabIndex = 0;
            // 
            // pnlPositive
            // 
            this.pnlPositive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPositive.Location = new System.Drawing.Point(0, 0);
            this.pnlPositive.Name = "pnlPositive";
            this.pnlPositive.Size = new System.Drawing.Size(300, 272);
            this.pnlPositive.TabIndex = 1;
            // 
            // pnlNegative
            // 
            this.pnlNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNegative.Location = new System.Drawing.Point(0, 0);
            this.pnlNegative.Name = "pnlNegative";
            this.pnlNegative.Size = new System.Drawing.Size(294, 272);
            this.pnlNegative.TabIndex = 0;
            // 
            // ControlClassificationRuleSwitch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Controls.Add(this.pnlMain);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlClassificationRuleSwitch";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(600, 300);
            this.pnlMain.ResumeLayout(false);
            this.splMain.Panel1.ResumeLayout(false);
            this.splMain.Panel1.PerformLayout();
            this.splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).EndInit();
            this.splMain.ResumeLayout(false);
            this.tsrMain.ResumeLayout(false);
            this.tsrMain.PerformLayout();
            this.splClassificationRule.Panel1.ResumeLayout(false);
            this.splClassificationRule.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splClassificationRule)).EndInit();
            this.splClassificationRule.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.ToolStrip tsrMain;
        private System.Windows.Forms.ToolStripButton btnPositive;
        private System.Windows.Forms.ToolStripButton btnNegative;
        private System.Windows.Forms.SplitContainer splClassificationRule;
        private System.Windows.Forms.Panel pnlPositive;
        private System.Windows.Forms.Panel pnlNegative;
    }

}