﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nové skupiny objektů lokálních hustot</para>
    /// <para lang="en">Form to create a new group of local density objects</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormLDsityObjectGroupNew
            : Form, INfiEstaControl, ITargetDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        private string msgDuplicateEntry = String.Empty;
        private string msgNoObjectSelected = String.Empty;
        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormLDsityObjectGroupNew(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormLDsityObjectGroupNew),     "Nová skupina objektů lokálních hustot"},
                        { nameof(btnOK),                        "Vytvořit"},
                        { nameof(btnCancel),                    "Zrušit"},
                        { nameof(cklObjectsList),               "ExtendedLabelCs"},
                        { nameof(grpObjectsList),               "Seznam objektů lokálních hustot:"},
                        { nameof(grpGroupOfObjects),            "Skupina objektů lokálních hustot:"},
                        { nameof(lblLabelCs),                   "Zkratka (národní):"},
                        { nameof(lblDescriptionCs),             "Popis (národní):"},
                        { nameof(lblLabelEn),                   "Zkratka (anglicky):"},
                        { nameof(lblDescriptionEn),             "Popis (anglicky):"},
                        { nameof(lblArealOrPopulation),         "Typ:"},
                        { nameof(msgDuplicateEntry),            "Záznam nesmí být duplicitní" },
                        { nameof(msgNoObjectSelected),          "Není vybrán žádný objekt." },
                        { nameof(msgLabelCsIsEmpty),            "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),      "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),            "Zkratka (anglicky) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),      "Popis (anglicky) musí být vyplněn." },
                        { nameof(msgLabelCsExists),             "Použitá zkratka (národní) již existuje pro jinou skupinu objektů." },
                        { nameof(msgDescriptionCsExists),       "Použitý popis (národní) již existuje pro jinou skupinu objektů." },
                        { nameof(msgLabelEnExists),             "Použitá zkratka (anglická) již existuje pro jinou skupinu objektů." },
                        { nameof(msgDescriptionEnExists),       "Použitý popis (anglický) již existuje pro jinou skupinu objektů." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormLDsityObjectGroupNew),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormLDsityObjectGroupNew),     "New group of local density objects"},
                        { nameof(btnOK),                        "Insert"},
                        { nameof(btnCancel),                    "Cancel"},
                        { nameof(cklObjectsList),               "ExtendedLabelEn"},
                        { nameof(grpObjectsList),               "List of local density objects:"},
                        { nameof(grpGroupOfObjects),            "Group of local density objects:"},
                        { nameof(lblLabelCs),                   "Label (national):"},
                        { nameof(lblDescriptionCs),             "Description (national):"},
                        { nameof(lblLabelEn),                   "Label (English):"},
                        { nameof(lblDescriptionEn),             "Description (English):"},
                        { nameof(lblArealOrPopulation),         "Type:"},
                        { nameof(msgDuplicateEntry),            "Duplicate entry is not allowed" },
                        { nameof(msgNoObjectSelected),          "No object selected." },
                        { nameof(msgLabelCsIsEmpty),            "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),      "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),            "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),      "Description (English) cannot be empty." },
                        { nameof(msgLabelCsExists),             "This label (national) already exists for another group of objects." },
                        { nameof(msgDescriptionCsExists),       "This description (national) already exists for another group of objects." },
                        { nameof(msgLabelEnExists),             "This label (English) already exists for another group of objects." },
                        { nameof(msgDescriptionEnExists),       "This description (English) already exists for another group of objects." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormLDsityObjectGroupNew),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            InitializeRadioButtonListArealOrPopulation();

            InitializeLabels();
            SetStyle();

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !SaveLDsityObjectGroup();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });

            btnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            TableLayoutPanelCellPosition pos;

            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;


            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;


            cklObjectsList.Font = Setting.CheckedListBoxFont;
            cklObjectsList.ForeColor = Setting.CheckedListBoxForeColor;


            grpObjectsList.Font = Setting.GroupBoxFont;
            grpObjectsList.ForeColor = Setting.GroupBoxForeColor;

            grpGroupOfObjects.Font = Setting.GroupBoxFont;
            grpGroupOfObjects.ForeColor = Setting.GroupBoxForeColor;


            lblLabelCs.Font = Setting.LabelFont;
            lblLabelCs.ForeColor = Setting.LabelForeColor;

            lblDescriptionCs.Font = Setting.LabelFont;
            lblDescriptionCs.ForeColor = Setting.LabelForeColor;

            lblLabelEn.Font = Setting.LabelFont;
            lblLabelEn.ForeColor = Setting.LabelForeColor;

            lblDescriptionEn.Font = Setting.LabelFont;
            lblDescriptionEn.ForeColor = Setting.LabelForeColor;

            lblArealOrPopulation.Font = Setting.LabelFont;
            lblArealOrPopulation.ForeColor = Setting.LabelForeColor;


            txtLabelCs.Font = Setting.TextBoxFont;
            txtLabelCs.ForeColor = Setting.TextBoxForeColor;
            pos = tlpGroupOfObjects.GetCellPosition(control: txtLabelCs);
            txtLabelCs.Margin = new Padding(
                left: 0, top: (int)((tlpGroupOfObjects.GetRowHeights()[pos.Row] - txtLabelCs.Height) / 2), right: 0, bottom: 0);

            txtDescriptionCs.Font = Setting.TextBoxFont;
            txtDescriptionCs.ForeColor = Setting.TextBoxForeColor;
            pos = tlpGroupOfObjects.GetCellPosition(control: txtDescriptionCs);
            txtDescriptionCs.Margin = new Padding(
                left: 0, top: (int)((tlpGroupOfObjects.GetRowHeights()[pos.Row] - txtDescriptionCs.Height) / 2), right: 0, bottom: 0);

            txtLabelEn.Font = Setting.TextBoxFont;
            txtLabelEn.ForeColor = Setting.TextBoxForeColor;
            pos = tlpGroupOfObjects.GetCellPosition(control: txtLabelEn);
            txtLabelEn.Margin = new Padding(
                left: 0, top: (int)((tlpGroupOfObjects.GetRowHeights()[pos.Row] - txtLabelEn.Height) / 2), right: 0, bottom: 0);

            txtDescriptionEn.Font = Setting.TextBoxFont;
            txtDescriptionEn.ForeColor = Setting.TextBoxForeColor;
            pos = tlpGroupOfObjects.GetCellPosition(control: txtDescriptionEn);
            txtDescriptionEn.Margin = new Padding(
                left: 0, top: (int)((tlpGroupOfObjects.GetRowHeights()[pos.Row] - txtDescriptionEn.Height) / 2), right: 0, bottom: 0);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: nameof(FormLDsityObjectGroupNew),
                    out string frmLDsityObjectGroupNewText)
                        ? frmLDsityObjectGroupNewText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(
                    key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(
                    key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            cklObjectsList.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cklObjectsList),
                    out string cklObjectsListText)
                        ? cklObjectsListText
                        : String.Empty;

            grpObjectsList.Text =
                labels.TryGetValue(
                    key: nameof(grpObjectsList),
                    out string grpObjectsListText)
                        ? grpObjectsListText
                        : String.Empty;

            grpGroupOfObjects.Text =
                labels.TryGetValue(
                    key: nameof(grpGroupOfObjects),
                    out string grpGroupOfObjectsText)
                        ? grpGroupOfObjectsText
                        : String.Empty;

            lblLabelCs.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelCs),
                    out string lblLabelCsText)
                        ? lblLabelCsText
                        : String.Empty;

            lblDescriptionCs.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionCs),
                    out string lblDescriptionCsText)
                        ? lblDescriptionCsText
                        : String.Empty;

            lblLabelEn.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelEn),
                    out string lblLabelEnText)
                        ? lblLabelEnText
                        : String.Empty;

            lblDescriptionEn.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionEn),
                    out string lblDescriptionEnText)
                        ? lblDescriptionEnText
                        : String.Empty;

            lblArealOrPopulation.Text =
                labels.TryGetValue(
                    key: nameof(lblArealOrPopulation),
                    out string lblArealOrPopulationText)
                        ? lblArealOrPopulationText
                        : String.Empty;

            msgDuplicateEntry =
              labels.TryGetValue(key: nameof(msgDuplicateEntry),
                      out msgDuplicateEntry)
                          ? msgDuplicateEntry
                          : String.Empty;

            msgNoObjectSelected =
              labels.TryGetValue(key: nameof(msgNoObjectSelected),
                      out msgNoObjectSelected)
                          ? msgNoObjectSelected
                          : String.Empty;

            msgLabelCsIsEmpty =
              labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                      out msgLabelCsIsEmpty)
                          ? msgLabelCsIsEmpty
                          : String.Empty;

            msgDescriptionCsIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                      out msgDescriptionCsIsEmpty)
                          ? msgDescriptionCsIsEmpty
                          : String.Empty;

            msgLabelEnIsEmpty =
              labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                      out msgLabelEnIsEmpty)
                          ? msgLabelEnIsEmpty
                          : String.Empty;

            msgDescriptionEnIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                      out msgDescriptionEnIsEmpty)
                          ? msgDescriptionEnIsEmpty
                          : String.Empty;

            msgLabelCsExists =
              labels.TryGetValue(key: nameof(msgLabelCsExists),
                      out msgLabelCsExists)
                          ? msgLabelCsExists
                          : String.Empty;

            msgDescriptionCsExists =
              labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                      out msgDescriptionCsExists)
                          ? msgDescriptionCsExists
                          : String.Empty;

            msgLabelEnExists =
              labels.TryGetValue(key: nameof(msgLabelEnExists),
                      out msgLabelEnExists)
                          ? msgLabelEnExists
                          : String.Empty;

            msgDescriptionEnExists =
              labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                      out msgDescriptionEnExists)
                          ? msgDescriptionEnExists
                          : String.Empty;

            foreach (RadioButton rdo in pnlArealOrPopulation.Controls)
            {
                rdo.Text =
                    (LanguageVersion == LanguageVersion.International)
                        ? ((TDArealOrPopulation)rdo.Tag).LabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? ((TDArealOrPopulation)rdo.Tag).LabelCs
                            : ((TDArealOrPopulation)rdo.Tag).Id.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu Areal nebo Population</para>
        /// <para lang="en">Initializing list Areal or Population</para>
        /// </summary>
        private void InitializeRadioButtonListArealOrPopulation()
        {
            int i = -1;
            pnlArealOrPopulation.Controls.Clear();

            foreach (TDArealOrPopulation item in Database.STargetData.CArealOrPopulation.Items)
            {
                RadioButton radioButton = new()
                {
                    Font = Setting.RadioButtonFont,
                    ForeColor = Setting.RadioButtonForeColor,
                    Left = 10 + (++i) * 100,
                    Tag = item,
                    Top = 5,
                    Text =
                        (LanguageVersion == LanguageVersion.National) ? item.LabelCs :
                        (LanguageVersion == LanguageVersion.International) ? item.LabelEn :
                        item.Id.ToString(),
                    Width = 100
                };

                radioButton.CheckedChanged += new EventHandler(
                    (sender, e) =>
                    {
                        ReloadLocalDensityObjectList(radioButton: (RadioButton)sender);
                    });

                radioButton.CreateControl();

                pnlArealOrPopulation.Controls.Add(value: radioButton);
            }

            if (pnlArealOrPopulation.Controls.Count > 0)
            {
                ((RadioButton)pnlArealOrPopulation.Controls[0]).Checked = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahraje seznam objektů lokálních hustot</para>
        /// <para lang="en">It loads list of local density objects</para>
        /// </summary>
        /// <param name="radioButton">
        /// <para lang="cs">Radio button pro volbou typu objektu lokální hustoty</para>
        /// <para lang="en">Radio button for choise of local density object type</para>
        /// </param>
        private void ReloadLocalDensityObjectList(RadioButton radioButton)
        {
            if (radioButton == null)
            {
                return;
            }

            if (radioButton.Tag == null)
            {
                return;
            }

            if (radioButton.Tag is not TDArealOrPopulation)
            {
                return;
            }

            TDArealOrPopulation arealOrPopulation =
                (TDArealOrPopulation)radioButton.Tag;

            if (radioButton.Checked)
            {
                if (LanguageVersion == LanguageVersion.National)
                {
                    cklObjectsList.DataSource =
                        Database.STargetData.CLDsityObject.Items
                            .Where(a => a.ArealOrPopulationId == arealOrPopulation.Id)
                            .OrderBy(a => a.Id)
                            .ToList<TDLDsityObject>();
                }
                else
                {
                    cklObjectsList.DataSource =
                        Database.STargetData.CLDsityObject.Items
                            .Where(a => a.ArealOrPopulationId == arealOrPopulation.Id)
                            .OrderBy(a => a.Id)
                            .ToList<TDLDsityObject>();
                }

                for (int i = 0; i < cklObjectsList.Items.Count; i++)
                    cklObjectsList.SetItemCheckState(
                        index: i,
                        value: CheckState.Unchecked);

                Dictionary<string, string> labels = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                cklObjectsList.DisplayMember =
                    labels.TryGetValue(
                        key: nameof(cklObjectsList),
                        out string cklObjectsListText)
                            ? cklObjectsListText
                            : String.Empty;
            }
        }

        /// <summary>
        /// <para lang="cs">Uloží skupinu objektů lokálních hustot do databázové tabulky</para>
        /// <para lang="en">It saves local density object group into database table</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud bylo možné uložit skupinu do databázové tabulky, jinak false</para>
        /// <para lang="en">It returns true if it was possible to save the group into database table, else false</para>
        /// </returns>
        private bool SaveLDsityObjectGroup()
        {
            List<TDLDsityObject> ldsityObjects =
                cklObjectsList.CheckedItems
                .OfType<TDLDsityObject>()
                .ToList<TDLDsityObject>();

            TDLDsityObjectGroupList objectGroups =
                TDFunctions.FnGetLDsityObjectGroup.Execute(
                    database: Database,
                    ldsityObjectGroupId: null);

            TDLDsityObjectList mappingTable =
                TDFunctions.FnGetLDsityObjectForLDsityObjectGroup.Execute(
                    database: Database,
                    ldsityObjectGroupId: null);

            string labelCs = txtLabelCs.Text.Trim();
            string descriptionCs = txtDescriptionCs.Text.Trim();
            string labelEn = txtLabelEn.Text.Trim();
            string descriptionEn = txtDescriptionEn.Text.Trim();

            if (ldsityObjects.Count == 0)
            {
                MessageBox.Show(
                    text: msgNoObjectSelected,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (
                mappingTable.Items.Count != 0 &&
                mappingTable.Items
                    .GroupBy(a => a.LDsityObjectGroup)
                    .Select(g => new
                    {
                        LDsityObjectGroup = g.Key,
                        LDsityObjectIds = String.Join(",",
                        g.OrderBy(a => a.Id)
                        .Select(a => a.Id.ToString()))
                    })
                    .Where(a => a.LDsityObjectIds == ldsityObjects
                    .OrderBy(b => b.Id)
                    .Select(b => b.Id.ToString())
                    .Aggregate((c, d) => $"{c},{d}"))
                    .Any()
                )
            {
                MessageBox.Show(
                     text: msgDuplicateEntry,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: labelCs))
            {
                MessageBox.Show(
                     text: msgLabelCsIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                MessageBox.Show(
                     text: msgDescriptionCsIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                MessageBox.Show(
                     text: msgLabelEnIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                MessageBox.Show(
                     text: msgDescriptionEnIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (objectGroups.Items
                     .Where(a => a.LabelCs == labelCs)
                     .Any())
            {
                MessageBox.Show(
                     text: msgLabelCsExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (objectGroups.Items
                     .Where(a => a.DescriptionCs == descriptionCs)
                     .Any())
            {
                MessageBox.Show(
                     text: msgDescriptionCsExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (objectGroups.Items.
                     Where(a => a.LabelEn == labelEn)
                     .Any())
            {
                MessageBox.Show(
                     text: msgLabelEnExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (objectGroups.Items
                    .Where(a => a.DescriptionEn == descriptionEn)
                    .Any())
            {
                MessageBox.Show(
                     text: msgDescriptionEnExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            TDFunctions.FnSaveLDsityObjectGroup.Execute(
                        database: Database,
                        labelCs: labelCs,
                        descriptionCs: descriptionCs,
                        labelEn: labelEn,
                        descriptionEn: descriptionEn,
                        ldsityObjects: ldsityObjects);

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnSaveLDsityObjectGroup.CommandText,
                    caption: TDFunctions.FnSaveLDsityObjectGroup.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            return !Database.Postgres.ExceptionFlag;
        }

        #endregion Methods

    }

}