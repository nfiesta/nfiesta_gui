﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlTDSelectionPanelRefYearSetGroup
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlTDSelectionPanelRefYearSetGroup));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCalculate = new System.Windows.Forms.Panel();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.pnlPrevious = new System.Windows.Forms.Panel();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splWorkSpace = new System.Windows.Forms.SplitContainer();
            this.grpPanelRefYearSetGroup = new System.Windows.Forms.GroupBox();
            this.tlpPanelRefYearSetGroup = new System.Windows.Forms.TableLayoutPanel();
            this.pnlLDsityObjectGroup = new System.Windows.Forms.Panel();
            this.lstPanelRefYearSetGroup = new System.Windows.Forms.ListBox();
            this.tsrPanelRefYearSetGroup = new System.Windows.Forms.ToolStrip();
            this.btnPanelRefYearSetGroupEdit = new System.Windows.Forms.ToolStripButton();
            this.btnPanelRefYearSetGroupAdd = new System.Windows.Forms.ToolStripButton();
            this.btnPanelRefYearSetGroupDelete = new System.Windows.Forms.ToolStripButton();
            this.grpPanelRefYearSetList = new System.Windows.Forms.GroupBox();
            this.pnlPanelRefYearSetList = new System.Windows.Forms.Panel();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.splCaption = new System.Windows.Forms.SplitContainer();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.tlpCaption = new System.Windows.Forms.TableLayoutPanel();
            this.lblSelectedLDsityObjectGroupValue = new System.Windows.Forms.Label();
            this.lblSelectedLDsityObjectGroupCaption = new System.Windows.Forms.Label();
            this.lblSelectedTargetVariableCaption = new System.Windows.Forms.Label();
            this.lblSelectedTargetVariableValue = new System.Windows.Forms.Label();
            this.lblSelectedPanelRefYearSetGroupCaption = new System.Windows.Forms.Label();
            this.lblSelectedPanelRefYearSetGroupValue = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlCalculate.SuspendLayout();
            this.pnlPrevious.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splWorkSpace)).BeginInit();
            this.splWorkSpace.Panel1.SuspendLayout();
            this.splWorkSpace.Panel2.SuspendLayout();
            this.splWorkSpace.SuspendLayout();
            this.grpPanelRefYearSetGroup.SuspendLayout();
            this.tlpPanelRefYearSetGroup.SuspendLayout();
            this.pnlLDsityObjectGroup.SuspendLayout();
            this.tsrPanelRefYearSetGroup.SuspendLayout();
            this.grpPanelRefYearSetList.SuspendLayout();
            this.pnlCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).BeginInit();
            this.splCaption.Panel1.SuspendLayout();
            this.splCaption.Panel2.SuspendLayout();
            this.splCaption.SuspendLayout();
            this.tlpCaption.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 2);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 1);
            this.tlpMain.Controls.Add(this.pnlCaption, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.Controls.Add(this.pnlCalculate, 2, 0);
            this.tlpButtons.Controls.Add(this.pnlPrevious, 1, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 500);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(960, 40);
            this.tlpButtons.TabIndex = 14;
            // 
            // pnlCalculate
            // 
            this.pnlCalculate.Controls.Add(this.btnCalculate);
            this.pnlCalculate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCalculate.Location = new System.Drawing.Point(800, 0);
            this.pnlCalculate.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCalculate.Name = "pnlCalculate";
            this.pnlCalculate.Padding = new System.Windows.Forms.Padding(5);
            this.pnlCalculate.Size = new System.Drawing.Size(160, 40);
            this.pnlCalculate.TabIndex = 16;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnCalculate.Location = new System.Drawing.Point(5, 5);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(0);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(150, 30);
            this.btnCalculate.TabIndex = 15;
            this.btnCalculate.Text = "btnCalculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            this.pnlPrevious.Controls.Add(this.btnPrevious);
            this.pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrevious.Location = new System.Drawing.Point(640, 0);
            this.pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPrevious.Name = "pnlPrevious";
            this.pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPrevious.Size = new System.Drawing.Size(160, 40);
            this.pnlPrevious.TabIndex = 17;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrevious.Location = new System.Drawing.Point(5, 5);
            this.btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(150, 30);
            this.btnPrevious.TabIndex = 15;
            this.btnPrevious.Text = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.Controls.Add(this.splWorkSpace);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(3, 123);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 374);
            this.pnlWorkSpace.TabIndex = 9;
            // 
            // splWorkSpace
            // 
            this.splWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splWorkSpace.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.splWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.splWorkSpace.Name = "splWorkSpace";
            // 
            // splWorkSpace.Panel1
            // 
            this.splWorkSpace.Panel1.Controls.Add(this.grpPanelRefYearSetGroup);
            // 
            // splWorkSpace.Panel2
            // 
            this.splWorkSpace.Panel2.Controls.Add(this.grpPanelRefYearSetList);
            this.splWorkSpace.Size = new System.Drawing.Size(954, 374);
            this.splWorkSpace.SplitterDistance = 275;
            this.splWorkSpace.TabIndex = 11;
            // 
            // grpPanelRefYearSetGroup
            // 
            this.grpPanelRefYearSetGroup.Controls.Add(this.tlpPanelRefYearSetGroup);
            this.grpPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpPanelRefYearSetGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpPanelRefYearSetGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpPanelRefYearSetGroup.Location = new System.Drawing.Point(0, 0);
            this.grpPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            this.grpPanelRefYearSetGroup.Name = "grpPanelRefYearSetGroup";
            this.grpPanelRefYearSetGroup.Padding = new System.Windows.Forms.Padding(5, 20, 5, 5);
            this.grpPanelRefYearSetGroup.Size = new System.Drawing.Size(275, 374);
            this.grpPanelRefYearSetGroup.TabIndex = 3;
            this.grpPanelRefYearSetGroup.TabStop = false;
            this.grpPanelRefYearSetGroup.Text = "grpPanelRefYearSetGroup";
            // 
            // tlpPanelRefYearSetGroup
            // 
            this.tlpPanelRefYearSetGroup.ColumnCount = 1;
            this.tlpPanelRefYearSetGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPanelRefYearSetGroup.Controls.Add(this.pnlLDsityObjectGroup, 0, 1);
            this.tlpPanelRefYearSetGroup.Controls.Add(this.tsrPanelRefYearSetGroup, 0, 0);
            this.tlpPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPanelRefYearSetGroup.Location = new System.Drawing.Point(5, 35);
            this.tlpPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            this.tlpPanelRefYearSetGroup.Name = "tlpPanelRefYearSetGroup";
            this.tlpPanelRefYearSetGroup.RowCount = 2;
            this.tlpPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPanelRefYearSetGroup.Size = new System.Drawing.Size(265, 334);
            this.tlpPanelRefYearSetGroup.TabIndex = 1;
            // 
            // pnlLDsityObjectGroup
            // 
            this.pnlLDsityObjectGroup.Controls.Add(this.lstPanelRefYearSetGroup);
            this.pnlLDsityObjectGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLDsityObjectGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlLDsityObjectGroup.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlLDsityObjectGroup.Location = new System.Drawing.Point(0, 25);
            this.pnlLDsityObjectGroup.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLDsityObjectGroup.Name = "pnlLDsityObjectGroup";
            this.pnlLDsityObjectGroup.Size = new System.Drawing.Size(265, 309);
            this.pnlLDsityObjectGroup.TabIndex = 2;
            // 
            // lstPanelRefYearSetGroup
            // 
            this.lstPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstPanelRefYearSetGroup.FormattingEnabled = true;
            this.lstPanelRefYearSetGroup.ItemHeight = 15;
            this.lstPanelRefYearSetGroup.Location = new System.Drawing.Point(0, 0);
            this.lstPanelRefYearSetGroup.Name = "lstPanelRefYearSetGroup";
            this.lstPanelRefYearSetGroup.Size = new System.Drawing.Size(265, 309);
            this.lstPanelRefYearSetGroup.TabIndex = 0;
            // 
            // tsrPanelRefYearSetGroup
            // 
            this.tsrPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrPanelRefYearSetGroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPanelRefYearSetGroupEdit,
            this.btnPanelRefYearSetGroupAdd,
            this.btnPanelRefYearSetGroupDelete});
            this.tsrPanelRefYearSetGroup.Location = new System.Drawing.Point(0, 0);
            this.tsrPanelRefYearSetGroup.Name = "tsrPanelRefYearSetGroup";
            this.tsrPanelRefYearSetGroup.Padding = new System.Windows.Forms.Padding(0);
            this.tsrPanelRefYearSetGroup.Size = new System.Drawing.Size(265, 25);
            this.tsrPanelRefYearSetGroup.TabIndex = 1;
            this.tsrPanelRefYearSetGroup.Text = "tsrPanelRefYearSetGroup";
            // 
            // btnPanelRefYearSetGroupEdit
            // 
            this.btnPanelRefYearSetGroupEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPanelRefYearSetGroupEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnPanelRefYearSetGroupEdit.Image")));
            this.btnPanelRefYearSetGroupEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPanelRefYearSetGroupEdit.Name = "btnPanelRefYearSetGroupEdit";
            this.btnPanelRefYearSetGroupEdit.Size = new System.Drawing.Size(23, 22);
            this.btnPanelRefYearSetGroupEdit.Text = "btnPanelRefYearSetGroupEdit";
            // 
            // btnPanelRefYearSetGroupAdd
            // 
            this.btnPanelRefYearSetGroupAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPanelRefYearSetGroupAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnPanelRefYearSetGroupAdd.Image")));
            this.btnPanelRefYearSetGroupAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPanelRefYearSetGroupAdd.Name = "btnPanelRefYearSetGroupAdd";
            this.btnPanelRefYearSetGroupAdd.Size = new System.Drawing.Size(23, 22);
            this.btnPanelRefYearSetGroupAdd.Text = "btnPanelRefYearSetGroupAdd";
            // 
            // btnPanelRefYearSetGroupDelete
            // 
            this.btnPanelRefYearSetGroupDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPanelRefYearSetGroupDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnPanelRefYearSetGroupDelete.Image")));
            this.btnPanelRefYearSetGroupDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPanelRefYearSetGroupDelete.Name = "btnPanelRefYearSetGroupDelete";
            this.btnPanelRefYearSetGroupDelete.Size = new System.Drawing.Size(23, 22);
            this.btnPanelRefYearSetGroupDelete.Text = "btnPanelRefYearSetGroupDelete";
            // 
            // grpPanelRefYearSetList
            // 
            this.grpPanelRefYearSetList.Controls.Add(this.pnlPanelRefYearSetList);
            this.grpPanelRefYearSetList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpPanelRefYearSetList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpPanelRefYearSetList.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpPanelRefYearSetList.Location = new System.Drawing.Point(0, 0);
            this.grpPanelRefYearSetList.Margin = new System.Windows.Forms.Padding(0);
            this.grpPanelRefYearSetList.Name = "grpPanelRefYearSetList";
            this.grpPanelRefYearSetList.Padding = new System.Windows.Forms.Padding(5);
            this.grpPanelRefYearSetList.Size = new System.Drawing.Size(675, 374);
            this.grpPanelRefYearSetList.TabIndex = 4;
            this.grpPanelRefYearSetList.TabStop = false;
            this.grpPanelRefYearSetList.Text = "grpPanelRefYearSetList";
            // 
            // pnlPanelRefYearSetList
            // 
            this.pnlPanelRefYearSetList.AutoScroll = true;
            this.pnlPanelRefYearSetList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPanelRefYearSetList.ForeColor = System.Drawing.SystemColors.WindowText;
            this.pnlPanelRefYearSetList.Location = new System.Drawing.Point(5, 20);
            this.pnlPanelRefYearSetList.Name = "pnlPanelRefYearSetList";
            this.pnlPanelRefYearSetList.Size = new System.Drawing.Size(665, 349);
            this.pnlPanelRefYearSetList.TabIndex = 0;
            // 
            // pnlCaption
            // 
            this.pnlCaption.Controls.Add(this.splCaption);
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Location = new System.Drawing.Point(2, 2);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(2);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Size = new System.Drawing.Size(956, 116);
            this.pnlCaption.TabIndex = 10;
            // 
            // splCaption
            // 
            this.splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCaption.IsSplitterFixed = true;
            this.splCaption.Location = new System.Drawing.Point(0, 0);
            this.splCaption.Margin = new System.Windows.Forms.Padding(0);
            this.splCaption.Name = "splCaption";
            this.splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            this.splCaption.Panel1.Controls.Add(this.lblMainCaption);
            // 
            // splCaption.Panel2
            // 
            this.splCaption.Panel2.Controls.Add(this.tlpCaption);
            this.splCaption.Size = new System.Drawing.Size(956, 116);
            this.splCaption.SplitterDistance = 30;
            this.splCaption.TabIndex = 13;
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(0, 0);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(956, 30);
            this.lblMainCaption.TabIndex = 0;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpCaption
            // 
            this.tlpCaption.ColumnCount = 2;
            this.tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaption.Controls.Add(this.lblSelectedLDsityObjectGroupValue, 1, 0);
            this.tlpCaption.Controls.Add(this.lblSelectedLDsityObjectGroupCaption, 0, 0);
            this.tlpCaption.Controls.Add(this.lblSelectedTargetVariableCaption, 0, 1);
            this.tlpCaption.Controls.Add(this.lblSelectedTargetVariableValue, 1, 1);
            this.tlpCaption.Controls.Add(this.lblSelectedPanelRefYearSetGroupCaption, 0, 2);
            this.tlpCaption.Controls.Add(this.lblSelectedPanelRefYearSetGroupValue, 1, 2);
            this.tlpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCaption.Location = new System.Drawing.Point(0, 0);
            this.tlpCaption.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCaption.Name = "tlpCaption";
            this.tlpCaption.RowCount = 4;
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaption.Size = new System.Drawing.Size(956, 82);
            this.tlpCaption.TabIndex = 5;
            // 
            // lblSelectedLDsityObjectGroupValue
            // 
            this.lblSelectedLDsityObjectGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedLDsityObjectGroupValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedLDsityObjectGroupValue.Location = new System.Drawing.Point(283, 0);
            this.lblSelectedLDsityObjectGroupValue.Name = "lblSelectedLDsityObjectGroupValue";
            this.lblSelectedLDsityObjectGroupValue.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedLDsityObjectGroupValue.Size = new System.Drawing.Size(670, 25);
            this.lblSelectedLDsityObjectGroupValue.TabIndex = 4;
            this.lblSelectedLDsityObjectGroupValue.Text = "lblSelectedLDsityObjectGroupValue";
            this.lblSelectedLDsityObjectGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedLDsityObjectGroupCaption
            // 
            this.lblSelectedLDsityObjectGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedLDsityObjectGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedLDsityObjectGroupCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSelectedLDsityObjectGroupCaption.Location = new System.Drawing.Point(0, 0);
            this.lblSelectedLDsityObjectGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblSelectedLDsityObjectGroupCaption.Name = "lblSelectedLDsityObjectGroupCaption";
            this.lblSelectedLDsityObjectGroupCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedLDsityObjectGroupCaption.Size = new System.Drawing.Size(280, 25);
            this.lblSelectedLDsityObjectGroupCaption.TabIndex = 0;
            this.lblSelectedLDsityObjectGroupCaption.Text = "lblSelectedLDsityObjectGroupCaption";
            this.lblSelectedLDsityObjectGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableCaption
            // 
            this.lblSelectedTargetVariableCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedTargetVariableCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedTargetVariableCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSelectedTargetVariableCaption.Location = new System.Drawing.Point(0, 25);
            this.lblSelectedTargetVariableCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblSelectedTargetVariableCaption.Name = "lblSelectedTargetVariableCaption";
            this.lblSelectedTargetVariableCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedTargetVariableCaption.Size = new System.Drawing.Size(280, 25);
            this.lblSelectedTargetVariableCaption.TabIndex = 5;
            this.lblSelectedTargetVariableCaption.Text = "lblSelectedTargetVariableCaption";
            this.lblSelectedTargetVariableCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableValue
            // 
            this.lblSelectedTargetVariableValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedTargetVariableValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedTargetVariableValue.Location = new System.Drawing.Point(283, 25);
            this.lblSelectedTargetVariableValue.Name = "lblSelectedTargetVariableValue";
            this.lblSelectedTargetVariableValue.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedTargetVariableValue.Size = new System.Drawing.Size(670, 25);
            this.lblSelectedTargetVariableValue.TabIndex = 6;
            this.lblSelectedTargetVariableValue.Text = "lblSelectedTargetVariableValue";
            this.lblSelectedTargetVariableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedPanelRefYearSetGroupCaption
            // 
            this.lblSelectedPanelRefYearSetGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedPanelRefYearSetGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedPanelRefYearSetGroupCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSelectedPanelRefYearSetGroupCaption.Location = new System.Drawing.Point(0, 50);
            this.lblSelectedPanelRefYearSetGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblSelectedPanelRefYearSetGroupCaption.Name = "lblSelectedPanelRefYearSetGroupCaption";
            this.lblSelectedPanelRefYearSetGroupCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedPanelRefYearSetGroupCaption.Size = new System.Drawing.Size(280, 25);
            this.lblSelectedPanelRefYearSetGroupCaption.TabIndex = 7;
            this.lblSelectedPanelRefYearSetGroupCaption.Text = "lblSelectedPanelRefYearSetGroupCaption";
            this.lblSelectedPanelRefYearSetGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedPanelRefYearSetGroupValue
            // 
            this.lblSelectedPanelRefYearSetGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedPanelRefYearSetGroupValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedPanelRefYearSetGroupValue.Location = new System.Drawing.Point(283, 50);
            this.lblSelectedPanelRefYearSetGroupValue.Name = "lblSelectedPanelRefYearSetGroupValue";
            this.lblSelectedPanelRefYearSetGroupValue.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedPanelRefYearSetGroupValue.Size = new System.Drawing.Size(670, 25);
            this.lblSelectedPanelRefYearSetGroupValue.TabIndex = 8;
            this.lblSelectedPanelRefYearSetGroupValue.Text = "lblSelectedPanelRefYearSetGroupValue";
            this.lblSelectedPanelRefYearSetGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlTDSelectionPanelRefYearSetGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Name = "ControlTDSelectionPanelRefYearSetGroup";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlCalculate.ResumeLayout(false);
            this.pnlPrevious.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splWorkSpace.Panel1.ResumeLayout(false);
            this.splWorkSpace.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splWorkSpace)).EndInit();
            this.splWorkSpace.ResumeLayout(false);
            this.grpPanelRefYearSetGroup.ResumeLayout(false);
            this.tlpPanelRefYearSetGroup.ResumeLayout(false);
            this.tlpPanelRefYearSetGroup.PerformLayout();
            this.pnlLDsityObjectGroup.ResumeLayout(false);
            this.tsrPanelRefYearSetGroup.ResumeLayout(false);
            this.tsrPanelRefYearSetGroup.PerformLayout();
            this.grpPanelRefYearSetList.ResumeLayout(false);
            this.pnlCaption.ResumeLayout(false);
            this.splCaption.Panel1.ResumeLayout(false);
            this.splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).EndInit();
            this.splCaption.ResumeLayout(false);
            this.tlpCaption.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splWorkSpace;
        private System.Windows.Forms.GroupBox grpPanelRefYearSetGroup;
        private System.Windows.Forms.TableLayoutPanel tlpPanelRefYearSetGroup;
        private System.Windows.Forms.Panel pnlLDsityObjectGroup;
        private System.Windows.Forms.ListBox lstPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStrip tsrPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripButton btnPanelRefYearSetGroupEdit;
        private System.Windows.Forms.ToolStripButton btnPanelRefYearSetGroupAdd;
        private System.Windows.Forms.ToolStripButton btnPanelRefYearSetGroupDelete;
        private System.Windows.Forms.GroupBox grpPanelRefYearSetList;
        private System.Windows.Forms.Panel pnlPanelRefYearSetList;
        private System.Windows.Forms.TableLayoutPanel tlpCaption;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupValue;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableValue;
        private System.Windows.Forms.Label lblSelectedPanelRefYearSetGroupCaption;
        private System.Windows.Forms.Label lblSelectedPanelRefYearSetGroupValue;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCalculate;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
    }

}
