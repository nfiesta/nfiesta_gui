﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba třídění lokální hustoty"</para>
    /// <para lang="en">Control "Selection of local density classification"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTDSelectionAttributeCategory
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Maximální počet vybraných domén</para>
        /// <para lang="en">Maximal number of selected domains</para>
        /// </summary>
        private const int maxSelectedDomains = 6;

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;


        // Výsledkem výběru v této komponentě je seznam identifikátorů categorization setup
        // The selection results in this component is list of categorization setup identifiers

        /// <summary>
        /// <para lang="cs">Seznam identifikátorů categorization setup</para>
        /// <para lang="en">List of categorization setup identifiers</para>
        /// </summary>
        private List<Nullable<int>> categorizationSetupIds;


        /// <summary>
        /// <para lang="cs">
        /// Objekty lokálních hustot pro vybranou cílovou proměnnou
        /// </para>
        /// <para lang="en">
        /// Local density objects for selected target variable
        /// </para>
        /// </summary>
        private Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            ldsityObjectsForTargetVariable;


        // Seznamy zobrazované komponentou:
        // Lists displayed by the component:

        /// <summary>
        /// <para lang="cs">Vybrané kombinace kategorií plošných domén</para>
        /// <para lang="en">Selected combinations of area domain categories</para>
        /// </summary>
        private TDDomainCategoryCombinationList<TDAreaDomainCategory> selectedAreaDomainCategoryCombinations;

        /// <summary>
        /// <para lang="cs">Vybrané kombinace kategorií subpopulací</para>
        /// <para lang="en">Selected combinations of subpopulation categories</para>
        /// </summary>
        private TDDomainCategoryCombinationList<TDSubPopulationCategory> selectedSubPopulationCategoryCombinations;

        /// <summary>
        /// <para lang="cs">Seznam vybraných atributových kategorií (kombinací kategorií plošných domén a subpopulací)</para>
        /// <para lang="en">List of selected attribute categories (combinations of area domain and subpopulation categories</para>
        /// </summary>
        private TDAttributeCategoryList selectedAttributeCategories;

        private string msgNone = String.Empty;
        private string msgNoneSelectedAreaDomain = String.Empty;
        private string msgCannotDeleteSelectedAreaDomain = String.Empty;
        private string msgNoneSelectedSubPopulation = String.Empty;
        private string msgCannotDeleteSelectedSubPopulation = String.Empty;
        private string msgNotEnoughAreaDomains = String.Empty;
        private string msgNotEnoughSubPopulations = String.Empty;
        private string msgMaxAreaDomainNumberReached = String.Empty;
        private string msgMaxSubPopulationNumberReached = String.Empty;
        private string msgTargetVariableNULL = String.Empty;
        private string msgLDsityObjectsForTargetVariableNULL = String.Empty;
        private string msgFnGetLDsityObjectsForTargetVariableNullKey = String.Empty;
        private string msgFnGetLDsityObjectsForTargetVariableNullValue = String.Empty;
        private string msgFnGetLDsityObjectsForTargetVariableDuplicateKey = String.Empty;
        private string msgFnGetLDsityObjectsForTargetVariableDuplicateValue = String.Empty;
        private string msgFnGetCategorizationSetupNULL = String.Empty;
        private string msgNoClassificationRulesForADSP = String.Empty;
        private string msgNoClassificationRulesForAD = String.Empty;
        private string msgNoClassificationRulesForSP = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Seznam vybraných plošných domén</para>
        /// <para lang="en">List of selected area domains</para>
        /// </summary>
        public ControlTDDomainListBox<TDAreaDomain, TDAreaDomainCategory> LstSelectedAreaDomains { get; private set; }


        /// <summary>
        /// <para lang="cs">Seznam vybraných subpopulací</para>
        /// <para lang="en">List of selected subpopulations</para>
        /// </summary>
        public ControlTDDomainListBox<TDSubPopulation, TDSubPopulationCategory> LstSelectedSubPopulations { get; private set; }

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">The "Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">The "Next" button click event</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTDSelectionAttributeCategory(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        #region Results from previous forms

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraná skupina objektů lokálních hustot (vybraná na 1.formuláři) (read-only)</para>
        /// <para lang="en">Selected group of local density objects (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectGroup SelectedLocalDensityObjectGroup
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionLDsityObjectGroup
                    .SelectedLocalDensityObjectGroup;
            }
        }

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot ve vybrané skupině (vybrané na 1.formuláři) (read-only)</para>
        /// <para lang="en">Local density objects in the selected group (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectList SelectedLocalDensityObjects
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionLDsityObjectGroup
                    .SelectedLocalDensityObjects;
            }
        }


        /// <summary>
        /// <para lang="cs">Vybraná skupina cílových proměnných (vybraná na 2.formuláři) (read-only)</para>
        /// <para lang="en">Selected target variable group (selected on the 2nd form) (read-only)</para>
        /// </summary>
        public TDFnGetTargetVariableType SelectedTargetVariableGroup
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionTargetVariableCore
                    .SelectedTargetVariableGroup;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná (vybraná na 2. nebo 3. formuláři) (read-only)</para>
        /// <para lang="en">Selected target variable (selected on the 2nd or 3rd form) (read-only)</para>
        /// </summary>
        public TDTargetVariable SelectedTargetVariable
        {
            get
            {
                if (SelectedTargetVariableGroup == null)
                {
                    throw new ArgumentNullException(
                            message: $"{nameof(SelectedTargetVariableGroup)} is null.",
                            paramName: nameof(SelectedTargetVariableGroup));
                }

                return SelectedTargetVariableGroup.ArealOrPopulationValue switch
                {
                    TDArealOrPopulationEnum.AreaDomain =>
                        ((ControlTargetData)ControlOwner)
                            .CtrTDSelectionTargetVariableDivision
                            .SelectedTargetVariable,

                    TDArealOrPopulationEnum.Population =>
                        ((ControlTargetData)ControlOwner)
                            .CtrTDSelectionTargetVariableCore
                            .SelectedTargetVariable,

                    _ => throw
                        new ArgumentException(
                            message: $"Invalid {nameof(SelectedTargetVariableGroup.ArealOrPopulationValue)} value.",
                            paramName: nameof(SelectedTargetVariableGroup.ArealOrPopulationValue)),
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Třídit subpopulacemi? (vybrané na 2. nebo 3. formuláři) (read-only)</para>
        /// <para lang="en">Classify by subPopulations? (selected on the 2nd or 3rd form) (read-only)</para>
        /// </summary>
        public bool ClassifyBySubPopulations
        {
            get
            {
                if (SelectedTargetVariableGroup == null)
                {
                    throw new ArgumentNullException(
                            message: $"{nameof(SelectedTargetVariableGroup)} is null.",
                            paramName: nameof(SelectedTargetVariableGroup));
                }

                return SelectedTargetVariableGroup.ArealOrPopulationValue switch
                {
                    TDArealOrPopulationEnum.AreaDomain =>
                        ((ControlTargetData)ControlOwner)
                            .CtrTDSelectionTargetVariableDivision
                            .ClassifyBySubPopulations,

                    TDArealOrPopulationEnum.Population =>
                        true,

                    _ => throw
                        new ArgumentException(
                            message: $"Invalid {nameof(SelectedTargetVariableGroup.ArealOrPopulationValue)} value.",
                            paramName: nameof(SelectedTargetVariableGroup.ArealOrPopulationValue)),
                };
            }
        }

        #endregion Results from previous forms


        // Výsledkem výběru v této komponentě je seznam identifikátorů categorization setup
        // The selection results in this component is list of categorization setup identifiers

        /// <summary>
        /// <para lang="cs">Seznam identifikátorů categorization setup (read-only)</para>
        /// <para lang="en">List of categorization setup identifiers (read-only)</para>
        /// </summary>
        public List<Nullable<int>> CategorizationSetupIds
        {
            get
            {
                return categorizationSetupIds;
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Objekty lokálních hustot pro vybranou cílovou proměnnou (read-only)
        /// (všechny Core + Division)
        /// (setříděno podle klíče)
        /// </para>
        /// <para lang="en">
        /// Local density objects for selected target variable (read-only)
        /// (all Core + Division)
        /// (sorted by key)
        /// </para>
        /// </summary>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            LDsityObjectsForTargetVariable
        {
            get
            {
                if (ldsityObjectsForTargetVariable == null)
                {
                    return null;
                }

                return
                    ldsityObjectsForTargetVariable
                    .OrderBy(a => a.Key)
                    .ToDictionary(a => a.Key, a => a.Value);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Objekty lokálních hustot pro vybranou cílovou proměnnou (read-only)
        /// (typ Core)
        /// (setříděno podle klíče)
        /// </para>
        /// <para lang="en">
        /// Local density objects for selected target variable (read-only)
        /// (Core type)
        /// (sorted by key)
        /// </para>
        /// </summary>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            LDsityObjectsForTargetVariableCore
        {
            get
            {
                if (LDsityObjectsForTargetVariable == null)
                {
                    return null;
                }

                return
                   LDsityObjectsForTargetVariable
                        .Where(a => a.Value.LDsityObjectType.Value == TDLDsityObjectTypeEnum.Core)
                        .OrderBy(a => a.Key)
                        .ToDictionary(a => a.Key, a => a.Value);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Objekty lokálních hustot pro vybranou cílovou proměnnou (read-only)
        /// (typ Division)
        /// (setříděno podle klíče)
        /// </para>
        /// <para lang="en">
        /// Local density objects for selected target variable (read-only)
        /// (Core type)
        /// (sorted by key)
        /// </para>
        /// </summary>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            LDsityObjectsForTargetVariableDivision
        {
            get
            {
                if (LDsityObjectsForTargetVariable == null)
                {
                    return null;
                }

                return
                   LDsityObjectsForTargetVariable
                        .Where(a => a.Value.LDsityObjectType.Value == TDLDsityObjectTypeEnum.Division)
                        .OrderBy(a => a.Key)
                        .ToDictionary(a => a.Key, a => a.Value);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných atributových kategorií (kombinací kategorií plošných domén a subpopulací) (read-only)</para>
        /// <para lang="en">List of selected attribute categories (combinations of area domain and subpopulation categories (read-only)</para>
        /// </summary>
        public TDAttributeCategoryList SelectedAttributeCategories
        {
            get
            {
                return selectedAttributeCategories;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnAreaDomainEdit),                                      "Editovat plošnou doménu" },
                        { nameof(btnAreaDomainAdd),                                       "Vytvořit novou plošnou doménu" },
                        { nameof(btnAreaDomainDelete),                                    "Smazat plošnou doménu" },
                        { nameof(btnAreaDomainExport),                                    "Export klasifikačních pravidel plošné domény do JSON souboru." },
                        { nameof(btnAreaDomainImport),                                    "Import klasifikačních pravidel plošné domény z JSON souboru." },
                        { nameof(btnAreaDomainSelect),                                    "Přidat plošnou doménu mezi vybrané" },
                        { nameof(btnAreaDomainCancel),                                    "Odebrat plošnou doménu z vybraných" },
                        { nameof(btnADCHierarchy),                                        "Upravit hierachii kategorií plošných domén" },
                        { nameof(btnAreaDomainCategoryEdit),                              "Editovat kategorii plošné domény" },
                        { nameof(btnLimitTargetVariable),                                 "Omezení domény nebo populace" },
                        { nameof(btnSubPopulationEdit),                                   "Editovat subpopulaci" },
                        { nameof(btnSubPopulationAdd),                                    "Vytvořit novou subpopulaci" },
                        { nameof(btnSubPopulationDelete),                                 "Smazat subpopulaci" },
                        { nameof(btnSubPopulationExport),                                 "Export klasifikačních pravidel subpopulace do JSON souboru." },
                        { nameof(btnSubPopulationImport),                                 "Import klasifikačních pravidel subpopulace z JSON souboru." },
                        { nameof(btnSubPopulationSelect),                                 "Přidat subpopulaci mezi vybrané" },
                        { nameof(btnSubPopulationCancel),                                 "Odebrat subpopulaci z vybraných" },
                        { nameof(btnSPCHierarchy),                                        "Upravit hierachii kategorií subpopulací" },
                        { nameof(btnSubPopulationCategoryEdit),                           "Editovat kategorii subpopulace" },
                        { nameof(btnSaveCategorizationSetup),                             "Uložit kategorizaci" },
                        { nameof(btnPrevious),                                            "Předchozí" },
                        { nameof(btnNext),                                                "Další" },
                        { nameof(cboAreaDomain),                                          "ExtendedLabelCs" },
                        { nameof(cboSubPopulation),                                       "ExtendedLabelCs" },
                        { nameof(grpAreaDomain),                                          "Plošná doména:" },
                        { nameof(grpAreaDomainCategory),                                  "Kategorie plošné domény:" },
                        { nameof(grpAreaDomainSelected),                                  "Vybrané plošné domény:" },
                        { nameof(grpAreaDomainCategoryCombination),                       "Kombinace kategorií vybraných plošných domén:" },
                        { nameof(grpSubPopulation),                                       "Subpopulace:" },
                        { nameof(grpSubPopulationCategory),                               "Kategorie subpopulace:" },
                        { nameof(grpSubPopulationSelected),                               "Vybrané subpopulace:" },
                        { nameof(grpSubPopulationCategoryCombination),                    "Kombinace kategorií vybraných subpopulací:" },
                        { nameof(grpAttributeCategory),                                   "Výsledná kategorizace:" },
                        { nameof(lblMainCaption),                                         "Volba třídění lokální hustoty" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),                    "Vybraná skupina objektů lokálních hustot:" },
                        { nameof(lblSelectedTargetVariableCaption),                       "Vybraná cílová proměnná:" },
                        { nameof(lstAreaDomainCategory),                                  "ExtendedLabelCs" },
                        { nameof(lstSubPopulationCategory),                               "ExtendedLabelCs" },
                        { nameof(rdoAreaDomainStandard),                                  "standardní" },
                        { nameof(rdoAreaDomainChangeOrDynamic),                           "změnová nebo dynamická" },
                        { nameof(rdoSubPopulationStandard),                               "standardní" },
                        { nameof(rdoSubPopulationChangeOrDynamic),                        "změnová nebo dynamická" },
                        { nameof(tsrAreaDomain),                                          String.Empty },
                        { nameof(tsrAreaDomainSelect),                                    String.Empty },
                        { nameof(tsrAreaDomainSelected),                                  String.Empty },
                        { nameof(tsrAreaDomainCategory),                                  String.Empty },
                        { nameof(tsrSubPopulation),                                       String.Empty },
                        { nameof(tsrSubPopulationSelect),                                 String.Empty },
                        { nameof(tsrSubPopulationSelected),                               String.Empty },
                        { nameof(tsrSubPopulationCategory),                               String.Empty },
                        { nameof(msgNone),                                                "žádná" },
                        { nameof(msgNoneSelectedAreaDomain),                              "Žádná plošná doména není vybraná." },
                        { nameof(msgCannotDeleteSelectedAreaDomain),                      "Vybranou plošnou doménu nelze smazat." },
                        { nameof(msgNoneSelectedSubPopulation),                           "Žádná subpopulace není vybraná." },
                        { nameof(msgCannotDeleteSelectedSubPopulation),                   "Vybranou subpopulaci nelze smazat." },
                        { nameof(msgNotEnoughAreaDomains),                                "Nejméně dvě plošné domény musí být vybrané pro úpravu hierarchie kategorií plošných domén." },
                        { nameof(msgNotEnoughSubPopulations),                             "Nejméně dvě subpopulace musí být vybrané pro úpravu hierachie kategorií subpopulací." },
                        { nameof(msgMaxAreaDomainNumberReached),                          "Byl dosažen maximální možný počet vybraných plošných domén." },
                        { nameof(msgMaxSubPopulationNumberReached),                       "Byl dosažen maximální možný počet vybraných subpopulací." },
                        { nameof(msgTargetVariableNULL),                                  "Argument target_variable uložené procedury $1 nesmí být NULL." },
                        { nameof(msgLDsityObjectsForTargetVariableNULL),                  "Neexistují objekty lokálních hustot pro vybranou cílovou proměnnou." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableNullKey),          "Uložená procedura $1 vrátila tabulku s NULL hodnotami ve sloupci key." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableNullValue),        "Uložená procedura $1 vrátila tabulku s NULL hodnotami ve sloupci value." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableDuplicateKey),     "Uložená procedura $1 vrátila tabulku s duplicitními hodnotami ve sloupci key." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableDuplicateValue),   "Uložená procedura $1 vrátila tabulku s duplicitními hodnotami ve sloupci value." },
                        { nameof(msgFnGetCategorizationSetupNULL),                        String.Concat(
                                                                                                $"Uložená procedura $1 vrátila $3. $0 $0",
                                                                                                $"Aby bylo povoleno tlačítko pro přechod na další formulář je potřeba,",
                                                                                                $"aby uložená procedura vrátila pole celých čísel s identifikátory. $0 $0",
                                                                                                $"Aby bylo povoleno tlačítko uložit kategorizaci je potřeba,",
                                                                                                $"aby uložená procedura vrátila pole, ve kterém budou chybějící identifikátory nahrazeny NULL hodnotou. $0 $0",
                                                                                                $"Z tohoto výsledku uložené procedury ($3) nelze určit, které tlačítko má být povoleno. $0 $0",
                                                                                                $"Provedený SQL příkaz: $0 $0 $2 $0") },
                        { nameof(msgNoClassificationRulesForADSP),                        "Některé kategorie plošných domén a subpopulací jsou bez klasifikačních pravidel." },
                        { nameof(msgNoClassificationRulesForAD),                          "Některé kategorie plošných domén jsou bez klasifikačních pravidel." },
                        { nameof(msgNoClassificationRulesForSP),                          "Některé kategorie subpopulací jsou bez klasifikačních pravidel." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionAttributeCategory),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnAreaDomainEdit),                                      "Edit area domain" },
                        { nameof(btnAreaDomainAdd),                                       "Create a new area domain" },
                        { nameof(btnAreaDomainDelete),                                    "Delete area domain" },
                        { nameof(btnAreaDomainExport),                                    "Export area domain classification rules into JSON file." },
                        { nameof(btnAreaDomainImport),                                    "Import area domain classification rules from JSON file." },
                        { nameof(btnAreaDomainSelect),                                    "Add area domain into selected area domains" },
                        { nameof(btnAreaDomainCancel),                                    "Remove area domain from selected area domains" },
                        { nameof(btnADCHierarchy),                                        "Adjust hierarchy of the area domain categories" },
                        { nameof(btnAreaDomainCategoryEdit),                              "Edit area domain category" },
                        { nameof(btnLimitTargetVariable),                                 "Restrict domain or population" },
                        { nameof(btnSubPopulationEdit),                                   "Edit subpopulation" },
                        { nameof(btnSubPopulationAdd),                                    "Create a new subpopulation" },
                        { nameof(btnSubPopulationDelete),                                 "Delete subpopulation" },
                        { nameof(btnSubPopulationExport),                                 "Export subpopulation classification rules into JSON file." },
                        { nameof(btnSubPopulationImport),                                 "Import subpopulation classification rules from JSON file." },
                        { nameof(btnSubPopulationSelect),                                 "Add subpopulation into selected subpopulations" },
                        { nameof(btnSubPopulationCancel),                                 "Remove subpopulation from selected subpopulations" },
                        { nameof(btnSPCHierarchy),                                        "Adjust hierarchy of the subpopulation categories" },
                        { nameof(btnSubPopulationCategoryEdit),                           "Edit subpopulation category" },
                        { nameof(btnSaveCategorizationSetup),                             "Save categorization setup" },
                        { nameof(btnPrevious),                                            "Previous" },
                        { nameof(btnNext),                                                "Next" },
                        { nameof(cboAreaDomain),                                          "ExtendedLabelEn" },
                        { nameof(cboSubPopulation),                                       "ExtendedLabelEn" },
                        { nameof(grpAreaDomain),                                          "Area domain:" },
                        { nameof(grpAreaDomainCategory),                                  "Area domain categories:" },
                        { nameof(grpAreaDomainSelected),                                  "Selected area domains:" },
                        { nameof(grpAreaDomainCategoryCombination),                       "Combinations of categories for selected area domains:" },
                        { nameof(grpSubPopulation),                                       "Subpopulation:" },
                        { nameof(grpSubPopulationCategory),                               "Subpopulation categories:" },
                        { nameof(grpSubPopulationSelected),                               "Selected subpopulations:" },
                        { nameof(grpSubPopulationCategoryCombination),                    "Combinations of categories for selected subpopulations:" },
                        { nameof(grpAttributeCategory),                                   "Resulting categorisation:" },
                        { nameof(lblMainCaption),                                         "Selection of local density classification" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),                    "Selected local density object group:" },
                        { nameof(lblSelectedTargetVariableCaption),                       "Selected target variable:" },
                        { nameof(lstAreaDomainCategory),                                  "ExtendedLabelEn" },
                        { nameof(lstSubPopulationCategory),                               "ExtendedLabelEn" },
                        { nameof(rdoAreaDomainStandard),                                  "standard" },
                        { nameof(rdoAreaDomainChangeOrDynamic),                           "change or dynamic" },
                        { nameof(rdoSubPopulationStandard),                               "standard" },
                        { nameof(rdoSubPopulationChangeOrDynamic),                        "change or dynamic" },
                        { nameof(tsrAreaDomain),                                          String.Empty },
                        { nameof(tsrAreaDomainSelect),                                    String.Empty },
                        { nameof(tsrAreaDomainSelected),                                  String.Empty },
                        { nameof(tsrAreaDomainCategory),                                  String.Empty },
                        { nameof(tsrSubPopulation),                                       String.Empty },
                        { nameof(tsrSubPopulationSelect),                                 String.Empty },
                        { nameof(tsrSubPopulationSelected),                               String.Empty },
                        { nameof(tsrSubPopulationCategory),                               String.Empty },
                        { nameof(msgNone),                                                "none" },
                        { nameof(msgNoneSelectedAreaDomain),                              "Area domain is not selected." },
                        { nameof(msgCannotDeleteSelectedAreaDomain),                      "Selected area domain cannot be deleted." },
                        { nameof(msgNoneSelectedSubPopulation),                           "Subpopulation is not selected." },
                        { nameof(msgCannotDeleteSelectedSubPopulation),                   "Selected subpopulation cannot be deleted." },
                        { nameof(msgNotEnoughAreaDomains),                                "At least two area domains must be selected to edit the hierarchy of area domain categories." },
                        { nameof(msgNotEnoughSubPopulations),                             "At least two subpopulation must be selected to edit the hierarchy of subpopulation categories." },
                        { nameof(msgMaxAreaDomainNumberReached),                          "The maximum number of selected area domains has been reached." },
                        { nameof(msgMaxSubPopulationNumberReached),                       "The maximum number of selected subpopulations has been reached." },
                        { nameof(msgTargetVariableNULL),                                  "Argument target_variable for stored procedure $1 must not be NULL." },
                        { nameof(msgLDsityObjectsForTargetVariableNULL),                  "Local density objects for selected target variable do not exist." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableNullKey),          "Stored procedure $1 has returned data table with NULL values in column key." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableNullValue),        "Stored procedure $1 has returned data table with NULL values in column value." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableDuplicateKey),     "Stored procedure $1 has returned data table with duplicate values in column key." },
                        { nameof(msgFnGetLDsityObjectsForTargetVariableDuplicateValue),   "Stored procedure $1 has returned data table with dupliacte values in column value." },
                        { nameof(msgFnGetCategorizationSetupNULL),                              String.Concat(
                                                                                                $"The stored procedure $1 returned $3. $0 $0",
                                                                                                $"Executed SQL statement: $0 $0 $2 $0") },
                        { nameof(msgNoClassificationRulesForADSP),                        "Some categories of area domains and subpopulations are without classification rules." },
                        { nameof(msgNoClassificationRulesForAD),                          "Some categories of area domains are without classification rules." },
                        { nameof(msgNoClassificationRulesForSP),                          "Some categories of subpopulations are without classification rules." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionAttributeCategory),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            btnPrevious.Click += new EventHandler(
                (sender, e) => { PreviousClick?.Invoke(sender: sender, e: e); });

            btnNext.Click += new EventHandler(
                (sender, e) => { NextClick?.Invoke(sender: sender, e: e); });

            rdoAreaDomainStandard.CheckedChanged += new EventHandler(
                (sender, e) => { if (rdoAreaDomainStandard.Checked) InitializeComboBoxAreaDomain(); });

            rdoAreaDomainChangeOrDynamic.CheckedChanged += new EventHandler(
                (sender, e) => { if (rdoAreaDomainChangeOrDynamic.Checked) InitializeComboBoxAreaDomain(); });

            rdoSubPopulationStandard.CheckedChanged += new EventHandler(
                (sender, e) => { if (rdoSubPopulationStandard.Checked) InitializeComboBoxSubPopulation(); });

            rdoSubPopulationChangeOrDynamic.CheckedChanged += new EventHandler(
                (sender, e) => { if (rdoSubPopulationChangeOrDynamic.Checked) InitializeComboBoxSubPopulation(); });

            cboAreaDomain.SelectedIndexChanged += new EventHandler(
                (sender, e) => { if (!onEdit) SelectAreaDomain(); });

            cboAreaDomain.Format += new ListControlConvertEventHandler(
                (sender, e) => { FormatAreaDomainItem(e: e, withClassificationType: false); });

            cboSubPopulation.SelectedIndexChanged += new EventHandler(
                (sender, e) => { if (!onEdit) SelectSubPopulation(); });

            cboSubPopulation.Format += new ListControlConvertEventHandler(
                (sender, e) => { FormatSubPopulationItem(e: e, withClassificationType: false); });

            splAreaDomain.SplitterMoved += new SplitterEventHandler(
                (sender, e) => { splSubPopulation.SplitterDistance = splAreaDomain.SplitterDistance; });

            splSubPopulation.SplitterMoved += new SplitterEventHandler(
                (sender, e) => { splAreaDomain.SplitterDistance = splSubPopulation.SplitterDistance; });

            ControlOwner = controlOwner;

            onEdit = false;

            categorizationSetupIds = null;

            ldsityObjectsForTargetVariable = null;

            selectedAreaDomainCategoryCombinations
                = new TDDomainCategoryCombinationList<TDAreaDomainCategory>(
                    database: Database,
                    items: null);

            selectedSubPopulationCategoryCombinations
                = new TDDomainCategoryCombinationList<TDSubPopulationCategory>(
                    database: Database,
                    items: null);

            selectedAttributeCategories
                = new TDAttributeCategoryList(
                    database: Database,
                    areaDomainCategoryCombinations: null,
                    subPopulationCategoryCombinations: null);

            btnLimitTargetVariable.Visible = false;

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            lblNotice.Text = String.Empty;

            splAreaDomainAndSubPopulation.Panel2Collapsed = !ClassifyBySubPopulations;

            // Controls:
            pnlAreaDomainSelected.Controls.Clear();
            LstSelectedAreaDomains =
                new ControlTDDomainListBox<TDAreaDomain, TDAreaDomainCategory>(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            LstSelectedAreaDomains.ResultChanged += TDDomainListBox_ResultChanged;
            pnlAreaDomainSelected.Controls.Add(value: LstSelectedAreaDomains);

            pnlSubPopulationSelected.Controls.Clear();
            LstSelectedSubPopulations =
                new ControlTDDomainListBox<TDSubPopulation, TDSubPopulationCategory>(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            LstSelectedSubPopulations.ResultChanged += TDDomainListBox_ResultChanged;
            pnlSubPopulationSelected.Controls.Add(value: LstSelectedSubPopulations);

            InitializeLabels();

            LoadContent();

            // Pozn. Metoda EnableButtonNext vyžaduje inicializaci jazyka (příkaz InitializeLabels();)
            // a nahrání obsahu databázových tabulek (příkaz LoadContent();)
            // Metoda provede inicializaci hodnoty CategorizationSetupIds
            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing the control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnAreaDomainEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainEdit),
                    out string btnAreaDomainEditText)
                ? btnAreaDomainEditText
                : String.Empty;

            btnAreaDomainAdd.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainAdd),
                    out string btnAreaDomainAddText)
                ? btnAreaDomainAddText
                : String.Empty;

            btnAreaDomainDelete.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainDelete),
                    out string btnAreaDomainDeleteText)
                ? btnAreaDomainDeleteText
                : String.Empty;

            btnAreaDomainExport.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainExport),
                    out string btnAreaDomainExportText)
                ? btnAreaDomainExportText
                : String.Empty;

            btnAreaDomainImport.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainImport),
                    out string btnAreaDomainImportText)
                ? btnAreaDomainImportText
                : String.Empty;

            btnAreaDomainSelect.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainSelect),
                    out string btnAreaDomainSelectText)
                ? btnAreaDomainSelectText
                : String.Empty;

            btnAreaDomainCancel.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainCancel),
                    out string btnAreaDomainCancelText)
                ? btnAreaDomainCancelText
                : String.Empty;

            btnADCHierarchy.Text =
                labels.TryGetValue(
                    key: nameof(btnADCHierarchy),
                    out string btnADCHierarchyText)
                ? btnADCHierarchyText
                : String.Empty;

            btnAreaDomainCategoryEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnAreaDomainCategoryEdit),
                    out string btnAreaDomainCategoryEditText)
                ? btnAreaDomainCategoryEditText
                : String.Empty;

            btnLimitTargetVariable.Text =
                labels.TryGetValue(
                    key: nameof(btnLimitTargetVariable),
                    out string btnLimitTargetVariableText)
                ? btnLimitTargetVariableText
                : String.Empty;

            btnSubPopulationEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnSubPopulationEdit),
                    out string btnSubPopulationEditText)
                ? btnSubPopulationEditText
                : String.Empty;

            btnSubPopulationAdd.Text =
                labels.TryGetValue(
                    key: nameof(btnSubPopulationAdd),
                    out string btnSubPopulationAddText)
                ? btnSubPopulationAddText
                : String.Empty;

            btnSubPopulationDelete.Text =
                labels.TryGetValue(
                    key: nameof(btnSubPopulationDelete),
                    out string btnSubPopulationDeleteText)
                ? btnSubPopulationDeleteText
                : String.Empty;

            btnSubPopulationExport.Text =
               labels.TryGetValue(
                   key: nameof(btnSubPopulationExport),
                   out string btnSubPopulationExportText)
               ? btnSubPopulationExportText
               : String.Empty;

            btnSubPopulationImport.Text =
                labels.TryGetValue(
                    key: nameof(btnSubPopulationImport),
                    out string btnSubPopulationImportText)
                ? btnSubPopulationImportText
                : String.Empty;

            btnSubPopulationSelect.Text =
                labels.TryGetValue(
                    key: nameof(btnSubPopulationSelect),
                    out string btnSubPopulationSelectText)
                ? btnSubPopulationSelectText
                : String.Empty;

            btnSubPopulationCancel.Text =
                labels.TryGetValue(
                    key: nameof(btnSubPopulationCancel),
                    out string btnSubPopulationCancelText)
                ? btnSubPopulationCancelText
                : String.Empty;

            btnSPCHierarchy.Text =
                labels.TryGetValue(
                    key: nameof(btnSPCHierarchy),
                    out string btnSPCHierarchyText)
                ? btnSPCHierarchyText
                : String.Empty;

            btnSubPopulationCategoryEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnSubPopulationCategoryEdit),
                    out string btnSubPopulationCategoryEditText)
                ? btnSubPopulationCategoryEditText
                : String.Empty;

            btnSaveCategorizationSetup.Text =
                labels.TryGetValue(
                    key: nameof(btnSaveCategorizationSetup),
                    out string btnSaveCategorizationSetupText)
                ? btnSaveCategorizationSetupText
                : String.Empty;

            btnPrevious.Text =
                labels.TryGetValue(
                    key: nameof(btnPrevious),
                    out string btnPreviousText)
                ? btnPreviousText
                : String.Empty;

            btnNext.Text =
                labels.TryGetValue(
                    key: nameof(btnNext),
                    out string btnNextText)
                ? btnNextText
                : String.Empty;

            grpAreaDomain.Text =
                labels.TryGetValue(
                    key: nameof(grpAreaDomain),
                    out string grpAreaDomainText)
                ? grpAreaDomainText
                : String.Empty;

            grpAreaDomainCategory.Text =
                labels.TryGetValue(
                    key: nameof(grpAreaDomainCategory),
                    out string grpAreaDomainCategoryText)
                ? grpAreaDomainCategoryText
                : String.Empty;

            grpAreaDomainSelected.Text =
                labels.TryGetValue(
                    key: nameof(grpAreaDomainSelected),
                    out string grpAreaDomainSelectedText)
                ? grpAreaDomainSelectedText
                : String.Empty;

            grpAreaDomainCategoryCombination.Text =
                labels.TryGetValue(
                    key: nameof(grpAreaDomainCategoryCombination),
                    out string grpAreaDomainCategoryCombinationText)
                ? grpAreaDomainCategoryCombinationText
                : String.Empty;

            grpSubPopulation.Text =
                labels.TryGetValue(
                    key: nameof(grpSubPopulation),
                    out string grpSubPopulationText)
                ? grpSubPopulationText
                : String.Empty;

            grpSubPopulationCategory.Text =
                labels.TryGetValue(
                    key: nameof(grpSubPopulationCategory),
                    out string grpSubPopulationCategoryText)
                ? grpSubPopulationCategoryText
                : String.Empty;

            grpSubPopulationSelected.Text =
                labels.TryGetValue(
                    key: nameof(grpSubPopulationSelected),
                    out string grpSubPopulationSelectedText)
                ? grpSubPopulationSelectedText
                : String.Empty;

            grpSubPopulationCategoryCombination.Text =
                labels.TryGetValue(
                    key: nameof(grpSubPopulationCategoryCombination),
                    out string grpSubPopulationCategoryCombinationText)
                ? grpSubPopulationCategoryCombinationText
                : String.Empty;

            grpAttributeCategory.Text =
                labels.TryGetValue(
                    key: nameof(grpAttributeCategory),
                    out string grpAttributeCategoryText)
                ? grpAttributeCategoryText
                : String.Empty;

            lblMainCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblMainCaption),
                    out string lblMainCaptionText)
                ? lblMainCaptionText
                : String.Empty;

            lblSelectedLDsityObjectGroupCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblSelectedLDsityObjectGroupCaption),
                    out string lblSelectedLDsityObjectGroupCaptionText)
                ? lblSelectedLDsityObjectGroupCaptionText
                : String.Empty;

            lblSelectedTargetVariableCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblSelectedTargetVariableCaption),
                    out string lblSelectedTargetVariableCaptionText)
                ? lblSelectedTargetVariableCaptionText
                : String.Empty;

            lblNotice.Text = String.Empty;

            rdoAreaDomainStandard.Text =
                labels.TryGetValue(
                    key: nameof(rdoAreaDomainStandard),
                    out string rdoAreaDomainStandardText)
                ? rdoAreaDomainStandardText
                : String.Empty;

            rdoAreaDomainChangeOrDynamic.Text =
               labels.TryGetValue(
                   key: nameof(rdoAreaDomainChangeOrDynamic),
                   out string rdoAreaDomainChangeOrDynamicText)
               ? rdoAreaDomainChangeOrDynamicText
               : String.Empty;

            rdoSubPopulationStandard.Text =
                labels.TryGetValue(
                    key: nameof(rdoSubPopulationStandard),
                    out string rdoSubPopulationStandardText)
                ? rdoSubPopulationStandardText
                : String.Empty;

            rdoSubPopulationChangeOrDynamic.Text =
                labels.TryGetValue(
                    key: nameof(rdoSubPopulationChangeOrDynamic),
                    out string rdoSubPopulationChangeOrDynamicText)
                ? rdoSubPopulationChangeOrDynamicText
                : String.Empty;

            tsrAreaDomain.Text =
                labels.TryGetValue(
                    key: nameof(tsrAreaDomain),
                    out string tsrAreaDomainText)
                ? tsrAreaDomainText
                : String.Empty;

            tsrAreaDomainSelect.Text =
                labels.TryGetValue(
                    key: nameof(tsrAreaDomainSelect),
                    out string tsrAreaDomainSelectText)
                ? tsrAreaDomainSelectText
                : String.Empty;

            tsrAreaDomainSelected.Text =
                labels.TryGetValue(
                    key: nameof(tsrAreaDomainSelected),
                    out string tsrAreaDomainSelectedText)
                ? tsrAreaDomainSelectedText
                : String.Empty;

            tsrAreaDomainCategory.Text =
                labels.TryGetValue(
                    key: nameof(tsrAreaDomainCategory),
                    out string tsrAreaDomainCategoryText)
                ? tsrAreaDomainCategoryText
                : String.Empty;

            tsrSubPopulation.Text =
                labels.TryGetValue(
                    key: nameof(tsrSubPopulation),
                    out string tsrSubPopulationText)
                ? tsrSubPopulationText
                : String.Empty;

            tsrSubPopulationSelect.Text =
                labels.TryGetValue(
                    key: nameof(tsrSubPopulationSelect),
                    out string tsrSubPopulationSelectText)
                ? tsrSubPopulationSelectText
                : String.Empty;

            tsrSubPopulationSelected.Text =
                labels.TryGetValue(
                    key: nameof(tsrSubPopulationSelected),
                    out string tsrSubPopulationSelectedText)
                ? tsrSubPopulationSelectedText
                : String.Empty;

            tsrSubPopulationCategory.Text =
                labels.TryGetValue(
                    key: nameof(tsrSubPopulationCategory),
                    out string tsrSubPopulationCategoryText)
                ? tsrSubPopulationCategoryText
                : String.Empty;

            msgNone =
                labels.TryGetValue(key: nameof(msgNone),
                        out msgNone)
                            ? msgNone
                            : String.Empty;

            msgNoneSelectedAreaDomain =
                labels.TryGetValue(key: nameof(msgNoneSelectedAreaDomain),
                        out msgNoneSelectedAreaDomain)
                            ? msgNoneSelectedAreaDomain
                            : String.Empty;

            msgCannotDeleteSelectedAreaDomain =
                labels.TryGetValue(key: nameof(msgCannotDeleteSelectedAreaDomain),
                        out msgCannotDeleteSelectedAreaDomain)
                            ? msgCannotDeleteSelectedAreaDomain
                            : String.Empty;

            msgNoneSelectedSubPopulation =
                labels.TryGetValue(key: nameof(msgNoneSelectedSubPopulation),
                        out msgNoneSelectedSubPopulation)
                            ? msgNoneSelectedSubPopulation
                            : String.Empty;

            msgCannotDeleteSelectedSubPopulation =
                labels.TryGetValue(key: nameof(msgCannotDeleteSelectedSubPopulation),
                        out msgCannotDeleteSelectedSubPopulation)
                            ? msgCannotDeleteSelectedSubPopulation
                            : String.Empty;

            msgNotEnoughAreaDomains =
                labels.TryGetValue(key: nameof(msgNotEnoughAreaDomains),
                        out msgNotEnoughAreaDomains)
                            ? msgNotEnoughAreaDomains
                            : String.Empty;

            msgNotEnoughSubPopulations =
                labels.TryGetValue(key: nameof(msgNotEnoughSubPopulations),
                        out msgNotEnoughSubPopulations)
                            ? msgNotEnoughSubPopulations
                            : String.Empty;

            msgMaxAreaDomainNumberReached =
                labels.TryGetValue(key: nameof(msgMaxAreaDomainNumberReached),
                        out msgMaxAreaDomainNumberReached)
                            ? msgMaxAreaDomainNumberReached
                            : String.Empty;

            msgMaxSubPopulationNumberReached =
                labels.TryGetValue(key: nameof(msgMaxSubPopulationNumberReached),
                        out msgMaxSubPopulationNumberReached)
                            ? msgMaxSubPopulationNumberReached
                            : String.Empty;

            msgTargetVariableNULL =
                labels.TryGetValue(key: nameof(msgTargetVariableNULL),
                        out msgTargetVariableNULL)
                            ? msgTargetVariableNULL
                            : String.Empty;

            msgLDsityObjectsForTargetVariableNULL =
                labels.TryGetValue(key: nameof(msgLDsityObjectsForTargetVariableNULL),
                        out msgLDsityObjectsForTargetVariableNULL)
                            ? msgLDsityObjectsForTargetVariableNULL
                            : String.Empty;

            msgFnGetLDsityObjectsForTargetVariableNullKey =
                labels.TryGetValue(key: nameof(msgFnGetLDsityObjectsForTargetVariableNullKey),
                        out msgFnGetLDsityObjectsForTargetVariableNullKey)
                            ? msgFnGetLDsityObjectsForTargetVariableNullKey
                            : String.Empty;

            msgFnGetLDsityObjectsForTargetVariableNullValue =
                labels.TryGetValue(key: nameof(msgFnGetLDsityObjectsForTargetVariableNullValue),
                        out msgFnGetLDsityObjectsForTargetVariableNullValue)
                            ? msgFnGetLDsityObjectsForTargetVariableNullValue
                            : String.Empty;

            msgFnGetLDsityObjectsForTargetVariableDuplicateKey =
                labels.TryGetValue(key: nameof(msgFnGetLDsityObjectsForTargetVariableDuplicateKey),
                        out msgFnGetLDsityObjectsForTargetVariableDuplicateKey)
                            ? msgFnGetLDsityObjectsForTargetVariableDuplicateKey
                            : String.Empty;

            msgFnGetLDsityObjectsForTargetVariableDuplicateValue =
                labels.TryGetValue(key: nameof(msgFnGetLDsityObjectsForTargetVariableDuplicateValue),
                        out msgFnGetLDsityObjectsForTargetVariableDuplicateValue)
                            ? msgFnGetLDsityObjectsForTargetVariableDuplicateValue
                            : String.Empty;

            msgFnGetCategorizationSetupNULL =
                labels.TryGetValue(key: nameof(msgFnGetCategorizationSetupNULL),
                        out msgFnGetCategorizationSetupNULL)
                            ? msgFnGetCategorizationSetupNULL
                            : String.Empty;

            msgNoClassificationRulesForADSP =
                labels.TryGetValue(key: nameof(msgNoClassificationRulesForADSP),
                        out msgNoClassificationRulesForADSP)
                            ? msgNoClassificationRulesForADSP
                            : String.Empty;

            msgNoClassificationRulesForAD =
                labels.TryGetValue(key: nameof(msgNoClassificationRulesForAD),
                        out msgNoClassificationRulesForAD)
                            ? msgNoClassificationRulesForAD
                            : String.Empty;

            msgNoClassificationRulesForSP =
                labels.TryGetValue(key: nameof(msgNoClassificationRulesForSP),
                        out msgNoClassificationRulesForSP)
                            ? msgNoClassificationRulesForSP
                            : String.Empty;

            onEdit = true;

            cboAreaDomain.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboAreaDomain),
                    out string cboAreaDomainDisplayMember)
                ? cboAreaDomainDisplayMember
                : ID;

            cboSubPopulation.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboSubPopulation),
                    out string cboSubPopulationDisplayMember)
                ? cboSubPopulationDisplayMember
                : ID;

            lstAreaDomainCategory.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstAreaDomainCategory),
                    out string lstAreaDomainCategoryDisplayMember)
                ? lstAreaDomainCategoryDisplayMember
                : ID;

            lstSubPopulationCategory.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstSubPopulationCategory),
                    out string lstSubPopulationCategoryDisplayMember)
                ? lstSubPopulationCategoryDisplayMember
                : ID;

            InitializeComboBoxAreaDomain();
            InitializeComboBoxSubPopulation();

            onEdit = false;

            LstSelectedAreaDomains.InitializeLabels();
            LstSelectedSubPopulations.InitializeLabels();

            selectedAreaDomainCategoryCombinations.DisplayInPanel(
                panel: pnlAreaDomainCategoryCombination,
                languageVersion: LanguageVersion);

            selectedSubPopulationCategoryCombinations.DisplayInPanel(
                panel: pnlSubPopulationCategoryCombination,
                languageVersion: LanguageVersion);

            selectedAttributeCategories.DisplayInPanel(
                panel: pnlAttributeCategory,
                languageVersion: LanguageVersion);

            btnLimitTargetVariable.Visible =
                selectedAttributeCategories.Items.Count != 0;

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Uploading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Database.STargetData.CAreaDomain
                = TDFunctions.FnGetAreaDomain.Execute(
                    database: Database,
                    areaDomainId: null,
                    targetVariableId: SelectedTargetVariable?.Id);

            Database.STargetData.CAreaDomainCategory
                = TDFunctions.FnGetAreaDomainCategoryForDomain.Execute(
                    database: Database,
                    areaDomainId: null);

            Database.STargetData.CClassificationType
                = TDFunctions.FnGetClassificationType.Execute(
                    database: Database,
                    targetVariableId: null);

            Database.STargetData.CSubPopulation
                = TDFunctions.FnGetSubPopulation.Execute(
                    database: Database,
                    subPopulationId: null,
                    targetVariableId: SelectedTargetVariable?.Id);

            Database.STargetData.CSubPopulationCategory
                = TDFunctions.FnGetSubPopulationCategoryForPopulation.Execute(
                    database: Database,
                    subPopulationId: null);

            Database.STargetData.TADCHierarchy.ReLoad(
                condition: null);

            Database.STargetData.TSPCHierarchy.ReLoad(
                condition: null);

            if (SelectedTargetVariableGroup == null)
            {
                ldsityObjectsForTargetVariable = null;
            }
            else if (SelectedTargetVariable == null)
            {
                ldsityObjectsForTargetVariable = null;
            }
            else
            {
                DataTable dt = TDFunctions.FnGetLDsityObjectsForTargetVariable.ExecuteQuery(
                        database: Database,
                        targetVariableId: SelectedTargetVariable.Id);

                ldsityObjectsForTargetVariable
                    = TDFunctions.FnGetLDsityObjectsForTargetVariable.Execute(
                        database: Database,
                        targetVariableId: SelectedTargetVariable.Id,
                        code: out TDFunctions.FnGetLDsityObjectsForTargetVariable.ErrorCode code);

                if (Setting.Verbose)
                {
                    string returns =
                        (LDsityObjectsForTargetVariable == null) ?
                            String.Empty :
                            LDsityObjectsForTargetVariable.Values
                                .Select(a => String.Concat(
                                    $"{a.LDsityObject.Id} - {a.LDsityObject.LabelCs} - ",
                                    $"{a.CmLDsityToTargetVariableId} - ",
                                    $"{a.LDsityObjectType.LabelCs} - ",
                                    $"negative: {a.UseNegative}"))
                                .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                    MessageBox.Show(
                        text: String.Concat(
                            $"{TDFunctions.FnGetLDsityObjectsForTargetVariable.CommandText}{Environment.NewLine}",
                            $"returns: {Environment.NewLine}{returns}"),
                        caption: TDFunctions.FnGetLDsityObjectsForTargetVariable.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                switch (code)
                {
                    case TDFunctions.FnGetLDsityObjectsForTargetVariable.ErrorCode.None:
                        break;

                    case TDFunctions.FnGetLDsityObjectsForTargetVariable.ErrorCode.NullKey:
                        // Uložená procedura fn_get_ldsity_objects4target_variable
                        // vrátila tabulku s NULL hodnotami ve sloupci key."
                        MessageBox.Show(
                            text: msgFnGetLDsityObjectsForTargetVariableNullKey
                                .Replace(oldValue: "$1", newValue: TDFunctions.FnGetLDsityObjectsForTargetVariable.Name),
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        break;

                    case TDFunctions.FnGetLDsityObjectsForTargetVariable.ErrorCode.NullValue:
                        // Uložená procedura fn_get_ldsity_objects4target_variable
                        // vrátila tabulku s NULL hodnotami ve sloupci value.
                        MessageBox.Show(
                            text: msgFnGetLDsityObjectsForTargetVariableNullValue
                                .Replace(oldValue: "$1", newValue: TDFunctions.FnGetLDsityObjectsForTargetVariable.Name),
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        break;

                    case TDFunctions.FnGetLDsityObjectsForTargetVariable.ErrorCode.DuplicateKey:
                        // Uložená procedura fn_get_ldsity_objects4target_variable
                        // vrátila tabulku s duplicitními hodnotami ve sloupci key.
                        MessageBox.Show(
                            text: msgFnGetLDsityObjectsForTargetVariableDuplicateKey
                                .Replace(oldValue: "$1", newValue: TDFunctions.FnGetLDsityObjectsForTargetVariable.Name),
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        break;

                    case TDFunctions.FnGetLDsityObjectsForTargetVariable.ErrorCode.DuplicateValue:
                        // Uložená procedura fn_get_ldsity_objects4target_variable
                        // vrátila tabulku s duplicitními hodnotami ve sloupci value.
                        MessageBox.Show(
                                text:
                                    msgFnGetLDsityObjectsForTargetVariableDuplicateValue
                                    .Replace(oldValue: "$1", newValue: TDFunctions.FnGetLDsityObjectsForTargetVariable.Name),
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        break;

                    default:
                        break;
                }
            }

            InitializeComboBoxAreaDomain();
            InitializeComboBoxSubPopulation();
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybere objekty lokálních hustot pro zobrazení ve druhém sloupci seznamu
        /// </para>
        /// <para lang="en">
        /// Select local density objects for display in second column of the list
        /// </para>
        /// </summary>
        /// <param name="domainType">
        /// <para lang="cs">Typ domény (plošná doména nebo subpopulace)</para>
        /// <para lang="en">Domain type (area domain or subpopulation)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Objekty lokálních hustot pro zobrazení ve druhém sloupci seznamu
        /// </para>
        /// <para lang="en">
        /// Local density objects for display in second column of the list
        /// </para>
        /// </returns>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            LDsityObjectsForTargetVariableSelect(TDArealOrPopulationEnum domainType)
        {

            if (LDsityObjectsForTargetVariable == null)
            {
                return null;
            }

            // TYP DOMÉNY (PLOŠNÁ DOMÉNA NEBO SUBPOPULACE)
            switch (domainType)
            {

                // TYP DOMÉNY (PLOŠNÁ DOMÉNA) -> VŽDY CORE
                case TDArealOrPopulationEnum.AreaDomain:
                    return
                        LDsityObjectsForTargetVariableCore;

                // TYP DOMÉNY (SUBPOPULACE)
                case TDArealOrPopulationEnum.Population:

                    // TYP CÍLOVÉ PROMĚNNÉ (PLOŠNÁ NEBO POPULAČNÍ)
                    switch (SelectedTargetVariableGroup.ArealOrPopulationValue)
                    {

                        // TYP CÍLOVÉ PROMĚNNÉ (PLOŠNÁ)
                        case TDArealOrPopulationEnum.AreaDomain:

                            // TŘÍDĚNÍ SUBPOPULACEMI
                            if (ClassifyBySubPopulations)
                            {
                                return
                                    LDsityObjectsForTargetVariableDivision;
                            }

                            // BEZ TŘÍDĚNÍ SUBPOPULACEMI
                            else
                            {
                                return
                                        LDsityObjectsForTargetVariableCore;
                            }

                        // TYP CÍLOVÉ PROMĚNNÉ (POPULAČNÍ) -> VŽDY CORE
                        case TDArealOrPopulationEnum.Population:
                            return
                                LDsityObjectsForTargetVariableCore;

                        default:
                            return null;
                    }

                default:
                    return null;
            }

        }

        /// <summary>
        /// <para lang="cs">
        /// Vybere příspěvky lokálních hustot pro kontroly klasifikačních pravidel
        /// </para>
        /// <para lang="en">
        /// Select local density contributions for classification rule checks
        /// </para>
        /// </summary>
        /// <param name="domainType">
        /// <para lang="cs">Typ domény (plošná doména nebo subpopulace)</para>
        /// <para lang="en">Domain type (area domain or subpopulation)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vybere příspěvky lokálních hustot pro kontroly klasifikačních pravidel
        /// </para>
        /// <para lang="en">
        /// Select Local density contributions for classification rule checks
        /// </para>
        /// </returns>
        public TDLDsityList LDsitiesForTargetVariableSelect(TDArealOrPopulationEnum domainType)
        {

            if (LDsityObjectsForTargetVariable == null)
            {
                return null;
            }

            // TYP DOMÉNY (PLOŠNÁ DOMÉNA NEBO SUBPOPULACE)
            switch (domainType)
            {

                // TYP DOMÉNY (PLOŠNÁ DOMÉNA) -> VŽDY CORE
                case TDArealOrPopulationEnum.AreaDomain:
                    return
                        TDFunctions.FnGetLDsity.Execute(
                            database: Database,
                            ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Core,
                            targetVariableIds: [SelectedTargetVariable.Id]);

                // TYP DOMÉNY (SUBPOPULACE)
                case TDArealOrPopulationEnum.Population:

                    // TYP CÍLOVÉ PROMĚNNÉ (PLOŠNÁ NEBO POPULAČNÍ)
                    switch (SelectedTargetVariableGroup.ArealOrPopulationValue)
                    {

                        // TYP CÍLOVÉ PROMĚNNÉ (PLOŠNÁ)
                        case TDArealOrPopulationEnum.AreaDomain:

                            // TŘÍDĚNÍ SUBPOPULACEMI
                            if (ClassifyBySubPopulations)
                            {
                                return
                                     TDFunctions.FnGetLDsity.Execute(
                                         database: Database,
                                         ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Division,
                                         targetVariableIds: [SelectedTargetVariable.Id]);
                            }

                            // BEZ TŘÍDĚNÍ SUBPOPULACEMI
                            else
                            {
                                return
                                        TDFunctions.FnGetLDsity.Execute(
                                            database: Database,
                                            ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Core,
                                            targetVariableIds: [SelectedTargetVariable.Id]);
                            }

                        // TYP CÍLOVÉ PROMĚNNÉ (POPULAČNÍ) -> VŽDY CORE
                        case TDArealOrPopulationEnum.Population:
                            return
                                        TDFunctions.FnGetLDsity.Execute(
                                            database: Database,
                                            ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Core,
                                            targetVariableIds: [SelectedTargetVariable.Id]);

                        default:
                            return null;
                    }

                default:
                    return null;
            }

        }


        /// <summary>
        /// <para lang="cs">Vrací položky pro ComboBox AreaDomain</para>
        /// <para lang="en">Returns items for ComboBox AreaDomain</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací položky pro ComboBox AreaDomain</para>
        /// <para lang="en">Returns items for ComboBox AreaDomain</para>
        /// </returns>
        private List<TDAreaDomain> GetComboBoxAreaDomainDataSource()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.International:
                    if (rdoAreaDomainStandard.Checked)
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.Standard)
                                .OrderBy(a => a.LabelEn)];
                    }
                    else if (rdoAreaDomainChangeOrDynamic.Checked)
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.ChangeOrDynamic)
                                .OrderBy(a => a.LabelEn)];
                    }
                    else
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items.OrderBy(a => a.LabelEn)];
                    }

                case LanguageVersion.National:
                    if (rdoAreaDomainStandard.Checked)
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.Standard)
                                .OrderBy(a => a.LabelCs)];
                    }
                    else if (rdoAreaDomainChangeOrDynamic.Checked)
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.ChangeOrDynamic)
                                .OrderBy(a => a.LabelCs)];
                    }
                    else
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items.OrderBy(a => a.LabelCs)];
                    }

                default:
                    if (rdoAreaDomainStandard.Checked)
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.Standard)
                                .OrderBy(a => a.Id)];
                    }
                    else if (rdoAreaDomainChangeOrDynamic.Checked)
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.ChangeOrDynamic)
                                .OrderBy(a => a.Id)];
                    }
                    else
                    {
                        return
                            [.. Database.STargetData.CAreaDomain.Items.OrderBy(a => a.Id)];
                    }
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení formátu popisků v ComboBox AreaDomain</para>
        /// <para lang="en">Sets format of labels in ComboBox AreaDomain</para>
        /// </summary>
        /// <param name="e">
        /// <para lang="cs">Data</para>
        /// <para lang="en">Data</para>
        /// </param>
        /// <param name="withClassificationType">
        /// <para lang="cs">Popisek obsahuje také název klasifikačního typu (ano/ne)</para>
        /// <para lang="en">Label contains classification type name too (true/false)</para>
        /// </param>
        private void FormatAreaDomainItem(
            ListControlConvertEventArgs e,
            bool withClassificationType)
        {
            if (e == null) { return; }
            if (e.ListItem == null) { return; }
            if (e.ListItem is not TDAreaDomain) { return; }

            TDAreaDomain itemAreaDomain = (TDAreaDomain)e.ListItem;
            e.Value =
                ((itemAreaDomain.ClassificationType == null) || (!withClassificationType))
                ? (LanguageVersion == LanguageVersion.National)
                    ? $"{itemAreaDomain.ExtendedLabelCs}"
                    : (LanguageVersion == LanguageVersion.International)
                        ? $"{itemAreaDomain.ExtendedLabelEn}"
                        : itemAreaDomain.Id.ToString()
                : (LanguageVersion == LanguageVersion.National)
                    ? $"{itemAreaDomain.ExtendedLabelCs} ({itemAreaDomain.ClassificationType.LabelCs})"
                    : (LanguageVersion == LanguageVersion.International)
                        ? $"{itemAreaDomain.ExtendedLabelEn} ({itemAreaDomain.ClassificationType.LabelEn})"
                        : itemAreaDomain.Id.ToString();
        }

        /// <summary>
        /// <para lang="cs">Do ComboBoxu AreaDomain vyplní seznam plošných domén</para>
        /// <para lang="en">It fills the AreaDomain ComboBox with a list of area domains</para>
        /// </summary>
        private void InitializeComboBoxAreaDomain()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            onEdit = true;
            cboAreaDomain.DataSource = null;
            cboAreaDomain.DataSource = GetComboBoxAreaDomainDataSource();
            cboAreaDomain.DisplayMember =
               labels.TryGetValue(
                   key: nameof(cboAreaDomain),
                   out string cboAreaDomainDisplayMember)
                        ? cboAreaDomainDisplayMember
                        : ID;
            onEdit = false;

            SelectAreaDomain();
        }


        /// <summary>
        /// <para lang="cs">Vrací položky ComboBox SubPopulation</para>
        /// <para lang="en">Returns items from ComboBox SubPopulation</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací položky ComboBox SubPopulation</para>
        /// <para lang="en">Returns items from ComboBox SubPopulation</para>
        /// </returns>
        private List<TDSubPopulation> GetComboBoxSubPopulationDataSource()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.International:
                    if (rdoSubPopulationStandard.Checked)
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.Standard)
                                .OrderBy(a => a.LabelEn)];
                    }
                    else if (rdoSubPopulationChangeOrDynamic.Checked)
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.ChangeOrDynamic)
                                .OrderBy(a => a.LabelEn)];
                    }
                    else
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items.OrderBy(a => a.LabelEn)];
                    }

                case LanguageVersion.National:
                    if (rdoSubPopulationStandard.Checked)
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.Standard)
                                .OrderBy(a => a.LabelCs)];
                    }
                    else if (rdoSubPopulationChangeOrDynamic.Checked)
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.ChangeOrDynamic)
                                .OrderBy(a => a.LabelCs)];
                    }
                    else
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items.OrderBy(a => a.LabelCs)];
                    }

                default:
                    if (rdoSubPopulationStandard.Checked)
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.Standard)
                                .OrderBy(a => a.Id)];
                    }
                    else if (rdoSubPopulationChangeOrDynamic.Checked)
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items
                                .Where(a => a.ClassificationType.Value == TDClassificationTypeEnum.ChangeOrDynamic)
                                .OrderBy(a => a.Id)];
                    }
                    else
                    {
                        return
                            [.. Database.STargetData.CSubPopulation.Items.OrderBy(a => a.Id)];
                    }
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení formátu popisků v ComboBox SubPopulation</para>
        /// <para lang="en">Sets format of labels in ComboBox SubPopulation</para>
        /// </summary>
        /// <param name="e">
        /// <para lang="cs">Data</para>
        /// <para lang="en">Data</para>
        /// </param>
        /// <param name="withClassificationType">
        /// <para lang="cs">Popisek obsahuje také název klasifikačního typu (ano/ne)</para>
        /// <para lang="en">Label contains classification type name too (true/false)</para>
        /// </param>
        private void FormatSubPopulationItem(
            ListControlConvertEventArgs e,
            bool withClassificationType)
        {
            if (e == null) { return; }
            if (e.ListItem == null) { return; }
            if (e.ListItem is not TDSubPopulation) { return; }

            TDSubPopulation itemSubPopulation = (TDSubPopulation)e.ListItem;
            e.Value =
                ((itemSubPopulation.ClassificationType == null) || (!withClassificationType))
                ? (LanguageVersion == LanguageVersion.National)
                    ? $"{itemSubPopulation.ExtendedLabelCs}"
                    : (LanguageVersion == LanguageVersion.International)
                        ? $"{itemSubPopulation.ExtendedLabelEn}"
                        : itemSubPopulation.Id.ToString()
                : (LanguageVersion == LanguageVersion.National)
                    ? $"{itemSubPopulation.ExtendedLabelCs} ({itemSubPopulation.ClassificationType.LabelCs})"
                    : (LanguageVersion == LanguageVersion.International)
                        ? $"{itemSubPopulation.ExtendedLabelEn} ({itemSubPopulation.ClassificationType.LabelEn})"
                        : itemSubPopulation.Id.ToString();
        }

        /// <summary>
        /// <para lang="cs">Do ComboBoxu SubPopulation vyplní seznam subpopulací</para>
        /// <para lang="en">It fills the SubPopulation ComboBox with a list of subpopulations</para>
        /// </summary>
        private void InitializeComboBoxSubPopulation()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            onEdit = true;
            cboSubPopulation.DataSource = null;
            cboSubPopulation.DataSource = GetComboBoxSubPopulationDataSource();
            cboSubPopulation.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboSubPopulation),
                    out string cboSubPopulationDisplayMember)
                ? cboSubPopulationDisplayMember
                : ID;
            onEdit = false;

            SelectSubPopulation();
        }


        /// <summary>
        /// <para lang="cs">Spouští se v případě, že v ComboBoxu AreaDomain byla vybrána plošná doména</para>
        /// <para lang="en">It triggers if an area domain has been selected in the AreaDomain ComboBox</para>
        /// </summary>
        private void SelectAreaDomain()
        {
            if (cboAreaDomain.SelectedItem == null)
            {
                InitializeListBoxAreaDomainCategory(
                    selectedAreaDomain: null);
            }
            else
            {
                InitializeListBoxAreaDomainCategory(
                    selectedAreaDomain: (TDAreaDomain)cboAreaDomain.SelectedItem);
            }
        }

        /// <summary>
        /// <para lang="cs">Spouští se v případě, že v ComboBoxu SubPopulation byla vybrána subpopulace</para>
        /// <para lang="en">It triggers if a subpopulation has been selected in the SubPopulation ComboBox</para>
        /// </summary>
        private void SelectSubPopulation()
        {
            if (cboSubPopulation.SelectedItem == null)
            {
                InitializeListBoxSubPopulationCategory(
                    selectedSubPopulation: null);
            }
            else
            {
                InitializeListBoxSubPopulationCategory(
                    selectedSubPopulation: (TDSubPopulation)cboSubPopulation.SelectedItem);
            }
        }


        /// <summary>
        /// <para lang="cs">Vrací položky pro ListBox AreaDomainCategory</para>
        /// <para lang="en">Returns items for ListBox AreaDomainCategory</para>
        /// </summary>
        /// <param name="selectedAreaDomain">
        /// <para lang="cs">Zvolená plošná doména</para>
        /// <para lang="en">Selected area domain</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací položky pro ListBox AreaDomainCategory</para>
        /// <para lang="en">Returns items for ListBox AreaDomainCategory</para>
        /// </returns>
        private List<TDAreaDomainCategory> GetListBoxAreaDomainCategoryDataSource(
            TDAreaDomain selectedAreaDomain)
        {
            List<TDAreaDomainCategory> result =
                (selectedAreaDomain == null)
                    ? []

                    : (LanguageVersion == LanguageVersion.National)
                        ? [.. Database.STargetData.CAreaDomainCategory.Items
                            .Where(a => a.AreaDomainId == selectedAreaDomain.Id)
                            .OrderBy(a => a.LabelCs)]

                    : (LanguageVersion == LanguageVersion.International)
                        ? [.. Database.STargetData.CAreaDomainCategory.Items
                            .Where(a => a.AreaDomainId == selectedAreaDomain.Id)
                            .OrderBy(a => a.LabelEn)]

                    : [.. Database.STargetData.CAreaDomainCategory.Items
                        .Where(a => a.AreaDomainId == selectedAreaDomain.Id)
                        .OrderBy(a => a.Id)];
            return result;
        }

        /// <summary>
        /// <para lang="cs">
        /// Do ListBoxu AreaDomainCategories vyplní
        /// seznam kategorií plošných domén pro zvolenou plošnou doménu
        /// </para>
        /// <para lang="en">
        /// The AreaDomainCategories ListBox is filled
        /// with a list of area domain categories for the selected area domain
        /// </para>
        /// </summary>
        /// <param name="selectedAreaDomain">
        /// <para lang="cs">Zvolená plošná doména</para>
        /// <para lang="en">Selected area domain</para>
        /// </param>
        private void InitializeListBoxAreaDomainCategory(TDAreaDomain selectedAreaDomain)
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            onEdit = true;

            lstAreaDomainCategory.DataSource =
                GetListBoxAreaDomainCategoryDataSource(
                    selectedAreaDomain: selectedAreaDomain);

            lstAreaDomainCategory.DisplayMember =
                    labels.TryGetValue(
                        key: nameof(lstAreaDomainCategory),
                        out string lstAreaDomainCategoryDisplayMember)
                            ? lstAreaDomainCategoryDisplayMember
                            : ID;

            onEdit = false;
        }


        /// <summary>
        /// <para lang="cs">Vrací položky pro ListBox SubPopulationCategory</para>
        /// <para lang="en">Returns items for ListBox SubPopulationCategory</para>
        /// </summary>
        /// <param name="selectedSubPopulation">
        /// <para lang="cs">Zvolená subpopulace</para>
        /// <para lang="en">Selected subpopulation</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací položky pro ListBox SubPopulationCategory</para>
        /// <para lang="en">Returns items for ListBox SubPopulationCategory</para>
        /// </returns>
        private List<TDSubPopulationCategory> GetListBoxSubPopulationCategoryDataSource(
            TDSubPopulation selectedSubPopulation)
        {
            return
                (selectedSubPopulation == null)
                    ? []

                    : (LanguageVersion == LanguageVersion.National)
                        ? [.. Database.STargetData.CSubPopulationCategory.Items
                            .Where(a => a.SubPopulationId == selectedSubPopulation.Id)
                            .OrderBy(a => a.LabelCs)]

                    : (LanguageVersion == LanguageVersion.International)
                        ? [.. Database.STargetData.CSubPopulationCategory.Items
                            .Where(a => a.SubPopulationId == selectedSubPopulation.Id)
                            .OrderBy(a => a.LabelEn)]

                    : Database.STargetData.CSubPopulationCategory.Items
                        .OrderBy(a => a.Id)
                        .Where(a => a.SubPopulationId == selectedSubPopulation.Id)
                        .ToList<TDSubPopulationCategory>();
        }

        /// <summary>
        /// <para lang="cs">
        /// Do ListBoxu SubPopulationCategories
        /// vyplní seznam kategorií subpopulací pro zvolenou subpopulaci
        /// </para>
        /// <para lang="en">
        /// The SubPopulationCategories ListBox is filled
        /// with a list of subpopulation categories for the selected subpopulation
        /// </para>
        /// </summary>
        /// <param name="selectedSubPopulation">
        /// <para lang="cs">Zvolená subpopulace</para>
        /// <para lang="en">Selected subpopulation</para>
        /// </param>
        private void InitializeListBoxSubPopulationCategory(TDSubPopulation selectedSubPopulation)
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            onEdit = true;

            lstSubPopulationCategory.DataSource =
                GetListBoxSubPopulationCategoryDataSource(
                    selectedSubPopulation: selectedSubPopulation);

            lstSubPopulationCategory.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstSubPopulationCategory),
                    out string lstSubPopulationCategoryDisplayMember)
                        ? lstSubPopulationCategoryDisplayMember
                        : ID;

            onEdit = false;
        }


        /// <summary>
        /// <para lang="cs">Nastaví nadpis ovládacího prvku</para>
        /// <para lang="en">Sets the control caption</para>
        /// </summary>
        private void SetCaption()
        {
            string selectedLocalDensityObjectGroupId;
            string selectedLocalDensityObjectGroupCaption;
            string selectedTargetVariableId;
            string selectedTargetVariableCaption;

            if (SelectedLocalDensityObjectGroup != null)
            {
                selectedLocalDensityObjectGroupId = SelectedLocalDensityObjectGroup.Id.ToString();
                selectedLocalDensityObjectGroupCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedLocalDensityObjectGroup.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedLocalDensityObjectGroup.LabelEn :
                    String.Empty;
                lblSelectedLDsityObjectGroupValue.Text = String.Concat(
                    $"({selectedLocalDensityObjectGroupId}) ",
                    $"{selectedLocalDensityObjectGroupCaption}");
            }
            else
            {
                lblSelectedLDsityObjectGroupValue.Text = msgNone;
            }

            if (SelectedTargetVariable != null)
            {
                selectedTargetVariableId = SelectedTargetVariable.Id.ToString();
                selectedTargetVariableCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedTargetVariable.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedTargetVariable.LabelEn : String.Empty;

                lblSelectedTargetVariableValue.Text = String.Concat(
                    $"({selectedTargetVariableId}) ",
                    $"{selectedTargetVariableCaption}");
            }
            else
            {
                lblSelectedTargetVariableValue.Text = msgNone;
            }
        }


        /// <summary>
        /// <para lang="cs">Existuje vybraná položka plošné domény v ComboBox</para>
        /// <para lang="en">Selected area domain item in ComboBox exists</para>
        /// </summary>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox</para>
        /// <para lang="en">Show MessageBox</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool ExistsAreaDomainItem(
            bool verbose = false)
        {
            if (cboAreaDomain.SelectedItem == null)
            {
                if (verbose)
                {
                    // V ComboBox není nic vybrané
                    // Nothing is selected in ComboBox
                    MessageBox.Show(
                        text: msgNoneSelectedAreaDomain,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }

            TDAreaDomain selectedAreaDomain
                = (TDAreaDomain)cboAreaDomain.SelectedItem;

            if (selectedAreaDomain == null)
            {
                if (verbose)
                {
                    // Žádná plošná doména není vybraná
                    // No area domain is selected
                    MessageBox.Show(
                        text: msgNoneSelectedAreaDomain,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }

            if (!Database.STargetData.CAreaDomain.Items
                    .Where(a => a.Id == selectedAreaDomain.Id)
                    .Any())
            {
                if (verbose)
                {
                    // Vybraná plošná doména není v databázové tabulce (nemůže nastat)
                    // The selected area domain is not in the database table (cannot occur)
                    MessageBox.Show(
                         text: msgNoneSelectedAreaDomain,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků plošné domény</para>
        /// <para lang="en">Editing area domain labels</para>
        /// </summary>
        private void UpdateAreaDomain()
        {
            if (!ExistsAreaDomainItem(verbose: true))
            {
                return;
            }

            TDAreaDomain selectedAreaDomain
                = (TDAreaDomain)cboAreaDomain.SelectedItem;

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CAreaDomain,
                displayedItemsIds: [selectedAreaDomain.Id]);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                LoadContent();

                // Výběr editované položky po aktualizaci databáze
                // Selecting an edited item after updating the database
                if (cboAreaDomain.Items
                        .OfType<TDAreaDomain>()
                        .Where(a => a.Id == selectedAreaDomain.Id)
                        .Any())
                {
                    cboAreaDomain.SelectedItem =
                        cboAreaDomain.Items
                        .OfType<TDAreaDomain>()
                        .Where(a => a.Id == selectedAreaDomain.Id)
                        .FirstOrDefault();
                }

                // Update seznamu
                LstSelectedAreaDomains.RefreshDomains();
            }
            else
            {
                // Dialog ukončen jinak než tlačítkem OK.
                // Dialogue terminated by other than the OK button.
            }
        }

        /// <summary>
        /// <para lang="cs">Vložení nové plošné domény</para>
        /// <para lang="en">Inserting a new area domain</para>
        /// </summary>
        private void InsertAreaDomain()
        {
            FormDomainNew<TDAreaDomain, TDAreaDomainCategory> frmNewAreaDomain
                = new(controlOwner: this);

            if (frmNewAreaDomain.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání vybrané plošné domény</para>
        /// <para lang="en">Deleting a selected area domain</para>
        /// </summary>
        private void DeleteAreaDomain()
        {
            if (!ExistsAreaDomainItem(verbose: true))
            {
                return;
            }

            TDAreaDomain selectedAreaDomain
                = (TDAreaDomain)cboAreaDomain.SelectedItem;

            if (!TDFunctions.FnTryDeleteAreaDomain.Execute(
                database: Database,
                areaDomainId: selectedAreaDomain.Id))
            {
                // Vybranou plošnou doménu nelze smazat
                // The selected area domain cannot be deleted
                MessageBox.Show(
                    text: msgCannotDeleteSelectedAreaDomain,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnTryDeleteAreaDomain.CommandText,
                        caption: TDFunctions.FnTryDeleteAreaDomain.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return;
            }

            TDFunctions.FnDeleteAreaDomain.Execute(
                database: Database,
                areaDomainId: selectedAreaDomain.Id);

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnTryDeleteAreaDomain.CommandText,
                    caption: TDFunctions.FnTryDeleteAreaDomain.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                MessageBox.Show(
                    text: TDFunctions.FnDeleteAreaDomain.CommandText,
                    caption: TDFunctions.FnDeleteAreaDomain.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            RemoveAreaDomainFromSelected(
                areaDomain: selectedAreaDomain);

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Export klasifikačních pravidel plošné domény do JSON souboru</para>
        /// <para lang="en">Export area domain classification rules into JSON file</para>
        /// </summary>
        private void ExportAreaDomainJSON()
        {
            if (!ExistsAreaDomainItem(verbose: true))
            {
                return;
            }

            TDAreaDomain selectedAreaDomain
                = (TDAreaDomain)cboAreaDomain.SelectedItem;

            List<TDAreaDomainCategory> selectedAreaDomainCategories =
                [.. Database.STargetData.CAreaDomainCategory.Items
                .Where(a => a.AreaDomainId == selectedAreaDomain.Id)
                .OrderBy(a => a.Id)];

            List<TDLDsityObject> ldsityObjects =
                [.. LDsityObjectsForTargetVariableSelect(
                    domainType: TDArealOrPopulationEnum.AreaDomain)
                .Values
                .Select(a => a.LDsityObject)
                .OrderBy(a => a.Id)];

            List<TDLDsityObject> ldsityObjectsForADSP =
                [.. ldsityObjects.Select(a =>
                    TDFunctions.FnGetLDsityObjectForADSP.Execute(
                        database: Database,
                        ldsityObjectId: a.Id,
                        arealOrPopulationId: (int)TDArealOrPopulationEnum.AreaDomain)
                    .Items)
                .SelectMany(a => a)
                .Distinct()
                .OrderBy(a => a.Id)];

            DataTable dtClassificationRules =
                ClassificationRuleCollection<TDAreaDomain, TDAreaDomainCategory>.EmptyDataTable();

            foreach (TDAreaDomainCategory category in selectedAreaDomainCategories)
            {
                foreach (TDLDsityObject ldsityObjectADSP in ldsityObjectsForADSP)
                {
                    DataRow row = dtClassificationRules.NewRow();

                    Functions.SetIntArg(row: row, name: "Category", val: category.Id);

                    Functions.SetIntArg(row: row, name: "LDsityObjectADSP", val: ldsityObjectADSP.Id);

                    Functions.SetStringArg(row: row, name: "PositiveRule",
                        val: TDFunctions.FnGetClassificationRuleForAdc.Execute(
                                    database: Database,
                                    areaDomainCategoryId: category.Id,
                                    ldsityObjectId: ldsityObjectADSP.Id,
                                    useNegative: false)
                                .Items.FirstOrDefault<TDADCToClassificationRule>()?
                                .ClassificationRule);

                    Functions.SetStringArg(row: row, name: "NegativeRule",
                        val: TDFunctions.FnGetClassificationRuleForAdc.Execute(
                                    database: Database,
                                    areaDomainCategoryId: category.Id,
                                    ldsityObjectId: ldsityObjectADSP.Id,
                                    useNegative: true)
                                .Items.FirstOrDefault<TDADCToClassificationRule>()?
                                .ClassificationRule);

                    dtClassificationRules.Rows.Add(row: row);
                }
            }

            ClassificationRuleCollection<TDAreaDomain, TDAreaDomainCategory> collection
                = new(
                    domain: selectedAreaDomain,
                    categories: selectedAreaDomainCategories,
                    ldsityObjectsForADSP: ldsityObjectsForADSP,
                    dtClassificationRules: dtClassificationRules);

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            SaveFileDialog sfd = new()
            {
                DefaultExt = "json",
                Filter = "Json files (*.json)|*.json",
                FileName = $"{selectedAreaDomain.Id}.json",
                InitialDirectory = Setting.AreaDomainJSONFolder,
                RestoreDirectory = true,
                Title =
                    labels.TryGetValue(
                        key: nameof(btnAreaDomainExport),
                        out string btnAreaDomainExportText)
                    ? btnAreaDomainExportText
                    : String.Empty
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetFullPath(sfd.FileName);
                Setting.AreaDomainJSONFolder = System.IO.Path.GetDirectoryName(path: path);
                collection.SaveToFile(path: path);
            }

            return;
        }

        /// <summary>
        /// <para lang="cs">Import klasifikačních pravidel plošné domény z JSON souboru</para>
        /// <para lang="en">Import area domain classification rules from JSON file</para>
        /// </summary>
        private void ImportAreaDomainJSON()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            OpenFileDialog openFileDialog = new()
            {
                DefaultExt = "json",
                Filter = "Json files (*.json)|*.json",
                InitialDirectory = Setting.AreaDomainJSONFolder,
                RestoreDirectory = true,
                Title =
                    labels.TryGetValue(
                        key: nameof(btnAreaDomainImport),
                        out string btnAreaDomainImportText)
                    ? btnAreaDomainImportText
                    : String.Empty
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetFullPath(path: openFileDialog.FileName);
                Setting.AreaDomainJSONFolder = System.IO.Path.GetDirectoryName(path: path);

                ClassificationRuleCollection<TDAreaDomain, TDAreaDomainCategory> collection =
                    ClassificationRuleCollection<TDAreaDomain, TDAreaDomainCategory>.LoadFromFile(path: path);

                if (collection != null)
                {
                    FormDomainFromJson<TDAreaDomain, TDAreaDomainCategory> frmPairing =
                        new(controlOwner: this)
                        {
                            LDsityObjects =
                                [.. LDsityObjectsForTargetVariableSelect(
                                    domainType: TDArealOrPopulationEnum.AreaDomain)
                                        .Values
                                        .Select(a => a.LDsityObject)
                                        .Distinct()
                                        .OrderBy(a => a.Id)],
                            Collection = collection
                        };

                    if (frmPairing.ShowDialog() == DialogResult.OK)
                    {
                        FormDomainNew<TDAreaDomain, TDAreaDomainCategory> frmNewAreaDomain
                             = new(controlOwner: this)
                             {
                                 Collection = collection,
                                 Pairing = frmPairing.Pairing
                             };

                        if (frmNewAreaDomain.ShowDialog() == DialogResult.OK)
                        {
                            LoadContent();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků kategorie plošné domény</para>
        /// <para lang="en">Editing area domain category labels</para>
        /// </summary>
        private void UpdateAreaDomainCategory()
        {
            if ((lstAreaDomainCategory.DataSource == null) ||
                (!lstAreaDomainCategory.Items
                .OfType<TDAreaDomainCategory>()
                .Any()))
            {
                // Seznam je prázdný
                // List is empty
                return;
            }

            FormLookupTable frmLookupTable = new(
                    controlOwner: this,
                    lookupTable: Database.STargetData.CAreaDomainCategory,
                    displayedItemsIds: lstAreaDomainCategory.Items
                                        .OfType<TDAreaDomainCategory>()
                                        .Select(a => (Nullable<int>)a.Id)
                                        .ToList<Nullable<int>>());

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
                InitializeListBoxAreaDomainCategoryCombinations();
            }
            else
            {
                // Dialog ukončen jinak než tlačítkem OK.
                // Dialogue terminated by other than the OK button.
            }
        }

        /// <summary>
        /// <para lang="cs">Přidá plošnou doménu mezi vybrané</para>
        /// <para lang="en">It adds the area domain to the selected ones</para>
        /// </summary>
        private void AddAreaDomainIntoSelected()
        {
            if (!ExistsAreaDomainItem(verbose: true))
            {
                return;
            }

            TDAreaDomain selectedAreaDomain
                 = (TDAreaDomain)cboAreaDomain.SelectedItem;

            if (LstSelectedAreaDomains.Domains.Count >= maxSelectedDomains)
            {
                // Dosaženo maximálního počtu plošných domén v seznamu
                // Maximal number of selected area domains in list was reached
                MessageBox.Show(
                    text: msgMaxAreaDomainNumberReached,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (LstSelectedAreaDomains.Items.ContainsKey(key: selectedAreaDomain))
            {
                // Seznam již vybranou plošnou doménu obsahuje
                // The list already contains the selected area domain
                return;
            }

            LstSelectedAreaDomains.Add(
                domain: selectedAreaDomain);

            InitializeListBoxAreaDomainCategoryCombinations();

            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Odstraní plošnou doménu z vybraných</para>
        /// <para lang="en">It removes the area domain from the selected ones</para>
        /// </summary>
        /// <param name="areaDomain">
        /// <para lang="cs">Vybraná plošná doména pro odstranění</para>
        /// <para lang="en">Selected area domain for remove</para>
        /// </param>
        private void RemoveAreaDomainFromSelected(TDAreaDomain areaDomain)
        {
            if (areaDomain == null)
            {
                // V seznamu není vybraná žádná plošná doména, která by se dala odstranit
                // There is no area domain selected in the list that can be deleted
                return;
            }

            if (!LstSelectedAreaDomains.Items.ContainsKey(key: areaDomain))
            {
                // Vybraná plošná doména pro odstranění v seznamu není
                // The area domain selected for deletion is not in the list
                return;
            }

            LstSelectedAreaDomains.Select(domain: areaDomain);
            LstSelectedAreaDomains.Remove();

            InitializeListBoxAreaDomainCategoryCombinations();

            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Do ListBoxu vyplní seznam kombinací kategorií plošných domén
        /// pro zvolený seznam plošných domén (selectedAreaDomains)</para>
        /// <para lang="en">It fills the ListBox with a list of combinations of area domain categories
        /// for the selected list of area domains (selectedAreaDomains)</para>
        /// </summary>
        private void InitializeListBoxAreaDomainCategoryCombinations()
        {
            if (LstSelectedAreaDomains.Domains.Count != 0)
            {
                selectedAreaDomainCategoryCombinations =
                    TDFunctions.FnGetAttributeDomainAD.Execute(
                        database: Database,
                        areaDomains: LstSelectedAreaDomains.Domains);
            }
            else
            {
                selectedAreaDomainCategoryCombinations =
                    new TDDomainCategoryCombinationList<TDAreaDomainCategory>(
                        database: Database,
                        items: null);
            }

            selectedAreaDomainCategoryCombinations.DisplayInPanel(
                panel: pnlAreaDomainCategoryCombination,
                languageVersion: LanguageVersion);

            InitializeListBoxAttributeCategory();
        }

        /// <summary>
        /// <para lang="cs">Zobrazí formulář pro úpravu hierarchie kategorií plošných domén</para>
        /// <para lang="en">It displays a form for adjusting the hierarchy of the area domain categories</para>
        /// </summary>
        private void AdjustAreaDomainCategoryHierarchy()
        {
            if (LstSelectedAreaDomains.Domains.Count < 2)
            {
                // Pro definici hierachie kategorií, musí být vybrány minimálně dvě domény
                // mezi, kterými se definuje hierachie kategorií
                MessageBox.Show(
                    text: msgNotEnoughAreaDomains,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            List<List<TDAreaDomainCategory>> selectedAreaDomainCategories = [];
            List<List<CategoryPair<TDAreaDomain, TDAreaDomainCategory>>> selectedAreaDomainCategoryPairs = [];

            foreach (TDAreaDomain areaDomain in LstSelectedAreaDomains.Domains)
            {
                selectedAreaDomainCategories.Add(
                    item: Database.STargetData.CAreaDomainCategory.Items
                        .Where(a => a.AreaDomainId == areaDomain.Id)
                        .ToList<TDAreaDomainCategory>());
            }

            for (int i = 0; i < LstSelectedAreaDomains.Domains.Count - 1; i++)
            {
                TDAreaDomain superiorAD = LstSelectedAreaDomains.Domains[i];
                TDAreaDomain inferiorAD = LstSelectedAreaDomains.Domains[i + 1];
                List<TDAreaDomainCategory> superiorADCs =
                    Database.STargetData.CAreaDomainCategory.Items
                        .Where(a => a.AreaDomainId == superiorAD.Id)
                        .ToList<TDAreaDomainCategory>();
                List<TDAreaDomainCategory> inferiorADCs =
                        Database.STargetData.CAreaDomainCategory.Items
                        .Where(a => a.AreaDomainId == inferiorAD.Id)
                        .ToList<TDAreaDomainCategory>();

                List<CategoryPair<TDAreaDomain, TDAreaDomainCategory>> pairs
                    = [];
                foreach (TDADCHierarchy adc in
                    Database.STargetData.TADCHierarchy.Items
                    .Where(a => superiorADCs.Select(b => b.Id).Contains(value: a.VariableSuperiorId))
                    .Where(a => inferiorADCs.Select(b => b.Id).Contains(value: a.VariableId)))
                {
                    pairs.Add(new CategoryPair<TDAreaDomain, TDAreaDomainCategory>(
                        superior: adc.VariableSuperior,
                        inferior: adc.Variable));
                }
                selectedAreaDomainCategoryPairs.Add(item: pairs);
            }

            FormHierarchy<TDAreaDomain, TDAreaDomainCategory> frm =
                new(
                    controlOwner: this,
                    domains: LstSelectedAreaDomains.Domains);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (SuperiorDomain<TDAreaDomain, TDAreaDomainCategory> sad in frm.Domains)
                {
                    sad.WriteToDatabase();
                }
            }
            else
            {
                // Dialog ukončen jinak než tlačítkem OK.
                // Dialogue terminated by other than the OK button.
            }
        }


        /// <summary>
        /// <para lang="cs">Existuje vybraná položka subpopulace v ComboBox</para>
        /// <para lang="en">Selected subpopulation item in ComboBox exists</para>
        /// </summary>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazovat MessageBox</para>
        /// <para lang="en">Show MessageBox</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano/ne</para>
        /// <para lang="en">true/false</para>
        /// </returns>
        private bool ExistsSubPopulationItem(
            bool verbose = false)
        {
            if (cboSubPopulation.SelectedItem == null)
            {
                if (verbose)
                {
                    // v ComboBox není nic vybrané
                    // nothing is selected in ComboBox
                    MessageBox.Show(
                       text: msgNoneSelectedSubPopulation,
                       caption: String.Empty,
                       buttons: MessageBoxButtons.OK,
                       icon: MessageBoxIcon.Information);
                }
                return false;
            }

            TDSubPopulation selectedSubPopulation
                = (TDSubPopulation)cboSubPopulation.SelectedItem;

            if (selectedSubPopulation == null)
            {
                if (verbose)
                {
                    // Žádná subpopulace není vybraná
                    // No subpopulation is selected
                    MessageBox.Show(
                        text: msgNoneSelectedSubPopulation,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }

            if (!Database.STargetData.CSubPopulation.Items
                .Where(a => a.Id == selectedSubPopulation.Id)
                .Any())
            {
                if (verbose)
                {
                    // Vybraná subpopulace není v databázové tabulce (nemůže nastat)
                    // The selected subpopulation is not in the database table (cannot occur)
                    MessageBox.Show(
                        text: msgNoneSelectedSubPopulation,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků subpopulace</para>
        /// <para lang="en">Editing the subpopulation labels</para>
        /// </summary>
        private void UpdateSubPopulation()
        {
            if (!ExistsSubPopulationItem(verbose: true))
            {
                return;
            }

            TDSubPopulation selectedSubPopulation
                = (TDSubPopulation)cboSubPopulation.SelectedItem;

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CSubPopulation,
                displayedItemsIds: [selectedSubPopulation.Id]);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                LoadContent();

                // Výběr editované položky po aktualizaci databáze
                // Selecting an edited item after updating the database
                if (cboSubPopulation.Items
                        .OfType<TDSubPopulation>()
                        .Where(a => a.Id == selectedSubPopulation.Id)
                        .Any())
                {
                    cboSubPopulation.SelectedItem =
                        cboSubPopulation.Items
                        .OfType<TDSubPopulation>()
                        .Where(a => a.Id == selectedSubPopulation.Id)
                        .FirstOrDefault();
                }

                // Update seznamu
                LstSelectedSubPopulations.RefreshDomains();
            }
            else
            {
                // Dialog ukončen jinak než tlačítkem OK.
                // Dialogue terminated by other than the OK button.
            }
        }

        /// <summary>
        /// <para lang="cs">Vložení nové subpopulace</para>
        /// <para lang="en">Inserting a new subpopulation</para>
        /// </summary>
        private void InsertSubPopulation()
        {
            FormDomainNew<TDSubPopulation, TDSubPopulationCategory> frmNewSubPopulation
                = new(
                    controlOwner: this);

            if (frmNewSubPopulation.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání vybrané subpopulace</para>
        /// <para lang="en">Deleting the selected subpopulation</para>
        /// </summary>
        private void DeleteSubPopulation()
        {
            if (!ExistsSubPopulationItem(verbose: true))
            {
                return;
            }

            TDSubPopulation selectedSubPopulation
                = (TDSubPopulation)cboSubPopulation.SelectedItem;

            if (!TDFunctions.FnTryDeleteSubPopulation.Execute(
                    database: Database,
                    subPopulationId: selectedSubPopulation.Id))
            {
                // Vybranou subpopulaci nelze smazat
                // The selected subpopulation cannot be deleted
                MessageBox.Show(
                    text: msgCannotDeleteSelectedSubPopulation,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnTryDeleteSubPopulation.CommandText,
                        caption: TDFunctions.FnTryDeleteSubPopulation.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return;
            }

            TDFunctions.FnDeleteSubPopulation.Execute(
                database: Database,
                subPopulationId: selectedSubPopulation.Id);

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnTryDeleteSubPopulation.CommandText,
                    caption: TDFunctions.FnTryDeleteSubPopulation.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                MessageBox.Show(
                    text: TDFunctions.FnDeleteSubPopulation.CommandText,
                    caption: TDFunctions.FnDeleteSubPopulation.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            RemoveSubPopulationFromSelected(
                subPopulation: selectedSubPopulation);

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Export klasifikačních pravidel subpopulace do JSON souboru</para>
        /// <para lang="en">Export subpopulation classification rules into JSON file</para>
        /// </summary>
        private void ExportSubPopulationJSON()
        {
            if (!ExistsSubPopulationItem(verbose: true))
            {
                return;
            }

            TDSubPopulation selectedSubPopulation
                = (TDSubPopulation)cboSubPopulation.SelectedItem;

            List<TDSubPopulationCategory> selectedSubPopulationCategories =
                Database.STargetData.CSubPopulationCategory.Items
                .Where(a => a.SubPopulationId == selectedSubPopulation.Id)
                .ToList<TDSubPopulationCategory>();

            List<TDLDsityObject> ldsityObjects =
                LDsityObjectsForTargetVariableSelect(
                    domainType: TDArealOrPopulationEnum.Population)
                .Values
                .Select(a => a.LDsityObject)
                .ToList<TDLDsityObject>();

            List<TDLDsityObject> ldsityObjectsForADSP =
                ldsityObjects.Select(a =>
                    TDFunctions.FnGetLDsityObjectForADSP.Execute(
                        database: Database,
                        ldsityObjectId: a.Id,
                        arealOrPopulationId: (int)TDArealOrPopulationEnum.Population)
                    .Items)
                .SelectMany(a => a)
                .Distinct()
                .ToList<TDLDsityObject>();

            DataTable dtClassificationRules =
               ClassificationRuleCollection<TDSubPopulation, TDSubPopulationCategory>.EmptyDataTable();

            foreach (TDSubPopulationCategory category in selectedSubPopulationCategories)
            {
                foreach (TDLDsityObject ldsityObjectADSP in ldsityObjectsForADSP)
                {
                    DataRow row = dtClassificationRules.NewRow();

                    Functions.SetIntArg(row: row, name: "Category", val: category.Id);

                    Functions.SetIntArg(row: row, name: "LDsityObjectADSP", val: ldsityObjectADSP.Id);

                    Functions.SetStringArg(row: row, name: "PositiveRule",
                        val: TDFunctions.FnGetClassificationRuleForSpc.Execute(
                                    database: Database,
                                    subPopulationCategoryId: category.Id,
                                    ldsityObjectId: ldsityObjectADSP.Id,
                                    useNegative: false)
                                .Items.FirstOrDefault<TDSPCToClassificationRule>()?
                                .ClassificationRule);

                    Functions.SetStringArg(row: row, name: "NegativeRule",
                        val: TDFunctions.FnGetClassificationRuleForSpc.Execute(
                                    database: Database,
                                    subPopulationCategoryId: category.Id,
                                    ldsityObjectId: ldsityObjectADSP.Id,
                                    useNegative: true)
                                .Items.FirstOrDefault<TDSPCToClassificationRule>()?
                                .ClassificationRule);

                    dtClassificationRules.Rows.Add(row: row);
                }
            }

            ClassificationRuleCollection<TDSubPopulation, TDSubPopulationCategory> collection
                = new(
                    domain: selectedSubPopulation,
                    categories: selectedSubPopulationCategories,
                    ldsityObjectsForADSP: ldsityObjectsForADSP,
                    dtClassificationRules: dtClassificationRules);

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            SaveFileDialog sfd = new()
            {
                DefaultExt = "json",
                Filter = "Json files (*.json)|*.json",
                FileName = $"{selectedSubPopulation.Id}.json",
                InitialDirectory = Setting.SubPopulationJSONFolder,
                RestoreDirectory = true,
                Title =
                    labels.TryGetValue(
                        key: nameof(btnSubPopulationExport),
                        out string btnSubPopulationExportText)
                            ? btnSubPopulationExportText
                            : String.Empty
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetFullPath(sfd.FileName);
                Setting.SubPopulationJSONFolder = System.IO.Path.GetDirectoryName(path: path);
                collection.SaveToFile(path: path);
            }

            return;
        }

        /// <summary>
        /// <para lang="cs">Import klasifikačních pravidel subpopulace z JSON souboru</para>
        /// <para lang="en">Import subpopulation classification rules from JSON file</para>
        /// </summary>
        private void ImportSubPopulationJSON()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            OpenFileDialog openFileDialog = new()
            {
                DefaultExt = "json",
                Filter = "Json files (*.json)|*.json",
                InitialDirectory = Setting.SubPopulationJSONFolder,
                RestoreDirectory = true,
                Title =
                    labels.TryGetValue(
                        key: "btnSubPopulationImport",
                        out string btnSubPopulationImportText)
                            ? btnSubPopulationImportText
                            : String.Empty
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetFullPath(path: openFileDialog.FileName);
                Setting.SubPopulationJSONFolder = System.IO.Path.GetDirectoryName(path: path);

                ClassificationRuleCollection<TDSubPopulation, TDSubPopulationCategory> collection =
                    ClassificationRuleCollection<TDSubPopulation, TDSubPopulationCategory>.LoadFromFile(path: path);

                if (collection != null)
                {
                    FormDomainFromJson<TDSubPopulation, TDSubPopulationCategory> frmPairing =
                        new(controlOwner: this)
                        {
                            LDsityObjects =
                                [.. LDsityObjectsForTargetVariableSelect(
                                    domainType: TDArealOrPopulationEnum.Population)
                                        .Values
                                        .Select(a => a.LDsityObject)
                                        .Distinct()
                                        .OrderBy(a => a.Id)],
                            Collection = collection
                        };

                    if (frmPairing.ShowDialog() == DialogResult.OK)
                    {
                        FormDomainNew<TDSubPopulation, TDSubPopulationCategory> frmNewSubPopulation
                             = new(controlOwner: this)
                             {
                                 Collection = collection,
                                 Pairing = frmPairing.Pairing
                             };

                        if (frmNewSubPopulation.ShowDialog() == DialogResult.OK)
                        {
                            LoadContent();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků kategorie subpopulace</para>
        /// <para lang="en">Editing the subpopulation category labels</para>
        /// </summary>
        private void UpdateSubPopulationCategory()
        {
            if ((lstSubPopulationCategory == null) ||
                (!lstSubPopulationCategory.Items.OfType<TDSubPopulationCategory>().Any()))
            {
                // Seznam je prázdný
                // List is empty
                return;
            }

            FormLookupTable frmLookupTable = new(
                    controlOwner: this,
                    lookupTable: Database.STargetData.CSubPopulationCategory,
                    displayedItemsIds: lstSubPopulationCategory.Items
                                        .OfType<TDSubPopulationCategory>()
                                        .Select(a => (Nullable<int>)a.Id)
                                        .ToList<Nullable<int>>());

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
                InitializeListBoxSubPopulationCategoryCombinations();
            }
            else
            {
                // Dialog ukončen jinak než tlačítkem OK.
                // Dialogue terminated by other than the OK button.
            }
        }

        /// <summary>
        /// <para lang="cs">Přidá subpopulaci mezi vybrané</para>
        /// <para lang="en">It adds the subpopulation to the selected ones</para>
        /// </summary>
        private void AddSubPopulationIntoSelected()
        {
            if (!ExistsSubPopulationItem(verbose: true))
            {
                return;
            }

            TDSubPopulation selectedSubPopulation
                = (TDSubPopulation)cboSubPopulation.SelectedItem;

            if (LstSelectedSubPopulations.Domains.Count >= maxSelectedDomains)
            {
                // Dosaženo maximálního počtu subpopulací v seznamu
                // Maximal number of selected subpopulations in list was reached
                MessageBox.Show(
                    text: msgMaxSubPopulationNumberReached,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (LstSelectedSubPopulations.Items.ContainsKey(key: selectedSubPopulation))
            {
                // Seznam již vybranou subpopulaci obsahuje
                // The list already contains the selected subpopulation
                return;
            }

            LstSelectedSubPopulations.Add(
                domain: selectedSubPopulation);

            InitializeListBoxSubPopulationCategoryCombinations();

            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Odstraní subpopulaci z vybraných</para>
        /// <para lang="en">It removes the subpopulation from the selected ones</para>
        /// </summary>
        /// <param name="subPopulation">
        /// <para lang="cs">Vybraná subpopulace pro odstranění</para>
        /// <para lang="en">Selected subpopulation for remove</para>
        /// </param>
        private void RemoveSubPopulationFromSelected(TDSubPopulation subPopulation)
        {
            if (subPopulation == null)
            {
                // V seznamu není vybraná žádná subpopulace, která by se dala odstranit
                // There is no subpopulation selected in the list that can be deleted
                return;
            }

            if (!LstSelectedSubPopulations.Items.ContainsKey(key: subPopulation))
            {
                // Vybraná subpopulace pro odstranění v seznamu není
                // The subpopulation selected for deletion is not in the list
                return;
            }

            LstSelectedSubPopulations.Select(domain: subPopulation);
            LstSelectedSubPopulations.Remove();

            InitializeListBoxSubPopulationCategoryCombinations();

            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Do ListBoxu vyplní seznam kombinací kategorií subpopulací
        /// pro zvolený seznam subpopulací (selectedSubPopulations)</para>
        /// <para lang="en">It fills the ListBox with a list of combinations of subpopulation categories
        /// for the selected list of subpopulations (selectedSubPopulations)</para>
        /// </summary>
        private void InitializeListBoxSubPopulationCategoryCombinations()
        {
            if (LstSelectedSubPopulations.Items.Keys.Count != 0)
            {
                selectedSubPopulationCategoryCombinations =
                    TDFunctions.FnGetAttributeDomainSP.Execute(
                        database: Database,
                        subPopulations: LstSelectedSubPopulations.Items
                                            .Select(a => a.Key)
                                            .ToList<TDSubPopulation>());
            }
            else
            {
                selectedSubPopulationCategoryCombinations =
                    new TDDomainCategoryCombinationList<TDSubPopulationCategory>(
                        database: Database,
                        items: null);
            }
            selectedSubPopulationCategoryCombinations.DisplayInPanel(
                panel: pnlSubPopulationCategoryCombination,
                languageVersion: LanguageVersion);

            InitializeListBoxAttributeCategory();
        }

        /// <summary>
        /// <para lang="cs">Zobrazí formulář pro úpravu hierarchie kategorií subpopulací</para>
        /// <para lang="en">It displays a form for adjusting the subpopulation category hierarchy</para>
        /// </summary>
        private void AdjustSubPopulationCategoryHierarchy()
        {
            if (LstSelectedSubPopulations.Domains.Count < 2)
            {
                // Pro definici hierachie kategorií, musí být vybrány minimálně dvě domény
                // mezi, kterými se definuje hierachie kategorií
                MessageBox.Show(
                    text: msgNotEnoughSubPopulations,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            List<List<TDSubPopulationCategory>> selectedSubPopulationCategories = [];
            List<List<CategoryPair<TDSubPopulation, TDSubPopulationCategory>>> selectedSubPopulationCategoryPairs = [];

            foreach (TDSubPopulation subpopulation in LstSelectedSubPopulations.Domains)
            {
                selectedSubPopulationCategories.Add(
                    item: Database.STargetData.CSubPopulationCategory.Items
                        .Where(a => a.SubPopulationId == subpopulation.Id)
                        .ToList<TDSubPopulationCategory>());
            }

            for (int i = 0; i < LstSelectedSubPopulations.Domains.Count - 1; i++)
            {
                TDSubPopulation superiorSP = LstSelectedSubPopulations.Domains[i];
                TDSubPopulation inferiorSP = LstSelectedSubPopulations.Domains[i + 1];
                List<TDSubPopulationCategory> superiorSPCs =
                    Database.STargetData.CSubPopulationCategory.Items
                        .Where(a => a.SubPopulationId == superiorSP.Id)
                        .ToList<TDSubPopulationCategory>();
                List<TDSubPopulationCategory> inferiorSPCs =
                        Database.STargetData.CSubPopulationCategory.Items
                        .Where(a => a.SubPopulationId == inferiorSP.Id)
                        .ToList<TDSubPopulationCategory>();

                List<CategoryPair<TDSubPopulation, TDSubPopulationCategory>> pairs = [];
                foreach (TDSPCHierarchy spc in
                    Database.STargetData.TSPCHierarchy.Items
                    .Where(a => superiorSPCs.Select(b => b.Id).Contains(value: a.VariableSuperiorId))
                    .Where(a => inferiorSPCs.Select(b => b.Id).Contains(value: a.VariableId)))
                {
                    pairs.Add(new CategoryPair<TDSubPopulation, TDSubPopulationCategory>(
                        superior: spc.VariableSuperior,
                        inferior: spc.Variable));
                }
                selectedSubPopulationCategoryPairs.Add(item: pairs);
            }

            FormHierarchy<TDSubPopulation, TDSubPopulationCategory> frm
                = new(
                    controlOwner: this,
                    domains: LstSelectedSubPopulations.Domains);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (SuperiorDomain<TDSubPopulation, TDSubPopulationCategory> ssp in frm.Domains)
                {
                    ssp.WriteToDatabase();
                }
            }
            else
            {
                // Dialog ukončen jinak než tlačítkem OK.
                // Dialogue terminated by other than the OK button.
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Do ListBoxu AttributeCategories vyplní seznam atributových kategorií
        /// pro zvolené seznamy plošných domén a subpopulací</para>
        /// <para lang="en">
        /// It fills the AttributeCategories ListBox with a list of attribute categories
        /// for the selected lists of area domains and subpopulations</para>
        /// </summary>
        private void InitializeListBoxAttributeCategory()
        {
            selectedAttributeCategories
                = new TDAttributeCategoryList(
                    database: Database,
                    areaDomainCategoryCombinations: selectedAreaDomainCategoryCombinations,
                    subPopulationCategoryCombinations: selectedSubPopulationCategoryCombinations);

            selectedAttributeCategories.DisplayInPanel(
                panel: pnlAttributeCategory,
                languageVersion: LanguageVersion);

            btnLimitTargetVariable.Visible =
                selectedAttributeCategories.Items.Count != 0;
        }


        /// <summary>
        /// <para lang="cs">
        /// Metoda vrací seznam s identifikátory categorizatition setup.
        /// V případě chyby vrátí null
        /// </para>
        /// <para lang="en">
        /// Method returns list with categorization setup identifiers.
        /// Null is returned in case of some error
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Metoda vrací seznam s identifikátory categorizatition setup.
        /// V případě chyby vrátí null
        /// </para>
        /// <para lang="en">
        /// Method returns list with categorization setup identifiers.
        /// Null is returned in case of some error
        /// </para>
        /// </returns>
        private List<Nullable<int>> GetCategorizationSetup(
            out bool testCatSetupInput,
            out string notice,
            out string commandText)
        {
            CategorizationSetupArgs args = new(owner: this);

            if (args.SelectedTargetVariable == null)
            {
                // Není vybraná žádná cílová proměnná.
                testCatSetupInput = false;
                notice = msgTargetVariableNULL
                            .Replace(
                                oldValue: "$1",
                                newValue: TDFunctions.FnSaveCategorizationSetup.Name);
                commandText = String.Empty;

                MessageBox.Show(
                    text: notice,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return null;
            }

            if (args.LDsityObjectsForTargetVariable == null)
            {
                // Nejsou žádné objekty lokálních hustot pro vybranou cílovou proměnnou
                testCatSetupInput = false;
                notice = msgLDsityObjectsForTargetVariableNULL;
                commandText = String.Empty;

                MessageBox.Show(
                    text: notice,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return null;
            }

            if (args.ClassificationRulesForADLDObjects.Where(a => !a.WithClassificationRules).Any() &&
                args.ClassificationRulesForSPLDObjects.Where(a => !a.WithClassificationRules).Any())
            {
                testCatSetupInput = false;
                notice = msgNoClassificationRulesForADSP;
                commandText = String.Empty;
                return null;
            }
            else if (args.ClassificationRulesForADLDObjects.Where(a => !a.WithClassificationRules).Any())
            {
                testCatSetupInput = false;
                notice = msgNoClassificationRulesForAD;
                commandText = String.Empty;
                return null;
            }
            else if (args.ClassificationRulesForSPLDObjects.Where(a => !a.WithClassificationRules).Any())
            {
                testCatSetupInput = false;
                notice = msgNoClassificationRulesForSP;
                commandText = String.Empty;
                return null;
            }

            // Kontrola vstupních argumentů uložené procedury fn_get_categorization_setup
            Database.Postgres.Notice = null;
            testCatSetupInput =
                TDFunctions.FnTestCategorizationSetupInput.Execute(
                    database: Database,
                    targetVariableId: args.TargetVariableId,
                    cmTargetVariableIds: args.CMTargetVariableIds,
                    areaDomainIds: args.AreaDomainIds,
                    areaDomainObjectIds: args.AreaDomainObjectIds,
                    subPopulationIds: args.SubPopulationIds,
                    subPopulationObjectIds: args.SubPopulationObjectIds);

            if (!testCatSetupInput)
            {
                // Pokud vstupní argumenty kontrolu nesplní, metoda vrátí null
                notice =
                    (Database.Postgres.Notice != null)
                        ? Database.Postgres.Notice.MessageText
                        : String.Empty;

                commandText =
                    TDFunctions.FnTestCategorizationSetupInput.CommandText;

                return null;
            }

            Database.Postgres.Notice = null;

            // Získání identifikátorů categorization setup
            List<Nullable<int>> csIds =
                TDFunctions.FnGetCategorizationSetup.Execute(
                    database: Database,
                    targetVariableId: args.TargetVariableId,
                    cmTargetVariableIds: args.CMTargetVariableIds,
                    areaDomainIds: args.AreaDomainIds,
                    areaDomainObjectIds: args.AreaDomainObjectIds,
                    subPopulationIds: args.SubPopulationIds,
                    subPopulationObjectIds: args.SubPopulationObjectIds);

            notice =
                (Database.Postgres.Notice != null)
                    ? Database.Postgres.Notice.MessageText
                    : String.Empty;

            commandText =
                TDFunctions.FnGetCategorizationSetup.CommandText;

            if (Setting.Verbose)
            {
                string returns;
                if ((csIds != null) && (csIds.Count != 0))
                {
                    returns = csIds
                        .Select(a => (a == null) ? Functions.StrNull : a.ToString())
                        .Aggregate((a, b) => $"{a}{(char)59}{b}");
                }
                else
                {
                    returns = String.Empty;
                }

                MessageBox.Show(
                    text: String.Concat(
                        $"{commandText}{Environment.NewLine}",
                        $"returns: {returns}"),
                    caption: TDFunctions.FnGetCategorizationSetup.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            return csIds;
        }

        /// <summary>
        /// <para lang="cs">
        /// Metoda uloží categorization setup do databáze
        /// a vrací seznam s identifikátory categorizatition setup.
        /// V případě chyby vrátí null
        /// </para>
        /// <para lang="en">
        /// Method saves categorizatin setup into database
        /// and returns list with categorization setup identifiers.
        /// Null is returned in case of some error
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Metoda uloží categorization setup do databáze
        /// a vrací seznam s identifikátory categorizatition setup.
        /// V případě chyby vrátí null
        /// </para>
        /// <para lang="en">
        /// Methods saves categorizatin setup into database
        /// and returns list with categorization setup identifiers.
        /// Null is returned in case of some error
        /// </para>
        /// </returns>
        private List<Nullable<int>> SaveCategorizationSetup()
        {
            CategorizationSetupArgs args =
                new(owner: this);

            if (args.SelectedTargetVariable == null)
            {
                // Není vybraná žádná cílová proměnná.
                MessageBox.Show(
                    text: msgTargetVariableNULL
                        .Replace(oldValue: "$1", newValue: TDFunctions.FnSaveCategorizationSetup.Name),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return null;
            }

            if (args.LDsityObjectsForTargetVariable == null)
            {
                // Nejsou žádné objekty lokálních hustot pro vybranou cílovou proměnnou
                MessageBox.Show(
                    text: msgLDsityObjectsForTargetVariableNULL,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return null;
            }

            TDFunctions.FnSaveCategorizationSetup.Execute(
                    database: Database,
                    targetVariableId: args.TargetVariableId,
                    cmTargetVariableIds: args.CMTargetVariableIds,
                    areaDomainIds: args.AreaDomainIds,
                    areaDomainObjectIds: args.AreaDomainObjectIds,
                    subPopulationIds: args.SubPopulationIds,
                    subPopulationObjectIds: args.SubPopulationObjectIds);

            List<Nullable<int>> csIds =
                TDFunctions.FnGetCategorizationSetup.Execute(
                    database: Database,
                    targetVariableId: args.TargetVariableId,
                    cmTargetVariableIds: args.CMTargetVariableIds,
                    areaDomainIds: args.AreaDomainIds,
                    areaDomainObjectIds: args.AreaDomainObjectIds,
                    subPopulationIds: args.SubPopulationIds,
                    subPopulationObjectIds: args.SubPopulationObjectIds);

            if (Setting.Verbose)
            {
                string returns = String.Empty;
                if ((csIds != null) && (csIds.Count != 0))
                {
                    returns = csIds
                        .Select(a => (a == null) ? Functions.StrNull : a.ToString())
                        .Aggregate((a, b) => $"{a}{(char)59}{b}");
                }

                MessageBox.Show(
                    text: String.Concat(
                        $"{TDFunctions.FnSaveCategorizationSetup.CommandText}{Environment.NewLine}",
                        $"returns: {returns}"),
                    caption: TDFunctions.FnSaveCategorizationSetup.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            return csIds;
        }

        /// <summary>
        /// <para lang="cs">
        /// Metoda uloží novou cílovou proměnnou omezenou vybranou atributovou kategorií
        /// </para>
        /// <para lang="en">
        /// Method saves new target variable constrained by selected attribute category
        /// </para>
        /// </summary>
        private void SaveConstraintTargetVariable()
        {
            CategorizationSetupArgs args =
                new(owner: this);

            if (args.SelectedTargetVariable == null)
            {
                // Není vybraná žádná cílová proměnná.
                MessageBox.Show(
                    text: msgTargetVariableNULL,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (args.LDsityObjectsForTargetVariable == null)
            {
                // Nejsou žádné objekty lokálních hustot pro vybranou cílovou proměnnou
                MessageBox.Show(
                    text: msgLDsityObjectsForTargetVariableNULL,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormTargetVariableLimit frmTargetVariableLimit =
                new(
                    controlOwner: this,
                    categorizationSetupArgs: args);

            if (frmTargetVariableLimit.ShowDialog() == DialogResult.OK)
            {
                Nullable<int> newTargetVariableId =
                    TDFunctions.FnSaveConstrainedTargetVariable.Execute(
                        database: Database,
                        targetVariableId: args.TargetVariableId,
                        targetVariableCm: args.CMTargetVariableIds,
                        areaDomainCategoryIds: args.AreaDomainCategoryIds,
                        adcObjectIds: args.AreaDomainObjectIds,
                        subPopulationCategoryIds: args.SubPopulationCategoryIds,
                        spcObjectIds: args.SubPopulationObjectIds);

                if (Setting.Verbose)
                {
                    string returns = (newTargetVariableId == null) ?
                        String.Empty : newTargetVariableId.ToString();

                    MessageBox.Show(
                        text: String.Concat(
                            $"{TDFunctions.FnSaveConstrainedTargetVariable.CommandText}{Environment.NewLine}",
                            $"returns: {returns}"),
                        caption: TDFunctions.FnSaveConstrainedTargetVariable.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                if (!Database.Postgres.ExceptionFlag)
                {
                    ((ControlTargetData)ControlOwner)
                        .ControlTDSelectionAttributeCategory_GoToTargetVariable();
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost tlačítek "Další" a "Uložit categorization setup"</para>
        /// <para lang="en">Enable or Disable button "Next" and "Save categorization setup"</para>
        /// </summary>
        private void EnableButtonNext()
        {
            List<Nullable<int>> csIds = GetCategorizationSetup(
                out bool testCatSetupInput,
                out string notice,
                out string commandText);

            // Vstupní argumenty nesplňují kontrolu uložené procedury
            // fn_test_categorization_setup_input:
            // zakázaná jsou obě tlačítka
            if (!testCatSetupInput)
            {
                btnSaveCategorizationSetup.Enabled = false;
                btnNext.Enabled = false;
                lblNotice.Text = notice;
                categorizationSetupIds = null;
                return;
            }

            // fn_get_categorization_setup vrátila NULL:
            // zakázaná jsou obě tlačítka
            if (csIds == null)
            {
                btnSaveCategorizationSetup.Enabled = false;
                btnNext.Enabled = false;
                lblNotice.Text = notice;
                categorizationSetupIds = null;

                if (!String.IsNullOrEmpty(value: commandText))
                {
                    MessageBox.Show(
                        text: msgFnGetCategorizationSetupNULL
                                .Replace(oldValue: "$1", newValue: TDFunctions.FnGetCategorizationSetup.Name)
                                .Replace(oldValue: "$2", newValue: commandText)
                                .Replace(oldValue: "$3", newValue: "NULL")
                                .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return;
            }

            // fn_get_categorization_setup vrátila prázdné pole
            // zakázaná jsou obě tlačítka
            if (csIds.Count == 0)
            {
                btnSaveCategorizationSetup.Enabled = false;
                btnNext.Enabled = false;
                lblNotice.Text = notice;
                categorizationSetupIds = null;

                if (!String.IsNullOrEmpty(value: commandText))
                {
                    MessageBox.Show(
                        text: msgFnGetCategorizationSetupNULL
                                .Replace(oldValue: "$1", newValue: TDFunctions.FnGetCategorizationSetup.Name)
                                .Replace(oldValue: "$2", newValue: commandText)
                                .Replace(oldValue: "$3", newValue: "prázdné pole")
                                .Replace(oldValue: "$0", newValue: Environment.NewLine),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return;
            }

            // fn_get_categorization_setup vrátila pole, které obsahuje NULL hodnoty:
            // -> pak některá categorization setup nejsou uložena
            // -> povolí se tlačítko pro uložení categorization setup
            // -> zakáže se tlačítko pro pokračování na další formulář
            if (csIds.Where(a => a == null).Any())
            {
                btnSaveCategorizationSetup.Enabled = true;
                btnNext.Enabled = false;
                lblNotice.Text = notice;
                categorizationSetupIds = null;

                return;
            }

            // fn_get_categorization_setup vrátila pole, které neobsahuje NULL hodnoty:
            // Všechna categoriyation setup jsou uložena a je možné pokračovat na další formulář
            // -> zakáže se tlačítko pro uložení categorization setup
            // -> povolí se tlačítko pro pokračování na další formulář
            btnSaveCategorizationSetup.Enabled = false;
            btnNext.Enabled = true;
            lblNotice.Text = notice;
            categorizationSetupIds = csIds
                .Where(a => a != null)
                .Select(a => (Nullable<int>)a)
                .ToList<Nullable<int>>();
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události
        /// "Změna výběru objektu lokální hustoty pro třídění v kategorii plošné domény nebo subpopulace"</para>
        /// <para lang="en">Handling the event
        /// "Change of the selection of the local density object for classification in area domain or subpopulation category"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (TDDomainListBox)</para>
        /// <para lang="en">Object that sends the event (TDDomainListBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void TDDomainListBox_ResultChanged(object sender, EventArgs e)
        {
            EnableButtonNext();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Editovat plošnou doménu"</para>
        /// <para lang="en">Handling the "Edit the area domain" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainEdit_Click(object sender, EventArgs e)
        {
            UpdateAreaDomain();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Vytvořit novou plošnou doménu"</para>
        /// <para lang="en">Handling the "Create a new area domain" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainAdd_Click(object sender, EventArgs e)
        {
            InsertAreaDomain();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Smazat plošnou doménu"</para>
        /// <para lang="en">Handling the "Delete the area domain" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainDelete_Click(object sender, EventArgs e)
        {
            DeleteAreaDomain();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Export klasifikačních pravidel plošné domény do JSON souboru"</para>
        /// <para lang="en">Handling the "Export area domain classification rules into JSON file" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainExport_Click(object sender, EventArgs e)
        {
            ExportAreaDomainJSON();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Import klasifikačních pravidel plošné domény z JSON souboru."</para>
        /// <para lang="en">Handling the "Import area domain classification rules from JSON file." button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainImport_Click(object sender, EventArgs e)
        {
            ImportAreaDomainJSON();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Editovat kategorii plošné domény"</para>
        /// <para lang="en">Handling the "Edit the area domain category" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainCategoryEdit_Click(object sender, EventArgs e)
        {
            UpdateAreaDomainCategory();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Přidat plošnou doménu mezi vybrané"</para>
        /// <para lang="en">Handling the "Add the area domain to the selected ones" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainSelect_Click(object sender, EventArgs e)
        {
            AddAreaDomainIntoSelected();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Odebrat plošnou doménu z vybraných"</para>
        /// <para lang="en">Handling the "Remove the area domain from the selected ones" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAreaDomainCancelSelect_Click(object sender, EventArgs e)
        {
            RemoveAreaDomainFromSelected(
                areaDomain:
                    (LstSelectedAreaDomains.SelectedItem == null) ? null :
                    (TDAreaDomain)LstSelectedAreaDomains.SelectedItem
                    ); ;
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Upravit hierachii kategorií plošných domén"</para>
        /// <para lang="en">Handling the "Adjust the hierarchy of area domain categories" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnAdcHierarchy_Click(object sender, EventArgs e)
        {
            AdjustAreaDomainCategoryHierarchy();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Editovat subpopulaci"</para>
        /// <para lang="en">Handling the "Edit the subpopulation" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationEdit_Click(object sender, EventArgs e)
        {
            UpdateSubPopulation();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Vytvořit novou subpopulaci"</para>
        /// <para lang="en">Handling the "Create a new subpopulation" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationAdd_Click(object sender, EventArgs e)
        {
            InsertSubPopulation();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Smazat subpopulaci"</para>
        /// <para lang="en">Handling the "Delete the subpopulation" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationDelete_Click(object sender, EventArgs e)
        {
            DeleteSubPopulation();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Export klasifikačních pravidel subpopulace do JSON souboru."</para>
        /// <para lang="en">Handling the "Export subpopulation classification rules into JSON file." button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationExport_Click(object sender, EventArgs e)
        {
            ExportSubPopulationJSON();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Import klasifikačních pravidel plošné domény z JSON souboru."</para>
        /// <para lang="en">Handling the "Import area domain classification rules from JSON file." button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationImport_Click(object sender, EventArgs e)
        {
            ImportSubPopulationJSON();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Editovat kategorii subpopulace"</para>
        /// <para lang="en">Handling the "Edit the subpopulation category" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationCategoryEdit_Click(object sender, EventArgs e)
        {
            UpdateSubPopulationCategory();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Přidat subpopulaci mezi vybrané"</para>
        /// <para lang="en">Handling the "Add the subpopulation to the selected ones" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationSelect_Click(object sender, EventArgs e)
        {
            AddSubPopulationIntoSelected();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Odebrat subpopulaci z vybraných"</para>
        /// <para lang="en">Handling the "Remove the subpopulation from the selected ones" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSubPopulationCancelSelect_Click(object sender, EventArgs e)
        {
            RemoveSubPopulationFromSelected(
                subPopulation:
                    (LstSelectedSubPopulations.SelectedItem == null) ? null :
                    (TDSubPopulation)LstSelectedSubPopulations.SelectedItem);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Upravit hierachii kategorií subpopulací"</para>
        /// <para lang="en">Handling the "Adjust the hierarchy of subpopulation categories" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSpcHierarchy_Click(object sender, EventArgs e)
        {
            AdjustSubPopulationCategoryHierarchy();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Omezení cílové veličiny na úrovni bez rozlišení"</para>
        /// <para lang="en">Handling the "Limit target variable at level of the altogether category"  button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnLimitTargetVariable_Click(object sender, EventArgs e)
        {
            SaveConstraintTargetVariable();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Uložit categorization setup"</para>
        /// <para lang="en">Handling the "Save categorization setup" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSaveCategorizationSetup_Click(object sender, EventArgs e)
        {
            SaveCategorizationSetup();
            EnableButtonNext();
        }

        #endregion Event Handlers

    }

}