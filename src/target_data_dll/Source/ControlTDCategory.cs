﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení kategorie plošné domény nebo subpopulace - třída pro Designer</para>
    /// <para lang="en">Control to display an area domain or subpopulation category - class for Designer</para>
    /// </summary>
    internal partial class ControlTDCategoryDesign
        : UserControl
    {
        protected ControlTDCategoryDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.ToolStripMenuItem CmsiDuplicate => cmsiDuplicate;
        protected System.Windows.Forms.ToolStripMenuItem CmsiRemove => cmsiRemove;
        protected System.Windows.Forms.ContextMenuStrip CmsMenu => cmsMenu;
        protected System.Windows.Forms.Label LblCaption => lblCaption;
        protected System.Windows.Forms.Panel PnlItems => pnlItems;
        protected System.Windows.Forms.SplitContainer SplMain => splMain;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro zobrazení kategorie plošné domény nebo subpopulace</para>
    /// <para lang="en">Control to display an area domain or subpopulation category</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlTDCategory<TDomain, TCategory>
            : ControlTDCategoryDesign, INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Nadřazený ovládací prvek pro zobrazení kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Superior control to display an area domain or subpopulation category</para>
        /// </summary>
        private ControlTDCategory<TDomain, TCategory> superiorControl;

        /// <summary>
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation category</para>
        /// </summary>
        private TCategory category;

        /// <summary>
        /// <para lang="cs">Je kategorie plošné domény nebo subpopulace nadřazená?</para>
        /// <para lang="en">Is the area domain or subpopulation category superior?</para>
        /// </summary>
        private bool isSuperior;

        /// <summary>
        /// <para lang="cs">Seznam podřízených ovládacích prvků</para>
        /// <para lang="en">List of inferior controls</para>
        /// </summary>
        private List<ControlTDCategory<TDomain, TCategory>> inferiorControls;

        /// <summary>
        /// <para lang="cs">Souřadnice bodu, kde bylo kliknuto</para>
        /// <para lang="en">Coordinates of the point where the click was made</para>
        /// </summary>
        private Point pClick;

        /// <summary>
        /// <para lang="cs">Bylo provedeno stisknutí levého tlačítka myši,
        /// ale tlačítko nebylo uvolněno, tj. probíhá přesouvání ovládacího prvku</para>
        /// <para lang="en">The left mouse button has been pressed,
        /// but the button has not been released, i.e. the control is being moved</para>
        /// </summary>
        private bool clicked;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Přemístění ovládacího prvku"</para>
        /// <para lang="en">"Control relocation" event</para>
        /// </summary>
        public event EventHandler Relocate;

        /// <summary>
        /// <para lang="cs">Událost "Duplikace ovládacího prvku"</para>
        /// <para lang="en">"Duplication of the control" event</para>
        /// </summary>
        public event EventHandler Duplicate;

        /// <summary>
        /// <para lang="cs">Událost "Odstranění ovládacího prvku"</para>
        /// <para lang="en">"Removing the control" event</para>
        /// </summary>
        public event EventHandler Remove;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="superiorControl">
        /// <para lang="cs">Nadřazený ovládací prvek pro zobrazení kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Superior control to display an area domain or subpopulation category</para>
        /// </param>
        /// <param name="category">
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation category</para>
        /// </param>
        /// <param name="isSuperior">
        /// <para lang="cs">Je kategorie plošné domény nebo subpopulace nadřazená?</para>
        /// <para lang="en">Is the area domain or subpopulation category superior?</para>
        /// </param>
        public ControlTDCategory(
            Control controlOwner,
            ControlTDCategory<TDomain, TCategory> superiorControl,
            TCategory category,
            bool isSuperior)
            : base()
        {
            Initialize(
                controlOwner: controlOwner,
                superiorControl: superiorControl,
                category: category,
                isSuperior: isSuperior);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormHierarchy<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(FormHierarchy<TDomain, TCategory>)}<TDomain,TCategory>."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormHierarchy<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(FormHierarchy<TDomain, TCategory>)}<TDomain,TCategory>."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((FormHierarchy<TDomain, TCategory>)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((FormHierarchy<TDomain, TCategory>)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((FormHierarchy<TDomain, TCategory>)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((FormHierarchy<TDomain, TCategory>)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Nadřazený ovládací prvek pro zobrazení kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Superior control to display an area domain or subpopulation category</para>
        /// </summary>
        public ControlTDCategory<TDomain, TCategory> SuperiorControl
        {
            get
            {
                return superiorControl;
            }
            set
            {
                superiorControl = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation category</para>
        /// </summary>
        public TCategory Category
        {
            get
            {
                if (category == null)
                {
                    throw new ArgumentException(
                       message: $"Argument {nameof(Category)} must not be null.",
                       paramName: nameof(Category));
                }

                return category;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException(
                       message: $"Argument {nameof(Category)} must not be null.",
                       paramName: nameof(Category));
                }

                category = value;
                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Je kategorie plošné domény nebo subpopulace nadřazená?</para>
        /// <para lang="en">Is the area domain or subpopulation category superior?</para>
        /// </summary>
        public bool IsSuperior
        {
            get
            {
                return isSuperior;
            }
            set
            {
                isSuperior = value;

                if (isSuperior)
                {
                    Height = 100 + 30 * PnlItems.Controls.Count;
                    Width = 200;
                    SplMain.Panel2Collapsed = false;
                    BackColor = Color.Wheat;
                }
                else
                {
                    Height = 25;
                    Width = 200;
                    SplMain.Panel2Collapsed = true;
                    BackColor = SystemColors.Info;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Je kategorie plošné domény nebo subpopulace již zařazená? (read-only)</para>
        /// <para lang="en">Is the area domain or subpopulation category already categorized? (read-only)</para>
        /// </summary>
        public bool IsCategorized
        {
            get
            {
                return (SuperiorControl != null);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam podřízených ovládacích prvků</para>
        /// <para lang="en">List of inferior controls</para>
        /// </summary>
        public List<ControlTDCategory<TDomain, TCategory>> InferiorControls
        {
            get
            {
                return inferiorControls;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(CmsiDuplicate),      "Duplikovat" },
                            { nameof(CmsiRemove),         "Odstranit" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDCategory<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(CmsiDuplicate),      "Duplikovat" },
                            { nameof(CmsiRemove),         "Odstranit" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDCategory<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictNationalSubPopulation)
                                ? dictNationalSubPopulation
                                : [],

                    _ => [],
                },

                LanguageVersion.International =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(CmsiDuplicate),      "Duplicate" },
                            { nameof(CmsiRemove),         "Remove" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDCategory<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(CmsiDuplicate),      "Duplicate" },
                            { nameof(CmsiRemove),         "Remove" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDCategory<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="superiorControl">
        /// <para lang="cs">Nadřazený ovládací prvek pro zobrazení kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Superior control to display an area domain or subpopulation category</para>
        /// </param>
        /// <param name="category">
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation category</para>
        /// </param>
        /// <param name="isSuperior">
        /// <para lang="cs">Je kategorie plošné domény nebo subpopulace nadřazená?</para>
        /// <para lang="en">Is the area domain or subpopulation category superior?</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ControlTDCategory<TDomain, TCategory> superiorControl,
            TCategory category,
            bool isSuperior)
        {
            ControlOwner = controlOwner;
            SuperiorControl = superiorControl;
            Category = category;
            IsSuperior = isSuperior;

            inferiorControls = [];

            pClick = new Point(0, 0);
            clicked = false;

            LblCaption.MouseDown += new MouseEventHandler(
                (sender, e) =>
                {
                    if (!IsCategorized)
                    {
                        if (e.Button == MouseButtons.Left)
                        {
                            clicked = true;
                            pClick = new Point() { X = e.X, Y = e.Y };
                        }
                    }

                    if (e.Button == MouseButtons.Right)
                    {
                        if (!isSuperior)
                        {
                            CmsMenu.Show(
                                control: this,
                                position: new Point() { X = e.X, Y = e.Y });
                        }
                    }
                });

            LblCaption.MouseMove += new MouseEventHandler(
                (sender, e) =>
                {
                    if (!IsCategorized)
                    {
                        if (clicked)
                        {
                            Point p = new()
                            {
                                X = e.X + Left,
                                Y = e.Y + Top
                            };

                            Left = p.X - pClick.X;
                            Top = p.Y - pClick.Y;
                        }
                    }
                });

            LblCaption.MouseUp += new MouseEventHandler(
                (sender, e) =>
                {
                    if (!IsCategorized)
                    {
                        clicked = false;
                        Relocate?.Invoke(sender: this, e: new EventArgs());
                    }
                });

            CmsiDuplicate.Click += new EventHandler(
                (sender, e) => { Duplicate?.Invoke(sender: this, e: new EventArgs()); });

            CmsiRemove.Click += new EventHandler(
                (sender, e) => { Remove?.Invoke(sender: this, e: new EventArgs()); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing the control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            CmsiDuplicate.Text =
                labels.TryGetValue(key: nameof(CmsiDuplicate),
                    out string cmsiDuplicateText)
                        ? cmsiDuplicateText
                        : String.Empty;

            CmsiRemove.Text =
                labels.TryGetValue(key: nameof(CmsiRemove),
                    out string cmsiRemoveText)
                        ? cmsiRemoveText
                        : String.Empty;

            LblCaption.Text = LanguageVersion switch
            {
                LanguageVersion.National =>
                    (Category == null)
                        ? String.Empty
                        : Category.LabelCs,

                LanguageVersion.International =>
                    (Category == null)
                        ? String.Empty
                        : Category.LabelEn,

                _ =>
                    (Category == null)
                        ? String.Empty
                        : LblCaption.Name
            };

            foreach (ControlTDCategory<TDomain, TCategory> item in PnlItems.Controls)
            {
                item.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Testuje, zda aktuální podřízený ovládací prvek je uvnitř nadřazeného ovládacího prvku</para>
        /// <para lang="en">It tests whether the current inferior control is inside the superior control</para>
        /// </summary>
        /// <param name="superior">
        /// <para lang="cs">Nadřazený ovládací prvek</para>
        /// <para lang="en">Superior control</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací true - pokud aktuální podřízený ovládací prvek je uvnitř nadřazeného. Jinak false.</para>
        /// <para lang="en">It returns true - if the current inferior control is inside the superior control. Otherwise false.</para>
        /// </returns>
        public bool IsInside(ControlTDCategory<TDomain, TCategory> superior)
        {
            return
                (!isSuperior) &&
                (Left > (superior.Left)) &&
                (Left < (superior.Left + superior.Width)) &&
                (Top > (superior.Top)) &&
                (Top < (superior.Top + superior.Height));
        }

        /// <summary>
        /// <para lang="cs">Přidá podřízený ovládací prvek do seznamu podřízených</para>
        /// <para lang="en">It adds the inferior control in the list of inferiors</para>
        /// </summary>
        /// <param name="inferior">
        /// <para lang="cs">Podřízený ovládací prvek</para>
        /// <para lang="en">Inferior control</para>
        /// </param>
        public void AddInferior(ControlTDCategory<TDomain, TCategory> inferior)
        {
            if (isSuperior)
            {
                if (!inferior.IsSuperior)
                {
                    if (!inferior.IsCategorized)
                    {
                        inferior.Left = 5;
                        inferior.Top = (PnlItems.Controls.Count * 30) + 5;
                        inferior.Width = PnlItems.Width - 10;
                        inferior.Height = 25;

                        inferior.ControlOwner = ControlOwner;
                        inferior.SuperiorControl = this;

                        PnlItems.Controls.Add(value: inferior);
                        InferiorControls.Add(item: inferior);

                        Height = 100 + 30 * PnlItems.Controls.Count;
                    }
                    else
                    {
                        // Pouze nezařazené podřízené prvky lze přidávat do nadřazeného ovládacího prvku
                        // Only uncategorized inferior controls can be added to the superior control
                    }
                }
                else
                {
                    // Pouze podřízené ovládací prvky lze přidávat do nadřazeného ovládacího prvku
                    // Only inferior controls can be added to the superior control
                }
            }
            else
            {
                // Pouze do nadřazeného ovládacího prvku lze přidávat podřízené ovládací prvky
                // Inferior controls can be added only to the superior control
            }
        }

        /// <summary>
        /// <para lang="cs">Odstraní podřízený ovládací prvek ze seznamu podřízených</para>
        /// <para lang="en">It removes the inferior control from the list of inferiors</para>
        /// </summary>
        /// <param name="inferior">
        /// <para lang="cs">Podřízený ovládací prvek</para>
        /// <para lang="en">Inferior control</para>
        /// </param>
        public void RemoveInferior(ControlTDCategory<TDomain, TCategory> inferior)
        {
            if (isSuperior)
            {
                if (!inferior.IsSuperior)
                {
                    if (inferior.IsCategorized)
                    {
                        PnlItems.Controls.Remove(value: inferior);
                        inferiorControls.Remove(item: inferior);
                        Height = 100 + 30 * PnlItems.Controls.Count;

                        int i = -1;
                        foreach (ControlTDCategory<TDomain, TCategory> item in PnlItems.Controls)
                        {
                            item.Top = (++i * 30) + 5;
                        }
                    }
                    else
                    {
                        // Pouze zařazené podřízené prvky lze odebírat z nadřazeného ovládacího prvku
                        // Only uncategorized inferior controls can be removed from the superior control
                    }
                }
                else
                {
                    // Pouze podřízené ovládací prvky lze odebírat z nadřazeného ovládacího prvku
                    // Only inferior controls can be removed from the superior control
                }
            }
            else
            {
                // Pouze z nadřazeného ovládacího prvku lze odebírat podřízené ovládací prvky
                // Inferior controls can be removed only from the superior control

            }
        }

        #endregion Methods

    }

}