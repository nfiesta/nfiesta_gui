﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Pár klasifikačních pravidel pro kladný a záporný příspěvek</para>
        /// <para lang="en">Classification rule pair for positive and negative contribution</para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        [SupportedOSPlatform("windows")]
        internal class ClassificationRulePair<TDomain, TCategory>
            : INfiEstaObject, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            private object owner;

            /// <summary>
            /// <para lang="cs">Uzel v TreeView</para>
            /// <para lang="en">Node in TreeView</para>
            /// </summary>
            private TreeNode node;

            /// <summary>
            /// <para lang="cs">TreeView nemá hlubší úrovně než 0</para>
            /// <para lang="en">TreeView has only TreeNode level 0</para>
            /// </summary>
            private bool flat;

            /// <summary>
            /// <para lang="cs">Typ klasifikace</para>
            /// <para lang="en">Classification type</para>
            /// </summary>
            private TDClassificationTypeEnum classificationType;

            /// <summary>
            /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Area domain category or subpopulation category</para>
            /// </summary>
            private TCategory category;

            /// <summary>
            /// <para lang="cs">Objekt s příspěvkem do lokální hustoty</para>
            /// <para lang="en">Object with a contribution to a local density</para>
            /// </summary>
            private TDLDsityObject ldsityObject;

            /// <summary>
            /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
            /// <para lang="en">Local density object used for classification</para>
            /// </summary>
            private TDLDsityObject ldsityObjectForADSP;

            /// <summary>
            /// <para lang="cs">Klasifikační pravidlo pro kladný příspěvek</para>
            /// <para lang="en">Classification rule for positive contribution</para>
            /// </summary>
            private ClassificationRule<TDomain, TCategory> positive;

            /// <summary>
            /// <para lang="cs">Klasifikační pravidlo pro záporný příspěvek</para>
            /// <para lang="en">Classification rule for negative contribution</para>
            /// </summary>
            private ClassificationRule<TDomain, TCategory> negative;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor páru klasifikačních pravidel</para>
            /// <para lang="en">Classification pair contructor</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            /// <param name="node">
            /// <para lang="cs">Uzel v TreeView</para>
            /// <para lang="en">Node in TreeView</para>
            /// </param>
            /// <param name="flat">
            /// <para lang="cs">TreeView nemá hlubší úrovně než 0</para>
            /// <para lang="en">TreeView has only TreeNode level 0</para>
            /// </param>
            /// <param name="classificationType">
            /// <para lang="cs">Typ klasifikace</para>
            /// <para lang="en">Classification type</para>
            /// </param>
            /// <param name="category">
            /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Area domain category or subpopulation category</para>
            /// </param>
            /// <param name="ldsityObject">
            /// <para lang="cs">Objekt s příspěvkem do lokální hustoty</para>
            /// <para lang="en">Object with a contribution to a local density</para>
            /// </param>
            /// <param name="ldsityObjectForADSP">
            /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
            /// <para lang="en">Local density object used for classification</para>
            /// </param>
            public ClassificationRulePair(
                object owner,
                TreeNode node,
                bool flat,
                TDClassificationTypeEnum classificationType,
                TCategory category,
                TDLDsityObject ldsityObject,
                TDLDsityObject ldsityObjectForADSP)
            {
                Initialize(
                    owner: owner,
                    node: node,
                    flat: flat,
                    classificationType: classificationType,
                    category: category,
                    ldsityObject: ldsityObject,
                    ldsityObjectForADSP: ldsityObjectForADSP);
            }

            #endregion Constructor


            #region Common Properties

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            public object Owner
            {
                get
                {
                    if (owner == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                    }

                    if (owner is not INfiEstaControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(INfiEstaControl)}.",
                            paramName: nameof(Owner));
                    }

                    if (owner is not ITargetDataControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(ITargetDataControl)}.",
                            paramName: nameof(Owner));
                    }

                    return owner;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                    }

                    if (value is not INfiEstaControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(INfiEstaControl)}.",
                            paramName: nameof(Owner));
                    }

                    if (value is not ITargetDataControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(ITargetDataControl)}.",
                            paramName: nameof(Owner));
                    }

                    owner = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Databázové tabulky (read-only)</para>
            /// <para lang="en">Database tables(read-only)</para>
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return ((INfiEstaControl)Owner).Database;
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyková verze (read-only)</para>
            /// <para lang="en">Language version (read-only)</para>
            /// </summary>
            public LanguageVersion LanguageVersion
            {
                get
                {
                    return ((INfiEstaControl)Owner).LanguageVersion;
                }
            }

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            public LanguageFile LanguageFile
            {
                get
                {
                    return ((ITargetDataControl)Owner).LanguageFile;
                }
            }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
            /// <para lang="en">Module for target data setting (read-only)</para>
            /// </summary>
            public Setting Setting
            {
                get
                {
                    return ((ITargetDataControl)Owner).Setting;
                }
            }

            #endregion Common Properties


            #region Properties

            /// <summary>
            /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
            /// <para lang="en">Domain and its categories type (read-only)</para>
            /// </summary>
            public static TDArealOrPopulationEnum ArealOrPopulation
            {
                get
                {
                    return typeof(TDomain).FullName switch
                    {
                        "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    TDArealOrPopulationEnum.AreaDomain,

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                        paramName: nameof(TCategory)),

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    TDArealOrPopulationEnum.Population,

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        _ =>
                            throw new ArgumentException(
                                message: String.Concat(
                                    $"Argument {nameof(TDomain)} must be type of ",
                                    $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                                paramName: nameof(TDomain))
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Uzel v TreeView</para>
            /// <para lang="en">Node in TreeView</para>
            /// </summary>
            public TreeNode Node
            {
                get
                {
                    if (node == null)
                    {
                        throw new ArgumentException(
                           message: $"Argument {nameof(Node)} must not be null.",
                           paramName: nameof(Node));
                    }

                    if ((node.Level < 0) || (node.Level > 2))
                    {
                        throw new ArgumentException(
                           message: $"Tree node must have level 0, 1 or 2.",
                           paramName: nameof(Node));
                    }

                    return node;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentException(
                           message: $"Argument {nameof(Node)} must not be null.",
                           paramName: nameof(Node));
                    }

                    if ((value.Level < 0) || (value.Level > 2))
                    {
                        throw new ArgumentException(
                           message: $"Tree node must have level 0, 1 or 2.",
                           paramName: nameof(Node));
                    }

                    node = value;
                }
            }

            /// <summary>
            /// <para lang="cs">TreeView nemá hlubší úrovně než 0</para>
            /// <para lang="en">TreeView has only TreeNode level 0</para>
            /// </summary>
            public bool Flat
            {
                get
                {
                    return flat;
                }
                set
                {
                    flat = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Typ klasifikace</para>
            /// <para lang="en">Classification type</para>
            /// </summary>
            private TDClassificationTypeEnum ClassificationType
            {
                get
                {
                    return classificationType;
                }
                set
                {
                    classificationType = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Area domain category or subpopulation category</para>
            /// </summary>
            public TCategory Category
            {
                get
                {
                    if (category == null)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Category)} must not be null.",
                            paramName: nameof(Category));
                    }
                    return category;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Category)} must not be null.",
                            paramName: nameof(Category));
                    }
                    category = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Objekt s příspěvkem do lokální hustoty</para>
            /// <para lang="en">Object with a contribution to a local density</para>
            /// </summary>
            public TDLDsityObject LDsityObject
            {
                get
                {
                    if (Flat)
                    {
                        return ldsityObject ??
                            throw new ArgumentException(
                                message: $"Argument {nameof(LDsityObject)} must not be null.",
                                paramName: nameof(LDsityObject));
                    }

                    // Objekt lokální hustoty je null pro uzly úrovně 0
                    // pro uzly úrovně 1 a 2 musí být vyplněn
                    return Node.Level switch
                    {
                        0 =>
                            null,

                        1 or 2 =>
                            ldsityObject ??
                                throw new ArgumentException(
                                    message: $"Argument {nameof(LDsityObject)} must not be null for TreeNode level {Node.Level}.",
                                    paramName: nameof(LDsityObject)),

                        _ =>
                            throw new ArgumentException(
                                message: "Tree node must have level 0, 1 or 2.",
                                paramName: nameof(Node)),
                    };
                }
                set
                {
                    if (Flat)
                    {
                        ldsityObject = value ??
                             throw new ArgumentException(
                               message: $"Argument {nameof(LDsityObject)} must not be null.",
                               paramName: nameof(LDsityObject));
                        return;
                    }

                    // Objekt lokální hustoty je null pro uzly úrovně 0
                    // pro uzly úrovně 1 a 2 musí být vyplněn
                    switch (Node.Level)
                    {
                        case 0:
                            ldsityObject = null;
                            return;

                        case 1:
                        case 2:
                            ldsityObject = value ??
                                throw new ArgumentException(
                                   message: $"Argument {nameof(LDsityObject)} must not be null for TreeNode level {Node.Level}.",
                                   paramName: nameof(LDsityObject));
                            return;

                        default:
                            throw new Exception(message: "Tree node must have level 0, 1 or 2.");
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
            /// <para lang="en">Local density object used for sorting</para>
            /// </summary>
            public TDLDsityObject LDsityObjectForADSP
            {
                get
                {
                    if (Flat)
                    {
                        return ldsityObjectForADSP ??
                                throw new ArgumentException(
                                    message: $"Argument  {nameof(LDsityObjectForADSP)} must not be null.",
                                    paramName: nameof(LDsityObjectForADSP));
                    }

                    // Objekt lokální hustoty ke třídění je null pro uzly úrovně 0 a 1
                    // pro uzly úrovně 2 musí být vyplněn
                    return Node.Level switch
                    {
                        0 or 1 =>
                            null,

                        2 =>
                            ldsityObjectForADSP ??
                                throw new ArgumentException(
                                    message: $"Argument {nameof(LDsityObjectForADSP)} must not be null for TreeNode level {Node.Level}.",
                                    paramName: nameof(LDsityObjectForADSP)),

                        _ =>
                            throw new ArgumentException(
                                message: "Tree node must have level 0, 1 or 2.",
                                paramName: nameof(Node)),
                    };
                }
                set
                {
                    if (Flat)
                    {
                        ldsityObjectForADSP = value ??
                            throw new ArgumentException(
                               message: $"Argument {nameof(LDsityObjectForADSP)} must not be null.",
                               paramName: nameof(LDsityObjectForADSP));
                        return;
                    }

                    switch (Node.Level)
                    {
                        case 0:
                        case 1:
                            ldsityObjectForADSP = null;
                            return;

                        case 2:
                            ldsityObjectForADSP = value ??
                                throw new ArgumentException(
                                   message: $"Argument {nameof(LDsityObjectForADSP)} must not be null for TreeNode level {Node.Level}.",
                                   paramName: nameof(LDsityObjectForADSP));
                            return;

                        default:
                            throw new Exception(message: "Tree node must have level 0, 1 or 2.");
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Klasifikační pravidlo pro kladný příspěvek</para>
            /// <para lang="en">Classification rule for positive contribution</para>
            /// </summary>
            public ClassificationRule<TDomain, TCategory> Positive
            {
                get
                {
                    return positive ??
                        throw new ArgumentException(
                            message: $"Argument {nameof(Positive)} must not be null.",
                            paramName: nameof(Positive));
                }
                set
                {
                    positive = value ??
                        throw new ArgumentException(
                            message: $"Argument {nameof(Positive)} must not be null.",
                            paramName: nameof(Positive));
                }
            }

            /// <summary>
            /// <para lang="cs">Klasifikační pravidlo pro záporný příspěvek</para>
            /// <para lang="en">Classification rule for negative contribution</para>
            /// </summary>
            public ClassificationRule<TDomain, TCategory> Negative
            {
                get
                {
                    return ClassificationType switch
                    {
                        // Standardní nemá záporné klasifikační pravidlo
                        TDClassificationTypeEnum.Standard =>
                            null,

                        // Změnová musí mít záporné klasifikační pravidlo
                        TDClassificationTypeEnum.ChangeOrDynamic =>
                            negative ??
                                throw new ArgumentException(
                                    message: $"Argument {nameof(Negative)} must not be null.",
                                    paramName: nameof(Negative)),

                        _ =>
                            throw new Exception(
                                message: $"Argument {nameof(ClassificationType)} must be Standard or Change or Dynamic.")
                    };
                }
                set
                {
                    switch (ClassificationType)
                    {
                        // Standardní nemá záporné klasifikační pravidlo
                        case TDClassificationTypeEnum.Standard:
                            negative = null;
                            return;

                        // Změnová musí mít záporné klasifikační pravidlo
                        case TDClassificationTypeEnum.ChangeOrDynamic:
                            negative = value ??
                                throw new ArgumentException(
                                    message: $"Argument {nameof(Negative)} must not be null.",
                                    paramName: nameof(Negative));
                            return;

                        default:
                            throw new Exception(
                                message: $"Argument {nameof(ClassificationType)} must be Standard or Change or Dynamic.");
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Validita páru klasifikačních pravidel (read-only)</para>
            /// <para lang="en">Classification rule pair validity (read-only)</para>
            /// </summary>
            public bool IsValid
            {
                get
                {
                    return ClassificationType switch
                    {
                        // V případě že klasifikační typ je 100 (Standard),
                        // pak je záporné klasifikační pravidlo null
                        // a pár je validní, když je validní kladné klasifikační pravidlo
                        TDClassificationTypeEnum.Standard =>
                            Positive.IsValid,

                        // V případě že klasifikační typ je 200 (Change or Dynamic)
                        // jsou vyplněna obě pravidla
                        // a pár je validní pokud jsou validní obě.
                        TDClassificationTypeEnum.ChangeOrDynamic =>
                            Positive.IsValid && Negative.IsValid,

                        _ => throw new ArgumentException(
                                message: $"Argument {nameof(ClassificationType)} must be Standard or Change or Dynamic.",
                                paramName: nameof(ClassificationType)),
                    };
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Zvolený jazyk</para>
            /// <para lang="en">Selected language</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">File with control labels for national and international version</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts in selected language</para>
            /// </returns>
            public static Dictionary<string, string> Dictionary(
                LanguageVersion languageVersion,
                LanguageFile languageFile)
            {
                return languageVersion switch
                {
                    LanguageVersion.National =>
                    ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                            ? []
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(ClassificationRulePair<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                                out Dictionary<string, string> dictNationalAreaDomain)
                                    ? dictNationalAreaDomain
                                    : [],

                        TDArealOrPopulationEnum.Population => (languageFile == null)
                            ? []
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(ClassificationRulePair<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                out Dictionary<string, string> dictNationalSubPopulation)
                                    ? dictNationalSubPopulation
                                    : [],

                        _ => [],
                    },

                    LanguageVersion.International =>
                    ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                            ? []
                            : languageFile.InternationalVersion.Data.TryGetValue(
                                key: $"{nameof(ClassificationRulePair<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                                out Dictionary<string, string> dictInternationalAreaDomain)
                                    ? dictInternationalAreaDomain
                                    : [],

                        TDArealOrPopulationEnum.Population =>
                            (languageFile == null)
                                ? []
                                : languageFile.InternationalVersion.Data.TryGetValue(
                                    key: $"{nameof(ClassificationRulePair<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                    out Dictionary<string, string> dictInternationalSubPopulation)
                                        ? dictInternationalSubPopulation
                                        : [],

                        _ => [],
                    },

                    _ => [],
                };
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Inicializace objektu páru klasifikačních pravidel</para>
            /// <para lang="en">Initializing the classification rule pair object</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            /// <param name="node">
            /// <para lang="cs">Uzel v TreeView</para>
            /// <para lang="en">Node in TreeView</para>
            /// </param>
            /// <param name="flat">
            /// <para lang="cs">TreeView nemá hlubší úrovně než 0</para>
            /// <para lang="en">TreeView has only TreeNode level 0</para>
            /// </param>
            /// <param name="classificationType">
            /// <para lang="cs">Typ klasifikace</para>
            /// <para lang="en">Classification type</para>
            /// </param>
            /// <param name="category">
            /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Area domain category or subpopulation category</para>
            /// </param>
            /// <param name="ldsityObject">
            /// <para lang="cs">Objekt s příspěvkem do lokální hustoty</para>
            /// <para lang="en">Object with a contribution to a local density</para>
            /// </param>
            /// <param name="ldsityObjectForADSP">
            /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
            /// <para lang="en">Local density object used for classification</para>
            /// </param>
            private void Initialize(
                object owner,
                TreeNode node,
                bool flat,
                TDClassificationTypeEnum classificationType,
                TCategory category,
                TDLDsityObject ldsityObject,
                TDLDsityObject ldsityObjectForADSP)
            {
                Owner = owner;
                Node = node;
                Flat = flat;
                ClassificationType = classificationType;
                Category = category;
                LDsityObject = ldsityObject;
                LDsityObjectForADSP = ldsityObjectForADSP;

                switch (ClassificationType)
                {
                    // Standardní nemá záporné klasifikační pravidlo
                    case TDClassificationTypeEnum.Standard:
                        Positive = new ClassificationRule<TDomain, TCategory>(owner: this);
                        Negative = null;
                        break;

                    // Změnová musí mít záporné klasifikační pravidlo
                    case TDClassificationTypeEnum.ChangeOrDynamic:
                        Positive = new ClassificationRule<TDomain, TCategory>(owner: this);
                        Negative = new ClassificationRule<TDomain, TCategory>(owner: this);
                        break;

                    default:
                        throw new Exception(
                            message: $"Argument {nameof(ClassificationType)} must be Standard or Change or Dynamic.");

                }
            }

            /// <summary>
            /// <para lang="cs">Kontroluje syntaxi vybraného páru klasifikačních pravidel</para>
            /// <para lang="en">It checks the syntax of the selected classification rule pair</para>
            /// </summary>
            /// <param name="verbose">
            /// <para lang="cs">Zobrazení hlášení o validitě klasifikačních pravidel</para>
            /// <para lang="en">To display a classification rules validity report</para>
            /// </param>
            public void ValidateSyntax(bool verbose)
            {
                Positive.ValidateSyntax(
                    verbose: verbose);

                Negative?.ValidateSyntax(
                    verbose: verbose);
            }

            /// <summary>
            /// <para lang="cs">Textový popis objektu páru klasifikačních pravidel</para>
            /// <para lang="en">Text representation of classification rule pair object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Textový popis objektu páru klasifikačních pravidel</para>
            /// <para lang="en">Text representation of classification rule pair object</para>
            /// </returns>
            public override string ToString()
            {
                string strNodeLevel =
                    (Node == null) ? Functions.StrNull :
                     Node.Level.ToString();

                string strCategory =
                    (Category == null) ? Functions.StrNull :
                     Category.ToString();

                string strLDsityObject =
                    (LDsityObject == null) ? Functions.StrNull :
                     LDsityObject.ToString();

                string strLDsityObjectForADSP =
                    (LDsityObjectForADSP == null) ? Functions.StrNull :
                     LDsityObjectForADSP.ToString();

                string strPositiveClassificationRule =
                    (Positive == null) ? Functions.StrNull :
                    (String.IsNullOrEmpty(value: Positive.Text)) ? Functions.StrNull :
                    Positive.Text;

                string strNegativeClassificationRule =
                    (Negative == null) ? Functions.StrNull :
                    (String.IsNullOrEmpty(value: Negative.Text)) ? Functions.StrNull :
                    Negative.Text;

                return String.Concat(
                    $"Level: {strNodeLevel}; ",
                    $"Category: {strCategory}; ",
                    $"LDsityObject: {strLDsityObject}; ",
                    $"LDsityObjectForADSP: {strLDsityObjectForADSP}; ",
                    $"Positive Classification Rule: {strPositiveClassificationRule}; ",
                    $"Negative Classification Rule: {strNegativeClassificationRule};");
            }

            /// <summary>
            /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
            /// <para lang="en">Determines whether specified object is equal to the current object</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Zadaný objekt</para>
            /// <para lang="en">Speicified object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not ClassificationRulePair<TCategory> Type, return False
                if (obj is not ClassificationRulePair<TDomain, TCategory>)
                {
                    return false;
                }

                return
                    Category.Equals(
                        obj: ((ClassificationRulePair<TDomain, TCategory>)obj).Category) &&

                    LDsityObject.Equals(
                        obj: ((ClassificationRulePair<TDomain, TCategory>)obj).LDsityObject) &&

                    LDsityObjectForADSP.Equals(
                        obj: ((ClassificationRulePair<TDomain, TCategory>)obj).LDsityObjectForADSP) &&

                    Positive.Equals(
                        obj: ((ClassificationRulePair<TDomain, TCategory>)obj).Positive) &&

                    Negative.Equals(
                        obj: ((ClassificationRulePair<TDomain, TCategory>)obj).Negative);
            }

            /// <summary>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </returns>
            public override int GetHashCode()
            {
                return
                    ((Category == null) ? 0 : Category.GetHashCode()) ^
                    ((LDsityObject == null) ? 0 : LDsityObject.GetHashCode()) ^
                    ((LDsityObjectForADSP == null) ? 0 : LDsityObjectForADSP.GetHashCode()) ^
                    ((Positive == null) ? 0 : Positive.GetHashCode()) ^
                    ((Negative == null) ? 0 : Negative.GetHashCode());
            }

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">Třída pro porovnání dvou párů klasifikačních pravidel.
        /// Páry klasifikačních pravidel jsou stejné pro stejnou kategorii plošné domény nebo subpopulace
        /// a pro stejný objekt lokální hustoty pro třídění
        /// </para>
        /// <para lang="en">Class for comparing two classifiction rule pairs.
        /// Classification rule pairs are equal for the same area domain or subpopulation category and
        /// for the same local density object for classification</para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        internal class ClassificationRulePairComparer<TDomain, TCategory>
            : IEqualityComparer<ClassificationRulePair<TDomain, TCategory>>
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Methods

            /// <summary>
            /// <para lang="cs">Určuje zda zadané dva objekty jsou stejné</para>
            /// <para lang="en">Determines whether two specified objects are equal</para>
            /// </summary>
            /// <param name="classificationRulePairA">
            /// <para lang="cs">První objekt páru klasifikačních pravidel</para>
            /// <para lang="en">First classification rule pair object</para>
            /// </param>
            /// <param name="classificationRulePairB">
            /// <para lang="cs">Druhý objekt páru klasifikačních pravidel</para>
            /// <para lang="en">Second classification rule pair object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public bool Equals(
                ClassificationRulePair<TDomain, TCategory> classificationRulePairA,
                ClassificationRulePair<TDomain, TCategory> classificationRulePairB)
            {

                if (Object.ReferenceEquals(classificationRulePairA, classificationRulePairB))
                    return true;

                if (classificationRulePairA == null)
                    return false;

                if (classificationRulePairB == null)
                    return false;

                if (OperatingSystem.IsWindows())
                {
                    return
                        (classificationRulePairA.Category.Id == classificationRulePairB.Category.Id) &&
                        (classificationRulePairA.LDsityObjectForADSP.Id == classificationRulePairB.LDsityObjectForADSP.Id);
                }
                return false;
            }

            /// <summary>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </returns>
            public int GetHashCode(ClassificationRulePair<TDomain, TCategory> classificationRulePair)
            {
                if (classificationRulePair == null)
                    return 0;

                if (OperatingSystem.IsWindows())
                {
                    if (classificationRulePair.Category == null)
                        return 0;

                    if (classificationRulePair.LDsityObjectForADSP == null)
                        return 0;

                    return
                        classificationRulePair.Category.Id.GetHashCode() ^
                        classificationRulePair.LDsityObjectForADSP.Id.GetHashCode();
                }
                return 0;
            }

            #endregion Methods

        }

    }
}