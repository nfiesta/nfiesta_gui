﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class FormLDsityObjectGroupNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            splGroupOfObjects = new System.Windows.Forms.SplitContainer();
            grpGroupOfObjects = new System.Windows.Forms.GroupBox();
            tlpGroupOfObjects = new System.Windows.Forms.TableLayoutPanel();
            lblArealOrPopulation = new System.Windows.Forms.Label();
            pnlArealOrPopulation = new System.Windows.Forms.Panel();
            lblDescriptionEn = new System.Windows.Forms.Label();
            lblLabelEn = new System.Windows.Forms.Label();
            lblDescriptionCs = new System.Windows.Forms.Label();
            lblLabelCs = new System.Windows.Forms.Label();
            txtDescriptionEn = new System.Windows.Forms.TextBox();
            txtLabelEn = new System.Windows.Forms.TextBox();
            txtDescriptionCs = new System.Windows.Forms.TextBox();
            txtLabelCs = new System.Windows.Forms.TextBox();
            grpObjectsList = new System.Windows.Forms.GroupBox();
            cklObjectsList = new System.Windows.Forms.CheckedListBox();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlInsert = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splGroupOfObjects).BeginInit();
            splGroupOfObjects.Panel1.SuspendLayout();
            splGroupOfObjects.Panel2.SuspendLayout();
            splGroupOfObjects.SuspendLayout();
            grpGroupOfObjects.SuspendLayout();
            tlpGroupOfObjects.SuspendLayout();
            grpObjectsList.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlInsert.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(splGroupOfObjects, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 3;
            // 
            // splGroupOfObjects
            // 
            splGroupOfObjects.Dock = System.Windows.Forms.DockStyle.Fill;
            splGroupOfObjects.Location = new System.Drawing.Point(0, 0);
            splGroupOfObjects.Margin = new System.Windows.Forms.Padding(0);
            splGroupOfObjects.Name = "splGroupOfObjects";
            // 
            // splGroupOfObjects.Panel1
            // 
            splGroupOfObjects.Panel1.Controls.Add(grpGroupOfObjects);
            // 
            // splGroupOfObjects.Panel2
            // 
            splGroupOfObjects.Panel2.Controls.Add(grpObjectsList);
            splGroupOfObjects.Size = new System.Drawing.Size(944, 461);
            splGroupOfObjects.SplitterDistance = 500;
            splGroupOfObjects.SplitterWidth = 1;
            splGroupOfObjects.TabIndex = 15;
            // 
            // grpGroupOfObjects
            // 
            grpGroupOfObjects.BackColor = System.Drawing.SystemColors.Control;
            grpGroupOfObjects.Controls.Add(tlpGroupOfObjects);
            grpGroupOfObjects.Dock = System.Windows.Forms.DockStyle.Fill;
            grpGroupOfObjects.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpGroupOfObjects.ForeColor = System.Drawing.Color.MediumBlue;
            grpGroupOfObjects.Location = new System.Drawing.Point(0, 0);
            grpGroupOfObjects.Margin = new System.Windows.Forms.Padding(0);
            grpGroupOfObjects.Name = "grpGroupOfObjects";
            grpGroupOfObjects.Padding = new System.Windows.Forms.Padding(5);
            grpGroupOfObjects.Size = new System.Drawing.Size(500, 461);
            grpGroupOfObjects.TabIndex = 1;
            grpGroupOfObjects.TabStop = false;
            grpGroupOfObjects.Text = "grpGroupOfObjects";
            // 
            // tlpGroupOfObjects
            // 
            tlpGroupOfObjects.ColumnCount = 2;
            tlpGroupOfObjects.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            tlpGroupOfObjects.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpGroupOfObjects.Controls.Add(lblArealOrPopulation, 0, 6);
            tlpGroupOfObjects.Controls.Add(pnlArealOrPopulation, 1, 6);
            tlpGroupOfObjects.Controls.Add(lblDescriptionEn, 0, 4);
            tlpGroupOfObjects.Controls.Add(lblLabelEn, 0, 3);
            tlpGroupOfObjects.Controls.Add(lblDescriptionCs, 0, 1);
            tlpGroupOfObjects.Controls.Add(lblLabelCs, 0, 0);
            tlpGroupOfObjects.Controls.Add(txtDescriptionEn, 1, 4);
            tlpGroupOfObjects.Controls.Add(txtLabelEn, 1, 3);
            tlpGroupOfObjects.Controls.Add(txtDescriptionCs, 1, 1);
            tlpGroupOfObjects.Controls.Add(txtLabelCs, 1, 0);
            tlpGroupOfObjects.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpGroupOfObjects.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpGroupOfObjects.Location = new System.Drawing.Point(5, 25);
            tlpGroupOfObjects.Margin = new System.Windows.Forms.Padding(0);
            tlpGroupOfObjects.Name = "tlpGroupOfObjects";
            tlpGroupOfObjects.RowCount = 7;
            tlpGroupOfObjects.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpGroupOfObjects.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpGroupOfObjects.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpGroupOfObjects.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpGroupOfObjects.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpGroupOfObjects.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpGroupOfObjects.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpGroupOfObjects.Size = new System.Drawing.Size(490, 431);
            tlpGroupOfObjects.TabIndex = 1;
            // 
            // lblArealOrPopulation
            // 
            lblArealOrPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            lblArealOrPopulation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblArealOrPopulation.ForeColor = System.Drawing.Color.MediumBlue;
            lblArealOrPopulation.Location = new System.Drawing.Point(0, 400);
            lblArealOrPopulation.Margin = new System.Windows.Forms.Padding(0);
            lblArealOrPopulation.Name = "lblArealOrPopulation";
            lblArealOrPopulation.Size = new System.Drawing.Size(120, 31);
            lblArealOrPopulation.TabIndex = 10;
            lblArealOrPopulation.Text = "lblArealOrPopulation";
            lblArealOrPopulation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlArealOrPopulation
            // 
            pnlArealOrPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlArealOrPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            pnlArealOrPopulation.Location = new System.Drawing.Point(120, 400);
            pnlArealOrPopulation.Margin = new System.Windows.Forms.Padding(0);
            pnlArealOrPopulation.Name = "pnlArealOrPopulation";
            pnlArealOrPopulation.Size = new System.Drawing.Size(370, 31);
            pnlArealOrPopulation.TabIndex = 0;
            // 
            // lblDescriptionEn
            // 
            lblDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionEn.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionEn.Location = new System.Drawing.Point(0, 230);
            lblDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEn.Name = "lblDescriptionEn";
            lblDescriptionEn.Size = new System.Drawing.Size(120, 100);
            lblDescriptionEn.TabIndex = 7;
            lblDescriptionEn.Text = "lblDescriptionEn";
            // 
            // lblLabelEn
            // 
            lblLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelEn.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelEn.Location = new System.Drawing.Point(0, 200);
            lblLabelEn.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEn.Name = "lblLabelEn";
            lblLabelEn.Size = new System.Drawing.Size(120, 30);
            lblLabelEn.TabIndex = 6;
            lblLabelEn.Text = "lblLabelEn";
            lblLabelEn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCs
            // 
            lblDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionCs.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCs.Location = new System.Drawing.Point(0, 30);
            lblDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCs.Name = "lblDescriptionCs";
            lblDescriptionCs.Size = new System.Drawing.Size(120, 155);
            lblDescriptionCs.TabIndex = 2;
            lblDescriptionCs.Text = "lblDescriptionCs";
            // 
            // lblLabelCs
            // 
            lblLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelCs.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCs.Location = new System.Drawing.Point(0, 0);
            lblLabelCs.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCs.Name = "lblLabelCs";
            lblLabelCs.Size = new System.Drawing.Size(120, 30);
            lblLabelCs.TabIndex = 1;
            lblLabelCs.Text = "lblLabelCs";
            lblLabelCs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescriptionEn
            // 
            txtDescriptionEn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtDescriptionEn.ForeColor = System.Drawing.Color.Black;
            txtDescriptionEn.Location = new System.Drawing.Point(120, 230);
            txtDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEn.Multiline = true;
            txtDescriptionEn.Name = "txtDescriptionEn";
            txtDescriptionEn.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionEn.Size = new System.Drawing.Size(370, 155);
            txtDescriptionEn.TabIndex = 9;
            // 
            // txtLabelEn
            // 
            txtLabelEn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelEn.ForeColor = System.Drawing.Color.Black;
            txtLabelEn.Location = new System.Drawing.Point(120, 200);
            txtLabelEn.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEn.Name = "txtLabelEn";
            txtLabelEn.Size = new System.Drawing.Size(370, 16);
            txtLabelEn.TabIndex = 8;
            // 
            // txtDescriptionCs
            // 
            txtDescriptionCs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtDescriptionCs.ForeColor = System.Drawing.Color.Black;
            txtDescriptionCs.Location = new System.Drawing.Point(120, 30);
            txtDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCs.Multiline = true;
            txtDescriptionCs.Name = "txtDescriptionCs";
            txtDescriptionCs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionCs.Size = new System.Drawing.Size(370, 155);
            txtDescriptionCs.TabIndex = 5;
            // 
            // txtLabelCs
            // 
            txtLabelCs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelCs.ForeColor = System.Drawing.Color.Black;
            txtLabelCs.Location = new System.Drawing.Point(120, 0);
            txtLabelCs.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCs.Name = "txtLabelCs";
            txtLabelCs.Size = new System.Drawing.Size(370, 16);
            txtLabelCs.TabIndex = 4;
            // 
            // grpObjectsList
            // 
            grpObjectsList.Controls.Add(cklObjectsList);
            grpObjectsList.Dock = System.Windows.Forms.DockStyle.Fill;
            grpObjectsList.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpObjectsList.ForeColor = System.Drawing.Color.MediumBlue;
            grpObjectsList.Location = new System.Drawing.Point(0, 0);
            grpObjectsList.Margin = new System.Windows.Forms.Padding(0);
            grpObjectsList.Name = "grpObjectsList";
            grpObjectsList.Padding = new System.Windows.Forms.Padding(5);
            grpObjectsList.Size = new System.Drawing.Size(443, 461);
            grpObjectsList.TabIndex = 0;
            grpObjectsList.TabStop = false;
            grpObjectsList.Text = "grpObjectsList";
            // 
            // cklObjectsList
            // 
            cklObjectsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            cklObjectsList.Dock = System.Windows.Forms.DockStyle.Fill;
            cklObjectsList.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            cklObjectsList.ForeColor = System.Drawing.Color.Black;
            cklObjectsList.FormattingEnabled = true;
            cklObjectsList.HorizontalScrollbar = true;
            cklObjectsList.Location = new System.Drawing.Point(5, 25);
            cklObjectsList.Margin = new System.Windows.Forms.Padding(0);
            cklObjectsList.Name = "cklObjectsList";
            cklObjectsList.Size = new System.Drawing.Size(433, 431);
            cklObjectsList.TabIndex = 0;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlInsert, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 14;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlInsert
            // 
            pnlInsert.Controls.Add(btnOK);
            pnlInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInsert.Location = new System.Drawing.Point(624, 0);
            pnlInsert.Margin = new System.Windows.Forms.Padding(0);
            pnlInsert.Name = "pnlInsert";
            pnlInsert.Padding = new System.Windows.Forms.Padding(5);
            pnlInsert.Size = new System.Drawing.Size(160, 40);
            pnlInsert.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // FormLDsityObjectGroupNew
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormLDsityObjectGroupNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormGroupOfObjectsNew";
            tlpMain.ResumeLayout(false);
            splGroupOfObjects.Panel1.ResumeLayout(false);
            splGroupOfObjects.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splGroupOfObjects).EndInit();
            splGroupOfObjects.ResumeLayout(false);
            grpGroupOfObjects.ResumeLayout(false);
            tlpGroupOfObjects.ResumeLayout(false);
            tlpGroupOfObjects.PerformLayout();
            grpObjectsList.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlInsert.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlInsert;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.SplitContainer splGroupOfObjects;
        private System.Windows.Forms.GroupBox grpGroupOfObjects;
        private System.Windows.Forms.TableLayoutPanel tlpGroupOfObjects;
        private System.Windows.Forms.Label lblArealOrPopulation;
        private System.Windows.Forms.Panel pnlArealOrPopulation;
        private System.Windows.Forms.Label lblDescriptionEn;
        private System.Windows.Forms.Label lblLabelEn;
        private System.Windows.Forms.Label lblDescriptionCs;
        private System.Windows.Forms.Label lblLabelCs;
        private System.Windows.Forms.TextBox txtDescriptionEn;
        private System.Windows.Forms.TextBox txtLabelEn;
        private System.Windows.Forms.TextBox txtDescriptionCs;
        private System.Windows.Forms.TextBox txtLabelCs;
        private System.Windows.Forms.GroupBox grpObjectsList;
        private System.Windows.Forms.CheckedListBox cklObjectsList;
    }

}