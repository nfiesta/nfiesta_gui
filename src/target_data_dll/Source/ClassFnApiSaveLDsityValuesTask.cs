﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Úkol pracovního vlákna pro výpočet lokálních hustot</para>
        /// <para lang="en">Task for worker thread for calculation local densities</para>
        /// </summary>
        internal class FnApiSaveLDsityValuesTask
            : ThreadTask<TFnGetRefYearSetToPanelMappingForGroup>
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Cílová proměnná</para>
            /// <para lang="en">Target variable</para>
            /// </summary>
            private TDTargetVariable targetVariable;

            /// <summary>
            /// <para lang="cs">Mezní hodnota pro výpočet lokálních hustot</para>
            /// <para lang="en">Local density threshold</para>
            /// </summary>
            private double localDensityThreshold;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="refYearSetToPanelMapping">
            /// <para lang="cs">Kombinace panelu a referenčního období</para>
            /// <para lang="en">Panel reference year set combination</para>
            /// </param>
            /// <param name="targetVariable">
            /// <para lang="cs">Cílová proměnná</para>
            /// <para lang="en">Target variable</para>
            /// </param>
            /// <param name="localDensityThreshold">
            /// <para lang="cs">Mezní hodnota pro výpočet lokálních hustot</para>
            /// <para lang="en">Local density threshold</para>
            /// </param>
            public FnApiSaveLDsityValuesTask(
                TFnGetRefYearSetToPanelMappingForGroup refYearSetToPanelMapping,
                TDTargetVariable targetVariable,
                double localDensityThreshold)
                : base(element: refYearSetToPanelMapping, priority: 1)
            {
                TargetVariable = targetVariable;
                LocalDensityThreshold = localDensityThreshold;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Cílová proměnná</para>
            /// <para lang="en">Target variable</para>
            /// </summary>
            public TDTargetVariable TargetVariable
            {
                get
                {
                    return targetVariable;
                }
                private set
                {
                    targetVariable = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Mezní hodnota pro výpočet lokálních hustot</para>
            /// <para lang="en">Local density threshold</para>
            /// </summary>
            public double LocalDensityThreshold
            {
                get
                {
                    return localDensityThreshold;
                }
                private set
                {
                    localDensityThreshold = value;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true|false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not FnApiSaveLDsityValuesTask Type, return False
                if (obj is not FnApiSaveLDsityValuesTask)
                {
                    return false;
                }

                return
                    ((FnApiSaveLDsityValuesTask)obj).Element.Equals(obj: Element) &&
                    ((FnApiSaveLDsityValuesTask)obj).Priority.Equals(obj: Priority) &&
                    ((FnApiSaveLDsityValuesTask)obj).TargetVariable.Equals(obj: TargetVariable) &&
                    ((FnApiSaveLDsityValuesTask)obj).LocalDensityThreshold.Equals(obj: LocalDensityThreshold);
            }

            /// <summary>
            /// Returns a hash code for the current object
            /// </summary>
            /// <returns>Hash code for the current object</returns>
            public override int GetHashCode()
            {
                return
                    Element.GetHashCode() ^
                    Priority.GetHashCode() ^
                    TargetVariable.GetHashCode() ^
                    LocalDensityThreshold.GetHashCode();
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>String that represents the current object</returns>
            public override string ToString()
            {
                return
                    (Element?.Id == null)
                        ? $"{nameof(Element)}: {ZaJi.PostgreSQL.Functions.StrNull}"
                        : $"{nameof(Element)}: {Element.Id}";
            }

            #endregion Methods

        }

    }
}