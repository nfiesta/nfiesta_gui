﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlTDCategoryDesign
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splMain = new System.Windows.Forms.SplitContainer();
            this.lblCaption = new System.Windows.Forms.Label();
            this.pnlItemsOuter = new System.Windows.Forms.Panel();
            this.pnlItems = new System.Windows.Forms.Panel();
            this.cmsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmsiDuplicate = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsiRemove = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).BeginInit();
            this.splMain.Panel1.SuspendLayout();
            this.splMain.Panel2.SuspendLayout();
            this.splMain.SuspendLayout();
            this.pnlItemsOuter.SuspendLayout();
            this.cmsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // splMain
            // 
            this.splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splMain.IsSplitterFixed = true;
            this.splMain.Location = new System.Drawing.Point(0, 0);
            this.splMain.Margin = new System.Windows.Forms.Padding(0);
            this.splMain.Name = "splMain";
            this.splMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMain.Panel1
            // 
            this.splMain.Panel1.Controls.Add(this.lblCaption);
            // 
            // splMain.Panel2
            // 
            this.splMain.Panel2.Controls.Add(this.pnlItemsOuter);
            this.splMain.Size = new System.Drawing.Size(200, 100);
            this.splMain.SplitterDistance = 25;
            this.splMain.SplitterWidth = 1;
            this.splMain.TabIndex = 0;
            // 
            // lblCaption
            // 
            this.lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Location = new System.Drawing.Point(0, 0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(200, 25);
            this.lblCaption.TabIndex = 0;
            this.lblCaption.Text = "lblCaption";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlItemsOuter
            // 
            this.pnlItemsOuter.Controls.Add(this.pnlItems);
            this.pnlItemsOuter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlItemsOuter.Location = new System.Drawing.Point(0, 0);
            this.pnlItemsOuter.Margin = new System.Windows.Forms.Padding(0);
            this.pnlItemsOuter.Name = "pnlItemsOuter";
            this.pnlItemsOuter.Padding = new System.Windows.Forms.Padding(3);
            this.pnlItemsOuter.Size = new System.Drawing.Size(200, 74);
            this.pnlItemsOuter.TabIndex = 0;
            // 
            // pnlItems
            // 
            this.pnlItems.BackColor = System.Drawing.SystemColors.Window;
            this.pnlItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlItems.Location = new System.Drawing.Point(3, 3);
            this.pnlItems.Margin = new System.Windows.Forms.Padding(0);
            this.pnlItems.Name = "pnlItems";
            this.pnlItems.Size = new System.Drawing.Size(194, 68);
            this.pnlItems.TabIndex = 0;
            // 
            // cmsMenu
            // 
            this.cmsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmsiDuplicate,
            this.cmsiRemove});
            this.cmsMenu.Name = "cmsContext";
            this.cmsMenu.Size = new System.Drawing.Size(150, 48);
            this.cmsMenu.Text = "cmsMenu";
            // 
            // cmsiDuplicate
            // 
            this.cmsiDuplicate.Name = "cmsiDuplicate";
            this.cmsiDuplicate.Size = new System.Drawing.Size(149, 22);
            this.cmsiDuplicate.Text = "cmsiDuplicate";
            // 
            // cmsiRemove
            // 
            this.cmsiRemove.Name = "cmsiRemove";
            this.cmsiRemove.Size = new System.Drawing.Size(149, 22);
            this.cmsiRemove.Text = "cmsiRemove";
            // 
            // ControlTDCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.splMain);
            this.Name = "ControlTDCategory";
            this.Size = new System.Drawing.Size(200, 100);
            this.splMain.Panel1.ResumeLayout(false);
            this.splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).EndInit();
            this.splMain.ResumeLayout(false);
            this.pnlItemsOuter.ResumeLayout(false);
            this.cmsMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Panel pnlItemsOuter;
        private System.Windows.Forms.Panel pnlItems;
        private System.Windows.Forms.ContextMenuStrip cmsMenu;
        private System.Windows.Forms.ToolStripMenuItem cmsiDuplicate;
        private System.Windows.Forms.ToolStripMenuItem cmsiRemove;
    }

}
