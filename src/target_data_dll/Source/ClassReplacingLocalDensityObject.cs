﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">
        /// Párování objektů lokálních hustot pro třídění,
        /// klíč: objekt lokální hustoty pro třídění z databáze,
        /// hodnota: objekt lokální hustoty pro třídění z json souboru
        /// </para>
        /// <para lang="en">
        /// Pairing local density objects for classification,
        /// key: local density object for classification from database,
        /// value: local density object for classification from json file
        /// </para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        [SupportedOSPlatform("windows")]
        internal class ReplacingLocalDensityObject<TDomain, TCategory>
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">
            /// Párování objektů lokálních hustot pro třídění,
            /// klíč: objekt lokální hustoty pro třídění z databáze,
            /// hodnota: objekt lokální hustoty pro třídění z json souboru
            /// </para>
            /// <para lang="en">
            /// Pairing local density objects for classification,
            /// key: local density object for classification from database,
            /// value: local density object for classification from json file
            /// </para>
            /// </summary>
            private readonly Dictionary<TDLDsityObject, ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON> pairing;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">
            /// Konstruktor objektu
            /// </para>
            /// <para lang="en">
            /// Object constructor
            /// </para>
            /// </summary>
            public ReplacingLocalDensityObject()
            {
                pairing = [];
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">
            /// Prázdný objekt
            /// </para>
            /// <para lang="en">
            /// Empty object
            /// </para>
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    return pairing.Count == 0;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Přidání páru objektů lokálních hustot pro třídění</para>
            /// <para lang="en">Adds local density objects for ADSP pair</para>
            /// </summary>
            /// <param name="ldsityObjectForADSP">
            /// <para lang="cs">Klíč: objekt lokální hustoty pro třídění z databáze</para>
            /// <para lang="en">Key: local density object for classification from database</para>
            /// </param>
            /// <param name="ldsityObjectForADSPJson">
            /// <para lang="cs">Hodnota: objekt lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Value: local density object for classification from json file</para>
            /// </param>
            public void Add(
                TDLDsityObject ldsityObjectForADSP,
                ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON ldsityObjectForADSPJson)
            {
                // Klíč nesmí být null a musí být unique
                if (ldsityObjectForADSP == null) { return; }
                if (pairing.ContainsKey(key: ldsityObjectForADSP)) { return; }

                pairing.Add(
                    key: ldsityObjectForADSP,
                    value: ldsityObjectForADSPJson);
            }

            /// <summary>
            /// <para lang="cs">Vrací hodnotu: objekt lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: local density object for classification from json file</para>
            /// </summary>
            /// <param name="ldsityObjectForADSP">
            /// <para lang="cs">Klíč: objekt lokální hustoty pro třídění z databáze</para>
            /// <para lang="en">Key: local density object for classification from database</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací hodnotu: objekt lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: local density object for classification from json file</para>
            /// </returns>
            public ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON GetValue(
                TDLDsityObject ldsityObjectForADSP)
            {
                // Klíč nesmí být null a musí být ve slovníku
                if (ldsityObjectForADSP == null) { return null; }
                if (!pairing.TryGetValue(key: ldsityObjectForADSP,
                    out ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON value)) { return null; }

                return value;
            }

            /// <summary>
            /// <para lang="cs">Vrací hodnotu: objekt lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: local density object for classification from json file</para>
            /// </summary>
            /// <param name="id">
            /// <para lang="cs">Klíč: identifikátor objektu lokální hustoty pro třídění z databáze</para>
            /// <para lang="en">Key: identifier of the local density object for classification from database</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací hodnotu: objekt lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: local density object for classification from json file</para>
            /// </returns>
            public ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON GetValue(int id)
            {
                TDLDsityObject key =
                    pairing.Keys
                    .Where(a => a.Id == id)
                    .FirstOrDefault<TDLDsityObject>();

                return GetValue(ldsityObjectForADSP: key);
            }

            /// <summary>
            /// <para lang="cs">Vrací hodnotu: identifikátor objektu lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: identifier of the local density object for classification from json file</para>
            /// </summary>
            /// <param name="ldsityObjectForADSP">
            /// <para lang="cs">Klíč: objekt lokální hustoty pro třídění z databáze</para>
            /// <para lang="en">Key: local density object for classification from database</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací hodnotu: identifikátor objektu lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: identifier of the local density object for classification from json file</para>
            /// </returns>
            public int GetIdentifier(TDLDsityObject ldsityObjectForADSP)
            {
                return
                    GetValue(ldsityObjectForADSP: ldsityObjectForADSP)?.LocalDensityObjectId
                        ?? 0;
            }

            /// <summary>
            /// <para lang="cs">Vrací hodnotu: identifikátor objektu lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: identifier of the local density object for classification from json file</para>
            /// </summary>
            /// <param name="id">
            /// <para lang="cs">Klíč: identifikátor objektu lokální hustoty pro třídění z databáze</para>
            /// <para lang="en">Key: identifier of the local density object for classification from database</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací hodnotu: identifikátor objektu lokální hustoty pro třídění z json souboru</para>
            /// <para lang="en">Returns value: identifier of the local density object for classification from json file</para>
            /// </returns>
            public int GetIdentifier(int id)
            {
                return
                    GetValue(id: id)?.LocalDensityObjectId
                        ?? 0;
            }

            /// <summary>
            /// <para lang="cs">Najde reálný LDO (klíč) podle LocalDensityObjectId z JSON (hodnota). Pokud nenajde, vrátí null.</para>
            /// <para lang="en">Finds the real LDO (key) based on the LocalDensityObjectId from JSON (value). If not found, returns null.</para>
            /// </summary>
            /// <param name="localDensityObjectIdFromJson">
            /// <para lang="cs">Identifikátor objektu lokální hustoty z JSON souboru, podle kterého se vyhledává v databázi.</para>
            /// <para lang="en">Identifier of the local density object from the JSON file used for database lookup.</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací reálný LDO (klíč) z databáze, nebo null, pokud nebyl nalezen.</para>
            /// <para lang="en">Returns the real LDO (key) from the database, or null if not found.</para>
            /// </returns>
            public TDLDsityObject FindLdoInDbByJson(int localDensityObjectIdFromJson)
            {
                // Projdeme všechny páry a hledáme ten, kde pairing[Key].LocalDensityObjectId == localDensityObjectIdFromJson
                // Go through all pairs and look for the one where pairing[Key].LocalDensityObjectId == localDensityObjectIdFromJson
                foreach (var kvp in pairing)
                {
                    TDLDsityObject ldsityObjectForADSP = kvp.Key;
                    var jsonLdsityObjectForADSP = kvp.Value;
                    if (jsonLdsityObjectForADSP != null && jsonLdsityObjectForADSP.LocalDensityObjectId == localDensityObjectIdFromJson)
                    {
                        // klíč
                        // key
                        return ldsityObjectForADSP;
                    }
                }

                // nebyl nalezen
                // not found
                return null;
            }

            #endregion Methods

        }

    }
}