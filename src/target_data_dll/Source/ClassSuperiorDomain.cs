﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Nadřazená plošná doména nebo subpopulace</para>
        /// <para lang="en">Superior area domain or subpopulation</para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        [SupportedOSPlatform("windows")]
        internal class SuperiorDomain<TDomain, TCategory>
            : INfiEstaObject, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            private object owner;

            /// <summary>
            /// <para lang="cs">Plošná doména nebo subpopulace</para>
            /// <para lang="en">Area domain or subpopulation</para>
            /// </summary>
            private TDomain domain;

            /// <summary>
            /// <para lang="cs">Podřízené plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domains or subpopulations</para>
            /// </summary>
            private List<TDomain> inferiorDomains;

            /// <summary>
            /// <para lang="cs">Kategorie podřízených plošných domén nebo subpopulací</para>
            /// <para lang="en">Categories of inferior area domains or subpopulations</para>
            /// </summary>
            private List<TCategory> inferiorCategories;

            /// <summary>
            /// <para lang="cs">Páry nadřazené a podřízené kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Pairs of a superior and inferior area domain or subpopulation category</para>
            /// </summary>
            private List<CategoryPair<TDomain, TCategory>> categoryPairs;

            /// <summary>
            /// <para lang="cs">Páry nadřazené a podřízené kategorie plošné domény nebo subpopulace před provedením změn</para>
            /// <para lang="en">Pairs of a superior and inferior area domain or subpopulation category before changes are made</para>
            /// </summary>
            private List<CategoryPair<TDomain, TCategory>> originalCategoryPairs;

            #endregion Private Fields


            #region Contructor

            /// <summary>
            /// <para lang="cs">Konstruktor nadřazené plošné domény nebo subpopulace
            /// (pro existující podřízené plošné domény nebo subpopulace)</para>
            /// <para lang="en">Constructor of a superior area domain or subpopulation
            /// (for existing inferior area domains or subpopulations)</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            /// <param name="domain">
            /// <para lang="cs">Plošná doména nebo subpopulace</para>
            /// <para lang="en">Area domain or subpopulation</para>
            /// </param>
            /// <param name="inferiorDomains">
            /// <para lang="cs">Existující podřízené plošné domény nebo subpopulace</para>
            /// <para lang="en">Existing inferior area domains or subpopulations</para>
            /// </param>
            public SuperiorDomain(
                object owner,
                TDomain domain,
                List<TDomain> inferiorDomains)
            {
                Initialize(
                    owner: owner,
                    domain: domain,
                    inferiorDomains: inferiorDomains);
            }

            /// <summary>
            /// <para lang="cs">Konstruktor nadřazené plošné domény nebo subpopulace
            /// (pro novou podřízenou plošnou doménu nebo subpopulaci)</para>
            /// <para lang="en">Constructor of a superior area domain or subpopulation
            /// (for a new inferior area domain or subpopulation)</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            /// <param name="domain">
            /// <para lang="cs">Plošná doména nebo subpopulace</para>
            /// <para lang="en">Area domain or subpopulation</para>
            /// </param>
            /// <param name="inferiorDomain">
            /// <para lang="cs">Nová podřízená plošná doména nebo subpopulace</para>
            /// <para lang="en">New inferior area domain or subpopulation</para>
            /// </param>
            /// <param name="inferiorCategories">
            /// <para lang="cs">Nové kategorie podřízené plošné domény nebo subpopulace</para>
            /// <para lang="en">New categories of an inferior area domain or subpopulation</para>
            /// </param>
            public SuperiorDomain(
                object owner,
                TDomain domain,
                TDomain inferiorDomain,
                List<TCategory> inferiorCategories)
            {
                Initialize(
                    owner: owner,
                    domain: domain,
                    inferiorDomain: inferiorDomain,
                    inferiorCategories: inferiorCategories);
            }

            #endregion Constructor


            #region Common Properties

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            public object Owner
            {
                get
                {
                    if (owner == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                    }

                    if (owner is not INfiEstaControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(INfiEstaControl)}.",
                            paramName: nameof(Owner));
                    }

                    if (owner is not ITargetDataControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(ITargetDataControl)}.",
                            paramName: nameof(Owner));
                    }

                    return owner;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                    }

                    if (value is not INfiEstaControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(INfiEstaControl)}.",
                            paramName: nameof(Owner));
                    }

                    if (value is not ITargetDataControl)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Owner)} must be type of {nameof(ITargetDataControl)}.",
                            paramName: nameof(Owner));
                    }

                    owner = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Databázové tabulky (read-only)</para>
            /// <para lang="en">Database tables(read-only)</para>
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return ((INfiEstaControl)Owner).Database;
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyková verze (read-only)</para>
            /// <para lang="en">Language version (read-only)</para>
            /// </summary>
            public LanguageVersion LanguageVersion
            {
                get
                {
                    return ((INfiEstaControl)Owner).LanguageVersion;
                }
            }

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            public LanguageFile LanguageFile
            {
                get
                {
                    return ((ITargetDataControl)Owner).LanguageFile;
                }
            }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
            /// <para lang="en">Module for target data setting (read-only)</para>
            /// </summary>
            public Setting Setting
            {
                get
                {
                    return ((ITargetDataControl)Owner).Setting;
                }
            }

            #endregion Common Properties


            #region Properties


            /// <summary>
            /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
            /// <para lang="en">Domain and its categories type (read-only)</para>
            /// </summary>
            public static TDArealOrPopulationEnum ArealOrPopulation
            {
                get
                {
                    return typeof(TDomain).FullName switch
                    {
                        "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    TDArealOrPopulationEnum.AreaDomain,

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                        paramName: nameof(TCategory)),

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    TDArealOrPopulationEnum.Population,

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        _ =>
                            throw new ArgumentException(
                                message: String.Concat(
                                    $"Argument {nameof(TDomain)} must be type of ",
                                    $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                                paramName: nameof(TDomain))
                    };
                }
            }


            /// <summary>
            /// <para lang="cs">Plošná doména nebo subpopulace (read-only)</para>
            /// <para lang="en">Area domain or subpopulation (read-only)</para>
            /// </summary>
            public TDomain Domain
            {
                get
                {
                    if (domain != null)
                    {
                        return domain;
                    }
                    else
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Domain)} must not be null.",
                            paramName: nameof(Domain));
                    }
                }
                set
                {
                    if (value != null)
                    {
                        domain = value;
                    }
                    else
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(Domain)} must not be null.",
                            paramName: nameof(Domain));
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Podřízené plošné domény nebo subpopulace (read-only)</para>
            /// <para lang="en">Inferior area domains or subpopulations(read-only)</para>
            /// </summary>
            public List<TDomain> InferiorDomains
            {
                get
                {
                    return inferiorDomains;
                }
            }

            /// <summary>
            /// <para lang="cs">Kategorie plošné domény nebo subpopulace (read-only)</para>
            /// <para lang="en">Area domain or subpopulation categories (read-only)</para>
            /// </summary>
            public List<TCategory> Categories
            {
                get
                {
                    return ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain =>
                            Database.STargetData.CAreaDomainCategory.Items
                                .Where(c => (c.AreaDomainId ?? 0) == Domain.Id)
                                .Select(c => (TCategory)Convert.ChangeType(
                                    value: c,
                                    conversionType: typeof(TCategory)))
                                .ToList<TCategory>(),

                        TDArealOrPopulationEnum.Population =>
                            Database.STargetData.CSubPopulationCategory.Items
                                .Where(c => (c.SubPopulationId ?? 0) == Domain.Id)
                                .Select(c => (TCategory)Convert.ChangeType(
                                    value: c,
                                    conversionType: typeof(TCategory)))
                                .ToList<TCategory>(),

                        _ => [],
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Kategorie podřízených plošných domén nebo subpopulací (read-only)</para>
            /// <para lang="en">Categories of inferior area domains or subpopulations (read-only)</para>
            /// </summary>
            public List<TCategory> InferiorCategories
            {
                get
                {
                    return inferiorCategories;
                }
            }

            /// <summary>
            /// <para lang="cs">Páry nadřazené a podřízené kategorie plošné domény nebo subpopulace (read-only)</para>
            /// <para lang="en">Pairs of a superior and inferior area domain or subpopulation category (read-only)</para>
            /// </summary>
            public List<CategoryPair<TDomain, TCategory>> CategoryPairs
            {
                get
                {
                    return categoryPairs;
                }
            }

            /// <summary>
            /// <para lang="cs">Páry nadřazené a podřízené kategorie plošné domény nebo subpopulace před provedením změn</para>
            /// <para lang="en">Pairs of a superior and inferior area domain or subpopulation category before changes are made</para>
            /// </summary>
            public List<CategoryPair<TDomain, TCategory>> OriginalCategoryPairs
            {
                get
                {
                    return originalCategoryPairs;
                }
            }

            /// <summary>
            /// <para lang="cs">Popis plošné domény nebo subpopulace (národní) (read-only)</para>
            /// <para lang="en">Area domain or subpopulation label (national) (read-only)</para>
            /// </summary>
            public string LabelCs
            {
                get
                {
                    return Domain.LabelCs;
                }
            }

            /// <summary>
            /// <para lang="cs">Popis plošné domény nebo subpopulace (anglicky) (read-only)</para>
            /// <para lang="en">Area domain or subpopulation label (English) (read-only)</para>
            /// </summary>
            public string LabelEn
            {
                get
                {
                    return Domain.LabelEn;
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Zvolený jazyk</para>
            /// <para lang="en">Selected language</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">File with control labels for national and international version</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts in selected language</para>
            /// </returns>
            public static Dictionary<string, string> Dictionary(
                LanguageVersion languageVersion,
                LanguageFile languageFile)
            {
                return languageVersion switch
                {
                    LanguageVersion.National =>
                    ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                            ? []
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(SuperiorDomain<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                                out Dictionary<string, string> dictNationalAreaDomain)
                                    ? dictNationalAreaDomain
                                    : [],

                        TDArealOrPopulationEnum.Population => (languageFile == null)
                            ? []
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(SuperiorDomain<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                out Dictionary<string, string> dictNationalSubPopulation)
                                    ? dictNationalSubPopulation
                                    : [],

                        _ => [],
                    },

                    LanguageVersion.International =>
                    ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                            ? []
                            : languageFile.InternationalVersion.Data.TryGetValue(
                                key: $"{nameof(SuperiorDomain<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                                out Dictionary<string, string> dictInternationalAreaDomain)
                                    ? dictInternationalAreaDomain
                                    : [],

                        TDArealOrPopulationEnum.Population => (languageFile == null)
                            ? []
                            : languageFile.InternationalVersion.Data.TryGetValue(
                                key: $"{nameof(SuperiorDomain<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                out Dictionary<string, string> dictInternationalSubPopulation)
                                    ? dictInternationalSubPopulation
                                    : [],

                        _ => [],
                    },

                    _ => [],
                };
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Inicializace objektu nadřazené plošné domény nebo subpopulace
            /// (pro existující podřízené plošné domény nebo subpopulace)</para>
            /// <para lang="en">Initializing the object of a superior area domain or subpopulation
            /// (for existing inferior area domains or subpopulations)</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            /// <param name="domain">
            /// <para lang="cs">Plošná doména nebo subpopulace</para>
            /// <para lang="en">Area domain or subpopulation</para>
            /// </param>
            /// <param name="inferiorDomains">
            /// <para lang="cs">Podřízené plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domains or subpopulations</para>
            /// </param>
            private void Initialize(
                object owner,
                TDomain domain,
                List<TDomain> inferiorDomains)
            {
                Owner = owner;

                Domain = domain;

                // InferiorDomains
                if (inferiorDomains != null)
                {
                    this.inferiorDomains =
                        inferiorDomains
                            .Where(a => a.Id != Domain.Id)
                            .ToList<TDomain>();
                }
                else
                {
                    throw new Exception(message: $"Argument {nameof(InferiorDomains)} must not be null.");
                }

                switch (ArealOrPopulation)
                {
                    case TDArealOrPopulationEnum.AreaDomain:

                        #region AreaDomain

                        // InferiorCategories
                        inferiorCategories =
                            Database.STargetData.CAreaDomainCategory.Items
                                .Where(c => InferiorDomains.Select(a => a.Id).Contains(value: c.AreaDomainId ?? 0))
                                .Select(c => (TCategory)Convert.ChangeType(
                                    value: c,
                                    conversionType: typeof(TCategory)))
                                .ToList<TCategory>();

                        // CategoryPairs
                        categoryPairs = Database.STargetData.TADCHierarchy.Items
                            .Where(a => Categories.Select(b => b.Id).Contains(value: a.VariableSuperiorId))
                            .Where(a => InferiorCategories.Select(b => b.Id).Contains(value: a.VariableId))
                            .Select(a =>
                                new CategoryPair<TDomain, TCategory>(
                                    (TCategory)Convert.ChangeType(
                                        value: a.VariableSuperior,
                                        conversionType: typeof(TCategory)),
                                    (TCategory)Convert.ChangeType(
                                        value: a.Variable,
                                        conversionType: typeof(TCategory))))
                            .ToList<CategoryPair<TDomain, TCategory>>();

                        // OriginalCategoryPairs
                        originalCategoryPairs = Database.STargetData.TADCHierarchy.Items
                            .Where(a => Categories.Select(b => b.Id).Contains(value: a.VariableSuperiorId))
                            .Where(a => InferiorCategories.Select(b => b.Id).Contains(value: a.VariableId))
                            .Select(a =>
                                new CategoryPair<TDomain, TCategory>(
                                    (TCategory)Convert.ChangeType(
                                        value: a.VariableSuperior,
                                        conversionType: typeof(TCategory)),
                                    (TCategory)Convert.ChangeType(
                                        value: a.Variable,
                                        conversionType: typeof(TCategory))))
                            .ToList<CategoryPair<TDomain, TCategory>>();
                        break;

                    #endregion AreaDomain

                    case TDArealOrPopulationEnum.Population:

                        #region Population

                        // InferiorCategories
                        inferiorCategories =
                            Database.STargetData.CSubPopulationCategory.Items
                                .Where(c => InferiorDomains.Select(a => a.Id).Contains(value: c.SubPopulationId ?? 0))
                                .Select(c => (TCategory)Convert.ChangeType(
                                    value: c,
                                    conversionType: typeof(TCategory)))
                                .ToList<TCategory>();

                        // CategoryPairs
                        categoryPairs = Database.STargetData.TSPCHierarchy.Items
                            .Where(a => Categories.Select(b => b.Id).Contains(value: a.VariableSuperiorId))
                            .Where(a => InferiorCategories.Select(b => b.Id).Contains(value: a.VariableId))
                            .Select(a =>
                                new CategoryPair<TDomain, TCategory>(
                                    (TCategory)Convert.ChangeType(
                                        value: a.VariableSuperior,
                                        conversionType: typeof(TCategory)),
                                    (TCategory)Convert.ChangeType(
                                        value: a.Variable,
                                        conversionType: typeof(TCategory))))
                            .ToList<CategoryPair<TDomain, TCategory>>();

                        // OriginalCategoryPairs
                        originalCategoryPairs = Database.STargetData.TSPCHierarchy.Items
                            .Where(a => Categories.Select(b => b.Id).Contains(value: a.VariableSuperiorId))
                            .Where(a => InferiorCategories.Select(b => b.Id).Contains(value: a.VariableId))
                            .Select(a => new CategoryPair<TDomain, TCategory>(
                                (TCategory)Convert.ChangeType(
                                    value: a.VariableSuperior,
                                    conversionType: typeof(TCategory)),
                                (TCategory)Convert.ChangeType(
                                    value: a.Variable,
                                    conversionType: typeof(TCategory))))
                            .ToList<CategoryPair<TDomain, TCategory>>();
                        break;

                    #endregion Population

                    default:
                        #region Default

                        // InferiorCategories
                        inferiorCategories = [];

                        // CategoryPairs
                        categoryPairs = [];

                        // OriginalCategoryPairs
                        originalCategoryPairs = [];

                        break;

                        #endregion Default
                }

            }

            /// <summary>
            /// <para lang="cs">Inicializace objektu nadřazené plošné domény nebo subpopulace
            /// (pro novou podřízenou plošnou doménu nebo subpopulaci)</para>
            /// <para lang="en">Initializing the object of a superior area domain or subpopulation
            /// (for a new inferior area domain or subpopulation)</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            /// <param name="domain">
            /// <para lang="cs">Plošná doména nebo subpopulace</para>
            /// <para lang="en">Area domain or subpopulation</para>
            /// </param>
            /// <param name="inferiorDomain">
            /// <para lang="cs">Nová podřízená plošná doména nebo subpopulace</para>
            /// <para lang="en">New inferior area domain or subpopulation</para>
            /// </param>
            /// <param name="inferiorCategories">
            /// <para lang="cs">Nové kategorie podřízené plošné domény nebo subpopulace</para>
            /// <para lang="en">New categories of an inferior area domain or subpopulation</para>
            /// </param>
            private void Initialize(
                object owner,
                TDomain domain,
                TDomain inferiorDomain,
                List<TCategory> inferiorCategories)
            {
                Owner = owner;

                Domain = domain;

                // InferiorDomains
                if (inferiorDomain != null)
                {
                    inferiorDomains =
                        (Domain.Id != inferiorDomain.Id) ?
                        [inferiorDomain] :
                        [];
                }
                else
                {
                    throw new Exception(message: $"Argument {nameof(InferiorDomains)} must not be null.");
                }

                // InferiorCategories
                if (inferiorCategories != null)
                {
                    this.inferiorCategories =
                        (domain.Id != inferiorDomain.Id) ?
                            inferiorCategories :
                            [];
                }
                else
                {
                    throw new Exception(message: $"Argument {nameof(InferiorCategories)} must not be null.");
                }

                // CategoryPairs
                categoryPairs = [];

                // OriginalCategoryPairs
                originalCategoryPairs = [];
            }

            /// <summary>
            /// <para lang="cs">Podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou doménu nebo subpopulaci</para>
            /// <para lang="en">Inferior area domain or subpopulation categories
            /// for the selected inferior area domain or subpopulation</para>
            /// </summary>
            /// <param name="inferiorDomain">
            /// <para lang="cs">Podřízená plošná doména nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou plošnou doménu nebo subpopulaci</para>
            /// <para lang="en">Inferior area domain or subpopulation categories
            /// for the selected inferior area domain or subpopulation</para>
            /// </returns>
            public List<TCategory> GetInferiorCategories(TDomain inferiorDomain)
            {
                if (inferiorDomain == null)
                {
                    return [];
                }

                return ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain =>
                        InferiorCategories
                            .Select(c => (TDAreaDomainCategory)Convert.ChangeType(
                                value: c,
                                conversionType: typeof(TDAreaDomainCategory)))
                            .Where(c => (c.AreaDomainId ?? 0) == inferiorDomain.Id)
                            .Select(c => (TCategory)Convert.ChangeType(
                                value: c,
                                conversionType: typeof(TCategory)))
                            .ToList<TCategory>(),

                    TDArealOrPopulationEnum.Population =>
                        InferiorCategories
                            .Select(c => (TDSubPopulationCategory)Convert.ChangeType(
                                value: c,
                                conversionType: typeof(TDSubPopulationCategory)))
                            .Where(c => (c.SubPopulationId ?? 0) == inferiorDomain.Id)
                            .Select(c => (TCategory)Convert.ChangeType(
                                value: c,
                                conversionType: typeof(TCategory)))
                            .ToList<TCategory>(),

                    _ => [],
                };
            }

            /// <summary>
            /// <para lang="cs">Páry nadřazené a podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou plošnou doménu nebo subpopulaci</para>
            /// <para lang="en">Pairs of a superior and inferior area domain or subpopulation category
            /// for the selected inferior area domain or subpopulation</para>
            /// </summary>
            /// <param name="inferiorDomain">
            /// <para lang="cs">Podřízená plošná doména nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Páry nadřazené a podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou plošnou doménu nebo subpopulaci</para>
            /// <para lang="en">Pairs of a superior and inferior area domain or subpopulation category
            /// for the selected inferior area domain or subpopulation</para>
            /// </returns>
            public List<CategoryPair<TDomain, TCategory>> GetCategoryPairs(TDomain inferiorDomain)
            {
                if (inferiorDomain == null)
                {
                    return [];
                }

                return ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain =>
                        CategoryPairs
                            .Where(p => (
                                (p.SuperiorAreaDomainCategory == null) ? 0 :
                                    p.SuperiorAreaDomainCategory.AreaDomainId ?? 0) == Domain.Id)
                            .Where(p => (
                                (p.InferiorAreaDomainCategory == null) ? 0 :
                                    p.InferiorAreaDomainCategory.AreaDomainId ?? 0) == inferiorDomain.Id)
                            .ToList<CategoryPair<TDomain, TCategory>>(),

                    TDArealOrPopulationEnum.Population =>
                        CategoryPairs
                            .Where(p => (
                                (p.SuperiorSubPopulationCategory == null) ? 0 :
                                    p.SuperiorSubPopulationCategory.SubPopulationId ?? 0) == Domain.Id)
                            .Where(p => (
                                (p.InferiorSubPopulationCategory == null) ? 0 :
                                    p.InferiorSubPopulationCategory.SubPopulationId ?? 0) == inferiorDomain.Id)
                            .ToList<CategoryPair<TDomain, TCategory>>(),

                    _ => [],
                };
            }

            /// <summary>
            /// <para lang="cs">Nezařazené podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou plošnou doménu nebo subpopulaci</para>
            /// <para lang="en">Unclassified inferior area domain or subpopulation categories
            /// for the selected inferior area domain or subpopulation</para>
            /// </summary>
            /// <param name="inferiorDomain">
            /// <para lang="cs">Podřízená plošná doména nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Nezařazené podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou plošnou doménu nebo subpopulaci</para>
            /// <para lang="en">Unclassified inferior area domain or subpopulation categories
            /// for the selected inferior area domain or subpopulation</para>
            /// </returns>
            public List<TCategory> GetUnClassifiedInferiorCategories(TDomain inferiorDomain)
            {
                List<TCategory> cats = GetInferiorCategories(
                    inferiorDomain: inferiorDomain);

                List<CategoryPair<TDomain, TCategory>> pairs = GetCategoryPairs(
                    inferiorDomain: inferiorDomain);

                return
                    cats
                        .Where(c => !pairs
                        .Select(p => p.Inferior.Id)
                        .Contains(value: c.Id))
                        .ToList<TCategory>();
            }

            /// <summary>
            /// <para lang="cs">Zařazené podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou plošnou doménu nebo subpopulaci</para>
            /// <para lang="en">Classified inferior area domain or subpopulation categories
            /// for the selected inferior area domain or subpopulation</para>
            /// </summary>
            /// <param name="inferiorDomain">
            /// <para lang="cs">Podřízená plošná doména nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation</para>
            /// </param>
            /// <param name="superiorCategory">
            /// <para lang="cs">Nadřazená kategorie plošné domény nebo subpopulace, do které jsou zařazeny</para>
            /// <para lang="en">Superior area domain or subpopulation category to which they are assigned</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Nezařazené podřízené kategorie plošné domény nebo subpopulace
            /// pro vybranou podřízenou plošnou doménu nebo subpopulaci</para>
            /// <para lang="en">Unclassified inferior area domain or subpopulation categories
            /// for the selected inferior area domain or subpopulation</para>
            /// </returns>
            public List<TCategory> GetClassifiedInferiorCategories(
                TDomain inferiorDomain,
                TCategory superiorCategory = default)
            {
                List<TCategory> cats =
                    GetInferiorCategories(
                        inferiorDomain: inferiorDomain);

                List<CategoryPair<TDomain, TCategory>> pairs =
                    GetCategoryPairs(
                        inferiorDomain: inferiorDomain);

                if (superiorCategory != null)
                {
                    return cats
                        .Where(c => pairs
                            .Where(p => p.Superior.Id == superiorCategory.Id)
                            .Select(p => p.Inferior.Id).Contains(value: c.Id))
                        .ToList<TCategory>();
                }
                else
                {
                    return cats
                        .Where(c => pairs
                        .Select(p => p.Inferior.Id).Contains(value: c.Id))
                        .ToList<TCategory>();
                }
            }

            /// <summary>
            /// <para lang="cs">Odstraní pár nadřazené a podřízené kategorie
            /// plošné domény nebo subpopulace ze seznamu</para>
            /// <para lang="en">It removes a pair of a superior and inferior
            /// area domain or subpopulation category from the list</para>
            /// </summary>
            /// <param name="superior">
            /// <para lang="cs">Nadřazená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Superior area domain or subpopulation category</para>
            /// </param>
            /// <param name="inferior">
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation category</para>
            /// </param>
            public void RemovePair(TCategory superior, TCategory inferior)
            {
                if (superior == null)
                {
                    return;
                }

                if (inferior == null)
                {
                    return;
                }

                CategoryPair<TDomain, TCategory> selectedItem =
                    CategoryPairs
                        .Where(a => a.Superior.Id == superior.Id)
                        .Where(a => a.Inferior.Id == inferior.Id)
                        .FirstOrDefault();

                if (selectedItem == null)
                {
                    // Není vybraný žádný pár pro odstranění
                    // No pair is selected to be removed
                    return;
                }

                CategoryPairs.Remove(item: selectedItem);
            }

            /// <summary>
            /// <para lang="cs">Přidá pár nadřazené a podřízené kategorie
            /// plošné domény nebo subpopulace do seznamu</para>
            /// <para lang="en">It adds a pair of a superior and inferior
            /// area domain or subpopulation category to the list</para>
            /// </summary>
            /// <param name="superior">
            /// <para lang="cs">Nadřazená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Superior area domain or subpopulation category</para>
            /// </param>
            /// <param name="inferior">
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation category</para>
            /// </param>
            public void AddPair(TCategory superior, TCategory inferior)
            {
                if (superior == null)
                {
                    return;
                }

                if (inferior == null)
                {
                    return;
                }

                CategoryPair<TDomain, TCategory> selectedItem =
                    CategoryPairs
                        .Where(a => a.Superior.Id == superior.Id)
                        .Where(a => a.Inferior.Id == inferior.Id)
                        .FirstOrDefault();

                if (selectedItem != null)
                {
                    // Vybraný pár již v seznamu existuje
                    // The selected pair already exists in the list
                    return;
                }

                CategoryPairs.Add(
                        item: new CategoryPair<TDomain, TCategory>(
                            superior: superior,
                            inferior: inferior));
            }

            /// <summary>
            /// <para lang="cs"> Odstraní podřízenou kategorii plošné domény nebo subpopulace
            /// a všechny páry v níchž je podřízená kategorie zařazena</para>
            /// <para lang="en">It removes inferior area domain or subpopulation category
            /// and all pairs that contains that inferior category</para>
            /// </summary>
            /// <param name="inferior">
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation category</para>
            /// </param>
            public void RemoveInferiorCategory(TCategory inferior)
            {
                if (inferior == null)
                {
                    return;
                }

                InferiorCategories.Remove(item: inferior);

                List<CategoryPair<TDomain, TCategory>> pairs
                    = CategoryPairs
                        .Where(a => a.Inferior.Id == inferior.Id)
                        .ToList<CategoryPair<TDomain, TCategory>>();

                foreach (CategoryPair<TDomain, TCategory> pair in pairs)
                {
                    CategoryPairs.Remove(item: pair);
                }
            }

            /// <summary>
            /// <para lang="cs"> Přidá podřízenou kategorii plošné domény nebo subpopulace</para>
            /// <para lang="en">It adds inferior area domain or subpopulation category </para>
            /// </summary>
            /// <param name="inferior">
            /// <para lang="cs">Podřízená kategorie plošné domény nebo subpopulace</para>
            /// <para lang="en">Inferior area domain or subpopulation category</para>
            /// </param>
            public void AddInferiorCategory(TCategory inferior)
            {
                if (inferior == null)
                {
                    return;
                }

                if (InferiorCategories.Contains(value: inferior))
                {
                    return;
                }

                InferiorCategories.Add(item: inferior);
            }

            /// <summary>
            /// <para lang="cs">Zápis párů nadřazené a podřízené kategorie
            /// plošné domény nebo subpopulace do databáze</para>
            /// <para lang="en">Writing of superior and inferior category pairs
            /// of area domain or subpopulation to the database</para>
            /// </summary>
            public void WriteToDatabase()
            {
                if (OriginalCategoryPairs.Count != 0)
                {
                    TDFunctions.FnDeleteHierarchy.Execute(
                        database: Database,
                        arealOrPopulation: ArealOrPopulation,
                        superiorCategoriesId: OriginalCategoryPairs
                                            .Select(a => (Nullable<int>)a.Superior.Id)
                                            .ToList<Nullable<int>>(),
                        inferiorCategoriesId: OriginalCategoryPairs
                                            .Select(a => (Nullable<int>)a.Inferior.Id)
                                            .ToList<Nullable<int>>());

                    if (Setting.Verbose)
                    {
                        System.Windows.Forms.MessageBox.Show(
                            text: TDFunctions.FnDeleteHierarchy.CommandText,
                            caption: TDFunctions.FnDeleteHierarchy.Name,
                            buttons: System.Windows.Forms.MessageBoxButtons.OK,
                            icon: System.Windows.Forms.MessageBoxIcon.Information);
                    }
                }

                if (CategoryPairs.Count != 0)
                {
                    TDFunctions.FnSaveHierarchy.Execute(
                        database: Database,
                        arealOrPopulationId: (int)ArealOrPopulation,
                        superiorCategoriesId: CategoryPairs
                                                .Select(a => (Nullable<int>)a.Superior.Id)
                                                .ToList<Nullable<int>>(),
                        inferiorCategoriesId: CategoryPairs
                                                .Select(a => (Nullable<int>)a.Inferior.Id)
                                                .ToList<Nullable<int>>());

                    if (Setting.Verbose)
                    {
                        System.Windows.Forms.MessageBox.Show(
                            text: TDFunctions.FnSaveHierarchy.CommandText,
                            caption: TDFunctions.FnSaveHierarchy.Name,
                            buttons: System.Windows.Forms.MessageBoxButtons.OK,
                            icon: System.Windows.Forms.MessageBoxIcon.Information);
                    }
                }

                Database.STargetData.TADCHierarchy.ReLoad(
                    condition: null);

                Database.STargetData.TSPCHierarchy.ReLoad(
                    condition: null);
            }

            /// <summary>
			/// <para lang="cs">Potvrdí provedené změny</para>
            /// <para lang="en">Confirm changes</para>
            /// </summary>
            public void ConfirmChanges()
            {
                originalCategoryPairs =
                    [.. CategoryPairs];
            }

            /// <summary>
			/// <para lang="cs">Zruší provedené změny</para>
            /// <para lang="en">Cancel changes</para>
            /// </summary>
            public void CancelChanges()
            {
                categoryPairs =
                    [.. OriginalCategoryPairs];
            }

            /// <summary>
            /// <para lang="cs">Seznam idenfikátorů popisující definované závislosti podřízených kategorií</para>
            /// <para lang="en">List of identifiers that describes defined dependencies of inferior categories</para>
            /// </summary>
            /// <param name="inferiorCategories">List of inferior categories</param>
            /// <returns>
            /// <para lang="cs">Seznam idenfikátorů popisující definované závislosti podřízených kategorií</para>
            /// <para lang="en">List of identifiers that describes defined dependencies of inferior categories</para>
            /// </returns>
            public List<List<Nullable<int>>> ToListOfListOfInt(List<TCategory> inferiorCategories)
            {
                if (inferiorCategories == null)
                {
                    return null;
                }

                if (inferiorCategories.Count == 0)
                {
                    return null;
                }

                List<List<Nullable<int>>> result = [];

                foreach (TCategory inferiorCategory in inferiorCategories)
                {
                    List<Nullable<int>> list = [.. CategoryPairs
                        .Where(a => a.Inferior.Id == inferiorCategory.Id)
                        .Select(a => (Nullable<int>)a.Superior.Id)
                        .OrderBy(a => a)];
                    result.Add(item: list);
                }

                // Doplnění null hodnotami na stejnou delku vnitřních polí
                // Completing with null values to the same length of internal arrays
                int maxLen = result.Max(a => a.Count);
                for (int i = 0; i < result.Count; i++)
                {
                    while (result[i].Count < maxLen)
                    {
                        result[i].Add(item: null);
                    }
                }

                return result;
            }

            #endregion Methods

        }

    }
}