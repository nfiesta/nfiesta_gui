﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba skupiny panelů a referenčního období"</para>
    /// <para lang="en">Control "Selection of panels and reference year sets"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTDSelectionPanelRefYearSetGroup
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        public bool onEdit;

        // Výsledkem výběru v této komponentě je vybraná skupina vhodných panelů a referenčního období,
        // pro kterou je možné vypočítat lokální hustoty
        // The result of the selection in this component is selected group of eligible panel and reference year set
        // where the calculation of local densities if possible

        /// <summary>
        /// <para lang="cs">Vhodné páry panelů a referenčního období</para>
        /// <para lang="en">Eligible panel and reference year set combinations</para>
        /// </summary>
        private TDFnGetEligiblePanelRefYearSetCombinationsTypeList eligiblePanelRefYearSetCombinations;

        /// <summary>
        /// <para lang="cs">Skupiny vhodných párů panelů a referenčního období</para>
        /// <para lang="en">Eligible panel and reference year set groups</para>
        /// </summary>
        private TDPanelRefYearSetGroupList eligiblePanelRefYearSetGroups;

        /// <summary>
        /// <para lang="cs">Vybraná skupina vhodných párů panelů a referenčního období</para>
        /// <para lang="en">Eligible panel and reference year set groups</para>
        /// </summary>
        private TDPanelRefYearSetGroup selectedEligiblePanelRefYearSetGroup;

        private string msgNone = String.Empty;
        private string msgNoneSelectedEligiblePanelRefYearSetGroup = String.Empty;
        private string msgNoEligiblePanelRefYearSetCombinations = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení párů panel a referenčního období</para>
        /// <para lang="en">Control for displaying panel and referenece year set pairs</para>
        /// </summary>
        private ControlPanelRefYearSetList ctrPanelRefYearSetList;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro zobrazení párů panel a referenčního období</para>
        /// <para lang="en">Control for displaying panel and referenece year set pairs</para>
        /// </summary>
        public ControlPanelRefYearSetList CtrPanelRefYearSetList
        {
            get
            {
                return ctrPanelRefYearSetList;
            }
            private set
            {
                ctrPanelRefYearSetList = value;
            }
        }

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">The "Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTDSelectionPanelRefYearSetGroup(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraná skupina objektů lokálních hustot (vybraná na 1.formuláři) (read-only)</para>
        /// <para lang="en">Selected group of local density objects (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectGroup SelectedLocalDensityObjectGroup
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionLDsityObjectGroup
                    .SelectedLocalDensityObjectGroup;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina cílových proměnných (vybraná na 2.formuláři) (read-only)</para>
        /// <para lang="en">Selected target variable group (selected on the 2nd form) (read-only)</para>
        /// </summary>
        public TDFnGetTargetVariableType SelectedTargetVariableGroup
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionTargetVariableCore
                    .SelectedTargetVariableGroup;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná (vybraná na 2. nebo 3. formuláři) (read-only)</para>
        /// <para lang="en">Selected target variable (selected on the 2nd or 3rd form) (read-only)</para>
        /// </summary>
        public TDTargetVariable SelectedTargetVariable
        {
            get
            {
                if (SelectedTargetVariableGroup == null)
                {
                    throw new ArgumentNullException(
                            message: $"{nameof(SelectedTargetVariableGroup)} is null.",
                            paramName: nameof(SelectedTargetVariableGroup));
                }

                return SelectedTargetVariableGroup.ArealOrPopulationValue switch
                {
                    TDArealOrPopulationEnum.AreaDomain =>
                        ((ControlTargetData)ControlOwner)
                            .CtrTDSelectionTargetVariableDivision
                            .SelectedTargetVariable,

                    TDArealOrPopulationEnum.Population =>
                        ((ControlTargetData)ControlOwner)
                            .CtrTDSelectionTargetVariableCore
                            .SelectedTargetVariable,

                    _ =>
                        throw new Exception(message: "Invalid category."),
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam identifikátorů categorization setup (read-only)</para>
        /// <para lang="en">List of categorization setup identifiers (read-only)</para>
        /// </summary>
        public List<Nullable<int>> CategorizationSetupIds
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionAttributeCategory
                    .CategorizationSetupIds;
            }
        }


        // Výsledkem výběru v této komponentě je vybraná skupina vhodných panelů a referenčního období,
        // pro kterou je možné vypočítat lokální hustoty
        // The result of the selection in this component is selected group of eligible panel and reference year set
        // where the calculation of local densities if possible

        /// <summary>
        /// <para lang="cs">Vhodné páry panelů a referenčního období</para>
        /// <para lang="en">Eligible panel and reference year set combinations</para>
        /// </summary>
        private TDFnGetEligiblePanelRefYearSetCombinationsTypeList EligiblePanelRefYearSetCombinations
        {
            get
            {
                return eligiblePanelRefYearSetCombinations ??
                    new TDFnGetEligiblePanelRefYearSetCombinationsTypeList(database: Database);
            }
            set
            {
                eligiblePanelRefYearSetCombinations = value ??
                    new TDFnGetEligiblePanelRefYearSetCombinationsTypeList(database: Database);
            }
        }

        /// <summary>
        /// <para lang="cs">Skupiny vhodných párů panelů a referenčního období</para>
        /// <para lang="en">Eligible panel and reference year set groups</para>
        /// </summary>
        private TDPanelRefYearSetGroupList EligiblePanelRefYearSetGroups
        {
            get
            {
                return eligiblePanelRefYearSetGroups ??
                    new TDPanelRefYearSetGroupList(database: Database);
            }
            set
            {
                eligiblePanelRefYearSetGroups = value ??
                    new TDPanelRefYearSetGroupList(database: Database);
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina vhodných panelů a referenčního období (read-only)</para>
        /// <para lang="en">Selected group of eligible panel and reference year set(read-only)</para>
        /// </summary>
        public TDPanelRefYearSetGroup SelectedEligiblePanelRefYearSetGroup
        {
            get
            {
                return selectedEligiblePanelRefYearSetGroup;
            }
            set
            {
                selectedEligiblePanelRefYearSetGroup = value;

                SetCaption();

                EnableButtonNext();

                SetPanelRefYearSetList();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPanelRefYearSetGroupEdit),                    "Editovat vybranou skupinu panelů a referenčního období" },
                        { nameof(btnPanelRefYearSetGroupAdd),                     "Vytvořit novou skupinu panelů a referenčního období" },
                        { nameof(btnPanelRefYearSetGroupDelete),                  "Smazat vybranou skupinu panelů a referenčního období" },
                        { nameof(btnPrevious),                                    "Předchozí" },
                        { nameof(btnCalculate),                                   "Výpočet" },
                        { nameof(grpPanelRefYearSetGroup),                        "Skupiny vhodných panelů a referenčního období:" },
                        { nameof(grpPanelRefYearSetList),                         "Seznam panelů s referenčním obdobím ve vybrané skupině:" },
                        { nameof(lblMainCaption),                                 "Volba skupiny panelů a referenčního období" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),            "Vybraná skupina objektů lokálních hustot:" },
                        { nameof(lblSelectedTargetVariableCaption),               "Vybraná cílová proměnná:" },
                        { nameof(lblSelectedPanelRefYearSetGroupCaption),         "Vybraná skupina panelů a ref. období:" },
                        { nameof(lstPanelRefYearSetGroup),                        "ExtendedLabelCs" },
                        { nameof(msgNone),                                        "žádná" },
                        { nameof(msgNoneSelectedEligiblePanelRefYearSetGroup),    "Žádná skupina vhodných panelů a referenčního období není vybraná."  },
                        { nameof(msgNoEligiblePanelRefYearSetCombinations),       "Žádné vhodné kombinace panelů a referenčních období nebyly nalezeny." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionPanelRefYearSetGroup),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPanelRefYearSetGroupEdit),                    "Edit the selected group of panels and reference year sets" },
                        { nameof(btnPanelRefYearSetGroupAdd),                     "Create a new group of panels and reference year sets" },
                        { nameof(btnPanelRefYearSetGroupDelete),                  "Delete the selected group of panels and reference year sets" },
                        { nameof(btnPrevious),                                    "Previous" },
                        { nameof(btnCalculate),                                   "Calculate" },
                        { nameof(grpPanelRefYearSetGroup),                        "Groups of eligible panels and reference year sets:" },
                        { nameof(grpPanelRefYearSetList),                         "List of panels with referenece year sets in selected group:" },
                        { nameof(lblMainCaption),                                 "Selection of the group of panels and reference year sets" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),            "Selected local density object group:" },
                        { nameof(lblSelectedTargetVariableCaption),               "Selected target variable:" },
                        { nameof(lblSelectedPanelRefYearSetGroupCaption),         "Selected group of panels and ref. year sets:" },
                        { nameof(lstPanelRefYearSetGroup),                        "ExtendedLabelEn" },
                        { nameof(msgNone),                                        "none" },
                        { nameof(msgNoneSelectedEligiblePanelRefYearSetGroup),    "Eligible group of panels and reference year sets is not selected." },
                        { nameof(msgNoEligiblePanelRefYearSetCombinations),       "No eligible combinations of panels and reference yearset combinations have been found." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionPanelRefYearSetGroup),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            pnlPanelRefYearSetList.Controls.Clear();
            ctrPanelRefYearSetList =
                new ControlPanelRefYearSetList(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    RefYearSetToPanelMappings = null,
                    IdentifierVisible = false,
                    StatusVisible = true
                };
            ctrPanelRefYearSetList.CreateControl();
            pnlPanelRefYearSetList.Controls.Add(value: ctrPanelRefYearSetList);

            eligiblePanelRefYearSetCombinations =
                new TDFnGetEligiblePanelRefYearSetCombinationsTypeList(
                    database: Database);

            eligiblePanelRefYearSetGroups =
                new TDPanelRefYearSetGroupList(
                    database: Database);

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            SelectedEligiblePanelRefYearSetGroup = null;

            lstPanelRefYearSetGroup.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SelectEligiblePanelRefYearSetGroup();
                    }
                });

            btnPanelRefYearSetGroupEdit.Click += new EventHandler(
                (sender, e) => { UpdatePanelRefYearSetGroup(); });

            btnPanelRefYearSetGroupAdd.Click += new EventHandler(
                (sender, e) => { InsertPanelRefYearSetGroup(); });

            btnPanelRefYearSetGroupDelete.Click += new EventHandler(
                (sender, e) => { DeletePanelRefYearSetGroup(); });

            btnPrevious.Click += new EventHandler(
                (sender, e) => { PreviousClick?.Invoke(sender: sender, e: e); });

            btnCalculate.Click += new EventHandler(
                (sender, e) =>
                {
                    ctrPanelRefYearSetList?.CalculateLocalDensities();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing the control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPanelRefYearSetGroupEdit.Text =
                labels.TryGetValue(key: nameof(btnPanelRefYearSetGroupEdit),
                    out string btnPanelRefYearSetGroupEditText)
                        ? btnPanelRefYearSetGroupEditText
                        : String.Empty;

            btnPanelRefYearSetGroupAdd.Text =
                labels.TryGetValue(key: nameof(btnPanelRefYearSetGroupAdd),
                    out string btnPanelRefYearSetGroupAddText)
                        ? btnPanelRefYearSetGroupAddText
                        : String.Empty;

            btnPanelRefYearSetGroupDelete.Text =
                labels.TryGetValue(key: nameof(btnPanelRefYearSetGroupDelete),
                    out string btnPanelRefYearSetGroupDeleteText)
                        ? btnPanelRefYearSetGroupDeleteText
                        : String.Empty;

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                    out string btnPreviousText)
                        ? btnPreviousText
                        : String.Empty;

            btnCalculate.Text =
                labels.TryGetValue(key: nameof(btnCalculate),
                    out string btnCalculateText)
                        ? btnCalculateText
                        : String.Empty;

            grpPanelRefYearSetGroup.Text =
                labels.TryGetValue(key: nameof(grpPanelRefYearSetGroup),
                    out string grpPanelRefYearSetGroupText)
                        ? grpPanelRefYearSetGroupText
                        : String.Empty;

            grpPanelRefYearSetList.Text =
                labels.TryGetValue(key: nameof(grpPanelRefYearSetList),
                    out string grpPanelRefYearSetListText)
                        ? grpPanelRefYearSetListText
                        : String.Empty;

            lblMainCaption.Text =
                labels.TryGetValue(key: nameof(lblMainCaption),
                    out string lblMainCaptionText)
                        ? lblMainCaptionText
                        : String.Empty;

            lblSelectedLDsityObjectGroupCaption.Text =
                labels.TryGetValue(key: nameof(lblSelectedLDsityObjectGroupCaption),
                    out string lblSelectedLDsityObjectGroupCaptionText)
                        ? lblSelectedLDsityObjectGroupCaptionText
                        : String.Empty;

            lblSelectedTargetVariableCaption.Text =
                labels.TryGetValue(key: nameof(lblSelectedTargetVariableCaption),
                    out string lblSelectedTargetVariableCaptionText)
                        ? lblSelectedTargetVariableCaptionText
                        : String.Empty;

            lblSelectedPanelRefYearSetGroupCaption.Text =
                labels.TryGetValue(key: nameof(lblSelectedPanelRefYearSetGroupCaption),
                    out string lblSelectedPanelRefYearSetGroupCaptionText)
                        ? lblSelectedPanelRefYearSetGroupCaptionText
                        : String.Empty;

            onEdit = true;
            lstPanelRefYearSetGroup.DisplayMember =
                labels.TryGetValue(key: nameof(lstPanelRefYearSetGroup),
                    out string lstPanelRefYearSetGroupDisplayMember)
                        ? lstPanelRefYearSetGroupDisplayMember
                        : ID;
            onEdit = false;

            msgNone =
                labels.TryGetValue(key: nameof(msgNone),
                        out msgNone)
                            ? msgNone
                            : String.Empty;

            msgNoneSelectedEligiblePanelRefYearSetGroup =
                labels.TryGetValue(key: nameof(msgNoneSelectedEligiblePanelRefYearSetGroup),
                        out msgNoneSelectedEligiblePanelRefYearSetGroup)
                            ? msgNoneSelectedEligiblePanelRefYearSetGroup
                            : String.Empty;

            msgNoEligiblePanelRefYearSetCombinations =
                labels.TryGetValue(key: nameof(msgNoEligiblePanelRefYearSetCombinations),
                        out msgNoEligiblePanelRefYearSetCombinations)
                            ? msgNoEligiblePanelRefYearSetCombinations
                            : String.Empty;

            ctrPanelRefYearSetList?.InitializeLabels();

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis ovládacího prvku</para>
        /// <para lang="en">Sets the control caption</para>
        /// </summary>
        private void SetCaption()
        {
            string selectedLocalDensityObjectGroupId;
            string selectedLocalDensityObjectGroupCaption;
            string selectedTargetVariableGroupId;
            string selectedTargetVariableGroupCaption;
            string selectedEligiblePanelRefYearSetGroupId;
            string selectedEligiblePanelRefYearSetGroupCaption;

            if (SelectedLocalDensityObjectGroup != null)
            {
                selectedLocalDensityObjectGroupId = SelectedLocalDensityObjectGroup.Id.ToString();
                selectedLocalDensityObjectGroupCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedLocalDensityObjectGroup.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedLocalDensityObjectGroup.LabelEn :
                    String.Empty;
                lblSelectedLDsityObjectGroupValue.Text = String.Concat(
                    $"({selectedLocalDensityObjectGroupId}) ",
                    $"{selectedLocalDensityObjectGroupCaption}");
            }
            else
            {
                lblSelectedLDsityObjectGroupValue.Text = msgNone;
            }

            if (SelectedTargetVariableGroup != null)
            {
                selectedTargetVariableGroupId = SelectedTargetVariableGroup.TargetVariableIds;
                selectedTargetVariableGroupCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedTargetVariableGroup.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedTargetVariableGroup.LabelEn :
                    String.Empty;

                lblSelectedTargetVariableValue.Text = String.Concat(
                    $"({selectedTargetVariableGroupId}) ",
                    $"{selectedTargetVariableGroupCaption}");
            }
            else
            {
                lblSelectedTargetVariableValue.Text = msgNone;
            }

            if (SelectedEligiblePanelRefYearSetGroup != null)
            {
                selectedEligiblePanelRefYearSetGroupId = SelectedEligiblePanelRefYearSetGroup.Id.ToString();
                selectedEligiblePanelRefYearSetGroupCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedEligiblePanelRefYearSetGroup.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedEligiblePanelRefYearSetGroup.LabelEn :
                    String.Empty;

                lblSelectedPanelRefYearSetGroupValue.Text = String.Concat(
                    $"({selectedEligiblePanelRefYearSetGroupId}) ",
                    $"{selectedEligiblePanelRefYearSetGroupCaption}");
            }
            else
            {
                lblSelectedPanelRefYearSetGroupValue.Text = msgNone;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost tlačítka "Spočítat lokální hustoty"</para>
        /// <para lang="en">Enable or Disable button "Calculate the local densities"</para>
        /// </summary>
        private void EnableButtonNext()
        {
            btnCalculate.Enabled =
                (SelectedEligiblePanelRefYearSetGroup != null);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Uploading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Database.STargetData.CPanelRefYearSetGroup =
                TDFunctions.FnGetPanelRefYearSetGroup.Execute(
                    database: Database,
                    panelRefYearSetGroupId: null);

            eligiblePanelRefYearSetCombinations =
                TDFunctions.FnGetEligiblePanelRefYearSetCombinationsA.Execute(
                    database: Database,
                    targetVariableId: SelectedTargetVariable.Id,
                    categorizationSetupIds: CategorizationSetupIds);

            eligiblePanelRefYearSetGroups =
                 TDFunctions.FnGetEligiblePanelRefYearSetGroups.Execute(
                    database: Database,
                    eligibleRefYearSetToPanelMappingIds:
                        eligiblePanelRefYearSetCombinations
                            .Items
                            .Select(a => (Nullable<int>)a.Id)
                            .ToList());

            InitializeListBoxPanelRefYearSetGroup();
        }

        /// <summary>
        /// <para lang="cs">Do ListBoxu vyplní seznam skupin vhodných párů panelů a referenčního období</para>
        /// <para lang="en">Fills the ListBox with a list of eligible panel and reference year set groups</para>
        /// </summary>
        private void InitializeListBoxPanelRefYearSetGroup()
        {
            onEdit = true;
            lstPanelRefYearSetGroup.DataSource = EligiblePanelRefYearSetGroups.Items;
            lstPanelRefYearSetGroup.DisplayMember =
                Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                    .TryGetValue(key: nameof(lstPanelRefYearSetGroup),
                        out string lstPanelRefYearSetGroupDisplayMember)
                            ? lstPanelRefYearSetGroupDisplayMember
                            : ID;
            onEdit = false;

            SelectEligiblePanelRefYearSetGroup();
        }

        /// <summary>
        /// <para lang="cs">Spouští se v případě, že byla v ListBoxu EligiblePanelRefYearSetGroups
        /// vybrána skupina vhodných párů panelů a referenčního období</para>
        /// <para lang="en">Fires if a group of eligible panel and reference year set
        /// was selected in the EligiblePanelRefYearSetGroups ListBox</para>
        /// </summary>
        private void SelectEligiblePanelRefYearSetGroup()
        {
            if (lstPanelRefYearSetGroup.SelectedItem == null)
            {
                SelectedEligiblePanelRefYearSetGroup = null;
                return;
            }

            SelectedEligiblePanelRefYearSetGroup =
                (TDPanelRefYearSetGroup)lstPanelRefYearSetGroup.SelectedItem;
        }

        /// <summary>
        /// <para lang="cs">Výpis panelů a referenčního období ve skupině</para>
        /// <para lang="en">List of panels and reference year sets</para>
        /// </summary>
        private void SetPanelRefYearSetList()
        {
            if (SelectedEligiblePanelRefYearSetGroup == null)
            {
                // Není vybraná skupina panelů a referenčního období
                // There is no group of panels and reference year set group selected
                ctrPanelRefYearSetList.RefYearSetToPanelMappings = null;
                return;
            }

            ctrPanelRefYearSetList.RefYearSetToPanelMappings =
                TDFunctions.FnGetRefYearSetToPanelMappingForGroup.Execute(
                    database: Database,
                    panelRefYearSetGroupId: SelectedEligiblePanelRefYearSetGroup.Id);

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnGetRefYearSetToPanelMappingForGroup.CommandText,
                    caption: TDFunctions.FnGetRefYearSetToPanelMappingForGroup.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Provede výběr EligiblePanelRefYearSetGroups v ListBoxu</para>
        /// <para lang="en">It selects item in ListBox EligiblePanelRefYearSetGroups</para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikátor skupiny panelů a referenčního období</para>
        /// <para lang="en">Panel and reference year set group identifier</para>
        /// </param>
        private void SelectEligiblePanelRefYearSetGroup(int id)
        {
            // Výběr editované položky po aktualizaci databáze
            // Selecting an edited item after updating the database
            if (lstPanelRefYearSetGroup.Items
                    .OfType<TDPanelRefYearSetGroup>()
                    .Where(a => a.Id == id)
                    .Any())
            {
                lstPanelRefYearSetGroup.SelectedItem =
                    lstPanelRefYearSetGroup.Items
                    .OfType<TDPanelRefYearSetGroup>()
                    .Where(a => a.Id == id)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// <para lang="cs">Zapíše změnu popisků skupiny vhodných párů panelů a referenčního období</para>
        /// <para lang="en">Writes a change to the labels of
        /// a group of eligible panel and reference year set groups</para>
        /// </summary>
        private void UpdatePanelRefYearSetGroup()
        {
            if (SelectedEligiblePanelRefYearSetGroup == null)
            {
                // SelectedLocalDensityObjectGroup je null
                // SelectedLocalDensityObjectGroup is null
                return;
            }

            FormLookupTable frmLookupTable = new(
                    controlOwner: this,
                    lookupTable: Database.STargetData.CPanelRefYearSetGroup,
                    displayedItemsIds: [SelectedEligiblePanelRefYearSetGroup.Id]);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                int id = SelectedEligiblePanelRefYearSetGroup.Id;
                LoadContent();
                SelectEligiblePanelRefYearSetGroup(id);
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové skupiny párů panelů a referenčního období</para>
        /// <para lang="en">Create a new group of panels and reference year sets</para>
        /// </summary>
        private void InsertPanelRefYearSetGroup()
        {
            FormPanelRefYearSetGroupNew frm =
                new(
                    controlOwner: this);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoření nové skupiny párů panelů a referenčního období pokud žádná neexistuje</para>
        /// <para lang="en">Create a new group of panels and reference year sets if none exists</para>
        /// </summary>
        public void InsertPanelRefYearSetGroupIfNoneExists()
        {
            if (EligiblePanelRefYearSetCombinations.Items.Count == 0)
            {
                MessageBox.Show(
                    caption: String.Empty,
                    text: msgNoEligiblePanelRefYearSetCombinations,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (EligiblePanelRefYearSetGroups.Items.Count == 0)
            {
                InsertPanelRefYearSetGroup();
                return;
            }

            return;
        }

        /// <summary>
        /// <para lang="cs">Smazání existující skupiny párů panelů a referenčního období</para>
        /// <para lang="en">Delete an existing group of panels and reference year sets</para>
        /// </summary>
        private void DeletePanelRefYearSetGroup()
        {
            if (SelectedEligiblePanelRefYearSetGroup == null)
            {
                // Žádná skupina panelů a referenčního období není vybraná
                // No group of panels and reference year sets is selected
                MessageBox.Show(
                    text: msgNoneSelectedEligiblePanelRefYearSetGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (!Database.STargetData.CPanelRefYearSetGroup.Items
                .Where(a => a.Id == SelectedEligiblePanelRefYearSetGroup.Id).Any())
            {
                // Vybraná skupina panelů a referenčního období není v databázové tabulce (nemůže nastat)
                // The selected group of panels and reference year set is not in the database table (cannot occur)
                MessageBox.Show(
                    text: msgNoneSelectedEligiblePanelRefYearSetGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            TDFunctions.FnDeletePyrGroup.Execute(
                database: Database,
                panelRefYearSetGroupId: SelectedEligiblePanelRefYearSetGroup.Id);

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnDeletePyrGroup.CommandText,
                    caption: TDFunctions.FnDeletePyrGroup.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            LoadContent();
        }

        #endregion Methods

    }

}