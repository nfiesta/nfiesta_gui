﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek - položka seznamu plošných domén nebo subpopulací - třída pro Designer</para>
    /// <para lang="en">Control - a list item for area domains or subpopulations - class for Designer</para>
    /// </summary>
    internal partial class ControlTDDomainListBoxItemDesign
        : UserControl
    {
        protected ControlTDDomainListBoxItemDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Button BtnCollapse => btnCollapse;
        protected System.Windows.Forms.Button BtnExpand => btnExpand;
        protected System.Windows.Forms.Panel PnlCollapsed => pnlCollapsed;
        protected System.Windows.Forms.Panel PnlCollapsedWorkSpace => pnlCollapsedWorkSpace;
        protected System.Windows.Forms.Panel PnlExpanded => pnlExpanded;
        protected System.Windows.Forms.Panel PnlExpandedWorkSpace => pnlExpandedWorkSpace;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek - položka seznamu plošných domén nebo subpopulací</para>
    /// <para lang="en">Control - a list item for area domains or subpopulations</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlTDDomainListBoxItem<TDomain, TCategory>
            : ControlTDDomainListBoxItemDesign, INfiEstaControl, ITargetDataControl
             where TDomain : IArealOrPopulationDomain
             where TCategory : IArealOrPopulationCategory
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Minimální výška ovládacího prvku</para>
        /// <para lang="en">Minimum height of the control</para>
        /// </summary>
        private const int CollapsedHeight = 25;

        /// <summary>
        /// <para lang="cs">Zvětšení výšky ovládacího prvku na jeden ldsityObject</para>
        /// <para lang="en">Height increment of the control per one ldsityObject</para>
        /// </summary>
        private int HeightIncrement = 25;

        /// <summary>
        /// <para lang="cs">Výška RadioButton</para>
        /// <para lang="en">Height of the RadioButton</para>
        /// </summary>
        private const int RadioButtonHeight = 25;

        /// <summary>
        /// <para lang="cs">Odsazení RadioButton od levého okraje panelu</para>
        /// <para lang="en">RadioButton offset from the left edge of the panel</para>
        /// </summary>
        private const int RadioButtonOffset = 5;

        /// <summary>
        /// <para lang="cs">Předpokládaná šířka scrollbaru v případě, že se RadioButtons v seznamu nezobrazují všechny</para>
        /// <para lang="en">Estimated width of the scrollbar in case RadioButtons are not all displayed in the list</para>
        /// </summary>
        private const int ScrollBarWidth = 20;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Index položky ze seznamu</para>
        /// <para lang="en">Index of a list item</para>
        /// </summary>
        private int index;

        /// <summary>
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </summary>
        private TDomain domain;

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot</para>
        /// <para lang="en">Local density objects</para>
        /// </summary>
        private Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            ldsityObjects;

        /// <summary>
        /// <para lang="cs">
        /// Slovník -
        /// klíč: Identifikátor objektu lokální hustoty,
        /// hodnota: Seznam objektů lokální hustoty pro třídění</para>
        /// <para lang="en">
        /// Dictionary -
        /// key: Local density object identifier,
        /// value: List of local density objects for adsp</para>
        /// </summary>
        private Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDLDsityObjectList>
            dictLDsityObjects;

        /// <summary>
        /// <para lang="cs">
        /// Výsledek (slovník) -
        /// klíč: Identifikátor objektu lokální hustoty,
        /// hodnota: Vybraný objekt lokální hustoty pro třídění</para>
        /// <para lang="en">
        /// Result (dictionary) -
        /// key: Local density object identifier,
        /// value: Selected local density object for adsp</para>
        /// </summary>
        private Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDLDsityObject>
            dictResult;

        /// <summary>
        /// <para lang="cs">Je položka vybraná</para>
        /// <para lang="en">Is an item selected</para>
        /// </summary>
        private bool selected;

        /// <summary>
        /// <para lang="cs">Stručné zobrazení ovládacího prvku</para>
        /// <para lang="en">Brief display of the control</para>
        /// </summary>
        private bool collapsed;

        private string msgEmptyLDsityObjectsForASPD = String.Empty;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost změna vybrané položky v seznamu</para>
        /// <para lang="en">Event of the selected item change in the list</para>
        /// </summary>
        public event EventHandler SelectedIndexChanged;

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna výběru objektu lokální hustoty pro třídění v kategorii plošné domény nebo subpopulace"</para>
        /// <para lang="en">Event
        /// "Change of the selection of the local density object for classification in area domain or subpopulation category"</para>
        /// </summary>
        public event EventHandler ResultChanged;

        /// <summary>
        /// <para lang="cs">Událost změna zobrazení ovládacího prvku</para>
        /// <para lang="en">Event of the control display change</para>
        /// </summary>
        public event EventHandler ExtendedOrCollapsed;

        #endregion Events


        #region Controls

        private System.Windows.Forms.Panel pnlDomainCollapsed;
        private System.Windows.Forms.Label lblDomainCollapsed;

        private System.Windows.Forms.Panel pnlDomain;
        private System.Windows.Forms.Label lblDomain;

        private System.Windows.Forms.Panel pnlLDsityObjects;

        private System.Windows.Forms.Panel pnlLDsityObjectsForADSP;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="index">
        /// <para lang="cs">Index položky v seznamu</para>
        /// <para lang="en">Index of a list item</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </param>
        /// <param name="ldsityObjects">
        /// <para lang="cs">Objekty lokálních hustot</para>
        /// <para lang="en">Local density objects</para>
        /// </param>
        public ControlTDDomainListBoxItem(
            Control controlOwner,
            int index,
            TDomain domain,
            Dictionary<
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject> ldsityObjects)
            : base()
        {
            Initialize(
                controlOwner: controlOwner,
                index: index,
                domain: domain,
                ldsityObjects: ldsityObjects);

            BtnCollapse.Click += new EventHandler(
               (sender, e) => { Collapse(); });

            BtnExpand.Click += new EventHandler(
               (sender, e) => { Expand(); });

            PnlExpandedWorkSpace.Resize += new EventHandler(
                (sender, e) => { ResizePanels(); });
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDDomainListBox<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDDomainListBox<TDomain, TCategory>)}<TDomain,TCategory>."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDDomainListBox<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDDomainListBox<TDomain, TCategory>)}<TDomain,TCategory>."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Index položky ze seznamu</para>
        /// <para lang="en">Index of a list item</para>
        /// </summary>
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </summary>
        public TDomain Domain
        {
            get
            {
                if (domain == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Domain)} must not be null.",
                        paramName: nameof(Domain));
                }

                return domain;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Domain)} must not be null.",
                        paramName: nameof(Domain));
                }

                domain = value;

                if (lblDomainCollapsed != null)
                {
                    lblDomainCollapsed.Text =
                        (LanguageVersion == LanguageVersion.National) ? domain.ExtendedLabelCs :
                        (LanguageVersion == LanguageVersion.International) ? domain.ExtendedLabelEn :
                        domain.Id.ToString();
                }

                if (lblDomain != null)
                {
                    lblDomain.Text =
                        (LanguageVersion == LanguageVersion.National) ? domain.ExtendedLabelCs :
                        (LanguageVersion == LanguageVersion.International) ? domain.ExtendedLabelEn :
                        domain.Id.ToString();
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Slovník -
        /// klíč: Identifikátor objektu lokální hustoty,
        /// hodnota: Seznam objektů lokální hustoty pro třídění</para>
        /// <para lang="en">
        /// Dictionary -
        /// key: Local density object identifier,
        /// value: List of local density objects for adsp</para>
        /// </summary>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDLDsityObjectList>
            DictLDsityObjects
        {
            get
            {
                return dictLDsityObjects
                    .OrderBy(a => a.Key)
                    .ToDictionary(a => a.Key, a => a.Value);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Výsledek (slovník) -
        /// klíč: Identifikátor objektu lokální hustoty,
        /// hodnota: Vybraný objekt lokální hustoty pro třídění</para>
        /// <para lang="en">
        /// Result (dictionary) -
        /// key: Local density object identifier,
        /// value: Selected local density object for adsp</para>
        /// </summary>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDLDsityObject>
            DictResult
        {
            get
            {
                return dictResult
                    .OrderBy(a => a.Key)
                    .ToDictionary(a => a.Key, a => a.Value);
            }
        }

        /// <summary>
        /// <para lang="cs">Je ovládací prvek vybraný</para>
        /// <para lang="en">Is a control selected</para>
        /// </summary>
        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
                HighlightIfSelected();
            }
        }

        /// <summary>
        /// <para lang="cs">Stručné zobrazení ovládacího prvku</para>
        /// <para lang="en">Brief display of the control</para>
        /// </summary>
        public bool Collapsed
        {
            get
            {
                return collapsed;
            }
            set
            {
                if (value)
                {
                    Collapse();
                }
                else
                {
                    Expand();
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(msgEmptyLDsityObjectsForASPD),
                                String.Concat(
                                    $"Uložená procedura $1 ",
                                    $"nevrátila žádné objekty lokálních hustot pro třídění.")
                            }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBoxItem<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(msgEmptyLDsityObjectsForASPD),
                                String.Concat(
                                    $"Uložená procedura $1 ",
                                    $"nevrátila žádné objekty lokálních hustot pro třídění.")
                            }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBoxItem<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    _ => [],
                },

                LanguageVersion.International =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(msgEmptyLDsityObjectsForASPD),
                                String.Concat(
                                    $"Stored procedure $1 ",
                                    $"returns no local density objects for classification.")
                            }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBoxItem<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(msgEmptyLDsityObjectsForASPD),
                                String.Concat(
                                    $"Stored procedure $1 ",
                                    $"returns no local density objects for classification.")
                            }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBoxItem<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="index">
        /// <para lang="cs">Index položky v seznamu</para>
        /// <para lang="en">Index of a list item</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </param>
        /// <param name="ldsityObjects">
        /// <para lang="cs">Objekty lokálních hustot</para>
        /// <para lang="en">Local density objects</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            int index,
            TDomain domain,
            Dictionary<
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject> ldsityObjects)
        {
            ControlOwner = controlOwner;
            Index = index;
            Domain = domain;

            dictLDsityObjects = [];

            dictResult = [];

            if (ldsityObjects != null)
            {
                this.ldsityObjects = ldsityObjects;

                foreach (
                    KeyValuePair<
                        TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                        TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
                        pair in ldsityObjects)
                {
                    if (!dictLDsityObjects.ContainsKey(key: pair.Key))
                    {
                        if (!dictResult.ContainsKey(key: pair.Key))
                        {
                            dictLDsityObjects.Add(
                                key: pair.Key,
                                value: null);
                            dictResult.Add(
                                key: pair.Key,
                                value: null);
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentException(
                    message: $"Argument {nameof(ldsityObjects)} must not be null.",
                    paramName: nameof(ldsityObjects));
            }

            selected = false;

            collapsed = true;

            InitializeLabels();

            LoadContent();

            HighlightIfSelected();

            InitializeControls();

            // Po vytvoření ovládacích prvků v metodě InitializeContent je nutné znovu zavolat
            // InitializeLabels, aby se nastavily jejich popisky
            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace obsahu ovládacího prvku</para>
        /// <para lang="en">Initializing content of the control</para>
        /// </summary>
        private void InitializeControls()
        {
            // Inicializace grafických ovládacích prvků:
            // Initializing graphic controls:

            PnlCollapsed.Dock = DockStyle.Fill;
            PnlExpanded.Dock = DockStyle.Fill;
            PnlCollapsedWorkSpace.Dock = DockStyle.Fill;
            PnlExpandedWorkSpace.Dock = DockStyle.Fill;

            PnlCollapsedWorkSpace.Controls.Clear();
            PnlExpandedWorkSpace.Controls.Clear();

            Collapse();

            // pnlDomainCollapsed
            pnlDomainCollapsed = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Height = PnlCollapsedWorkSpace.Height,
                Left = 0,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Tag = domain,
                Top = 0,
                Width = PnlCollapsedWorkSpace.Width
            };
            pnlDomainCollapsed.CreateControl();
            PnlCollapsedWorkSpace.Controls.Add(value: pnlDomainCollapsed);

            // pnlDomain
            pnlDomain = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.None,
                Height = PnlExpandedWorkSpace.Height,
                Left = 0,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Tag = domain,
                Top = 0,
                Width = 200,
            };
            pnlDomain.CreateControl();
            PnlExpandedWorkSpace.Controls.Add(value: pnlDomain);

            // pnlLDsityObjects
            pnlLDsityObjects = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.None,
                Height = PnlExpandedWorkSpace.Height,
                Left = pnlDomain.Width,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Tag = null,
                Top = 0,
                Width = 200
            };
            pnlLDsityObjects.CreateControl();
            PnlExpandedWorkSpace.Controls.Add(value: pnlLDsityObjects);

            // pnlLDsityObjectsForADSP
            pnlLDsityObjectsForADSP = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.None,
                Height = PnlExpandedWorkSpace.Height,
                Left = (pnlDomain.Width + pnlLDsityObjects.Width),
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Tag = null,
                Top = 0,
                Width = PnlExpandedWorkSpace.Width - (pnlDomain.Width + pnlLDsityObjects.Width)
            };
            pnlLDsityObjectsForADSP.CreateControl();
            PnlExpandedWorkSpace.Controls.Add(value: pnlLDsityObjectsForADSP);

            // lblDomainCollapsed
            lblDomainCollapsed = new System.Windows.Forms.Label()
            {
                AutoSize = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Tag = domain,
                Text = String.Empty,
                TextAlign = ContentAlignment.TopLeft
            };
            lblDomainCollapsed.Click += new EventHandler(
                (sender, e) => { SelectedIndexChanged?.Invoke(sender: this, e: e); });
            lblDomainCollapsed.CreateControl();
            pnlDomainCollapsed.Controls.Add(value: lblDomainCollapsed);

            // lblDomain
            lblDomain = new System.Windows.Forms.Label()
            {
                AutoSize = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Tag = domain,
                Text = String.Empty,
                TextAlign = ContentAlignment.TopLeft
            };
            lblDomain.Click += new EventHandler(
                (sender, e) => { SelectedIndexChanged?.Invoke(sender: this, e: e); });
            lblDomain.CreateControl();
            pnlDomain.Controls.Add(value: lblDomain);

            // pnlLDsityObjects - Controls
            int i = -1;
            foreach (KeyValuePair<
                        TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                        TDLDsityObjectList>
                        pair in DictLDsityObjects)
            {
                System.Windows.Forms.Panel panel = new()
                {
                    AutoScroll = false,
                    BorderStyle = BorderStyle.None,
                    Dock = DockStyle.None,
                    Height = HeightIncrement,
                    Left = 0,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    Tag = pair.Key,
                    Top = HeightIncrement * (++i),
                    Width = pnlLDsityObjects.Width
                };
                panel.CreateControl();
                pnlLDsityObjects.Controls.Add(value: panel);

                System.Windows.Forms.Label label = new()
                {
                    AutoSize = false,
                    BorderStyle = BorderStyle.None,
                    Dock = DockStyle.Fill,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    Tag = pair.Key,
                    Text = String.Empty,
                    TextAlign = ContentAlignment.TopLeft
                };
                label.Click += new EventHandler(
                    (sender, e) => { SelectedIndexChanged?.Invoke(sender: this, e: e); });
                label.CreateControl();
                panel.Controls.Add(value: label);
            }

            // pnlLDsityObjectsForADSP - Controls
            i = -1;
            foreach (KeyValuePair<
                        TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                        TDLDsityObjectList>
                        pair in DictLDsityObjects)
            {
                TDLDsityObjectList ldsityObjectsForADSP = pair.Value;

                System.Windows.Forms.Panel panel = new()
                {
                    AutoScroll = true,
                    BorderStyle = BorderStyle.None,
                    Dock = DockStyle.None,
                    Height = HeightIncrement,
                    Left = 0,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    Tag = pair.Key,
                    Top = HeightIncrement * (++i),
                    Width = pnlLDsityObjectsForADSP.Width
                };
                panel.MouseUp +=
                    (sender, e) =>
                    {
                        foreach (Control control in panel.Controls)
                        {
                            if (control is RadioButton radioButton)
                            {
                                if (!radioButton.Enabled)
                                {
                                    if (radioButton.Bounds.Contains(pt: e.Location))
                                    {
                                        switch (e.Button)
                                        {
                                            case MouseButtons.Left:
                                                break;

                                            case MouseButtons.Right:
                                                ShowFormClassificationRule(
                                                    radioButton: radioButton,
                                                    readOnlyMode: false);
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    };
                panel.CreateControl();
                pnlLDsityObjectsForADSP.Controls.Add(value: panel);

                int j = -1;
                foreach (TDLDsityObject ldsityObjectForADSP in ldsityObjectsForADSP.Items)
                {
                    RadioButton radioButton = new()
                    {
                        Enabled = ldsityObjectForADSP.ClassificationRule ?? false,
                        Height = RadioButtonHeight,
                        Left = RadioButtonOffset,
                        Tag = new List<object>() { pair.Key, ldsityObjectForADSP },
                        Text = String.Empty,
                        Top = (++j) * RadioButtonHeight,
                        Width = pnlLDsityObjectsForADSP.Width - RadioButtonOffset - ScrollBarWidth,
                    };
                    radioButton.MouseUp +=
                        (sender, e) =>
                        {
                            if (radioButton.Enabled)
                            {
                                switch (e.Button)
                                {
                                    case MouseButtons.Left:
                                        break;
                                    case MouseButtons.Right:
                                        ShowFormClassificationRule(
                                            radioButton: radioButton,
                                            readOnlyMode: true);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        };
                    radioButton.CheckedChanged +=
                        (sender, e) =>
                        {
                            RadioButton rdoSender = (RadioButton)sender;
                            List<object> tag = (List<object>)rdoSender.Tag;
                            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier key =
                                (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier)tag[0];
                            TDLDsityObject val = (TDLDsityObject)tag[1];
                            if (rdoSender.Checked)
                            {
                                dictResult[key] = val;
                                ResultChanged?.Invoke(sender: this, e: new EventArgs());
                            }
                        };
                    radioButton.CreateControl();
                    panel.Controls.Add(value: radioButton);
                }
            }

            // Vybere první RadioButton v každém panelu jako defaultní
            // Select first enabled RadioButton in each Panel as default
            foreach (System.Windows.Forms.Panel panel in pnlLDsityObjectsForADSP.Controls)
            {
                foreach (Control control in panel.Controls)
                {
                    if (control is RadioButton radioButton)
                    {
                        if (radioButton.Enabled)
                        {
                            radioButton.Checked = true;
                            break;
                        }
                    }
                }
            }

            ResizePanels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing user control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

            msgEmptyLDsityObjectsForASPD =
              messages.TryGetValue(key: nameof(msgEmptyLDsityObjectsForASPD),
                      out msgEmptyLDsityObjectsForASPD)
                          ? msgEmptyLDsityObjectsForASPD
                          : String.Empty;

            switch (LanguageVersion)
            {
                case LanguageVersion.National:

                    if (lblDomainCollapsed != null)
                    {
                        lblDomainCollapsed.Text = domain.ExtendedLabelCs;
                    }

                    if (lblDomain != null)
                    {
                        lblDomain.Text = domain.ExtendedLabelCs;
                    }

                    if (pnlLDsityObjects != null)
                    {
                        foreach (System.Windows.Forms.Panel panel in pnlLDsityObjects.Controls)
                        {
                            foreach (Label label in panel.Controls)
                            {
                                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier identifier =
                                    (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier)label.Tag;

                                label.Text = String.Concat(
                                    $"{identifier.LDsityObject.ExtendedLabelCs} ",
                                    identifier.UseNegative ? "(-)" : "(+)");
                            }
                        }
                    }

                    if (pnlLDsityObjectsForADSP != null)
                    {
                        foreach (System.Windows.Forms.Panel panel in pnlLDsityObjectsForADSP.Controls)
                        {
                            foreach (Control control in panel.Controls)
                            {
                                if (control is RadioButton radioButton)
                                {
                                    List<object> tag = (List<object>)radioButton.Tag;
                                    TDLDsityObject ldsityObjectForADSP = (TDLDsityObject)tag[1];
                                    radioButton.Text = ldsityObjectForADSP.ExtendedLabelCs;
                                }
                            }
                        }
                    }

                    break;

                case LanguageVersion.International:

                    if (lblDomainCollapsed != null)
                    {
                        lblDomainCollapsed.Text = domain.ExtendedLabelEn;
                    }

                    if (lblDomain != null)
                    {
                        lblDomain.Text = domain.ExtendedLabelEn;
                    }

                    if (pnlLDsityObjects != null)
                    {
                        foreach (System.Windows.Forms.Panel panel in pnlLDsityObjects.Controls)
                        {
                            foreach (Label label in panel.Controls)
                            {
                                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier identifier =
                                   (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier)label.Tag;

                                label.Text = String.Concat(
                                    $"{identifier.LDsityObject.ExtendedLabelEn} ",
                                    identifier.UseNegative ? "(-)" : "(+)");
                            }
                        }
                    }

                    if (pnlLDsityObjectsForADSP != null)
                    {
                        foreach (System.Windows.Forms.Panel panel in pnlLDsityObjectsForADSP.Controls)
                        {
                            foreach (RadioButton radioButton in panel.Controls)
                            {
                                List<object> tag = (List<object>)radioButton.Tag;
                                TDLDsityObject ldsityObjectForADSP = (TDLDsityObject)tag[1];
                                radioButton.Text = ldsityObjectForADSP.ExtendedLabelEn;
                            }
                        }
                    }

                    break;

                default:

                    if (lblDomainCollapsed != null)
                    {
                        lblDomainCollapsed.Text = domain.Id.ToString();
                    }

                    if (lblDomain != null)
                    {
                        lblDomain.Text = domain.Id.ToString();
                    }

                    if (pnlLDsityObjects != null)
                    {
                        foreach (System.Windows.Forms.Panel panel in pnlLDsityObjects.Controls)
                        {
                            foreach (Label label in panel.Controls)
                            {
                                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier identifier =
                                   (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier)label.Tag;

                                label.Text = identifier.ToString();
                            }
                        }
                    }

                    if (pnlLDsityObjectsForADSP != null)
                    {
                        foreach (System.Windows.Forms.Panel panel in pnlLDsityObjectsForADSP.Controls)
                        {
                            foreach (RadioButton radioButton in panel.Controls)
                            {
                                List<object> tag = (List<object>)radioButton.Tag;
                                TDLDsityObject ldsityObjectForADSP = (TDLDsityObject)tag[1];
                                radioButton.Text = ldsityObjectForADSP.Id.ToString();
                            }
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí formulář pro definici klasifikačních pravidel</para>
        /// <para lang="en">It shows form for classification rule definition</para>
        /// </summary>
        /// <param name="radioButton">
        /// <para lang="cs">radioButton</para>
        /// <para lang="en">radioButton</para>
        /// </param>
        /// <param name="readOnlyMode">
        /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
        /// <para lang="en">Changes in classification rules are not allowed</para>
        /// </param>
        private void ShowFormClassificationRule(RadioButton radioButton, bool readOnlyMode)
        {
            List<object> tag = (List<object>)radioButton.Tag;
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier key =
                (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier)tag[0];
            TDLDsityObject ldsityObjectForADSP = (TDLDsityObject)tag[1];

            switch (ArealOrPopulation)
            {
                case TDArealOrPopulationEnum.AreaDomain:
                    FormClassificationRule<TDomain, TDAreaDomainCategory> frmAreal =
                        new(
                            controlOwner: ((ControlTDDomainListBox<TDomain, TCategory>)ControlOwner).ControlOwner,
                            domain: domain,
                            ldsityObject: key.LDsityObject,
                            ldsityObjectForADSP: ldsityObjectForADSP,
                            readOnlyMode: readOnlyMode
                        );
                    if (frmAreal.ShowDialog() == DialogResult.OK)
                    {
                        Initialize(
                            controlOwner: ControlOwner,
                            index: index,
                            domain: domain,
                            ldsityObjects: ldsityObjects);
                    }
                    break;

                case TDArealOrPopulationEnum.Population:

                    FormClassificationRule<TDomain, TDSubPopulationCategory> frmPopulation =
                        new(
                            controlOwner: ((ControlTDDomainListBox<TDomain, TCategory>)ControlOwner).ControlOwner,
                            domain: domain,
                            ldsityObject: key.LDsityObject,
                            ldsityObjectForADSP: ldsityObjectForADSP,
                            readOnlyMode: readOnlyMode
                        );
                    if (frmPopulation.ShowDialog() == DialogResult.OK)
                    {
                        Initialize(
                            controlOwner: ControlOwner,
                            index: index,
                            domain: domain,
                            ldsityObjects: ldsityObjects);
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            List<TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier> keys = [];

            foreach (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier
                key in DictLDsityObjects.Keys)
            {
                keys.Add(item: key);
            }

            foreach (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier key in keys)
            {
                dictLDsityObjects[key] = domain.ClassificationTypeValue switch
                {
                    TDClassificationTypeEnum.Standard =>
                        // Objekty lokálních hustot a klasifikační pravidla pro kladné i záporné příspěvky jsou stejné (kladné)
                        TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.Execute(
                            database: Database,
                            ldsityObjectId: key.LDsityObjectId,
                            arealOrPopulationId: (int)ArealOrPopulation,
                            domainId: domain.Id,
                            useNegative: false),

                    TDClassificationTypeEnum.ChangeOrDynamic =>
                        // Objekty lokálních hustot a klasifikační pravidla pro kladné i záporné příspěvky jsou různé
                        TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.Execute(
                            database: Database,
                            ldsityObjectId: key.LDsityObjectId,
                            arealOrPopulationId: (int)ArealOrPopulation,
                            domainId: domain.Id,
                            useNegative: key.UseNegative),

                    _ =>
                        // Chyba, uložená procedura z databáze vrátila null,
                        // nebo někdo přidal do číselníku c_classification_type novou kategorii
                        TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.Execute(
                            database: Database,
                            ldsityObjectId: key.LDsityObjectId,
                            arealOrPopulationId: (int)ArealOrPopulation,
                            domainId: domain.Id,
                            useNegative: key.UseNegative),
                };
                dictResult[key] = null;

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.CommandText,
                        caption: TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            if (dictLDsityObjects.Values
                .Where(a => (a == null) || (a.Items.Count == 0))
                .Any())
            {
                MessageBox.Show(
                    text: msgEmptyLDsityObjectsForASPD
                         .Replace(
                            oldValue: "$1",
                            newValue: TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.Name),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            int maxCount = dictLDsityObjects.Values.Select(a => a?.Items.Count ?? 0).Max();
            maxCount = (maxCount == 0) ? 1 : maxCount;
            HeightIncrement = RadioButtonHeight * maxCount;
        }

        /// <summary>
        /// <para lang="cs">Označí položku seznamu jako vybranou</para>
        /// <para lang="en">It marks an item in the list as selected</para>
        /// </summary>
        private void HighlightIfSelected()
        {
            if (selected)
            {
                BackColor = SystemColors.Highlight;
                ForeColor = SystemColors.HighlightText;
                Font = new Font("Microsoft Sans Serif", 8.0f, FontStyle.Regular);
            }
            else
            {
                BackColor = SystemColors.Window;
                ForeColor = SystemColors.WindowText;
                Font = new Font("Microsoft Sans Serif", 8.0f, FontStyle.Regular);
            }
        }

        /// <summary>
        /// <para lang="cs">Podrobné zobrazení ovládacího prvku</para>
        /// <para lang="en">Detailed display of the control</para>
        /// </summary>
        private void Expand()
        {
            collapsed = false;
            PnlCollapsed.Visible = collapsed;
            PnlExpanded.Visible = !collapsed;
            Height = HeightIncrement * dictLDsityObjects.Keys.Count;
            ExtendedOrCollapsed?.Invoke(sender: this, e: new EventArgs());
        }

        /// <summary>
        /// <para lang="cs">Stručné zobrazení ovládacího prvku</para>
        /// <para lang="en">Brief display of the control</para>
        /// </summary>
        private void Collapse()
        {
            collapsed = true;
            PnlCollapsed.Visible = collapsed;
            PnlExpanded.Visible = !collapsed;
            Height = CollapsedHeight;
            ExtendedOrCollapsed?.Invoke(sender: this, e: new EventArgs());
        }

        /// <summary>
        /// <para lang="cs">Upraví rozměry panelů</para>
        /// <para lang="en">Adjusts panel dimensions</para>
        /// </summary>
        private void ResizePanels()
        {
            if ((pnlLDsityObjectsForADSP != null) &&
                (PnlExpandedWorkSpace != null) &&
                (pnlDomain != null) &&
                (pnlLDsityObjects != null))
            {
                pnlLDsityObjectsForADSP.Width =
                    PnlExpandedWorkSpace.Width - (pnlDomain.Width + pnlLDsityObjects.Width);

                pnlLDsityObjectsForADSP.Height =
                    PnlExpandedWorkSpace.Height;

                foreach (System.Windows.Forms.Panel pnl in pnlLDsityObjectsForADSP.Controls)
                {
                    pnl.Width = pnlLDsityObjectsForADSP.Width;
                    foreach (Control control in pnl.Controls)
                    {
                        if (control is RadioButton radioButton)
                        {
                            radioButton.Width = pnlLDsityObjectsForADSP.Width - RadioButtonOffset - ScrollBarWidth;
                        }
                    }
                }
            }
        }

        #endregion Methods

    }

}