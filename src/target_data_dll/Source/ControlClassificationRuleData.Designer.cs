﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlClassificationRuleDataDesign
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            grpMain = new System.Windows.Forms.GroupBox();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tsrMain = new System.Windows.Forms.ToolStrip();
            btnValidateSyntax = new System.Windows.Forms.ToolStripButton();
            btnValidateClassificationRule = new System.Windows.Forms.ToolStripButton();
            btnValidateClassificationRuleSet = new System.Windows.Forms.ToolStripButton();
            btnExportJson = new System.Windows.Forms.ToolStripButton();
            splMain = new System.Windows.Forms.SplitContainer();
            pnlClassificationRuleEditor = new System.Windows.Forms.Panel();
            splData = new System.Windows.Forms.SplitContainer();
            grpValidateClassificationRule = new System.Windows.Forms.GroupBox();
            pnlValidateClassificationRule = new System.Windows.Forms.Panel();
            tsrValidateClassificationRule = new System.Windows.Forms.ToolStrip();
            btnValidateClassificationRuleClose = new System.Windows.Forms.ToolStripButton();
            grpValidateClassificationRuleSet = new System.Windows.Forms.GroupBox();
            pnlValidateClassificationRuleSet = new System.Windows.Forms.Panel();
            tsrValidateClassificationRuleSet = new System.Windows.Forms.ToolStrip();
            btnValidateClassificationRuleSetClose = new System.Windows.Forms.ToolStripButton();
            btnImportJson = new System.Windows.Forms.ToolStripButton();
            pnlMain.SuspendLayout();
            grpMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tsrMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splData).BeginInit();
            splData.Panel1.SuspendLayout();
            splData.Panel2.SuspendLayout();
            splData.SuspendLayout();
            grpValidateClassificationRule.SuspendLayout();
            tsrValidateClassificationRule.SuspendLayout();
            grpValidateClassificationRuleSet.SuspendLayout();
            tsrValidateClassificationRuleSet.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.SystemColors.Control;
            pnlMain.Controls.Add(grpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(1, 1);
            pnlMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(698, 344);
            pnlMain.TabIndex = 0;
            // 
            // grpMain
            // 
            grpMain.Controls.Add(tlpMain);
            grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpMain.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpMain.Location = new System.Drawing.Point(0, 0);
            grpMain.Margin = new System.Windows.Forms.Padding(0);
            grpMain.Name = "grpMain";
            grpMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpMain.Size = new System.Drawing.Size(698, 344);
            grpMain.TabIndex = 8;
            grpMain.TabStop = false;
            grpMain.Text = "grpMain";
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tsrMain, 0, 0);
            tlpMain.Controls.Add(splMain, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(4, 18);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Size = new System.Drawing.Size(690, 323);
            tlpMain.TabIndex = 1;
            // 
            // tsrMain
            // 
            tsrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnValidateSyntax, btnValidateClassificationRule, btnValidateClassificationRuleSet, btnExportJson, btnImportJson });
            tsrMain.Location = new System.Drawing.Point(0, 0);
            tsrMain.Name = "tsrMain";
            tsrMain.Size = new System.Drawing.Size(690, 25);
            tsrMain.TabIndex = 1;
            tsrMain.Text = "tsrMain";
            // 
            // btnValidateSyntax
            // 
            btnValidateSyntax.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnValidateSyntax.ForeColor = System.Drawing.SystemColors.ControlText;
            btnValidateSyntax.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnValidateSyntax.Name = "btnValidateSyntax";
            btnValidateSyntax.Size = new System.Drawing.Size(105, 22);
            btnValidateSyntax.Text = "btnValidateSyntax";
            // 
            // btnValidateClassificationRule
            // 
            btnValidateClassificationRule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnValidateClassificationRule.ForeColor = System.Drawing.SystemColors.ControlText;
            btnValidateClassificationRule.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnValidateClassificationRule.Name = "btnValidateClassificationRule";
            btnValidateClassificationRule.Size = new System.Drawing.Size(163, 22);
            btnValidateClassificationRule.Text = "btnValidateClassificationRule";
            // 
            // btnValidateClassificationRuleSet
            // 
            btnValidateClassificationRuleSet.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnValidateClassificationRuleSet.ForeColor = System.Drawing.SystemColors.ControlText;
            btnValidateClassificationRuleSet.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnValidateClassificationRuleSet.Name = "btnValidateClassificationRuleSet";
            btnValidateClassificationRuleSet.Size = new System.Drawing.Size(179, 22);
            btnValidateClassificationRuleSet.Text = "btnValidateClassificationRuleSet";
            // 
            // btnExportJson
            // 
            btnExportJson.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnExportJson.ForeColor = System.Drawing.SystemColors.ControlText;
            btnExportJson.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnExportJson.Name = "btnExportJson";
            btnExportJson.Size = new System.Drawing.Size(86, 22);
            btnExportJson.Text = "btnExportJson";
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.Location = new System.Drawing.Point(0, 29);
            splMain.Margin = new System.Windows.Forms.Padding(0);
            splMain.Name = "splMain";
            splMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(pnlClassificationRuleEditor);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(splData);
            splMain.Size = new System.Drawing.Size(690, 294);
            splMain.SplitterDistance = 64;
            splMain.SplitterWidth = 1;
            splMain.TabIndex = 0;
            // 
            // pnlClassificationRuleEditor
            // 
            pnlClassificationRuleEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClassificationRuleEditor.ForeColor = System.Drawing.SystemColors.ControlText;
            pnlClassificationRuleEditor.Location = new System.Drawing.Point(0, 0);
            pnlClassificationRuleEditor.Margin = new System.Windows.Forms.Padding(0);
            pnlClassificationRuleEditor.Name = "pnlClassificationRuleEditor";
            pnlClassificationRuleEditor.Size = new System.Drawing.Size(690, 64);
            pnlClassificationRuleEditor.TabIndex = 0;
            // 
            // splData
            // 
            splData.Dock = System.Windows.Forms.DockStyle.Fill;
            splData.IsSplitterFixed = true;
            splData.Location = new System.Drawing.Point(0, 0);
            splData.Margin = new System.Windows.Forms.Padding(0);
            splData.Name = "splData";
            splData.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splData.Panel1
            // 
            splData.Panel1.Controls.Add(grpValidateClassificationRule);
            splData.Panel1MinSize = 0;
            // 
            // splData.Panel2
            // 
            splData.Panel2.Controls.Add(grpValidateClassificationRuleSet);
            splData.Panel2MinSize = 0;
            splData.Size = new System.Drawing.Size(690, 229);
            splData.SplitterDistance = 103;
            splData.SplitterWidth = 1;
            splData.TabIndex = 0;
            // 
            // grpValidateClassificationRule
            // 
            grpValidateClassificationRule.Controls.Add(pnlValidateClassificationRule);
            grpValidateClassificationRule.Controls.Add(tsrValidateClassificationRule);
            grpValidateClassificationRule.Dock = System.Windows.Forms.DockStyle.Fill;
            grpValidateClassificationRule.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpValidateClassificationRule.Location = new System.Drawing.Point(0, 0);
            grpValidateClassificationRule.Margin = new System.Windows.Forms.Padding(0);
            grpValidateClassificationRule.Name = "grpValidateClassificationRule";
            grpValidateClassificationRule.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpValidateClassificationRule.Size = new System.Drawing.Size(690, 103);
            grpValidateClassificationRule.TabIndex = 6;
            grpValidateClassificationRule.TabStop = false;
            grpValidateClassificationRule.Text = "grpValidateClassificationRule";
            // 
            // pnlValidateClassificationRule
            // 
            pnlValidateClassificationRule.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlValidateClassificationRule.Location = new System.Drawing.Point(4, 43);
            pnlValidateClassificationRule.Margin = new System.Windows.Forms.Padding(0);
            pnlValidateClassificationRule.Name = "pnlValidateClassificationRule";
            pnlValidateClassificationRule.Size = new System.Drawing.Size(682, 57);
            pnlValidateClassificationRule.TabIndex = 3;
            // 
            // tsrValidateClassificationRule
            // 
            tsrValidateClassificationRule.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnValidateClassificationRuleClose });
            tsrValidateClassificationRule.Location = new System.Drawing.Point(4, 18);
            tsrValidateClassificationRule.Name = "tsrValidateClassificationRule";
            tsrValidateClassificationRule.Size = new System.Drawing.Size(682, 25);
            tsrValidateClassificationRule.TabIndex = 2;
            tsrValidateClassificationRule.Text = "tsrValidateClassificationRule";
            // 
            // btnValidateClassificationRuleClose
            // 
            btnValidateClassificationRuleClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            btnValidateClassificationRuleClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnValidateClassificationRuleClose.ForeColor = System.Drawing.SystemColors.ControlText;
            btnValidateClassificationRuleClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnValidateClassificationRuleClose.Name = "btnValidateClassificationRuleClose";
            btnValidateClassificationRuleClose.Size = new System.Drawing.Size(192, 22);
            btnValidateClassificationRuleClose.Text = "btnValidateClassificationRuleClose";
            // 
            // grpValidateClassificationRuleSet
            // 
            grpValidateClassificationRuleSet.Controls.Add(pnlValidateClassificationRuleSet);
            grpValidateClassificationRuleSet.Controls.Add(tsrValidateClassificationRuleSet);
            grpValidateClassificationRuleSet.Dock = System.Windows.Forms.DockStyle.Fill;
            grpValidateClassificationRuleSet.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpValidateClassificationRuleSet.Location = new System.Drawing.Point(0, 0);
            grpValidateClassificationRuleSet.Margin = new System.Windows.Forms.Padding(0);
            grpValidateClassificationRuleSet.Name = "grpValidateClassificationRuleSet";
            grpValidateClassificationRuleSet.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpValidateClassificationRuleSet.Size = new System.Drawing.Size(690, 125);
            grpValidateClassificationRuleSet.TabIndex = 7;
            grpValidateClassificationRuleSet.TabStop = false;
            grpValidateClassificationRuleSet.Text = "grpValidateClassificationRuleSet";
            // 
            // pnlValidateClassificationRuleSet
            // 
            pnlValidateClassificationRuleSet.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlValidateClassificationRuleSet.Location = new System.Drawing.Point(4, 43);
            pnlValidateClassificationRuleSet.Margin = new System.Windows.Forms.Padding(0);
            pnlValidateClassificationRuleSet.Name = "pnlValidateClassificationRuleSet";
            pnlValidateClassificationRuleSet.Size = new System.Drawing.Size(682, 79);
            pnlValidateClassificationRuleSet.TabIndex = 4;
            // 
            // tsrValidateClassificationRuleSet
            // 
            tsrValidateClassificationRuleSet.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnValidateClassificationRuleSetClose });
            tsrValidateClassificationRuleSet.Location = new System.Drawing.Point(4, 18);
            tsrValidateClassificationRuleSet.Name = "tsrValidateClassificationRuleSet";
            tsrValidateClassificationRuleSet.Size = new System.Drawing.Size(682, 25);
            tsrValidateClassificationRuleSet.TabIndex = 2;
            tsrValidateClassificationRuleSet.Text = "tsrValidateClassificationRuleSet";
            // 
            // btnValidateClassificationRuleSetClose
            // 
            btnValidateClassificationRuleSetClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            btnValidateClassificationRuleSetClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnValidateClassificationRuleSetClose.ForeColor = System.Drawing.SystemColors.ControlText;
            btnValidateClassificationRuleSetClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnValidateClassificationRuleSetClose.Name = "btnValidateClassificationRuleSetClose";
            btnValidateClassificationRuleSetClose.Size = new System.Drawing.Size(208, 22);
            btnValidateClassificationRuleSetClose.Text = "btnValidateClassificationRuleSetClose";
            // 
            // btnImportJson
            // 
            btnImportJson.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnImportJson.ForeColor = System.Drawing.SystemColors.ControlText;
            btnImportJson.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnImportJson.Name = "btnImportJson";
            btnImportJson.Size = new System.Drawing.Size(88, 22);
            btnImportJson.Text = "btnImportJson";
            // 
            // ControlClassificationRuleDataDesign
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.SystemColors.HotTrack;
            Controls.Add(pnlMain);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlClassificationRuleDataDesign";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(700, 346);
            pnlMain.ResumeLayout(false);
            grpMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            tsrMain.ResumeLayout(false);
            tsrMain.PerformLayout();
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            splData.Panel1.ResumeLayout(false);
            splData.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splData).EndInit();
            splData.ResumeLayout(false);
            grpValidateClassificationRule.ResumeLayout(false);
            grpValidateClassificationRule.PerformLayout();
            tsrValidateClassificationRule.ResumeLayout(false);
            tsrValidateClassificationRule.PerformLayout();
            grpValidateClassificationRuleSet.ResumeLayout(false);
            grpValidateClassificationRuleSet.PerformLayout();
            tsrValidateClassificationRuleSet.ResumeLayout(false);
            tsrValidateClassificationRuleSet.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.ToolStrip tsrMain;
        private System.Windows.Forms.ToolStripButton btnValidateClassificationRule;
        private System.Windows.Forms.ToolStripButton btnValidateClassificationRuleSet;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.Panel pnlClassificationRuleEditor;
        private System.Windows.Forms.SplitContainer splData;
        private System.Windows.Forms.GroupBox grpValidateClassificationRule;
        private System.Windows.Forms.ToolStrip tsrValidateClassificationRule;
        private System.Windows.Forms.ToolStripButton btnValidateClassificationRuleClose;
        private System.Windows.Forms.GroupBox grpValidateClassificationRuleSet;
        private System.Windows.Forms.ToolStrip tsrValidateClassificationRuleSet;
        private System.Windows.Forms.ToolStripButton btnValidateClassificationRuleSetClose;
        private System.Windows.Forms.Panel pnlValidateClassificationRule;
        private System.Windows.Forms.Panel pnlValidateClassificationRuleSet;
        private System.Windows.Forms.ToolStripButton btnValidateSyntax;
        private System.Windows.Forms.ToolStripButton btnExportJson;
        private System.Windows.Forms.ToolStripButton btnImportJson;
    }

}
