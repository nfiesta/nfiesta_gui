﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek - nadpis pro klasifikační pravidla - třída pro Designer</para>
    /// <para lang="en">Control - caption for classification rules - class for Designer</para>
    /// </summary>
    internal partial class ControlClassificationRuleCaptionDesign
        : UserControl
    {
        protected ControlClassificationRuleCaptionDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.CheckBox ChkIncludeLDsityObjectForADSP => chkIncludeLDsityObjectForADSP;
        protected System.Windows.Forms.Label LblCategoryCaption => lblCategoryCaption;
        protected System.Windows.Forms.Label LblCategoryPlaceHolder => lblCategoryPlaceHolder;
        protected System.Windows.Forms.Label LblCategoryValue => lblCategoryValue;
        protected System.Windows.Forms.Label LblLDsityObjectCaption => lblLDsityObjectCaption;
        protected System.Windows.Forms.Label LblLDsityObjectForADSPCaption => lblLDsityObjectForADSPCaption;
        protected System.Windows.Forms.Label LblLDsityObjectForADSPValue => lblLDsityObjectForADSPValue;
        protected System.Windows.Forms.Label LblLDsityObjectPlaceHolder => lblLDsityObjectPlaceHolder;
        protected System.Windows.Forms.Label LblLDsityObjectValue => lblLDsityObjectValue;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek - nadpis pro klasifikační pravidla</para>
    /// <para lang="en">Control - caption for classification rules</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlClassificationRuleCaption<TDomain, TCategory>
        : ControlClassificationRuleCaptionDesign, INfiEstaControl, ITargetDataControl
        where TDomain : IArealOrPopulationDomain
        where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Zobrazovat CheckBox pro zahrnutí objektu lokálních hustot pro třídění
        /// do seznamu pro ukládání do databáze</para>
        /// <para lang="en">Display CheckBox for include local density object for classification
        /// among objecs for saving in database</para>
        /// </summary>
        private bool displayCheckBox;

        /// <summary>
        /// <para lang="cs">Seznam objektů lokálních hustot pro třídění
        /// vyřazených z ukládání do databáze</para>
        /// <para lang="en">List of local density objects for classification
        /// excluded from saving into database</para>
        /// </summary>
        private List<TDLDsityObject> excludedLDsityObjectsForADSP;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        private string strCategoryValueNone = String.Empty;
        private string strLDsityObjectValueNone = String.Empty;
        private string strLDsityObjectForADSPValueNone = String.Empty;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">
        /// Událost
        /// "Zahrnout nebo vyřadit objekt lokální hustoty
        /// pro třídění ze seznamu objektů ukládaných do databáze"</para>
        /// <para lang="en">
        /// "Include or exclude local density object for classification
        /// from the list of objects for saving into database" event</para>
        /// </summary>
        public event EventHandler LDsityObjectForADSPExcluded;

        #endregion Events


        #region Controls

        /// <summary>
        /// <para lang="cs">ToolTip pro CheckBox zahrnutí objektu lokální hustoty pro třídění
        /// do seznamu pro uložení do databáze</para>
        /// <para lang="en">ToolTip for CheckBox for include local density object for classification
        /// among objecs for saving in database</para>
        /// </summary>
        private ToolTip tipIncludeLDsityObjectForADSP;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlClassificationRuleCaption(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not IArealOrPopulationForm<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(IArealOrPopulationForm<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not IArealOrPopulationForm<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                         message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(IArealOrPopulationForm<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Základní formulář</para>
        /// <para lang="en">Base form</para>
        /// </summary>
        public IArealOrPopulationForm<TDomain, TCategory> Base
        {
            get
            {
                return
                    ((IArealOrPopulationForm<TDomain, TCategory>)ControlOwner);
            }
        }

        /// <summary>
        /// <para lang="cs">Pár klasifikačních pravidel pro kladný a záporný příspěvek (read-only)</para>
        /// <para lang="en">Classification rule pair for positive and negative contribution (read-only)</para>
        /// </summary>
        private ClassificationRulePair<TDomain, TCategory> ClassificationRulePair
        {
            get
            {
                return Base.SelectedClassificationRulePair;
            }
        }

        /// <summary>
        /// <para lang="cs">Je aktuální objekt lokální hustoty pro třídění
        /// zahrnutý mezi objekty pro uložení do databáze? (read-only)</para>
        /// <para lang="en">Is current local density object for classification
        /// included among objects for saving in database? (read-only)</para>
        /// </summary>
        public bool LDsityObjectForADSPIncluded
        {
            get
            {
                if (ClassificationRulePair == null)
                {
                    return false;
                }

                return IsIncludedLDsityObjectForADSP(
                    ldsityObjectForADSP: ClassificationRulePair.LDsityObjectForADSP);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazovat CheckBox pro zahrnutí objektu lokálních hustot pro třídění
        /// do seznamu pro ukládání do databáze</para>
        /// <para lang="en">Display CheckBox for include local density object for classification
        /// among objecs for saving in database</para>
        /// </summary>
        public bool DisplayCheckBox
        {
            get
            {
                return displayCheckBox;
            }
            set
            {
                displayCheckBox = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam objektů lokálních hustot pro třídění
        /// vyřazených z ukládání do databáze</para>
        /// <para lang="en">List of local density objects for ADSP
        /// excluded from saving into database</para>
        /// </summary>
        private List<TDLDsityObject> ExcludedLDsityObjectsForADSP
        {
            get
            {
                return excludedLDsityObjectsForADSP ?? [];
            }
            set
            {
                excludedLDsityObjectsForADSP = value ?? [];
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                    ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain =>
                            (languageFile == null)
                            ? new Dictionary<string, string>()
                            {
                                { nameof(LblCategoryCaption),               "Kategorie plošné domény:" },
                                { nameof(LblCategoryPlaceHolder),           String.Empty  },
                                { nameof(LblLDsityObjectCaption),           "Objekt lokální hustoty:" },
                                { nameof(LblLDsityObjectForADSPCaption),    "Objekt lokální hustoty pro třídění:" },
                                { nameof(LblLDsityObjectPlaceHolder),       String.Empty},
                                { nameof(tipIncludeLDsityObjectForADSP),    "Zahrnout objekt lokální hustoty pro třídění mezi objekty ukládané do databáze?" },
                                { nameof(strCategoryValueNone),             "žádná" },
                                { nameof(strLDsityObjectValueNone),         "žádný" },
                                { nameof(strLDsityObjectForADSPValueNone),  "žádný" }
                            }
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(ControlClassificationRuleCaption<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                                out Dictionary<string, string> dictNationalAreaDomain)
                                    ? dictNationalAreaDomain
                                    : [],

                        TDArealOrPopulationEnum.Population => (languageFile == null)
                            ? new Dictionary<string, string>()
                            {
                                { nameof(LblCategoryCaption),               "Kategorie subpopulace:"  },
                                { nameof(LblCategoryPlaceHolder),           String.Empty  },
                                { nameof(LblLDsityObjectCaption),           "Objekt lokální hustoty:" },
                                { nameof(LblLDsityObjectForADSPCaption),    "Objekt lokální hustoty pro třídění:"  },
                                { nameof(LblLDsityObjectPlaceHolder),       String.Empty},
                                { nameof(tipIncludeLDsityObjectForADSP),    "Zahrnout objekt lokální hustoty pro třídění mezi objekty ukládané do databáze?" },
                                { nameof(strCategoryValueNone),             "žádná" },
                                { nameof(strLDsityObjectValueNone),         "žádný" },
                                { nameof(strLDsityObjectForADSPValueNone),  "žádný" }
                            }
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(ControlClassificationRuleCaption<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                out Dictionary<string, string> dictNationalSubPopulation)
                                    ? dictNationalSubPopulation
                                    : [],

                        _ => [],
                    },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain =>
                        (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(LblCategoryCaption),               "Area domain category:" },
                            { nameof(LblCategoryPlaceHolder),           String.Empty  },
                            { nameof(LblLDsityObjectCaption),           "Local density object:" },
                            { nameof(LblLDsityObjectForADSPCaption),    "Local density object for classification:" },
                            { nameof(LblLDsityObjectPlaceHolder),       String.Empty},
                            { nameof(tipIncludeLDsityObjectForADSP),    "Enable saving to database this local density object for classification?"  },
                            { nameof(strCategoryValueNone),             "none" },
                            { nameof(strLDsityObjectValueNone),         "none" },
                            { nameof(strLDsityObjectForADSPValueNone),  "none" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleCaption<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population =>
                        (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(LblCategoryCaption),                   "Subpopulation category:" },
                            { nameof(LblCategoryPlaceHolder),               String.Empty  },
                            { nameof(LblLDsityObjectCaption),               "Local density object:" },
                            { nameof(LblLDsityObjectForADSPCaption),        "Local density object for classification:" },
                            { nameof(LblLDsityObjectPlaceHolder),           String.Empty},
                            { nameof(tipIncludeLDsityObjectForADSP),        "Enable saving to database this local density object for classification?"  },
                            { nameof(strCategoryValueNone),                 "none" },
                            { nameof(strLDsityObjectValueNone),             "none" },
                            { nameof(strLDsityObjectForADSPValueNone),      "none" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleCaption<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BackColor = System.Drawing.SystemColors.Control;

            ControlOwner = controlOwner;

            DisplayCheckBox = false;

            ExcludedLDsityObjectsForADSP = [];

            onEdit = false;

            LoadContent();

            InitializeControls();

            InitializeLabels();

            Reset();

            ChkIncludeLDsityObjectForADSP.CheckedChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        IncludeOrExcludeLDsityObjectForADSP();
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacích prvků</para>
        /// <para lang="en">Initializing user controls</para>
        /// </summary>
        private void InitializeControls()
        {
            // ToolTip
            tipIncludeLDsityObjectForADSP = new ToolTip();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing user control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            LblCategoryCaption.Text =
                labels.TryGetValue(key: nameof(LblCategoryCaption),
                    out string lblCategoryCaptionText)
                        ? lblCategoryCaptionText
                        : String.Empty;

            LblCategoryPlaceHolder.Text =
                labels.TryGetValue(key: nameof(LblCategoryPlaceHolder),
                    out string lblCategoryPlaceHolderText)
                        ? lblCategoryPlaceHolderText
                        : String.Empty;

            LblLDsityObjectCaption.Text =
                labels.TryGetValue(key: nameof(LblLDsityObjectCaption),
                    out string lblLDsityObjectCaptionText)
                        ? lblLDsityObjectCaptionText
                        : String.Empty;

            LblLDsityObjectForADSPCaption.Text =
                labels.TryGetValue(key: nameof(LblLDsityObjectForADSPCaption),
                    out string lblLDsityObjectForADSPCaptionText)
                        ? lblLDsityObjectForADSPCaptionText
                        : String.Empty;

            LblLDsityObjectPlaceHolder.Text =
                labels.TryGetValue(key: nameof(LblLDsityObjectPlaceHolder),
                    out string lblLDsityObjectPlaceHolderText)
                        ? lblLDsityObjectPlaceHolderText
                        : String.Empty;

            tipIncludeLDsityObjectForADSP.SetToolTip(
                control: ChkIncludeLDsityObjectForADSP,
                caption: labels.TryGetValue(key: nameof(tipIncludeLDsityObjectForADSP),
                    out string tipIncludeLDsityObjectForADSPText)
                        ? tipIncludeLDsityObjectForADSPText
                        : String.Empty);
        }

        /// <summary>
        /// <para lang="cs">
        /// Zahrne nebo odebere objekt lokální hustoty pro třídění
        /// mezi objekty ukládané do databáze"</para>
        /// <para lang="en">
        /// Include or exclude local denisty object for ADSP
        /// among objects for saving into database"</para>
        /// </summary>
        private void IncludeOrExcludeLDsityObjectForADSP()
        {
            if (ChkIncludeLDsityObjectForADSP.Checked)
            {
                // Zahrne (odstraní z odebraných)
                // objekt lokální hustoty pro třídění mezi objekty ukládané do databáze
                if (ExcludedLDsityObjectsForADSP.Contains(
                        item: ClassificationRulePair.LDsityObjectForADSP))
                {
                    ExcludedLDsityObjectsForADSP.Remove(
                        item: ClassificationRulePair.LDsityObjectForADSP);
                }
            }

            else
            {
                // Odebere (přidá mezi odebrané)
                // objekt lokální hustoty pro třídění z objektů ukládaných do databáze
                if (!ExcludedLDsityObjectsForADSP.Contains(
                        item: ClassificationRulePair.LDsityObjectForADSP))
                {
                    ExcludedLDsityObjectsForADSP.Add(
                        item: ClassificationRulePair.LDsityObjectForADSP);
                }
            }

            LDsityObjectForADSPExcluded?.Invoke(sender: this, e: new EventArgs());
        }

        /// <summary>
        /// <para lang="cs">Je objekt lokální hustoty pro třídění
        /// zahrnutý mezi objekty pro uložení do databáze?</para>
        /// <para lang="en">Is local density object for classification
        /// included among objects for saving in database?</para>
        /// </summary>
        /// <param name="ldsityObjectForADSP">
        /// <para lang="cs">Objekt lokální hustoty pro třídění</para>
        /// <para lang="en">Local density object for classification</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Je objekt lokální hustoty pro třídění
        /// zahrnutý mezi objekty pro uložení do databáze?</para>
        /// <para lang="en">Is local density object for classification
        /// included among objects for saving in database?</para>
        /// </returns>
        public bool IsIncludedLDsityObjectForADSP(
            TDLDsityObject ldsityObjectForADSP)
        {
            if (ldsityObjectForADSP == null)
            {
                return false;
            }

            return
                !ExcludedLDsityObjectsForADSP.Contains(
                    item: ldsityObjectForADSP);
        }

        /// <summary>
        /// <para lang="cs">Nastavení nadpisu pro klasifikační pravidlo</para>
        /// <para lang="en">Reset caption for classification rule</para>
        /// </summary>
        public void Reset()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            strCategoryValueNone =
                labels.TryGetValue(key: nameof(strCategoryValueNone),
                    out string strCategoryValueNoneText)
                        ? strCategoryValueNoneText
                        : String.Empty;

            strLDsityObjectValueNone =
                labels.TryGetValue(key: nameof(strLDsityObjectValueNone),
                    out string strLDsityObjectValueNoneText)
                        ? strLDsityObjectValueNoneText
                        : String.Empty;

            strLDsityObjectForADSPValueNone =
                labels.TryGetValue(key: nameof(strLDsityObjectForADSPValueNone),
                    out string strLDsityObjectForADSPValueNoneText)
                        ? strLDsityObjectForADSPValueNoneText
                        : String.Empty;

            if (ClassificationRulePair == null)
            {
                LblCategoryValue.Text = strCategoryValueNone;
                LblLDsityObjectValue.Text = strLDsityObjectValueNone;
                LblLDsityObjectForADSPValue.Text = strLDsityObjectForADSPValueNone;

                if (DisplayCheckBox)
                {
                    onEdit = true;
                    ChkIncludeLDsityObjectForADSP.Visible = false;
                    ChkIncludeLDsityObjectForADSP.Checked = false;
                    onEdit = false;
                }
                return;
            }

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    LblCategoryValue.Text =
                        (ClassificationRulePair.Category == null) ?
                        strCategoryValueNone :
                        ClassificationRulePair.Category.ExtendedLabelCs;

                    LblLDsityObjectValue.Text =
                        (ClassificationRulePair.LDsityObject == null) ?
                        strLDsityObjectValueNone :
                        ClassificationRulePair.LDsityObject.ExtendedLabelCs;

                    LblLDsityObjectForADSPValue.Text =
                        (ClassificationRulePair.LDsityObjectForADSP == null) ?
                        strLDsityObjectForADSPValueNone :
                        ClassificationRulePair.LDsityObjectForADSP.ExtendedLabelCs;
                    break;

                case LanguageVersion.International:
                    LblCategoryValue.Text =
                        (ClassificationRulePair.Category == null) ?
                        strCategoryValueNone :
                        ClassificationRulePair.Category.ExtendedLabelEn;

                    LblLDsityObjectValue.Text =
                        (ClassificationRulePair.LDsityObject == null) ?
                        strLDsityObjectValueNone :
                        ClassificationRulePair.LDsityObject.ExtendedLabelEn;

                    LblLDsityObjectForADSPValue.Text =
                        (ClassificationRulePair.LDsityObjectForADSP == null) ?
                        strLDsityObjectForADSPValueNone :
                        ClassificationRulePair.LDsityObjectForADSP.ExtendedLabelEn;
                    break;

                default:
                    LblCategoryValue.Text = String.Empty;
                    LblLDsityObjectValue.Text = String.Empty;
                    LblLDsityObjectForADSPValue.Text = String.Empty;
                    break;
            }

            if (DisplayCheckBox)
            {
                onEdit = true;
                ChkIncludeLDsityObjectForADSP.Visible =
                    ClassificationRulePair.LDsityObjectForADSP != null;
                ChkIncludeLDsityObjectForADSP.Checked =
                    LDsityObjectForADSPIncluded;
                onEdit = false;
            }
        }

        #endregion Methods

    }

}