﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlDependenceEditorDesign
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.grpDependence = new System.Windows.Forms.GroupBox();
            this.splDependence = new System.Windows.Forms.SplitContainer();
            this.grpDependencePossible = new System.Windows.Forms.GroupBox();
            this.tlpDependencePossible = new System.Windows.Forms.TableLayoutPanel();
            this.tsrDependencePossible = new System.Windows.Forms.ToolStrip();
            this.btnDependenceSelect = new System.Windows.Forms.ToolStripButton();
            this.lstDependencePossible = new System.Windows.Forms.ListBox();
            this.splDependenceInner = new System.Windows.Forms.SplitContainer();
            this.grpDependenceSelected = new System.Windows.Forms.GroupBox();
            this.tlpDependenceSelected = new System.Windows.Forms.TableLayoutPanel();
            this.lstDependenceSelected = new System.Windows.Forms.ListBox();
            this.tsrDependenceSelected = new System.Windows.Forms.ToolStrip();
            this.btnDependenceCancel = new System.Windows.Forms.ToolStripButton();
            this.btnHierarchy = new System.Windows.Forms.ToolStripButton();
            this.tsrMain = new System.Windows.Forms.ToolStrip();
            this.btnClose = new System.Windows.Forms.ToolStripButton();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.grpDependence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splDependence)).BeginInit();
            this.splDependence.Panel1.SuspendLayout();
            this.splDependence.Panel2.SuspendLayout();
            this.splDependence.SuspendLayout();
            this.grpDependencePossible.SuspendLayout();
            this.tlpDependencePossible.SuspendLayout();
            this.tsrDependencePossible.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splDependenceInner)).BeginInit();
            this.splDependenceInner.Panel1.SuspendLayout();
            this.splDependenceInner.SuspendLayout();
            this.grpDependenceSelected.SuspendLayout();
            this.tlpDependenceSelected.SuspendLayout();
            this.tsrDependenceSelected.SuspendLayout();
            this.tsrMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(598, 298);
            this.pnlMain.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.grpDependence, 0, 1);
            this.tlpMain.Controls.Add(this.tsrMain, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.Padding = new System.Windows.Forms.Padding(3);
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Size = new System.Drawing.Size(598, 298);
            this.tlpMain.TabIndex = 10;
            // 
            // grpDependence
            // 
            this.grpDependence.Controls.Add(this.splDependence);
            this.grpDependence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDependence.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpDependence.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDependence.Location = new System.Drawing.Point(3, 28);
            this.grpDependence.Margin = new System.Windows.Forms.Padding(0);
            this.grpDependence.Name = "grpDependence";
            this.grpDependence.Size = new System.Drawing.Size(592, 267);
            this.grpDependence.TabIndex = 10;
            this.grpDependence.TabStop = false;
            this.grpDependence.Text = "grpDependence";
            // 
            // splDependence
            // 
            this.splDependence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splDependence.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splDependence.Location = new System.Drawing.Point(3, 18);
            this.splDependence.Margin = new System.Windows.Forms.Padding(0);
            this.splDependence.Name = "splDependence";
            // 
            // splDependence.Panel1
            // 
            this.splDependence.Panel1.Controls.Add(this.grpDependencePossible);
            // 
            // splDependence.Panel2
            // 
            this.splDependence.Panel2.Controls.Add(this.splDependenceInner);
            this.splDependence.Size = new System.Drawing.Size(586, 246);
            this.splDependence.SplitterDistance = 200;
            this.splDependence.TabIndex = 0;
            // 
            // grpDependencePossible
            // 
            this.grpDependencePossible.Controls.Add(this.tlpDependencePossible);
            this.grpDependencePossible.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDependencePossible.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpDependencePossible.Location = new System.Drawing.Point(0, 0);
            this.grpDependencePossible.Margin = new System.Windows.Forms.Padding(0);
            this.grpDependencePossible.Name = "grpDependencePossible";
            this.grpDependencePossible.Size = new System.Drawing.Size(200, 246);
            this.grpDependencePossible.TabIndex = 0;
            this.grpDependencePossible.TabStop = false;
            this.grpDependencePossible.Text = "grpDependencePossible";
            // 
            // tlpDependencePossible
            // 
            this.tlpDependencePossible.ColumnCount = 1;
            this.tlpDependencePossible.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDependencePossible.Controls.Add(this.tsrDependencePossible, 0, 0);
            this.tlpDependencePossible.Controls.Add(this.lstDependencePossible, 0, 1);
            this.tlpDependencePossible.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDependencePossible.Location = new System.Drawing.Point(3, 18);
            this.tlpDependencePossible.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDependencePossible.Name = "tlpDependencePossible";
            this.tlpDependencePossible.RowCount = 2;
            this.tlpDependencePossible.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpDependencePossible.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDependencePossible.Size = new System.Drawing.Size(194, 225);
            this.tlpDependencePossible.TabIndex = 0;
            // 
            // tsrDependencePossible
            // 
            this.tsrDependencePossible.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDependenceSelect});
            this.tsrDependencePossible.Location = new System.Drawing.Point(0, 0);
            this.tsrDependencePossible.Name = "tsrDependencePossible";
            this.tsrDependencePossible.Size = new System.Drawing.Size(194, 20);
            this.tsrDependencePossible.TabIndex = 2;
            this.tsrDependencePossible.Text = "tsrPossibleDependence";
            // 
            // btnDependenceSelect
            // 
            this.btnDependenceSelect.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnDependenceSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDependenceSelect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDependenceSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDependenceSelect.Name = "btnDependenceSelect";
            this.btnDependenceSelect.Size = new System.Drawing.Size(126, 17);
            this.btnDependenceSelect.Text = "btnDependenceSelect";
            // 
            // lstDependencePossible
            // 
            this.lstDependencePossible.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDependencePossible.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstDependencePossible.FormattingEnabled = true;
            this.lstDependencePossible.ItemHeight = 15;
            this.lstDependencePossible.Location = new System.Drawing.Point(3, 23);
            this.lstDependencePossible.Name = "lstDependencePossible";
            this.lstDependencePossible.Size = new System.Drawing.Size(188, 199);
            this.lstDependencePossible.TabIndex = 3;
            // 
            // splDependenceInner
            // 
            this.splDependenceInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splDependenceInner.Location = new System.Drawing.Point(0, 0);
            this.splDependenceInner.Name = "splDependenceInner";
            // 
            // splDependenceInner.Panel1
            // 
            this.splDependenceInner.Panel1.Controls.Add(this.grpDependenceSelected);
            this.splDependenceInner.Panel2Collapsed = true;
            this.splDependenceInner.Size = new System.Drawing.Size(382, 246);
            this.splDependenceInner.SplitterDistance = 129;
            this.splDependenceInner.TabIndex = 0;
            // 
            // grpDependenceSelected
            // 
            this.grpDependenceSelected.Controls.Add(this.tlpDependenceSelected);
            this.grpDependenceSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDependenceSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpDependenceSelected.Location = new System.Drawing.Point(0, 0);
            this.grpDependenceSelected.Margin = new System.Windows.Forms.Padding(0);
            this.grpDependenceSelected.Name = "grpDependenceSelected";
            this.grpDependenceSelected.Size = new System.Drawing.Size(382, 246);
            this.grpDependenceSelected.TabIndex = 2;
            this.grpDependenceSelected.TabStop = false;
            this.grpDependenceSelected.Text = "grpSelectedDependence";
            // 
            // tlpDependenceSelected
            // 
            this.tlpDependenceSelected.ColumnCount = 1;
            this.tlpDependenceSelected.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDependenceSelected.Controls.Add(this.lstDependenceSelected, 0, 1);
            this.tlpDependenceSelected.Controls.Add(this.tsrDependenceSelected, 0, 0);
            this.tlpDependenceSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDependenceSelected.Location = new System.Drawing.Point(3, 18);
            this.tlpDependenceSelected.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDependenceSelected.Name = "tlpDependenceSelected";
            this.tlpDependenceSelected.RowCount = 2;
            this.tlpDependenceSelected.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpDependenceSelected.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDependenceSelected.Size = new System.Drawing.Size(376, 225);
            this.tlpDependenceSelected.TabIndex = 1;
            // 
            // lstDependenceSelected
            // 
            this.lstDependenceSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDependenceSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstDependenceSelected.FormattingEnabled = true;
            this.lstDependenceSelected.ItemHeight = 15;
            this.lstDependenceSelected.Location = new System.Drawing.Point(3, 23);
            this.lstDependenceSelected.Name = "lstDependenceSelected";
            this.lstDependenceSelected.Size = new System.Drawing.Size(370, 199);
            this.lstDependenceSelected.TabIndex = 4;
            // 
            // tsrDependenceSelected
            // 
            this.tsrDependenceSelected.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDependenceCancel,
            this.btnHierarchy});
            this.tsrDependenceSelected.Location = new System.Drawing.Point(0, 0);
            this.tsrDependenceSelected.Name = "tsrDependenceSelected";
            this.tsrDependenceSelected.Size = new System.Drawing.Size(376, 20);
            this.tsrDependenceSelected.TabIndex = 2;
            this.tsrDependenceSelected.Text = "tsrSelectedDependence";
            // 
            // btnDependenceCancel
            // 
            this.btnDependenceCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDependenceCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDependenceCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDependenceCancel.Name = "btnDependenceCancel";
            this.btnDependenceCancel.Size = new System.Drawing.Size(131, 17);
            this.btnDependenceCancel.Text = "btnDependenceCancel";
            // 
            // btnHierarchy
            // 
            this.btnHierarchy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnHierarchy.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnHierarchy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHierarchy.Name = "btnHierarchy";
            this.btnHierarchy.Size = new System.Drawing.Size(80, 17);
            this.btnHierarchy.Text = "btnHierarchy";
            // 
            // tsrMain
            // 
            this.tsrMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnClose});
            this.tsrMain.Location = new System.Drawing.Point(3, 3);
            this.tsrMain.Name = "tsrMain";
            this.tsrMain.Padding = new System.Windows.Forms.Padding(0);
            this.tsrMain.Size = new System.Drawing.Size(592, 25);
            this.tsrMain.TabIndex = 0;
            this.tsrMain.Text = "tsrMain";
            // 
            // btnClose
            // 
            this.btnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(58, 25);
            this.btnClose.Text = "btnClose";
            // 
            // ControlDependenceEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlDependenceEditor";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(600, 300);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.grpDependence.ResumeLayout(false);
            this.splDependence.Panel1.ResumeLayout(false);
            this.splDependence.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splDependence)).EndInit();
            this.splDependence.ResumeLayout(false);
            this.grpDependencePossible.ResumeLayout(false);
            this.tlpDependencePossible.ResumeLayout(false);
            this.tlpDependencePossible.PerformLayout();
            this.tsrDependencePossible.ResumeLayout(false);
            this.tsrDependencePossible.PerformLayout();
            this.splDependenceInner.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splDependenceInner)).EndInit();
            this.splDependenceInner.ResumeLayout(false);
            this.grpDependenceSelected.ResumeLayout(false);
            this.tlpDependenceSelected.ResumeLayout(false);
            this.tlpDependenceSelected.PerformLayout();
            this.tsrDependenceSelected.ResumeLayout(false);
            this.tsrDependenceSelected.PerformLayout();
            this.tsrMain.ResumeLayout(false);
            this.tsrMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpDependence;
        private System.Windows.Forms.SplitContainer splDependence;
        private System.Windows.Forms.GroupBox grpDependencePossible;
        private System.Windows.Forms.TableLayoutPanel tlpDependencePossible;
        private System.Windows.Forms.ToolStrip tsrDependencePossible;
        private System.Windows.Forms.ToolStripButton btnDependenceSelect;
        private System.Windows.Forms.ListBox lstDependencePossible;
        private System.Windows.Forms.SplitContainer splDependenceInner;
        private System.Windows.Forms.GroupBox grpDependenceSelected;
        private System.Windows.Forms.TableLayoutPanel tlpDependenceSelected;
        private System.Windows.Forms.ListBox lstDependenceSelected;
        private System.Windows.Forms.ToolStrip tsrDependenceSelected;
        private System.Windows.Forms.ToolStripButton btnDependenceCancel;
        private System.Windows.Forms.ToolStripButton btnHierarchy;
        private System.Windows.Forms.ToolStrip tsrMain;
        private System.Windows.Forms.ToolStripButton btnClose;
    }

}
