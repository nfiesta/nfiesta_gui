﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">Files with control labels for national and international version</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class LanguageFile
        {

            /// <summary>
            /// <para lang="cs">Data jazykové verze</para>
            /// <para lang="en">Language version data</para>
            /// </summary>
            public class Version
            {

                #region Private Fields

                /// <summary>
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </summary>
                private LanguageVersion languageVersion;

                /// <summary>
                /// <para lang="cs">Jazyk </para>
                /// <para lang="en">Language</para>
                /// </summary>
                private Language language;

                /// <summary>
                /// <para lang="cs">Slovník s popisky ovládacích prvků</para>
                /// <para lang="en">Dictionary with control labels</para>
                /// </summary>
                private Dictionary<string, Dictionary<string, string>> data;

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                private JsonSerializerOptions serializerOptions;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// <para lang="cs">Konstruktor objektu (default: angličtina)</para>
                /// <para lang="en">Object constructor (default: English)</para>
                /// </summary>
                public Version()
                {
                    SerializerOptions = null;
                    LanguageVersion = LanguageVersion.International;
                    Language = Language.EN;
                    Data = LanguageFile.Dictionary(
                        languageVersion: LanguageVersion.International,
                        languageFile: null);
                }

                /// <summary>
                /// <para lang="cs">Konstruktor objektu</para>
                /// <para lang="en">Object constructor</para>
                /// </summary>
                /// <param name="languageVersion">
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </param>
                /// <param name="language">
                /// <para lang="cs">Jazyk </para>
                /// <para lang="en">Language</para>
                /// </param>
                /// <param name="data">
                /// <para lang="cs">Slovník s popisky ovládacích prvků</para>
                /// <para lang="en">Dictionary with control labels</para>
                /// </param>
                public Version(
                    LanguageVersion languageVersion,
                    Language language,
                    Dictionary<string, Dictionary<string, string>> data)
                {
                    SerializerOptions = null;
                    LanguageVersion = languageVersion;
                    Language = language;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </summary>
                [JsonIgnore]
                public LanguageVersion LanguageVersion
                {
                    get
                    {
                        return languageVersion;
                    }
                    set
                    {
                        languageVersion = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Jazyk </para>
                /// <para lang="en">Language</para>
                /// </summary>
                public Language Language
                {
                    get
                    {
                        return language;
                    }
                    set
                    {
                        language = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Slovník s popisky ovládacích prvků</para>
                /// <para lang="en">Dictionary with control labels</para>
                /// </summary>
                public Dictionary<string, Dictionary<string, string>> Data
                {
                    get
                    {
                        return data ??
                            LanguageFile.Dictionary(
                            languageVersion: LanguageVersion,
                            languageFile: null);
                    }
                    set
                    {
                        data = value ??
                            LanguageFile.Dictionary(
                            languageVersion: LanguageVersion,
                            languageFile: null);
                    }
                }

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                [JsonIgnore]
                private JsonSerializerOptions SerializerOptions
                {
                    get
                    {
                        return serializerOptions ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = true
                            };
                    }
                    set
                    {
                        serializerOptions = value ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = true
                            };
                    }
                }

                /// <summary>
                /// <para lang="cs">Serializace objektu do formátu JSON</para>
                /// <para lang="en">Object serialization to JSON format</para>
                /// </summary>
                [JsonIgnore]
                public string JSON
                {
                    get
                    {
                        return JsonSerializer.Serialize(
                            value: this,
                            options: SerializerOptions);
                    }
                }

                #endregion Properties


                #region Static Methods

                /// <summary>
                /// <para lang="cs">Obnovení objektu z formátu JSON</para>
                /// <para lang="en">Restoring an object from JSON format</para>
                /// </summary>
                /// <param name="languageVersion">
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </param>
                /// <param name="json">
                /// <para lang="cs">JSON text</para>
                /// <para lang="en">JSON text</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">Data jazykové verze</para>
                /// <para lang="en">Language version data</para>
                /// </returns>
                public static Version Deserialize(
                    LanguageVersion languageVersion,
                    string json)
                {
                    try
                    {
                        Version version =
                            JsonSerializer.Deserialize<Version>(json: json);
                        version.LanguageVersion = languageVersion;
                        return version;
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            text: exception.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Exclamation);

                        return new Version()
                        { LanguageVersion = languageVersion };
                    }
                }

                /// <summary>
                /// <para lang="cs">Načte objekt ze souboru json</para>
                /// <para lang="en">Loads object from json file</para>
                /// </summary>
                /// <param name="languageVersion">
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </param>
                /// <param name="path">
                /// <para lang="cs">Cesta k json souboru</para>
                /// <para lang="en">Path to json file</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">Data jazykové verze</para>
                /// <para lang="en">Language version data</para>
                /// </returns>
                public static Version LoadFromFile(
                    LanguageVersion languageVersion,
                    string path)
                {
                    try
                    {
                        return Deserialize(
                            languageVersion: languageVersion,
                            json: File.ReadAllText(
                                path: path,
                                encoding: Encoding.UTF8));
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            text: exception.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);

                        return new Version()
                        { LanguageVersion = languageVersion };
                    }
                }

                #endregion Static Methods


                #region Methods

                /// <summary>
                /// <para lang="cs">Uloží objekt do souboru json</para>
                /// <para lang="en">Save object into json file</para>
                /// </summary>
                /// <param name="path">
                /// <para lang="cs">Cesta k json souboru</para>
                /// <para lang="en">Path to json file</para>
                /// </param>
                public void SaveToFile(string path)
                {
                    try
                    {
                        File.WriteAllText(
                            path: path,
                            contents: JSON,
                            encoding: Encoding.UTF8);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            text: exception.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                #endregion Methods

            }

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Národní verze</para>
            /// <para lang="en">National version</para>
            /// </summary>
            private Version nationalVersion;

            /// <summary>
            /// <para lang="cs">Mezinárodní verze</para>
            /// <para lang="en">International version</para>
            /// </summary>
            private Version internationalVersion;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu
            /// (výchozí verze EN/CS : použijí se popisky ze zdrojového kódu)</para>
            /// <para lang="en">Object constructor
            /// (default version EN/CS : using labels from source code)</para>
            /// </summary>
            public LanguageFile()
            {
                NationalVersion = new Version(
                    languageVersion: LanguageVersion.National,
                    language: Language.CS,
                    data: LanguageFile.Dictionary(
                            languageVersion: LanguageVersion.National,
                            languageFile: null));

                InternationalVersion = new Version(
                    languageVersion: LanguageVersion.International,
                    language: Language.EN,
                    data: LanguageFile.Dictionary(
                            languageVersion: LanguageVersion.International,
                            languageFile: null));
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Národní verze</para>
            /// <para lang="en">National version</para>
            /// </summary>
            public Version NationalVersion
            {
                get
                {
                    return nationalVersion ?? new Version(
                        languageVersion: LanguageVersion.National,
                        language: Language.CS,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null));
                }
                set
                {
                    nationalVersion = value ?? new Version(
                        languageVersion: LanguageVersion.National,
                        language: Language.CS,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null));
                }
            }

            /// <summary>
            /// <para lang="cs">Mezinárodní verze</para>
            /// <para lang="en">International version</para>
            /// </summary>
            public Version InternationalVersion
            {
                get
                {
                    return internationalVersion ?? new Version(
                        languageVersion: LanguageVersion.International,
                        language: Language.EN,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null));
                }
                set
                {
                    internationalVersion = value ?? new Version(
                        languageVersion: LanguageVersion.International,
                        language: Language.EN,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null));
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Zvolený jazyk</para>
            /// <para lang="en">Selected language</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">File with control labels for national and international version</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts in selected language</para>
            /// </returns>
            public static Dictionary<string, Dictionary<string, string>>
                Dictionary(
                    LanguageVersion languageVersion,
                    LanguageFile languageFile)
            {
                return
                    new Dictionary<string, Dictionary<string, string>>()
                    {
                        { $"{nameof(ClassificationRule<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            ClassificationRule<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ClassificationRule<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            ClassificationRule<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ClassificationRulePair<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            ClassificationRulePair<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ClassificationRulePair<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            ClassificationRulePair<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(SuperiorDomain<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             SuperiorDomain<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(SuperiorDomain<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             SuperiorDomain<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleCaption<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlClassificationRuleCaption<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleCaption<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlClassificationRuleCaption<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleData<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlClassificationRuleData<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleData<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlClassificationRuleData<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleEditor<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlClassificationRuleEditor<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleEditor<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlClassificationRuleEditor<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleSwitch<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlClassificationRuleSwitch<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlClassificationRuleSwitch<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlClassificationRuleSwitch<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlDependenceEditor<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlDependenceEditor<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlDependenceEditor<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlDependenceEditor<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlPanelRefYearSetList),
                             ControlPanelRefYearSetList.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTargetData),
                             ControlTargetData.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlTDCategory<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlTDCategory<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlTDCategory<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlTDCategory<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlTDDomainListBox<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlTDDomainListBox<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlTDDomainListBox<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlTDDomainListBox<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlTDDomainListBoxItem<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             ControlTDDomainListBoxItem<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(ControlTDDomainListBoxItem<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             ControlTDDomainListBoxItem<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTDLDsityInformation),
                             ControlTDLDsityInformation.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTDLDsityObject),
                             ControlTDLDsityObject.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTDSelectionAttributeCategory),
                             ControlTDSelectionAttributeCategory.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTDSelectionLDsityObjectGroup),
                             ControlTDSelectionLDsityObjectGroup.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTDSelectionPanelRefYearSetGroup),
                             ControlTDSelectionPanelRefYearSetGroup.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTDSelectionTargetVariableCore),
                             ControlTDSelectionTargetVariableCore.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(ControlTDSelectionTargetVariableDivision),
                             ControlTDSelectionTargetVariableDivision.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormCategoryEdit<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             FormCategoryEdit<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormCategoryEdit<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             FormCategoryEdit<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                         { $"{nameof(FormCategoryNew<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                             FormCategoryNew<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormCategoryNew<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             FormCategoryNew<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormClassificationRule<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            FormClassificationRule<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormClassificationRule<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             FormClassificationRule<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormDomainFromJson<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            FormDomainFromJson<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormDomainFromJson<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             FormDomainFromJson<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormDomainNew<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            FormDomainNew<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormDomainNew<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             FormDomainNew<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(FormErrorLog),
                             FormErrorLog.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                         { $"{nameof(FormHierarchy<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            FormHierarchy<TDAreaDomain, TDAreaDomainCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { $"{nameof(FormHierarchy<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             FormHierarchy<TDSubPopulation, TDSubPopulationCategory>.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(FormLDsityObjectGroupNew),
                             FormLDsityObjectGroupNew.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(FormLookupTable),
                             FormLookupTable.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(FormPanelRefYearSetGroupNew),
                             FormPanelRefYearSetGroupNew.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(FormTargetVariableCoreNew),
                             FormTargetVariableCoreNew.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(FormTargetVariableDivisionNew),
                             FormTargetVariableDivisionNew.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },

                        { nameof(FormTargetVariableLimit),
                             FormTargetVariableLimit.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) }
                    };
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </returns>
            public override string ToString()
            {
                return String.Concat(
                    $"National version language : ",
                    $"{LanguageList.Name(language: NationalVersion.Language)} {Environment.NewLine}",
                    $"International version language : ",
                    $"{LanguageList.Name(language: InternationalVersion.Language)} {Environment.NewLine}");
            }

            #endregion Methods

        }

    }
}