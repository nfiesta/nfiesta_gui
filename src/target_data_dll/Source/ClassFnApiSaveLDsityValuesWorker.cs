﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Pracovní vlákno pro výpočet lokálních hustot</para>
        /// <para lang="en">Worker thread for calculation local densities</para>
        /// </summary>
        internal class FnApiSaveLDsityValuesWorker
            : WorkerThread<FnApiSaveLDsityValuesTask, TFnGetRefYearSetToPanelMappingForGroup>
        {

            #region Methods

            /// <summary>
            /// <para lang="cs">Spuštění výpočtu lokálních hustot</para>
            /// <para lang="en">Starting the calculation of local densities</para>
            /// </summary>
            public override object Execute(ref ThreadEventArgs e)
            {
                if (ThreadConnection == null) { return null; }
                if (Task == null) { return null; }

                Task.SQL = TDFunctions.FnSaveLDsityValues
                    .GetCommandText(
                        refYearSetToPanelMappingIds: [Task.Element?.Id],
                        targetVariableId: Task.TargetVariable.Id,
                        threshold: Task.LocalDensityThreshold);

                Task.Result = TDFunctions.FnSaveLDsityValues
                    .Execute(
                        connection: ThreadConnection,
                        refYearSetToPanelMappingIds: [Task.Element?.Id],
                        targetVariableId: Task.TargetVariable.Id,
                        threshold: Task.LocalDensityThreshold);

                return Task.Result;
            }

            #endregion Methods

        }

    }
}