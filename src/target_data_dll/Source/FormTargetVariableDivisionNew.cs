﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro definici nové cílové proměnné (typ Division)</para>
    /// <para lang="en">Form for defining a new target variable (type Division)</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormTargetVariableDivisionNew
            : Form, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        public bool onEdit;


        /// <summary>
        /// <para lang="cs">Seznam cílových proměnných typu Core
        /// ve vybrané skupině cílových proměnných (předpokládá se že je jen jedna)</para>
        /// <para lang="en">List of target variable of type Core
        /// in selected target variable group (only one Core item is expected)</para>
        /// </summary>
        private TDTargetVariableList targetVariablesCore;

        /// <summary>
        /// <para lang="cs">Seznam příspěvků lokálních hustot
        /// pro cílovou proměnnou typu Core
        /// ve vybrané skupině cílových proměnných</para>
        /// <para lang="en">List of local density contributions
        /// for target variable of type Core
        /// in selected target variable group</para>
        /// </summary>
        private TDLDsityList ldsityContributionsCore;

        /// <summary>
        /// <para lang="cs">Seznam příspěvků lokálních hustot
        /// vrácený funkcí fn_get_ldsity_subpop4object
        /// pro vybraný objekt lokální hustoty</para>
        /// <para lang="en">List of local density contributions
        /// obtained from function fn_get_ldsity_subpop4object
        /// for selecteed local density object</para>
        /// </summary>
        private TDLDsityList fnGetLDsitySubPopForObject;

        /// <summary>
        /// <para lang="cs">Identifikační číslo nově vytvořené cílové proměnné (typ Division)</para>
        /// <para lang="en">New created target variable identifier (type Division)</para>
        /// </summary>
        private Nullable<int> newTargetVariableId;

        /// <summary>
        /// <para lang="cs">Seznam všech objektů lokálních hustot</para>
        /// <para lang="en">List of all local density objects</para>
        /// </summary>
        private TDLDsityObjectList ldsityObjects;

        private string msgLDsitiesIsEmpty = String.Empty;
        private string msgLDsitiesObjectTypesIsEmpty = String.Empty;
        private string msgLDsityVersionIsEmpty = String.Empty;
        private string msgFnSaveTargetVariableUnexpectedOutput = String.Empty;
        private string msgNoCommonUnit = String.Empty;
        private string msgNoCommonVersion = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="targetVariablesCore">
        /// <para lang="cs">Seznam cílových proměnných typu Core
        /// ve vybrané skupině cílových proměnných</para>
        /// <para lang="en">List of target variable of type Core
        /// in selected target variable group</para>
        /// </param>
        /// <param name="ldsityContributionsCore">
        /// <para lang="cs">Seznam příspěvků lokálních hustot
        /// pro cílové proměnné typu Core
        /// ve vybrané skupině cílových proměnných</para>
        /// <para lang="en">List of local density contributions
        /// for target variable of type Core
        /// in selected target variable group</para>
        /// </param>
        /// <param name="useCopy">
        /// <para lang="cs">Použít kopii vybrané cílové proměnné</para>
        /// <para lang="en">Use copy of selected target variable</para>
        /// </param>
        public FormTargetVariableDivisionNew(
            Control controlOwner,
            TDTargetVariableList targetVariablesCore,
            TDLDsityList ldsityContributionsCore,
            bool useCopy)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                targetVariablesCore: targetVariablesCore,
                ldsityContributionsCore: ldsityContributionsCore,
                useCopy: useCopy);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionTargetVariableDivision)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionTargetVariableDivision)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionTargetVariableDivision)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionTargetVariableDivision)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot
        /// ve vybrané skupině objektů lokálních hustot (vybrané na 1.formuláři)</para>
        /// <para lang="en">Local density objects
        /// in the selected group of local density object (selected on the 1st form)</para>
        /// </summary>
        private TDLDsityObjectList SelectedLocalDensityObjects
        {
            get
            {
                return ((ControlTDSelectionTargetVariableDivision)ControlOwner).SelectedLocalDensityObjects;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný objekt lokální hustoty
        /// ve vybrané skupině objektů lokálních hustot (vybrané na 1.formuláři) (read-only)</para>
        /// <para lang="en">Selected local density object
        /// in the selected group of local density object (selected on the 1st form) (read-only)</para>
        /// </summary>
        private TDLDsityObject SelectedLocalDensityObject
        {
            get
            {
                // Předpokládá se, že je pouze jeden
                return SelectedLocalDensityObjects.Items.FirstOrDefault();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam cílových proměnných typu Core
        /// ve vybrané skupině cílových proměnných (předpokládá se že je jen jedna)</para>
        /// <para lang="en">List of target variable of type Core
        /// in selected target variable group (only one Core item is expected)</para>
        /// </summary>
        private TDTargetVariableList TargetVariablesCore
        {
            get
            {
                if (targetVariablesCore == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(TargetVariablesCore)} must not be null.",
                        paramName: nameof(TargetVariablesCore));
                }

                if (targetVariablesCore.Items.Count == 0)
                {
                    throw new ArgumentException(
                        message: "Core target variable doesn't exist.",
                        paramName: nameof(TargetVariablesCore));
                }

                if (targetVariablesCore.Items.Count > 1)
                {
                    // Předpokládá se že je pouze jedna cílová proměnná
                    throw new ArgumentException(
                        message: String.Concat(
                                "Too many core target variables, only one was expected. ",
                                $"(targetVariableCount = {targetVariablesCore.Items.Count})"),
                        paramName: nameof(TargetVariablesCore));
                }

                return targetVariablesCore;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(TargetVariablesCore)} must not be null.",
                        paramName: nameof(TargetVariablesCore));
                }

                if (value.Items.Count == 0)
                {
                    throw new ArgumentException(
                        message: "Core target variable doesn't exist.",
                        paramName: nameof(TargetVariablesCore));
                }

                if (value.Items.Count > 1)
                {
                    // Předpokládá se že je pouze jedna cílová proměnná
                    throw new ArgumentException(
                        message: String.Concat(
                                "Too many core target variables, only one was expected. ",
                                $"(targetVariableCount = {value.Items.Count})"),
                        paramName: nameof(TargetVariablesCore));
                }

                targetVariablesCore = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná typu Core
        /// ve vybrané skupině cílových proměnných (read-only)</para>
        /// <para lang="en">Selected target target variable of type Core
        /// in selected target variable group (read-only)</para>
        /// </summary>
        private TDTargetVariable SelectedTargetVariableCore
        {
            get
            {
                return TargetVariablesCore.Items.FirstOrDefault();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný typ pro novou cílovou proměnnou (read-only)</para>
        /// <para lang="en">Selected target variable type for new target variable(read-only)</para>
        /// </summary>
        private TDStateOrChangeEnum StateOrChange
        {
            get
            {
                return SelectedTargetVariableCore.StateOrChangeValue;
            }
        }


        /// <summary>
        /// <para lang="cs">Seznam příspěvků lokálních hustot
        /// pro cílovou proměnnou typu Core
        /// ve vybrané skupině cílových proměnných</para>
        /// <para lang="en">List of local density contributions
        /// for target variable of type Core
        /// in selected target variable group</para>
        /// </summary>
        private List<TDLDsity> LDsityContributionsCore
        {
            get
            {
                return ldsityContributionsCore.Items
                    .Where(a => (a.TargetVariableId ?? 0) == SelectedTargetVariableCore.Id)
                    .ToList<TDLDsity>();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných příspěvků lokálních hustot
        /// pro novou cílovou proměnou typu Division (read-only)</para>
        /// <para lang="en">List of selected local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<TDLDsity> LDsityContributionsDivision
        {
            get
            {
                List<TDLDsity> result = [];

                foreach (ControlTDLDsityObject control in pnlLDsityContributions.Controls)
                {
                    result.AddRange(control.SelectedLDsities);
                }

                return result
                    .Where(a => a != null)
                    .ToList<TDLDsity>();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam identifikátorů vybraných příspěvků lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en">List of identifiers of selected local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> LDsityContributionIdentifiers
        {
            get
            {
                List<Nullable<int>> core = LDsityContributionsCore
                        .Select(a => (Nullable<int>)a.Id)
                        .ToList<Nullable<int>>();

                List<Nullable<int>> division = LDsityContributionsDivision
                        .Select(a => (Nullable<int>)a.Id)
                        .ToList<Nullable<int>>();

                core.AddRange(collection: division);

                return core;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam identifikátorů typů vybraných příspěvků lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en">List of identifiers of types of selected local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> LDsityObjectTypeIdentifiers
        {
            get
            {
                // Funkce fn_get_ldsity4object nevrací hodnotu ldsity_object_type_id,
                // sloupeček v tabulce je vždy null
                // Typ příspěvku lokální hustoty je v tomto případě 100 - core a 200 - division

                // Function fn_get_ldsity4object does not return value of ldsity_object_type_id,
                // this column in database table is always null
                // Type of local density contribution is in this case always of  100 - core a 200 - division

                List<Nullable<int>> core = LDsityContributionsCore
                        .Select(a => (Nullable<int>)TDLDsityObjectTypeEnum.Core)
                        .ToList<Nullable<int>>();

                List<Nullable<int>> division = LDsityContributionsDivision
                        .Select(a => (Nullable<int>)TDLDsityObjectTypeEnum.Division)
                        .ToList<Nullable<int>>();

                core.AddRange(collection: division);

                return core;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam identifikačních čísel verzí pro příspěvky lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en"> List of version identifiers for local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> LDsityObjectVersionIdentifiers
        {
            get
            {
                if (cboVersion.SelectedItem == null)
                {
                    return [];
                }

                TDVersion version = (TDVersion)cboVersion.SelectedItem;

                List<Nullable<int>> core =
                    LDsityContributionsCore
                        .Select(a => (Nullable<int>)version.Id)
                        .ToList<Nullable<int>>();

                List<Nullable<int>> division =
                    LDsityContributionsDivision
                        .Select(a => (Nullable<int>)version.Id)
                        .ToList<Nullable<int>>();

                core.AddRange(collection: division);

                return core;
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikační číslo nově vytvořené cílové proměnné (typ Division) (read-only)</para>
        /// <para lang="en">New created target variable identifier (type Division) (read-only)</para>
        /// </summary>
        public Nullable<int> NewTargetVariableId
        {
            get
            {
                return newTargetVariableId;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTargetVariableDivisionNew),              "Nová cílová proměnná plošného odhadu tříděného subpopulacemi" },
                        { nameof(btnCancel),                                  "Zrušit" },
                        { nameof(btnOK),                                      "Zapsat do databáze" },
                        { nameof(cboUnitOfMeasure),                           "LabelCs" },
                        { nameof(cboVersion),                                 "LabelCs" },
                        { nameof(grpTargetVariable),                          "Cílová proměnná:" },
                        { nameof(grpLDsityContributions),                     "Příspěvky lokálních hustot:" },
                        { nameof(lblLabelCsCaption),                          "Zkratka (národní):" },
                        { nameof(lblDescriptionCsCaption),                    "Popis (národní):" },
                        { nameof(lblLabelEnCaption),                          "Zkratka (anglická):" },
                        { nameof(lblDescriptionEnCaption),                    "Popis (anglický):" },
                        { nameof(lblUnitOfMeasure),                           "Jednotka:" },
                        { nameof(lblVersion),                                 "Verze příspěvků lokálních hustot:" },
                        { nameof(msgLDsitiesIsEmpty),                         "Nejsou zvoleny žádné příspěvky lokálních hustot." },
                        { nameof(msgLDsitiesObjectTypesIsEmpty),              "Nejsou zvoleny žádné typy objektů příspěvků lokálních hustot nebo obsahují NULL hodnoty." },
                        { nameof(msgLDsityVersionIsEmpty),                    "Není zvolena žádná verze pro příspěvky lokálních hustot." },
                        { nameof(msgFnSaveTargetVariableUnexpectedOutput),    "Uložená procedura fn_save_target_variable nevrátila identifikátor cílové proměnné." },
                        { nameof(msgNoCommonUnit),                            "Příspěvky lokálních hustot této cílové proměnné nemají stejnou jednotku!" },
                        { nameof(msgNoCommonVersion),                         "Příspěvky lokálních hustot této cílové proměnné nemají stejnou verzi!" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormTargetVariableDivisionNew),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTargetVariableDivisionNew),              "New target variable for an areal estimate classified by subpopulations" },
                        { nameof(btnCancel),                                  "Cancel" },
                        { nameof(btnOK),                                      "Write to database" },
                        { nameof(cboUnitOfMeasure),                           "LabelEn" },
                        { nameof(cboVersion),                                 "LabelEn" },
                        { nameof(grpTargetVariable),                          "Target variable:" },
                        { nameof(grpLDsityContributions),                     "Local density contributions:" },
                        { nameof(lblLabelCsCaption),                          "Label (national):" },
                        { nameof(lblDescriptionCsCaption),                    "Description (national):" },
                        { nameof(lblLabelEnCaption),                          "Label (English):" },
                        { nameof(lblDescriptionEnCaption),                    "Description (English):" },
                        { nameof(lblUnitOfMeasure),                           "Unit:" },
                        { nameof(lblVersion),                                 "Version of local density contributions:" },
                        { nameof(msgLDsitiesIsEmpty),                         "There are not selected any local density contributions."},
                        { nameof(msgLDsitiesObjectTypesIsEmpty),              "There are not selected any object types of local density contributions or they contain NULL values." },
                        { nameof(msgLDsityVersionIsEmpty),                    "There is not selected any version for local density contributions." },
                        { nameof(msgFnSaveTargetVariableUnexpectedOutput),    "Stored procedure fn_save_target_variable did not return target variable identifier." },
                        { nameof(msgNoCommonUnit),                            "The local density contributions of this target variable do not have the same unit!" },
                        { nameof(msgNoCommonVersion),                         "The positive local density contributions of this target variable do not have the same version!" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormTargetVariableDivisionNew),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="targetVariablesCore">
        /// <para lang="cs">Seznam cílových proměnných typu Core
        /// ve vybrané skupině cílových proměnných</para>
        /// <para lang="en">List of target variable of type Core
        /// in selected target variable group</para>
        /// </param>
        /// <param name="ldsityContributionsCore">
        /// <para lang="cs">Seznam příspěvků lokálních hustot
        /// pro cílové proměnné typu Core
        /// ve vybrané skupině cílových proměnných</para>
        /// <para lang="en">List of local density contributions
        /// for target variable of type Core
        /// in selected target variable group</para>
        /// </param>
        /// <param name="useCopy">
        /// <para lang="cs">Použít kopii vybrané cílové proměnné</para>
        /// <para lang="en">Use copy of selected target variable</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            TDTargetVariableList targetVariablesCore,
            TDLDsityList ldsityContributionsCore,
            bool useCopy)
        {
            ControlOwner = controlOwner;
            TargetVariablesCore = targetVariablesCore;

            if (ldsityContributionsCore != null)
            {
                this.ldsityContributionsCore = ldsityContributionsCore;
            }
            else
            {
                throw new ArgumentException(
                    message: $"Argument {nameof(ldsityContributionsCore)} must not be null.",
                    paramName: nameof(ldsityContributionsCore));
            }

            ldsityObjects
               = new TDLDsityObjectList(database: Database);

            onEdit = false;
            newTargetVariableId = null;

            LoadContent();

            InitializeTextBoxes();

            InitializeComboBoxUnitOfMeasure();

            InitializeComboBoxVersion();

            InitializeLabels();
            SetStyle();

            if (useCopy)
            {
                UseCopy();
            }
            else
            {
                InitializeLDsityObjectControls(
                    selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                    selectedVersion: (TDVersion)cboVersion.SelectedItem);
            }

            btnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: nameof(FormTargetVariableDivisionNew),
                    out string frmTargetVariableDivisionNewText)
                        ? frmTargetVariableDivisionNewText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(
                    key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(
                    key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            grpTargetVariable.Text =
                labels.TryGetValue(
                    key: nameof(grpTargetVariable),
                    out string grpTargetVariableText)
                        ? grpTargetVariableText
                        : String.Empty;

            grpLDsityContributions.Text =
                labels.TryGetValue(
                    key: nameof(grpLDsityContributions),
                    out string grpLDsityContributionsText)
                        ? grpLDsityContributionsText
                        : String.Empty;

            lblLabelCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelCsCaption),
                    out string lblLabelCsCaptionText)
                        ? lblLabelCsCaptionText
                        : String.Empty;

            lblDescriptionCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionCsCaption),
                    out string lblDescriptionCsCaptionText)
                        ? lblDescriptionCsCaptionText
                        : String.Empty;

            lblLabelEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelEnCaption),
                    out string lblLabelEnCaptionText)
                        ? lblLabelEnCaptionText
                        : String.Empty;

            lblDescriptionEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionEnCaption),
                    out string lblDescriptionEnCaptionText)
                        ? lblDescriptionEnCaptionText
                        : String.Empty;

            lblUnitOfMeasure.Text =
                labels.TryGetValue(
                    key: nameof(lblUnitOfMeasure),
                    out string lblUnitOfMeasureText)
                        ? lblUnitOfMeasureText
                        : String.Empty;

            lblVersion.Text =
                labels.TryGetValue(
                    key: nameof(lblVersion),
                    out string lblVersionText)
                        ? lblVersionText
                        : String.Empty;

            msgLDsitiesIsEmpty =
              labels.TryGetValue(key: nameof(msgLDsitiesIsEmpty),
                      out msgLDsitiesIsEmpty)
                          ? msgLDsitiesIsEmpty
                          : String.Empty;

            msgLDsitiesObjectTypesIsEmpty =
              labels.TryGetValue(key: nameof(msgLDsitiesObjectTypesIsEmpty),
                      out msgLDsitiesObjectTypesIsEmpty)
                          ? msgLDsitiesObjectTypesIsEmpty
                          : String.Empty;

            msgLDsityVersionIsEmpty =
              labels.TryGetValue(key: nameof(msgLDsityVersionIsEmpty),
                      out msgLDsityVersionIsEmpty)
                          ? msgLDsityVersionIsEmpty
                          : String.Empty;

            msgFnSaveTargetVariableUnexpectedOutput =
              labels.TryGetValue(key: nameof(msgFnSaveTargetVariableUnexpectedOutput),
                      out msgFnSaveTargetVariableUnexpectedOutput)
                          ? msgFnSaveTargetVariableUnexpectedOutput
                          : String.Empty;

            msgNoCommonUnit =
              labels.TryGetValue(key: nameof(msgNoCommonUnit),
                      out msgNoCommonUnit)
                          ? msgNoCommonUnit
                          : String.Empty;

            msgNoCommonVersion =
              labels.TryGetValue(key: nameof(msgNoCommonVersion),
                      out msgNoCommonVersion)
                          ? msgNoCommonVersion
                          : String.Empty;

            onEdit = true;

            cboUnitOfMeasure.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboUnitOfMeasure),
                    out string cboUnitOfMeasureDisplayMember)
                        ? cboUnitOfMeasureDisplayMember
                        : ID;

            cboVersion.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboVersion),
                    out string cboVersionDisplayMember)
                        ? cboVersionDisplayMember
                        : ID;

            onEdit = false;

            foreach (ControlTDLDsityObject control in pnlLDsityContributions.Controls)
            {
                control.InitializeLabels();
            }
        }


        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            TableLayoutPanelCellPosition pos;

            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;


            grpTargetVariable.Font = Setting.GroupBoxFont;
            grpTargetVariable.ForeColor = Setting.GroupBoxForeColor;

            grpLDsityContributions.Font = Setting.GroupBoxFont;
            grpLDsityContributions.ForeColor = Setting.GroupBoxForeColor;


            lblLabelCsCaption.Font = Setting.LabelFont;
            lblLabelCsCaption.ForeColor = Setting.LabelForeColor;

            lblLabelEnCaption.Font = Setting.LabelFont;
            lblLabelEnCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionCsCaption.Font = Setting.LabelFont;
            lblDescriptionCsCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionEnCaption.Font = Setting.LabelFont;
            lblDescriptionEnCaption.ForeColor = Setting.LabelForeColor;

            lblUnitOfMeasure.Font = Setting.LabelFont;
            lblUnitOfMeasure.ForeColor = Setting.LabelForeColor;

            lblVersion.Font = Setting.LabelFont;
            lblVersion.ForeColor = Setting.LabelForeColor;

            txtLabelCsValue.Font = Setting.TextBoxFont;
            txtLabelCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtLabelCsValue);
            txtLabelCsValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtLabelCsValue.Height) / 2), right: 0, bottom: 0);

            txtDescriptionCsValue.Font = Setting.TextBoxFont;
            txtDescriptionCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtDescriptionCsValue);
            txtDescriptionCsValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtDescriptionCsValue.Height) / 2), right: 0, bottom: 0);

            txtLabelEnValue.Font = Setting.TextBoxFont;
            txtLabelEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtLabelEnValue);
            txtLabelEnValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtLabelEnValue.Height) / 2), right: 0, bottom: 0);

            txtDescriptionEnValue.Font = Setting.TextBoxFont;
            txtDescriptionEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtDescriptionEnValue);
            txtDescriptionEnValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtDescriptionEnValue.Height) / 2), right: 0, bottom: 0);

            cboUnitOfMeasure.Font = Setting.TextBoxFont;
            cboUnitOfMeasure.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: cboUnitOfMeasure);
            cboUnitOfMeasure.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - cboUnitOfMeasure.Height) / 2), right: 0, bottom: 0);

            cboVersion.Font = Setting.TextBoxFont;
            cboVersion.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: cboVersion);
            cboVersion.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - cboVersion.Height) / 2), right: 0, bottom: 0);
        }


        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení ve formuláři</para>
        /// <para lang="en">Loading database table data and displaying it in the form</para>
        /// </summary>
        public void LoadContent()
        {
            ldsityObjects?.ReLoad();
            fnGetLDsitySubPopForObject =
                TDFunctions.FnGetLDsitySubPopForObject.Execute(
                    database: Database,
                    ldsityObjectId:
                        (SelectedLocalDensityObject == null) ?
                            (Nullable<int>)null :
                            SelectedLocalDensityObject.Id);
        }

        /// <summary>
        /// <para lang="cs">Použít kopii vybrané cílové proměnné</para>
        /// <para lang="en">Use copy of selected target variable</para>
        /// </summary>
        private void UseCopy()
        {
            TDLDsityList selectedLDsities =
                ((ControlTDSelectionTargetVariableDivision)ControlOwner)
                .GetLocalDensityForTargetVariableDivision();

            if ((selectedLDsities != null) && selectedLDsities.Items.Count != 0)
            {
                onEdit = true;

                #region Jednotka vybrané cílové proměnné
                int unitOfMeasureId = selectedLDsities.Items.First<TDLDsity>().UnitOfMeasureId;

                if (selectedLDsities.Items.Where(a => a.UnitOfMeasureId != unitOfMeasureId).Any())
                {
                    MessageBox.Show(
                        text: msgNoCommonUnit,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                TDUnitOfMeasure unitOfMeasureObject =
                    cboUnitOfMeasure.Items
                    .OfType<TDUnitOfMeasure>()
                    .Where(a => a.Id == unitOfMeasureId)
                    .FirstOrDefault<TDUnitOfMeasure>();

                if (unitOfMeasureObject != null)
                {
                    cboUnitOfMeasure.SelectedItem = unitOfMeasureObject;
                }
                #endregion Jednotka vybrané cílové proměnné

                #region Verze příspěvků lokálních hustot
                int versionId = selectedLDsities.Items.First<TDLDsity>().VersionId ?? 0;

                if (selectedLDsities.Items.Where(a => (a.VersionId ?? 0) != versionId).Any())
                {
                    MessageBox.Show(
                        text: msgNoCommonVersion,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                TDVersion versionObject =
                    cboVersion.Items
                    .OfType<TDVersion>()
                    .Where(a => a.Id == versionId)
                    .FirstOrDefault<TDVersion>();

                if (versionObject != null)
                {
                    cboVersion.SelectedItem = versionObject;
                }
                #endregion Verze příspěvků lokálních hustot

                onEdit = false;
            }

            InitializeLDsityObjectControls(
                    selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                    selectedVersion: (TDVersion)cboVersion.SelectedItem);

            if ((selectedLDsities != null) && selectedLDsities.Items.Count != 0)
            {
                onEdit = true;

                #region Příspěvky lokálních hustot
                foreach (ControlTDLDsityObject control in
                    pnlLDsityContributions.Controls.OfType<ControlTDLDsityObject>())
                {
                    control.SelectedLDsities = selectedLDsities.Items;
                }
                #endregion Příspěvky lokálních hustot

                onEdit = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace textů v TextBoxech Label a Description</para>
        /// <para lang="en">Initializing texts in TextBoxes Label and Description</para>
        /// </summary>
        private void InitializeTextBoxes()
        {
            // Nová cílová proměnná typu Division musí mít stejné jméno jako cílová proměnná typu Core
            // a nelze ji změnit:

            txtLabelCsValue.Text =
                (SelectedTargetVariableCore == null) ? String.Empty :
                SelectedTargetVariableCore.LabelCs;
            txtLabelCsValue.Enabled = false;

            txtDescriptionCsValue.Text =
                (SelectedTargetVariableCore == null) ? String.Empty :
                SelectedTargetVariableCore.DescriptionCs;
            txtDescriptionCsValue.Enabled = false;

            txtLabelEnValue.Text =
                (SelectedTargetVariableCore == null) ? String.Empty :
                SelectedTargetVariableCore.LabelEn;
            txtLabelEnValue.Enabled = false;

            txtDescriptionEnValue.Text =
                (SelectedTargetVariableCore == null) ? String.Empty :
                SelectedTargetVariableCore.DescriptionEn;
            txtDescriptionEnValue.Enabled = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ComboBox "Unit of measure"</para>
        /// <para lang="en">Initializing items in ComboBox "Unit of measure"</para>
        /// </summary>
        private void InitializeComboBoxUnitOfMeasure()
        {
            onEdit = true;

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            cboUnitOfMeasure.DataSource =
                Database.STargetData.CUnitOfMeasure.Items;

            cboUnitOfMeasure.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboUnitOfMeasure),
                    out string cboUnitOfMeasureDisplayMember)
                        ? cboUnitOfMeasureDisplayMember
                        : ID;

            if (cboUnitOfMeasure.Items.Count > 0)
            {
                cboUnitOfMeasure.SelectedIndex = 0;
            }

            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ComboBox "Version"</para>
        /// <para lang="en">Initializing items in ComboBox "Version"</para>
        /// </summary>
        private void InitializeComboBoxVersion()
        {
            onEdit = true;

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            cboVersion.DataSource =
                Database.STargetData.CVersion.Items;

            cboVersion.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboVersion),
                    out string cboVersionDisplayMember)
                        ? cboVersionDisplayMember
                        : ID;

            if (cboVersion.Items.Count > 0)
            {
                cboVersion.SelectedIndex = 0;
            }

            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku pro výběr příspěvků lokálních hustot</para>
        /// <para lang="en">Initializing user control for selection local density contributions</para>
        /// </summary>
        private void InitializeLDsityObjectControls(
            TDUnitOfMeasure selectedUnitOfMeasure,
            TDVersion selectedVersion)
        {
            TDLDsityList contributions;
            if ((selectedUnitOfMeasure == null) && (selectedVersion == null))
            {
                contributions = new TDLDsityList(
                    database: Database,
                    data: fnGetLDsitySubPopForObject.Data);
            }
            else if (selectedVersion == null)
            {
                contributions = new TDLDsityList(
                    database: Database,
                    rows: fnGetLDsitySubPopForObject.Data.AsEnumerable()
                            .Where(a => (a.Field<Nullable<int>>(columnName: TDLDsityList.ColUnitOfMeasureId.Name) ?? 0)
                                        == selectedUnitOfMeasure.Id));
            }
            else if (selectedUnitOfMeasure == null)
            {
                contributions = new TDLDsityList(
                    database: Database,
                    rows: fnGetLDsitySubPopForObject.Data.AsEnumerable()
                            .Where(a => (a.Field<Nullable<int>>(columnName: TDLDsityList.ColVersionId.Name) ?? 0)
                                        == selectedVersion.Id));
            }
            else
            {
                contributions = new TDLDsityList(
                    database: Database,
                    rows: fnGetLDsitySubPopForObject.Data.AsEnumerable()
                            .Where(a => (a.Field<Nullable<int>>(columnName: TDLDsityList.ColUnitOfMeasureId.Name) ?? 0)
                                        == selectedUnitOfMeasure.Id)
                            .Where(a => (a.Field<Nullable<int>>(columnName: TDLDsityList.ColVersionId.Name) ?? 0)
                                        == selectedVersion.Id));
            }

            List<TDLDsityObject> objs = ldsityObjects.Items
                .Where(a => contributions.Items.Select(b => b.LDsityObjectId).Distinct().Contains(value: a.Id))
                .ToList<TDLDsityObject>();

            pnlLDsityContributions.Controls.Clear();
            int top = 10;
            onEdit = true;
            foreach (TDLDsityObject obj in objs)
            {
                TDLDsityList ldsityList = new(
                    database: Database,
                    rows: contributions.Data.AsEnumerable()
                    .Where(a => (a.Field<Nullable<int>>(columnName: TDLDsityList.ColLDsityObjectId.Name) ?? 0)
                                        == obj.Id));
                ControlTDLDsityObject control = new(
                    controlOwner: this,
                    ldsityObject: obj,
                    ldsities: ldsityList)
                {
                    EnableEmptySelectedLDsities = true,
                    Left = 10,
                    Top = top,
                    Width = pnlLDsityContributions.Width - 40
                };
                // control.SelectionChanged += PositiveContributionsChanged;
                control.CreateControl();
                pnlLDsityContributions.Controls.Add(value: control);
                top = control.Top + control.Height + 5;
            }
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Uloží cílovou proměnnou do databázové tabulky</para>
        /// <para lang="en">It saves target variable into database table</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud bylo možné uložit cílovou proměnnou do databázové tabulky, jinak false</para>
        /// <para lang="en">It returns true if it was possible to save the target variable into database table, else false</para>
        /// </returns>
        private bool SaveTargetVariableDivision()
        {
            newTargetVariableId = null;

            string labelCs = txtLabelCsValue.Text.Trim();
            string descriptionCs = txtDescriptionCsValue.Text.Trim();
            string labelEn = txtLabelEnValue.Text.Trim();
            string descriptionEn = txtDescriptionEnValue.Text.Trim();

            // Není třeba kontrolovat LabelCs, DescriptionCs, LabelEn, DescriptionEn,
            // jsou pevně stanovené z cílové proměnné typu Core a uživatel je na tomto formuláři nemuže změnit.
            // There are not necessary checks on values LabelCs, DescriptionCs, LabelEn, DescriptionEn,
            // they are already given by names of Core target variable and user cannot change them on this form.

            if (LDsityContributionIdentifiers.Count == 0)
            {
                // Příspěvky lokálních hustot jsou prázdné
                // Contributions of local densities are empty
                MessageBox.Show(
                    text: msgLDsitiesIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (LDsityContributionsDivision.Count == 0)
            {
                // Příspěvky lokálních hustot jsou prázdné
                // Musí být vybraný alespoň jeden příspěvek lokální hustoty typu Division
                // jinak uložená procedura vytvoří druhou cílovou proměnou typu Core ve stejné skupině cílových proměnných
                // Skupiny se dvěmi cílovými proměnnými typu Core, nejsou povolené.

                // Contributions of local densities are empty
                MessageBox.Show(
                    text: msgLDsitiesIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if ((LDsityObjectTypeIdentifiers.Count == 0) ||
                (LDsityObjectTypeIdentifiers.Where(a => a == 0).Any()))
            {
                // Typy objektů příspěvků lokálních hustot jsou prázdné nebo obsahují null hodnoty
                // Object types of local density contributions are empty or contain null values
                MessageBox.Show(
                    text: msgLDsitiesObjectTypesIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (LDsityObjectVersionIdentifiers.Count == 0)
            {
                // Není vybrána verze pro příspěvky lokálních hustot
                // Version is not selected for contributions of local densities
                MessageBox.Show(
                    text: msgLDsityVersionIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            List<Nullable<int>> result =
                TDFunctions.FnSaveTargetVariable.Execute(
                    database: Database,
                    stateOrChange: (int)StateOrChange,
                    labelCs: labelCs,
                    descriptionCs: descriptionCs,
                    labelEn: labelEn,
                    descriptionEn: descriptionEn,
                    ldsityIds: LDsityContributionIdentifiers,
                    ldsityObjectTypeIds: LDsityObjectTypeIdentifiers,
                    versionIds: LDsityObjectVersionIdentifiers,
                    targetVariableIds: null);

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnSaveTargetVariable.CommandText,
                    caption: TDFunctions.FnSaveTargetVariable.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            if (Database.Postgres.ExceptionFlag)
            {
                return false;
            }

            if (result == null)
            {
                MessageBox.Show(
                    text: msgFnSaveTargetVariableUnexpectedOutput,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (result.Count == 0)
            {
                MessageBox.Show(
                    text: msgFnSaveTargetVariableUnexpectedOutput,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            newTargetVariableId = result[0];
            return true;
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události "Zavření formuláře"</para>
        /// <para lang="en">Handling the "Form closing" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (FormTargetVariableDivisionNew)</para>
        /// <para lang="en">Object that sends the event (FormTargetVariableDivisionNew)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void FormTargetVariableDivisionNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                e.Cancel = !SaveTargetVariableDivision();
            }
            else
            {
                e.Cancel = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna výběru jednotky"</para>
        /// <para lang="en">Handling the "Change of unit selection" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CboUnitOfMeasure_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
                InitializeLDsityObjectControls(
                    selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                    selectedVersion: (TDVersion)cboVersion.SelectedItem);
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna výběru verze pro příspěvky lokálních hustot"</para>
        /// <para lang="en">Handling the "Change of the version selection for local density contributions" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CboVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
                InitializeLDsityObjectControls(
                    selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                    selectedVersion: (TDVersion)cboVersion.SelectedItem);
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna velikosti panelu s příspěvky lokálních hustot"
        /// Při změně velikosti panelu s ovládacími prvky pro výběr příspěvků lokálních hustot
        /// zarovná velikost ovládacího prvku do velikosti panelu</para>
        /// <para lang="en">Handling the "Resizing the panel with local density contributions" event
        /// When resizing the panel with controls for selection of local density contributions,
        /// the control size aligns to the panel size</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Panel)</para>
        /// <para lang="en">Object that sends the event (Panel)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void PnlLDsityContributions_Resize(object sender, EventArgs e)
        {
            foreach (ControlTDLDsityObject control in pnlLDsityContributions.Controls)
            {
                control.Width = pnlLDsityContributions.Width - 40;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zrušit"</para>
        /// <para lang="en">Handling the "Cancel" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zapsat do databáze"</para>
        /// <para lang="en">Handling the "Write to database" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        #endregion Event Handlers

    }

}