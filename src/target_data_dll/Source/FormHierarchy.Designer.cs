﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class FormHierarchyDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            splWorkSpace = new System.Windows.Forms.SplitContainer();
            tlpHeader = new System.Windows.Forms.TableLayoutPanel();
            pnlInferior = new System.Windows.Forms.Panel();
            cboInferior = new System.Windows.Forms.ComboBox();
            lblInferiorValue = new System.Windows.Forms.Label();
            pnlLblInferior = new System.Windows.Forms.Panel();
            lblInferiorCaption = new System.Windows.Forms.Label();
            pnlSuperior = new System.Windows.Forms.Panel();
            cboSuperior = new System.Windows.Forms.ComboBox();
            lblSuperiorValue = new System.Windows.Forms.Label();
            pnlLblSuperior = new System.Windows.Forms.Panel();
            lblSuperiorCaption = new System.Windows.Forms.Label();
            pnlWorkSpace = new System.Windows.Forms.Panel();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splWorkSpace).BeginInit();
            splWorkSpace.Panel1.SuspendLayout();
            splWorkSpace.Panel2.SuspendLayout();
            splWorkSpace.SuspendLayout();
            tlpHeader.SuspendLayout();
            pnlInferior.SuspendLayout();
            pnlLblInferior.SuspendLayout();
            pnlSuperior.SuspendLayout();
            pnlLblSuperior.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(944, 501);
            pnlMain.TabIndex = 4;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(splWorkSpace, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 13;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // splWorkSpace
            // 
            splWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            splWorkSpace.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splWorkSpace.IsSplitterFixed = true;
            splWorkSpace.Location = new System.Drawing.Point(0, 0);
            splWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            splWorkSpace.Name = "splWorkSpace";
            splWorkSpace.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splWorkSpace.Panel1
            // 
            splWorkSpace.Panel1.Controls.Add(tlpHeader);
            // 
            // splWorkSpace.Panel2
            // 
            splWorkSpace.Panel2.Controls.Add(pnlWorkSpace);
            splWorkSpace.Size = new System.Drawing.Size(944, 461);
            splWorkSpace.SplitterDistance = 60;
            splWorkSpace.SplitterWidth = 1;
            splWorkSpace.TabIndex = 5;
            // 
            // tlpHeader
            // 
            tlpHeader.ColumnCount = 2;
            tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpHeader.Controls.Add(pnlInferior, 1, 1);
            tlpHeader.Controls.Add(pnlLblInferior, 0, 1);
            tlpHeader.Controls.Add(pnlSuperior, 1, 0);
            tlpHeader.Controls.Add(pnlLblSuperior, 0, 0);
            tlpHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpHeader.Location = new System.Drawing.Point(0, 0);
            tlpHeader.Margin = new System.Windows.Forms.Padding(0);
            tlpHeader.Name = "tlpHeader";
            tlpHeader.RowCount = 3;
            tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpHeader.Size = new System.Drawing.Size(944, 60);
            tlpHeader.TabIndex = 0;
            // 
            // pnlInferior
            // 
            pnlInferior.Controls.Add(cboInferior);
            pnlInferior.Controls.Add(lblInferiorValue);
            pnlInferior.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInferior.Location = new System.Drawing.Point(150, 30);
            pnlInferior.Margin = new System.Windows.Forms.Padding(0);
            pnlInferior.Name = "pnlInferior";
            pnlInferior.Padding = new System.Windows.Forms.Padding(3);
            pnlInferior.Size = new System.Drawing.Size(794, 30);
            pnlInferior.TabIndex = 3;
            // 
            // cboInferior
            // 
            cboInferior.Dock = System.Windows.Forms.DockStyle.Fill;
            cboInferior.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboInferior.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            cboInferior.FormattingEnabled = true;
            cboInferior.Location = new System.Drawing.Point(153, 3);
            cboInferior.Name = "cboInferior";
            cboInferior.Size = new System.Drawing.Size(638, 23);
            cboInferior.TabIndex = 3;
            // 
            // lblInferiorValue
            // 
            lblInferiorValue.Dock = System.Windows.Forms.DockStyle.Left;
            lblInferiorValue.Location = new System.Drawing.Point(3, 3);
            lblInferiorValue.Margin = new System.Windows.Forms.Padding(0);
            lblInferiorValue.Name = "lblInferiorValue";
            lblInferiorValue.Size = new System.Drawing.Size(150, 24);
            lblInferiorValue.TabIndex = 2;
            lblInferiorValue.Text = "lblInferiorValue";
            lblInferiorValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlLblInferior
            // 
            pnlLblInferior.Controls.Add(lblInferiorCaption);
            pnlLblInferior.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLblInferior.Location = new System.Drawing.Point(0, 30);
            pnlLblInferior.Margin = new System.Windows.Forms.Padding(0);
            pnlLblInferior.Name = "pnlLblInferior";
            pnlLblInferior.Padding = new System.Windows.Forms.Padding(3);
            pnlLblInferior.Size = new System.Drawing.Size(150, 30);
            pnlLblInferior.TabIndex = 2;
            // 
            // lblInferiorCaption
            // 
            lblInferiorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblInferiorCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblInferiorCaption.Location = new System.Drawing.Point(3, 3);
            lblInferiorCaption.Margin = new System.Windows.Forms.Padding(0);
            lblInferiorCaption.Name = "lblInferiorCaption";
            lblInferiorCaption.Size = new System.Drawing.Size(144, 24);
            lblInferiorCaption.TabIndex = 1;
            lblInferiorCaption.Text = "lblInferiorCaption";
            lblInferiorCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlSuperior
            // 
            pnlSuperior.Controls.Add(cboSuperior);
            pnlSuperior.Controls.Add(lblSuperiorValue);
            pnlSuperior.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSuperior.Location = new System.Drawing.Point(150, 0);
            pnlSuperior.Margin = new System.Windows.Forms.Padding(0);
            pnlSuperior.Name = "pnlSuperior";
            pnlSuperior.Padding = new System.Windows.Forms.Padding(3);
            pnlSuperior.Size = new System.Drawing.Size(794, 30);
            pnlSuperior.TabIndex = 1;
            // 
            // cboSuperior
            // 
            cboSuperior.Dock = System.Windows.Forms.DockStyle.Fill;
            cboSuperior.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboSuperior.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            cboSuperior.FormattingEnabled = true;
            cboSuperior.Location = new System.Drawing.Point(153, 3);
            cboSuperior.Name = "cboSuperior";
            cboSuperior.Size = new System.Drawing.Size(638, 23);
            cboSuperior.TabIndex = 2;
            // 
            // lblSuperiorValue
            // 
            lblSuperiorValue.Dock = System.Windows.Forms.DockStyle.Left;
            lblSuperiorValue.Location = new System.Drawing.Point(3, 3);
            lblSuperiorValue.Margin = new System.Windows.Forms.Padding(0);
            lblSuperiorValue.Name = "lblSuperiorValue";
            lblSuperiorValue.Size = new System.Drawing.Size(150, 24);
            lblSuperiorValue.TabIndex = 1;
            lblSuperiorValue.Text = "lblSuperiorValue";
            lblSuperiorValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlLblSuperior
            // 
            pnlLblSuperior.Controls.Add(lblSuperiorCaption);
            pnlLblSuperior.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLblSuperior.Location = new System.Drawing.Point(0, 0);
            pnlLblSuperior.Margin = new System.Windows.Forms.Padding(0);
            pnlLblSuperior.Name = "pnlLblSuperior";
            pnlLblSuperior.Padding = new System.Windows.Forms.Padding(3);
            pnlLblSuperior.Size = new System.Drawing.Size(150, 30);
            pnlLblSuperior.TabIndex = 0;
            // 
            // lblSuperiorCaption
            // 
            lblSuperiorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSuperiorCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblSuperiorCaption.Location = new System.Drawing.Point(3, 3);
            lblSuperiorCaption.Margin = new System.Windows.Forms.Padding(0);
            lblSuperiorCaption.Name = "lblSuperiorCaption";
            lblSuperiorCaption.Size = new System.Drawing.Size(144, 24);
            lblSuperiorCaption.TabIndex = 0;
            lblSuperiorCaption.Text = "lblSuperiorCaption";
            lblSuperiorCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlWorkSpace
            // 
            pnlWorkSpace.AutoScroll = true;
            pnlWorkSpace.BackColor = System.Drawing.SystemColors.Window;
            pnlWorkSpace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlWorkSpace.Location = new System.Drawing.Point(0, 0);
            pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            pnlWorkSpace.Name = "pnlWorkSpace";
            pnlWorkSpace.Size = new System.Drawing.Size(944, 400);
            pnlWorkSpace.TabIndex = 11;
            // 
            // FormHierarchyDesign
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(pnlMain);
            DoubleBuffered = true;
            Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.Color.Black;
            Name = "FormHierarchyDesign";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormHierarchy";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            splWorkSpace.Panel1.ResumeLayout(false);
            splWorkSpace.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splWorkSpace).EndInit();
            splWorkSpace.ResumeLayout(false);
            tlpHeader.ResumeLayout(false);
            pnlInferior.ResumeLayout(false);
            pnlLblInferior.ResumeLayout(false);
            pnlSuperior.ResumeLayout(false);
            pnlLblSuperior.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.SplitContainer splWorkSpace;
        private System.Windows.Forms.TableLayoutPanel tlpHeader;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.Panel pnlInferior;
        private System.Windows.Forms.Panel pnlLblInferior;
        private System.Windows.Forms.Label lblInferiorCaption;
        private System.Windows.Forms.Panel pnlSuperior;
        private System.Windows.Forms.Panel pnlLblSuperior;
        private System.Windows.Forms.Label lblSuperiorCaption;
        private System.Windows.Forms.ComboBox cboInferior;
        private System.Windows.Forms.Label lblInferiorValue;
        private System.Windows.Forms.ComboBox cboSuperior;
        private System.Windows.Forms.Label lblSuperiorValue;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
    }

}