﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class FormTargetVariableCoreNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            splMain = new System.Windows.Forms.SplitContainer();
            grpTargetVariable = new System.Windows.Forms.GroupBox();
            pnlTargetVariable = new System.Windows.Forms.Panel();
            tlpTargetVariable = new System.Windows.Forms.TableLayoutPanel();
            cboUnitOfMeasure = new System.Windows.Forms.ComboBox();
            cboVersionNegative = new System.Windows.Forms.ComboBox();
            lblVersionNegative = new System.Windows.Forms.Label();
            txtDescriptionEnValue = new System.Windows.Forms.TextBox();
            txtLabelEnValue = new System.Windows.Forms.TextBox();
            txtDescriptionCsValue = new System.Windows.Forms.TextBox();
            lblLabelCsCaption = new System.Windows.Forms.Label();
            lblDescriptionCsCaption = new System.Windows.Forms.Label();
            lblLabelEnCaption = new System.Windows.Forms.Label();
            lblDescriptionEnCaption = new System.Windows.Forms.Label();
            txtLabelCsValue = new System.Windows.Forms.TextBox();
            lblVersionPositive = new System.Windows.Forms.Label();
            cboVersionPositive = new System.Windows.Forms.ComboBox();
            lblStateOrChangeCaption = new System.Windows.Forms.Label();
            pnlStateOrChange = new System.Windows.Forms.Panel();
            lblUnitOfMeasure = new System.Windows.Forms.Label();
            splPositiveNegative = new System.Windows.Forms.SplitContainer();
            grpLDsityObjectsPositive = new System.Windows.Forms.GroupBox();
            pnlLDsityObjectsPositive = new System.Windows.Forms.Panel();
            grpLDsityObjectsNegative = new System.Windows.Forms.GroupBox();
            pnlLDsityObjectsNegative = new System.Windows.Forms.Panel();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            grpTargetVariable.SuspendLayout();
            pnlTargetVariable.SuspendLayout();
            tlpTargetVariable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splPositiveNegative).BeginInit();
            splPositiveNegative.Panel1.SuspendLayout();
            splPositiveNegative.Panel2.SuspendLayout();
            splPositiveNegative.SuspendLayout();
            grpLDsityObjectsPositive.SuspendLayout();
            grpLDsityObjectsNegative.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(944, 501);
            pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(splMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 14;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += BtnCancel_Click;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += BtnOK_Click;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splMain.Location = new System.Drawing.Point(0, 0);
            splMain.Margin = new System.Windows.Forms.Padding(0);
            splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(grpTargetVariable);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(splPositiveNegative);
            splMain.Size = new System.Drawing.Size(944, 461);
            splMain.SplitterDistance = 400;
            splMain.TabIndex = 5;
            // 
            // grpTargetVariable
            // 
            grpTargetVariable.Controls.Add(pnlTargetVariable);
            grpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            grpTargetVariable.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpTargetVariable.ForeColor = System.Drawing.Color.MediumBlue;
            grpTargetVariable.Location = new System.Drawing.Point(0, 0);
            grpTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            grpTargetVariable.Name = "grpTargetVariable";
            grpTargetVariable.Padding = new System.Windows.Forms.Padding(5);
            grpTargetVariable.Size = new System.Drawing.Size(400, 461);
            grpTargetVariable.TabIndex = 3;
            grpTargetVariable.TabStop = false;
            grpTargetVariable.Text = "grpTargetVariable";
            // 
            // pnlTargetVariable
            // 
            pnlTargetVariable.Controls.Add(tlpTargetVariable);
            pnlTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTargetVariable.Location = new System.Drawing.Point(5, 25);
            pnlTargetVariable.Name = "pnlTargetVariable";
            pnlTargetVariable.Size = new System.Drawing.Size(390, 431);
            pnlTargetVariable.TabIndex = 0;
            // 
            // tlpTargetVariable
            // 
            tlpTargetVariable.ColumnCount = 2;
            tlpTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpTargetVariable.Controls.Add(cboUnitOfMeasure, 1, 6);
            tlpTargetVariable.Controls.Add(cboVersionNegative, 1, 8);
            tlpTargetVariable.Controls.Add(lblVersionNegative, 0, 8);
            tlpTargetVariable.Controls.Add(txtDescriptionEnValue, 1, 4);
            tlpTargetVariable.Controls.Add(txtLabelEnValue, 1, 3);
            tlpTargetVariable.Controls.Add(txtDescriptionCsValue, 1, 1);
            tlpTargetVariable.Controls.Add(lblLabelCsCaption, 0, 0);
            tlpTargetVariable.Controls.Add(lblDescriptionCsCaption, 0, 1);
            tlpTargetVariable.Controls.Add(lblLabelEnCaption, 0, 3);
            tlpTargetVariable.Controls.Add(lblDescriptionEnCaption, 0, 4);
            tlpTargetVariable.Controls.Add(txtLabelCsValue, 1, 0);
            tlpTargetVariable.Controls.Add(lblVersionPositive, 0, 7);
            tlpTargetVariable.Controls.Add(cboVersionPositive, 1, 7);
            tlpTargetVariable.Controls.Add(lblStateOrChangeCaption, 0, 9);
            tlpTargetVariable.Controls.Add(pnlStateOrChange, 1, 9);
            tlpTargetVariable.Controls.Add(lblUnitOfMeasure, 0, 6);
            tlpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpTargetVariable.Location = new System.Drawing.Point(0, 0);
            tlpTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            tlpTargetVariable.Name = "tlpTargetVariable";
            tlpTargetVariable.RowCount = 10;
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpTargetVariable.Size = new System.Drawing.Size(390, 431);
            tlpTargetVariable.TabIndex = 3;
            // 
            // cboUnitOfMeasure
            // 
            cboUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            cboUnitOfMeasure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboUnitOfMeasure.Font = new System.Drawing.Font("Segoe UI", 9F);
            cboUnitOfMeasure.ForeColor = System.Drawing.Color.Black;
            cboUnitOfMeasure.FormattingEnabled = true;
            cboUnitOfMeasure.Location = new System.Drawing.Point(153, 259);
            cboUnitOfMeasure.Name = "cboUnitOfMeasure";
            cboUnitOfMeasure.Size = new System.Drawing.Size(234, 23);
            cboUnitOfMeasure.TabIndex = 15;
            cboUnitOfMeasure.SelectedIndexChanged += CboUnitOfMeasure_SelectedIndexChanged;
            // 
            // cboVersionNegative
            // 
            cboVersionNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            cboVersionNegative.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboVersionNegative.Font = new System.Drawing.Font("Segoe UI", 9F);
            cboVersionNegative.ForeColor = System.Drawing.Color.Black;
            cboVersionNegative.FormattingEnabled = true;
            cboVersionNegative.Location = new System.Drawing.Point(153, 319);
            cboVersionNegative.Name = "cboVersionNegative";
            cboVersionNegative.Size = new System.Drawing.Size(234, 23);
            cboVersionNegative.TabIndex = 13;
            cboVersionNegative.SelectedIndexChanged += CboVersionNegative_SelectedIndexChanged;
            // 
            // lblVersionNegative
            // 
            lblVersionNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            lblVersionNegative.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblVersionNegative.ForeColor = System.Drawing.Color.MediumBlue;
            lblVersionNegative.Location = new System.Drawing.Point(0, 316);
            lblVersionNegative.Margin = new System.Windows.Forms.Padding(0);
            lblVersionNegative.Name = "lblVersionNegative";
            lblVersionNegative.Size = new System.Drawing.Size(150, 30);
            lblVersionNegative.TabIndex = 11;
            lblVersionNegative.Text = "lblVersionNegative";
            lblVersionNegative.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescriptionEnValue
            // 
            txtDescriptionEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEnValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtDescriptionEnValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionEnValue.Location = new System.Drawing.Point(150, 158);
            txtDescriptionEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEnValue.Multiline = true;
            txtDescriptionEnValue.Name = "txtDescriptionEnValue";
            txtDescriptionEnValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionEnValue.Size = new System.Drawing.Size(240, 83);
            txtDescriptionEnValue.TabIndex = 7;
            // 
            // txtLabelEnValue
            // 
            txtLabelEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtLabelEnValue.ForeColor = System.Drawing.Color.Black;
            txtLabelEnValue.Location = new System.Drawing.Point(150, 128);
            txtLabelEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEnValue.Name = "txtLabelEnValue";
            txtLabelEnValue.Size = new System.Drawing.Size(240, 16);
            txtLabelEnValue.TabIndex = 6;
            // 
            // txtDescriptionCsValue
            // 
            txtDescriptionCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCsValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtDescriptionCsValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionCsValue.Location = new System.Drawing.Point(150, 30);
            txtDescriptionCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCsValue.Multiline = true;
            txtDescriptionCsValue.Name = "txtDescriptionCsValue";
            txtDescriptionCsValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionCsValue.Size = new System.Drawing.Size(240, 83);
            txtDescriptionCsValue.TabIndex = 5;
            // 
            // lblLabelCsCaption
            // 
            lblLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblLabelCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCsCaption.Location = new System.Drawing.Point(0, 0);
            lblLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCsCaption.Name = "lblLabelCsCaption";
            lblLabelCsCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelCsCaption.TabIndex = 0;
            lblLabelCsCaption.Text = "lblLabelCsCaption";
            lblLabelCsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCsCaption
            // 
            lblDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblDescriptionCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCsCaption.Location = new System.Drawing.Point(0, 30);
            lblDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCsCaption.Name = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.Size = new System.Drawing.Size(150, 83);
            lblDescriptionCsCaption.TabIndex = 1;
            lblDescriptionCsCaption.Text = "lblDescriptionCsCaption";
            // 
            // lblLabelEnCaption
            // 
            lblLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblLabelEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelEnCaption.Location = new System.Drawing.Point(0, 128);
            lblLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEnCaption.Name = "lblLabelEnCaption";
            lblLabelEnCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelEnCaption.TabIndex = 2;
            lblLabelEnCaption.Text = "lblLabelEnCaption";
            lblLabelEnCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionEnCaption
            // 
            lblDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblDescriptionEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionEnCaption.Location = new System.Drawing.Point(0, 158);
            lblDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEnCaption.Name = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.Size = new System.Drawing.Size(150, 83);
            lblDescriptionEnCaption.TabIndex = 3;
            lblDescriptionEnCaption.Text = "lblDescriptionEnCaption";
            // 
            // txtLabelCsValue
            // 
            txtLabelCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCsValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtLabelCsValue.ForeColor = System.Drawing.Color.Black;
            txtLabelCsValue.Location = new System.Drawing.Point(150, 0);
            txtLabelCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCsValue.Name = "txtLabelCsValue";
            txtLabelCsValue.Size = new System.Drawing.Size(240, 16);
            txtLabelCsValue.TabIndex = 4;
            // 
            // lblVersionPositive
            // 
            lblVersionPositive.Dock = System.Windows.Forms.DockStyle.Fill;
            lblVersionPositive.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblVersionPositive.ForeColor = System.Drawing.Color.MediumBlue;
            lblVersionPositive.Location = new System.Drawing.Point(0, 286);
            lblVersionPositive.Margin = new System.Windows.Forms.Padding(0);
            lblVersionPositive.Name = "lblVersionPositive";
            lblVersionPositive.Size = new System.Drawing.Size(150, 30);
            lblVersionPositive.TabIndex = 10;
            lblVersionPositive.Text = "lblVersionPositive";
            lblVersionPositive.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboVersionPositive
            // 
            cboVersionPositive.Dock = System.Windows.Forms.DockStyle.Fill;
            cboVersionPositive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboVersionPositive.Font = new System.Drawing.Font("Segoe UI", 9F);
            cboVersionPositive.ForeColor = System.Drawing.Color.Black;
            cboVersionPositive.FormattingEnabled = true;
            cboVersionPositive.Location = new System.Drawing.Point(153, 289);
            cboVersionPositive.Name = "cboVersionPositive";
            cboVersionPositive.Size = new System.Drawing.Size(234, 23);
            cboVersionPositive.TabIndex = 12;
            cboVersionPositive.SelectedIndexChanged += CboVersionPositive_SelectedIndexChanged;
            // 
            // lblStateOrChangeCaption
            // 
            lblStateOrChangeCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblStateOrChangeCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblStateOrChangeCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblStateOrChangeCaption.Location = new System.Drawing.Point(0, 346);
            lblStateOrChangeCaption.Margin = new System.Windows.Forms.Padding(0);
            lblStateOrChangeCaption.Name = "lblStateOrChangeCaption";
            lblStateOrChangeCaption.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            lblStateOrChangeCaption.Size = new System.Drawing.Size(150, 85);
            lblStateOrChangeCaption.TabIndex = 9;
            lblStateOrChangeCaption.Text = "lblStateOrChangeCaption";
            // 
            // pnlStateOrChange
            // 
            pnlStateOrChange.AutoScroll = true;
            pnlStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlStateOrChange.Font = new System.Drawing.Font("Segoe UI", 9F);
            pnlStateOrChange.ForeColor = System.Drawing.Color.Black;
            pnlStateOrChange.Location = new System.Drawing.Point(150, 346);
            pnlStateOrChange.Margin = new System.Windows.Forms.Padding(0);
            pnlStateOrChange.Name = "pnlStateOrChange";
            pnlStateOrChange.Size = new System.Drawing.Size(240, 85);
            pnlStateOrChange.TabIndex = 8;
            pnlStateOrChange.Resize += PnlStateOrChange_Resize;
            // 
            // lblUnitOfMeasure
            // 
            lblUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            lblUnitOfMeasure.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblUnitOfMeasure.ForeColor = System.Drawing.Color.MediumBlue;
            lblUnitOfMeasure.Location = new System.Drawing.Point(0, 256);
            lblUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            lblUnitOfMeasure.Name = "lblUnitOfMeasure";
            lblUnitOfMeasure.Size = new System.Drawing.Size(150, 30);
            lblUnitOfMeasure.TabIndex = 14;
            lblUnitOfMeasure.Text = "lblUnitOfMeasure";
            lblUnitOfMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splPositiveNegative
            // 
            splPositiveNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            splPositiveNegative.Location = new System.Drawing.Point(0, 0);
            splPositiveNegative.Name = "splPositiveNegative";
            // 
            // splPositiveNegative.Panel1
            // 
            splPositiveNegative.Panel1.Controls.Add(grpLDsityObjectsPositive);
            // 
            // splPositiveNegative.Panel2
            // 
            splPositiveNegative.Panel2.Controls.Add(grpLDsityObjectsNegative);
            splPositiveNegative.Size = new System.Drawing.Size(540, 461);
            splPositiveNegative.SplitterDistance = 270;
            splPositiveNegative.TabIndex = 2;
            splPositiveNegative.Visible = false;
            // 
            // grpLDsityObjectsPositive
            // 
            grpLDsityObjectsPositive.Controls.Add(pnlLDsityObjectsPositive);
            grpLDsityObjectsPositive.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLDsityObjectsPositive.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpLDsityObjectsPositive.ForeColor = System.Drawing.Color.MediumBlue;
            grpLDsityObjectsPositive.Location = new System.Drawing.Point(0, 0);
            grpLDsityObjectsPositive.Margin = new System.Windows.Forms.Padding(0);
            grpLDsityObjectsPositive.Name = "grpLDsityObjectsPositive";
            grpLDsityObjectsPositive.Padding = new System.Windows.Forms.Padding(5);
            grpLDsityObjectsPositive.Size = new System.Drawing.Size(270, 461);
            grpLDsityObjectsPositive.TabIndex = 1;
            grpLDsityObjectsPositive.TabStop = false;
            grpLDsityObjectsPositive.Text = "grpLDsityObjectsPositive";
            // 
            // pnlLDsityObjectsPositive
            // 
            pnlLDsityObjectsPositive.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLDsityObjectsPositive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            pnlLDsityObjectsPositive.ForeColor = System.Drawing.SystemColors.ControlText;
            pnlLDsityObjectsPositive.Location = new System.Drawing.Point(5, 25);
            pnlLDsityObjectsPositive.Margin = new System.Windows.Forms.Padding(0);
            pnlLDsityObjectsPositive.Name = "pnlLDsityObjectsPositive";
            pnlLDsityObjectsPositive.Size = new System.Drawing.Size(260, 431);
            pnlLDsityObjectsPositive.TabIndex = 7;
            pnlLDsityObjectsPositive.Resize += PnlLDsityObjectsPositive_Resize;
            // 
            // grpLDsityObjectsNegative
            // 
            grpLDsityObjectsNegative.Controls.Add(pnlLDsityObjectsNegative);
            grpLDsityObjectsNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLDsityObjectsNegative.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpLDsityObjectsNegative.ForeColor = System.Drawing.Color.MediumBlue;
            grpLDsityObjectsNegative.Location = new System.Drawing.Point(0, 0);
            grpLDsityObjectsNegative.Margin = new System.Windows.Forms.Padding(0);
            grpLDsityObjectsNegative.Name = "grpLDsityObjectsNegative";
            grpLDsityObjectsNegative.Padding = new System.Windows.Forms.Padding(5);
            grpLDsityObjectsNegative.Size = new System.Drawing.Size(266, 461);
            grpLDsityObjectsNegative.TabIndex = 2;
            grpLDsityObjectsNegative.TabStop = false;
            grpLDsityObjectsNegative.Text = "grpLDsityObjectsNegative";
            // 
            // pnlLDsityObjectsNegative
            // 
            pnlLDsityObjectsNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLDsityObjectsNegative.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            pnlLDsityObjectsNegative.ForeColor = System.Drawing.SystemColors.ControlText;
            pnlLDsityObjectsNegative.Location = new System.Drawing.Point(5, 25);
            pnlLDsityObjectsNegative.Margin = new System.Windows.Forms.Padding(0);
            pnlLDsityObjectsNegative.Name = "pnlLDsityObjectsNegative";
            pnlLDsityObjectsNegative.Size = new System.Drawing.Size(256, 431);
            pnlLDsityObjectsNegative.TabIndex = 7;
            pnlLDsityObjectsNegative.Resize += PnlLDsityObjectsNegative_Resize;
            // 
            // FormTargetVariableCoreNew
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(pnlMain);
            Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormTargetVariableCoreNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormTargetVariableCoreNew";
            FormClosing += FormTargetVariableCoreNew_FormClosing;
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            grpTargetVariable.ResumeLayout(false);
            pnlTargetVariable.ResumeLayout(false);
            tlpTargetVariable.ResumeLayout(false);
            tlpTargetVariable.PerformLayout();
            splPositiveNegative.Panel1.ResumeLayout(false);
            splPositiveNegative.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splPositiveNegative).EndInit();
            splPositiveNegative.ResumeLayout(false);
            grpLDsityObjectsPositive.ResumeLayout(false);
            grpLDsityObjectsNegative.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.SplitContainer splPositiveNegative;
        private System.Windows.Forms.GroupBox grpLDsityObjectsPositive;
        private System.Windows.Forms.Panel pnlLDsityObjectsPositive;
        private System.Windows.Forms.GroupBox grpLDsityObjectsNegative;
        private System.Windows.Forms.Panel pnlLDsityObjectsNegative;
        private System.Windows.Forms.GroupBox grpTargetVariable;
        private System.Windows.Forms.Panel pnlTargetVariable;
        private System.Windows.Forms.TableLayoutPanel tlpTargetVariable;
        private System.Windows.Forms.ComboBox cboUnitOfMeasure;
        private System.Windows.Forms.ComboBox cboVersionNegative;
        private System.Windows.Forms.Label lblVersionNegative;
        private System.Windows.Forms.TextBox txtDescriptionEnValue;
        private System.Windows.Forms.TextBox txtLabelEnValue;
        private System.Windows.Forms.TextBox txtDescriptionCsValue;
        private System.Windows.Forms.Label lblLabelCsCaption;
        private System.Windows.Forms.Label lblDescriptionCsCaption;
        private System.Windows.Forms.Label lblLabelEnCaption;
        private System.Windows.Forms.Label lblDescriptionEnCaption;
        private System.Windows.Forms.TextBox txtLabelCsValue;
        private System.Windows.Forms.Label lblVersionPositive;
        private System.Windows.Forms.ComboBox cboVersionPositive;
        private System.Windows.Forms.Label lblStateOrChangeCaption;
        private System.Windows.Forms.Panel pnlStateOrChange;
        private System.Windows.Forms.Label lblUnitOfMeasure;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
    }

}