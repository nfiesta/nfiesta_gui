﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlTDDomainListBoxItemDesign
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlExpanded = new System.Windows.Forms.Panel();
            this.tlpExpanded = new System.Windows.Forms.TableLayoutPanel();
            this.pnlExpandedWorkSpace = new System.Windows.Forms.Panel();
            this.btnCollapse = new System.Windows.Forms.Button();
            this.pnlCollapsed = new System.Windows.Forms.Panel();
            this.tlpCollapsed = new System.Windows.Forms.TableLayoutPanel();
            this.btnExpand = new System.Windows.Forms.Button();
            this.pnlCollapsedWorkSpace = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            this.pnlExpanded.SuspendLayout();
            this.tlpExpanded.SuspendLayout();
            this.pnlCollapsed.SuspendLayout();
            this.tlpCollapsed.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlExpanded);
            this.pnlMain.Controls.Add(this.pnlCollapsed);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(600, 100);
            this.pnlMain.TabIndex = 0;
            // 
            // pnlExpanded
            // 
            this.pnlExpanded.Controls.Add(this.tlpExpanded);
            this.pnlExpanded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExpanded.Location = new System.Drawing.Point(0, 25);
            this.pnlExpanded.Margin = new System.Windows.Forms.Padding(0);
            this.pnlExpanded.Name = "pnlExpanded";
            this.pnlExpanded.Size = new System.Drawing.Size(600, 75);
            this.pnlExpanded.TabIndex = 1;
            // 
            // tlpExpanded
            // 
            this.tlpExpanded.ColumnCount = 2;
            this.tlpExpanded.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpExpanded.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpExpanded.Controls.Add(this.pnlExpandedWorkSpace, 0, 0);
            this.tlpExpanded.Controls.Add(this.btnCollapse, 0, 0);
            this.tlpExpanded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpExpanded.Location = new System.Drawing.Point(0, 0);
            this.tlpExpanded.Margin = new System.Windows.Forms.Padding(0);
            this.tlpExpanded.Name = "tlpExpanded";
            this.tlpExpanded.RowCount = 1;
            this.tlpExpanded.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpExpanded.Size = new System.Drawing.Size(600, 75);
            this.tlpExpanded.TabIndex = 1;
            // 
            // pnlExpandedWorkSpace
            // 
            this.pnlExpandedWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExpandedWorkSpace.Location = new System.Drawing.Point(25, 0);
            this.pnlExpandedWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlExpandedWorkSpace.Name = "pnlExpandedWorkSpace";
            this.pnlExpandedWorkSpace.Size = new System.Drawing.Size(575, 75);
            this.pnlExpandedWorkSpace.TabIndex = 3;
            
            // 
            // btnCollapse
            // 
            this.btnCollapse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCollapse.FlatAppearance.BorderSize = 0;
            this.btnCollapse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCollapse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnCollapse.Location = new System.Drawing.Point(0, 0);
            this.btnCollapse.Margin = new System.Windows.Forms.Padding(0);
            this.btnCollapse.Name = "btnCollapse";
            this.btnCollapse.Size = new System.Drawing.Size(25, 75);
            this.btnCollapse.TabIndex = 2;
            this.btnCollapse.Text = "-";
            this.btnCollapse.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnCollapse.UseVisualStyleBackColor = true;
           
            // 
            // pnlCollapsed
            // 
            this.pnlCollapsed.Controls.Add(this.tlpCollapsed);
            this.pnlCollapsed.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCollapsed.Location = new System.Drawing.Point(0, 0);
            this.pnlCollapsed.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCollapsed.Name = "pnlCollapsed";
            this.pnlCollapsed.Size = new System.Drawing.Size(600, 25);
            this.pnlCollapsed.TabIndex = 0;
            // 
            // tlpCollapsed
            // 
            this.tlpCollapsed.ColumnCount = 2;
            this.tlpCollapsed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCollapsed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCollapsed.Controls.Add(this.btnExpand, 0, 0);
            this.tlpCollapsed.Controls.Add(this.pnlCollapsedWorkSpace, 1, 0);
            this.tlpCollapsed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCollapsed.Location = new System.Drawing.Point(0, 0);
            this.tlpCollapsed.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCollapsed.Name = "tlpCollapsed";
            this.tlpCollapsed.RowCount = 1;
            this.tlpCollapsed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCollapsed.Size = new System.Drawing.Size(600, 25);
            this.tlpCollapsed.TabIndex = 0;
            // 
            // btnExpand
            // 
            this.btnExpand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExpand.FlatAppearance.BorderSize = 0;
            this.btnExpand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnExpand.Location = new System.Drawing.Point(0, 0);
            this.btnExpand.Margin = new System.Windows.Forms.Padding(0);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(25, 25);
            this.btnExpand.TabIndex = 1;
            this.btnExpand.Text = "+";
            this.btnExpand.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnExpand.UseVisualStyleBackColor = true;
            
            // 
            // pnlCollapsedWorkSpace
            // 
            this.pnlCollapsedWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCollapsedWorkSpace.Location = new System.Drawing.Point(25, 0);
            this.pnlCollapsedWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCollapsedWorkSpace.Name = "pnlCollapsedWorkSpace";
            this.pnlCollapsedWorkSpace.Size = new System.Drawing.Size(575, 25);
            this.pnlCollapsedWorkSpace.TabIndex = 2;
            // 
            // ControlTDDomainListBoxItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlTDDomainListBoxItem";
            this.Size = new System.Drawing.Size(600, 100);
            this.pnlMain.ResumeLayout(false);
            this.pnlExpanded.ResumeLayout(false);
            this.tlpExpanded.ResumeLayout(false);
            this.pnlCollapsed.ResumeLayout(false);
            this.tlpCollapsed.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlExpanded;
        private System.Windows.Forms.Panel pnlCollapsed;
        private System.Windows.Forms.TableLayoutPanel tlpCollapsed;
        private System.Windows.Forms.TableLayoutPanel tlpExpanded;
        private System.Windows.Forms.Button btnCollapse;
        private System.Windows.Forms.Button btnExpand;
        private System.Windows.Forms.Panel pnlExpandedWorkSpace;
        private System.Windows.Forms.Panel pnlCollapsedWorkSpace;
    }

}
