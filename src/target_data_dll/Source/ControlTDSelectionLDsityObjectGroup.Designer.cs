﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlTDSelectionLDsityObjectGroup
    {

        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlTDSelectionLDsityObjectGroup));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlNext = new System.Windows.Forms.Panel();
            this.btnNext = new System.Windows.Forms.Button();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.splCaption = new System.Windows.Forms.SplitContainer();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.tlpCaption = new System.Windows.Forms.TableLayoutPanel();
            this.lblSelectedLDsityObjectGroupValue = new System.Windows.Forms.Label();
            this.lblSelectedLDsityObjectGroupCaption = new System.Windows.Forms.Label();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splLDsityObjectGroup = new System.Windows.Forms.SplitContainer();
            this.grpLDsityObjectGroup = new System.Windows.Forms.GroupBox();
            this.tlpLDsityObjectGroup = new System.Windows.Forms.TableLayoutPanel();
            this.pnlLDsityObjectGroup = new System.Windows.Forms.Panel();
            this.lstLDsityObjectGroup = new System.Windows.Forms.ListBox();
            this.tsrLDsityObjectGroup = new System.Windows.Forms.ToolStrip();
            this.btnLDsityObjectGroupEdit = new System.Windows.Forms.ToolStripButton();
            this.btnLDsityObjectGroupAdd = new System.Windows.Forms.ToolStripButton();
            this.btnLDsityObjectGroupDelete = new System.Windows.Forms.ToolStripButton();
            this.splLDsityObject = new System.Windows.Forms.SplitContainer();
            this.grpLDsityObject = new System.Windows.Forms.GroupBox();
            this.tlpLDsityObject = new System.Windows.Forms.TableLayoutPanel();
            this.tsrLDsityObject = new System.Windows.Forms.ToolStrip();
            this.btnLDsityObjectEdit = new System.Windows.Forms.ToolStripButton();
            this.pnlLDsityObject = new System.Windows.Forms.Panel();
            this.lstLDsityObject = new System.Windows.Forms.ListBox();
            this.lblLDsityObjectGroupIsEmpty = new System.Windows.Forms.Label();
            this.grpLDsityObjectGroupInfo = new System.Windows.Forms.GroupBox();
            this.tlpLDsityObjectGroupInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lblInfoDescriptionEnCaption = new System.Windows.Forms.Label();
            this.lblInfoLabelEnCaption = new System.Windows.Forms.Label();
            this.lblInfoDescriptionCsCaption = new System.Windows.Forms.Label();
            this.lblInfoLabelCsCaption = new System.Windows.Forms.Label();
            this.lblDescriptionEnValue = new System.Windows.Forms.Label();
            this.lblLabelEnValue = new System.Windows.Forms.Label();
            this.lblDescriptionCsValue = new System.Windows.Forms.Label();
            this.lblLabelCsValue = new System.Windows.Forms.Label();
            this.lblInfoIDValue = new System.Windows.Forms.Label();
            this.lblInfoIDCaption = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlNext.SuspendLayout();
            this.pnlCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).BeginInit();
            this.splCaption.Panel1.SuspendLayout();
            this.splCaption.Panel2.SuspendLayout();
            this.splCaption.SuspendLayout();
            this.tlpCaption.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObjectGroup)).BeginInit();
            this.splLDsityObjectGroup.Panel1.SuspendLayout();
            this.splLDsityObjectGroup.Panel2.SuspendLayout();
            this.splLDsityObjectGroup.SuspendLayout();
            this.grpLDsityObjectGroup.SuspendLayout();
            this.tlpLDsityObjectGroup.SuspendLayout();
            this.pnlLDsityObjectGroup.SuspendLayout();
            this.tsrLDsityObjectGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObject)).BeginInit();
            this.splLDsityObject.Panel1.SuspendLayout();
            this.splLDsityObject.Panel2.SuspendLayout();
            this.splLDsityObject.SuspendLayout();
            this.grpLDsityObject.SuspendLayout();
            this.tlpLDsityObject.SuspendLayout();
            this.tsrLDsityObject.SuspendLayout();
            this.pnlLDsityObject.SuspendLayout();
            this.grpLDsityObjectGroupInfo.SuspendLayout();
            this.tlpLDsityObjectGroupInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 1;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 2);
            this.tlpMain.Controls.Add(this.pnlCaption, 0, 0);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 1);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 2;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.Controls.Add(this.pnlNext, 1, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 500);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(960, 40);
            this.tlpButtons.TabIndex = 14;
            // 
            // pnlNext
            // 
            this.pnlNext.Controls.Add(this.btnNext);
            this.pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNext.Location = new System.Drawing.Point(800, 0);
            this.pnlNext.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNext.Name = "pnlNext";
            this.pnlNext.Padding = new System.Windows.Forms.Padding(5);
            this.pnlNext.Size = new System.Drawing.Size(160, 40);
            this.pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnNext.Location = new System.Drawing.Point(5, 5);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 15;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlCaption
            // 
            this.pnlCaption.Controls.Add(this.splCaption);
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Size = new System.Drawing.Size(960, 60);
            this.pnlCaption.TabIndex = 10;
            // 
            // splCaption
            // 
            this.splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCaption.IsSplitterFixed = true;
            this.splCaption.Location = new System.Drawing.Point(0, 0);
            this.splCaption.Margin = new System.Windows.Forms.Padding(0);
            this.splCaption.Name = "splCaption";
            this.splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            this.splCaption.Panel1.Controls.Add(this.lblMainCaption);
            // 
            // splCaption.Panel2
            // 
            this.splCaption.Panel2.Controls.Add(this.tlpCaption);
            this.splCaption.Size = new System.Drawing.Size(960, 60);
            this.splCaption.SplitterDistance = 30;
            this.splCaption.TabIndex = 13;
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(0, 0);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(960, 30);
            this.lblMainCaption.TabIndex = 0;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpCaption
            // 
            this.tlpCaption.ColumnCount = 2;
            this.tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaption.Controls.Add(this.lblSelectedLDsityObjectGroupValue, 1, 0);
            this.tlpCaption.Controls.Add(this.lblSelectedLDsityObjectGroupCaption, 0, 0);
            this.tlpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCaption.Location = new System.Drawing.Point(0, 0);
            this.tlpCaption.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCaption.Name = "tlpCaption";
            this.tlpCaption.RowCount = 2;
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaption.Size = new System.Drawing.Size(960, 26);
            this.tlpCaption.TabIndex = 0;
            // 
            // lblSelectedLDsityObjectGroupValue
            // 
            this.lblSelectedLDsityObjectGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedLDsityObjectGroupValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedLDsityObjectGroupValue.Location = new System.Drawing.Point(283, 0);
            this.lblSelectedLDsityObjectGroupValue.Name = "lblSelectedLDsityObjectGroupValue";
            this.lblSelectedLDsityObjectGroupValue.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedLDsityObjectGroupValue.Size = new System.Drawing.Size(674, 25);
            this.lblSelectedLDsityObjectGroupValue.TabIndex = 4;
            this.lblSelectedLDsityObjectGroupValue.Text = "lblSelectedLDsityObjectGroupValue";
            this.lblSelectedLDsityObjectGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedLDsityObjectGroupCaption
            // 
            this.lblSelectedLDsityObjectGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedLDsityObjectGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedLDsityObjectGroupCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSelectedLDsityObjectGroupCaption.Location = new System.Drawing.Point(0, 0);
            this.lblSelectedLDsityObjectGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblSelectedLDsityObjectGroupCaption.Name = "lblSelectedLDsityObjectGroupCaption";
            this.lblSelectedLDsityObjectGroupCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedLDsityObjectGroupCaption.Size = new System.Drawing.Size(280, 25);
            this.lblSelectedLDsityObjectGroupCaption.TabIndex = 0;
            this.lblSelectedLDsityObjectGroupCaption.Text = "lblSelectedLDsityObjectGroupCaption";
            this.lblSelectedLDsityObjectGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.Controls.Add(this.splLDsityObjectGroup);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(3, 63);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 434);
            this.pnlWorkSpace.TabIndex = 9;
            // 
            // splLDsityObjectGroup
            // 
            this.splLDsityObjectGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splLDsityObjectGroup.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splLDsityObjectGroup.Location = new System.Drawing.Point(0, 0);
            this.splLDsityObjectGroup.Margin = new System.Windows.Forms.Padding(0);
            this.splLDsityObjectGroup.Name = "splLDsityObjectGroup";
            // 
            // splLDsityObjectGroup.Panel1
            // 
            this.splLDsityObjectGroup.Panel1.Controls.Add(this.grpLDsityObjectGroup);
            // 
            // splLDsityObjectGroup.Panel2
            // 
            this.splLDsityObjectGroup.Panel2.Controls.Add(this.splLDsityObject);
            this.splLDsityObjectGroup.Size = new System.Drawing.Size(954, 434);
            this.splLDsityObjectGroup.SplitterDistance = 275;
            this.splLDsityObjectGroup.TabIndex = 9;
            // 
            // grpLDsityObjectGroup
            // 
            this.grpLDsityObjectGroup.Controls.Add(this.tlpLDsityObjectGroup);
            this.grpLDsityObjectGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpLDsityObjectGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpLDsityObjectGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpLDsityObjectGroup.Location = new System.Drawing.Point(0, 0);
            this.grpLDsityObjectGroup.Margin = new System.Windows.Forms.Padding(0);
            this.grpLDsityObjectGroup.Name = "grpLDsityObjectGroup";
            this.grpLDsityObjectGroup.Padding = new System.Windows.Forms.Padding(5);
            this.grpLDsityObjectGroup.Size = new System.Drawing.Size(275, 434);
            this.grpLDsityObjectGroup.TabIndex = 3;
            this.grpLDsityObjectGroup.TabStop = false;
            this.grpLDsityObjectGroup.Text = "grpLDsityObjectGroup";
            // 
            // tlpLDsityObjectGroup
            // 
            this.tlpLDsityObjectGroup.ColumnCount = 1;
            this.tlpLDsityObjectGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObjectGroup.Controls.Add(this.pnlLDsityObjectGroup, 0, 1);
            this.tlpLDsityObjectGroup.Controls.Add(this.tsrLDsityObjectGroup, 0, 0);
            this.tlpLDsityObjectGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLDsityObjectGroup.Location = new System.Drawing.Point(5, 20);
            this.tlpLDsityObjectGroup.Margin = new System.Windows.Forms.Padding(0);
            this.tlpLDsityObjectGroup.Name = "tlpLDsityObjectGroup";
            this.tlpLDsityObjectGroup.RowCount = 2;
            this.tlpLDsityObjectGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpLDsityObjectGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObjectGroup.Size = new System.Drawing.Size(265, 409);
            this.tlpLDsityObjectGroup.TabIndex = 1;
            // 
            // pnlLDsityObjectGroup
            // 
            this.pnlLDsityObjectGroup.Controls.Add(this.lstLDsityObjectGroup);
            this.pnlLDsityObjectGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLDsityObjectGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlLDsityObjectGroup.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlLDsityObjectGroup.Location = new System.Drawing.Point(0, 25);
            this.pnlLDsityObjectGroup.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLDsityObjectGroup.Name = "pnlLDsityObjectGroup";
            this.pnlLDsityObjectGroup.Size = new System.Drawing.Size(265, 384);
            this.pnlLDsityObjectGroup.TabIndex = 2;
            // 
            // lstLDsityObjectGroup
            // 
            this.lstLDsityObjectGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLDsityObjectGroup.FormattingEnabled = true;
            this.lstLDsityObjectGroup.ItemHeight = 15;
            this.lstLDsityObjectGroup.Location = new System.Drawing.Point(0, 0);
            this.lstLDsityObjectGroup.Name = "lstLDsityObjectGroup";
            this.lstLDsityObjectGroup.Size = new System.Drawing.Size(265, 384);
            this.lstLDsityObjectGroup.TabIndex = 0;
            // 
            // tsrLDsityObjectGroup
            // 
            this.tsrLDsityObjectGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrLDsityObjectGroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLDsityObjectGroupEdit,
            this.btnLDsityObjectGroupAdd,
            this.btnLDsityObjectGroupDelete});
            this.tsrLDsityObjectGroup.Location = new System.Drawing.Point(0, 0);
            this.tsrLDsityObjectGroup.Name = "tsrLDsityObjectGroup";
            this.tsrLDsityObjectGroup.Padding = new System.Windows.Forms.Padding(0);
            this.tsrLDsityObjectGroup.Size = new System.Drawing.Size(265, 25);
            this.tsrLDsityObjectGroup.TabIndex = 1;
            this.tsrLDsityObjectGroup.Text = "tsrLDsityObjectGroup";
            // 
            // btnLDsityObjectGroupEdit
            // 
            this.btnLDsityObjectGroupEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLDsityObjectGroupEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnLDsityObjectGroupEdit.Image")));
            this.btnLDsityObjectGroupEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLDsityObjectGroupEdit.Name = "btnLDsityObjectGroupEdit";
            this.btnLDsityObjectGroupEdit.Size = new System.Drawing.Size(23, 22);
            this.btnLDsityObjectGroupEdit.Text = "btnLDsityObjectGroupEdit";
            // 
            // btnLDsityObjectGroupAdd
            // 
            this.btnLDsityObjectGroupAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLDsityObjectGroupAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnLDsityObjectGroupAdd.Image")));
            this.btnLDsityObjectGroupAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLDsityObjectGroupAdd.Name = "btnLDsityObjectGroupAdd";
            this.btnLDsityObjectGroupAdd.Size = new System.Drawing.Size(23, 22);
            this.btnLDsityObjectGroupAdd.Text = "btnLDsityObjectGroupAdd";
            // 
            // btnLDsityObjectGroupDelete
            // 
            this.btnLDsityObjectGroupDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLDsityObjectGroupDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnLDsityObjectGroupDelete.Image")));
            this.btnLDsityObjectGroupDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLDsityObjectGroupDelete.Name = "btnLDsityObjectGroupDelete";
            this.btnLDsityObjectGroupDelete.Size = new System.Drawing.Size(23, 22);
            this.btnLDsityObjectGroupDelete.Text = "btnLDsityObjectGroupDelete";
            // 
            // splLDsityObject
            // 
            this.splLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splLDsityObject.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splLDsityObject.Location = new System.Drawing.Point(0, 0);
            this.splLDsityObject.Margin = new System.Windows.Forms.Padding(0);
            this.splLDsityObject.Name = "splLDsityObject";
            // 
            // splLDsityObject.Panel1
            // 
            this.splLDsityObject.Panel1.Controls.Add(this.grpLDsityObject);
            // 
            // splLDsityObject.Panel2
            // 
            this.splLDsityObject.Panel2.Controls.Add(this.grpLDsityObjectGroupInfo);
            this.splLDsityObject.Size = new System.Drawing.Size(675, 434);
            this.splLDsityObject.SplitterDistance = 275;
            this.splLDsityObject.TabIndex = 0;
            // 
            // grpLDsityObject
            // 
            this.grpLDsityObject.Controls.Add(this.tlpLDsityObject);
            this.grpLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpLDsityObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpLDsityObject.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpLDsityObject.Location = new System.Drawing.Point(0, 0);
            this.grpLDsityObject.Margin = new System.Windows.Forms.Padding(0);
            this.grpLDsityObject.Name = "grpLDsityObject";
            this.grpLDsityObject.Padding = new System.Windows.Forms.Padding(5);
            this.grpLDsityObject.Size = new System.Drawing.Size(275, 434);
            this.grpLDsityObject.TabIndex = 7;
            this.grpLDsityObject.TabStop = false;
            this.grpLDsityObject.Text = "grpLDsityObject";
            // 
            // tlpLDsityObject
            // 
            this.tlpLDsityObject.ColumnCount = 1;
            this.tlpLDsityObject.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObject.Controls.Add(this.tsrLDsityObject, 0, 0);
            this.tlpLDsityObject.Controls.Add(this.pnlLDsityObject, 0, 1);
            this.tlpLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLDsityObject.Location = new System.Drawing.Point(5, 20);
            this.tlpLDsityObject.Margin = new System.Windows.Forms.Padding(0);
            this.tlpLDsityObject.Name = "tlpLDsityObject";
            this.tlpLDsityObject.RowCount = 2;
            this.tlpLDsityObject.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpLDsityObject.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObject.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLDsityObject.Size = new System.Drawing.Size(265, 409);
            this.tlpLDsityObject.TabIndex = 1;
            // 
            // tsrLDsityObject
            // 
            this.tsrLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrLDsityObject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLDsityObjectEdit});
            this.tsrLDsityObject.Location = new System.Drawing.Point(0, 0);
            this.tsrLDsityObject.Name = "tsrLDsityObject";
            this.tsrLDsityObject.Padding = new System.Windows.Forms.Padding(0);
            this.tsrLDsityObject.Size = new System.Drawing.Size(265, 25);
            this.tsrLDsityObject.TabIndex = 16;
            this.tsrLDsityObject.Text = "tsrLDsityObject";
            // 
            // btnLDsityObjectEdit
            // 
            this.btnLDsityObjectEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLDsityObjectEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnLDsityObjectEdit.Image")));
            this.btnLDsityObjectEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLDsityObjectEdit.Name = "btnLDsityObjectEdit";
            this.btnLDsityObjectEdit.Size = new System.Drawing.Size(23, 22);
            this.btnLDsityObjectEdit.Text = "btnLDsityObjectEdit";
            // 
            // pnlLDsityObject
            // 
            this.pnlLDsityObject.Controls.Add(this.lstLDsityObject);
            this.pnlLDsityObject.Controls.Add(this.lblLDsityObjectGroupIsEmpty);
            this.pnlLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLDsityObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlLDsityObject.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlLDsityObject.Location = new System.Drawing.Point(0, 25);
            this.pnlLDsityObject.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLDsityObject.Name = "pnlLDsityObject";
            this.pnlLDsityObject.Size = new System.Drawing.Size(265, 384);
            this.pnlLDsityObject.TabIndex = 15;
            // 
            // lstLDsityObject
            // 
            this.lstLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLDsityObject.FormattingEnabled = true;
            this.lstLDsityObject.ItemHeight = 15;
            this.lstLDsityObject.Location = new System.Drawing.Point(0, 0);
            this.lstLDsityObject.Name = "lstLDsityObject";
            this.lstLDsityObject.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstLDsityObject.Size = new System.Drawing.Size(265, 384);
            this.lstLDsityObject.TabIndex = 3;
            // 
            // lblLDsityObjectGroupIsEmpty
            // 
            this.lblLDsityObjectGroupIsEmpty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLDsityObjectGroupIsEmpty.ForeColor = System.Drawing.Color.Red;
            this.lblLDsityObjectGroupIsEmpty.Location = new System.Drawing.Point(0, 0);
            this.lblLDsityObjectGroupIsEmpty.Name = "lblLDsityObjectGroupIsEmpty";
            this.lblLDsityObjectGroupIsEmpty.Size = new System.Drawing.Size(265, 384);
            this.lblLDsityObjectGroupIsEmpty.TabIndex = 2;
            this.lblLDsityObjectGroupIsEmpty.Text = "label1";
            this.lblLDsityObjectGroupIsEmpty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpLDsityObjectGroupInfo
            // 
            this.grpLDsityObjectGroupInfo.Controls.Add(this.tlpLDsityObjectGroupInfo);
            this.grpLDsityObjectGroupInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpLDsityObjectGroupInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpLDsityObjectGroupInfo.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpLDsityObjectGroupInfo.Location = new System.Drawing.Point(0, 0);
            this.grpLDsityObjectGroupInfo.Margin = new System.Windows.Forms.Padding(0);
            this.grpLDsityObjectGroupInfo.Name = "grpLDsityObjectGroupInfo";
            this.grpLDsityObjectGroupInfo.Padding = new System.Windows.Forms.Padding(5);
            this.grpLDsityObjectGroupInfo.Size = new System.Drawing.Size(396, 434);
            this.grpLDsityObjectGroupInfo.TabIndex = 0;
            this.grpLDsityObjectGroupInfo.TabStop = false;
            this.grpLDsityObjectGroupInfo.Text = "grpLDsityObjectGroupInfo";
            // 
            // tlpLDsityObjectGroupInfo
            // 
            this.tlpLDsityObjectGroupInfo.ColumnCount = 2;
            this.tlpLDsityObjectGroupInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpLDsityObjectGroupInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblInfoDescriptionEnCaption, 0, 6);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblInfoLabelEnCaption, 0, 5);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblInfoDescriptionCsCaption, 0, 3);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblInfoLabelCsCaption, 0, 2);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblDescriptionEnValue, 1, 6);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblLabelEnValue, 1, 5);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblDescriptionCsValue, 1, 3);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblLabelCsValue, 1, 2);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblInfoIDValue, 1, 0);
            this.tlpLDsityObjectGroupInfo.Controls.Add(this.lblInfoIDCaption, 0, 0);
            this.tlpLDsityObjectGroupInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLDsityObjectGroupInfo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlpLDsityObjectGroupInfo.ForeColor = System.Drawing.Color.Black;
            this.tlpLDsityObjectGroupInfo.Location = new System.Drawing.Point(5, 20);
            this.tlpLDsityObjectGroupInfo.Margin = new System.Windows.Forms.Padding(0);
            this.tlpLDsityObjectGroupInfo.Name = "tlpLDsityObjectGroupInfo";
            this.tlpLDsityObjectGroupInfo.RowCount = 8;
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpLDsityObjectGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObjectGroupInfo.Size = new System.Drawing.Size(386, 409);
            this.tlpLDsityObjectGroupInfo.TabIndex = 0;
            // 
            // lblInfoDescriptionEnCaption
            // 
            this.lblInfoDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfoDescriptionEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoDescriptionEnCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblInfoDescriptionEnCaption.Location = new System.Drawing.Point(0, 130);
            this.lblInfoDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblInfoDescriptionEnCaption.Name = "lblInfoDescriptionEnCaption";
            this.lblInfoDescriptionEnCaption.Size = new System.Drawing.Size(150, 30);
            this.lblInfoDescriptionEnCaption.TabIndex = 3;
            this.lblInfoDescriptionEnCaption.Text = "lblInfoDescriptionEnCaption";
            // 
            // lblInfoLabelEnCaption
            // 
            this.lblInfoLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfoLabelEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoLabelEnCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblInfoLabelEnCaption.Location = new System.Drawing.Point(0, 100);
            this.lblInfoLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblInfoLabelEnCaption.Name = "lblInfoLabelEnCaption";
            this.lblInfoLabelEnCaption.Size = new System.Drawing.Size(150, 30);
            this.lblInfoLabelEnCaption.TabIndex = 2;
            this.lblInfoLabelEnCaption.Text = "lblInfoLabelEnCaption";
            // 
            // lblInfoDescriptionCsCaption
            // 
            this.lblInfoDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfoDescriptionCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoDescriptionCsCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblInfoDescriptionCsCaption.Location = new System.Drawing.Point(0, 70);
            this.lblInfoDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblInfoDescriptionCsCaption.Name = "lblInfoDescriptionCsCaption";
            this.lblInfoDescriptionCsCaption.Size = new System.Drawing.Size(150, 30);
            this.lblInfoDescriptionCsCaption.TabIndex = 1;
            this.lblInfoDescriptionCsCaption.Text = "lblInfoDescriptionCsCaption";
            // 
            // lblInfoLabelCsCaption
            // 
            this.lblInfoLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfoLabelCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoLabelCsCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblInfoLabelCsCaption.Location = new System.Drawing.Point(0, 40);
            this.lblInfoLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblInfoLabelCsCaption.Name = "lblInfoLabelCsCaption";
            this.lblInfoLabelCsCaption.Size = new System.Drawing.Size(150, 30);
            this.lblInfoLabelCsCaption.TabIndex = 0;
            this.lblInfoLabelCsCaption.Text = "lblInfoLabelCsCaption";
            // 
            // lblDescriptionEnValue
            // 
            this.lblDescriptionEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescriptionEnValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionEnValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDescriptionEnValue.Location = new System.Drawing.Point(150, 130);
            this.lblDescriptionEnValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDescriptionEnValue.Name = "lblDescriptionEnValue";
            this.lblDescriptionEnValue.Size = new System.Drawing.Size(236, 30);
            this.lblDescriptionEnValue.TabIndex = 7;
            this.lblDescriptionEnValue.Text = "lblDescriptionEnValue";
            // 
            // lblLabelEnValue
            // 
            this.lblLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabelEnValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLabelEnValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLabelEnValue.Location = new System.Drawing.Point(150, 100);
            this.lblLabelEnValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblLabelEnValue.Name = "lblLabelEnValue";
            this.lblLabelEnValue.Size = new System.Drawing.Size(236, 30);
            this.lblLabelEnValue.TabIndex = 6;
            this.lblLabelEnValue.Text = "lblLabelEnValue";
            // 
            // lblDescriptionCsValue
            // 
            this.lblDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescriptionCsValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionCsValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDescriptionCsValue.Location = new System.Drawing.Point(150, 70);
            this.lblDescriptionCsValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblDescriptionCsValue.Name = "lblDescriptionCsValue";
            this.lblDescriptionCsValue.Size = new System.Drawing.Size(236, 30);
            this.lblDescriptionCsValue.TabIndex = 5;
            this.lblDescriptionCsValue.Text = "lblDescriptionCsValue";
            // 
            // lblLabelCsValue
            // 
            this.lblLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabelCsValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLabelCsValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLabelCsValue.Location = new System.Drawing.Point(150, 40);
            this.lblLabelCsValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblLabelCsValue.Name = "lblLabelCsValue";
            this.lblLabelCsValue.Size = new System.Drawing.Size(236, 30);
            this.lblLabelCsValue.TabIndex = 4;
            this.lblLabelCsValue.Text = "lblLabelCsValue";
            // 
            // lblInfoIDValue
            // 
            this.lblInfoIDValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfoIDValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoIDValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblInfoIDValue.Location = new System.Drawing.Point(150, 0);
            this.lblInfoIDValue.Margin = new System.Windows.Forms.Padding(0);
            this.lblInfoIDValue.Name = "lblInfoIDValue";
            this.lblInfoIDValue.Size = new System.Drawing.Size(236, 30);
            this.lblInfoIDValue.TabIndex = 9;
            this.lblInfoIDValue.Text = "lblInfoIDValue";
            // 
            // lblInfoIDCaption
            // 
            this.lblInfoIDCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfoIDCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoIDCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblInfoIDCaption.Location = new System.Drawing.Point(0, 0);
            this.lblInfoIDCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblInfoIDCaption.Name = "lblInfoIDCaption";
            this.lblInfoIDCaption.Size = new System.Drawing.Size(150, 30);
            this.lblInfoIDCaption.TabIndex = 8;
            this.lblInfoIDCaption.Text = "lblInfoIDCaption";
            // 
            // ControlTDSelectionLDsityObjectGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Name = "ControlTDSelectionLDsityObjectGroup";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlNext.ResumeLayout(false);
            this.pnlCaption.ResumeLayout(false);
            this.splCaption.Panel1.ResumeLayout(false);
            this.splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).EndInit();
            this.splCaption.ResumeLayout(false);
            this.tlpCaption.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splLDsityObjectGroup.Panel1.ResumeLayout(false);
            this.splLDsityObjectGroup.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObjectGroup)).EndInit();
            this.splLDsityObjectGroup.ResumeLayout(false);
            this.grpLDsityObjectGroup.ResumeLayout(false);
            this.tlpLDsityObjectGroup.ResumeLayout(false);
            this.tlpLDsityObjectGroup.PerformLayout();
            this.pnlLDsityObjectGroup.ResumeLayout(false);
            this.tsrLDsityObjectGroup.ResumeLayout(false);
            this.tsrLDsityObjectGroup.PerformLayout();
            this.splLDsityObject.Panel1.ResumeLayout(false);
            this.splLDsityObject.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObject)).EndInit();
            this.splLDsityObject.ResumeLayout(false);
            this.grpLDsityObject.ResumeLayout(false);
            this.tlpLDsityObject.ResumeLayout(false);
            this.tlpLDsityObject.PerformLayout();
            this.tsrLDsityObject.ResumeLayout(false);
            this.tsrLDsityObject.PerformLayout();
            this.pnlLDsityObject.ResumeLayout(false);
            this.grpLDsityObjectGroupInfo.ResumeLayout(false);
            this.tlpLDsityObjectGroupInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splLDsityObjectGroup;
        private System.Windows.Forms.GroupBox grpLDsityObjectGroup;
        private System.Windows.Forms.SplitContainer splLDsityObject;
        private System.Windows.Forms.GroupBox grpLDsityObject;
        private System.Windows.Forms.TableLayoutPanel tlpLDsityObjectGroup;
        private System.Windows.Forms.Panel pnlLDsityObjectGroup;
        private System.Windows.Forms.ToolStrip tsrLDsityObjectGroup;
        private System.Windows.Forms.ToolStripButton btnLDsityObjectGroupEdit;
        private System.Windows.Forms.TableLayoutPanel tlpLDsityObject;
        private System.Windows.Forms.Panel pnlLDsityObject;
        private System.Windows.Forms.ToolStripButton btnLDsityObjectGroupAdd;
        private System.Windows.Forms.ToolStripButton btnLDsityObjectGroupDelete;
        private System.Windows.Forms.GroupBox grpLDsityObjectGroupInfo;
        private System.Windows.Forms.TableLayoutPanel tlpLDsityObjectGroupInfo;
        private System.Windows.Forms.Label lblDescriptionEnValue;
        private System.Windows.Forms.Label lblLabelEnValue;
        private System.Windows.Forms.Label lblDescriptionCsValue;
        private System.Windows.Forms.Label lblLabelCsValue;
        private System.Windows.Forms.Label lblInfoLabelCsCaption;
        private System.Windows.Forms.Label lblInfoDescriptionCsCaption;
        private System.Windows.Forms.Label lblInfoLabelEnCaption;
        private System.Windows.Forms.Label lblInfoDescriptionEnCaption;
        private System.Windows.Forms.ToolStrip tsrLDsityObject;
        private System.Windows.Forms.ToolStripButton btnLDsityObjectEdit;
        private System.Windows.Forms.Label lblInfoIDValue;
        private System.Windows.Forms.Label lblInfoIDCaption;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.ListBox lstLDsityObjectGroup;
        private System.Windows.Forms.ListBox lstLDsityObject;
        private System.Windows.Forms.Label lblLDsityObjectGroupIsEmpty;
        private System.Windows.Forms.TableLayoutPanel tlpCaption;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupValue;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupCaption;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
    }

}
