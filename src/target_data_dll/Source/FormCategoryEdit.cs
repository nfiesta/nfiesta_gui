﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro editace kategorií plošných domén nebo subpopulací - třída pro Designer</para>
    /// <para lang="en">Form for editing categories of area domains or subpopulations - class for Designer</para>
    /// </summary>
    internal partial class FormCategoryEditDesign
        : Form
    {
        protected FormCategoryEditDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Button BtnCancel => btnCancel;
        protected System.Windows.Forms.Button BtnOK => btnOK;
        protected System.Windows.Forms.GroupBox GrpCategory => grpCategory;
        protected System.Windows.Forms.Label LblDescriptionCsCaption => lblDescriptionCsCaption;
        protected System.Windows.Forms.Label LblDescriptionEnCaption => lblDescriptionEnCaption;
        protected System.Windows.Forms.Label LblIDCaption => lblIDCaption;
        protected System.Windows.Forms.Label LblIDValue => lblIDValue;
        protected System.Windows.Forms.Label LblLabelCsCaption => lblLabelCsCaption;
        protected System.Windows.Forms.Label LblLabelEnCaption => lblLabelEnCaption;
        protected System.Windows.Forms.TableLayoutPanel TlpCategory => tlpCategory;
        protected System.Windows.Forms.TextBox TxtDescriptionCsValue => txtDescriptionCsValue;
        protected System.Windows.Forms.TextBox TxtDescriptionEnValue => txtDescriptionEnValue;
        protected System.Windows.Forms.TextBox TxtLabelCsValue => txtLabelCsValue;
        protected System.Windows.Forms.TextBox TxtLabelEnValue => txtLabelEnValue;
    }

    /// <summary>
    /// <para lang="cs">Formulář pro editace kategorií plošných domén nebo subpopulací</para>
    /// <para lang="en">Form for editing categories of area domains or subpopulations</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class FormCategoryEdit<TDomain, TCategory>
            : FormCategoryEditDesign, INfiEstaControl, ITargetDataControl
             where TDomain : IArealOrPopulationDomain
             where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Edited area domain or subpopulation category</para>
        /// </summary>
        private TCategory category;

        /// <summary>
        /// <para lang="cs">Seznam existujících kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">List of existing area domain or subpopulation categories</para>
        /// </summary>
        private List<TCategory> categories;

        /// <summary>
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace je nová</para>
        /// <para lang="en">Edited area domain or subpopulation category is new</para>
        /// </summary>
        private bool categoryIsNew;

        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="category">
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Edited area domain or subpopulation category</para>
        /// </param>
        /// <param name="categories">
        /// <para lang="cs">Seznam existujících kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">List of existing area domain or subpopulation categories</para>
        /// </param>
        /// <param name="categoryIsNew">
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace je nová</para>
        /// <para lang="en">Edited area domain or subpopulation category is new</para>
        /// </param>
        public FormCategoryEdit(
            Control controlOwner,
            TCategory category,
            List<TCategory> categories,
            bool categoryIsNew)
            : base()
        {
            Initialize(
                controlOwner: controlOwner,
                category: category,
                categories: categories,
                categoryIsNew: categoryIsNew);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Edited area domain or subpopulation category</para>
        /// </summary>
        private TCategory Category
        {
            get
            {
                if (category == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Category)} must not be null.",
                        paramName: nameof(Category));
                }
                return category;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Category)} must not be null.",
                        paramName: nameof(Category));
                }
                category = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam existujících kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">List of existing area domain or subpopulation categories</para>
        /// </summary>
        private List<TCategory> Categories
        {
            get
            {
                if (categories == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Categories)} must not be null.",
                        paramName: nameof(Categories));
                }
                return categories;
            }
            set
            {
                categories = value ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(Categories)} must not be null.",
                        paramName: nameof(Categories));
            }
        }

        /// <summary>
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace je nová</para>
        /// <para lang="en">Edited area domain or subpopulation category is new</para>
        /// </summary>
        private bool CategoryIsNew
        {
            get
            {
                return categoryIsNew;
            }
            set
            {
                categoryIsNew = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",        "Editovat vybranou kategorii plošné domény" },
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}New",     "Nová kategorie plošné domény" },
                            { nameof(BtnOK),                                            "Uložit" },
                            { nameof(BtnCancel),                                        "Zrušit" },
                            { nameof(GrpCategory),                                      String.Empty },
                            { nameof(LblIDCaption),                                     "Identifikační číslo:" },
                            { nameof(LblLabelCsCaption),                                "Zkratka (národní):" },
                            { nameof(LblDescriptionCsCaption),                          "Popis (národní):" },
                            { nameof(LblLabelEnCaption),                                "Zkratka (anglicky):" },
                            { nameof(LblDescriptionEnCaption),                          "Popis (anglicky):"},
                            { nameof(msgLabelCsIsEmpty),                                "Zkratka (národní) musí být vyplněna." },
                            { nameof(msgDescriptionCsIsEmpty),                          "Popis (národní) musí být vyplněn." },
                            { nameof(msgLabelEnIsEmpty),                                "Zkratka (anglicky) musí být vyplněna." },
                            { nameof(msgDescriptionEnIsEmpty),                          "Popis (anglicky) musí být vyplněn." },
                            { nameof(msgLabelCsExists),                                 "Použitá zkratka (národní) již existuje pro jinou kategorii plošné domény." },
                            { nameof(msgDescriptionCsExists),                           "Použitý popis (národní) již existuje pro jinou kategorii plošné domény." },
                            { nameof(msgLabelEnExists),                                 "Použitá zkratka (anglická) již existuje pro jinou kategorii plošné domény." },
                            { nameof(msgDescriptionEnExists),                           "Použitý popis (anglický) již existuje pro jinou kategorii plošné domény." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryEdit<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",        "Editovat vybranou kategorii subpopulace" },
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}New",     "Nová kategorie subpopulace" },
                            { nameof(BtnOK),                                            "Uložit" },
                            { nameof(BtnCancel),                                        "Zrušit" },
                            { nameof(GrpCategory),                                      String.Empty },
                            { nameof(LblIDCaption),                                     "Identifikační číslo:" },
                            { nameof(LblLabelCsCaption),                                "Zkratka (národní):" },
                            { nameof(LblDescriptionCsCaption),                          "Popis (národní):" },
                            { nameof(LblLabelEnCaption),                                "Zkratka (anglicky):" },
                            { nameof(LblDescriptionEnCaption),                          "Popis (anglicky):"},
                            { nameof(msgLabelCsIsEmpty),                                "Zkratka (národní) musí být vyplněna." },
                            { nameof(msgDescriptionCsIsEmpty),                          "Popis (národní) musí být vyplněn." },
                            { nameof(msgLabelEnIsEmpty),                                "Zkratka (anglicky) musí být vyplněna." },
                            { nameof(msgDescriptionEnIsEmpty),                          "Popis (anglicky) musí být vyplněn." },
                            { nameof(msgLabelCsExists),                                 "Použitá zkratka (národní) již existuje pro jinou kategorii subpopulace." },
                            { nameof(msgDescriptionCsExists),                           "Použitý popis (národní) již existuje pro jinou kategorii subpopulace." },
                            { nameof(msgLabelEnExists),                                 "Použitá zkratka (anglická) již existuje pro jinou kategorii subpopulace." },
                            { nameof(msgDescriptionEnExists),                           "Použitý popis (anglický) již existuje pro jinou kategorii subpopulace." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryEdit<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictNationalSubPopulation)
                                ? dictNationalSubPopulation
                                : [],

                    _ => [],
                },

                LanguageVersion.International =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",        "Edit the selected area domain category" },
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}New",     "New area domain category" },
                            { nameof(BtnOK),                                            "Save" },
                            { nameof(BtnCancel),                                        "Cancel" },
                            { nameof(GrpCategory),                                      String.Empty },
                            { nameof(LblIDCaption),                                     "Category identifier:" },
                            { nameof(LblLabelCsCaption),                                "Label (national):" },
                            { nameof(LblDescriptionCsCaption),                          "Description (national):" },
                            { nameof(LblLabelEnCaption),                                "Label (English):" },
                            { nameof(LblDescriptionEnCaption),                          "Description (English):" },
                            { nameof(msgLabelCsIsEmpty),                                "Label (national) cannot be empty." },
                            { nameof(msgDescriptionCsIsEmpty),                          "Description (national) cannot be empty." },
                            { nameof(msgLabelEnIsEmpty),                                "Label (English) cannot be empty." },
                            { nameof(msgDescriptionEnIsEmpty),                          "Description (English) cannot be empty." },
                            { nameof(msgLabelCsExists),                                 "This label (national) already exists for another area domain category." },
                            { nameof(msgDescriptionCsExists),                           "This description (national) already exists for another area domain category." },
                            { nameof(msgLabelEnExists),                                 "This label (English) already exists for another area domain category." },
                            { nameof(msgDescriptionEnExists),                           "This description (English) already exists for another area domain category." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryEdit<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",        "Edit the selected subpopulation category" },
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}New",     "New subpopulation category" },
                            { nameof(BtnOK),                                            "Save" },
                            { nameof(BtnCancel),                                        "Cancel" },
                            { nameof(GrpCategory),                                      String.Empty },
                            { nameof(LblIDCaption),                                     "Category identifier:" },
                            { nameof(LblLabelCsCaption),                                "Label (national):" },
                            { nameof(LblDescriptionCsCaption),                          "Description (national):" },
                            { nameof(LblLabelEnCaption),                                "Label (English):" },
                            { nameof(LblDescriptionEnCaption),                          "Description (English):" },
                            { nameof(msgLabelCsIsEmpty),                                "Label (national) cannot be empty." },
                            { nameof(msgDescriptionCsIsEmpty),                          "Description (national) cannot be empty." },
                            { nameof(msgLabelEnIsEmpty),                                "Label (English) cannot be empty." },
                            { nameof(msgDescriptionEnIsEmpty),                          "Description (English) cannot be empty." },
                            { nameof(msgLabelCsExists),                                 "This label (national) already exists for another subpopulation category." },
                            { nameof(msgDescriptionCsExists),                           "This description (national) already exists for another subpopulation category." },
                            { nameof(msgLabelEnExists),                                 "This label (English) already exists for another subpopulation category." },
                            { nameof(msgDescriptionEnExists),                           "This description (English) already exists for another subpopulation category." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryEdit<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="category">
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Edited area domain or subpopulation category</para>
        /// </param>
        /// <param name="categories">
        /// <para lang="cs">Seznam existujících kategorií plošných domén nebo subpopulací</para>
        /// <para lang="en">List of existing area domain or subpopulation categories</para>
        /// </param>
        /// <param name="categoryIsNew">
        /// <para lang="cs">Editovaná kategorie plošné domény nebo subpopulace je nová</para>
        /// <para lang="en">Edited area domain or subpopulation category is new</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            TCategory category,
            List<TCategory> categories,
            bool categoryIsNew)
        {
            ControlOwner = controlOwner;
            Category = category;
            Categories = categories;
            CategoryIsNew = categoryIsNew;

            LblIDValue.Text = Category.Id.ToString();
            TxtLabelCsValue.Text = Category.LabelCs;
            TxtDescriptionCsValue.Text = Category.DescriptionCs;
            TxtLabelEnValue.Text = Category.LabelEn;
            TxtDescriptionEnValue.Text = Category.DescriptionEn;

            InitializeLabels();
            SetStyle();

            BtnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            BtnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !UpdateCategory();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });

            BtnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            TableLayoutPanelCellPosition pos;

            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;


            BtnOK.Font = Setting.ButtonFont;
            BtnOK.ForeColor = Setting.ButtonForeColor;

            BtnCancel.Font = Setting.ButtonFont;
            BtnCancel.ForeColor = Setting.ButtonForeColor;


            GrpCategory.Font = Setting.GroupBoxFont;
            GrpCategory.ForeColor = Setting.GroupBoxForeColor;


            LblIDCaption.Font = Setting.LabelFont;
            LblIDCaption.ForeColor = Setting.LabelForeColor;

            LblLabelCsCaption.Font = Setting.LabelFont;
            LblLabelCsCaption.ForeColor = Setting.LabelForeColor;

            LblDescriptionCsCaption.Font = Setting.LabelFont;
            LblDescriptionCsCaption.ForeColor = Setting.LabelForeColor;

            LblLabelEnCaption.Font = Setting.LabelFont;
            LblLabelEnCaption.ForeColor = Setting.LabelForeColor;

            LblDescriptionEnCaption.Font = Setting.LabelFont;
            LblDescriptionEnCaption.ForeColor = Setting.LabelForeColor;


            LblIDValue.Font = Setting.LabelValueFont;
            LblIDValue.ForeColor = Setting.LabelValueForeColor;

            TxtLabelCsValue.Font = Setting.TextBoxFont;
            TxtLabelCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpCategory.GetCellPosition(control: TxtLabelCsValue);
            TxtLabelCsValue.Margin = new Padding(
                left: 0, top: (int)((TlpCategory.GetRowHeights()[pos.Row] - TxtLabelCsValue.Height) / 2), right: 0, bottom: 0);

            TxtDescriptionCsValue.Font = Setting.TextBoxFont;
            TxtDescriptionCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpCategory.GetCellPosition(control: TxtDescriptionCsValue);
            TxtDescriptionCsValue.Margin = new Padding(
                left: 0, top: (int)((TlpCategory.GetRowHeights()[pos.Row] - TxtDescriptionCsValue.Height) / 2), right: 0, bottom: 0);

            TxtLabelEnValue.Font = Setting.TextBoxFont;
            TxtLabelEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpCategory.GetCellPosition(control: TxtLabelEnValue);
            TxtLabelEnValue.Margin = new Padding(
                left: 0, top: (int)((TlpCategory.GetRowHeights()[pos.Row] - TxtLabelEnValue.Height) / 2), right: 0, bottom: 0);

            TxtDescriptionEnValue.Font = Setting.TextBoxFont;
            TxtDescriptionEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = TlpCategory.GetCellPosition(control: TxtDescriptionEnValue);
            TxtDescriptionEnValue.Margin = new Padding(
                left: 0, top: (int)((TlpCategory.GetRowHeights()[pos.Row] - TxtDescriptionEnValue.Height) / 2), right: 0, bottom: 0);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text = CategoryIsNew
                ? labels.TryGetValue(
                    key: $"{nameof(FormCategoryEdit<TDomain, TCategory>)}New",
                    out string frmCategoryNewText)
                        ? frmCategoryNewText
                        : String.Empty
                : labels.TryGetValue(
                    key: $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",
                    out string frmCategoryEditText)
                        ? frmCategoryEditText
                        : String.Empty;

            BtnOK.Text =
                labels.TryGetValue(
                    key: nameof(BtnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            BtnCancel.Text =
                labels.TryGetValue(
                    key: nameof(BtnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            GrpCategory.Text =
                labels.TryGetValue(key: nameof(GrpCategory),
                    out string grpCategoryText)
                        ? grpCategoryText
                        : String.Empty;

            LblIDCaption.Text =
                labels.TryGetValue(key: nameof(LblIDCaption),
                    out string lblIDCaptionText)
                        ? lblIDCaptionText
                        : String.Empty;

            LblLabelCsCaption.Text =
                labels.TryGetValue(key: nameof(LblLabelCsCaption),
                    out string lblLabelCsCaptionText)
                        ? lblLabelCsCaptionText
                        : String.Empty;

            LblDescriptionCsCaption.Text =
                labels.TryGetValue(key: nameof(LblDescriptionCsCaption),
                    out string lblDescriptionCsCaptiontText)
                        ? lblDescriptionCsCaptiontText
                        : String.Empty;

            LblLabelEnCaption.Text =
                labels.TryGetValue(key: nameof(LblLabelEnCaption),
                    out string lblLabelEnCaptionText)
                        ? lblLabelEnCaptionText
                        : String.Empty;

            LblDescriptionEnCaption.Text =
                labels.TryGetValue(key: nameof(LblDescriptionEnCaption),
                    out string lblDescriptionEnCaptionText)
                        ? lblDescriptionEnCaptionText
                        : String.Empty;

            msgLabelCsIsEmpty =
               labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                       out msgLabelCsIsEmpty)
                           ? msgLabelCsIsEmpty
                           : String.Empty;

            msgDescriptionCsIsEmpty =
               labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                       out msgDescriptionCsIsEmpty)
                           ? msgDescriptionCsIsEmpty
                           : String.Empty;

            msgLabelEnIsEmpty =
               labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                       out msgLabelEnIsEmpty)
                           ? msgLabelEnIsEmpty
                           : String.Empty;

            msgDescriptionEnIsEmpty =
               labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                       out msgDescriptionEnIsEmpty)
                           ? msgDescriptionEnIsEmpty
                           : String.Empty;

            msgLabelCsExists =
               labels.TryGetValue(key: nameof(msgLabelCsExists),
                       out msgLabelCsExists)
                           ? msgLabelCsExists
                           : String.Empty;

            msgDescriptionCsExists =
               labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                       out msgDescriptionCsExists)
                           ? msgDescriptionCsExists
                           : String.Empty;

            msgLabelEnExists =
               labels.TryGetValue(key: nameof(msgLabelEnExists),
                       out msgLabelEnExists)
                           ? msgLabelEnExists
                           : String.Empty;

            msgDescriptionEnExists =
               labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                       out msgDescriptionEnExists)
                           ? msgDescriptionEnExists
                           : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Aktualizuje vlastnosti kategorie plošné domény nebo subpopulace z formuláře pokud jsou platné</para>
        /// <para lang="en">Updates properties of the area domain category or subpopulation category if they are valid</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrácí true pokud zadané vlastnosti jsou platné, jinak false</para>
        /// <para lang="en">If given properties are valid then it returns true else false</para>
        /// </returns>
        private bool UpdateCategory()
        {
            string labelCs = TxtLabelCsValue.Text.Trim();
            string descriptionCs = TxtDescriptionCsValue.Text.Trim();
            string labelEn = TxtLabelEnValue.Text.Trim();
            string descriptionEn = TxtDescriptionEnValue.Text.Trim();

            if (String.IsNullOrEmpty(value: labelCs))
            {
                // LabelCs - prázdné
                // LabelCs - empty
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                // DescriptionCs - prázdné
                // DescriptionCs - empty
                MessageBox.Show(
                    text: msgDescriptionCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                // LabelEn - prázdné
                // LabelEn - empty
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                // DescriptionEn - prázdné
                // DescriptionEn - empty
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if ((Category.LabelCs != labelCs) &&
                (Categories.Where(a => a.LabelCs == labelCs).Any()))
            {
                // LabelCs - duplicitní
                // LabelCs - duplicated
                MessageBox.Show(
                    text: msgLabelCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if ((Category.DescriptionCs != descriptionCs) &&
                (Categories.Where(a => a.DescriptionCs == descriptionCs).Any()))
            {
                // DescriptionCs - duplicitní
                // DescriptionCs - duplicated
                MessageBox.Show(
                    text: msgDescriptionCsExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if ((Category.LabelEn != labelEn) &&
                (Categories.Where(a => a.LabelEn == labelEn).Any()))
            {
                // LabelEn - duplicitní
                // LabelEn - duplicated
                MessageBox.Show(
                    text: msgLabelEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if ((Category.DescriptionEn != descriptionEn) &&
                (Categories.Where(a => a.DescriptionEn == descriptionEn).Any()))
            {
                // DescriptionEn - duplicitní
                // DescriptionEn - duplicated
                MessageBox.Show(
                    text: msgDescriptionEnExists,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Zápis kategorie plošné domény nebo subpopulace
            // Writing an area domain or subpopulation category
            Category.LabelCs = labelCs;
            Category.DescriptionCs = descriptionCs;
            Category.LabelEn = labelEn;
            Category.DescriptionEn = descriptionEn;
            return true;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}