﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro editace číselníkových položek</para>
    /// <para lang="en">Form for editing lookup table items</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormLookupTable
            : Form, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        public bool onEdit;

        /// <summary>
        /// <para lang="cs">Data číselníku</para>
        /// <para lang="en">Lookup table data</para>
        /// </summary>
        private ILookupTable lookupTable;

        /// <summary>
        /// <para lang="cs">Identifikátory zobrazovaných položek</para>
        /// <para lang="en">Displayed items identifiers</para>
        /// </summary>
        private List<Nullable<int>> displayedItemsIds;

        /// <summary>
        /// <para lang="cs">Záznamy v číselnících jsou změněny přímo pomocí UPDATE příkazu (true)
        /// nebo nepřímo pomocí fn_save_ uložené procedury (false)</para>
        /// <para lang="en">Records in lookup tables are changed directly by UPDATE command (true)
        /// or indirectly by fn_save_ stored procedure (false)</para>
        /// </summary>
        private bool directAccess;

        /// <summary>
        /// <para lang="cs">Došlo ke změně v databázi</para>
        /// <para lang="en">A change has been made to the database</para>
        /// </summary>
        private bool databaseChanged;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro editace číselníkových položek</para>
        /// <para lang="en">Form constructor for editing lookup table items</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="lookupTable">
        /// <para lang="cs">Data číselníku</para>
        /// <para lang="en">Lookup tablle data</para>
        /// </param>
        /// <param name="displayedItemsIds">
        /// <para lang="cs">Identifikátory zobrazovaných položek</para>
        /// <para lang="en">Displayed items identifiers</para>
        /// </param>
        /// <param name="directAccess">
        /// <summary>
        /// <para lang="cs">Záznamy v číselnících jsou změněny přímo pomocí UPDATE příkazu (true)
        /// nebo nepřímo pomocí fn_save_ uložené procedury (false)</para>
        /// <para lang="en">Records in lookup tables are changed directly by UPDATE command (true)
        /// or indirectly by fn_save_ stored procedure (false)</para>
        /// </summary>
        /// </param>
        public FormLookupTable(
            Control controlOwner,
            ILookupTable lookupTable,
            List<Nullable<int>> displayedItemsIds,
            bool directAccess = false)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                lookupTable: lookupTable,
                displayedItemsIds: displayedItemsIds,
                directAccess: directAccess);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Data číselníku</para>
        /// <para lang="en">Lookup table data</para>
        /// </summary>
        private ILookupTable LookupTable
        {
            get
            {
                return lookupTable ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(LookupTable)} must not be null.",
                        paramName: nameof(LookupTable));
            }
            set
            {
                lookupTable = value ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(LookupTable)} must not be null.",
                        paramName: nameof(LookupTable));
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikátory zobrazovaných položek</para>
        /// <para lang="en">Displayed items identifiers</para>
        /// </summary>
        private List<Nullable<int>> DisplayedItemsIds
        {
            get
            {
                return
                    displayedItemsIds ?? [];
            }
            set
            {
                displayedItemsIds = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Záznamy v číselnících jsou změněny přímo pomocí UPDATE příkazu (true)
        /// nebo nepřímo pomocí fn_save_ uložené procedury (false)</para>
        /// <para lang="en">Records in lookup tables are changed directly by UPDATE command (true)
        /// or indirectly by fn_save_ stored procedure (false)</para>
        /// </summary>
        public bool DirectAccess
        {
            get
            {
                return directAccess;
            }
            set
            {
                directAccess = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná položka číselníku</para>
        /// <para lang="en">Selected item from lookup table</para>
        /// </summary>
        public ILookupTableEntry SelectedItem
        {
            get
            {
                if (lstLookupTable.SelectedItem == null)
                {
                    return null;
                }

                return (ILookupTableEntry)lstLookupTable.SelectedItem;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                     ? new Dictionary<string, string>
                     {
                        { nameof(FormLookupTable),                      "Editace položek číselníku $1" },
                        { nameof(btnCancel),                            "Zavřít" },
                        { nameof(btnOK),                                "Zapsat do databáze" },
                        { nameof(lblIDCaption),                         "Identifikační číslo:" },
                        { nameof(lblLabelCsCaption),                    "Zkratka (národní):" },
                        { nameof(lblDescriptionCsCaption),              "Popis (národní):" },
                        { nameof(lblLabelEnCaption),                    "Zkratka (anglická):" },
                        { nameof(lblDescriptionEnCaption),              "Popis (anglický):" },
                        { nameof(lstLookupTable),                       "ExtendedLabelCs" },
                        { "grpLookupTableAreaDomain",                   "Plošné domény:" },
                        { "grpLookupTableAreaDomainCategory",           "Kategorie plošných domén:" },
                        { "grpLookupTableArealOrPopulation",            "Charakter objektu lokální hustoty:" },
                        { "grpLookupTableClassificationType",           "Klasifikační typy:" },
                        { "grpLookupTableDefinitionVariant",            "Definiční varianty:" },
                        { "grpLookupTableLDsity",                       "Příspěvky lokálních hustot:" },
                        { "grpLookupTableLDsityObject",                 "Objekty lokálních hustot:" },
                        { "grpLookupTableLDsityObjectGroup",            "Skupiny objektů lokálních hustot:" },
                        { "grpLookupTableLDsityObjectType",             "Typ objektu lokální hustoty:" },
                        { "grpLookupTablePanelRefYearSetGroup",         "Skupiny panelů a roků měření:" },
                        { "grpLookupTableStateOrChange",                "Charakter cílové proměnné:" },
                        { "grpLookupTableSubPopulation",                "Subpopulace:" },
                        { "grpLookupTableSubPopulationCategory",        "Kategorie subpopulací:" },
                        { "grpLookupTableTargetVariable",               "Cílové proměnné:" },
                        { "grpLookupTableUnitOfMeasure",                "Jednotky měření:" },
                        { "grpLookupTableVersion",                      "Verze:" },
                        { "grpLookupTableItemsAreaDomain",               String.Empty },
                        { "grpLookupTableItemsAreaDomainCategory",       String.Empty },
                        { "grpLookupTableItemsArealOrPopulation",        String.Empty },
                        { "grpLookupTableItemsClassificationType",       String.Empty },
                        { "grpLookupTableItemsDefinitionVariant",        String.Empty },
                        { "grpLookupTableItemsLDsity",                   String.Empty },
                        { "grpLookupTableItemsLDsityObject",             String.Empty },
                        { "grpLookupTableItemsLDsityObjectGroup",        String.Empty },
                        { "grpLookupTableItemsLDsityObjectType",         String.Empty },
                        { "grpLookupTableItemsPanelRefYearSetGroup",     String.Empty },
                        { "grpLookupTableItemsStateOrChange",            String.Empty },
                        { "grpLookupTableItemsSubPopulation",            String.Empty },
                        { "grpLookupTableItemsSubPopulationCategory",    String.Empty },
                        { "grpLookupTableItemsTargetVariable",           String.Empty },
                        { "grpLookupTableItemsUnitOfMeasure",            String.Empty },
                        { "grpLookupTableItemsVersion",                  String.Empty },
                        { "msgLabelCsIsEmpty",                          "Zkratka (národní) musí být vyplněna." },
                        { "msgDescriptionCsIsEmpty",                    "Popis (národní) musí být vyplněn." },
                        { "msgLabelEnIsEmpty",                          "Zkratka (anglická) musí být vyplněna." },
                        { "msgDescriptionEnIsEmpty",                    "Popis (anglický) musí být vyplněn." },
                        { "msgNoneSelectedItem",                        "Není vybraná žádná položka číselníku." },
                        { "msgLabelCsExists",                           "Použitá zkratka (národní) již v číselníku existuje." },
                        { "msgDescriptionCsExists",                     "Použitý popis (národní) již v číselníku existuje." },
                        { "msgLabelEnExists",                           "Použitá zkratka (anglická) již v číselníku existuje." },
                        { "msDescriptionEnExists",                      "Použitý popis (anglický) již v číselníku existuje." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormLookupTable),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>
                    {
                        { nameof(FormLookupTable),                      "Changes of items in lookup table $1" },
                        { nameof(btnCancel),                            "Close" },
                        { nameof(btnOK),                                "Write to database" },
                        { nameof(lblIDCaption),                         "ID:" },
                        { nameof(lblLabelCsCaption),                    "Label (national):" },
                        { nameof(lblDescriptionCsCaption),              "Description (national):" },
                        { nameof(lblLabelEnCaption),                    "Label (English):" },
                        { nameof(lblDescriptionEnCaption),              "Description (English):" },
                        { nameof(lstLookupTable),                       "ExtendedLabelEn" },
                        { "grpLookupTableAreaDomain",                   "Area domains:" },
                        { "grpLookupTableAreaDomainCategory",           "Area domain categories:" },
                        { "grpLookupTableArealOrPopulation",            "Local density object character:" },
                        { "grpLookupTableClassificationType",           "Classification type:" },
                        { "grpLookupTableDefinitionVariant",            "Definition variants:" },
                        { "grpLookupTableLDsity",                       "Local density contributions:" },
                        { "grpLookupTableLDsityObject",                 "Local density objects:" },
                        { "grpLookupTableLDsityObjectGroup",            "Local density object groups:" },
                        { "grpLookupTableLDsityObjectType",             "Local density object type:" },
                        { "grpLookupTablePanelRefYearSetGroup",         "Skupiny panelů a roků měření:" },
                        { "grpLookupTableStateOrChange",                "Target variable character:" },
                        { "grpLookupTableSubPopulation",                "Subpopulations:" },
                        { "grpLookupTableSubPopulationCategory",        "Subpopulation categories:" },
                        { "grpLookupTableTargetVariable",               "Target variables:" },
                        { "grpLookupTableUnitOfMeasure",                "Units of measure:" },
                        { "grpLookupTableVersion",                      "Versions:" },
                        { "grpLookupTableItemsAreaDomain",               String.Empty },
                        { "grpLookupTableItemsAreaDomainCategory",       String.Empty },
                        { "grpLookupTableItemsArealOrPopulation",        String.Empty },
                        { "grpLookupTableItemsClassificationType",       String.Empty },
                        { "grpLookupTableItemsDefinitionVariant",        String.Empty },
                        { "grpLookupTableItemsLDsity",                   String.Empty },
                        { "grpLookupTableItemsLDsityObject",             String.Empty },
                        { "grpLookupTableItemsLDsityObjectGroup",        String.Empty },
                        { "grpLookupTableItemsLDsityObjectType",         String.Empty },
                        { "grpLookupTableItemsPanelRefYearSetGroup",     String.Empty },
                        { "grpLookupTableItemsStateOrChange",            String.Empty },
                        { "grpLookupTableItemsSubPopulation",            String.Empty },
                        { "grpLookupTableItemsSubPopulationCategory",    String.Empty },
                        { "grpLookupTableItemsTargetVariable",           String.Empty },
                        { "grpLookupTableItemsUnitOfMeasure",            String.Empty },
                        { "grpLookupTableItemsVersion",                  String.Empty },
                        { "msgLabelCsIsEmpty",                          "Label (national) cannot be empty." },
                        { "msgDescriptionCsIsEmpty",                    "Description (national) cannot be empty." },
                        { "msgLabelEnIsEmpty",                          "Label (English) cannot be empty." },
                        { "msgDescriptionEnIsEmpty",                    "Description (English) cannot be empty." },
                        { "msgNoneSelectedItem",                        "No item selected from lookup table." },
                        { "msgLabelCsExists",                           "This label (national) already exists in lookup table." },
                        { "msgLabelEnExists",                           "This label (English) already exists in lookup table." },
                        { "msgDescriptionCsExists",                     "This description (national) already exists in lookup table." },
                        { "msDescriptionEnExists",                      "This description (English) already exists in lookup table." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormLookupTable),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro editace číselníkových položek</para>
        /// <para lang="en">Initializing the form for editing lookup table items</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="lookupTable">
        /// <para lang="cs">Data číselníku</para>
        /// <para lang="en">Lookup tablle data</para>
        /// </param>
        /// <param name="displayedItemsIds">
        /// <para lang="cs">Identifikátory zobrazovaných položek</para>
        /// <para lang="en">Displayed items identifiers</para>
        /// </param>
        /// <param name="directAccess">
        /// <summary>
        /// <para lang="cs">Záznamy v číselnících jsou změněny přímo pomocí UPDATE příkazu (true)
        /// nebo nepřímo pomocí fn_save_ uložené procedury (false)</para>
        /// <para lang="en">Records in lookup tables are changed directly by UPDATE command (true)
        /// or indirectly by fn_save_ stored procedure (false)</para>
        /// </summary>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ILookupTable lookupTable,
            List<Nullable<int>> displayedItemsIds,
            bool directAccess)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            LookupTable = lookupTable;

            DisplayedItemsIds = displayedItemsIds;

            DirectAccess = directAccess;

            databaseChanged = false;

            InitializeListBoxLookupTable();

            InitializeLabels();

            SetStyle();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                    {
                        databaseChanged = WriteToDatabase() | databaseChanged;
                        LookupTable.ReLoad(
                            condition: LookupTable.Condition,
                            limit: LookupTable.Limit,
                            extended: LookupTable.Extended);
                        int selectedIndex = lstLookupTable.SelectedIndex;
                        InitializeListBoxLookupTable(selectedIndex: selectedIndex);
                    });

            btnCancel.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = databaseChanged ? DialogResult.OK : DialogResult.Cancel;
                   Close();
               });

            lstLookupTable.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                    {
                        if (!onEdit)
                        {
                            SelectedIndexChanged();
                        }
                    });

            txtLabelCsValue.TextChanged += new EventHandler(
                (sender, e) =>
                {
                    EnableButtonWriteToDatabase();
                });

            txtDescriptionCsValue.TextChanged += new EventHandler(
                (sender, e) =>
                {
                    EnableButtonWriteToDatabase();
                });

            txtLabelEnValue.TextChanged += new EventHandler(
                (sender, e) =>
                {
                    EnableButtonWriteToDatabase();
                });

            txtDescriptionEnValue.TextChanged += new EventHandler(
                (sender, e) =>
                {
                    EnableButtonWriteToDatabase();
                });

            btnOK.Focus();
        }


        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            TableLayoutPanelCellPosition pos;

            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;


            grpLookupTable.Font = Setting.GroupBoxFont;
            grpLookupTable.ForeColor = Setting.GroupBoxForeColor;

            grpLookupTableItems.Font = Setting.GroupBoxFont;
            grpLookupTableItems.ForeColor = Setting.GroupBoxForeColor;


            lstLookupTable.Font = Setting.ListBoxFont;
            lstLookupTable.ForeColor = Setting.ListBoxForeColor;


            lblIDCaption.Font = Setting.LabelFont;
            lblIDCaption.ForeColor = Setting.LabelForeColor;

            lblLabelCsCaption.Font = Setting.LabelFont;
            lblLabelCsCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionCsCaption.Font = Setting.LabelFont;
            lblDescriptionCsCaption.ForeColor = Setting.LabelForeColor;

            lblLabelEnCaption.Font = Setting.LabelFont;
            lblLabelEnCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionEnCaption.Font = Setting.LabelFont;
            lblDescriptionEnCaption.ForeColor = Setting.LabelForeColor;


            lblIDValue.Font = Setting.LabelValueFont;
            lblIDValue.ForeColor = Setting.LabelValueForeColor;


            txtLabelCsValue.Font = Setting.TextBoxFont;
            txtLabelCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpDescription.GetCellPosition(control: txtLabelCsValue);
            txtLabelCsValue.Margin = new Padding(
                left: 0, top: (int)((tlpDescription.GetRowHeights()[pos.Row] - txtLabelCsValue.Height) / 2), right: 0, bottom: 0);

            txtDescriptionCsValue.Font = Setting.TextBoxFont;
            txtDescriptionCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpDescription.GetCellPosition(control: txtDescriptionCsValue);
            txtDescriptionCsValue.Margin = new Padding(
                left: 0, top: (int)((tlpDescription.GetRowHeights()[pos.Row] - txtDescriptionCsValue.Height) / 2), right: 0, bottom: 0);

            txtLabelEnValue.Font = Setting.TextBoxFont;
            txtLabelEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpDescription.GetCellPosition(control: txtLabelEnValue);
            txtLabelEnValue.Margin = new Padding(
                left: 0, top: (int)((tlpDescription.GetRowHeights()[pos.Row] - txtLabelEnValue.Height) / 2), right: 0, bottom: 0);

            txtDescriptionEnValue.Font = Setting.TextBoxFont;
            txtDescriptionEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpDescription.GetCellPosition(control: txtDescriptionEnValue);
            txtDescriptionEnValue.Margin = new Padding(
                left: 0, top: (int)((tlpDescription.GetRowHeights()[pos.Row] - txtDescriptionEnValue.Height) / 2), right: 0, bottom: 0);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro editace číselníkových položek</para>
        /// <para lang="en">Initializing form labels for editing lookup table items</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: nameof(FormLookupTable),
                    out string frmLookupTableText)
                        ? frmLookupTableText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(
                    key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(
                    key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            lblIDCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblIDCaption),
                    out string lblIDCaptionText)
                ? lblIDCaptionText
                : String.Empty;

            lblLabelCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelCsCaption),
                    out string lblLabelCsCaptionText)
                ? lblLabelCsCaptionText
                : String.Empty;

            lblDescriptionCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionCsCaption),
                    out string lblDescriptionCsCaptionText)
                ? lblDescriptionCsCaptionText
                : String.Empty;

            lblLabelEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelEnCaption),
                    out string lblLabelEnCaptionText)
                ? lblLabelEnCaptionText
                : String.Empty;

            lblDescriptionEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionEnCaption),
                    out string lblDescriptionEnCaptionText)
                ? lblDescriptionEnCaptionText
                : String.Empty;

            onEdit = true;
            lstLookupTable.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLookupTable),
                    out string lstLookupTableDisplayMember)
                ? lstLookupTableDisplayMember
                : ID;
            onEdit = false;

            // c_area_domain
            if (LookupTable.GetType() == typeof(TDAreaDomainList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDAreaDomainList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableAreaDomain",
                        out string grpLookupTableAreaDomainText)
                    ? grpLookupTableAreaDomainText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsAreaDomain",
                        out string grpLookupTableItemsAreaDomainText)
                    ? grpLookupTableItemsAreaDomainText
                    : String.Empty;
            }

            // c_area_domain_category
            else if (LookupTable.GetType() == typeof(TDAreaDomainCategoryList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDAreaDomainCategoryList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableAreaDomainCategory",
                        out string grpLookupTableAreaDomainCategoryText)
                    ? grpLookupTableAreaDomainCategoryText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsAreaDomainCategory",
                        out string grpLookupTableItemsAreaDomainCategoryText)
                    ? grpLookupTableItemsAreaDomainCategoryText
                    : String.Empty;
            }

            // c_areal_or_population
            else if (LookupTable.GetType() == typeof(TDArealOrPopulationList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDArealOrPopulationList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableArealOrPopulation",
                        out string grpLookupTableArealOrPopulationText)
                    ? grpLookupTableArealOrPopulationText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsArealOrPopulation",
                        out string grpLookupTableItemsArealOrPopulationText)
                    ? grpLookupTableItemsArealOrPopulationText
                    : String.Empty;
            }

            // c_classification_type
            else if (LookupTable.GetType() == typeof(TDClassificationTypeList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDClassificationTypeList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableClassificationType",
                        out string grpLookupTableClassificationTypeText)
                    ? grpLookupTableClassificationTypeText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsClassificationType",
                        out string grpLookupTableItemsClassificationTypeText)
                    ? grpLookupTableItemsClassificationTypeText
                    : String.Empty;
            }

            // c_definition_variant
            else if (LookupTable.GetType() == typeof(TDDefinitionVariantList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDDefinitionVariantList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableDefinitionVariant",
                        out string grpLookupTableDefinitionVariantText)
                    ? grpLookupTableDefinitionVariantText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsDefinitionVariant",
                        out string grpLookupTableItemsDefinitionVariantText)
                    ? grpLookupTableItemsDefinitionVariantText
                    : String.Empty;
            }

            // c_ldsity
            else if (LookupTable.GetType() == typeof(TDLDsityList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDLDsityList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableLDsity",
                        out string grpLookupTableLDsityText)
                    ? grpLookupTableLDsityText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsLDsity",
                        out string grpLookupTableItemsLDsityText)
                    ? grpLookupTableItemsLDsityText
                    : String.Empty;
            }

            // c_ldsity_object
            else if (LookupTable.GetType() == typeof(TDLDsityObjectList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDLDsityObjectList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableLDsityObject",
                        out string grpLookupTableLDsityObjectText)
                    ? grpLookupTableLDsityObjectText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsLDsityObject",
                        out string grpLookupTableItemsLDsityObjectText)
                    ? grpLookupTableItemsLDsityObjectText
                    : String.Empty;
            }

            // c_ldsity_object_group
            else if (LookupTable.GetType() == typeof(TDLDsityObjectGroupList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDLDsityObjectGroupList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableLDsityObjectGroup",
                        out string grpLookupTableLDsityObjectGroupText)
                    ? grpLookupTableLDsityObjectGroupText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsLDsityObjectGroup",
                        out string grpLookupTableItemsLDsityObjectGroupText)
                    ? grpLookupTableItemsLDsityObjectGroupText
                    : String.Empty;
            }

            // c_ldsity_object_type
            else if (LookupTable.GetType() == typeof(TDLDsityObjectTypeList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDLDsityObjectTypeList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableLDsityObjectType",
                        out string grpLookupTableLDsityObjectTypeText)
                    ? grpLookupTableLDsityObjectTypeText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsLDsityObjectType",
                        out string grpLookupTableItemsLDsityObjectTypeText)
                    ? grpLookupTableItemsLDsityObjectTypeText
                    : String.Empty;
            }

            // c_panel_refyearset_group
            else if (LookupTable.GetType() == typeof(TDPanelRefYearSetGroupList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDPanelRefYearSetGroupList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTablePanelRefYearSetGroup",
                        out string grpLookupTablePanelRefYearSetGroupText)
                    ? grpLookupTablePanelRefYearSetGroupText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsPanelRefYearSetGroup",
                        out string grpLookupTableItemsPanelRefYearSetGroupText)
                    ? grpLookupTableItemsPanelRefYearSetGroupText
                    : String.Empty;
            }

            // c_state_or_change
            else if (LookupTable.GetType() == typeof(TDStateOrChangeList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDStateOrChangeList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableStateOrChange",
                        out string grpLookupTableStateOrChangeText)
                    ? grpLookupTableStateOrChangeText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsStateOrChange",
                        out string grpLookupTableItemsStateOrChangeText)
                    ? grpLookupTableItemsStateOrChangeText
                    : String.Empty;
            }

            // c_subpopulation
            else if (LookupTable.GetType() == typeof(TDSubPopulationList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDSubPopulationList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableSubPopulation",
                        out string grpLookupTableSubPopulationText)
                    ? grpLookupTableSubPopulationText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsSubPopulation",
                        out string grpLookupTableItemsSubPopulationText)
                    ? grpLookupTableItemsSubPopulationText
                    : String.Empty;
            }

            // c_subpopulation_category
            else if (LookupTable.GetType() == typeof(TDSubPopulationCategoryList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDSubPopulationCategoryList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableSubPopulationCategory",
                        out string grpLookupTableSubPopulationCategoryText)
                    ? grpLookupTableSubPopulationCategoryText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsSubPopulationCategory",
                        out string grpLookupTableItemsSubPopulationCategoryText)
                    ? grpLookupTableItemsSubPopulationCategoryText
                    : String.Empty;
            }

            // c_target_variable
            else if (LookupTable.GetType() == typeof(TDTargetVariableList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDTargetVariableList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableTargetVariable",
                        out string grpLookupTableTargetVariableText)
                    ? grpLookupTableTargetVariableText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsTargetVariable",
                        out string grpLookupTableItemsTargetVariableText)
                    ? grpLookupTableItemsTargetVariableText
                    : String.Empty;
            }

            // c_unit_of_measure
            else if (LookupTable.GetType() == typeof(TDUnitOfMeasureList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDUnitOfMeasureList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableUnitOfMeasure",
                        out string grpLookupTableUnitOfMeasureText)
                    ? grpLookupTableUnitOfMeasureText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsUnitOfMeasure",
                        out string grpLookupTableItemsUnitOfMeasureText)
                    ? grpLookupTableItemsUnitOfMeasureText
                    : String.Empty;
            }

            // c_version
            else if (LookupTable.GetType() == typeof(TDVersionList))
            {
                Text = Text.Replace(oldValue: "$1", newValue: TDVersionList.Name);

                grpLookupTable.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableVersion",
                        out string grpLookupTableVersionText)
                    ? grpLookupTableVersionText
                    : String.Empty;

                grpLookupTableItems.Text =
                    labels.TryGetValue(
                        key: "grpLookupTableItemsVersion",
                        out string grpLookupTableItemsVersionText)
                    ? grpLookupTableItemsVersionText
                    : String.Empty;
            }

            // unknown lookup table
            else
            {
                throw new Exception(message: "Unknown lookup table type.");
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ListBox "Položky číselníku"</para>
        /// <para lang="en">Initializing items in ListBox "Lookup table items"</para>
        /// </summary>
        /// <param name="selectedIndex">
        /// <para lang="cs">Index vybrané pložky</para>
        /// <para lang="en">Selected item index</para>
        /// </param>
        private void InitializeListBoxLookupTable(int selectedIndex = 0)
        {
            onEdit = true;

            #region DataSource

            // c_area_domain
            if (LookupTable.GetType() == typeof(TDAreaDomainList))
            {
                List<TDAreaDomain> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDAreaDomainList)LookupTable).Items :
                     ((TDAreaDomainList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDAreaDomain>();
                lstLookupTable.DataSource = source;
            }

            // c_area_domain_category
            else if (LookupTable.GetType() == typeof(TDAreaDomainCategoryList))
            {
                List<TDAreaDomainCategory> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDAreaDomainCategoryList)LookupTable).Items :
                     ((TDAreaDomainCategoryList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDAreaDomainCategory>();
                lstLookupTable.DataSource = source;
            }

            // c_areal_or_population
            else if (LookupTable.GetType() == typeof(TDArealOrPopulationList))
            {
                List<TDArealOrPopulation> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDArealOrPopulationList)LookupTable).Items :
                     ((TDArealOrPopulationList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDArealOrPopulation>();
                lstLookupTable.DataSource = source;

                lblLabelCsCaption.Visible = false;
                lblDescriptionCsCaption.Visible = false;
                txtLabelCsValue.Visible = false;
                txtDescriptionCsValue.Visible = false;
            }

            // c_classification_type
            else if (LookupTable.GetType() == typeof(TDClassificationTypeList))
            {
                List<TDClassificationType> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDClassificationTypeList)LookupTable).Items :
                     ((TDClassificationTypeList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDClassificationType>();
                lstLookupTable.DataSource = source;

                lblLabelCsCaption.Visible = false;
                lblDescriptionCsCaption.Visible = false;
                txtLabelCsValue.Visible = false;
                txtDescriptionCsValue.Visible = false;
            }

            // c_definition_variant
            else if (LookupTable.GetType() == typeof(TDDefinitionVariantList))
            {
                List<TDDefinitionVariant> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDDefinitionVariantList)LookupTable).Items :
                     ((TDDefinitionVariantList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDDefinitionVariant>();
                lstLookupTable.DataSource = source;
            }

            // c_ldsity
            else if (LookupTable.GetType() == typeof(TDLDsityList))
            {
                List<TDLDsity> source =
                     (DisplayedItemsIds.Count == 0) ?
                     ((TDLDsityList)LookupTable).Items :
                      ((TDLDsityList)LookupTable).Items
                      .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                      .ToList<TDLDsity>();
                lstLookupTable.DataSource = source;
            }

            // c_ldsity_object
            else if (LookupTable.GetType() == typeof(TDLDsityObjectList))
            {
                List<TDLDsityObject> source =
                     (DisplayedItemsIds.Count == 0) ?
                     ((TDLDsityObjectList)LookupTable).Items :
                      ((TDLDsityObjectList)LookupTable).Items
                      .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                      .ToList<TDLDsityObject>();
                lstLookupTable.DataSource = source;
            }

            // c_ldsity_object_group
            else if (LookupTable.GetType() == typeof(TDLDsityObjectGroupList))
            {
                List<TDLDsityObjectGroup> source =
                     (DisplayedItemsIds.Count == 0) ?
                     ((TDLDsityObjectGroupList)LookupTable).Items :
                      ((TDLDsityObjectGroupList)LookupTable).Items
                      .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                      .ToList<TDLDsityObjectGroup>();
                lstLookupTable.DataSource = source;
            }

            // c_ldsity_object_type
            else if (LookupTable.GetType() == typeof(TDLDsityObjectTypeList))
            {
                List<TDLDsityObjectType> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDLDsityObjectTypeList)LookupTable).Items :
                     ((TDLDsityObjectTypeList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDLDsityObjectType>();
                lstLookupTable.DataSource = source;

                lblLabelCsCaption.Visible = false;
                lblDescriptionCsCaption.Visible = false;
                txtLabelCsValue.Visible = false;
                txtDescriptionCsValue.Visible = false;
            }

            // c_panel_refyearset_group
            else if (LookupTable.GetType() == typeof(TDPanelRefYearSetGroupList))
            {
                List<TDPanelRefYearSetGroup> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDPanelRefYearSetGroupList)LookupTable).Items :
                     ((TDPanelRefYearSetGroupList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDPanelRefYearSetGroup>();
                lstLookupTable.DataSource = source;
            }

            // c_state_or_change
            else if (LookupTable.GetType() == typeof(TDStateOrChangeList))
            {
                List<TDStateOrChange> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDStateOrChangeList)LookupTable).Items :
                     ((TDStateOrChangeList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDStateOrChange>();
                lstLookupTable.DataSource = source;

                lblLabelEnCaption.Visible = false;
                lblDescriptionEnCaption.Visible = false;
                txtLabelEnValue.Visible = false;
                txtDescriptionEnValue.Visible = false;
            }

            // c_subpopulation
            else if (LookupTable.GetType() == typeof(TDSubPopulationList))
            {
                List<TDSubPopulation> source =
                    (DisplayedItemsIds.Count == 0) ?
                    ((TDSubPopulationList)LookupTable).Items :
                     ((TDSubPopulationList)LookupTable).Items
                     .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                     .ToList<TDSubPopulation>();
                lstLookupTable.DataSource = source;
            }

            // c_subpopulation_category
            else if (LookupTable.GetType() == typeof(TDSubPopulationCategoryList))
            {
                List<TDSubPopulationCategory> source =
                   (DisplayedItemsIds.Count == 0) ?
                   ((TDSubPopulationCategoryList)LookupTable).Items :
                    ((TDSubPopulationCategoryList)LookupTable).Items
                    .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                    .ToList<TDSubPopulationCategory>();
                lstLookupTable.DataSource = source;
            }

            // c_target_variable
            else if (LookupTable.GetType() == typeof(TDTargetVariableList))
            {
                List<TDTargetVariable> source =
                   (DisplayedItemsIds.Count == 0) ?
                   ((TDTargetVariableList)LookupTable).Items :
                    ((TDTargetVariableList)LookupTable).Items
                    .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                    .ToList<TDTargetVariable>();
                lstLookupTable.DataSource = source;
            }

            // c_unit_of_measure
            else if (LookupTable.GetType() == typeof(TDUnitOfMeasureList))
            {
                List<TDUnitOfMeasure> source =
                   (DisplayedItemsIds.Count == 0) ?
                   ((TDUnitOfMeasureList)LookupTable).Items :
                    ((TDUnitOfMeasureList)LookupTable).Items
                    .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                    .ToList<TDUnitOfMeasure>();
                lstLookupTable.DataSource = source;
            }

            // c_version
            else if (LookupTable.GetType() == typeof(TDVersionList))
            {
                List<TDVersion> source =
                   (DisplayedItemsIds.Count == 0) ?
                   ((TDVersionList)LookupTable).Items :
                    ((TDVersionList)LookupTable).Items
                    .Where(a => DisplayedItemsIds.Contains(item: a.Id))
                    .ToList<TDVersion>();
                lstLookupTable.DataSource = source;
            }

            // unknown lookup table
            else
            {
                throw new Exception(message: "Unknown lookup table type.");
            }

            #endregion DataSource

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            lstLookupTable.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLookupTable),
                    out string lstLookupTableDisplayMember)
                        ? lstLookupTableDisplayMember
                        : ID;

            // Vybere položku v seznamu
            // It selects item of the list
            if (lstLookupTable.Items.Count > selectedIndex)
            {
                lstLookupTable.SelectedIndex = selectedIndex;
            }

            onEdit = false;

            // Pokud je v seznamu pouze jedna položka, seznam není třeba zobrazovat
            // If there is only one item in the list, it is not necessary to display list
            splMain.Panel1Collapsed = lstLookupTable.Items.Count <= 1;

            SelectedIndexChanged();
        }

        /// <summary>
        /// <para lang="cs">Spouští se v případě, že byla v ListBoxu LookupTable vybrána položka</para>
        /// <para lang="en">It fires if the item was selected in the LookupTable ListBox</para>
        /// </summary>
        private void SelectedIndexChanged()
        {
            if (lstLookupTable.SelectedItem == null)
            {
                grpLookupTableItems.Visible = false;
                return;
            }

            grpLookupTableItems.Visible = true;

            ILookupTableEntry item = (ILookupTableEntry)lstLookupTable.SelectedItem;
            lblIDValue.Text = item.Id.ToString();
            txtLabelCsValue.Text = item.LabelCs;
            txtDescriptionCsValue.Text = item.DescriptionCs;
            txtLabelEnValue.Text = item.LabelEn;
            txtDescriptionEnValue.Text = item.DescriptionEn;

            EnableButtonWriteToDatabase();
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost tlačítka pro zápis do databáze</para>
        /// <para lang="en">Set accessibility of the button for writing changes into database</para>
        /// </summary>
        private void EnableButtonWriteToDatabase()
        {
            if (lstLookupTable.SelectedItem == null)
            {
                btnOK.Enabled = false;
                return;
            }

            ILookupTableEntry item = (ILookupTableEntry)lstLookupTable.SelectedItem;

            txtLabelCsValue.BackColor =
                (txtLabelCsValue.Text.Trim() == item.LabelCs) ?
                System.Drawing.SystemColors.Window :
                System.Drawing.SystemColors.Info;

            txtDescriptionCsValue.BackColor =
                (txtDescriptionCsValue.Text.Trim() == item.DescriptionCs) ?
                System.Drawing.SystemColors.Window :
                System.Drawing.SystemColors.Info;

            txtLabelEnValue.BackColor =
                (txtLabelEnValue.Text.Trim() == item.LabelEn) ?
                System.Drawing.SystemColors.Window :
                System.Drawing.SystemColors.Info;

            txtDescriptionEnValue.BackColor =
                (txtDescriptionEnValue.Text.Trim() == item.DescriptionEn) ?
                System.Drawing.SystemColors.Window :
                System.Drawing.SystemColors.Info;

            btnOK.Enabled = !(
                (txtLabelCsValue.Text.Trim() == item.LabelCs) &&
                (txtDescriptionCsValue.Text.Trim() == item.DescriptionCs) &&
                (txtLabelEnValue.Text.Trim() == item.LabelEn) &&
                (txtDescriptionEnValue.Text.Trim() == item.DescriptionEn));
        }

        /// <summary>
        /// <para lang="cs">Zápis číselníku do databáze</para>
        /// <para lang="en">Writing lookup table into database</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool WriteToDatabase()
        {
            string labelCs = txtLabelCsValue.Text.Trim();
            string descriptionCs = txtDescriptionCsValue.Text.Trim();
            string labelEn = txtLabelEnValue.Text.Trim();
            string descriptionEn = txtDescriptionEnValue.Text.Trim();

            // LabelCs - prázdné
            // LabelCs - empty
            if (String.IsNullOrEmpty(value: labelCs))
            {
                Dictionary<string, string> messages = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsIsEmpty",
                            out string msgLabelCsIsEmptyText)
                                ? msgLabelCsIsEmptyText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - prázdné
            // DescriptionCs - empty
            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                Dictionary<string, string> messages = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsIsEmpty",
                            out string msgDescriptionCsIsEmptyText)
                                ? msgDescriptionCsIsEmptyText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - prázdné
            // LabelEn - empty
            if (String.IsNullOrEmpty(value: labelEn))
            {
                Dictionary<string, string> messages = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnIsEmpty",
                            out string msgLabelEnIsEmptyText)
                                ? msgLabelEnIsEmptyText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - prázdné
            // DescriptionEn - empty
            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                Dictionary<string, string> messages = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnIsEmpty",
                            out string msgDescriptionEnIsEmptyText)
                                ? msgDescriptionEnIsEmptyText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Provedení zápisu
            // Making an entry

            // c_area_domain
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDAreaDomain))
            {
                TDAreaDomain item = (TDAreaDomain)lstLookupTable.SelectedItem;
                return UpdateAreaDomain(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_area_domain_category
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDAreaDomainCategory))
            {
                TDAreaDomainCategory item = (TDAreaDomainCategory)lstLookupTable.SelectedItem;
                return UpdateAreaDomainCategory(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_areal_or_population
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDArealOrPopulation))
            {
                TDArealOrPopulation item = (TDArealOrPopulation)lstLookupTable.SelectedItem;
                return UpdateArealOrPopulation(
                    item: item,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_classification_type
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDClassificationType))
            {
                TDClassificationType item = (TDClassificationType)lstLookupTable.SelectedItem;
                return UpdateClassificationType(
                    item: item,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_definition_variant
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDDefinitionVariant))
            {
                TDDefinitionVariant item = (TDDefinitionVariant)lstLookupTable.SelectedItem;
                return UpdateDefinitionVariant(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_ldsity
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDLDsity))
            {
                TDLDsity item = (TDLDsity)lstLookupTable.SelectedItem;
                return UpdateLDsity(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_ldsity_object
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDLDsityObject))
            {
                TDLDsityObject item = (TDLDsityObject)lstLookupTable.SelectedItem;
                return UpdateLDsityObject(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_ldsity_object_group
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDLDsityObjectGroup))
            {
                TDLDsityObjectGroup item = (TDLDsityObjectGroup)lstLookupTable.SelectedItem;
                return UpdateLDsityObjectGroup(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_ldsity_object_type
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDLDsityObjectType))
            {
                TDLDsityObjectType item = (TDLDsityObjectType)lstLookupTable.SelectedItem;
                return UpdateLDsityObjectType(
                    item: item,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_panel_refyearset_group
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDPanelRefYearSetGroup))
            {
                TDPanelRefYearSetGroup item = (TDPanelRefYearSetGroup)lstLookupTable.SelectedItem;
                return UpdatePanelReferenceYearSetGroup(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_state_or_change
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDStateOrChange))
            {
                TDStateOrChange item = (TDStateOrChange)lstLookupTable.SelectedItem;
                return UpdateStateOrChange(
                    item: item,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_sub_population
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDSubPopulation))
            {
                TDSubPopulation item = (TDSubPopulation)lstLookupTable.SelectedItem;
                return UpdateSubPopulation(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_sub_population_category
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDSubPopulationCategory))
            {
                TDSubPopulationCategory item = (TDSubPopulationCategory)lstLookupTable.SelectedItem;
                return UpdateSubPopulationCategory(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_target_variable
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDTargetVariable))
            {
                return UpdateTargetVariables(
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_unit_of_measure
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDUnitOfMeasure))
            {
                TDUnitOfMeasure item = (TDUnitOfMeasure)lstLookupTable.SelectedItem;
                return UpdateUnitOfMeasure(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // c_version
            if (lstLookupTable.SelectedItem.GetType() == typeof(TDVersion))
            {
                TDVersion item = (TDVersion)lstLookupTable.SelectedItem;
                return UpdateVersion(
                    item: item,
                    labelCs: labelCs, descriptionCs: descriptionCs,
                    labelEn: labelEn, descriptionEn: descriptionEn);
            }

            // unknown lookup table
            else
            {
                throw new ArgumentException(
                    message: "Unknown lookup table type.");
            }
        }

        #endregion Methods


        #region Update Methods

        /// <summary>
        /// <para lang="cs">Zápis plošné domény</para>
        /// <para lang="en">Writing an area domain</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Plošná doména</para>
        /// <para lang="en">Area domain</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateAreaDomain(TDAreaDomain item,
           string labelCs, string descriptionCs,
           string labelEn, string descriptionEn)
        {
            // Žádná plošné doména není vybraná
            // No area domain is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná plošné doména není v databázové tabulce
            // The selected area domain is not in the database table
            else if (!Database.STargetData.CAreaDomain.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CAreaDomain.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                                ? msgLabelCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CAreaDomain.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(key: "msgDescriptionCsExists",
                        out string msgDescriptionCsExistsText)
                            ? msgDescriptionCsExistsText
                            : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CAreaDomain.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                                ? msgLabelEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CAreaDomain.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                                ? msgDescriptionEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CAreaDomain.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CAreaDomain.CommandText,
                            caption: Database.STargetData.CAreaDomain.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveArealOrPop.Execute(
                           database: Database,
                           arealOrPopulationId: (int)TDArealOrPopulationEnum.AreaDomain,
                           labelCs: item.LabelCs,
                           descriptionCs: item.DescriptionCs,
                           labelEn: item.LabelEn,
                           descriptionEn: item.DescriptionEn,
                           classificationType: item.ClassificationTypeValue,
                           id: item.Id);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveArealOrPop.CommandText,
                            caption: TDFunctions.FnSaveArealOrPop.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis kategorie plošné domény</para>
        /// <para lang="en">Writing an area domain category</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Kategorie plošné domény</para>
        /// <para lang="en">Area domain category</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateAreaDomainCategory(TDAreaDomainCategory item,
           string labelCs, string descriptionCs,
           string labelEn, string descriptionEn)
        {
            // Žádná kategorie plošné domény není vybraná
            // No area domain category is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná kategorie plošné domény není v databázové tabulce
            // The selected area domain category is not in the database table
            else if (!Database.STargetData.CAreaDomainCategory.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní (uvnitř skupiny určené plošnou doménou)
            // LabelCs - duplicated (within a group specified by an area domain)
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CAreaDomainCategory.Items
                .Where(a => (a.AreaDomainId ?? 0) == (item.AreaDomainId ?? 0))
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                                ? msgLabelCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní (uvnitř skupiny určené plošnou doménou)
            // DescriptionCs - duplicated (within a group specified by an area domain)
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CAreaDomainCategory.Items
                .Where(a => (a.AreaDomainId ?? 0) == (item.AreaDomainId ?? 0))
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                                ? msgDescriptionCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní (uvnitř skupiny určené plošnou doménou)
            // LabelEn - duplicated (within a group specified by an area domain)
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CAreaDomainCategory.Items
                .Where(a => (a.AreaDomainId ?? 0) == (item.AreaDomainId ?? 0))
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní (uvnitř skupiny určené plošnou doménou)
            // DescriptionEn - duplicated (within a group specified by an area domain)
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CAreaDomainCategory.Items
                .Where(a => (a.AreaDomainId ?? 0) == (item.AreaDomainId ?? 0))
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CAreaDomainCategory.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CAreaDomainCategory.CommandText,
                            caption: Database.STargetData.CAreaDomainCategory.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveCategory.Execute(
                        database: Database,
                        arealOrPopulationId: (int)TDArealOrPopulationEnum.AreaDomain,
                        labelCs: item.LabelCs,
                        descriptionCs: item.DescriptionCs,
                        labelEn: item.LabelEn,
                        descriptionEn: item.DescriptionEn,
                        parentId: null,
                        id: item.Id);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveCategory.CommandText,
                            caption: TDFunctions.FnSaveCategory.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis charakteru objektu lokální hustoty</para>
        /// <para lang="en">Writing a character of a local density object</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Charakter objektu lokální hustoty</para>
        /// <para lang="en">Character of a local density object</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateArealOrPopulation(TDArealOrPopulation item,
           string labelEn, string descriptionEn)
        {
            // Žádný charakter objektu lokální hustoty není vybraný
            // No character of a local density object is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraný charakter objektu lokální hustoty není v databázové tabulce
            // The selected character of the local density object is not in the database table
            else if (!Database.STargetData.CArealOrPopulation.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CArealOrPopulation.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CArealOrPopulation.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                Database.STargetData.CArealOrPopulation.Update(item: item);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: Database.STargetData.CArealOrPopulation.CommandText,
                        caption: Database.STargetData.CArealOrPopulation.TableName,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis klasifikačního typu</para>
        /// <para lang="en">Writing a classification type</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Klasifikační typ</para>
        /// <para lang="en">Classification type</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateClassificationType(TDClassificationType item,
           string labelEn, string descriptionEn)
        {
            // Žádný klasifikační typ není vybraný
            // No classification type is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraný klasifikační typ není v databázové tabulce
            // The selected classification type is not in the database table
            else if (!Database.STargetData.CClassificationType.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CClassificationType.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CClassificationType.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                Database.STargetData.CClassificationType.Update(item: item);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: Database.STargetData.CClassificationType.CommandText,
                        caption: Database.STargetData.CClassificationType.TableName,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis definiční varinaty</para>
        /// <para lang="en">Writing a definition variant</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Definiční varianta</para>
        /// <para lang="en">Definition variant</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateDefinitionVariant(TDDefinitionVariant item,
          string labelCs, string descriptionCs,
          string labelEn, string descriptionEn)
        {
            // Žádná definiční varianta není vybraná
            // No definition variant is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná definiční varianta není v databázové tabulce
            // The selected definition variant is not in the database table
            else if (!Database.STargetData.CDefinitionVariant.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CDefinitionVariant.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                        ? msgLabelCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CDefinitionVariant.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                        ? msgDescriptionCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CDefinitionVariant.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CDefinitionVariant.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CDefinitionVariant.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CDefinitionVariant.CommandText,
                            caption: Database.STargetData.CDefinitionVariant.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveDefinitionVariant.Execute(
                       database: Database,
                       definitionVariantId: item.Id,
                       labelCs: item.LabelCs,
                       descriptionCs: item.DescriptionCs,
                       labelEn: item.LabelEn,
                       descriptionEn: item.DescriptionEn);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveDefinitionVariant.CommandText,
                            caption: TDFunctions.FnSaveDefinitionVariant.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis příspěvku lokální hustoty</para>
        /// <para lang="en">Writing a local density contribution</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Příspěvek lokální hustoty</para>
        /// <para lang="en">Local density contribution</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateLDsity(TDLDsity item,
           string labelCs, string descriptionCs,
           string labelEn, string descriptionEn)
        {
            // Žádný příspěvek lokální hustoty není vybraný
            // No local density contribution is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraný příspěvek lokální hustoty není v databázové tabulce
            // The selected local density contribution is not in the database table
            else if (!Database.STargetData.CLDsity.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CLDsity.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                        ? msgLabelCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CLDsity.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                        ? msgDescriptionCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CLDsity.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CLDsity.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CLDsity.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CLDsity.CommandText,
                            caption: Database.STargetData.CLDsity.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveLDsity.Execute(
                        database: Database,
                        ldsityId: item.Id,
                        labelCs: item.LabelCs,
                        descriptionCs: item.DescriptionCs,
                        labelEn: item.LabelEn,
                        descriptionEn: item.DescriptionEn
                        );

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveLDsity.CommandText,
                            caption: TDFunctions.FnSaveLDsity.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis objektu lokální hustoty</para>
        /// <para lang="en">Writing a local density object</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateLDsityObject(TDLDsityObject item,
          string labelCs, string descriptionCs,
          string labelEn, string descriptionEn)
        {
            // Žádný objekt lokální hustoty není vybraný
            // No local density object is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraný objekt lokální hustoty není v databázové tabulce
            // The selected local density object is not in the database table
            else if (!Database.STargetData.CLDsityObject.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CLDsityObject.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                        ? msgLabelCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CLDsityObject.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                        ? msgDescriptionCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CLDsityObject.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CLDsityObject.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CLDsityObject.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CLDsityObject.CommandText,
                            caption: Database.STargetData.CLDsityObject.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveLDsityObject.Execute(
                         database: Database,
                         ldsityObjectId: item.Id,
                         labelCs: item.LabelCs,
                         descriptionCs: item.DescriptionCs,
                         labelEn: item.LabelEn,
                         descriptionEn: item.DescriptionEn
                         );

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveLDsityObject.CommandText,
                            caption: TDFunctions.FnSaveLDsityObject.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis skupiny objektů lokálních hustot</para>
        /// <para lang="en">Writing a group of local density objects</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Skupina objektů lokálních hustot</para>
        /// <para lang="en">Group of local density objects</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateLDsityObjectGroup(TDLDsityObjectGroup item,
            string labelCs, string descriptionCs,
            string labelEn, string descriptionEn)
        {
            // Žádná skupina objektů lokálních hustot není vybraná
            // No group of local density objects is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná skupina objektů lokálních hustot není v databázové tabulce
            // The selected group of local density objects is not in the database table
            else if (!Database.STargetData.CLDsityObjectGroup.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CLDsityObjectGroup.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                        ? msgLabelCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                ;
                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CLDsityObjectGroup.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                        ? msgDescriptionCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CLDsityObjectGroup.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CLDsityObjectGroup.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CLDsityObjectGroup.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CLDsityObjectGroup.CommandText,
                            caption: Database.STargetData.CLDsityObjectGroup.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveLDsityObjectGroup.Execute(
                       database: Database,
                       labelCs: item.LabelCs,
                       descriptionCs: item.DescriptionCs,
                       labelEn: item.LabelEn,
                       descriptionEn: item.DescriptionEn,
                       ldsityObjectIds: null,
                       ldsityObjectGroupId: item.Id);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveLDsityObjectGroup.CommandText,
                            caption: TDFunctions.FnSaveLDsityObjectGroup.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis typu objektu lokální hustoty</para>
        /// <para lang="en">Writing a type of a local density object</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Typ objektu lokální hustoty</para>
        /// <para lang="en">Type of a local density object</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateLDsityObjectType(TDLDsityObjectType item,
           string labelEn, string descriptionEn)
        {
            // Žádný typ objektu lokální hustoty není vybraný
            // No type of a local density object is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraný typ objektu lokální hustoty není v databázové tabulce
            // The selected type of the local density object is not in the database table
            else if (!Database.STargetData.CLDsityObjectType.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CLDsityObjectType.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CLDsityObjectType.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                Database.STargetData.CLDsityObjectType.Update(item: item);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: Database.STargetData.CLDsityObjectType.CommandText,
                        caption: Database.STargetData.CLDsityObjectType.TableName,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis skupiny panelů a odpovídajících roků měření</para>
        /// <para lang="en">Writing a group of panels and corresponding measurement years</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Skupiny panelů a odpovídajících roků měření</para>
        /// <para lang="en">Groups of panels and corresponding measurement years</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdatePanelReferenceYearSetGroup(TDPanelRefYearSetGroup item,
            string labelCs, string descriptionCs,
            string labelEn, string descriptionEn)
        {
            // Žádná skupina panelů a odpovídajících roků měření není vybraná
            // No group of panels and corresponding measurement years is selected.
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                     languageVersion: LanguageVersion,
                     languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná skupina panelů a odpovídajících roků měření není v databázové tabulce
            // The selected group of panels and corresponding measurement years is not in the database table.
            else if (!Database.STargetData.CPanelRefYearSetGroup.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CPanelRefYearSetGroup.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                        ? msgLabelCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CPanelRefYearSetGroup.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                        ? msgDescriptionCsExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CPanelRefYearSetGroup.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CPanelRefYearSetGroup.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CPanelRefYearSetGroup.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CPanelRefYearSetGroup.CommandText,
                            caption: Database.STargetData.CPanelRefYearSetGroup.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnUpdatePyrGroup.Execute(
                      database: Database,
                      labelCs: item.LabelCs,
                      descriptionCs: item.DescriptionCs,
                      labelEn: item.LabelEn,
                      descriptionEn: item.DescriptionEn,
                      id: item.Id);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnUpdatePyrGroup.CommandText,
                            caption: TDFunctions.FnUpdatePyrGroup.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis typu cílové proměnné</para>
        /// <para lang="en">Writing a type of a target variable</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Typ cílové proměnné</para>
        /// <para lang="en">Type of a target variable</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateStateOrChange(TDStateOrChange item,
           string labelEn, string descriptionEn)
        {
            // Žádný typ cílové proměnné není vybraný
            // No type of a target variable is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraný typ cílové proměnné není v databázové tabulce
            // The selected type of the target variable is not in the database table
            else if (!Database.STargetData.CStateOrChange.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                        ? msgNoneSelectedItemText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CStateOrChange.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                        ? msgLabelEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CStateOrChange.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                        ? msgDescriptionEnExistsText
                        : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                Database.STargetData.CStateOrChange.Update(item: item);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: Database.STargetData.CStateOrChange.CommandText,
                        caption: Database.STargetData.CStateOrChange.TableName,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis subpopulace</para>
        /// <para lang="en">Writing a subpopulation</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Subpopulace</para>
        /// <para lang="en">Subpopulation</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateSubPopulation(TDSubPopulation item,
           string labelCs, string descriptionCs,
           string labelEn, string descriptionEn)
        {
            // Žádná subpopulace není vybraná
            // No subpopulation is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                     text:
                         messages.TryGetValue(
                             key: "msgNoneSelectedItem",
                             out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná subpopulace není v databázové tabulce
            // The selected subpopulation is not in the database table
            else if (!Database.STargetData.CSubPopulation.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                     text:
                         messages.TryGetValue(
                             key: "msgNoneSelectedItem",
                             out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CSubPopulation.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                     text:
                         messages.TryGetValue(key: "msgLabelCsExists",
                         out string msgLabelCsExistsText)
                            ? msgLabelCsExistsText
                            : String.Empty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CSubPopulation.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                                ? msgDescriptionCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CSubPopulation.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                                ? msgLabelEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CSubPopulation.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                                ? msgDescriptionEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CSubPopulation.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CSubPopulation.CommandText,
                            caption: Database.STargetData.CSubPopulation.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveArealOrPop.Execute(
                           database: Database,
                           arealOrPopulationId: (int)TDArealOrPopulationEnum.Population,
                           labelCs: item.LabelCs,
                           descriptionCs: item.DescriptionCs,
                           labelEn: item.LabelEn,
                           descriptionEn: item.DescriptionEn,
                           classificationType: item.ClassificationTypeValue,
                           id: item.Id);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveArealOrPop.CommandText,
                            caption: TDFunctions.FnSaveArealOrPop.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis kategorie subpopulace</para>
        /// <para lang="en">Writing a subpopulation category</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Kategorie subpopulace</para>
        /// <para lang="en">Subpopulation category</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateSubPopulationCategory(TDSubPopulationCategory item,
           string labelCs, string descriptionCs,
           string labelEn, string descriptionEn)
        {
            // Žádná kategorie subpopulace není vybraná
            // No subpopulation category is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná kategorie subpopulace není v databázové tabulce
            // The selected subpopulation category is not in the database table
            else if (!Database.STargetData.CSubPopulationCategory.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní (uvnitř skupiny určené subpopulací)
            // LabelCs - duplicated (within a group specified by a subpopulation)
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CSubPopulationCategory.Items
                .Where(a => (a.SubPopulationId ?? 0) == (item.SubPopulationId ?? 0))
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                                ? msgLabelCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní (uvnitř skupiny určené subpopulací)
            // DescriptionCs - duplicated (within a group specified by a subpopulation)
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CSubPopulationCategory.Items
                .Where(a => (a.SubPopulationId ?? 0) == (item.SubPopulationId ?? 0))
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                                ? msgDescriptionCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní (uvnitř skupiny určené subpopulací)
            // LabelEn - duplicated (within a group specified by a subpopulation)
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CSubPopulationCategory.Items
                .Where(a => (a.SubPopulationId ?? 0) == (item.SubPopulationId ?? 0))
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                                ? msgLabelEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní (uvnitř skupiny určené subpopulací)
            // DescriptionEn - duplicated (within a group specified by a subpopulation)
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CSubPopulationCategory.Items
                .Where(a => (a.SubPopulationId ?? 0) == (item.SubPopulationId ?? 0))
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                                ? msgDescriptionEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CSubPopulationCategory.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CSubPopulationCategory.CommandText,
                            caption: Database.STargetData.CSubPopulationCategory.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveCategory.Execute(
                        database: Database,
                        arealOrPopulationId: (int)TDArealOrPopulationEnum.Population,
                        labelCs: item.LabelCs,
                        descriptionCs: item.DescriptionCs,
                        labelEn: item.LabelEn,
                        descriptionEn: item.DescriptionEn,
                        parentId: null,
                        id: item.Id);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveCategory.CommandText,
                            caption: TDFunctions.FnSaveCategory.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis stejných popisků pro všechny zobrazené cílové proměnné</para>
        /// <para lang="en">Writing same lables for all displayed target variables</para>
        /// </summary>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - chyba při zápisu, false - zápis proveden</para>
        /// <para lang="en">true - error during writing, false - writing done</para>
        /// </returns>
        private bool UpdateTargetVariables(
            string labelCs, string descriptionCs,
            string labelEn, string descriptionEn)
        {
            List<TDTargetVariable> list =
                (List<TDTargetVariable>)lstLookupTable.DataSource;

            foreach (TDTargetVariable targetVariable in list)
            {
                // Žádná cílová proměnná není vybraná
                // No target variable is selected
                if (targetVariable == null)
                {
                    Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgNoneSelectedItem",
                                out string msgNoneSelectedItemText)
                                    ? msgNoneSelectedItemText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }

                // Vybraná cílová proměnná není v databázové tabulce
                // The selected target variable is not in the database table
                if (!Database.STargetData.CTargetVariable.Items
                    .Where(a => a.Id == targetVariable.Id)
                    .Any())
                {
                    Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgNoneSelectedItem",
                                out string msgNoneSelectedItemText)
                                    ? msgNoneSelectedItemText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }

                // LabelCs - duplicitní
                // LabelCs - duplicated
                if ((targetVariable.LabelCs != labelCs) &&
                    Database.STargetData.CTargetVariable.Items
                    .Where(a => a.LabelCs == labelCs)
                    .Any())
                {
                    Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgLabelCsExists",
                                out string msgLabelCsExistsText)
                                    ? msgLabelCsExistsText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }

                // DescriptionCs - duplicitní
                // DescriptionCs - duplicated
                if ((targetVariable.DescriptionCs != descriptionCs) &&
                    Database.STargetData.CTargetVariable.Items
                    .Where(a => a.DescriptionCs == descriptionCs)
                    .Any())
                {
                    Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgDescriptionCsExists",
                                out string msgDescriptionCsExistsText)
                                    ? msgDescriptionCsExistsText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }

                // LabelEn - duplicitní
                // LabelEn - duplicated
                if ((targetVariable.LabelEn != labelEn) &&
                    Database.STargetData.CTargetVariable.Items
                    .Where(a => a.LabelEn == labelEn)
                    .Any())
                {
                    Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgLabelEnExists",
                                out string msgLabelEnExistsText)
                                    ? msgLabelEnExistsText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }

                // DescriptionEn - duplicitní
                // DescriptionEn - duplicated
                if ((targetVariable.DescriptionEn != descriptionEn) &&
                    Database.STargetData.CTargetVariable.Items
                    .Where(a => a.DescriptionEn == descriptionEn)
                    .Any())
                {
                    Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgDescriptionEnExists",
                                out string msgDescriptionEnExistsText)
                                    ? msgDescriptionEnExistsText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }
            }

            if (DirectAccess)
            {
                //Database.STargetData.CTargetVariable.Update(item: targetVariable);

                //if (TargetDataSetting.Verbose)
                //{
                //    MessageBox.Show(
                //        text: Database.STargetData.CTargetVariable.CommandText,
                //        caption: Database.STargetData.CTargetVariable.TableName,
                //        buttons: MessageBoxButtons.OK,
                //        icon: MessageBoxIcon.Information);
                //}
                return false;
            }

            else
            {
                if (SelectedItem == null)
                {
                    Dictionary<string, string> messages = Dictionary(
                          languageVersion: LanguageVersion,
                          languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgNoneSelectedItem",
                                out string msgNoneSelectedItemText)
                                    ? msgNoneSelectedItemText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }

                if (SelectedItem is not TDTargetVariable)
                {
                    Dictionary<string, string> messages = Dictionary(
                          languageVersion: LanguageVersion,
                          languageFile: LanguageFile);

                    MessageBox.Show(
                        text:
                            messages.TryGetValue(
                                key: "msgNoneSelectedItem",
                                out string msgNoneSelectedItemText)
                                    ? msgNoneSelectedItemText
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return false;
                }

                TDFunctions.FnSaveTargetVariable.Execute(
                    database: Database,
                    stateOrChange: (int)((TDTargetVariable)SelectedItem).StateOrChangeValue,
                    labelCs: labelCs,
                    descriptionCs: descriptionCs,
                    labelEn: labelEn,
                    descriptionEn: descriptionEn,
                    ldsityIds: null,
                    ldsityObjectTypeIds: null,
                    versionIds: null,
                    targetVariableIds: list
                        .Select(a => (Nullable<int>)a.Id)
                        .ToList<Nullable<int>>()
                        );

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnSaveTargetVariable.CommandText,
                        caption: TDFunctions.FnSaveTargetVariable.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            return true;
        }

        /// <summary>
        /// <para lang="cs">Zápis jednotky měření</para>
        /// <para lang="en">Writing a unit of measurement</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Jednotka měření</para>
        /// <para lang="en">Unit of measurement</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateUnitOfMeasure(TDUnitOfMeasure item,
            string labelCs, string descriptionCs,
            string labelEn, string descriptionEn)
        {
            // Žádná jednotka měření není vybraná
            // No unit of measurement is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná jednotka měření není v databázové tabulce
            // The selected unit of measurement is not in the database table
            else if (!Database.STargetData.CUnitOfMeasure.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CUnitOfMeasure.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                                ? msgLabelCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CUnitOfMeasure.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                                ? msgDescriptionCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CUnitOfMeasure.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                                ? msgLabelEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CUnitOfMeasure.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                                ? msgDescriptionEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CUnitOfMeasure.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CUnitOfMeasure.CommandText,
                            caption: Database.STargetData.CUnitOfMeasure.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveUnitOfMeasure.Execute(
                       database: Database,
                       unitOfMeasureId: item.Id,
                       labelCs: item.LabelCs,
                       descriptionCs: item.DescriptionCs,
                       labelEn: item.LabelEn,
                       descriptionEn: item.DescriptionEn);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveUnitOfMeasure.CommandText,
                            caption: TDFunctions.FnSaveUnitOfMeasure.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// <para lang="cs">Zápis verze</para>
        /// <para lang="en">Writing a version</para>
        /// </summary>
        /// <param name="item">
        /// <para lang="cs">Verze</para>
        /// <para lang="en">Version</para>
        /// </param>
        /// <param name="labelCs">
        /// <para lang="cs">Zkratka (národní)</para>
        /// <para lang="en">Label (national)</para>
        /// </param>
        /// <param name="descriptionCs">
        /// <para lang="cs">Popis (národní)</para>
        /// <para lang="en">Description (national)</para>
        /// </param>
        /// <param name="labelEn">
        /// <para lang="cs">Zkratka (anglická)</para>
        /// <para lang="en">Label (English)</para>
        /// </param>
        /// <param name="descriptionEn">
        /// <para lang="cs">Popis (anglický)</para>
        /// <para lang="en">Description (English)</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">true - zápis proveden, false - chyba při zápisu</para>
        /// <para lang="en">true - writing done, false - error during writing</para>
        /// </returns>
        private bool UpdateVersion(TDVersion item,
            string labelCs, string descriptionCs,
            string labelEn, string descriptionEn)
        {
            // Žádná verze není vybraná
            // No version is selected
            if (item == null)
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // Vybraná verze není v databázové tabulce
            // The selected version is not in the database table
            else if (!Database.STargetData.CVersion.Items.Where(a => a.Id == item.Id).Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgNoneSelectedItem",
                            out string msgNoneSelectedItemText)
                                ? msgNoneSelectedItemText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelCs - duplicitní
            // LabelCs - duplicated
            else if ((item.LabelCs != labelCs) &&
                Database.STargetData.CVersion.Items
                .Where(a => a.LabelCs == labelCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelCsExists",
                            out string msgLabelCsExistsText)
                                ? msgLabelCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionCs - duplicitní
            // DescriptionCs - duplicated
            else if ((item.DescriptionCs != descriptionCs) &&
                Database.STargetData.CVersion.Items
                .Where(a => a.DescriptionCs == descriptionCs)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionCsExists",
                            out string msgDescriptionCsExistsText)
                                ? msgDescriptionCsExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // LabelEn - duplicitní
            // LabelEn - duplicated
            else if ((item.LabelEn != labelEn) &&
                Database.STargetData.CVersion.Items
                .Where(a => a.LabelEn == labelEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgLabelEnExists",
                            out string msgLabelEnExistsText)
                                ? msgLabelEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            // DescriptionEn - duplicitní
            // DescriptionEn - duplicated
            else if ((item.DescriptionEn != descriptionEn) &&
                Database.STargetData.CVersion.Items
                .Where(a => a.DescriptionEn == descriptionEn)
                .Any())
            {
                Dictionary<string, string> messages = Dictionary(
                      languageVersion: LanguageVersion,
                      languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.TryGetValue(
                            key: "msgDescriptionEnExists",
                            out string msgDescriptionEnExistsText)
                                ? msgDescriptionEnExistsText
                                : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            else
            {
                item.LabelCs = labelCs;
                item.DescriptionCs = descriptionCs;
                item.LabelEn = labelEn;
                item.DescriptionEn = descriptionEn;

                if (DirectAccess)
                {
                    Database.STargetData.CVersion.Update(item: item);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: Database.STargetData.CVersion.CommandText,
                            caption: Database.STargetData.CVersion.TableName,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
                else
                {
                    TDFunctions.FnSaveVersion.Execute(
                        database: Database,
                        versionId: item.Id,
                        labelCs: item.LabelCs,
                        descriptionCs: item.DescriptionCs,
                        labelEn: item.LabelEn,
                        descriptionEn: item.DescriptionEn);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveVersion.CommandText,
                            caption: TDFunctions.FnSaveVersion.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                return true;
            }
        }

        #endregion Update Methods

    }

}