﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class FormCategoryNewDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCancel = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlOK = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.splWorkSpace = new System.Windows.Forms.SplitContainer();
            this.grpCategory = new System.Windows.Forms.GroupBox();
            this.tlpCategory = new System.Windows.Forms.TableLayoutPanel();
            this.tsrCategory = new System.Windows.Forms.ToolStrip();
            this.btnCategoryEdit = new System.Windows.Forms.ToolStripButton();
            this.btnCategoryAdd = new System.Windows.Forms.ToolStripButton();
            this.btnDependenceDefine = new System.Windows.Forms.ToolStripButton();
            this.btnCategoryDelete = new System.Windows.Forms.ToolStripButton();
            this.tvwCategory = new System.Windows.Forms.TreeView();
            this.grpWorkSpace = new System.Windows.Forms.GroupBox();
            this.tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            this.pnlClassificationRuleCaption = new System.Windows.Forms.Panel();
            this.lblClassificationRuleCaption = new System.Windows.Forms.Label();
            this.splClassificationRule = new System.Windows.Forms.SplitContainer();
            this.pnlClassificationRuleSwitch = new System.Windows.Forms.Panel();
            this.lblClassificationRuleSwitch = new System.Windows.Forms.Label();
            this.pnlDependenceEditor = new System.Windows.Forms.Panel();
            this.lblDependenceEditor = new System.Windows.Forms.Label();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.lblCaption = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlCancel.SuspendLayout();
            this.pnlOK.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splWorkSpace)).BeginInit();
            this.splWorkSpace.Panel1.SuspendLayout();
            this.splWorkSpace.Panel2.SuspendLayout();
            this.splWorkSpace.SuspendLayout();
            this.grpCategory.SuspendLayout();
            this.tlpCategory.SuspendLayout();
            this.tsrCategory.SuspendLayout();
            this.grpWorkSpace.SuspendLayout();
            this.tlpWorkSpace.SuspendLayout();
            this.pnlClassificationRuleCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splClassificationRule)).BeginInit();
            this.splClassificationRule.Panel1.SuspendLayout();
            this.splClassificationRule.Panel2.SuspendLayout();
            this.splClassificationRule.SuspendLayout();
            this.pnlClassificationRuleSwitch.SuspendLayout();
            this.pnlDependenceEditor.SuspendLayout();
            this.pnlCaption.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(944, 501);
            this.pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.SystemColors.Control;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 2);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 1);
            this.tlpMain.Controls.Add(this.pnlCaption, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(944, 501);
            this.tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.Controls.Add(this.pnlCancel, 2, 0);
            this.tlpButtons.Controls.Add(this.pnlOK, 1, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 461);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(944, 40);
            this.tlpButtons.TabIndex = 11;
            // 
            // pnlCancel
            // 
            this.pnlCancel.Controls.Add(this.btnCancel);
            this.pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCancel.Location = new System.Drawing.Point(784, 0);
            this.pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCancel.Name = "pnlCancel";
            this.pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            this.pnlCancel.Size = new System.Drawing.Size(160, 40);
            this.pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnCancel.Location = new System.Drawing.Point(5, 5);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 30);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            
            // 
            // pnlOK
            // 
            this.pnlOK.Controls.Add(this.btnOK);
            this.pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOK.Location = new System.Drawing.Point(624, 0);
            this.pnlOK.Margin = new System.Windows.Forms.Padding(0);
            this.pnlOK.Name = "pnlOK";
            this.pnlOK.Padding = new System.Windows.Forms.Padding(5);
            this.pnlOK.Size = new System.Drawing.Size(160, 40);
            this.pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnOK.Location = new System.Drawing.Point(5, 5);
            this.btnOK.Margin = new System.Windows.Forms.Padding(0);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(150, 30);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.BackColor = System.Drawing.SystemColors.Control;
            this.pnlWorkSpace.Controls.Add(this.splWorkSpace);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(0, 30);
            this.pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Size = new System.Drawing.Size(944, 431);
            this.pnlWorkSpace.TabIndex = 9;
            // 
            // splWorkSpace
            // 
            this.splWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splWorkSpace.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.splWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.splWorkSpace.Name = "splWorkSpace";
            // 
            // splWorkSpace.Panel1
            // 
            this.splWorkSpace.Panel1.Controls.Add(this.grpCategory);
            this.splWorkSpace.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // splWorkSpace.Panel2
            // 
            this.splWorkSpace.Panel2.Controls.Add(this.grpWorkSpace);
            this.splWorkSpace.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.splWorkSpace.Size = new System.Drawing.Size(944, 431);
            this.splWorkSpace.SplitterDistance = 275;
            this.splWorkSpace.TabIndex = 9;
            // 
            // grpCategory
            // 
            this.grpCategory.Controls.Add(this.tlpCategory);
            this.grpCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCategory.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.grpCategory.ForeColor = System.Drawing.Color.MediumBlue;
            this.grpCategory.Location = new System.Drawing.Point(3, 3);
            this.grpCategory.Margin = new System.Windows.Forms.Padding(0);
            this.grpCategory.Name = "grpCategory";
            this.grpCategory.Padding = new System.Windows.Forms.Padding(5);
            this.grpCategory.Size = new System.Drawing.Size(269, 425);
            this.grpCategory.TabIndex = 4;
            this.grpCategory.TabStop = false;
            this.grpCategory.Text = "grpCategory";
            // 
            // tlpCategory
            // 
            this.tlpCategory.ColumnCount = 1;
            this.tlpCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCategory.Controls.Add(this.tsrCategory, 0, 0);
            this.tlpCategory.Controls.Add(this.tvwCategory, 0, 1);
            this.tlpCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCategory.Location = new System.Drawing.Point(5, 25);
            this.tlpCategory.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCategory.Name = "tlpCategory";
            this.tlpCategory.RowCount = 2;
            this.tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpCategory.Size = new System.Drawing.Size(259, 395);
            this.tlpCategory.TabIndex = 0;
            // 
            // tsrCategory
            // 
            this.tsrCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCategoryEdit,
            this.btnCategoryAdd,
            this.btnDependenceDefine,
            this.btnCategoryDelete});
            this.tsrCategory.Location = new System.Drawing.Point(0, 0);
            this.tsrCategory.Name = "tsrCategory";
            this.tsrCategory.Padding = new System.Windows.Forms.Padding(0);
            this.tsrCategory.Size = new System.Drawing.Size(259, 25);
            this.tsrCategory.TabIndex = 0;
            this.tsrCategory.Text = "tsrCategory";
            // 
            // btnCategoryEdit
            // 
            this.btnCategoryEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnCategoryEdit.ForeColor = System.Drawing.Color.Black;
            this.btnCategoryEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCategoryEdit.Name = "btnCategoryEdit";
            this.btnCategoryEdit.Size = new System.Drawing.Size(97, 22);
            this.btnCategoryEdit.Text = "btnCategoryEdit";
            
            // 
            // btnCategoryAdd
            // 
            this.btnCategoryAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnCategoryAdd.ForeColor = System.Drawing.Color.Black;
            this.btnCategoryAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCategoryAdd.Name = "btnCategoryAdd";
            this.btnCategoryAdd.Size = new System.Drawing.Size(99, 22);
            this.btnCategoryAdd.Text = "btnCategoryAdd";
            
            // 
            // btnDependenceDefine
            // 
            this.btnDependenceDefine.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnDependenceDefine.BackColor = System.Drawing.SystemColors.Control;
            this.btnDependenceDefine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDependenceDefine.ForeColor = System.Drawing.Color.Black;
            this.btnDependenceDefine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDependenceDefine.Name = "btnDependenceDefine";
            this.btnDependenceDefine.Size = new System.Drawing.Size(129, 19);
            this.btnDependenceDefine.Text = "btnDependenceDefine";
            
            // 
            // btnCategoryDelete
            // 
            this.btnCategoryDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnCategoryDelete.ForeColor = System.Drawing.Color.Black;
            this.btnCategoryDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCategoryDelete.Name = "btnCategoryDelete";
            this.btnCategoryDelete.Size = new System.Drawing.Size(110, 19);
            this.btnCategoryDelete.Text = "btnCategoryDelete";
            
            // 
            // tvwCategory
            // 
            this.tvwCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvwCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tvwCategory.Location = new System.Drawing.Point(0, 25);
            this.tvwCategory.Margin = new System.Windows.Forms.Padding(0);
            this.tvwCategory.Name = "tvwCategory";
            this.tvwCategory.ShowNodeToolTips = true;
            this.tvwCategory.Size = new System.Drawing.Size(259, 370);
            this.tvwCategory.TabIndex = 1;
            
            // 
            // grpWorkSpace
            // 
            this.grpWorkSpace.Controls.Add(this.tlpWorkSpace);
            this.grpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpWorkSpace.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.grpWorkSpace.ForeColor = System.Drawing.Color.MediumBlue;
            this.grpWorkSpace.Location = new System.Drawing.Point(3, 3);
            this.grpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.grpWorkSpace.Name = "grpWorkSpace";
            this.grpWorkSpace.Padding = new System.Windows.Forms.Padding(5);
            this.grpWorkSpace.Size = new System.Drawing.Size(659, 425);
            this.grpWorkSpace.TabIndex = 5;
            this.grpWorkSpace.TabStop = false;
            this.grpWorkSpace.Text = "grpWorkSpace";
            // 
            // tlpWorkSpace
            // 
            this.tlpWorkSpace.ColumnCount = 1;
            this.tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWorkSpace.Controls.Add(this.pnlClassificationRuleCaption, 0, 0);
            this.tlpWorkSpace.Controls.Add(this.splClassificationRule, 0, 1);
            this.tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpWorkSpace.Location = new System.Drawing.Point(5, 25);
            this.tlpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            this.tlpWorkSpace.Name = "tlpWorkSpace";
            this.tlpWorkSpace.RowCount = 2;
            this.tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWorkSpace.Size = new System.Drawing.Size(649, 395);
            this.tlpWorkSpace.TabIndex = 0;
            // 
            // pnlClassificationRuleCaption
            // 
            this.pnlClassificationRuleCaption.BackColor = System.Drawing.SystemColors.Control;
            this.pnlClassificationRuleCaption.Controls.Add(this.lblClassificationRuleCaption);
            this.pnlClassificationRuleCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClassificationRuleCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlClassificationRuleCaption.ForeColor = System.Drawing.Color.Black;
            this.pnlClassificationRuleCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlClassificationRuleCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClassificationRuleCaption.Name = "pnlClassificationRuleCaption";
            this.pnlClassificationRuleCaption.Size = new System.Drawing.Size(649, 75);
            this.pnlClassificationRuleCaption.TabIndex = 11;
            // 
            // lblClassificationRuleCaption
            // 
            this.lblClassificationRuleCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblClassificationRuleCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClassificationRuleCaption.ForeColor = System.Drawing.Color.Black;
            this.lblClassificationRuleCaption.Location = new System.Drawing.Point(0, 0);
            this.lblClassificationRuleCaption.Name = "lblClassificationRuleCaption";
            this.lblClassificationRuleCaption.Size = new System.Drawing.Size(649, 75);
            this.lblClassificationRuleCaption.TabIndex = 1;
            this.lblClassificationRuleCaption.Text = "lblClassificationRuleCaption";
            this.lblClassificationRuleCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splClassificationRule
            // 
            this.splClassificationRule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splClassificationRule.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splClassificationRule.IsSplitterFixed = true;
            this.splClassificationRule.Location = new System.Drawing.Point(0, 75);
            this.splClassificationRule.Margin = new System.Windows.Forms.Padding(0);
            this.splClassificationRule.Name = "splClassificationRule";
            this.splClassificationRule.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splClassificationRule.Panel1
            // 
            this.splClassificationRule.Panel1.Controls.Add(this.pnlClassificationRuleSwitch);
            // 
            // splClassificationRule.Panel2
            // 
            this.splClassificationRule.Panel2.Controls.Add(this.pnlDependenceEditor);
            this.splClassificationRule.Size = new System.Drawing.Size(649, 320);
            this.splClassificationRule.SplitterDistance = 160;
            this.splClassificationRule.SplitterWidth = 1;
            this.splClassificationRule.TabIndex = 12;
            // 
            // pnlClassificationRuleSwitch
            // 
            this.pnlClassificationRuleSwitch.BackColor = System.Drawing.SystemColors.Control;
            this.pnlClassificationRuleSwitch.Controls.Add(this.lblClassificationRuleSwitch);
            this.pnlClassificationRuleSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClassificationRuleSwitch.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.pnlClassificationRuleSwitch.ForeColor = System.Drawing.Color.Black;
            this.pnlClassificationRuleSwitch.Location = new System.Drawing.Point(0, 0);
            this.pnlClassificationRuleSwitch.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClassificationRuleSwitch.Name = "pnlClassificationRuleSwitch";
            this.pnlClassificationRuleSwitch.Size = new System.Drawing.Size(649, 160);
            this.pnlClassificationRuleSwitch.TabIndex = 12;
            // 
            // lblClassificationRuleSwitch
            // 
            this.lblClassificationRuleSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblClassificationRuleSwitch.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClassificationRuleSwitch.ForeColor = System.Drawing.Color.Black;
            this.lblClassificationRuleSwitch.Location = new System.Drawing.Point(0, 0);
            this.lblClassificationRuleSwitch.Name = "lblClassificationRuleSwitch";
            this.lblClassificationRuleSwitch.Size = new System.Drawing.Size(649, 160);
            this.lblClassificationRuleSwitch.TabIndex = 0;
            this.lblClassificationRuleSwitch.Text = "lblClassificationRuleSwitch";
            this.lblClassificationRuleSwitch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDependenceEditor
            // 
            this.pnlDependenceEditor.BackColor = System.Drawing.SystemColors.Control;
            this.pnlDependenceEditor.Controls.Add(this.lblDependenceEditor);
            this.pnlDependenceEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDependenceEditor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.pnlDependenceEditor.ForeColor = System.Drawing.Color.Black;
            this.pnlDependenceEditor.Location = new System.Drawing.Point(0, 0);
            this.pnlDependenceEditor.Margin = new System.Windows.Forms.Padding(0);
            this.pnlDependenceEditor.Name = "pnlDependenceEditor";
            this.pnlDependenceEditor.Size = new System.Drawing.Size(649, 159);
            this.pnlDependenceEditor.TabIndex = 13;
            // 
            // lblDependenceEditor
            // 
            this.lblDependenceEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDependenceEditor.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDependenceEditor.ForeColor = System.Drawing.Color.Black;
            this.lblDependenceEditor.Location = new System.Drawing.Point(0, 0);
            this.lblDependenceEditor.Name = "lblDependenceEditor";
            this.lblDependenceEditor.Size = new System.Drawing.Size(649, 159);
            this.lblDependenceEditor.TabIndex = 1;
            this.lblDependenceEditor.Text = "lblDependenceEditor";
            this.lblDependenceEditor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlCaption
            // 
            this.pnlCaption.BackColor = System.Drawing.SystemColors.Control;
            this.pnlCaption.Controls.Add(this.lblCaption);
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Location = new System.Drawing.Point(0, 0);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Size = new System.Drawing.Size(944, 30);
            this.pnlCaption.TabIndex = 10;
            // 
            // lblCaption
            // 
            this.lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCaption.Location = new System.Drawing.Point(0, 0);
            this.lblCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblCaption.Size = new System.Drawing.Size(944, 30);
            this.lblCaption.TabIndex = 2;
            this.lblCaption.Text = "lblCaption";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormCategoryNew
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.pnlMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "FormCategoryNew";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormCategoryNew";
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlCancel.ResumeLayout(false);
            this.pnlOK.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.splWorkSpace.Panel1.ResumeLayout(false);
            this.splWorkSpace.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splWorkSpace)).EndInit();
            this.splWorkSpace.ResumeLayout(false);
            this.grpCategory.ResumeLayout(false);
            this.tlpCategory.ResumeLayout(false);
            this.tlpCategory.PerformLayout();
            this.tsrCategory.ResumeLayout(false);
            this.tsrCategory.PerformLayout();
            this.grpWorkSpace.ResumeLayout(false);
            this.tlpWorkSpace.ResumeLayout(false);
            this.pnlClassificationRuleCaption.ResumeLayout(false);
            this.splClassificationRule.Panel1.ResumeLayout(false);
            this.splClassificationRule.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splClassificationRule)).EndInit();
            this.splClassificationRule.ResumeLayout(false);
            this.pnlClassificationRuleSwitch.ResumeLayout(false);
            this.pnlDependenceEditor.ResumeLayout(false);
            this.pnlCaption.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splWorkSpace;
        private System.Windows.Forms.GroupBox grpCategory;
        private System.Windows.Forms.TableLayoutPanel tlpCategory;
        private System.Windows.Forms.ToolStrip tsrCategory;
        private System.Windows.Forms.ToolStripButton btnCategoryEdit;
        private System.Windows.Forms.ToolStripButton btnCategoryAdd;
        private System.Windows.Forms.ToolStripButton btnCategoryDelete;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.TreeView tvwCategory;
        private System.Windows.Forms.ToolStripButton btnDependenceDefine;
        private System.Windows.Forms.GroupBox grpWorkSpace;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.Panel pnlClassificationRuleCaption;
        private System.Windows.Forms.SplitContainer splClassificationRule;
        private System.Windows.Forms.Panel pnlClassificationRuleSwitch;
        private System.Windows.Forms.Label lblClassificationRuleSwitch;
        private System.Windows.Forms.Panel pnlDependenceEditor;
        private System.Windows.Forms.Label lblDependenceEditor;
        private System.Windows.Forms.Label lblClassificationRuleCaption;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
    }

}