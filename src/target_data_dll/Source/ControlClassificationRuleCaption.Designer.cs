﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlClassificationRuleCaptionDesign
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.lblLDsityObjectForADSPCaption = new System.Windows.Forms.Label();
            this.lblCategoryCaption = new System.Windows.Forms.Label();
            this.lblLDsityObjectCaption = new System.Windows.Forms.Label();
            this.lblCategoryPlaceHolder = new System.Windows.Forms.Label();
            this.lblLDsityObjectPlaceHolder = new System.Windows.Forms.Label();
            this.chkIncludeLDsityObjectForADSP = new System.Windows.Forms.CheckBox();
            this.lblCategoryValue = new System.Windows.Forms.Label();
            this.lblLDsityObjectValue = new System.Windows.Forms.Label();
            this.lblLDsityObjectForADSPValue = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(598, 58);
            this.pnlMain.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.lblLDsityObjectForADSPCaption, 0, 2);
            this.tlpMain.Controls.Add(this.lblCategoryCaption, 0, 0);
            this.tlpMain.Controls.Add(this.lblLDsityObjectCaption, 0, 1);
            this.tlpMain.Controls.Add(this.lblCategoryPlaceHolder, 1, 0);
            this.tlpMain.Controls.Add(this.lblLDsityObjectPlaceHolder, 1, 1);
            this.tlpMain.Controls.Add(this.chkIncludeLDsityObjectForADSP, 1, 2);
            this.tlpMain.Controls.Add(this.lblCategoryValue, 2, 0);
            this.tlpMain.Controls.Add(this.lblLDsityObjectValue, 2, 1);
            this.tlpMain.Controls.Add(this.lblLDsityObjectForADSPValue, 2, 2);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 4;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Size = new System.Drawing.Size(598, 58);
            this.tlpMain.TabIndex = 0;
            // 
            // lblLDsityObjectForADSPCaption
            // 
            this.lblLDsityObjectForADSPCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLDsityObjectForADSPCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLDsityObjectForADSPCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblLDsityObjectForADSPCaption.Location = new System.Drawing.Point(3, 40);
            this.lblLDsityObjectForADSPCaption.Name = "lblLDsityObjectForADSPCaption";
            this.lblLDsityObjectForADSPCaption.Size = new System.Drawing.Size(274, 20);
            this.lblLDsityObjectForADSPCaption.TabIndex = 3;
            this.lblLDsityObjectForADSPCaption.Text = "lblLDsityObjectForADSPCaption";
            this.lblLDsityObjectForADSPCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCategoryCaption
            // 
            this.lblCategoryCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCategoryCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCategoryCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblCategoryCaption.Location = new System.Drawing.Point(3, 0);
            this.lblCategoryCaption.Name = "lblCategoryCaption";
            this.lblCategoryCaption.Size = new System.Drawing.Size(274, 20);
            this.lblCategoryCaption.TabIndex = 1;
            this.lblCategoryCaption.Text = "lblCategoryCaption";
            this.lblCategoryCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLDsityObjectCaption
            // 
            this.lblLDsityObjectCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLDsityObjectCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLDsityObjectCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblLDsityObjectCaption.Location = new System.Drawing.Point(3, 20);
            this.lblLDsityObjectCaption.Name = "lblLDsityObjectCaption";
            this.lblLDsityObjectCaption.Size = new System.Drawing.Size(274, 20);
            this.lblLDsityObjectCaption.TabIndex = 2;
            this.lblLDsityObjectCaption.Text = "lblLDsityObjectCaption";
            this.lblLDsityObjectCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCategoryPlaceHolder
            // 
            this.lblCategoryPlaceHolder.AutoSize = true;
            this.lblCategoryPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCategoryPlaceHolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblCategoryPlaceHolder.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblCategoryPlaceHolder.Location = new System.Drawing.Point(280, 0);
            this.lblCategoryPlaceHolder.Margin = new System.Windows.Forms.Padding(0);
            this.lblCategoryPlaceHolder.Name = "lblCategoryPlaceHolder";
            this.lblCategoryPlaceHolder.Size = new System.Drawing.Size(20, 20);
            this.lblCategoryPlaceHolder.TabIndex = 4;
            this.lblCategoryPlaceHolder.Text = "lblCategoryPlaceHolder";
            this.lblCategoryPlaceHolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLDsityObjectPlaceHolder
            // 
            this.lblLDsityObjectPlaceHolder.AutoSize = true;
            this.lblLDsityObjectPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLDsityObjectPlaceHolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblLDsityObjectPlaceHolder.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblLDsityObjectPlaceHolder.Location = new System.Drawing.Point(280, 20);
            this.lblLDsityObjectPlaceHolder.Margin = new System.Windows.Forms.Padding(0);
            this.lblLDsityObjectPlaceHolder.Name = "lblLDsityObjectPlaceHolder";
            this.lblLDsityObjectPlaceHolder.Size = new System.Drawing.Size(20, 20);
            this.lblLDsityObjectPlaceHolder.TabIndex = 5;
            this.lblLDsityObjectPlaceHolder.Text = "lblLDsityObjectPlaceHolder";
            this.lblLDsityObjectPlaceHolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkIncludeLDsityObjectForADSP
            // 
            this.chkIncludeLDsityObjectForADSP.AutoSize = true;
            this.chkIncludeLDsityObjectForADSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkIncludeLDsityObjectForADSP.Location = new System.Drawing.Point(283, 43);
            this.chkIncludeLDsityObjectForADSP.Name = "chkIncludeLDsityObjectForADSP";
            this.chkIncludeLDsityObjectForADSP.Size = new System.Drawing.Size(14, 14);
            this.chkIncludeLDsityObjectForADSP.TabIndex = 6;
            this.chkIncludeLDsityObjectForADSP.UseVisualStyleBackColor = true;
            this.chkIncludeLDsityObjectForADSP.Visible = false;
            // 
            // lblCategoryValue
            // 
            this.lblCategoryValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCategoryValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCategoryValue.Location = new System.Drawing.Point(303, 0);
            this.lblCategoryValue.Name = "lblCategoryValue";
            this.lblCategoryValue.Size = new System.Drawing.Size(160, 20);
            this.lblCategoryValue.TabIndex = 7;
            this.lblCategoryValue.Text = "lblCategoryValue";
            this.lblCategoryValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLDsityObjectValue
            // 
            this.lblLDsityObjectValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLDsityObjectValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLDsityObjectValue.Location = new System.Drawing.Point(303, 20);
            this.lblLDsityObjectValue.Name = "lblLDsityObjectValue";
            this.lblLDsityObjectValue.Size = new System.Drawing.Size(292, 20);
            this.lblLDsityObjectValue.TabIndex = 8;
            this.lblLDsityObjectValue.Text = "lblLDsityObjectValue";
            this.lblLDsityObjectValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLDsityObjectForADSPValue
            // 
            this.lblLDsityObjectForADSPValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLDsityObjectForADSPValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLDsityObjectForADSPValue.Location = new System.Drawing.Point(303, 40);
            this.lblLDsityObjectForADSPValue.Name = "lblLDsityObjectForADSPValue";
            this.lblLDsityObjectForADSPValue.Size = new System.Drawing.Size(292, 20);
            this.lblLDsityObjectForADSPValue.TabIndex = 9;
            this.lblLDsityObjectForADSPValue.Text = "lblLDsityObjectForADSPValue";
            this.lblLDsityObjectForADSPValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlClassificationRuleCaption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlClassificationRuleCaption";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(600, 60);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Label lblCategoryCaption;
        private System.Windows.Forms.Label lblLDsityObjectCaption;
        private System.Windows.Forms.Label lblLDsityObjectForADSPCaption;
        private System.Windows.Forms.Label lblCategoryPlaceHolder;
        private System.Windows.Forms.Label lblLDsityObjectPlaceHolder;
        private System.Windows.Forms.CheckBox chkIncludeLDsityObjectForADSP;
        private System.Windows.Forms.Label lblCategoryValue;
        private System.Windows.Forms.Label lblLDsityObjectValue;
        private System.Windows.Forms.Label lblLDsityObjectForADSPValue;
    }

}
