﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlResources
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlResources));
            this.tsrMain = new System.Windows.Forms.ToolStrip();
            this.IconRedBall = new System.Windows.Forms.ToolStripButton();
            this.IconGreenBall = new System.Windows.Forms.ToolStripButton();
            this.IconBlueBall = new System.Windows.Forms.ToolStripButton();
            this.IconArrowLeft = new System.Windows.Forms.ToolStripButton();
            this.IconArrowRight = new System.Windows.Forms.ToolStripButton();
            this.IconPencil = new System.Windows.Forms.ToolStripButton();
            this.IconPlusGreen = new System.Windows.Forms.ToolStripButton();
            this.IconMinusRed = new System.Windows.Forms.ToolStripButton();
            this.IconSchema = new System.Windows.Forms.ToolStripButton();
            this.IconValidateGreen = new System.Windows.Forms.ToolStripButton();
            this.IconValidateBlack = new System.Windows.Forms.ToolStripButton();
            this.IconValidateSet = new System.Windows.Forms.ToolStripButton();
            this.IconRedCross = new System.Windows.Forms.ToolStripButton();
            this.IconExport = new System.Windows.Forms.ToolStripButton();
            this.IconImport = new System.Windows.Forms.ToolStripButton();
            this.tsrMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsrMain
            // 
            this.tsrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IconRedBall,
            this.IconGreenBall,
            this.IconBlueBall,
            this.IconArrowLeft,
            this.IconArrowRight,
            this.IconPencil,
            this.IconPlusGreen,
            this.IconMinusRed,
            this.IconSchema,
            this.IconValidateGreen,
            this.IconValidateBlack,
            this.IconValidateSet,
            this.IconRedCross,
            this.IconExport,
            this.IconImport});
            this.tsrMain.Location = new System.Drawing.Point(0, 0);
            this.tsrMain.Name = "tsrMain";
            this.tsrMain.Size = new System.Drawing.Size(400, 25);
            this.tsrMain.TabIndex = 0;
            this.tsrMain.Text = "toolStrip1";
            // 
            // IconRedBall
            // 
            this.IconRedBall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconRedBall.Image = ((System.Drawing.Image)(resources.GetObject("IconRedBall.Image")));
            this.IconRedBall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconRedBall.Name = "IconRedBall";
            this.IconRedBall.Size = new System.Drawing.Size(23, 22);
            this.IconRedBall.Text = "IconRedBall";
            // 
            // IconGreenBall
            // 
            this.IconGreenBall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconGreenBall.Image = ((System.Drawing.Image)(resources.GetObject("IconGreenBall.Image")));
            this.IconGreenBall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconGreenBall.Name = "IconGreenBall";
            this.IconGreenBall.Size = new System.Drawing.Size(23, 22);
            this.IconGreenBall.Text = "IconGreenBall";
            // 
            // IconBlueBall
            // 
            this.IconBlueBall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconBlueBall.Image = ((System.Drawing.Image)(resources.GetObject("IconBlueBall.Image")));
            this.IconBlueBall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconBlueBall.Name = "IconBlueBall";
            this.IconBlueBall.Size = new System.Drawing.Size(23, 22);
            this.IconBlueBall.Text = "IconBlueBall";
            // 
            // IconArrowLeft
            // 
            this.IconArrowLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconArrowLeft.Image = ((System.Drawing.Image)(resources.GetObject("IconArrowLeft.Image")));
            this.IconArrowLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconArrowLeft.Name = "IconArrowLeft";
            this.IconArrowLeft.Size = new System.Drawing.Size(23, 22);
            this.IconArrowLeft.Text = "IconArrowLeft";
            // 
            // IconArrowRight
            // 
            this.IconArrowRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconArrowRight.Image = ((System.Drawing.Image)(resources.GetObject("IconArrowRight.Image")));
            this.IconArrowRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconArrowRight.Name = "IconArrowRight";
            this.IconArrowRight.Size = new System.Drawing.Size(23, 22);
            this.IconArrowRight.Text = "IconArrowRight";
            // 
            // IconPencil
            // 
            this.IconPencil.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconPencil.Image = ((System.Drawing.Image)(resources.GetObject("IconPencil.Image")));
            this.IconPencil.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconPencil.Name = "IconPencil";
            this.IconPencil.Size = new System.Drawing.Size(23, 22);
            this.IconPencil.Text = "IconPencil";
            // 
            // IconPlusGreen
            // 
            this.IconPlusGreen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconPlusGreen.Image = ((System.Drawing.Image)(resources.GetObject("IconPlusGreen.Image")));
            this.IconPlusGreen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconPlusGreen.Name = "IconPlusGreen";
            this.IconPlusGreen.Size = new System.Drawing.Size(23, 22);
            this.IconPlusGreen.Text = "IconPlusGreen";
            // 
            // IconMinusRed
            // 
            this.IconMinusRed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconMinusRed.Image = ((System.Drawing.Image)(resources.GetObject("IconMinusRed.Image")));
            this.IconMinusRed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconMinusRed.Name = "IconMinusRed";
            this.IconMinusRed.Size = new System.Drawing.Size(23, 22);
            this.IconMinusRed.Text = "IconMinusRed";
            // 
            // IconSchema
            // 
            this.IconSchema.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconSchema.Image = ((System.Drawing.Image)(resources.GetObject("IconSchema.Image")));
            this.IconSchema.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconSchema.Name = "IconSchema";
            this.IconSchema.Size = new System.Drawing.Size(23, 22);
            this.IconSchema.Text = "IconSchema";
            // 
            // IconValidateGreen
            // 
            this.IconValidateGreen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconValidateGreen.Image = ((System.Drawing.Image)(resources.GetObject("IconValidateGreen.Image")));
            this.IconValidateGreen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconValidateGreen.Name = "IconValidateGreen";
            this.IconValidateGreen.Size = new System.Drawing.Size(23, 22);
            this.IconValidateGreen.Text = "IconValidateGreen";
            // 
            // IconValidateBlack
            // 
            this.IconValidateBlack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconValidateBlack.Image = ((System.Drawing.Image)(resources.GetObject("IconValidateBlack.Image")));
            this.IconValidateBlack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconValidateBlack.Name = "IconValidateBlack";
            this.IconValidateBlack.Size = new System.Drawing.Size(23, 22);
            this.IconValidateBlack.Text = "IconValidateBlack";
            // 
            // IconValidateSet
            // 
            this.IconValidateSet.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconValidateSet.Image = ((System.Drawing.Image)(resources.GetObject("IconValidateSet.Image")));
            this.IconValidateSet.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconValidateSet.Name = "IconValidateSet";
            this.IconValidateSet.Size = new System.Drawing.Size(23, 22);
            this.IconValidateSet.Text = "IconValidateSet";
            // 
            // IconRedCross
            // 
            this.IconRedCross.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconRedCross.Image = ((System.Drawing.Image)(resources.GetObject("IconRedCross.Image")));
            this.IconRedCross.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconRedCross.Name = "IconRedCross";
            this.IconRedCross.Size = new System.Drawing.Size(23, 22);
            this.IconRedCross.Text = "toolStripButton1";
            // 
            // IconExport
            // 
            this.IconExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconExport.Image = ((System.Drawing.Image)(resources.GetObject("IconExport.Image")));
            this.IconExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconExport.Name = "IconExport";
            this.IconExport.Size = new System.Drawing.Size(23, 22);
            this.IconExport.Text = "toolStripButton1";
            // 
            // IconImport
            // 
            this.IconImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IconImport.Image = ((System.Drawing.Image)(resources.GetObject("IconImport.Image")));
            this.IconImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IconImport.Name = "IconImport";
            this.IconImport.Size = new System.Drawing.Size(23, 22);
            this.IconImport.Text = "toolStripButton1";
            // 
            // ControlResources
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tsrMain);
            this.Name = "ControlResources";
            this.Size = new System.Drawing.Size(400, 25);
            this.tsrMain.ResumeLayout(false);
            this.tsrMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsrMain;
        internal System.Windows.Forms.ToolStripButton IconRedBall;
        internal System.Windows.Forms.ToolStripButton IconGreenBall;
        internal System.Windows.Forms.ToolStripButton IconBlueBall;
        internal System.Windows.Forms.ToolStripButton IconArrowLeft;
        internal System.Windows.Forms.ToolStripButton IconArrowRight;
        internal System.Windows.Forms.ToolStripButton IconPencil;
        internal System.Windows.Forms.ToolStripButton IconPlusGreen;
        internal System.Windows.Forms.ToolStripButton IconMinusRed;
        internal System.Windows.Forms.ToolStripButton IconSchema;
        internal System.Windows.Forms.ToolStripButton IconValidateGreen;
        internal System.Windows.Forms.ToolStripButton IconValidateBlack;
        internal System.Windows.Forms.ToolStripButton IconValidateSet;
        internal System.Windows.Forms.ToolStripButton IconRedCross;
        internal System.Windows.Forms.ToolStripButton IconExport;
        internal System.Windows.Forms.ToolStripButton IconImport;
    }

}