﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek pro výběr příspěvků lokálních hustot</para>
    /// <para lang="en">Control for selecting local density contributions</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTDLDsityObject
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const int radioButtonHeight = 18;
        private const int spacing = 2;
        private const int marginTop = 5;
        private const int marginLeft = 5;
        private const int marginBottom = 5;
        private const int marginRight = 5;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </summary>
        private TDLDsityObject ldsityObject;

        /// <summary>
        /// <para lang="cs">Seznam příspěvků lokálních hustot</para>
        /// <para lang="en">List of local density contributions</para>
        /// </summary>
        private TDLDsityList ldsities;

        /// <summary>
        /// <para lang="cs">Povoluje prázdný seznam příspěvků lokálních hustot</para>
        /// <para lang="en">Enables empty list of selected local density contributions</para>
        /// </summary>
        private bool enableEmptySelectedLDsities;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Výběr příspěvku lokální hustoty"</para>
        /// <para lang="en">"Selection of the local density contribution changed" event</para>
        /// </summary>
        public event EventHandler SelectionChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="ldsityObject">
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </param>
        /// <param name="ldsities">
        /// <para lang="cs">Seznam příspěvků lokálních hustot</para>
        /// <para lang="en">List of local density contibutions</para>
        /// </param>
        public ControlTDLDsityObject(
            Control controlOwner,
            TDLDsityObject ldsityObject,
            TDLDsityList ldsities
            )
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                ldsityObject: ldsityObject,
                ldsities: ldsities);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </summary>
        public TDLDsityObject LDsityObject
        {
            get
            {
                return ldsityObject ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(LDsityObject)} must not be null.",
                        paramName: nameof(LDsityObject));
            }
            set
            {
                ldsityObject = value ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(LDsityObject)} must not be null.",
                        paramName: nameof(LDsityObject));
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam příspěvků lokálních hustot</para>
        /// <para lang="en">List of local density contributions</para>
        /// </summary>
        public TDLDsityList LDsities
        {
            get
            {
                return ldsities ??
                     throw new ArgumentException(
                        message: $"Argument {nameof(LDsities)} must not be null.",
                        paramName: nameof(LDsities));
            }
            set
            {
                ldsities = value ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(LDsities)} must not be null.",
                        paramName: nameof(LDsities));
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných příspěvků lokálních hustot</para>
        /// <para lang="en">List of selected local density contributions</para>
        /// </summary>
        public List<TDLDsity> SelectedLDsities
        {
            get
            {
                List<TDLDsity> list = [];
                foreach (RadioButton rdo in
                    pnlControls.Controls.OfType<RadioButton>())
                {
                    if (rdo.Checked)
                    {
                        list.Add(item: (TDLDsity)rdo.Tag);
                    }
                }
                return list;
            }
            set
            {
                List<int> list = value.Select(a => a.Id).ToList<int>();
                foreach (RadioButton rdo in
                    pnlControls.Controls.OfType<RadioButton>())
                {
                    rdo.Checked =
                        list.Contains(item: ((TDLDsity)rdo.Tag).Id);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Povoluje prázdný seznam příspěvků lokálních hustot</para>
        /// <para lang="en">Enables empty list of selected local density contributions</para>
        /// </summary>
        public bool EnableEmptySelectedLDsities
        {
            get
            {
                return enableEmptySelectedLDsities;
            }
            set
            {
                enableEmptySelectedLDsities = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                (languageFile == null)
                    ? []
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDLDsityObject),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International =>
                (languageFile == null)
                    ? []
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDLDsityObject),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="ldsityObject">
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </param>
        /// <param name="ldsities">
        /// <para lang="cs">Seznam příspěvků lokálních hustot</para>
        /// <para lang="en">List of local density contibutions</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            TDLDsityObject ldsityObject,
            TDLDsityList ldsities
            )
        {
            ControlOwner = controlOwner;
            LDsityObject = ldsityObject;
            LDsities = ldsities;
            EnableEmptySelectedLDsities = false;

            InitializeControls();
            InitializeLabels();

            pnlControls.Resize += new EventHandler(
                (sender, e) =>
                {
                    foreach (RadioButton rdo in
                        pnlControls.Controls.OfType<RadioButton>())
                    {
                        rdo.Width = pnlControls.Width - marginLeft - marginRight;
                    }
                });

            lblLDsityObjectCaption.Click += new EventHandler(
                (sender, e) =>
                {
                    if (EnableEmptySelectedLDsities)
                    {
                        foreach (RadioButton rdo in
                            pnlControls.Controls.OfType<RadioButton>())
                        {
                            rdo.Checked = false;
                        }
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        private void InitializeControls()
        {
            int i = -1;
            pnlControls.Controls.Clear();

            if (LDsities == null)
            {
                Height = lblLDsityObjectCaption.Height;
                return;
            }

            if (LDsities.Items.Count == 0)
            {
                Height = lblLDsityObjectCaption.Height;
                return;
            }

            foreach (TDLDsity ldsity in LDsities.Items)
            {
                RadioButton rdo = new()
                {
                    Checked = false,
                    Height = radioButtonHeight,
                    Left = marginLeft,
                    Tag = ldsity,
                    Text = String.Empty,
                    Top = marginTop + (++i) * (radioButtonHeight + spacing),
                    Width = pnlControls.Width - marginLeft - marginRight
                };
                rdo.CheckedChanged += new EventHandler(
                    (sender, e) => { SelectionChanged?.Invoke(sender: this, e: new EventArgs()); });
                rdo.CreateControl();
                pnlControls.Controls.Add(value: rdo);
            }
            RadioButton rdoFirst =
                pnlControls.Controls.OfType<RadioButton>().FirstOrDefault<RadioButton>();
            if (rdoFirst != null)
            {
                rdoFirst.Checked = true;
            }
            Height =
                lblLDsityObjectCaption.Height +
                marginTop +
                pnlControls.Controls.Count * (radioButtonHeight + spacing) +
                marginBottom;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:

                    lblLDsityObjectCaption.Text =
                        (LDsityObject != null) ?
                        LDsityObject.LabelCs : String.Empty;

                    foreach (RadioButton rdo in
                        pnlControls.Controls.OfType<RadioButton>())
                    {
                        TDLDsity ldsity = (TDLDsity)rdo.Tag;
                        rdo.Text =
                            (ldsity != null) ?
                            ldsity.ExtendedLabelCs : String.Empty;
                    }

                    break;

                case LanguageVersion.International:

                    lblLDsityObjectCaption.Text =
                        (LDsityObject != null) ?
                        LDsityObject.LabelEn : String.Empty;

                    foreach (RadioButton rdo in
                        pnlControls.Controls.OfType<RadioButton>())
                    {
                        TDLDsity ldsity = (TDLDsity)rdo.Tag;
                        rdo.Text =
                            (ldsity != null) ?
                            ldsity.ExtendedLabelEn : String.Empty;
                    }

                    break;

                default:

                    lblLDsityObjectCaption.Text = String.Empty;

                    foreach (RadioButton rdo in
                        pnlControls.Controls.OfType<RadioButton>())
                    {
                        rdo.Text = String.Empty;
                    }

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}