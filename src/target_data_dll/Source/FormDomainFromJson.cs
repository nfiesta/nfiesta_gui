﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nové plošné domény nebo subpopulace
    /// načtením parametrů plošné domény nebo subpopulace z JSON souboru - třída pro Designer</para>
    /// <para lang="en">Form to create a new area domain or subpopulation
    /// by loading area domain or subpopulation parameters from JSON file - class for Designer</para>
    /// </summary>
    internal partial class FormDomainFromJsonDesign
        : Form
    {
        protected FormDomainFromJsonDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Button BtnCancel => btnCancel;
        protected System.Windows.Forms.Button BtnOK => btnOK;
        protected System.Windows.Forms.Panel PnlHeader => pnlHeader;
        protected System.Windows.Forms.Panel PnlWorkSpace => pnlWorkSpace;
    }

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nové plošné domény nebo subpopulace
    /// načtením parametrů plošné domény nebo subpopulace z JSON souboru</para>
    /// <para lang="en">Form to create a new area domain or subpopulation
    /// by loading area domain or subpopulation parameters from JSON file</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class FormDomainFromJson<TDomain, TCategory>
        : FormDomainFromJsonDesign, INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot</para>
        /// <para lang="en">Local density objects</para>
        /// </summary>
        private List<TDLDsityObject> ldsityObjects;

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot pro třídění</para>
        /// <para lang="en">Local density objects for classification</para>
        /// </summary>
        private Dictionary<TDLDsityObject, List<TDLDsityObject>> ldsityObjectsForADSP;

        /// <summary>
        /// <para lang="cs">Skupina klasifikačních pravidel</para>
        /// <para lang="en">Collection of classification rules</para>
        /// </summary>
        private ClassificationRuleCollection<TDomain, TCategory> collection;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Omezuje zobrazení pouze na 1 zvolený LDsityObject a LDsityObjectForADSP</para>
        /// <para lang="en">Restricts a display to 1 selected LDsityObject and LDsityObjectForADSP</para>
        /// </summary>
        public bool SingleRowMode { get; set; } = false;

        #endregion Private Fields


        #region Controls

        private System.Windows.Forms.Label lblLDsityObjectHeader;

        private System.Windows.Forms.Label lblLDsityObjectForADSPHeader;

        private System.Windows.Forms.Label lblLDsityObjectJsonHeader;

        private readonly System.Windows.Forms.ComboBox cboLocalDensityObject = new();

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormDomainFromJson(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot</para>
        /// <para lang="en">Local density objects</para>
        /// </summary>
        public List<TDLDsityObject> LDsityObjects
        {
            get
            {
                return ldsityObjects ?? [];
            }
            set
            {
                ldsityObjects = value ?? [];

                ldsityObjectsForADSP = [];
                foreach (TDLDsityObject ldsityObject in LDsityObjects)
                {
                    if (!ldsityObjectsForADSP.ContainsKey(key: ldsityObject))
                    {
                        ldsityObjectsForADSP.Add(
                            key: ldsityObject,
                            value:
                                TDFunctions.FnGetLDsityObjectForADSP.Execute(
                                    database: Database,
                                    ldsityObjectId: ldsityObject.Id,
                                    arealOrPopulationId: (int)ArealOrPopulation).Items);
                    }
                }

                if (LDsityObjects.Count != 0 && !Collection.IsEmpty)
                {
                    InitializeControls();
                    InitializeLabels();
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Objekty lokálních hustot pro třídění,
        /// klíč: objekt lokální hustoty,
        /// hodnota: seznam odpovídajících objektů lokální hustoty pro třídění (read-only)</para>
        /// <para lang="en">
        /// Local density objects for classification,
        /// key: local density object,
        /// value: list of corresponding local density objects for classification (read-only)</para>
        /// </summary>
        public Dictionary<TDLDsityObject, List<TDLDsityObject>> LDsityObjectsForADSP
        {
            get
            {
                return ldsityObjectsForADSP ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Skupina klasifikačních pravidel</para>
        /// <para lang="en">Collection of classification rules</para>
        /// </summary>
        public ClassificationRuleCollection<TDomain, TCategory> Collection
        {
            get
            {
                return
                    collection ??
                        new ClassificationRuleCollection<TDomain, TCategory>();
            }
            set
            {
                collection =
                    value ??
                        new ClassificationRuleCollection<TDomain, TCategory>();

                if (LDsityObjects.Count != 0 && !Collection.IsEmpty)
                {
                    InitializeControls();
                    InitializeLabels();
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Párování objektů lokálních hustot pro třídění (výsledek tohoto formuláře),
        /// klíč: objekt lokální hustoty pro třídění z databáze,
        /// hodnota: objekt lokální hustoty pro třídění z json souboru (read-only)</para>
        /// <para lang="en">
        /// Pairing local density objects for classification (result of this form),
        /// key: local density object for classification from database,
        /// value: local density object for classification from json file (read-only)</para>
        /// </summary>
        public ReplacingLocalDensityObject<TDomain, TCategory> Pairing
        {
            get
            {
                ReplacingLocalDensityObject<TDomain, TCategory> pairing =
                    new();

                if (!SingleRowMode)
                {
                    foreach (TDLDsityObject ldsityObjectForADSP in LDsityObjectsForADSP.Values
                                    .SelectMany(a => a)
                                    .Distinct()
                                    .OrderBy(a => a.Id))
                    {
                        pairing.Add(
                            ldsityObjectForADSP: ldsityObjectForADSP,
                            ldsityObjectForADSPJson:
                                PnlWorkSpace.Controls.OfType<System.Windows.Forms.ComboBox>()
                                .Where(a => (a.Tag != null) && (a.Tag is TDLDsityObject tag) && (tag.Id == ldsityObjectForADSP.Id))
                                .Where(a => a.SelectedItem != null)
                                .Select(a => (ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON)a.SelectedItem)
                                .FirstOrDefault<ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON>());
                    }
                }
                else
                {
                    // Najdeme ComboBox
                    // Finding ComboBox
                    ComboBox cmbJson = PnlWorkSpace.Controls
                        .OfType<ComboBox>()
                        .FirstOrDefault();

                    if (cmbJson != null)
                    {
                        TDLDsityObject ldsityObjectForADSP = (TDLDsityObject)cmbJson.Tag;
                        if (ldsityObjectForADSP == null)
                            return pairing;

                        // vybraná položka v ComboBoxu => LocalDensityObjectJSON
                        // selected item in combobox ComboBoxu => LocalDensityObjectJSON
                        if (cmbJson.SelectedItem is ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON selectedJson)
                        {
                            pairing.Add(ldsityObjectForADSP, selectedJson);
                        }
                    }
                }

                return pairing;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",    "Načtení klasifikačních pravidel plošné domény z JSON souboru" },
                            { nameof(lblLDsityObjectHeader),                        "Objekt lokální hustoty" },
                            { nameof(lblLDsityObjectForADSPHeader),                 "Objekt lokální hustoty pro třídění" },
                            { nameof(lblLDsityObjectJsonHeader),                    "Přiřazený objekt s klasifikačními pravidly (json)" },
                            { nameof(cboLocalDensityObject),                        "LocalDensityObjectExtendedLabelCs" },
                            { nameof(BtnOK),                                        "Další" },
                            { nameof(BtnCancel),                                    "Zrušit" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormDomainFromJson<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",    "Načtení klasifikačních pravidel subpopulace z JSON souboru" },
                            { nameof(lblLDsityObjectHeader),                        "Objekt lokální hustoty" },
                            { nameof(lblLDsityObjectForADSPHeader),                 "Objekt lokální hustoty pro třídění" },
                            { nameof(lblLDsityObjectJsonHeader),                    "Přiřazený objekt s klasifikačními pravidly (json)" },
                            { nameof(cboLocalDensityObject),                        "LocalDensityObjectExtendedLabelCs" },
                            { nameof(BtnOK),                                        "Další" },
                            { nameof(BtnCancel),                                    "Zrušit" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormDomainFromJson<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictNationalSubPopulation)
                                ? dictNationalSubPopulation
                                : [],

                    _ => [],
                },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",    "Import classification rules for area domain from JSON file"},
                            { nameof(lblLDsityObjectHeader),                        "Local density object" },
                            { nameof(lblLDsityObjectForADSPHeader),                 "Local density object for classification" },
                            { nameof(lblLDsityObjectJsonHeader),                    "Assigned object with classification rules (json)" },
                            { nameof(cboLocalDensityObject),                        "LocalDensityObjectExtendedLabelEn" },
                            { nameof(BtnOK),                                        "Next" },
                            { nameof(BtnCancel),                                    "Cancel" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormDomainFromJson<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryEdit<TDomain, TCategory>)}",    "Import classification rules for area domain from JSON file"},
                            { nameof(lblLDsityObjectHeader),                        "Local density object" },
                            { nameof(lblLDsityObjectForADSPHeader),                 "Local density object for classification" },
                            { nameof(lblLDsityObjectJsonHeader),                    "Assigned object with classification rules (json)" },
                            { nameof(cboLocalDensityObject),                        "LocalDensityObjectExtendedLabelEn" },
                            { nameof(BtnOK),                                        "Next" },
                            { nameof(BtnCancel),                                    "Cancel" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                             key: $"{nameof(FormDomainFromJson<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                             out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            LDsityObjects = [];
            Collection = new ClassificationRuleCollection<TDomain, TCategory>();

            onEdit = false;

            InitializeLabels();

            BtnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            BtnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            SetStyle();
            BtnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacích prvků na formuláři</para>
        /// <para lang="en">Initializing user controls on the form</para>
        /// </summary>
        private void InitializeControls()
        {
            int columnWidth = 290;
            int rowHeight = 30;
            int indentWidth = 10;

            #region Header

            lblLDsityObjectHeader = new()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Top,
                AutoEllipsis = true,
                AutoSize = false,
                Font = Setting.TableHeaderFont,
                ForeColor = Setting.TableHeaderForeColor,
                Height = PnlHeader.Height,
                Left = 0 * (columnWidth + indentWidth) + indentWidth,
                Name = nameof(lblLDsityObjectHeader),
                TextAlign = ContentAlignment.MiddleLeft,
                Top = 0,
                Width = columnWidth
            };

            lblLDsityObjectForADSPHeader = new()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Top,
                AutoEllipsis = true,
                AutoSize = false,
                Font = Setting.TableHeaderFont,
                ForeColor = Setting.TableHeaderForeColor,
                Height = PnlHeader.Height,
                Left = 1 * (columnWidth + indentWidth) + indentWidth,
                Name = nameof(lblLDsityObjectForADSPHeader),
                Tag = null,
                TextAlign = ContentAlignment.MiddleLeft,
                Top = 0,
                Width = columnWidth
            };

            lblLDsityObjectJsonHeader = new()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right,
                AutoEllipsis = true,
                AutoSize = false,
                Font = Setting.TableHeaderFont,
                ForeColor = Setting.TableHeaderForeColor,
                Height = PnlHeader.Height,
                Left = 2 * (columnWidth + indentWidth) + indentWidth,
                Name = nameof(lblLDsityObjectJsonHeader),
                TextAlign = ContentAlignment.MiddleLeft,
                Top = 0,
                Width = columnWidth
            };

            lblLDsityObjectHeader.CreateControl();
            lblLDsityObjectForADSPHeader.CreateControl();
            lblLDsityObjectJsonHeader.CreateControl();

            PnlHeader.Controls.Clear();
            PnlHeader.Controls.Add(value: lblLDsityObjectHeader);
            PnlHeader.Controls.Add(value: lblLDsityObjectForADSPHeader);
            PnlHeader.Controls.Add(value: lblLDsityObjectJsonHeader);

            #endregion Header

            #region WorkSpace

            PnlWorkSpace.Controls.Clear();

            int top = 0;

            if (!SingleRowMode)
            {
                foreach (TDLDsityObject ldsityObject in LDsityObjectsForADSP.Keys)
                {
                    System.Windows.Forms.Label lblLDsityObject =
                        new()
                        {
                            Anchor = AnchorStyles.Left | AnchorStyles.Top,
                            AutoEllipsis = true,
                            AutoSize = false,
                            Font = Setting.LabelValueFont,
                            ForeColor = Setting.LabelValueForeColor,
                            Height = rowHeight,
                            Left = 0 * (columnWidth + indentWidth) + indentWidth,
                            Tag = ldsityObject,
                            TextAlign = ContentAlignment.MiddleLeft,
                            Top = top,
                            Width = columnWidth
                        };
                    lblLDsityObject.CreateControl();
                    PnlWorkSpace.Controls.Add(value: lblLDsityObject);

                    foreach (TDLDsityObject ldsityObjectForADSP in LDsityObjectsForADSP[ldsityObject])
                    {
                        System.Windows.Forms.Label lblLDsityObjectForADSP =
                            new()
                            {
                                Anchor = AnchorStyles.Left | AnchorStyles.Top,
                                AutoEllipsis = true,
                                AutoSize = false,
                                Font = Setting.LabelValueFont,
                                ForeColor = Setting.LabelValueForeColor,
                                Height = rowHeight,
                                Left = 1 * (columnWidth + indentWidth) + indentWidth,
                                Tag = ldsityObjectForADSP,
                                TextAlign = ContentAlignment.MiddleLeft,
                                Top = top,
                                Width = columnWidth
                            };
                        lblLDsityObjectForADSP.CreateControl();
                        PnlWorkSpace.Controls.Add(value: lblLDsityObjectForADSP);

                        System.Windows.Forms.ComboBox cboLDsityObjectForADSP =
                            new()
                            {
                                Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right,
                                DataSource = Collection.LDsityObjectsForADSPExtended,
                                DropDownStyle = ComboBoxStyle.DropDownList,
                                FlatStyle = FlatStyle.Flat,
                                Font = Setting.ComboBoxFont,
                                ForeColor = Setting.ComboBoxForeColor,
                                Left = 2 * (columnWidth + indentWidth) + indentWidth,
                                Tag = ldsityObjectForADSP,
                                Top = top + 5,
                                Width = columnWidth
                            };

                        cboLDsityObjectForADSP.SelectedIndexChanged += new EventHandler(
                            (sender, e) =>
                            {
                                if (
                                    (!onEdit) &&
                                    (cboLDsityObjectForADSP.SelectedItem != null) &&
                                    (cboLDsityObjectForADSP.Tag != null))
                                {
                                    int index = cboLDsityObjectForADSP.SelectedIndex;
                                    TDLDsityObject ldsityObjAdsp = (TDLDsityObject)cboLDsityObjectForADSP.Tag;

                                    onEdit = true;

                                    List<ComboBox> comboBoxes =
                                        PnlWorkSpace.Controls.OfType<ComboBox>()
                                            .Where(a => ((TDLDsityObject)a.Tag).Id == ldsityObjAdsp.Id)
                                            .Select(a => (ComboBox)a)
                                            .ToList<ComboBox>();

                                    foreach (ComboBox comboBox in comboBoxes)
                                    {
                                        comboBox.SelectedIndex = index;
                                    }

                                    onEdit = false;
                                }
                                BtnOK.Focus();
                            });

                        cboLDsityObjectForADSP.CreateControl();
                        PnlWorkSpace.Controls.Add(value: cboLDsityObjectForADSP);

                        top = lblLDsityObjectForADSP.Top + lblLDsityObjectForADSP.Height;
                    }
                }
            }
            else // SingleRowMode
            {
                var frmClassificationRule = Application.OpenForms
                    .OfType<FormClassificationRule<TDomain, TCategory>>()
                    .FirstOrDefault();

                TDLDsityObject ldsityObject = frmClassificationRule.LDsityObject;
                TDLDsityObject ldsityObjectForADSP = frmClassificationRule.LDsityObjectForADSP;
                TDLDsityObject ldsityObjectsForADSP = LDsityObjects.FirstOrDefault();

                List<ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON> allJsonObjects
                     = new List<ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON>();
                foreach (var cat in Collection.Categories)
                {
                    allJsonObjects.AddRange(cat.LDsityObjectsForADSP);
                }

                // 1) Label pro Local density object v prvním sloupci
                // 1) Label for Local density object in the first column
                Label lblLDsityObject = new Label
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Top,
                    AutoSize = false,
                    Font = Setting.LabelValueFont,
                    ForeColor = Setting.LabelValueForeColor,
                    Height = rowHeight,
                    Left = 0 * (columnWidth + indentWidth) + indentWidth,
                    Top = top,
                    Width = columnWidth,
                    TextAlign = ContentAlignment.MiddleLeft,
                    Tag = ldsityObject
                };

                PnlWorkSpace.Controls.Add(lblLDsityObject);

                // 2) Label pro Local density object for classification ve druhém sloupci
                // 2) Label for Local density object for classification in the second column
                Label lblLDsityObjectForClassification = new Label
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Top,
                    AutoSize = false,
                    Font = Setting.LabelValueFont,
                    ForeColor = Setting.LabelValueForeColor,
                    Height = rowHeight,
                    Left = 1 * (columnWidth + indentWidth) + indentWidth,
                    Top = top,
                    Width = columnWidth,
                    TextAlign = ContentAlignment.MiddleLeft,
                    Tag = /*lsityObjectsForADSP*/ ldsityObjectForADSP
                };

                PnlWorkSpace.Controls.Add(lblLDsityObjectForClassification);

                // 3) ComboBox – DataSource = allJsonObjects
                ComboBox cmbJson = new ComboBox
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Top,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    Font = Setting.ComboBoxFont,
                    ForeColor = Setting.ComboBoxForeColor,
                    Left = 2 * (columnWidth + indentWidth) + indentWidth,
                    Top = top + 5,
                    Width = columnWidth,
                    Tag = ldsityObjectsForADSP
                };

                // Nastavíme DataSource
                // Setting DataSource
                cmbJson.DataSource = allJsonObjects
                    .Distinct()
                    .OrderBy(o => o.LocalDensityObjectId)
                    .ToList();

                // Zobrazíme buď LocalDensityObjectLabelCs nebo LocalDensityObjectLabelEn
                // Displaying either LocalDensityObjectLabelCs nebo LocalDensityObjectLabelEn
                cmbJson.DisplayMember =
                   (LanguageVersion == LanguageVersion.National)
                     ? nameof(ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON.LocalDensityObjectLabelCs)
                     : nameof(ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON.LocalDensityObjectLabelEn);

                PnlWorkSpace.Controls.Add(cmbJson);
            }

            #endregion WorkSpace
        }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            BtnOK.Font = Setting.ButtonFont;
            BtnOK.ForeColor = Setting.ButtonForeColor;

            BtnCancel.Font = Setting.ButtonFont;
            BtnCancel.ForeColor = Setting.ButtonForeColor;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: $"{nameof(FormDomainFromJson<TDomain, TCategory>)}",
                    out string frmDomainFromJsonText)
                        ? frmDomainFromJsonText
                        : String.Empty;

            BtnOK.Text =
               labels.TryGetValue(
                   key: nameof(BtnOK),
                   out string btnOKText)
                       ? btnOKText
                       : String.Empty;

            BtnCancel.Text =
                labels.TryGetValue(
                    key: nameof(BtnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            if (lblLDsityObjectHeader != null)
            {
                lblLDsityObjectHeader.Text =
                    labels.TryGetValue(
                        key: nameof(lblLDsityObjectHeader),
                        out string lblLDsityObjectHeaderText)
                            ? lblLDsityObjectHeaderText
                            : String.Empty;
            }

            if (lblLDsityObjectForADSPHeader != null)
            {
                lblLDsityObjectForADSPHeader.Text =
                    labels.TryGetValue(
                        key: nameof(lblLDsityObjectForADSPHeader),
                        out string lblLDsityObjectForADSPHeaderText)
                            ? lblLDsityObjectForADSPHeaderText
                            : String.Empty;
            }

            if (lblLDsityObjectJsonHeader != null)
            {
                lblLDsityObjectJsonHeader.Text =
                    labels.TryGetValue(
                        key: nameof(lblLDsityObjectJsonHeader),
                        out string lblLDsityObjectJsonHeaderText)
                            ? lblLDsityObjectJsonHeaderText
                            : String.Empty;
            }

            foreach (System.Windows.Forms.Label label in
                PnlWorkSpace.Controls.OfType<System.Windows.Forms.Label>())
            {
                if ((label.Tag != null) && (label.Tag is TDLDsityObject ldsityObject))
                {
                    label.Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? ldsityObject.ExtendedLabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? ldsityObject.ExtendedLabelCs
                                : ldsityObject.ExtendedLabelEn;
                }
            }

            onEdit = true;
            foreach (System.Windows.Forms.ComboBox comboBox in
                PnlWorkSpace.Controls.OfType<System.Windows.Forms.ComboBox>())
            {
                comboBox.DisplayMember =
                    labels.TryGetValue(
                        key: nameof(cboLocalDensityObject),
                        out string cboLocalDensityObjectDisplayMember)
                            ? cboLocalDensityObjectDisplayMember
                            : String.Empty;
            }
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}