﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro "Omezení cílové veličiny na úrovni bez rozlišení"</para>
    /// <para lang="en">Form for "Limit target variable at level of the altogether category"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormTargetVariableLimit
            : Form, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Barva vybraného tlačítka pro objekt lokální hustoty</para>
        /// <para lang="en">Selected button for local density object color</para>
        /// </summary>
        private static readonly Color ActiveButtonColor = SystemColors.Window;

        /// <summary>
        /// <para lang="cs">Barva ostatních tlačítek pro objekty lokální hustoty</para>
        /// <para lang="en">Other buttons for local density objects color</para>
        /// </summary>
        private static readonly Color InActiveButtonColor = SystemColors.Control;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Vstupní argumenty uložených procedur
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input a fn_save_constrained_target_variable
        /// </para>
        /// <para lang="en">Input arguments for stored procedures
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input and fn_save_constrained_target_variable
        /// </para>
        /// </summary>
        private CategorizationSetupArgs categorizationSetupArgs;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="categorizationSetupArgs">
        /// <para lang="cs">Vstupní argumenty uložených procedur
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input a fn_save_constrained_target_variable
        /// </para>
        /// <para lang="en">Input arguments for stored procedures
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input and fn_save_constrained_target_variable
        /// </para>
        /// </param>
        public FormTargetVariableLimit(
            Control controlOwner,
            CategorizationSetupArgs categorizationSetupArgs)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                categorizationSetupArgs: categorizationSetupArgs);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vstupní argumenty uložených procedur
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input a fn_save_constrained_target_variable
        /// </para>
        /// <para lang="en">Input arguments for stored procedures
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input and fn_save_constrained_target_variable
        /// </para>
        /// </summary>
        public CategorizationSetupArgs CategorizationSetupArgs
        {
            get
            {
                return categorizationSetupArgs;
            }
            private set
            {
                categorizationSetupArgs = value;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Seznam vybraných atributových kategorií
        /// (kombinací kategorií plošných domén a subpopulací)
        /// (read-only)
        /// </para>
        /// <para lang="en">
        /// List of selected attribute categories
        /// (combinations of area domain and subpopulation categories)
        /// (read-only)</para>
        /// </summary>
        private TDAttributeCategoryList AttributeCategories
        {
            get
            {
                return
                    ((ControlTDSelectionAttributeCategory)ControlOwner)
                    .SelectedAttributeCategories;
            }
        }

        /// <summary>
        /// <para lang="cs"> Objekty lokálních hustot pro vybranou cílovou proměnnou (read-only) </para>
        /// <para lang="en"> Local density objects for selected target variable (read-only) </para>
        /// </summary>
        private Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            LDsityObjectsForTargetVariable
        {
            get
            {
                return
                    ((ControlTDSelectionAttributeCategory)ControlOwner)
                    .LDsityObjectsForTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs"> Vybrané atributové kategorie pro jednotlivé objekty lokálních hustot </para>
        /// <para lang="en"> Selected attribute categories for each local density object </para>
        /// </summary>
        private Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDAttributeCategory>
            SelectedAttributeCategories
        {
            get
            {
                Dictionary<
                    TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                    TDAttributeCategory>
                        result = [];

                foreach (Button button in pnlLDsityObject.Controls)
                {
                    Panel panel = (Panel)button.Tag;

                    TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier ldsityObject =
                        (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier)panel.Tag;

                    RadioButton selectedRadioButton =
                        panel.Controls.OfType<RadioButton>()
                        .Where(a => a.Checked)
                        .FirstOrDefault<RadioButton>();

                    result.Add(
                        key: ldsityObject,
                        value: (TDAttributeCategory)selectedRadioButton.Tag);
                }

                return result;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Seznam identifikačních čísel kategorií plošné domény, které tvoří atributovou kategorii
        /// </para>
        /// <para lang="en">
        /// List of area domain category identification numbers that make up an attribute category
        /// </para>
        /// </summary>
        public List<List<Nullable<int>>> AreaDomainCategoryIds
        {
            get
            {
                List<List<Nullable<int>>> result = [];

                // Prvky ve vnějším poli musí být seřazeny
                // podle identifikátoru lokální hustoty a use negative (podle klíče slovníku)
                foreach (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier key in
                    SelectedAttributeCategories
                    .OrderBy(a => a.Key)
                    .ToDictionary(a => a.Key, a => a.Value)
                    .Keys)
                {
                    List<Nullable<int>> inner = null;
                    TDAttributeCategory attributeCategory = SelectedAttributeCategories[key];
                    if (attributeCategory.AreaDomainCategoryCombination.Items.Count != 0)
                    {
                        inner = attributeCategory.AreaDomainCategoryCombination.Items
                                .Select(a => (Nullable<int>)a.Id)
                                .ToList<Nullable<int>>();
                    }
                    result.Add(item: inner);
                }

                if (result.All(a => a == null))
                {
                    return null;
                }

                return result;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Seznam identifikačních čísel kategorií subpopulace, které tvoří atributovou kategorii
        /// </para>
        /// <para lang="en">
        /// List of subpopulation category identification numbers that make up an attribute category
        /// </para>
        /// </summary>
        public List<List<Nullable<int>>> SubPopulationCategoryIds
        {
            get
            {
                List<List<Nullable<int>>> result = [];

                // Prvky ve vnějším poli musí být seřazeny
                // podle identifikátoru lokální hustoty a use negative (podle klíče slovníku)
                foreach (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier key in
                    SelectedAttributeCategories
                    .OrderBy(a => a.Key)
                    .ToDictionary(a => a.Key, a => a.Value)
                    .Keys)
                {
                    List<Nullable<int>> inner = null;
                    TDAttributeCategory attributeCategory = SelectedAttributeCategories[key];
                    if (attributeCategory.SubPopulationCategoryCombination.Items.Count != 0)
                    {
                        inner = attributeCategory.SubPopulationCategoryCombination.Items
                                .Select(a => (Nullable<int>)a.Id)
                                .ToList<Nullable<int>>();
                    }
                    result.Add(item: inner);
                }

                if (result.All(a => a == null))
                {
                    return null;
                }

                return result;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTargetVariableLimit),  "Omezení cílové veličiny na úrovni bez rozlišení" },
                        { nameof(btnCancel),                "Zrušit" },
                        { nameof(btnOK),                    "Zapsat do databáze" },
                        { nameof(grpAttributeCategory),     "Atributová kategorie 'bez rozlišení':" },
                        { nameof(grpLDsityObject),          "Objekt lokální hustoty:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormTargetVariableLimit),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTargetVariableLimit),  "Limit target variable at level of the altogether category" },
                        { nameof(btnCancel),                "Cancel" },
                        { nameof(btnOK),                    "Write to database" },
                        { nameof(grpAttributeCategory),     "Root attribute category:" },
                        { nameof(grpLDsityObject),          "Local density object:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormTargetVariableLimit),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="categorizationSetupArgs">
        /// <para lang="cs">Vstupní argumenty uložených procedur
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input a fn_save_constrained_target_variable
        /// </para>
        /// <para lang="en">Input arguments for stored procedures
        /// fn_get_categorization_setup, fn_save_categorization_setup,
        /// fn_test_categorization_setup_input and fn_save_constrained_target_variable
        /// </para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            CategorizationSetupArgs categorizationSetupArgs)
        {
            ControlOwner = controlOwner;

            CategorizationSetupArgs = categorizationSetupArgs;
            CategorizationSetupArgs.FormTargetVariableLimit = this;

            if (LDsityObjectsForTargetVariable == null)
            {
                btnOK.Enabled = false;
                return;
            }

            if (LDsityObjectsForTargetVariable.Keys.Count == 0)
            {
                btnOK.Enabled = false;
                return;
            }

            if (AttributeCategories == null)
            {
                btnOK.Enabled = false;
                return;
            }

            if (AttributeCategories.Items.Count == 0)
            {
                btnOK.Enabled = false;
                return;
            }

            LoadContent();

            InitializeControls();

            InitializeLabels();

            SetStyle();

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;

            grpAttributeCategory.Font = Setting.GroupBoxFont;
            grpAttributeCategory.ForeColor = Setting.GroupBoxForeColor;

            grpLDsityObject.Font = Setting.GroupBoxFont;
            grpLDsityObject.ForeColor = Setting.GroupBoxForeColor;
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu objektů lokálních hustot</para>
        /// <para lang="en">Initialization of list of local density objects</para>
        /// </summary>
        private void InitializeControls()
        {
            pnlLDsityObject.Controls.Clear();
            pnlAttributeCategory.Controls.Clear();

            int i = 0;
            foreach (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier ldsityObject
                in LDsityObjectsForTargetVariable.Keys)
            {
                Panel panel = new()
                {
                    AutoScroll = true,
                    Dock = DockStyle.Fill,
                    Visible = false,
                    Tag = ldsityObject
                };
                panel.CreateControl();
                pnlAttributeCategory.Controls.Add(value: panel);

                int j = 0;
                foreach (TDAttributeCategory attributeCategory in AttributeCategories.Items)
                {
                    RadioButton rdo = new()
                    {
                        Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                        Height = 23,
                        Left = 10,
                        Tag = attributeCategory,
                        Text = String.Empty,
                        Top = 25 * j + 5,
                        Width = pnlAttributeCategory.Width - 30,
                    };
                    rdo.CreateControl();
                    panel.Controls.Add(value: rdo);
                    j++;

                    List<int> areaDomainsWithoutClassRules =
                        CategorizationSetupArgs.ClassificationRulesForADLDObjects
                        .Where(a => ldsityObject.ToString() == a.LDSityObject.ToString())
                        .Where(a => !a.WithClassificationRules)
                        .Select(a => a.AreaDomain.Id)
                        .ToList<int>();

                    List<int> subPopulationsWithoutClassRules =
                        CategorizationSetupArgs.ClassificationRulesForSPLDObjects
                        .Where(a => ldsityObject.ToString() == a.LDSityObject.ToString())
                        .Where(a => !a.WithClassificationRules)
                        .Select(a => a.SubPopulation.Id)
                        .ToList<int>();

                    rdo.Enabled = !(
                        attributeCategory.AreaDomainCategoryCombination.Items
                            .Where(a => areaDomainsWithoutClassRules.Contains(item: a.AreaDomainId ?? 0))
                            .Any() ||
                        attributeCategory.SubPopulationCategoryCombination.Items
                            .Where(a => subPopulationsWithoutClassRules.Contains(item: a.SubPopulationId ?? 0))
                            .Any()
                        );
                }

                RadioButton firstRadioButton =
                    panel.Controls.OfType<RadioButton>()
                        .Where(a => a.Enabled)
                        .FirstOrDefault();
                if (firstRadioButton != null)
                {
                    firstRadioButton.Checked = true;
                }

                Button button = new()
                {
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    BackColor = SystemColors.Control,
                    FlatStyle = FlatStyle.Flat,
                    Font = new Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9F,
                        style: FontStyle.Regular,
                        unit: GraphicsUnit.Point,
                        gdiCharSet: (byte)0),
                    Height = 25,
                    Left = 10,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    Tag = panel,
                    Text = ldsityObject.ToString(),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Top = 30 * i + 5,
                    Width = pnlLDsityObject.Width - 20
                };
                button.Click += (sender, e) =>
                {
                    SelectLDsityObject(sender: (Button)sender);
                };
                button.CreateControl();
                pnlLDsityObject.Controls.Add(value: button);
                i++;
            }

            SelectLDsityObject(sender:
                pnlLDsityObject.Controls.OfType<Button>().FirstOrDefault());
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing the form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: nameof(FormTargetVariableLimit),
                    out string frmTargetVariableLimitText)
                        ? frmTargetVariableLimitText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(
                    key: "btnCancel",
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(
                    key: "btnOK",
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            grpAttributeCategory.Text =
               labels.TryGetValue(
                   key: "grpAttributeCategory",
                   out string grpAttributeCategoryText)
                       ? grpAttributeCategoryText
                       : String.Empty;

            grpLDsityObject.Text =
               labels.TryGetValue(
                   key: "grpLDsityObject",
                   out string grpLDsityObjectText)
                       ? grpLDsityObjectText
                       : String.Empty;

            foreach (Button btn in pnlLDsityObject.Controls.OfType<Button>())
            {
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier key =
                    (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier)
                    ((Panel)btn.Tag).Tag;

                btn.Text = LanguageVersion switch
                {
                    LanguageVersion.International =>
                       String.Concat(
                            $"{key.LDsityObject.ExtendedLabelEn} ",
                            key.UseNegative ? "(-)" : "(+)"),

                    LanguageVersion.National =>
                        String.Concat(
                            $"{key.LDsityObject.ExtendedLabelCs} ",
                            key.UseNegative ? "(-)" : "(+)"),
                    _ =>
                        String.Concat(
                            $"{key.LDsityObject.ExtendedLabelEn} ",
                            key.UseNegative ? "(-)" : "(+)"),
                };
            }

            foreach (Panel panel in pnlAttributeCategory.Controls.OfType<Panel>())
            {
                foreach (RadioButton rdo in panel.Controls.OfType<RadioButton>())
                {
                    TDAttributeCategory attributeCategory =
                        (TDAttributeCategory)rdo.Tag;

                    rdo.Text = LanguageVersion switch
                    {
                        LanguageVersion.International =>
                            attributeCategory.ExtendedLabelEn,
                        LanguageVersion.National =>
                            attributeCategory.ExtendedLabelCs,
                        _ =>
                            String.Empty,
                    };
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vyběr objektu lokální hustoty v seznamu</para>
        /// <para lang="en">Selection of the local density object in the list</para>
        /// </summary>
        private void SelectLDsityObject(Button sender)
        {
            if (sender == null)
            {
                return;
            }

            foreach (Button button in pnlLDsityObject.Controls.OfType<Button>())
            {
                button.BackColor = InActiveButtonColor;
                ((Panel)button.Tag).Visible = false;

                if (button.Equals(obj: sender))
                {
                    button.BackColor = ActiveButtonColor;
                    ((Panel)button.Tag).Visible = true;
                    pnlLDsityObject.Tag = button;
                }
            }
        }

        #endregion Methods

    }

}