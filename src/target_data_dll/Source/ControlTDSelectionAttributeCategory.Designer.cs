﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//


namespace ZaJi.ModuleTargetData
{

    partial class ControlTDSelectionAttributeCategory
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlTDSelectionAttributeCategory));
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            pnlSaveCategorizationSetup = new System.Windows.Forms.Panel();
            btnSaveCategorizationSetup = new System.Windows.Forms.Button();
            pnlNotice = new System.Windows.Forms.Panel();
            lblNotice = new System.Windows.Forms.Label();
            pnlWorkSpace = new System.Windows.Forms.Panel();
            splMain = new System.Windows.Forms.SplitContainer();
            splAreaDomainAndSubPopulation = new System.Windows.Forms.SplitContainer();
            pnlAreaDomain = new System.Windows.Forms.Panel();
            splAreaDomain = new System.Windows.Forms.SplitContainer();
            tlpAreaDomain = new System.Windows.Forms.TableLayoutPanel();
            grpAreaDomain = new System.Windows.Forms.GroupBox();
            tlpAreaDomainInner = new System.Windows.Forms.TableLayoutPanel();
            tlpAreaDomainButtons = new System.Windows.Forms.TableLayoutPanel();
            tsrAreaDomain = new System.Windows.Forms.ToolStrip();
            btnAreaDomainEdit = new System.Windows.Forms.ToolStripButton();
            btnAreaDomainAdd = new System.Windows.Forms.ToolStripButton();
            btnAreaDomainDelete = new System.Windows.Forms.ToolStripButton();
            btnAreaDomainExport = new System.Windows.Forms.ToolStripButton();
            btnAreaDomainImport = new System.Windows.Forms.ToolStripButton();
            tsrAreaDomainSelect = new System.Windows.Forms.ToolStrip();
            btnAreaDomainSelect = new System.Windows.Forms.ToolStripButton();
            cboAreaDomain = new System.Windows.Forms.ComboBox();
            pnlAreaDomainClassificationType = new System.Windows.Forms.Panel();
            rdoAreaDomainChangeOrDynamic = new System.Windows.Forms.RadioButton();
            rdoAreaDomainStandard = new System.Windows.Forms.RadioButton();
            grpAreaDomainCategory = new System.Windows.Forms.GroupBox();
            tlpAreaDomainCategoryInner = new System.Windows.Forms.TableLayoutPanel();
            lstAreaDomainCategory = new System.Windows.Forms.ListBox();
            tlpAreaDomainCategoryButtons = new System.Windows.Forms.TableLayoutPanel();
            tsrAreaDomainCategory = new System.Windows.Forms.ToolStrip();
            btnAreaDomainCategoryEdit = new System.Windows.Forms.ToolStripButton();
            splAreaDomainSelected = new System.Windows.Forms.SplitContainer();
            grpAreaDomainSelected = new System.Windows.Forms.GroupBox();
            tlpAreaDomainSelectedInner = new System.Windows.Forms.TableLayoutPanel();
            tsrAreaDomainSelected = new System.Windows.Forms.ToolStrip();
            btnAreaDomainCancel = new System.Windows.Forms.ToolStripButton();
            btnADCHierarchy = new System.Windows.Forms.ToolStripButton();
            pnlAreaDomainSelected = new System.Windows.Forms.Panel();
            grpAreaDomainCategoryCombination = new System.Windows.Forms.GroupBox();
            pnlAreaDomainCategoryCombination = new System.Windows.Forms.Panel();
            pnlSubPopulation = new System.Windows.Forms.Panel();
            splSubPopulation = new System.Windows.Forms.SplitContainer();
            tlpSubPopulation = new System.Windows.Forms.TableLayoutPanel();
            grpSubPopulation = new System.Windows.Forms.GroupBox();
            tlpSubPopulationInner = new System.Windows.Forms.TableLayoutPanel();
            cboSubPopulation = new System.Windows.Forms.ComboBox();
            tlpSubPopulationButtons = new System.Windows.Forms.TableLayoutPanel();
            tsrSubPopulationSelect = new System.Windows.Forms.ToolStrip();
            btnSubPopulationSelect = new System.Windows.Forms.ToolStripButton();
            tsrSubPopulation = new System.Windows.Forms.ToolStrip();
            btnSubPopulationEdit = new System.Windows.Forms.ToolStripButton();
            btnSubPopulationAdd = new System.Windows.Forms.ToolStripButton();
            btnSubPopulationDelete = new System.Windows.Forms.ToolStripButton();
            btnSubPopulationExport = new System.Windows.Forms.ToolStripButton();
            btnSubPopulationImport = new System.Windows.Forms.ToolStripButton();
            pnlSubPopulationClassificationType = new System.Windows.Forms.Panel();
            rdoSubPopulationChangeOrDynamic = new System.Windows.Forms.RadioButton();
            rdoSubPopulationStandard = new System.Windows.Forms.RadioButton();
            grpSubPopulationCategory = new System.Windows.Forms.GroupBox();
            tlpSubPopulationCategoryInner = new System.Windows.Forms.TableLayoutPanel();
            lstSubPopulationCategory = new System.Windows.Forms.ListBox();
            tlpSubPopulationCategoryButtons = new System.Windows.Forms.TableLayoutPanel();
            tsrSubPopulationCategory = new System.Windows.Forms.ToolStrip();
            btnSubPopulationCategoryEdit = new System.Windows.Forms.ToolStripButton();
            splSubPopulationSelected = new System.Windows.Forms.SplitContainer();
            grpSubPopulationSelected = new System.Windows.Forms.GroupBox();
            tlpSubPopulationSelectedInner = new System.Windows.Forms.TableLayoutPanel();
            pnlSubPopulationSelected = new System.Windows.Forms.Panel();
            tsrSubPopulationSelected = new System.Windows.Forms.ToolStrip();
            btnSubPopulationCancel = new System.Windows.Forms.ToolStripButton();
            btnSPCHierarchy = new System.Windows.Forms.ToolStripButton();
            grpSubPopulationCategoryCombination = new System.Windows.Forms.GroupBox();
            pnlSubPopulationCategoryCombination = new System.Windows.Forms.Panel();
            grpAttributeCategory = new System.Windows.Forms.GroupBox();
            tlpAttributeCategoryInner = new System.Windows.Forms.TableLayoutPanel();
            pnlAttributeCategory = new System.Windows.Forms.Panel();
            tsrAttributeCategorySelected = new System.Windows.Forms.ToolStrip();
            btnLimitTargetVariable = new System.Windows.Forms.ToolStripButton();
            pnlCaption = new System.Windows.Forms.Panel();
            splCaption = new System.Windows.Forms.SplitContainer();
            lblMainCaption = new System.Windows.Forms.Label();
            tlpCaption = new System.Windows.Forms.TableLayoutPanel();
            lblSelectedLDsityObjectGroupValue = new System.Windows.Forms.Label();
            lblSelectedLDsityObjectGroupCaption = new System.Windows.Forms.Label();
            lblSelectedTargetVariableCaption = new System.Windows.Forms.Label();
            lblSelectedTargetVariableValue = new System.Windows.Forms.Label();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            pnlSaveCategorizationSetup.SuspendLayout();
            pnlNotice.SuspendLayout();
            pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splAreaDomainAndSubPopulation).BeginInit();
            splAreaDomainAndSubPopulation.Panel1.SuspendLayout();
            splAreaDomainAndSubPopulation.Panel2.SuspendLayout();
            splAreaDomainAndSubPopulation.SuspendLayout();
            pnlAreaDomain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splAreaDomain).BeginInit();
            splAreaDomain.Panel1.SuspendLayout();
            splAreaDomain.Panel2.SuspendLayout();
            splAreaDomain.SuspendLayout();
            tlpAreaDomain.SuspendLayout();
            grpAreaDomain.SuspendLayout();
            tlpAreaDomainInner.SuspendLayout();
            tlpAreaDomainButtons.SuspendLayout();
            tsrAreaDomain.SuspendLayout();
            tsrAreaDomainSelect.SuspendLayout();
            pnlAreaDomainClassificationType.SuspendLayout();
            grpAreaDomainCategory.SuspendLayout();
            tlpAreaDomainCategoryInner.SuspendLayout();
            tlpAreaDomainCategoryButtons.SuspendLayout();
            tsrAreaDomainCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splAreaDomainSelected).BeginInit();
            splAreaDomainSelected.Panel1.SuspendLayout();
            splAreaDomainSelected.Panel2.SuspendLayout();
            splAreaDomainSelected.SuspendLayout();
            grpAreaDomainSelected.SuspendLayout();
            tlpAreaDomainSelectedInner.SuspendLayout();
            tsrAreaDomainSelected.SuspendLayout();
            grpAreaDomainCategoryCombination.SuspendLayout();
            pnlSubPopulation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splSubPopulation).BeginInit();
            splSubPopulation.Panel1.SuspendLayout();
            splSubPopulation.Panel2.SuspendLayout();
            splSubPopulation.SuspendLayout();
            tlpSubPopulation.SuspendLayout();
            grpSubPopulation.SuspendLayout();
            tlpSubPopulationInner.SuspendLayout();
            tlpSubPopulationButtons.SuspendLayout();
            tsrSubPopulationSelect.SuspendLayout();
            tsrSubPopulation.SuspendLayout();
            pnlSubPopulationClassificationType.SuspendLayout();
            grpSubPopulationCategory.SuspendLayout();
            tlpSubPopulationCategoryInner.SuspendLayout();
            tlpSubPopulationCategoryButtons.SuspendLayout();
            tsrSubPopulationCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splSubPopulationSelected).BeginInit();
            splSubPopulationSelected.Panel1.SuspendLayout();
            splSubPopulationSelected.Panel2.SuspendLayout();
            splSubPopulationSelected.SuspendLayout();
            grpSubPopulationSelected.SuspendLayout();
            tlpSubPopulationSelectedInner.SuspendLayout();
            tsrSubPopulationSelected.SuspendLayout();
            grpSubPopulationCategoryCombination.SuspendLayout();
            grpAttributeCategory.SuspendLayout();
            tlpAttributeCategoryInner.SuspendLayout();
            tsrAttributeCategorySelected.SuspendLayout();
            pnlCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splCaption).BeginInit();
            splCaption.Panel1.SuspendLayout();
            splCaption.Panel2.SuspendLayout();
            splCaption.SuspendLayout();
            tlpCaption.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(1120, 623);
            pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 2);
            tlpMain.Controls.Add(pnlWorkSpace, 0, 1);
            tlpMain.Controls.Add(pnlCaption, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1120, 623);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 4;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlNext, 3, 0);
            tlpButtons.Controls.Add(pnlPrevious, 2, 0);
            tlpButtons.Controls.Add(pnlSaveCategorizationSetup, 1, 0);
            tlpButtons.Controls.Add(pnlNotice, 0, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 577);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(1120, 46);
            tlpButtons.TabIndex = 13;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(933, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(6);
            pnlNext.Size = new System.Drawing.Size(187, 46);
            pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnNext.Location = new System.Drawing.Point(6, 6);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(175, 34);
            btnNext.TabIndex = 15;
            btnNext.Text = "btnNext";
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(746, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(6);
            pnlPrevious.Size = new System.Drawing.Size(187, 46);
            pnlPrevious.TabIndex = 17;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnPrevious.Location = new System.Drawing.Point(6, 6);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(175, 34);
            btnPrevious.TabIndex = 15;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlSaveCategorizationSetup
            // 
            pnlSaveCategorizationSetup.Controls.Add(btnSaveCategorizationSetup);
            pnlSaveCategorizationSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSaveCategorizationSetup.Location = new System.Drawing.Point(559, 0);
            pnlSaveCategorizationSetup.Margin = new System.Windows.Forms.Padding(0);
            pnlSaveCategorizationSetup.Name = "pnlSaveCategorizationSetup";
            pnlSaveCategorizationSetup.Padding = new System.Windows.Forms.Padding(6);
            pnlSaveCategorizationSetup.Size = new System.Drawing.Size(187, 46);
            pnlSaveCategorizationSetup.TabIndex = 18;
            // 
            // btnSaveCategorizationSetup
            // 
            btnSaveCategorizationSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            btnSaveCategorizationSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnSaveCategorizationSetup.Location = new System.Drawing.Point(6, 6);
            btnSaveCategorizationSetup.Margin = new System.Windows.Forms.Padding(0);
            btnSaveCategorizationSetup.Name = "btnSaveCategorizationSetup";
            btnSaveCategorizationSetup.Size = new System.Drawing.Size(175, 34);
            btnSaveCategorizationSetup.TabIndex = 15;
            btnSaveCategorizationSetup.Text = "btnSaveCategorizationSetup";
            btnSaveCategorizationSetup.UseVisualStyleBackColor = true;
            btnSaveCategorizationSetup.Click += BtnSaveCategorizationSetup_Click;
            // 
            // pnlNotice
            // 
            pnlNotice.Controls.Add(lblNotice);
            pnlNotice.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNotice.Location = new System.Drawing.Point(0, 0);
            pnlNotice.Margin = new System.Windows.Forms.Padding(0);
            pnlNotice.Name = "pnlNotice";
            pnlNotice.Size = new System.Drawing.Size(559, 46);
            pnlNotice.TabIndex = 19;
            // 
            // lblNotice
            // 
            lblNotice.AutoEllipsis = true;
            lblNotice.Dock = System.Windows.Forms.DockStyle.Fill;
            lblNotice.Location = new System.Drawing.Point(0, 0);
            lblNotice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblNotice.Name = "lblNotice";
            lblNotice.Size = new System.Drawing.Size(559, 46);
            lblNotice.TabIndex = 0;
            lblNotice.Text = "lblNotice";
            // 
            // pnlWorkSpace
            // 
            pnlWorkSpace.Controls.Add(splMain);
            pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlWorkSpace.Location = new System.Drawing.Point(0, 104);
            pnlWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            pnlWorkSpace.Name = "pnlWorkSpace";
            pnlWorkSpace.Size = new System.Drawing.Size(1120, 473);
            pnlWorkSpace.TabIndex = 9;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            splMain.Location = new System.Drawing.Point(0, 0);
            splMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(splAreaDomainAndSubPopulation);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(grpAttributeCategory);
            splMain.Size = new System.Drawing.Size(1120, 473);
            splMain.SplitterDistance = 814;
            splMain.SplitterWidth = 5;
            splMain.TabIndex = 7;
            // 
            // splAreaDomainAndSubPopulation
            // 
            splAreaDomainAndSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            splAreaDomainAndSubPopulation.Location = new System.Drawing.Point(0, 0);
            splAreaDomainAndSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            splAreaDomainAndSubPopulation.Name = "splAreaDomainAndSubPopulation";
            splAreaDomainAndSubPopulation.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splAreaDomainAndSubPopulation.Panel1
            // 
            splAreaDomainAndSubPopulation.Panel1.Controls.Add(pnlAreaDomain);
            // 
            // splAreaDomainAndSubPopulation.Panel2
            // 
            splAreaDomainAndSubPopulation.Panel2.Controls.Add(pnlSubPopulation);
            splAreaDomainAndSubPopulation.Size = new System.Drawing.Size(814, 473);
            splAreaDomainAndSubPopulation.SplitterDistance = 251;
            splAreaDomainAndSubPopulation.SplitterWidth = 5;
            splAreaDomainAndSubPopulation.TabIndex = 6;
            // 
            // pnlAreaDomain
            // 
            pnlAreaDomain.Controls.Add(splAreaDomain);
            pnlAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAreaDomain.Location = new System.Drawing.Point(0, 0);
            pnlAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            pnlAreaDomain.Name = "pnlAreaDomain";
            pnlAreaDomain.Size = new System.Drawing.Size(814, 251);
            pnlAreaDomain.TabIndex = 0;
            // 
            // splAreaDomain
            // 
            splAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            splAreaDomain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splAreaDomain.Location = new System.Drawing.Point(0, 0);
            splAreaDomain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splAreaDomain.Name = "splAreaDomain";
            // 
            // splAreaDomain.Panel1
            // 
            splAreaDomain.Panel1.Controls.Add(tlpAreaDomain);
            // 
            // splAreaDomain.Panel2
            // 
            splAreaDomain.Panel2.Controls.Add(splAreaDomainSelected);
            splAreaDomain.Size = new System.Drawing.Size(814, 251);
            splAreaDomain.SplitterDistance = 408;
            splAreaDomain.SplitterWidth = 5;
            splAreaDomain.TabIndex = 1;
            // 
            // tlpAreaDomain
            // 
            tlpAreaDomain.ColumnCount = 1;
            tlpAreaDomain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomain.Controls.Add(grpAreaDomain, 0, 0);
            tlpAreaDomain.Controls.Add(grpAreaDomainCategory, 0, 1);
            tlpAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAreaDomain.Location = new System.Drawing.Point(0, 0);
            tlpAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            tlpAreaDomain.Name = "tlpAreaDomain";
            tlpAreaDomain.RowCount = 2;
            tlpAreaDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            tlpAreaDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomain.Size = new System.Drawing.Size(408, 251);
            tlpAreaDomain.TabIndex = 0;
            // 
            // grpAreaDomain
            // 
            grpAreaDomain.Controls.Add(tlpAreaDomainInner);
            grpAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpAreaDomain.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpAreaDomain.Location = new System.Drawing.Point(4, 3);
            grpAreaDomain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpAreaDomain.Name = "grpAreaDomain";
            grpAreaDomain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpAreaDomain.Size = new System.Drawing.Size(400, 121);
            grpAreaDomain.TabIndex = 12;
            grpAreaDomain.TabStop = false;
            grpAreaDomain.Text = "grpAreaDomain";
            // 
            // tlpAreaDomainInner
            // 
            tlpAreaDomainInner.ColumnCount = 1;
            tlpAreaDomainInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainInner.Controls.Add(tlpAreaDomainButtons, 0, 0);
            tlpAreaDomainInner.Controls.Add(cboAreaDomain, 0, 1);
            tlpAreaDomainInner.Controls.Add(pnlAreaDomainClassificationType, 0, 2);
            tlpAreaDomainInner.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAreaDomainInner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpAreaDomainInner.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpAreaDomainInner.Location = new System.Drawing.Point(4, 18);
            tlpAreaDomainInner.Margin = new System.Windows.Forms.Padding(0);
            tlpAreaDomainInner.Name = "tlpAreaDomainInner";
            tlpAreaDomainInner.RowCount = 4;
            tlpAreaDomainInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpAreaDomainInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpAreaDomainInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpAreaDomainInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainInner.Size = new System.Drawing.Size(392, 100);
            tlpAreaDomainInner.TabIndex = 15;
            // 
            // tlpAreaDomainButtons
            // 
            tlpAreaDomainButtons.ColumnCount = 2;
            tlpAreaDomainButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpAreaDomainButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpAreaDomainButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpAreaDomainButtons.Controls.Add(tsrAreaDomain, 0, 0);
            tlpAreaDomainButtons.Controls.Add(tsrAreaDomainSelect, 1, 0);
            tlpAreaDomainButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAreaDomainButtons.Location = new System.Drawing.Point(0, 0);
            tlpAreaDomainButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpAreaDomainButtons.Name = "tlpAreaDomainButtons";
            tlpAreaDomainButtons.RowCount = 1;
            tlpAreaDomainButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainButtons.Size = new System.Drawing.Size(392, 29);
            tlpAreaDomainButtons.TabIndex = 17;
            // 
            // tsrAreaDomain
            // 
            tsrAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrAreaDomain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnAreaDomainEdit, btnAreaDomainAdd, btnAreaDomainDelete, btnAreaDomainExport, btnAreaDomainImport });
            tsrAreaDomain.Location = new System.Drawing.Point(0, 0);
            tsrAreaDomain.Name = "tsrAreaDomain";
            tsrAreaDomain.Size = new System.Drawing.Size(196, 29);
            tsrAreaDomain.TabIndex = 15;
            tsrAreaDomain.Text = "tsrAreaDomain";
            // 
            // btnAreaDomainEdit
            // 
            btnAreaDomainEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainEdit.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainEdit.Image");
            btnAreaDomainEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainEdit.Margin = new System.Windows.Forms.Padding(0);
            btnAreaDomainEdit.Name = "btnAreaDomainEdit";
            btnAreaDomainEdit.Size = new System.Drawing.Size(23, 29);
            btnAreaDomainEdit.Text = "btnAreaDomainEdit";
            btnAreaDomainEdit.Click += BtnAreaDomainEdit_Click;
            // 
            // btnAreaDomainAdd
            // 
            btnAreaDomainAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainAdd.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainAdd.Image");
            btnAreaDomainAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainAdd.Margin = new System.Windows.Forms.Padding(0);
            btnAreaDomainAdd.Name = "btnAreaDomainAdd";
            btnAreaDomainAdd.Size = new System.Drawing.Size(23, 29);
            btnAreaDomainAdd.Text = "btnAreaDomainAdd";
            btnAreaDomainAdd.Click += BtnAreaDomainAdd_Click;
            // 
            // btnAreaDomainDelete
            // 
            btnAreaDomainDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainDelete.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainDelete.Image");
            btnAreaDomainDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainDelete.Name = "btnAreaDomainDelete";
            btnAreaDomainDelete.Size = new System.Drawing.Size(23, 26);
            btnAreaDomainDelete.Text = "btnAreaDomainDelete";
            btnAreaDomainDelete.Click += BtnAreaDomainDelete_Click;
            // 
            // btnAreaDomainExport
            // 
            btnAreaDomainExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainExport.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainExport.Image");
            btnAreaDomainExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainExport.Name = "btnAreaDomainExport";
            btnAreaDomainExport.Size = new System.Drawing.Size(23, 26);
            btnAreaDomainExport.Text = "btnAreaDomainExport";
            btnAreaDomainExport.Click += BtnAreaDomainExport_Click;
            // 
            // btnAreaDomainImport
            // 
            btnAreaDomainImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainImport.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainImport.Image");
            btnAreaDomainImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainImport.Name = "btnAreaDomainImport";
            btnAreaDomainImport.Size = new System.Drawing.Size(23, 26);
            btnAreaDomainImport.Text = "btnAreaDomainImport";
            btnAreaDomainImport.Click += BtnAreaDomainImport_Click;
            // 
            // tsrAreaDomainSelect
            // 
            tsrAreaDomainSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrAreaDomainSelect.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnAreaDomainSelect });
            tsrAreaDomainSelect.Location = new System.Drawing.Point(196, 0);
            tsrAreaDomainSelect.Name = "tsrAreaDomainSelect";
            tsrAreaDomainSelect.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            tsrAreaDomainSelect.Size = new System.Drawing.Size(196, 29);
            tsrAreaDomainSelect.TabIndex = 17;
            tsrAreaDomainSelect.Text = "tsrAreaDomainSelect";
            // 
            // btnAreaDomainSelect
            // 
            btnAreaDomainSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainSelect.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainSelect.Image");
            btnAreaDomainSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainSelect.Margin = new System.Windows.Forms.Padding(0);
            btnAreaDomainSelect.Name = "btnAreaDomainSelect";
            btnAreaDomainSelect.Size = new System.Drawing.Size(23, 29);
            btnAreaDomainSelect.Text = "btnAreaDomainSelect";
            btnAreaDomainSelect.Click += BtnAreaDomainSelect_Click;
            // 
            // cboAreaDomain
            // 
            cboAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            cboAreaDomain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            cboAreaDomain.FormattingEnabled = true;
            cboAreaDomain.Location = new System.Drawing.Point(0, 29);
            cboAreaDomain.Margin = new System.Windows.Forms.Padding(0);
            cboAreaDomain.Name = "cboAreaDomain";
            cboAreaDomain.Size = new System.Drawing.Size(392, 23);
            cboAreaDomain.TabIndex = 16;
            // 
            // pnlAreaDomainClassificationType
            // 
            pnlAreaDomainClassificationType.Controls.Add(rdoAreaDomainChangeOrDynamic);
            pnlAreaDomainClassificationType.Controls.Add(rdoAreaDomainStandard);
            pnlAreaDomainClassificationType.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAreaDomainClassificationType.Location = new System.Drawing.Point(0, 64);
            pnlAreaDomainClassificationType.Margin = new System.Windows.Forms.Padding(0);
            pnlAreaDomainClassificationType.Name = "pnlAreaDomainClassificationType";
            pnlAreaDomainClassificationType.Size = new System.Drawing.Size(392, 29);
            pnlAreaDomainClassificationType.TabIndex = 18;
            // 
            // rdoAreaDomainChangeOrDynamic
            // 
            rdoAreaDomainChangeOrDynamic.AutoSize = true;
            rdoAreaDomainChangeOrDynamic.Dock = System.Windows.Forms.DockStyle.Left;
            rdoAreaDomainChangeOrDynamic.Location = new System.Drawing.Point(141, 0);
            rdoAreaDomainChangeOrDynamic.Margin = new System.Windows.Forms.Padding(0);
            rdoAreaDomainChangeOrDynamic.Name = "rdoAreaDomainChangeOrDynamic";
            rdoAreaDomainChangeOrDynamic.Size = new System.Drawing.Size(187, 29);
            rdoAreaDomainChangeOrDynamic.TabIndex = 1;
            rdoAreaDomainChangeOrDynamic.Text = "rdoAreaDomainChangeOrDynamic";
            rdoAreaDomainChangeOrDynamic.UseVisualStyleBackColor = true;
            // 
            // rdoAreaDomainStandard
            // 
            rdoAreaDomainStandard.AutoSize = true;
            rdoAreaDomainStandard.Checked = true;
            rdoAreaDomainStandard.Dock = System.Windows.Forms.DockStyle.Left;
            rdoAreaDomainStandard.Location = new System.Drawing.Point(0, 0);
            rdoAreaDomainStandard.Margin = new System.Windows.Forms.Padding(0);
            rdoAreaDomainStandard.Name = "rdoAreaDomainStandard";
            rdoAreaDomainStandard.Size = new System.Drawing.Size(141, 29);
            rdoAreaDomainStandard.TabIndex = 0;
            rdoAreaDomainStandard.TabStop = true;
            rdoAreaDomainStandard.Text = "rdoAreaDomainStandard";
            rdoAreaDomainStandard.UseVisualStyleBackColor = true;
            // 
            // grpAreaDomainCategory
            // 
            grpAreaDomainCategory.Controls.Add(tlpAreaDomainCategoryInner);
            grpAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAreaDomainCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpAreaDomainCategory.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpAreaDomainCategory.Location = new System.Drawing.Point(0, 127);
            grpAreaDomainCategory.Margin = new System.Windows.Forms.Padding(0);
            grpAreaDomainCategory.Name = "grpAreaDomainCategory";
            grpAreaDomainCategory.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpAreaDomainCategory.Size = new System.Drawing.Size(408, 124);
            grpAreaDomainCategory.TabIndex = 13;
            grpAreaDomainCategory.TabStop = false;
            grpAreaDomainCategory.Text = "grpAreaDomainCategory";
            // 
            // tlpAreaDomainCategoryInner
            // 
            tlpAreaDomainCategoryInner.ColumnCount = 1;
            tlpAreaDomainCategoryInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainCategoryInner.Controls.Add(lstAreaDomainCategory, 0, 1);
            tlpAreaDomainCategoryInner.Controls.Add(tlpAreaDomainCategoryButtons, 0, 0);
            tlpAreaDomainCategoryInner.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAreaDomainCategoryInner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpAreaDomainCategoryInner.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpAreaDomainCategoryInner.Location = new System.Drawing.Point(4, 18);
            tlpAreaDomainCategoryInner.Margin = new System.Windows.Forms.Padding(0);
            tlpAreaDomainCategoryInner.Name = "tlpAreaDomainCategoryInner";
            tlpAreaDomainCategoryInner.RowCount = 2;
            tlpAreaDomainCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpAreaDomainCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpAreaDomainCategoryInner.Size = new System.Drawing.Size(400, 103);
            tlpAreaDomainCategoryInner.TabIndex = 17;
            // 
            // lstAreaDomainCategory
            // 
            lstAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            lstAreaDomainCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lstAreaDomainCategory.FormattingEnabled = true;
            lstAreaDomainCategory.ItemHeight = 15;
            lstAreaDomainCategory.Location = new System.Drawing.Point(0, 29);
            lstAreaDomainCategory.Margin = new System.Windows.Forms.Padding(0);
            lstAreaDomainCategory.Name = "lstAreaDomainCategory";
            lstAreaDomainCategory.SelectionMode = System.Windows.Forms.SelectionMode.None;
            lstAreaDomainCategory.Size = new System.Drawing.Size(400, 74);
            lstAreaDomainCategory.TabIndex = 18;
            // 
            // tlpAreaDomainCategoryButtons
            // 
            tlpAreaDomainCategoryButtons.ColumnCount = 1;
            tlpAreaDomainCategoryButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainCategoryButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpAreaDomainCategoryButtons.Controls.Add(tsrAreaDomainCategory, 0, 0);
            tlpAreaDomainCategoryButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAreaDomainCategoryButtons.Location = new System.Drawing.Point(0, 0);
            tlpAreaDomainCategoryButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpAreaDomainCategoryButtons.Name = "tlpAreaDomainCategoryButtons";
            tlpAreaDomainCategoryButtons.RowCount = 1;
            tlpAreaDomainCategoryButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainCategoryButtons.Size = new System.Drawing.Size(400, 29);
            tlpAreaDomainCategoryButtons.TabIndex = 17;
            // 
            // tsrAreaDomainCategory
            // 
            tsrAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrAreaDomainCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnAreaDomainCategoryEdit });
            tsrAreaDomainCategory.Location = new System.Drawing.Point(0, 0);
            tsrAreaDomainCategory.Name = "tsrAreaDomainCategory";
            tsrAreaDomainCategory.Size = new System.Drawing.Size(400, 29);
            tsrAreaDomainCategory.TabIndex = 15;
            tsrAreaDomainCategory.Text = "tsrAreaDomainCategory";
            // 
            // btnAreaDomainCategoryEdit
            // 
            btnAreaDomainCategoryEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainCategoryEdit.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainCategoryEdit.Image");
            btnAreaDomainCategoryEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainCategoryEdit.Margin = new System.Windows.Forms.Padding(0);
            btnAreaDomainCategoryEdit.Name = "btnAreaDomainCategoryEdit";
            btnAreaDomainCategoryEdit.Size = new System.Drawing.Size(23, 29);
            btnAreaDomainCategoryEdit.Text = "btnAreaDomainCategoryEdit";
            btnAreaDomainCategoryEdit.Click += BtnAreaDomainCategoryEdit_Click;
            // 
            // splAreaDomainSelected
            // 
            splAreaDomainSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            splAreaDomainSelected.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splAreaDomainSelected.Location = new System.Drawing.Point(0, 0);
            splAreaDomainSelected.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splAreaDomainSelected.Name = "splAreaDomainSelected";
            splAreaDomainSelected.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splAreaDomainSelected.Panel1
            // 
            splAreaDomainSelected.Panel1.Controls.Add(grpAreaDomainSelected);
            // 
            // splAreaDomainSelected.Panel2
            // 
            splAreaDomainSelected.Panel2.Controls.Add(grpAreaDomainCategoryCombination);
            splAreaDomainSelected.Size = new System.Drawing.Size(401, 251);
            splAreaDomainSelected.SplitterDistance = 115;
            splAreaDomainSelected.SplitterWidth = 5;
            splAreaDomainSelected.TabIndex = 0;
            // 
            // grpAreaDomainSelected
            // 
            grpAreaDomainSelected.Controls.Add(tlpAreaDomainSelectedInner);
            grpAreaDomainSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAreaDomainSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpAreaDomainSelected.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpAreaDomainSelected.Location = new System.Drawing.Point(0, 0);
            grpAreaDomainSelected.Margin = new System.Windows.Forms.Padding(0);
            grpAreaDomainSelected.Name = "grpAreaDomainSelected";
            grpAreaDomainSelected.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpAreaDomainSelected.Size = new System.Drawing.Size(401, 115);
            grpAreaDomainSelected.TabIndex = 8;
            grpAreaDomainSelected.TabStop = false;
            grpAreaDomainSelected.Text = "grpAreaDomainSelected";
            // 
            // tlpAreaDomainSelectedInner
            // 
            tlpAreaDomainSelectedInner.ColumnCount = 1;
            tlpAreaDomainSelectedInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainSelectedInner.Controls.Add(tsrAreaDomainSelected, 0, 0);
            tlpAreaDomainSelectedInner.Controls.Add(pnlAreaDomainSelected, 0, 1);
            tlpAreaDomainSelectedInner.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAreaDomainSelectedInner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpAreaDomainSelectedInner.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpAreaDomainSelectedInner.Location = new System.Drawing.Point(4, 18);
            tlpAreaDomainSelectedInner.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpAreaDomainSelectedInner.Name = "tlpAreaDomainSelectedInner";
            tlpAreaDomainSelectedInner.RowCount = 2;
            tlpAreaDomainSelectedInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpAreaDomainSelectedInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAreaDomainSelectedInner.Size = new System.Drawing.Size(393, 94);
            tlpAreaDomainSelectedInner.TabIndex = 0;
            // 
            // tsrAreaDomainSelected
            // 
            tsrAreaDomainSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrAreaDomainSelected.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnAreaDomainCancel, btnADCHierarchy });
            tsrAreaDomainSelected.Location = new System.Drawing.Point(0, 0);
            tsrAreaDomainSelected.Name = "tsrAreaDomainSelected";
            tsrAreaDomainSelected.RightToLeft = System.Windows.Forms.RightToLeft.No;
            tsrAreaDomainSelected.Size = new System.Drawing.Size(393, 35);
            tsrAreaDomainSelected.TabIndex = 17;
            tsrAreaDomainSelected.Text = "tsrAreaDomainSelected";
            // 
            // btnAreaDomainCancel
            // 
            btnAreaDomainCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAreaDomainCancel.Image = (System.Drawing.Image)resources.GetObject("btnAreaDomainCancel.Image");
            btnAreaDomainCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAreaDomainCancel.Margin = new System.Windows.Forms.Padding(0);
            btnAreaDomainCancel.Name = "btnAreaDomainCancel";
            btnAreaDomainCancel.Size = new System.Drawing.Size(23, 35);
            btnAreaDomainCancel.Text = "btnAreaDomainCancel";
            btnAreaDomainCancel.Click += BtnAreaDomainCancelSelect_Click;
            // 
            // btnADCHierarchy
            // 
            btnADCHierarchy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnADCHierarchy.Image = (System.Drawing.Image)resources.GetObject("btnADCHierarchy.Image");
            btnADCHierarchy.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnADCHierarchy.Name = "btnADCHierarchy";
            btnADCHierarchy.Size = new System.Drawing.Size(23, 32);
            btnADCHierarchy.Text = "btnADCHierarchy";
            btnADCHierarchy.Click += BtnAdcHierarchy_Click;
            // 
            // pnlAreaDomainSelected
            // 
            pnlAreaDomainSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAreaDomainSelected.Location = new System.Drawing.Point(0, 35);
            pnlAreaDomainSelected.Margin = new System.Windows.Forms.Padding(0);
            pnlAreaDomainSelected.Name = "pnlAreaDomainSelected";
            pnlAreaDomainSelected.Size = new System.Drawing.Size(393, 59);
            pnlAreaDomainSelected.TabIndex = 18;
            // 
            // grpAreaDomainCategoryCombination
            // 
            grpAreaDomainCategoryCombination.Controls.Add(pnlAreaDomainCategoryCombination);
            grpAreaDomainCategoryCombination.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAreaDomainCategoryCombination.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpAreaDomainCategoryCombination.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpAreaDomainCategoryCombination.Location = new System.Drawing.Point(0, 0);
            grpAreaDomainCategoryCombination.Margin = new System.Windows.Forms.Padding(0);
            grpAreaDomainCategoryCombination.Name = "grpAreaDomainCategoryCombination";
            grpAreaDomainCategoryCombination.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpAreaDomainCategoryCombination.Size = new System.Drawing.Size(401, 131);
            grpAreaDomainCategoryCombination.TabIndex = 15;
            grpAreaDomainCategoryCombination.TabStop = false;
            grpAreaDomainCategoryCombination.Text = "grpAreaDomainCategoryCombination";
            // 
            // pnlAreaDomainCategoryCombination
            // 
            pnlAreaDomainCategoryCombination.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAreaDomainCategoryCombination.Location = new System.Drawing.Point(4, 18);
            pnlAreaDomainCategoryCombination.Margin = new System.Windows.Forms.Padding(0);
            pnlAreaDomainCategoryCombination.Name = "pnlAreaDomainCategoryCombination";
            pnlAreaDomainCategoryCombination.Size = new System.Drawing.Size(393, 110);
            pnlAreaDomainCategoryCombination.TabIndex = 19;
            // 
            // pnlSubPopulation
            // 
            pnlSubPopulation.Controls.Add(splSubPopulation);
            pnlSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSubPopulation.Location = new System.Drawing.Point(0, 0);
            pnlSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            pnlSubPopulation.Name = "pnlSubPopulation";
            pnlSubPopulation.Size = new System.Drawing.Size(814, 217);
            pnlSubPopulation.TabIndex = 1;
            // 
            // splSubPopulation
            // 
            splSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            splSubPopulation.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splSubPopulation.Location = new System.Drawing.Point(0, 0);
            splSubPopulation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splSubPopulation.Name = "splSubPopulation";
            // 
            // splSubPopulation.Panel1
            // 
            splSubPopulation.Panel1.Controls.Add(tlpSubPopulation);
            // 
            // splSubPopulation.Panel2
            // 
            splSubPopulation.Panel2.Controls.Add(splSubPopulationSelected);
            splSubPopulation.Size = new System.Drawing.Size(814, 217);
            splSubPopulation.SplitterDistance = 408;
            splSubPopulation.SplitterWidth = 5;
            splSubPopulation.TabIndex = 1;
            // 
            // tlpSubPopulation
            // 
            tlpSubPopulation.ColumnCount = 1;
            tlpSubPopulation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulation.Controls.Add(grpSubPopulation, 0, 0);
            tlpSubPopulation.Controls.Add(grpSubPopulationCategory, 0, 1);
            tlpSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSubPopulation.Location = new System.Drawing.Point(0, 0);
            tlpSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            tlpSubPopulation.Name = "tlpSubPopulation";
            tlpSubPopulation.RowCount = 2;
            tlpSubPopulation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            tlpSubPopulation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulation.Size = new System.Drawing.Size(408, 217);
            tlpSubPopulation.TabIndex = 0;
            // 
            // grpSubPopulation
            // 
            grpSubPopulation.Controls.Add(tlpSubPopulationInner);
            grpSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpSubPopulation.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSubPopulation.Location = new System.Drawing.Point(4, 3);
            grpSubPopulation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpSubPopulation.Name = "grpSubPopulation";
            grpSubPopulation.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpSubPopulation.Size = new System.Drawing.Size(400, 121);
            grpSubPopulation.TabIndex = 12;
            grpSubPopulation.TabStop = false;
            grpSubPopulation.Text = "grpSubPopulation";
            // 
            // tlpSubPopulationInner
            // 
            tlpSubPopulationInner.ColumnCount = 1;
            tlpSubPopulationInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationInner.Controls.Add(cboSubPopulation, 0, 1);
            tlpSubPopulationInner.Controls.Add(tlpSubPopulationButtons, 0, 0);
            tlpSubPopulationInner.Controls.Add(pnlSubPopulationClassificationType, 0, 2);
            tlpSubPopulationInner.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSubPopulationInner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpSubPopulationInner.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpSubPopulationInner.Location = new System.Drawing.Point(4, 18);
            tlpSubPopulationInner.Margin = new System.Windows.Forms.Padding(0);
            tlpSubPopulationInner.Name = "tlpSubPopulationInner";
            tlpSubPopulationInner.RowCount = 4;
            tlpSubPopulationInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSubPopulationInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpSubPopulationInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSubPopulationInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationInner.Size = new System.Drawing.Size(392, 100);
            tlpSubPopulationInner.TabIndex = 15;
            // 
            // cboSubPopulation
            // 
            cboSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            cboSubPopulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            cboSubPopulation.FormattingEnabled = true;
            cboSubPopulation.Location = new System.Drawing.Point(0, 29);
            cboSubPopulation.Margin = new System.Windows.Forms.Padding(0);
            cboSubPopulation.Name = "cboSubPopulation";
            cboSubPopulation.Size = new System.Drawing.Size(392, 23);
            cboSubPopulation.TabIndex = 16;
            // 
            // tlpSubPopulationButtons
            // 
            tlpSubPopulationButtons.ColumnCount = 2;
            tlpSubPopulationButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpSubPopulationButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpSubPopulationButtons.Controls.Add(tsrSubPopulationSelect, 0, 0);
            tlpSubPopulationButtons.Controls.Add(tsrSubPopulation, 0, 0);
            tlpSubPopulationButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSubPopulationButtons.Location = new System.Drawing.Point(0, 0);
            tlpSubPopulationButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpSubPopulationButtons.Name = "tlpSubPopulationButtons";
            tlpSubPopulationButtons.RowCount = 1;
            tlpSubPopulationButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpSubPopulationButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpSubPopulationButtons.Size = new System.Drawing.Size(392, 29);
            tlpSubPopulationButtons.TabIndex = 17;
            // 
            // tsrSubPopulationSelect
            // 
            tsrSubPopulationSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrSubPopulationSelect.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnSubPopulationSelect });
            tsrSubPopulationSelect.Location = new System.Drawing.Point(196, 0);
            tsrSubPopulationSelect.Name = "tsrSubPopulationSelect";
            tsrSubPopulationSelect.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            tsrSubPopulationSelect.Size = new System.Drawing.Size(196, 29);
            tsrSubPopulationSelect.TabIndex = 16;
            tsrSubPopulationSelect.Text = "tsrSubPopulationSelect";
            // 
            // btnSubPopulationSelect
            // 
            btnSubPopulationSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationSelect.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationSelect.Image");
            btnSubPopulationSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationSelect.Margin = new System.Windows.Forms.Padding(0);
            btnSubPopulationSelect.Name = "btnSubPopulationSelect";
            btnSubPopulationSelect.Size = new System.Drawing.Size(23, 29);
            btnSubPopulationSelect.Text = "btnSubPopulationSelect";
            btnSubPopulationSelect.Click += BtnSubPopulationSelect_Click;
            // 
            // tsrSubPopulation
            // 
            tsrSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrSubPopulation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnSubPopulationEdit, btnSubPopulationAdd, btnSubPopulationDelete, btnSubPopulationExport, btnSubPopulationImport });
            tsrSubPopulation.Location = new System.Drawing.Point(0, 0);
            tsrSubPopulation.Name = "tsrSubPopulation";
            tsrSubPopulation.Size = new System.Drawing.Size(196, 29);
            tsrSubPopulation.TabIndex = 15;
            tsrSubPopulation.Text = "tsrSubPopulation";
            // 
            // btnSubPopulationEdit
            // 
            btnSubPopulationEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationEdit.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationEdit.Image");
            btnSubPopulationEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationEdit.Margin = new System.Windows.Forms.Padding(0);
            btnSubPopulationEdit.Name = "btnSubPopulationEdit";
            btnSubPopulationEdit.Size = new System.Drawing.Size(23, 29);
            btnSubPopulationEdit.Text = "btnSubPopulationEdit";
            btnSubPopulationEdit.Click += BtnSubPopulationEdit_Click;
            // 
            // btnSubPopulationAdd
            // 
            btnSubPopulationAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationAdd.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationAdd.Image");
            btnSubPopulationAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationAdd.Margin = new System.Windows.Forms.Padding(0);
            btnSubPopulationAdd.Name = "btnSubPopulationAdd";
            btnSubPopulationAdd.Size = new System.Drawing.Size(23, 29);
            btnSubPopulationAdd.Text = "btnSubPopulationAdd";
            btnSubPopulationAdd.Click += BtnSubPopulationAdd_Click;
            // 
            // btnSubPopulationDelete
            // 
            btnSubPopulationDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationDelete.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationDelete.Image");
            btnSubPopulationDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationDelete.Name = "btnSubPopulationDelete";
            btnSubPopulationDelete.Size = new System.Drawing.Size(23, 26);
            btnSubPopulationDelete.Text = "btnSubPopulationDelete";
            btnSubPopulationDelete.Click += BtnSubPopulationDelete_Click;
            // 
            // btnSubPopulationExport
            // 
            btnSubPopulationExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationExport.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationExport.Image");
            btnSubPopulationExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationExport.Name = "btnSubPopulationExport";
            btnSubPopulationExport.Size = new System.Drawing.Size(23, 26);
            btnSubPopulationExport.Text = "btnSubPopulationExport";
            btnSubPopulationExport.Click += BtnSubPopulationExport_Click;
            // 
            // btnSubPopulationImport
            // 
            btnSubPopulationImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationImport.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationImport.Image");
            btnSubPopulationImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationImport.Name = "btnSubPopulationImport";
            btnSubPopulationImport.Size = new System.Drawing.Size(23, 26);
            btnSubPopulationImport.Text = "btnSubPopulationImport";
            btnSubPopulationImport.Click += BtnSubPopulationImport_Click;
            // 
            // pnlSubPopulationClassificationType
            // 
            pnlSubPopulationClassificationType.Controls.Add(rdoSubPopulationChangeOrDynamic);
            pnlSubPopulationClassificationType.Controls.Add(rdoSubPopulationStandard);
            pnlSubPopulationClassificationType.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSubPopulationClassificationType.Location = new System.Drawing.Point(0, 64);
            pnlSubPopulationClassificationType.Margin = new System.Windows.Forms.Padding(0);
            pnlSubPopulationClassificationType.Name = "pnlSubPopulationClassificationType";
            pnlSubPopulationClassificationType.Size = new System.Drawing.Size(392, 29);
            pnlSubPopulationClassificationType.TabIndex = 18;
            // 
            // rdoSubPopulationChangeOrDynamic
            // 
            rdoSubPopulationChangeOrDynamic.AutoSize = true;
            rdoSubPopulationChangeOrDynamic.Dock = System.Windows.Forms.DockStyle.Left;
            rdoSubPopulationChangeOrDynamic.Location = new System.Drawing.Point(152, 0);
            rdoSubPopulationChangeOrDynamic.Margin = new System.Windows.Forms.Padding(0);
            rdoSubPopulationChangeOrDynamic.Name = "rdoSubPopulationChangeOrDynamic";
            rdoSubPopulationChangeOrDynamic.Size = new System.Drawing.Size(198, 29);
            rdoSubPopulationChangeOrDynamic.TabIndex = 2;
            rdoSubPopulationChangeOrDynamic.Text = "rdoSubPopulationChangeOrDynamic";
            rdoSubPopulationChangeOrDynamic.UseVisualStyleBackColor = true;
            // 
            // rdoSubPopulationStandard
            // 
            rdoSubPopulationStandard.AutoSize = true;
            rdoSubPopulationStandard.Checked = true;
            rdoSubPopulationStandard.Dock = System.Windows.Forms.DockStyle.Left;
            rdoSubPopulationStandard.Location = new System.Drawing.Point(0, 0);
            rdoSubPopulationStandard.Margin = new System.Windows.Forms.Padding(0);
            rdoSubPopulationStandard.Name = "rdoSubPopulationStandard";
            rdoSubPopulationStandard.Size = new System.Drawing.Size(152, 29);
            rdoSubPopulationStandard.TabIndex = 1;
            rdoSubPopulationStandard.TabStop = true;
            rdoSubPopulationStandard.Text = "rdoSubPopulationStandard";
            rdoSubPopulationStandard.UseVisualStyleBackColor = true;
            // 
            // grpSubPopulationCategory
            // 
            grpSubPopulationCategory.Controls.Add(tlpSubPopulationCategoryInner);
            grpSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSubPopulationCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpSubPopulationCategory.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSubPopulationCategory.Location = new System.Drawing.Point(0, 127);
            grpSubPopulationCategory.Margin = new System.Windows.Forms.Padding(0);
            grpSubPopulationCategory.Name = "grpSubPopulationCategory";
            grpSubPopulationCategory.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpSubPopulationCategory.Size = new System.Drawing.Size(408, 90);
            grpSubPopulationCategory.TabIndex = 13;
            grpSubPopulationCategory.TabStop = false;
            grpSubPopulationCategory.Text = "grpSubPopulationCategory";
            // 
            // tlpSubPopulationCategoryInner
            // 
            tlpSubPopulationCategoryInner.ColumnCount = 1;
            tlpSubPopulationCategoryInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationCategoryInner.Controls.Add(lstSubPopulationCategory, 0, 1);
            tlpSubPopulationCategoryInner.Controls.Add(tlpSubPopulationCategoryButtons, 0, 0);
            tlpSubPopulationCategoryInner.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSubPopulationCategoryInner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpSubPopulationCategoryInner.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpSubPopulationCategoryInner.Location = new System.Drawing.Point(4, 18);
            tlpSubPopulationCategoryInner.Margin = new System.Windows.Forms.Padding(0);
            tlpSubPopulationCategoryInner.Name = "tlpSubPopulationCategoryInner";
            tlpSubPopulationCategoryInner.RowCount = 2;
            tlpSubPopulationCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpSubPopulationCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpSubPopulationCategoryInner.Size = new System.Drawing.Size(400, 69);
            tlpSubPopulationCategoryInner.TabIndex = 16;
            // 
            // lstSubPopulationCategory
            // 
            lstSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            lstSubPopulationCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lstSubPopulationCategory.FormattingEnabled = true;
            lstSubPopulationCategory.ItemHeight = 15;
            lstSubPopulationCategory.Location = new System.Drawing.Point(0, 29);
            lstSubPopulationCategory.Margin = new System.Windows.Forms.Padding(0);
            lstSubPopulationCategory.Name = "lstSubPopulationCategory";
            lstSubPopulationCategory.SelectionMode = System.Windows.Forms.SelectionMode.None;
            lstSubPopulationCategory.Size = new System.Drawing.Size(400, 40);
            lstSubPopulationCategory.TabIndex = 18;
            // 
            // tlpSubPopulationCategoryButtons
            // 
            tlpSubPopulationCategoryButtons.ColumnCount = 1;
            tlpSubPopulationCategoryButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationCategoryButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpSubPopulationCategoryButtons.Controls.Add(tsrSubPopulationCategory, 0, 0);
            tlpSubPopulationCategoryButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSubPopulationCategoryButtons.Location = new System.Drawing.Point(0, 0);
            tlpSubPopulationCategoryButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpSubPopulationCategoryButtons.Name = "tlpSubPopulationCategoryButtons";
            tlpSubPopulationCategoryButtons.RowCount = 1;
            tlpSubPopulationCategoryButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationCategoryButtons.Size = new System.Drawing.Size(400, 29);
            tlpSubPopulationCategoryButtons.TabIndex = 17;
            // 
            // tsrSubPopulationCategory
            // 
            tsrSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrSubPopulationCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnSubPopulationCategoryEdit });
            tsrSubPopulationCategory.Location = new System.Drawing.Point(0, 0);
            tsrSubPopulationCategory.Name = "tsrSubPopulationCategory";
            tsrSubPopulationCategory.Size = new System.Drawing.Size(400, 29);
            tsrSubPopulationCategory.TabIndex = 15;
            tsrSubPopulationCategory.Text = "tsrSubPopulationCategory";
            // 
            // btnSubPopulationCategoryEdit
            // 
            btnSubPopulationCategoryEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationCategoryEdit.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationCategoryEdit.Image");
            btnSubPopulationCategoryEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationCategoryEdit.Margin = new System.Windows.Forms.Padding(0);
            btnSubPopulationCategoryEdit.Name = "btnSubPopulationCategoryEdit";
            btnSubPopulationCategoryEdit.Size = new System.Drawing.Size(23, 29);
            btnSubPopulationCategoryEdit.Text = "btnSubPopulationCategoryEdit";
            btnSubPopulationCategoryEdit.Click += BtnSubPopulationCategoryEdit_Click;
            // 
            // splSubPopulationSelected
            // 
            splSubPopulationSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            splSubPopulationSelected.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splSubPopulationSelected.Location = new System.Drawing.Point(0, 0);
            splSubPopulationSelected.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splSubPopulationSelected.Name = "splSubPopulationSelected";
            splSubPopulationSelected.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splSubPopulationSelected.Panel1
            // 
            splSubPopulationSelected.Panel1.Controls.Add(grpSubPopulationSelected);
            // 
            // splSubPopulationSelected.Panel2
            // 
            splSubPopulationSelected.Panel2.Controls.Add(grpSubPopulationCategoryCombination);
            splSubPopulationSelected.Size = new System.Drawing.Size(401, 217);
            splSubPopulationSelected.SplitterDistance = 115;
            splSubPopulationSelected.SplitterWidth = 5;
            splSubPopulationSelected.TabIndex = 0;
            // 
            // grpSubPopulationSelected
            // 
            grpSubPopulationSelected.Controls.Add(tlpSubPopulationSelectedInner);
            grpSubPopulationSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSubPopulationSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpSubPopulationSelected.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSubPopulationSelected.Location = new System.Drawing.Point(0, 0);
            grpSubPopulationSelected.Margin = new System.Windows.Forms.Padding(0);
            grpSubPopulationSelected.Name = "grpSubPopulationSelected";
            grpSubPopulationSelected.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpSubPopulationSelected.Size = new System.Drawing.Size(401, 115);
            grpSubPopulationSelected.TabIndex = 8;
            grpSubPopulationSelected.TabStop = false;
            grpSubPopulationSelected.Text = "grpSubPopulationSelected";
            // 
            // tlpSubPopulationSelectedInner
            // 
            tlpSubPopulationSelectedInner.ColumnCount = 1;
            tlpSubPopulationSelectedInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationSelectedInner.Controls.Add(pnlSubPopulationSelected, 0, 1);
            tlpSubPopulationSelectedInner.Controls.Add(tsrSubPopulationSelected, 0, 0);
            tlpSubPopulationSelectedInner.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSubPopulationSelectedInner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpSubPopulationSelectedInner.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpSubPopulationSelectedInner.Location = new System.Drawing.Point(4, 18);
            tlpSubPopulationSelectedInner.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpSubPopulationSelectedInner.Name = "tlpSubPopulationSelectedInner";
            tlpSubPopulationSelectedInner.RowCount = 2;
            tlpSubPopulationSelectedInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpSubPopulationSelectedInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSubPopulationSelectedInner.Size = new System.Drawing.Size(393, 94);
            tlpSubPopulationSelectedInner.TabIndex = 0;
            // 
            // pnlSubPopulationSelected
            // 
            pnlSubPopulationSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSubPopulationSelected.Location = new System.Drawing.Point(0, 35);
            pnlSubPopulationSelected.Margin = new System.Windows.Forms.Padding(0);
            pnlSubPopulationSelected.Name = "pnlSubPopulationSelected";
            pnlSubPopulationSelected.Size = new System.Drawing.Size(393, 59);
            pnlSubPopulationSelected.TabIndex = 19;
            // 
            // tsrSubPopulationSelected
            // 
            tsrSubPopulationSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrSubPopulationSelected.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnSubPopulationCancel, btnSPCHierarchy });
            tsrSubPopulationSelected.Location = new System.Drawing.Point(0, 0);
            tsrSubPopulationSelected.Name = "tsrSubPopulationSelected";
            tsrSubPopulationSelected.RightToLeft = System.Windows.Forms.RightToLeft.No;
            tsrSubPopulationSelected.Size = new System.Drawing.Size(393, 35);
            tsrSubPopulationSelected.TabIndex = 17;
            tsrSubPopulationSelected.Text = "tsrSubPopulationSelected";
            // 
            // btnSubPopulationCancel
            // 
            btnSubPopulationCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSubPopulationCancel.Image = (System.Drawing.Image)resources.GetObject("btnSubPopulationCancel.Image");
            btnSubPopulationCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSubPopulationCancel.Margin = new System.Windows.Forms.Padding(0);
            btnSubPopulationCancel.Name = "btnSubPopulationCancel";
            btnSubPopulationCancel.Size = new System.Drawing.Size(23, 35);
            btnSubPopulationCancel.Text = "btnSubPopulationCancel";
            btnSubPopulationCancel.Click += BtnSubPopulationCancelSelect_Click;
            // 
            // btnSPCHierarchy
            // 
            btnSPCHierarchy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnSPCHierarchy.Image = (System.Drawing.Image)resources.GetObject("btnSPCHierarchy.Image");
            btnSPCHierarchy.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSPCHierarchy.Name = "btnSPCHierarchy";
            btnSPCHierarchy.Size = new System.Drawing.Size(23, 32);
            btnSPCHierarchy.Text = "btnSPCHierarchy";
            btnSPCHierarchy.Click += BtnSpcHierarchy_Click;
            // 
            // grpSubPopulationCategoryCombination
            // 
            grpSubPopulationCategoryCombination.Controls.Add(pnlSubPopulationCategoryCombination);
            grpSubPopulationCategoryCombination.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSubPopulationCategoryCombination.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpSubPopulationCategoryCombination.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSubPopulationCategoryCombination.Location = new System.Drawing.Point(0, 0);
            grpSubPopulationCategoryCombination.Margin = new System.Windows.Forms.Padding(0);
            grpSubPopulationCategoryCombination.Name = "grpSubPopulationCategoryCombination";
            grpSubPopulationCategoryCombination.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpSubPopulationCategoryCombination.Size = new System.Drawing.Size(401, 97);
            grpSubPopulationCategoryCombination.TabIndex = 15;
            grpSubPopulationCategoryCombination.TabStop = false;
            grpSubPopulationCategoryCombination.Text = "grpSubPopulationCategoryCombination";
            // 
            // pnlSubPopulationCategoryCombination
            // 
            pnlSubPopulationCategoryCombination.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSubPopulationCategoryCombination.Location = new System.Drawing.Point(4, 18);
            pnlSubPopulationCategoryCombination.Margin = new System.Windows.Forms.Padding(0);
            pnlSubPopulationCategoryCombination.Name = "pnlSubPopulationCategoryCombination";
            pnlSubPopulationCategoryCombination.Size = new System.Drawing.Size(393, 76);
            pnlSubPopulationCategoryCombination.TabIndex = 20;
            // 
            // grpAttributeCategory
            // 
            grpAttributeCategory.Controls.Add(tlpAttributeCategoryInner);
            grpAttributeCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAttributeCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpAttributeCategory.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpAttributeCategory.Location = new System.Drawing.Point(0, 0);
            grpAttributeCategory.Margin = new System.Windows.Forms.Padding(0);
            grpAttributeCategory.Name = "grpAttributeCategory";
            grpAttributeCategory.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpAttributeCategory.Size = new System.Drawing.Size(301, 473);
            grpAttributeCategory.TabIndex = 9;
            grpAttributeCategory.TabStop = false;
            grpAttributeCategory.Text = "grpAttributeCategory";
            // 
            // tlpAttributeCategoryInner
            // 
            tlpAttributeCategoryInner.ColumnCount = 1;
            tlpAttributeCategoryInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAttributeCategoryInner.Controls.Add(pnlAttributeCategory, 0, 1);
            tlpAttributeCategoryInner.Controls.Add(tsrAttributeCategorySelected, 0, 0);
            tlpAttributeCategoryInner.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAttributeCategoryInner.Location = new System.Drawing.Point(4, 18);
            tlpAttributeCategoryInner.Margin = new System.Windows.Forms.Padding(0);
            tlpAttributeCategoryInner.Name = "tlpAttributeCategoryInner";
            tlpAttributeCategoryInner.RowCount = 2;
            tlpAttributeCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpAttributeCategoryInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAttributeCategoryInner.Size = new System.Drawing.Size(293, 452);
            tlpAttributeCategoryInner.TabIndex = 0;
            // 
            // pnlAttributeCategory
            // 
            pnlAttributeCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAttributeCategory.Location = new System.Drawing.Point(0, 29);
            pnlAttributeCategory.Margin = new System.Windows.Forms.Padding(0);
            pnlAttributeCategory.Name = "pnlAttributeCategory";
            pnlAttributeCategory.Size = new System.Drawing.Size(293, 423);
            pnlAttributeCategory.TabIndex = 21;
            // 
            // tsrAttributeCategorySelected
            // 
            tsrAttributeCategorySelected.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnLimitTargetVariable });
            tsrAttributeCategorySelected.Location = new System.Drawing.Point(0, 0);
            tsrAttributeCategorySelected.Name = "tsrAttributeCategorySelected";
            tsrAttributeCategorySelected.Size = new System.Drawing.Size(293, 25);
            tsrAttributeCategorySelected.TabIndex = 0;
            tsrAttributeCategorySelected.Text = "toolStrip1";
            // 
            // btnLimitTargetVariable
            // 
            btnLimitTargetVariable.ForeColor = System.Drawing.SystemColors.ControlText;
            btnLimitTargetVariable.Image = (System.Drawing.Image)resources.GetObject("btnLimitTargetVariable.Image");
            btnLimitTargetVariable.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLimitTargetVariable.Name = "btnLimitTargetVariable";
            btnLimitTargetVariable.Size = new System.Drawing.Size(145, 22);
            btnLimitTargetVariable.Text = "btnLimitTargetVariable";
            btnLimitTargetVariable.Click += BtnLimitTargetVariable_Click;
            // 
            // pnlCaption
            // 
            pnlCaption.Controls.Add(splCaption);
            pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCaption.Location = new System.Drawing.Point(0, 0);
            pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            pnlCaption.Name = "pnlCaption";
            pnlCaption.Size = new System.Drawing.Size(1120, 104);
            pnlCaption.TabIndex = 10;
            // 
            // splCaption
            // 
            splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splCaption.IsSplitterFixed = true;
            splCaption.Location = new System.Drawing.Point(0, 0);
            splCaption.Margin = new System.Windows.Forms.Padding(0);
            splCaption.Name = "splCaption";
            splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            splCaption.Panel1.Controls.Add(lblMainCaption);
            // 
            // splCaption.Panel2
            // 
            splCaption.Panel2.Controls.Add(tlpCaption);
            splCaption.Size = new System.Drawing.Size(1120, 104);
            splCaption.SplitterDistance = 35;
            splCaption.SplitterWidth = 5;
            splCaption.TabIndex = 13;
            // 
            // lblMainCaption
            // 
            lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblMainCaption.Location = new System.Drawing.Point(0, 0);
            lblMainCaption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblMainCaption.Name = "lblMainCaption";
            lblMainCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblMainCaption.Size = new System.Drawing.Size(1120, 35);
            lblMainCaption.TabIndex = 0;
            lblMainCaption.Text = "lblMainCaption";
            lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpCaption
            // 
            tlpCaption.ColumnCount = 2;
            tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 327F));
            tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCaption.Controls.Add(lblSelectedLDsityObjectGroupValue, 1, 0);
            tlpCaption.Controls.Add(lblSelectedLDsityObjectGroupCaption, 0, 0);
            tlpCaption.Controls.Add(lblSelectedTargetVariableCaption, 0, 1);
            tlpCaption.Controls.Add(lblSelectedTargetVariableValue, 1, 1);
            tlpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpCaption.Location = new System.Drawing.Point(0, 0);
            tlpCaption.Margin = new System.Windows.Forms.Padding(0);
            tlpCaption.Name = "tlpCaption";
            tlpCaption.RowCount = 3;
            tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCaption.Size = new System.Drawing.Size(1120, 64);
            tlpCaption.TabIndex = 5;
            // 
            // lblSelectedLDsityObjectGroupValue
            // 
            lblSelectedLDsityObjectGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedLDsityObjectGroupValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedLDsityObjectGroupValue.Location = new System.Drawing.Point(331, 0);
            lblSelectedLDsityObjectGroupValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblSelectedLDsityObjectGroupValue.Name = "lblSelectedLDsityObjectGroupValue";
            lblSelectedLDsityObjectGroupValue.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedLDsityObjectGroupValue.Size = new System.Drawing.Size(785, 29);
            lblSelectedLDsityObjectGroupValue.TabIndex = 4;
            lblSelectedLDsityObjectGroupValue.Text = "lblSelectedLDsityObjectGroupValue";
            lblSelectedLDsityObjectGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedLDsityObjectGroupCaption
            // 
            lblSelectedLDsityObjectGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedLDsityObjectGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedLDsityObjectGroupCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblSelectedLDsityObjectGroupCaption.Location = new System.Drawing.Point(0, 0);
            lblSelectedLDsityObjectGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            lblSelectedLDsityObjectGroupCaption.Name = "lblSelectedLDsityObjectGroupCaption";
            lblSelectedLDsityObjectGroupCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedLDsityObjectGroupCaption.Size = new System.Drawing.Size(327, 29);
            lblSelectedLDsityObjectGroupCaption.TabIndex = 0;
            lblSelectedLDsityObjectGroupCaption.Text = "lblSelectedLDsityObjectGroupCaption";
            lblSelectedLDsityObjectGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableCaption
            // 
            lblSelectedTargetVariableCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedTargetVariableCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedTargetVariableCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblSelectedTargetVariableCaption.Location = new System.Drawing.Point(0, 29);
            lblSelectedTargetVariableCaption.Margin = new System.Windows.Forms.Padding(0);
            lblSelectedTargetVariableCaption.Name = "lblSelectedTargetVariableCaption";
            lblSelectedTargetVariableCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedTargetVariableCaption.Size = new System.Drawing.Size(327, 29);
            lblSelectedTargetVariableCaption.TabIndex = 5;
            lblSelectedTargetVariableCaption.Text = "lblSelectedTargetVariableCaption";
            lblSelectedTargetVariableCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableValue
            // 
            lblSelectedTargetVariableValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedTargetVariableValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedTargetVariableValue.Location = new System.Drawing.Point(331, 29);
            lblSelectedTargetVariableValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblSelectedTargetVariableValue.Name = "lblSelectedTargetVariableValue";
            lblSelectedTargetVariableValue.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedTargetVariableValue.Size = new System.Drawing.Size(785, 29);
            lblSelectedTargetVariableValue.TabIndex = 6;
            lblSelectedTargetVariableValue.Text = "lblSelectedTargetVariableValue";
            lblSelectedTargetVariableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlTDSelectionAttributeCategory
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(pnlMain);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlTDSelectionAttributeCategory";
            Size = new System.Drawing.Size(1120, 623);
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            pnlSaveCategorizationSetup.ResumeLayout(false);
            pnlNotice.ResumeLayout(false);
            pnlWorkSpace.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            splAreaDomainAndSubPopulation.Panel1.ResumeLayout(false);
            splAreaDomainAndSubPopulation.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splAreaDomainAndSubPopulation).EndInit();
            splAreaDomainAndSubPopulation.ResumeLayout(false);
            pnlAreaDomain.ResumeLayout(false);
            splAreaDomain.Panel1.ResumeLayout(false);
            splAreaDomain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splAreaDomain).EndInit();
            splAreaDomain.ResumeLayout(false);
            tlpAreaDomain.ResumeLayout(false);
            grpAreaDomain.ResumeLayout(false);
            tlpAreaDomainInner.ResumeLayout(false);
            tlpAreaDomainButtons.ResumeLayout(false);
            tlpAreaDomainButtons.PerformLayout();
            tsrAreaDomain.ResumeLayout(false);
            tsrAreaDomain.PerformLayout();
            tsrAreaDomainSelect.ResumeLayout(false);
            tsrAreaDomainSelect.PerformLayout();
            pnlAreaDomainClassificationType.ResumeLayout(false);
            pnlAreaDomainClassificationType.PerformLayout();
            grpAreaDomainCategory.ResumeLayout(false);
            tlpAreaDomainCategoryInner.ResumeLayout(false);
            tlpAreaDomainCategoryButtons.ResumeLayout(false);
            tlpAreaDomainCategoryButtons.PerformLayout();
            tsrAreaDomainCategory.ResumeLayout(false);
            tsrAreaDomainCategory.PerformLayout();
            splAreaDomainSelected.Panel1.ResumeLayout(false);
            splAreaDomainSelected.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splAreaDomainSelected).EndInit();
            splAreaDomainSelected.ResumeLayout(false);
            grpAreaDomainSelected.ResumeLayout(false);
            tlpAreaDomainSelectedInner.ResumeLayout(false);
            tlpAreaDomainSelectedInner.PerformLayout();
            tsrAreaDomainSelected.ResumeLayout(false);
            tsrAreaDomainSelected.PerformLayout();
            grpAreaDomainCategoryCombination.ResumeLayout(false);
            pnlSubPopulation.ResumeLayout(false);
            splSubPopulation.Panel1.ResumeLayout(false);
            splSubPopulation.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splSubPopulation).EndInit();
            splSubPopulation.ResumeLayout(false);
            tlpSubPopulation.ResumeLayout(false);
            grpSubPopulation.ResumeLayout(false);
            tlpSubPopulationInner.ResumeLayout(false);
            tlpSubPopulationButtons.ResumeLayout(false);
            tlpSubPopulationButtons.PerformLayout();
            tsrSubPopulationSelect.ResumeLayout(false);
            tsrSubPopulationSelect.PerformLayout();
            tsrSubPopulation.ResumeLayout(false);
            tsrSubPopulation.PerformLayout();
            pnlSubPopulationClassificationType.ResumeLayout(false);
            pnlSubPopulationClassificationType.PerformLayout();
            grpSubPopulationCategory.ResumeLayout(false);
            tlpSubPopulationCategoryInner.ResumeLayout(false);
            tlpSubPopulationCategoryButtons.ResumeLayout(false);
            tlpSubPopulationCategoryButtons.PerformLayout();
            tsrSubPopulationCategory.ResumeLayout(false);
            tsrSubPopulationCategory.PerformLayout();
            splSubPopulationSelected.Panel1.ResumeLayout(false);
            splSubPopulationSelected.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splSubPopulationSelected).EndInit();
            splSubPopulationSelected.ResumeLayout(false);
            grpSubPopulationSelected.ResumeLayout(false);
            tlpSubPopulationSelectedInner.ResumeLayout(false);
            tlpSubPopulationSelectedInner.PerformLayout();
            tsrSubPopulationSelected.ResumeLayout(false);
            tsrSubPopulationSelected.PerformLayout();
            grpSubPopulationCategoryCombination.ResumeLayout(false);
            grpAttributeCategory.ResumeLayout(false);
            tlpAttributeCategoryInner.ResumeLayout(false);
            tlpAttributeCategoryInner.PerformLayout();
            tsrAttributeCategorySelected.ResumeLayout(false);
            tsrAttributeCategorySelected.PerformLayout();
            pnlCaption.ResumeLayout(false);
            splCaption.Panel1.ResumeLayout(false);
            splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splCaption).EndInit();
            splCaption.ResumeLayout(false);
            tlpCaption.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splAreaDomainAndSubPopulation;
        private System.Windows.Forms.Panel pnlAreaDomain;
        private System.Windows.Forms.SplitContainer splAreaDomain;
        private System.Windows.Forms.TableLayoutPanel tlpAreaDomain;
        private System.Windows.Forms.GroupBox grpAreaDomain;
        private System.Windows.Forms.TableLayoutPanel tlpAreaDomainInner;
        private System.Windows.Forms.ComboBox cboAreaDomain;
        private System.Windows.Forms.TableLayoutPanel tlpAreaDomainButtons;
        private System.Windows.Forms.ToolStrip tsrAreaDomain;
        private System.Windows.Forms.ToolStripButton btnAreaDomainEdit;
        private System.Windows.Forms.ToolStripButton btnAreaDomainAdd;
        private System.Windows.Forms.ToolStripButton btnAreaDomainDelete;
        private System.Windows.Forms.GroupBox grpAreaDomainCategory;
        private System.Windows.Forms.SplitContainer splAreaDomainSelected;
        private System.Windows.Forms.GroupBox grpAreaDomainSelected;
        private System.Windows.Forms.TableLayoutPanel tlpAreaDomainSelectedInner;
        private System.Windows.Forms.ToolStrip tsrAreaDomainSelected;
        private System.Windows.Forms.ToolStripButton btnAreaDomainCancel;
        private System.Windows.Forms.GroupBox grpAreaDomainCategoryCombination;
        private System.Windows.Forms.Panel pnlSubPopulation;
        private System.Windows.Forms.SplitContainer splSubPopulation;
        private System.Windows.Forms.TableLayoutPanel tlpSubPopulation;
        private System.Windows.Forms.GroupBox grpSubPopulation;
        private System.Windows.Forms.TableLayoutPanel tlpSubPopulationInner;
        private System.Windows.Forms.ComboBox cboSubPopulation;
        private System.Windows.Forms.TableLayoutPanel tlpSubPopulationButtons;
        private System.Windows.Forms.ToolStrip tsrSubPopulationSelect;
        private System.Windows.Forms.ToolStripButton btnSubPopulationSelect;
        private System.Windows.Forms.ToolStrip tsrSubPopulation;
        private System.Windows.Forms.ToolStripButton btnSubPopulationEdit;
        private System.Windows.Forms.ToolStripButton btnSubPopulationAdd;
        private System.Windows.Forms.ToolStripButton btnSubPopulationDelete;
        private System.Windows.Forms.GroupBox grpSubPopulationCategory;
        private System.Windows.Forms.SplitContainer splSubPopulationSelected;
        private System.Windows.Forms.GroupBox grpSubPopulationSelected;
        private System.Windows.Forms.TableLayoutPanel tlpSubPopulationSelectedInner;
        private System.Windows.Forms.ToolStrip tsrSubPopulationSelected;
        private System.Windows.Forms.ToolStripButton btnSubPopulationCancel;
        private System.Windows.Forms.GroupBox grpSubPopulationCategoryCombination;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.GroupBox grpAttributeCategory;
        private System.Windows.Forms.ToolStripButton btnADCHierarchy;
        private System.Windows.Forms.ToolStripButton btnSPCHierarchy;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.Panel pnlAreaDomainSelected;
        private System.Windows.Forms.Panel pnlSubPopulationSelected;
        private System.Windows.Forms.TableLayoutPanel tlpSubPopulationCategoryInner;
        private System.Windows.Forms.TableLayoutPanel tlpSubPopulationCategoryButtons;
        private System.Windows.Forms.ToolStrip tsrSubPopulationCategory;
        private System.Windows.Forms.ToolStripButton btnSubPopulationCategoryEdit;
        private System.Windows.Forms.ListBox lstSubPopulationCategory;
        private System.Windows.Forms.TableLayoutPanel tlpAreaDomainCategoryInner;
        private System.Windows.Forms.ListBox lstAreaDomainCategory;
        private System.Windows.Forms.TableLayoutPanel tlpAreaDomainCategoryButtons;
        private System.Windows.Forms.ToolStrip tsrAreaDomainCategory;
        private System.Windows.Forms.ToolStripButton btnAreaDomainCategoryEdit;
        private System.Windows.Forms.ToolStrip tsrAreaDomainSelect;
        private System.Windows.Forms.ToolStripButton btnAreaDomainSelect;
        private System.Windows.Forms.TableLayoutPanel tlpCaption;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupValue;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableValue;
        private System.Windows.Forms.Panel pnlAreaDomainCategoryCombination;
        private System.Windows.Forms.Panel pnlSubPopulationCategoryCombination;
        private System.Windows.Forms.TableLayoutPanel tlpAttributeCategoryInner;
        private System.Windows.Forms.Panel pnlAttributeCategory;
        private System.Windows.Forms.ToolStrip tsrAttributeCategorySelected;
        private System.Windows.Forms.ToolStripButton btnLimitTargetVariable;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Panel pnlSaveCategorizationSetup;
        private System.Windows.Forms.Button btnSaveCategorizationSetup;
        private System.Windows.Forms.Panel pnlNotice;
        private System.Windows.Forms.Label lblNotice;
        private System.Windows.Forms.ToolStripButton btnAreaDomainExport;
        private System.Windows.Forms.ToolStripButton btnAreaDomainImport;
        private System.Windows.Forms.ToolStripButton btnSubPopulationExport;
        private System.Windows.Forms.ToolStripButton btnSubPopulationImport;
        private System.Windows.Forms.Panel pnlAreaDomainClassificationType;
        private System.Windows.Forms.RadioButton rdoAreaDomainChangeOrDynamic;
        private System.Windows.Forms.RadioButton rdoAreaDomainStandard;
        private System.Windows.Forms.Panel pnlSubPopulationClassificationType;
        private System.Windows.Forms.RadioButton rdoSubPopulationChangeOrDynamic;
        private System.Windows.Forms.RadioButton rdoSubPopulationStandard;
    }

}