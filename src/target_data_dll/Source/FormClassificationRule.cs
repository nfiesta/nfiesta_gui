﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro definici klasifikačních pravidel - třída pro Designer</para>
    /// <para lang="en">Form for classification rules definition - class for Designer</para>
    /// </summary>
    internal partial class FormClassificationRuleDesign
        : Form
    {
        protected FormClassificationRuleDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Button BtnCancel => btnCancel;
        protected System.Windows.Forms.ToolStripButton BtnDependenceDefine => btnDependenceDefine;
        protected System.Windows.Forms.Button BtnOK => btnOK;
        protected System.Windows.Forms.GroupBox GrpCategory => grpCategory;
        protected System.Windows.Forms.GroupBox GrpWorkSpace => grpWorkSpace;
        protected System.Windows.Forms.Label LblCaption => lblCaption;
        protected System.Windows.Forms.Label LblClassificationRuleCaption => lblClassificationRuleCaption;
        protected System.Windows.Forms.Label LblClassificationRuleSwitch => lblClassificationRuleSwitch;
        protected System.Windows.Forms.Label LblDependenceEditor => lblDependenceEditor;
        protected System.Windows.Forms.Panel PnlClassificationRuleCaption => pnlClassificationRuleCaption;
        protected System.Windows.Forms.Panel PnlClassificationRuleSwitch => pnlClassificationRuleSwitch;
        protected System.Windows.Forms.Panel PnlDependenceEditor => pnlDependenceEditor;
        protected System.Windows.Forms.SplitContainer SplClassificationRule => splClassificationRule;
        protected System.Windows.Forms.TableLayoutPanel TlpCategory => tlpCategory;
        protected System.Windows.Forms.ToolStrip TsrCategory => tsrCategory;
        public System.Windows.Forms.TreeView TvwCategory => tvwCategory;
    }

    /// <summary>
    /// <para lang="cs">Formulář pro definici klasifikačních pravidel</para>
    /// <para lang="en">Form for classification rules definition</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class FormClassificationRule<TDomain, TCategory>
           : FormClassificationRuleDesign,
            IArealOrPopulationForm<TDomain, TCategory>,
            INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </summary>
        private TDomain domain;

        /// <summary>
        /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
        /// <para lang="en">Changes in classification rules are not allowed</para>
        /// </summary>
        private bool readOnlyMode;

        /// <summary>
        /// <para lang="cs">Seznam všech uzlů v TreeView kategorií</para>
        /// <para lang="en">List of all nodes in TreeView of categories</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Uzel předcházející vybranému uzlu</para>
        /// <para lang="en">TreeNode previous selected TreeNode</para>
        /// </summary>
        private TreeNode previousNode;

        /// <summary>
        /// <para lang="cs">Seznam objektů lokálních hustot (obsah číselníku c_ldsity_object)</para>
        /// <para lang="en">List of local density objects (content of the lookup table c_ldsity_object)</para>
        /// </summary>
        private TDLDsityObjectList cLDsityObjects;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;


        /// <summary>
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </summary>
        private TDLDsityObject ldsityObject;

        /// <summary>
        /// <para lang="cs">Objekt lokální hustoty pro třídění</para>
        /// <para lang="en">Local density object for clasification</para>
        /// </summary>
        private TDLDsityObject ldsityObjectForADSP;


        /// <summary>
        /// <para lang="cs">V tabulkách existují klasifikační pravidla, která nejsou unikátní</para>
        /// <para lang="en">There are some classification rules in database tables, that are not unique</para>
        /// </summary>
        private bool duplicateClassificationRule;

        private string msgNoLDsityContribForLDsityObject = String.Empty;
        private string msgTooManyLDsityContribForLDsityObject = String.Empty;
        private string msgInvalidClassificationRules = String.Empty;
        private string msgNoCategoryForSave = String.Empty;
        private string msgNoLDsityObjectsForADSPForSave = String.Empty;
        private string msgNoClassificationRules = String.Empty;
        private string msgNoSuccesfullyCategorizedPanels = String.Empty;
        private string msgADCToClassificationRuleNotUninque = String.Empty;
        private string msgSPCToClassificationRuleNotUninque = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek - nadpis pro klasifikační pravidla</para>
        /// <para lang="en">Control - caption for classification rules</para>
        /// </summary>
        private ControlClassificationRuleCaption<TDomain, TCategory> ctrClassificationRuleCaption;

        /// <summary>
        /// <para lang="cs">Ovládací prvek - přepínač klasifikačních pravidel</para>
        /// <para lang="en">Control - classification rule switch</para>
        /// </summary>
        private ControlClassificationRuleSwitch<TDomain, TCategory> ctrClassificationRuleSwitch;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro definici závislostí
        /// mezi kategoriemi plošné domény nebo subpopulace</para>
        /// <para lang="en">Control for definition dependencies
        /// among area domain or subpopulation categories</para>
        /// </summary>
        private ControlDependenceEditor<TDomain, TCategory> ctrDependenceEditor;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </param>
        /// <param name="ldsityObject">
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </param>
        /// <param name="ldsityObjectForADSP">
        /// <para lang="cs">Objekt lokální hustoty pro třídění</para>
        /// <para lang="en">Local density object for clasification</para>
        /// </param>
        /// <param name="readOnlyMode">
        /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
        /// <para lang="en">Changes in classification rules are not allowed</para>
        /// </param>
        public FormClassificationRule(
            Control controlOwner,
            TDomain domain,
            TDLDsityObject ldsityObject,
            TDLDsityObject ldsityObjectForADSP,
            bool readOnlyMode)
            : base()
        {
            Initialize(
                controlOwner: controlOwner,
                domain: domain,
                ldsityObject: ldsityObject,
                ldsityObjectForADSP: ldsityObjectForADSP,
                readOnlyMode: readOnlyMode);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Klasifikační typ (read-only)</para>
        /// <para lang="en">Classification type (read-only)</para>
        /// </summary>
        public TDClassificationTypeEnum ClassificationType
        {
            get
            {
                return Domain.ClassificationTypeValue;
            }
        }


        /// <summary>
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </summary>
        public TDomain Domain
        {
            get
            {
                if (domain == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Domain)} must not be null.",
                        paramName: nameof(Domain));
                }
                return domain;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Domain)} must not be null.",
                        paramName: nameof(Domain));
                }
                domain = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </summary>
        public TDLDsityObject LDsityObject
        {
            get
            {
                if (ldsityObject == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(LDsityObject)} must not be null.",
                        paramName: nameof(LDsityObject));
                }
                return ldsityObject;
            }
            set
            {
                ldsityObject = value ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(LDsityObject)} must not be null.",
                        paramName: nameof(LDsityObject));
            }
        }

        /// <summary>
        /// <para lang="cs">Objekt lokální hustoty pro třídění (read-only)</para>
        /// <para lang="en">Local density object for clasification (read-only)</para>
        /// </summary>
        public TDLDsityObject LDsityObjectForADSP
        {
            get
            {
                if (ldsityObjectForADSP == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(LDsityObjectForADSP)} must not be null.",
                        paramName: nameof(LDsityObjectForADSP));
                }
                return ldsityObjectForADSP;
            }
            set
            {
                ldsityObjectForADSP = value ??
                    throw new ArgumentException(
                        message: $"Argument {nameof(LDsityObjectForADSP)} must not be null.",
                        paramName: nameof(LDsityObjectForADSP));
            }
        }


        /// <summary>
        /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
        /// <para lang="en">Changes in classification rules are not allowed</para>
        /// </summary>
        public bool ReadOnlyMode
        {
            get
            {
                return readOnlyMode;
            }
            set
            {
                readOnlyMode = value;

                if (ReadOnlyMode)
                {
                    BtnOK.Visible = false;
                    BtnCancel.Location = new System.Drawing.Point(x: 790, y: 5);

                    TsrCategory.Visible = false;
                    BtnDependenceDefine.Visible = false;
                    TlpCategory.RowStyles[0].Height = 0;

                    if (ctrClassificationRuleSwitch != null)
                    {
                        ctrClassificationRuleSwitch.PositiveClassificationRuleData.BtnImportJson.Visible = false;
                        ctrClassificationRuleSwitch.NegativeClassificationRuleData.BtnImportJson.Visible = false;
                    }
                }
                else
                {
                    BtnOK.Visible = true;
                    BtnCancel.Location = new System.Drawing.Point(x: 635, y: 5);

                    TsrCategory.Visible = true;
                    BtnDependenceDefine.Visible = true;
                    TlpCategory.RowStyles[0].Height = 25;

                    if (ctrClassificationRuleSwitch != null)
                    {
                        ctrClassificationRuleSwitch.PositiveClassificationRuleData.BtnImportJson.Visible = true;
                        ctrClassificationRuleSwitch.NegativeClassificationRuleData.BtnImportJson.Visible = true;
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Úroveň uzlů v TreeView, kde jsou uloženy páry klasifikačních pravidel (read-only)</para>
        /// <para lang="en">TreeView Node level, where classification rule pairs are stored (read-only)</para>
        /// </summary>
        public static int Level
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// <para lang="cs">Je aktuální objekt lokální hustoty pro třídění
        /// zahrnutý mezi objekty pro uložení do databáze? (read-only)</para>
        /// <para lang="en">Is current local density object for classification
        /// included among objects for saving in database? (read-only)</para>
        /// </summary>
        public bool SelectedLDsityObjectForADSPIncluded
        {
            get
            {
                return true;
            }
        }


        /// <summary>
        /// <para lang="cs">Seznam všech uzlů v TreeView kategorií</para>
        /// <para lang="en">List of all nodes in TreeView of categories</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzel předcházející vybranému uzlu</para>
        /// <para lang="en">TreeNode previous selected TreeNode</para>
        /// </summary>
        private TreeNode PreviousNode
        {
            get
            {
                return previousNode;
            }
            set
            {
                previousNode = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam objektů lokálních hustot (obsah číselníku c_ldsity_object)</para>
        /// <para lang="en">List of local density objects (content of the lookup table c_ldsity_object)</para>
        /// </summary>
        private TDLDsityObjectList CLDsityObjects
        {
            get
            {
                return cLDsityObjects ??
                    new TDLDsityObjectList(database: Database);
            }
            set
            {
                cLDsityObjects = value ??
                    new TDLDsityObjectList(database: Database);
            }
        }


        /// <summary>
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace (read-only)</para>
        /// <para lang="en">Categories of the area domain or subpopulation (read-only)</para>
        /// </summary>
        public List<TCategory> Categories
        {
            get
            {
                return [.. Nodes
                    .Where(a => a.Level == 0)
                    .Select(a => (ClassificationRulePair<TDomain, TCategory>)a.Tag)
                    .Select(a => a.Category)
                    .Distinct()
                    .OrderBy(a => a.Id)];
            }
        }

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot pro třídění (read-only)</para>
        /// <para lang="en">Local density objects for classification (read-only)</para>
        /// </summary>
        public List<TDLDsityObject> LDsityObjectsForADSP
        {
            get
            {
                return [.. Nodes
                    .Where(a => a.Level == Level)
                    .Select(a => (ClassificationRulePair<TDomain, TCategory>)a.Tag)
                    .Select(a => a.LDsityObjectForADSP)
                    .Distinct()
                    .OrderBy(a => a.Id)];
            }
        }


        /// <summary>
        /// <para lang="cs">Páry klasifikačních pravidel(read-only)</para>
        /// <para lang="en">Classification rule pairs(read-only)</para>
        /// </summary>
        public List<ClassificationRulePair<TDomain, TCategory>> ClassificationRulePairs
        {
            get
            {
                return [.. Nodes
                    .Where(a => a.Level == Level)
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is ClassificationRulePair<TDomain, TCategory>)
                    .Select(a => (ClassificationRulePair<TDomain, TCategory>)a.Tag)
                    .Distinct()
                    .OrderBy(a => a.Category.Id)
                    .ThenBy(a => a.LDsityObject.Id)
                    .ThenBy(a => a.LDsityObjectForADSP.Id)];
            }
        }

        /// <summary>
        /// <para lang="cs">Klasifikační pravidla kategorií plošných domén nebo subpopulací
        /// pro kladné příspěvky lokálních hustot(read-only)</para>
        /// <para lang="en">Classification rules for area domain or subpopulation categories
        /// for positive local density contributions(read-only)</para>
        /// </summary>
        public List<ClassificationRule<TDomain, TCategory>> ClassificationRulesPositive
        {
            get
            {
                return ClassificationRulePairs
                    .Select(a => a.Positive)
                    .Distinct()
                    .ToList<ClassificationRule<TDomain, TCategory>>();
            }
        }

        /// <summary>
        /// <para lang="cs">Klasifikační pravidla kategorií plošných domén nebo subpopulací
        /// pro záporné příspěvky lokálních hustot(read-only)</para>
        /// <para lang="en">Classification rules for area domain or subpopulation categories
        /// for negative local density contributions(read-only)</para>
        /// </summary>
        public List<ClassificationRule<TDomain, TCategory>> ClassificationRulesNegative
        {
            get
            {
                return ClassificationRulePairs
                    .Select(a => a.Negative)
                    .Distinct()
                    .ToList();
            }
        }


        /// <summary>
        /// <para lang="cs">Vybraný pár klasifikačních pravidel (read-only)</para>
        /// <para lang="en">Selected classification rule pair (read-only)</para>
        /// </summary>
        public ClassificationRulePair<TDomain, TCategory> SelectedClassificationRulePair
        {
            get
            {
                if (TvwCategory.SelectedNode == null)
                {
                    return null;
                }

                if (TvwCategory.SelectedNode.Tag == null)
                {
                    return null;
                }

                return
                    (ClassificationRulePair<TDomain, TCategory>)
                        TvwCategory.SelectedNode.Tag;
            }
        }

        /// <summary>
        /// <para lang="cs">Předcházející pár klasifikačních pravidel (read-only)</para>
        /// <para lang="en">Previous classification rule pair (read-only)</para>
        /// </summary>
        public ClassificationRulePair<TDomain, TCategory> PreviousClassificationRulePair
        {
            get
            {
                if (PreviousNode == null)
                {
                    return null;
                }

                if (PreviousNode.Tag == null)
                {
                    return null;
                }

                return
                    (ClassificationRulePair<TDomain, TCategory>)
                        PreviousNode.Tag;
            }
        }


        /// <summary>
        /// <para lang="cs">Vybrané kladné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Selected positive classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> SelectedClassificationRulePositive
        {
            get
            {
                if (SelectedClassificationRulePair == null)
                {
                    return null;
                }

                if (TvwCategory.SelectedNode.Level != Level)
                {
                    return null;
                }

                return
                    SelectedClassificationRulePair.Positive;
            }
        }

        /// <summary>
        /// <para lang="cs">Předcházející kladné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Previous positive classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> PreviousClassificationRulePositive
        {
            get
            {
                if (PreviousClassificationRulePair == null)
                {
                    return null;
                }

                if (previousNode.Level != Level)
                {
                    return null;
                }

                return
                    PreviousClassificationRulePair.Positive;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané záporné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Selected negative classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> SelectedClassificationRuleNegative
        {
            get
            {
                if (ClassificationType != TDClassificationTypeEnum.ChangeOrDynamic)
                {
                    return null;
                }

                if (SelectedClassificationRulePair == null)
                {
                    return null;
                }

                if (TvwCategory.SelectedNode.Level != Level)
                {
                    return null;
                }

                return
                    SelectedClassificationRulePair.Negative;
            }
        }

        /// <summary>
        /// <para lang="cs">Předcházející záporné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Previous negative classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> PreviousClassificationRuleNegative
        {
            get
            {
                if (ClassificationType != TDClassificationTypeEnum.ChangeOrDynamic)
                {
                    return null;
                }

                if (PreviousClassificationRulePair == null)
                {
                    return null;
                }

                if (previousNode.Level != Level)
                {
                    return null;
                }

                return
                    PreviousClassificationRulePair.Negative;
            }
        }


        /// <summary>
        /// <para lang="cs">Pořadové číslo pro novou kategorii plošné domény nebo subpopulace (read-only)</para>
        /// <para lang="en">Serial number for new area domain or subpopulation category (read-only)</para>
        /// </summary>
        public int SerialNumber
        {
            get
            {
                return
                    Categories.Count != 0
                        ? Categories.Max(a => a.Id) + 1
                        : 1;
            }
        }


        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot ve vybrané skupině
        /// (vybrané na 1.formuláři) (read-only)</para>
        /// <para lang="en">Local density objects in the selected group
        /// (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectList SelectedLocalDensityObjects
        {
            get
            {
                Dictionary<
                    TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                    TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
                    ldsityObjectsForTargetVariable =
                        ((ControlTDSelectionAttributeCategory)ControlOwner)
                            .LDsityObjectsForTargetVariableSelect(
                                domainType: ArealOrPopulation);

                List<int> listLDsityObjectsForTargetVariableIds
                    = ldsityObjectsForTargetVariable.Values
                    .Select(b => b.LDsityObject.Id)
                    .ToList();

                TDLDsityObjectList result = new(
                    database: Database,
                    rows:
                        CLDsityObjects.Data.AsEnumerable()
                        .Where(a => listLDsityObjectsForTargetVariableIds
                        .Contains(Functions.GetIntArg(
                            row: a,
                            name: TDLDsityObjectList.ColId.Name,
                            defaultValue: 0))));

                return result;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané příspěvky lokálních hustot
        /// pro vybranou skupinu cílových proměnných
        /// (vybrané na 2.formuláři) (read-only)</para>
        /// <para lang="en">Selected local density contributions
        /// for the selected target variable group
        /// (selected on the 2nd form) (read-only)</para>
        /// </summary>
        public TDLDsityList SelectedLDsities
        {
            get
            {
                return ((ControlTDSelectionAttributeCategory)ControlOwner)
                    .LDsitiesForTargetVariableSelect(
                        domainType: ArealOrPopulation);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}",          "Editace klasifikačních pravidel pro kategorie plošné domény" },
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}ReadOnly",  "Klasifikační pravidla pro kategorie plošné domény" },
                            { nameof(BtnCancel),                                                "Zrušit" },
                            { nameof(BtnDependenceDefine),                                      "Definovat závislosti" },
                            { nameof(BtnOK),                                                    "Zapsat do databáze" },
                            { nameof(GrpCategory),                                              "Kategorie plošné domény" },
                            { nameof(GrpWorkSpace),                                             String.Empty },
                            { nameof(TsrCategory),                                              String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),          "Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),     "Pro vybraný objekt lokální hustoty v databázi existuje více než jeden příspěvek lokální hustoty."  },
                            { nameof(msgInvalidClassificationRules),              "Některé kategorie plošné domény mají neplatná klasifikační pravidla." },
                            { nameof(msgNoCategoryForSave),                       "Není definovaná žádná kategorie plošné domény pro uložení do databáze." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),           "Nejsou zvolené žádné objekty lokálních hustot pro třídění." },
                            { nameof(msgNoClassificationRules),                   "Nejsou zvolena žádná klasifikační pravidla pro uložení do databáze." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),          "Neexistují žádné panely s plně a úspěšně zařazenými objekty podle klasifikačních pravidel." },
                            { nameof(msgADCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Záznamy klasifikačních pravidel v tabulce cm_adc2classification_rule nejsou unikátní. $0",
                                                                                    "Uložená procedura fn_get_classification_rule4adc vrací více než jeden záznam klasifikačního pravidla $0",
                                                                                    "pro vstup area_domain_category: $1, ldsity_object: $2, use_negative: $3. $0") },
                            { nameof(msgSPCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Záznamy klasifikačních pravidel v tabulce cm_spc2classification_rule nejsou unikátní. $0",
                                                                                    "Uložená procedura fn_get_classification_rule4spc vrací více než jeden záznam klasifikačního pravidla $0",
                                                                                    "pro vstup sub_population_category: $1, ldsity_object: $2, use_negative: $3. $0") }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormClassificationRule<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}",          "Editace klasifikačních pravidel pro kategorie subpopulace" },
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}ReadOnly",  "Klasifikační pravidla pro kategorie subpopulace" },
                            { nameof(BtnCancel),                                                "Zrušit" },
                            { nameof(BtnDependenceDefine),                                      "Definovat závislosti" },
                            { nameof(BtnOK),                                                    "Zapsat do databáze" },
                            { nameof(GrpCategory),                                              "Kategorie subpopulace" },
                            { nameof(GrpWorkSpace),                                             String.Empty },
                            { nameof(TsrCategory),                                              String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),          "Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),     "Pro vybraný objekt lokální hustoty v databázi existuje více než jeden příspěvek lokální hustoty."  },
                            { nameof(msgInvalidClassificationRules),              "Některé kategorie subpopulace mají neplatná klasifikační pravidla." },
                            { nameof(msgNoCategoryForSave),                       "Není definovaná žádná kategorie subpopulace pro uložení do databáze." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),           "Nejsou zvolené žádné objekty lokálních hustot pro třídění." },
                            { nameof(msgNoClassificationRules),                   "Nejsou zvolena žádná klasifikační pravidla pro uložení do databáze." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),          "Neexistují žádné panely s plně a úspěšně zařazenými objekty podle klasifikačních pravidel." },
                            { nameof(msgADCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Záznamy klasifikačních pravidel v tabulce cm_adc2classification_rule nejsou unikátní. $0",
                                                                                    "Uložená procedura fn_get_classification_rule4adc vrací více než jeden záznam klasifikačního pravidla $0",
                                                                                    "pro vstup area_domain_category: $1, ldsity_object: $2, use_negative: $3. $0") },
                            { nameof(msgSPCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Záznamy klasifikačních pravidel v tabulce cm_spc2classification_rule nejsou unikátní. $0",
                                                                                    "Uložená procedura fn_get_classification_rule4spc vrací více než jeden záznam klasifikačního pravidla $0",
                                                                                    "pro vstup sub_population_category: $1, ldsity_object: $2, use_negative: $3. $0") }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormClassificationRule<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictNationalSubPopulation)
                                ? dictNationalSubPopulation
                                : [],

                    _ => [],
                },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}",              "Change classification rules for area domain categories" },
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}ReadOnly",      "Classification rules for area domain categories" },
                            { nameof(BtnCancel),                                                    "Cancel" },
                            { nameof(BtnDependenceDefine),                                          "Define dependences" },
                            { nameof(BtnOK),                                                        "Write to database" },
                            { nameof(GrpCategory),                                                  "Area domain categories" },
                            { nameof(GrpWorkSpace),                                                 String.Empty },
                            { nameof(TsrCategory),                                                  String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),          "There is no local density contribution in database for selected local density object." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),     "There are more than one local density contribution in database for selected local density object." },
                            { nameof(msgInvalidClassificationRules),              "Some area domain categories contain invalid classification rules." },
                            { nameof(msgNoCategoryForSave),                       "There is no defined area domain category for inserting into database." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),           "There are no selected local density objects for classification." },
                            { nameof(msgNoClassificationRules),                   "There are no selected classification rules for inserting into database." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),          "There are no panels with completely and succesfully categorized objects by given classification rules." },
                            { nameof(msgADCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Classification rules in database table cm_adc2classification_rule are not unique. $0",
                                                                                    "Stored procedure fn_get_classification_rule4adc returns more than one classification rule $0",
                                                                                    "for parameters area_domain_category: $1, ldsity_object: $2, use_negative: $3. $0") },
                            { nameof(msgSPCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Classification rules in database table cm_spc2classification_rule are not unique. $0",
                                                                                    "Stored procedure  fn_get_classification_rule4spc vrátila returns more than one classification rule $0",
                                                                                    "for parameters sub_population_category: $1, ldsity_object: $2, use_negative: $3. $0") }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormClassificationRule<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}",          "Change classification rules for subpopulation categories" },
                            { $"{nameof(FormClassificationRule<TDomain, TCategory>)}ReadOnly",  "Classification rules for subpopulation categories" },
                            { nameof(BtnCancel),                                                "Cancel" },
                            { nameof(BtnDependenceDefine),                                      "Define dependences" },
                            { nameof(BtnOK),                                                    "Write to database" },
                            { nameof(GrpCategory),                                              "Subpopulation categories" },
                            { nameof(GrpWorkSpace),                                             String.Empty },
                            { nameof(TsrCategory),                                              String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),          "There is no local density contribution in database for selected local density object." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),     "There are more than one local density contribution in database for selected local density object." },
                            { nameof(msgInvalidClassificationRules),              "Some subpopulation categories contain invalid classification rules." },
                            { nameof(msgNoCategoryForSave),                       "There is no defined subpopulation category for inserting into database." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),           "There are no selected local density objects for classification." },
                            { nameof(msgNoClassificationRules),                   "There are no selected classification rules for inserting into database." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),          "There are no panels with completely and succesfully categorized objects by given classification rules." },
                            { nameof(msgADCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Classification rules in database table cm_adc2classification_rule are not unique. $0",
                                                                                    "Stored procedure fn_get_classification_rule4adc returns more than one classification rule $0",
                                                                                    "for parameters area_domain_category: $1, ldsity_object: $2, use_negative: $3. $0") },
                            { nameof(msgSPCToClassificationRuleNotUninque),       String.Concat(
                                                                                    "Classification rules in database table cm_spc2classification_rule are not unique. $0",
                                                                                    "Stored procedure  fn_get_classification_rule4spc vrátila returns more than one classification rule $0",
                                                                                    "for parameters sub_population_category: $1, ldsity_object: $2, use_negative: $3. $0") }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormClassificationRule<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </param>
        /// <param name="ldsityObject">
        /// <para lang="cs">Objekt lokální hustoty</para>
        /// <para lang="en">Local density object</para>
        /// </param>
        /// <param name="ldsityObjectForADSP">
        /// <para lang="cs">Objekt lokální hustoty pro třídění</para>
        /// <para lang="en">Local density object for clasification</para>
        /// </param>
        /// <param name="readOnlyMode">
        /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
        /// <para lang="en">Changes in classification rules are not allowed</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            TDomain domain,
            TDLDsityObject ldsityObject,
            TDLDsityObject ldsityObjectForADSP,
            bool readOnlyMode)
        {
            ControlOwner = controlOwner;

            Domain = domain;

            LDsityObject = ldsityObject;

            LDsityObjectForADSP = ldsityObjectForADSP;

            Nodes = null;

            PreviousNode = null;

            CLDsityObjects = null;

            onEdit = false;

            duplicateClassificationRule = false;

            LoadContent();

            InitializeToolStripIcons();

            InitializeControls();

            // Nastaveno až po InitializeControls, aby viditelnost tlačítka btnImportJson reagovala na readonly mode (btnImportJson je součástí ControlClassificationRuleData)
            // Set after InitializeControls, so that the visibility of the btnImportJson button responds to readonly mode (btnImportJson is part of ControlClassificationRuleData)
            ReadOnlyMode = readOnlyMode;

            InitializeLabels();

            InitializeTreeView();

            SelectCategory();

            SetStyle();

            BtnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            BtnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            BtnDependenceDefine.Click += new EventHandler(
                (sender, e) =>
                {
                    ShowDependencies();
                });

            TvwCategory.AfterSelect += new TreeViewEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SelectCategory();
                    }
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !SaveRules();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });

            BtnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu uzlů v TreeView kategoriemi plošné domény nebo subpopulace
        /// Pro novou plošnou doménu nebo subpopulaci (nemá ještě žádné kategorie) zůstává TreeView prázdné</para>
        /// <para lang="en">Initializing list of nodes in TreeView by categories of area domain or subpopulation
        /// For new area domain or subpopulation (it has no categories yet) TreeView is empty</para>
        /// </summary>
        private void InitializeTreeView()
        {
            // TreeView Categories
            TvwCategory.Nodes.Clear();
            Nodes = null;

            duplicateClassificationRule = false;
            switch (ArealOrPopulation)
            {
                case TDArealOrPopulationEnum.AreaDomain:
                    foreach (
                        TDAreaDomainCategory adc in
                        Database.STargetData.CAreaDomainCategory.Items
                            .Where(a => a.AreaDomainId == domain.Id))
                    {
                        AddCategoryIntoTreeView(
                            category: (TCategory)Convert.ChangeType(
                                value: adc,
                                conversionType: typeof(TCategory)));
                    }
                    return;

                case TDArealOrPopulationEnum.Population:
                    foreach (
                        TDSubPopulationCategory spc in
                        Database.STargetData.CSubPopulationCategory.Items
                            .Where(a => a.SubPopulationId == domain.Id))
                    {
                        AddCategoryIntoTreeView(
                            category: (TCategory)Convert.ChangeType(
                                value: spc,
                                conversionType: typeof(TCategory)));
                    }
                    return;

                default:
                    return;
            }
        }


        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            BtnOK.Font = Setting.ButtonFont;
            BtnOK.ForeColor = Setting.ButtonForeColor;

            BtnCancel.Font = Setting.ButtonFont;
            BtnCancel.ForeColor = Setting.ButtonForeColor;

            BtnDependenceDefine.Font = Setting.ToolStripButtonFont;
            BtnDependenceDefine.ForeColor = Setting.ToolStripButtonForeColor;

            GrpCategory.Font = Setting.GroupBoxFont;
            GrpCategory.ForeColor = Setting.GroupBoxForeColor;

            GrpWorkSpace.Font = Setting.GroupBoxFont;
            GrpWorkSpace.ForeColor = Setting.GroupBoxForeColor;

            LblCaption.Font = Setting.LabelMainFont;
            LblCaption.ForeColor = Setting.LabelMainForeColor;

            LblClassificationRuleCaption.Font = Setting.LabelValueFont;
            LblClassificationRuleCaption.ForeColor = Setting.LabelValueForeColor;

            LblClassificationRuleSwitch.Font = Setting.LabelValueFont;
            LblClassificationRuleSwitch.ForeColor = Setting.LabelValueForeColor;

            LblDependenceEditor.Font = Setting.LabelValueFont;
            LblDependenceEditor.ForeColor = Setting.LabelValueForeColor;
        }


        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent()
        {
            CLDsityObjects = new TDLDsityObjectList(database: Database);
            CLDsityObjects.ReLoad();

            switch (ArealOrPopulation)
            {
                case TDArealOrPopulationEnum.AreaDomain:
                    Database.STargetData.CAreaDomain.ReLoad();
                    Database.STargetData.CAreaDomainCategory.ReLoad();
                    Database.STargetData.CLDsityObject.ReLoad();
                    return;

                case TDArealOrPopulationEnum.Population:
                    Database.STargetData.CSubPopulation.ReLoad();
                    Database.STargetData.CSubPopulationCategory.ReLoad();
                    Database.STargetData.CLDsityObject.ReLoad();
                    return;

                default:
                    return;
            }
        }


        /// <summary>
        /// <para lang="cs">Inicializace ikon tlačítek</para>
        /// <para lang="en">Initializing the button icons</para>
        /// </summary>
        private void InitializeToolStripIcons()
        {
            ControlResources resources = new();

            BtnDependenceDefine.Image = resources.IconSchema.Image;
            BtnDependenceDefine.DisplayStyle = ToolStripItemDisplayStyle.Image;

            ImageList ilCategoryStatus = new();

            ilCategoryStatus.Images.Add(
                key: "blue",
                image: resources.IconBlueBall.Image);

            ilCategoryStatus.Images.Add(
                key: "green",
                image: resources.IconGreenBall.Image);

            ilCategoryStatus.Images.Add(
                key: "red", image:
                resources.IconRedBall.Image);

            TvwCategory.ImageList = ilCategoryStatus;
            TvwCategory.SelectedImageKey = "blue";
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacích prvků</para>
        /// <para lang="en">Initializing user controls</para>
        /// </summary>
        private void InitializeControls()
        {
            // TreeView Categories
            TvwCategory.Nodes.Clear();
            Nodes = null;

            HideDependencies();

            // ControlClassificationRuleCaption
            PnlClassificationRuleCaption.Controls.Clear();
            ctrClassificationRuleCaption =
                new ControlClassificationRuleCaption<TDomain, TCategory>(
                    controlOwner: this)
                {
                    DisplayCheckBox = false,
                    Dock = DockStyle.Fill
                };
            ctrClassificationRuleCaption.LDsityObjectForADSPExcluded
                += new EventHandler(
                    (sender, e) =>
                    {
                        ctrClassificationRuleSwitch?.Reset();
                        ctrDependenceEditor?.Reset();
                    });
            PnlClassificationRuleCaption.Controls.Add(
                value: ctrClassificationRuleCaption);

            // ControlClassificationRuleSwitch
            PnlClassificationRuleSwitch.Controls.Clear();
            ctrClassificationRuleSwitch =
                new ControlClassificationRuleSwitch<TDomain, TCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrClassificationRuleSwitch.ClassificationRuleTextChanged +=
                new EventHandler(
                    (sender, e) =>
                    {
                        SetClassificationRules();
                    });
            PnlClassificationRuleSwitch.Controls.Add(
                value: ctrClassificationRuleSwitch);

            ctrClassificationRuleSwitch.PositiveClassificationRuleData.BtnExportJson.Visible = false;
            ctrClassificationRuleSwitch.NegativeClassificationRuleData.BtnExportJson.Visible = false;

            // ControlDependenceEditor
            PnlDependenceEditor.Controls.Clear();
            ctrDependenceEditor =
                new ControlDependenceEditor<TDomain, TCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrDependenceEditor.DependenceEditorClosed += new EventHandler(
                (sender, e) =>
                {
                    HideDependencies();
                });
            PnlDependenceEditor.Controls.Add(
                value: ctrDependenceEditor);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing the form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text = ReadOnlyMode
                ? (labels.TryGetValue(
                    key: $"{nameof(FormClassificationRule<TDomain, TCategory>)}ReadOnly",
                    out string frmClassificationRuleReadOnly)
                        ? frmClassificationRuleReadOnly
                        : String.Empty)
                : (labels.TryGetValue(
                    key: $"{nameof(FormClassificationRule<TDomain, TCategory>)}",
                    out string frmClassificationRuleText)
                        ? frmClassificationRuleText
                        : String.Empty);

            BtnCancel.Text =
                labels.TryGetValue(
                    key: nameof(BtnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            BtnDependenceDefine.Text =
                labels.TryGetValue(
                    key: nameof(BtnDependenceDefine),
                    out string btnDependenceDefineText)
                        ? btnDependenceDefineText
                        : String.Empty;

            BtnOK.Text =
                labels.TryGetValue(
                    key: nameof(BtnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            GrpCategory.Text =
                labels.TryGetValue(
                    key: nameof(GrpCategory),
                    out string grpCategoryText)
                        ? grpCategoryText
                        : String.Empty;

            GrpWorkSpace.Text =
                labels.TryGetValue(
                    key: nameof(GrpWorkSpace),
                    out string grpWorkSpaceText)
                        ? grpWorkSpaceText
                        : String.Empty;

            LblCaption.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? Domain.ExtendedLabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? Domain.ExtendedLabelCs
                        : Domain.ExtendedLabelEn;

            TsrCategory.Text =
                labels.TryGetValue(
                    key: nameof(TsrCategory),
                    out string tsrCategoryText)
                        ? tsrCategoryText
                        : String.Empty;

            msgNoLDsityContribForLDsityObject =
               labels.TryGetValue(key: nameof(msgNoLDsityContribForLDsityObject),
                       out msgNoLDsityContribForLDsityObject)
                           ? msgNoLDsityContribForLDsityObject
                           : String.Empty;

            msgTooManyLDsityContribForLDsityObject =
               labels.TryGetValue(key: nameof(msgTooManyLDsityContribForLDsityObject),
                       out msgTooManyLDsityContribForLDsityObject)
                           ? msgTooManyLDsityContribForLDsityObject
                           : String.Empty;

            msgInvalidClassificationRules =
               labels.TryGetValue(key: nameof(msgInvalidClassificationRules),
                       out msgInvalidClassificationRules)
                           ? msgInvalidClassificationRules
                           : String.Empty;

            msgNoCategoryForSave =
               labels.TryGetValue(key: nameof(msgNoCategoryForSave),
                       out msgNoCategoryForSave)
                           ? msgNoCategoryForSave
                           : String.Empty;

            msgNoLDsityObjectsForADSPForSave =
               labels.TryGetValue(key: nameof(msgNoLDsityObjectsForADSPForSave),
                       out msgNoLDsityObjectsForADSPForSave)
                           ? msgNoLDsityObjectsForADSPForSave
                           : String.Empty;

            msgNoClassificationRules =
               labels.TryGetValue(key: nameof(msgNoClassificationRules),
                       out msgNoClassificationRules)
                           ? msgNoClassificationRules
                           : String.Empty;

            msgNoSuccesfullyCategorizedPanels =
               labels.TryGetValue(key: nameof(msgNoSuccesfullyCategorizedPanels),
                       out msgNoSuccesfullyCategorizedPanels)
                           ? msgNoSuccesfullyCategorizedPanels
                           : String.Empty;

            msgADCToClassificationRuleNotUninque =
               labels.TryGetValue(key: nameof(msgADCToClassificationRuleNotUninque),
                       out msgADCToClassificationRuleNotUninque)
                           ? msgADCToClassificationRuleNotUninque
                           : String.Empty;

            msgSPCToClassificationRuleNotUninque =
               labels.TryGetValue(key: nameof(msgSPCToClassificationRuleNotUninque),
                       out msgSPCToClassificationRuleNotUninque)
                           ? msgSPCToClassificationRuleNotUninque
                           : String.Empty;

            ctrClassificationRuleCaption?.InitializeLabels();

            ctrClassificationRuleSwitch?.InitializeLabels();

            ctrDependenceEditor?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Přidání kategorie plošné domény nebo subpopulace do TreeView</para>
        /// <para lang="en">Add area domain or subpopulation category into TreeView</para>
        /// </summary>
        /// <param name="category">
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Area domain or subpoulation category</para>
        /// </param>

        private void AddCategoryIntoTreeView(TCategory category)
        {
            if (category == null)
            {
                return;
            }

            onEdit = true;

            TreeNode nodeCategory = new()
            {
                Text =
                   (LanguageVersion == LanguageVersion.National) ? category.ExtendedLabelCs :
                   (LanguageVersion == LanguageVersion.International) ? category.ExtendedLabelEn :
                   category.Id.ToString()
            };

            // TreeView má pouze jednu úroveň
            // TreeView has only one level
            TvwCategory.Nodes.Add(node: nodeCategory);
            Nodes.Add(item: nodeCategory);

            ClassificationRulePair<TDomain, TCategory> cpair
                = new(
                        owner: this,
                        node: nodeCategory,
                        flat: true,
                        classificationType: ClassificationType,
                        category: category,
                        ldsityObject: LDsityObject,
                        ldsityObjectForADSP: LDsityObjectForADSP);

            switch (ArealOrPopulation)
            {
                case TDArealOrPopulationEnum.AreaDomain:

                    TDADCToClassificationRuleList adcToClassificationRulePositiveList =
                        TDFunctions.FnGetClassificationRuleForAdc.Execute(
                            database: Database,
                            areaDomainCategoryId: category.Id,
                            ldsityObjectId: ldsityObjectForADSP.Id,
                            useNegative: false);

                    TDADCToClassificationRule adcToClassificationRulePositive =
                        adcToClassificationRulePositiveList
                            .Items
                            .FirstOrDefault<TDADCToClassificationRule>();

                    TDADCToClassificationRuleList adcToClassificationRuleNegativeList =
                        TDFunctions.FnGetClassificationRuleForAdc.Execute(
                            database: Database,
                            areaDomainCategoryId: category.Id,
                            ldsityObjectId: ldsityObjectForADSP.Id,
                            useNegative: true);

                    TDADCToClassificationRule adcToClassificationRuleNegative =
                        adcToClassificationRuleNegativeList
                            .Items
                            .FirstOrDefault<TDADCToClassificationRule>();

                    cpair.Positive = new ClassificationRule<TDomain, TCategory>(
                        owner: cpair)
                    {
                        Text =
                            (adcToClassificationRulePositive != null) ?
                             adcToClassificationRulePositive.ClassificationRule : String.Empty
                    };

                    cpair.Negative = ClassificationType switch
                    {
                        TDClassificationTypeEnum.Standard =>
                            null,

                        TDClassificationTypeEnum.ChangeOrDynamic =>
                            new ClassificationRule<TDomain, TCategory>(owner: cpair)
                            {
                                Text = (adcToClassificationRuleNegative != null)
                                   ? adcToClassificationRuleNegative.ClassificationRule
                                   : String.Empty
                            },

                        _ => null,
                    };
                    nodeCategory.Tag = cpair;

                    if (!duplicateClassificationRule)
                    {
                        if (adcToClassificationRulePositiveList.Items.Count > 1)
                        {
                            duplicateClassificationRule = true;
                            MessageBox.Show(
                                text: msgADCToClassificationRuleNotUninque
                                        .Replace(oldValue: "$1", newValue: category.Id.ToString())
                                        .Replace(oldValue: "$2", newValue: ldsityObjectForADSP.Id.ToString())
                                        .Replace(oldValue: "$3", newValue: "false")
                                        .Replace(oldValue: "$0", newValue: Environment.NewLine),
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        }

                        else
                        {
                            if (adcToClassificationRuleNegativeList.Items.Count > 1)
                            {
                                duplicateClassificationRule = true;
                                MessageBox.Show(
                                   text: msgADCToClassificationRuleNotUninque
                                        .Replace(oldValue: "$1", newValue: category.Id.ToString())
                                        .Replace(oldValue: "$2", newValue: ldsityObjectForADSP.Id.ToString())
                                        .Replace(oldValue: "$3", newValue: "true")
                                        .Replace(oldValue: "$0", newValue: Environment.NewLine),
                                    caption: String.Empty,
                                    buttons: MessageBoxButtons.OK,
                                    icon: MessageBoxIcon.Information);
                            }
                        }
                    }

                    break;

                case TDArealOrPopulationEnum.Population:

                    TDSPCToClassificationRuleList spcToClassificationRulePositiveList =
                        TDFunctions.FnGetClassificationRuleForSpc.Execute(
                            database: Database,
                            subPopulationCategoryId: category.Id,
                            ldsityObjectId: ldsityObjectForADSP.Id,
                            useNegative: false);

                    TDSPCToClassificationRule spcToClassificationRulePositive =
                        spcToClassificationRulePositiveList
                            .Items
                            .FirstOrDefault<TDSPCToClassificationRule>();

                    TDSPCToClassificationRuleList spcToClassificationRuleNegativeList =
                        TDFunctions.FnGetClassificationRuleForSpc.Execute(
                            database: Database,
                            subPopulationCategoryId: category.Id,
                            ldsityObjectId: ldsityObjectForADSP.Id,
                            useNegative: true);

                    TDSPCToClassificationRule spcToClassificationRuleNegative =
                        spcToClassificationRuleNegativeList
                            .Items
                            .FirstOrDefault<TDSPCToClassificationRule>();

                    cpair.Positive = new ClassificationRule<TDomain, TCategory>(
                        owner: cpair)
                    {
                        Text =
                            (spcToClassificationRulePositive != null) ?
                             spcToClassificationRulePositive.ClassificationRule : String.Empty
                    };

                    cpair.Negative = ClassificationType switch
                    {
                        TDClassificationTypeEnum.Standard =>
                            null,

                        TDClassificationTypeEnum.ChangeOrDynamic =>
                            new ClassificationRule<TDomain, TCategory>(owner: cpair)
                            {
                                Text = (spcToClassificationRuleNegative != null)
                                        ? spcToClassificationRuleNegative.ClassificationRule
                                        : String.Empty
                            },

                        _ => null,
                    };
                    nodeCategory.Tag = cpair;

                    if (!duplicateClassificationRule)
                    {
                        if (spcToClassificationRulePositiveList.Items.Count > 1)
                        {
                            duplicateClassificationRule = true;
                            MessageBox.Show(
                                text: msgSPCToClassificationRuleNotUninque
                                        .Replace(oldValue: "$1", newValue: category.Id.ToString())
                                        .Replace(oldValue: "$2", newValue: ldsityObjectForADSP.Id.ToString())
                                        .Replace(oldValue: "$3", newValue: "false")
                                        .Replace(oldValue: "$0", newValue: Environment.NewLine),
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                        }

                        else
                        {
                            duplicateClassificationRule = true;
                            if (spcToClassificationRuleNegativeList.Items.Count > 1)
                            {
                                MessageBox.Show(
                                   text: msgSPCToClassificationRuleNotUninque
                                        .Replace(oldValue: "$1", newValue: category.Id.ToString())
                                        .Replace(oldValue: "$2", newValue: ldsityObjectForADSP.Id.ToString())
                                        .Replace(oldValue: "$3", newValue: "true")
                                        .Replace(oldValue: "$0", newValue: Environment.NewLine),
                                    caption: String.Empty,
                                    buttons: MessageBoxButtons.OK,
                                    icon: MessageBoxIcon.Information);
                            }
                        }

                    }

                    break;

                default:
                    nodeCategory.Tag = null;
                    break;
            }

            TvwCategory.SelectedNode = nodeCategory;

            onEdit = false;

            SelectCategory();
        }


        /// <summary>
        /// <para lang="cs">Spouští se v případě, že byla v TreeView vybrána kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">It fires if an area domain or subpopulation category was selected in the TreeView</para>
        /// </summary>
        private void SelectCategory()
        {
            if (TvwCategory.SelectedNode == null)
            {
                return;
            }

            ShowOrHideButtonDependenceDefine();

            ValidatePreviousNode();
            ColorTreeNodes();

            previousNode = TvwCategory.SelectedNode;

            //
            // Nový vybraný uzel -> musí se přenastavit nadpis a klasifikační pravidla
            //
            ctrClassificationRuleCaption?.Reset();
            ctrClassificationRuleSwitch?.Reset();
            ctrDependenceEditor?.Reset();
        }

        /// <summary>
        /// <para lang="cs">
        /// Kontrola validity klasifikačních pravidel předchozího uzlu
        /// </para>
        /// <para lang="en">
        /// Classification rules validation for previous TreeView Node
        /// </para>
        /// </summary>
        private void ValidatePreviousNode()
        {
            if (PreviousClassificationRulePair == null)
            {
                return;
            }

            PreviousClassificationRulePair
                .ValidateSyntax(verbose: false);

            if (ClassificationRulePairs == null)
            {
                return;
            }

            if (PreviousClassificationRulePair.Category == null)
            {
                return;
            }

            if (PreviousClassificationRulePair.LDsityObjectForADSP == null)
            {
                return;
            }

            // Validace všech uzlů v ramci stejné kategorie
            // a stejného objektu lokální hustoty pro třídění
            foreach (ClassificationRulePair<TDomain, TCategory> crp in ClassificationRulePairs
                .Where(a => a.Category.Id == PreviousClassificationRulePair.Category.Id)
                .Where(a => a.LDsityObjectForADSP.Id == PreviousClassificationRulePair.LDsityObjectForADSP.Id))
            {
                crp.ValidateSyntax(verbose: false);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Obarví červeně uzly v TreeView s nevalidním klasifikačním pravidlem
        /// a zeleně uzly s validním klasifikačním pravidlem
        /// </para>
        /// <para lang="en">
        /// Color red TreeNodes with not valid classification rulus
        /// and green TreeNodes with valid classification rules
        /// </para>
        /// </summary>
        public void ColorTreeNodes()
        {
            foreach (TreeNode node in Nodes)
            {
                if (node.Tag == null)
                {
                    node.ImageKey = "red";
                    continue;
                }

                ClassificationRulePair<TDomain, TCategory> pair =
                    (ClassificationRulePair<TDomain, TCategory>)node.Tag;

                if (pair == null)
                {
                    node.ImageKey = "red";
                    continue;
                }

                node.ImageKey =
                    pair.IsValid ?
                    "green" : "red";
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Zobrazení ovládacího prvku pro definici závislostí
        /// mezi kategoriemi plošné domény nebo subpopulace </para>
        /// <para lang="en">
        /// Display control for definition dependencies
        /// among area domain or subpopulation categories
        /// </para>
        /// </summary>
        private void ShowDependencies()
        {
            SplClassificationRule.Panel1Collapsed = true;
            SplClassificationRule.Panel2Collapsed = false;
        }

        /// <summary>
        /// <para lang="cs">
        /// Skrytí ovládacího prvku pro definici závislostí
        /// mezi kategoriemi plošné domény nebo subpopulace </para>
        /// <para lang="en">
        /// Hide control for definition dependencies
        /// among area domain or subpopulation categories
        /// </para>
        /// </summary>
        private void HideDependencies()
        {
            SplClassificationRule.Panel1Collapsed = false;
            SplClassificationRule.Panel2Collapsed = true;

            ShowOrHideButtonDependenceDefine();
        }

        /// <summary>
        /// <para lang="cs">
        /// Zobrazí nebo skryje tlačítko pro definici závislostí </para>
        /// <para lang="en">
        /// Show or hide buttun for define dependencies
        /// </para>
        /// </summary>
        private void ShowOrHideButtonDependenceDefine()
        {
            // Závislosti se definují pro kategorie v rámci
            // objektů lokálních hustot pro třídění (tj. nesmí být null)

            if (SelectedClassificationRulePair == null)
            {
                BtnDependenceDefine.Enabled = false;
                return;
            }

            if (SelectedClassificationRulePair.LDsityObjectForADSP == null)
            {
                BtnDependenceDefine.Enabled = false;
                return;
            }

            BtnDependenceDefine.Enabled = true;
        }

        /// <summary>
        /// <para lang="cs">Nastaví klasifikační pravidla pro vybrané uzely</para>
        /// <para lang="en">Set classifications rule for selected TreeNodes</para>
        /// </summary>
        private void SetClassificationRules()
        {
            if (SelectedClassificationRulePair == null)
            {
                return;
            }

            if (ClassificationRulePairs == null)
            {
                return;
            }

            // Stejné klasifikační pravidlo musí být nastaveno pro všechny ldsityObjects
            // v rámci jedné kategorie plošné domény nebo subpopulace
            // Same classification rule must be set for all ldsityObjects
            // in one area domain or subpopulation category
            foreach (ClassificationRulePair<TDomain, TCategory> crp in ClassificationRulePairs
                .Where(a => a.Category.Id == SelectedClassificationRulePair.Category.Id)
                .Where(a => a.LDsityObjectForADSP.Id == SelectedClassificationRulePair.LDsityObjectForADSP.Id))
            {
                if (crp.Positive != null)
                {
                    crp.Positive.Text =
                        (SelectedClassificationRulePair.Positive == null)
                        ? String.Empty
                        : SelectedClassificationRulePair.Positive.Text;
                }

                if (crp.Negative != null)
                {
                    crp.Negative.Text =
                        (SelectedClassificationRulePair.Negative == null)
                        ? String.Empty
                        : SelectedClassificationRulePair.Negative.Text;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Kontroluje počet vybraných příspěvků lokálních hustot</para>
        /// <para lang="en">Method checks number of selected local density contributions</para>
        /// </summary>
        /// <param name="ldsities">
        /// <para lang="cs">Seznam vybraných příspěvků lokálních hustot</para>
        /// <para lang="en">List of selected local density contributions</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrácí true pokud existuje právě jeden</para>
        /// <para lang="en">Returns true when just on local density contribution exists</para>
        /// </returns>
        public bool LDsityCountIsValid(List<TDLDsity> ldsities)
        {
            if (ldsities == null)
            {
                // Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty
                // There is none local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgNoLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            TDLDsity ldsity = ldsities.FirstOrDefault();

            if (ldsity == null)
            {
                // Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty
                // There is none local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgNoLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (ldsities.Count < 1)
            {
                // Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty
                // There is none local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgNoLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (ldsities.Count > 1)
            {
                // Pro vybraný objekt lokální hustoty v databázi existuje více než jeden příspěvek lokální hustoty
                // There are more than one local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgTooManyLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            return true;
        }


        /// <summary>
        /// <para lang="cs">
        /// Připraví vstupní argument pro uložené procedury,
        /// který obsahuje popis závislostí mezi kategoriemi plošných domén
        /// </para>
        /// <para lang="en">
        /// Prepares the input argument for the stored procedures,
        /// which contains a description of the dependencies between the area domain categories
        /// </para>
        /// </summary>
        /// <param name="ldsityObjectForADSP">
        /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
        /// <para lang="en">Local density object used for classification</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrací pole polí,
        /// vnitřní pole jsou pro jednotivé podřízené kategorie
        /// a obsahují seznam identifikátorů svých nadřízených kategorií
        /// </para>
        /// <para lang="en">
        /// Returns an array of arrays,
        /// the internal arrays are for each inferior area domain category
        /// and contain a list of identifiers of their superior area domain categories</para>
        /// </returns>
        public List<List<Nullable<int>>> PrepareParamADC(
            TDLDsityObject ldsityObjectForADSP)
        {
            if (ldsityObjectForADSP == null)
            {
                return null;
            }

            if (ArealOrPopulation != TDArealOrPopulationEnum.AreaDomain)
            {
                return null;
            }

            SuperiorDomain<TDomain, TCategory> dependencies =
                ctrDependenceEditor.Dependences.TryGetValue(
                    key: ldsityObjectForADSP,
                    out SuperiorDomain<TDomain, TCategory> superiorDomain)
                        ? superiorDomain
                        : null;

            return
                dependencies?.ToListOfListOfInt(
                    inferiorCategories: [.. Categories.OrderBy(a => a.Id)]);
        }

        /// <summary>
        /// <para lang="cs">
        /// Připraví vstupní argument pro uložené procedury,
        /// který obsahuje popis závislostí mezi kategoriemi subpopulací
        /// </para>
        /// <para lang="en">
        /// Prepares the input argument for the stored procedures,
        /// which contains a description of the dependencies between the subpopulation categories
        /// </para>
        /// </summary>
        /// <param name="ldsityObjectForADSP">
        /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
        /// <para lang="en">Local density object used for classification</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrací pole polí,
        /// vnitřní pole jsou pro jednotivé podřízené kategorie
        /// a obsahují seznam identifikátorů svých nadřízených kategorií
        /// </para>
        /// <para lang="en">
        /// Returns an array of arrays,
        /// the internal arrays are for each inferior subpopulation category
        /// and contain a list of identifiers of their superior subpopulation categories</para>
        /// </returns>
        public List<List<Nullable<int>>> PrepareParamSPC(
            TDLDsityObject ldsityObjectForADSP)
        {
            if (ldsityObjectForADSP == null)
            {
                return null;
            }

            if (ArealOrPopulation != TDArealOrPopulationEnum.Population)
            {
                return null;
            }

            SuperiorDomain<TDomain, TCategory> dependencies =
                ctrDependenceEditor.Dependences.TryGetValue(
                    key: ldsityObjectForADSP,
                    out SuperiorDomain<TDomain, TCategory> superiorDomain)
                        ? superiorDomain
                        : null;

            return
                dependencies?.ToListOfListOfInt(
                    inferiorCategories: [.. Categories.OrderBy(a => a.Id)]);
        }


        /// <summary>
        /// <para lang="cs">Výsledek validace celé sady klasifikačních pravidel</para>
        /// <para lang="en">Result of the validation for whole set of classification rules</para>
        /// </summary>
        /// <param name="classificationRule">
        /// <para lang="cs">Klasifikační pravidlo</para>
        /// <para lang="en">Classification rule</para>
        /// </param>
        /// <param name="useNegative">
        /// <para lang="cs">Záporné příspěvky lokálních hustot</para>
        /// <para lang="en">Negative local density contributions</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací výsledek validace celé sady klasifikačních pravidel</para>
        /// <para lang="en">Returns result of the validation for whole set of classification rules</para>
        /// </returns>
        public TDVwClassificationRuleCheckList GetClassificationRuleCheckList(
            ClassificationRule<TDomain, TCategory> classificationRule,
            bool useNegative)
        {
            if (classificationRule == null)
            {
                return null;
            }

            // Příspěvek lokální hustoty pro vybraný objekt lokální hustoty
            // Pro každný objekt lokální hustoty by měl existovat právě jeden příspěvek lokální hustoty
            // Local density contribution for selected local density object
            // For each local density object should exist only and just one local density contribution
            List<TDLDsity> ldsities =
                SelectedLDsities.Items
                    .Where(a => a.LDsityObjectId == classificationRule.LDsityObject.Id)
                    .Where(a => a.UseNegative == useNegative)
                    .ToList();
            TDLDsity ldsity = ldsities.FirstOrDefault();

            if (!LDsityCountIsValid(ldsities: ldsities))
            {
                return null;
            }

            List<ClassificationRule<TDomain, TCategory>> classificationRulesForAllCategories;
            if (!useNegative)
            {
                classificationRulesForAllCategories =
                    ClassificationRulesPositive
                    .Where(a => a.LDsityObject.Id == classificationRule.LDsityObject.Id)
                    .Where(a => a.LDsityObjectForADSP.Id == classificationRule.LDsityObjectForADSP.Id)
                    .ToList<ClassificationRule<TDomain, TCategory>>();
            }
            else
            {
                classificationRulesForAllCategories =
                    ClassificationRulesNegative
                    .Where(a => a.LDsityObject.Id == classificationRule.LDsityObject.Id)
                    .Where(a => a.LDsityObjectForADSP.Id == classificationRule.LDsityObjectForADSP.Id)
                    .ToList();
            }

            if (!classificationRulesForAllCategories
                .Select(a => a.IsValid).Aggregate((a, b) => a && b))
            {
                MessageBox.Show(
                       text: msgInvalidClassificationRules,
                       caption: String.Empty,
                       buttons: MessageBoxButtons.OK,
                       icon: MessageBoxIcon.Information);
                return null;
            }

            List<string> paramRules =
                classificationRulesForAllCategories
                    .Select(a => a.Text)
                    .ToList();

            List<List<Nullable<int>>> paramADC =
                PrepareParamADC(
                    ldsityObjectForADSP: classificationRule.LDsityObjectForADSP);

            List<List<Nullable<int>>> paramSPC =
                PrepareParamSPC(
                    ldsityObjectForADSP: classificationRule.LDsityObjectForADSP);

            TDFnGetPanelRefYearSetTypeList fnPanelRefYearSets =
                TDFunctions.FnGetPanelRefYearSet.Execute(
                    database: Database,
                    panelRefYearSetId: null);

            DataTable dtClassificationRuleChecks =
                TDFnCheckClassificationRulesTypeList.EmptyDataTable();

            foreach (TDFnGetPanelRefYearSetType fnPanelRefYearSet in fnPanelRefYearSets.Items)
            {
                // fn_check_classification_rules
                dtClassificationRuleChecks.Merge(
                    table: TDFunctions.FnCheckClassificationRules.Execute(
                                database: Database,
                                ldsityId: ldsity.Id,
                                ldsityObjectId: classificationRule.LDsityObjectForADSP.Id,
                                rules: paramRules,
                                panelRefYearSetId: fnPanelRefYearSet.Id,
                                adc: paramADC,
                                spc: paramSPC,
                                useNegative: useNegative)
                            .Data);

                if (Database.Postgres.ExceptionFlag)
                {
                    // Přerušení cyklu v případě chyby v SQL příkazu
                    // Breaks loop in case of error in SQL command
                    break;
                }
            }

            if (Setting.Verbose)
            {
                // Výpis posledního příkazu v cyklu
                MessageBox.Show(
                    text: TDFunctions.FnCheckClassificationRules.CommandText,
                    caption: TDFunctions.FnCheckClassificationRules.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            TDFnCheckClassificationRulesTypeList fnClassificationRuleChecks =
                new(
                    database: Database,
                    data: dtClassificationRuleChecks);

            TDVwClassificationRuleCheckList vwClassificationRuleChecks =
                new(database: Database);

            vwClassificationRuleChecks.ReLoad(
                fnPanelRefYearSet: fnPanelRefYearSets,
                fnClassificationRuleCheck: fnClassificationRuleChecks);

            return vwClassificationRuleChecks;
        }


        /// <summary>
        /// <para lang="cs">Uloží novou plošnou doménu nebo subpopulaci, její kategorie, klasifikační pravidla a hierarchie do databáze</para>
        /// <para lang="en">Writes new area domain or subpopulation, its categories, classification rules and hierarchies into database</para>
        /// </summary>
        private bool SaveRules()
        {
            previousNode = TvwCategory.SelectedNode;
            ValidatePreviousNode();

            // Kategorie plošné domény nebo subpopulace:
            if ((Categories == null) ||
                (Categories.Count == 0))
            {
                // "Není definovaná žádná kategorie plošné domény
                // nebo subpopulace pro uložení do databáze."
                MessageBox.Show(
                    text: msgNoCategoryForSave,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Objekty lokálních hustot pro třídění
            if (LDsityObjectForADSP == null)
            {
                // "Nejsou zvolené žádné objekty lokálních hustot pro třídění."
                MessageBox.Show(
                    text: msgNoLDsityObjectsForADSPForSave,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Vybraná kladná klasifikační pravidla:
            if ((ClassificationRulesPositive == null) ||
                (ClassificationRulesPositive.Count == 0))
            {
                // "Nejsou zvolena žádná kladná klasifikační pravidla pro uložení do databáze."
                MessageBox.Show(
                    text: msgNoClassificationRules,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (!ClassificationRulesPositive
                .Select(a => a.IsValid)
                .Aggregate((a, b) => a && b))
            {
                // "Některé kategorie plošné domény nebo subpopulace
                // mají neplatná kladná klasifikační pravidla."
                MessageBox.Show(
                    text: msgInvalidClassificationRules,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Kontroly kladných klasifikačních pravidel:
            Dictionary<int, List<ClassificationRule<TDomain, TCategory>>> positiveRulesForLDsityObjectForADSP = [];

            Dictionary<int, TDVwClassificationRuleCheckList> positiveRuleChecks = [];

            positiveRulesForLDsityObjectForADSP.Add(
                key: LDsityObjectForADSP.Id,
                value: ClassificationRulesPositive
                    .OrderBy(a => a.Category.Id)
                    .ThenBy(a => a.LDsityObject.Id)
                    .ThenBy(a => a.LDsityObjectForADSP.Id)
                    .Where(a => a.LDsityObjectForADSP.Id == LDsityObjectForADSP.Id)
                    .Distinct(comparer: new ClassificationRuleComparer<TDomain, TCategory>())
                    .ToList());

            positiveRuleChecks.Add(
                key: LDsityObjectForADSP.Id,
                value: GetClassificationRuleCheckList(
                    classificationRule:
                        positiveRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                        .FirstOrDefault(),
                    useNegative: false) ??
                    new TDVwClassificationRuleCheckList(database: Database));

            if (!positiveRuleChecks.Values
                .Select(a => a.Items.Where(b => b.Result ?? false).Any())
                .Aggregate((a, b) => a && b))
            {
                MessageBox.Show(
                    text: msgNoSuccesfullyCategorizedPanels,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Vybraná záporná klasifikační pravidla:
            Dictionary<int, List<ClassificationRule<TDomain, TCategory>>> negativeRulesForLDsityObjectForADSP
                    = [];

            Dictionary<int, TDVwClassificationRuleCheckList> negativeRuleChecks
                = [];

            if (ClassificationType == TDClassificationTypeEnum.ChangeOrDynamic)
            {
                if ((ClassificationRulesNegative == null) ||
                    (ClassificationRulesNegative.Count == 0))
                {
                    // "Nejsou zvolena žádná záporná klasifikační pravidla pro uložení do databáze."
                    MessageBox.Show(
                        text: msgNoClassificationRules,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return false;
                }

                if (!ClassificationRulesNegative
                        .Select(a => a.IsValid)
                        .Aggregate((a, b) => a && b))
                {
                    // "Některé kategorie plošné domény nebo subpopulace
                    // mají neplatná záporná klasifikační pravidla."
                    MessageBox.Show(
                        text: msgInvalidClassificationRules,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return false;
                }

                // Kontroly záporných klasifikačních pravidel
                negativeRulesForLDsityObjectForADSP.Add(
                        key: LDsityObjectForADSP.Id,
                        value: ClassificationRulesNegative
                            .OrderBy(a => a.Category.Id)
                            .ThenBy(a => a.LDsityObject.Id)
                            .ThenBy(a => a.LDsityObjectForADSP.Id)
                            .Where(a => a.LDsityObjectForADSP.Id == LDsityObjectForADSP.Id)
                            .Distinct(comparer: new ClassificationRuleComparer<TDomain, TCategory>())
                            .ToList());

                negativeRuleChecks.Add(
                    key: LDsityObjectForADSP.Id,
                    value: GetClassificationRuleCheckList(
                        classificationRule:
                            negativeRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                            .FirstOrDefault(),
                        useNegative: true) ??
                        new TDVwClassificationRuleCheckList(database: Database));

                if (!negativeRuleChecks.Values
                    .Select(a => a.Items.Where(b => b.Result ?? false).Any())
                    .Aggregate((a, b) => a && b))
                {
                    MessageBox.Show(
                        text: msgNoSuccesfullyCategorizedPanels,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return false;
                }
            }

            int paramLDsityObjectId = LDsityObjectForADSP.Id;
            int paramArealOrPopulationId = (int)ArealOrPopulation;
            int paramClassificationTypeId = (int)ClassificationType;

            // Klasifikační pravidla:

            List<Nullable<int>> paramCategoryIds = [];
            List<string> paramRules = [];
            List<Nullable<bool>> paramUseNegative = [];
            List<List<Nullable<int>>> paramRefYearSetIds = [];

            switch (ClassificationType)
            {
                case TDClassificationTypeEnum.Standard:

                    paramCategoryIds =
                        ClassificationRulesPositive
                           .OrderBy(a => a.Category.Id)
                           .Select(a => (Nullable<int>)a.Category.Id)
                           .ToList<Nullable<int>>();

                    paramRules =
                        positiveRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                            .Select(a => a.Text)
                            .ToList<string>();

                    paramUseNegative =
                        positiveRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                            .Select(a => (Nullable<bool>)false)
                            .ToList<Nullable<bool>>();

                    paramRefYearSetIds.Add(item:
                        positiveRuleChecks[LDsityObjectForADSP.Id].Items
                            .Where(a => a.Result ?? false)
                            .Select(a => (Nullable<int>)a.Id)
                            .ToList<Nullable<int>>());

                    break;

                case TDClassificationTypeEnum.ChangeOrDynamic:

                    paramCategoryIds =
                        ClassificationRulesPositive
                           .OrderBy(a => a.Category.Id)
                           .Select(a => (Nullable<int>)a.Category.Id)
                        .Concat(
                        ClassificationRulesNegative
                           .OrderBy(a => a.Category.Id)
                           .Select(a => (Nullable<int>)a.Category.Id))
                        .ToList<Nullable<int>>();

                    paramRules =
                        positiveRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                            .Select(a => a.Text)
                        .Concat(
                        negativeRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                            .Select(a => a.Text))
                        .ToList();

                    paramUseNegative =
                        positiveRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                            .Select(a => (Nullable<bool>)false)
                        .Concat(
                        negativeRulesForLDsityObjectForADSP[LDsityObjectForADSP.Id]
                            .Select(a => (Nullable<bool>)true))
                        .ToList<Nullable<bool>>();

                    paramRefYearSetIds.Add(item:
                        positiveRuleChecks[LDsityObjectForADSP.Id].Items
                            .Where(a => a.Result ?? false)
                            .Select(a => (Nullable<int>)a.Id)
                            .ToList());

                    paramRefYearSetIds.Add(item:
                        negativeRuleChecks[LDsityObjectForADSP.Id].Items
                            .Where(a => a.Result ?? false)
                            .Select(a => (Nullable<int>)a.Id)
                            .ToList());

                    break;
            }

            // Závislosti:
            // Dependencies:

            List<List<Nullable<int>>> paramADC =
                PrepareParamADC(
                    ldsityObjectForADSP: LDsityObjectForADSP);

            List<List<Nullable<int>>> paramSPC =
                PrepareParamSPC(
                    ldsityObjectForADSP: LDsityObjectForADSP);

            // Uložení klasifikačních pravidel:
            DataTable dtFnSaveRules = null;
            dtFnSaveRules = TDFunctions.FnSaveRules.ExecuteQuery(
                database: Database,
                ldsityObjectId: paramLDsityObjectId,
                arealOrPopulationId: paramArealOrPopulationId,
                classificationTypeId: paramClassificationTypeId,
                categoryIds: paramCategoryIds,
                rules: paramRules,
                useNegative: paramUseNegative,
                panelRefYearSetIds: paramRefYearSetIds,
                adc: paramADC,
                spc: paramSPC
                );

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnSaveRules.CommandText,
                    caption: TDFunctions.FnSaveRules.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            // Znovu načtení nových uložených plošných domén nebo subpopulací, kategorií,
            // klasifikačních pravidel a hierarchií z databáze:
            LoadContent();

            return true;
        }

        /// <summary>
        /// <para lang="cs">Přidává pár pravidel klasifikace do stromové struktury zobrazené pomocí TreeView.</para>
        /// <para lang="en">Adds a classification rule pair to the tree structure displayed using a TreeView. </para>
        /// </summary>
        /// <param name="pair">
        /// <para lang="cs">Pár pravidel klasifikace obsahující doménu a kategorii, které mají být přidány nebo aktualizovány v TreeView.</para>
        /// <para lang="en">The classification rule pair containing the domain and category to be added or updated in the TreeView.</para>
        /// </param>
        public void AddClassificationRulePairToTreeView(ClassificationRulePair<TDomain, TCategory> pair)
        {
            if (pair == null) return;

            TreeNode existingNode = null;

            foreach (TreeNode node in Nodes)
            {
                if (node.Tag is ClassificationRulePair<TDomain, TCategory> existingPair)
                {
                    if (existingPair.Category.Id == pair.Category.Id
                        && existingPair.LDsityObjectForADSP.Id == pair.LDsityObjectForADSP.Id)
                    {
                        existingNode = node;
                        break;
                    }
                }
            }

        }

        #endregion Methods

    }

}