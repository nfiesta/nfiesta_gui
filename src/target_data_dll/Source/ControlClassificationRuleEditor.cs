﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleTargetData
{
    /// <summary>
    /// <para lang="cs">Editor klasifikačních pravidel - třída pro Designer</para>
    /// <para lang="en">Editor for classification rules - class for Designer</para>
    /// </summary>
    internal partial class ControlClassificationRuleEditorDesign
            : UserControl
    {
        protected ControlClassificationRuleEditorDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.GroupBox GrpCategories => grpCategories;
        protected System.Windows.Forms.GroupBox GrpColumns => grpColumns;
        protected System.Windows.Forms.Label LblAND => lblAND;
        protected System.Windows.Forms.Label LblComma => lblComma;
        protected System.Windows.Forms.Label LblEqual => lblEqual;
        protected System.Windows.Forms.Label LblGreater => lblGreater;
        protected System.Windows.Forms.Label LblGreaterOrEqual => lblGreaterOrEqual;
        protected System.Windows.Forms.Label LblIN => lblIN;
        protected System.Windows.Forms.ToolStripStatusLabel LblInfo => lblInfo;
        protected System.Windows.Forms.Label LblLeftBracket => lblLeftBracket;
        protected System.Windows.Forms.Label LblLess => lblLess;
        protected System.Windows.Forms.Label LblLessOrEqual => lblLessOrEqual;
        protected System.Windows.Forms.Label LblNOT => lblNOT;
        protected System.Windows.Forms.Label LblNotEqual => lblNotEqual;
        protected System.Windows.Forms.Label LblOR => lblOR;
        protected System.Windows.Forms.Label LblRightBracket => lblRightBracket;
        protected System.Windows.Forms.RichTextBox RtbClassificationRule => rtbClassificationRule;
        protected System.Windows.Forms.SplitContainer SplCategories => splCategories;
        protected System.Windows.Forms.SplitContainer SplClassificationRule => splClassificationRule;
        protected System.Windows.Forms.SplitContainer SplColumns => splColumns;
        protected System.Windows.Forms.SplitContainer SplOperators => splOperators;
        protected System.Windows.Forms.StatusStrip SsMain => ssMain;
        protected System.Windows.Forms.TableLayoutPanel TlpOperators => tlpOperators;
        protected System.Windows.Forms.TextBox TxtClassificationRule => txtClassificationRule;
    }


    /// <summary>
    /// <para lang="cs">Editor klasifikačních pravidel</para>
    /// <para lang="en">Editor for classification rules</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlClassificationRuleEditor<TDomain, TCategory>
            : ControlClassificationRuleEditorDesign, INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Odsazení vlevo pro položky
        /// (sloupce tabulky nebo kategorie číselníku) v seznamech</para>
        /// <para lang="en">Left padding for items
        /// (data table columns or lookup table categories) in lists</para>
        /// </summary>
        private const int leftPad = 5;

        /// <summary>
        /// <para lang="cs">Odsazení vpravo pro položky
        /// (sloupce tabulky nebo kategorie číselníku) v seznamech</para>
        /// <para lang="en">Right padding for items
        /// (data table columns or lookup table categories) in lists</para>
        /// </summary>
        private const int rightPad = 20;

        /// <summary>
        /// <para lang="cs">Výška položky (sloupce tabulky nebo kategorie číselníku)
        /// v seznamu</para>
        /// <para lang="en">Item height (data table columns or lookup table categories)
        /// in lists</para>
        /// </summary>
        private const int itemHeight = 15;

        /// <summary>
        /// <para lang="cs">Barva pozadí nevybraných položek v seznamu</para>
        /// <para lang="en">BackColor for items in list that are not selected</para>
        /// </summary>
        private static readonly System.Drawing.Color notSelectedItemBackColor
            = System.Drawing.SystemColors.Control;

        /// <summary>
        /// <para lang="cs">Barva písma nevybraných položek v seznamu</para>
        /// <para lang="en">ForeColor for items in list that are not selected</para>
        /// </summary>
        private static readonly System.Drawing.Color notSelectedItemForeColor
            = System.Drawing.SystemColors.WindowText;

        /// <summary>
        /// <para lang="cs">Barva pozadí vybraných položek v seznamu</para>
        /// <para lang="en">BackColor for items in list that are selected</para>
        /// </summary>
        private static readonly System.Drawing.Color selectedItemBackColor
            = System.Drawing.SystemColors.Highlight;

        /// <summary>
        /// <para lang="cs">Barva písma vybraných položek v seznamu</para>
        /// <para lang="en">ForeColor for items in list that are selected</para>
        /// </summary>
        private static readonly System.Drawing.Color selectedItemForeColor
            = System.Drawing.SystemColors.Window;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna textu klasifikačního pravidla"</para>
        /// <para lang="en">Event of the
        /// "Classification rule text change"</para>
        /// </summary>
        public event EventHandler ClassificationRuleTextChanged;

        #endregion Events


        #region Controls

        /// <summary>
        /// <para lang="cs">Panel pro seznam sloupců tabulky</para>
        /// <para lang="en">Panel for list of data table columns</para>
        /// </summary>
        private System.Windows.Forms.Panel pnlColumns;

        /// <summary>
        /// <para lang="cs">Panel pro seznam kategorií číselníku</para>
        /// <para lang="en">Panel for list of lookup table categories</para>
        /// </summary>
        private System.Windows.Forms.Panel pnlCategories;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlClassificationRuleEditor(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlClassificationRuleData<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlClassificationRuleData<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlClassificationRuleData<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlClassificationRuleData<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Základní formulář</para>
        /// <para lang="en">Base form</para>
        /// </summary>
        public IArealOrPopulationForm<TDomain, TCategory> Base
        {
            get
            {
                return ((ControlClassificationRuleData<TDomain, TCategory>)ControlOwner)
                    .Base;
            }
        }

        /// <summary>
        /// <para lang="cs">Pár klasifikačních pravidel pro kladný a záporný příspěvek (read-only)</para>
        /// <para lang="en">Classification rule pair for positive and negative contribution (read-only)</para>
        /// </summary>
        private ClassificationRulePair<TDomain, TCategory> ClassificationRulePair
        {
            get
            {
                return Base.SelectedClassificationRulePair;
            }
        }

        /// <summary>
        /// <para lang="cs">Objekt lokální hustoty pro třídění (read-only)</para>
        /// <para lang="en">Local density object for clasification (read-only)</para>
        /// </summary>
        private TDLDsityObject LDsityObjectForADSP
        {
            get
            {
                if (ClassificationRulePair == null)
                {
                    return null;
                }

                return ClassificationRulePair.LDsityObjectForADSP;
            }
        }

        /// <summary>
        /// <para lang="cs">Kladné (true) nebo záporné (false) klasifikační pravidlo je zobrazeno</para>
        /// <para lang="en">Positive (true) or negative (false) classification rule is displayed</para>
        /// </summary>
        private bool Positive
        {
            get
            {
                return
                    ((ControlClassificationRuleData<TDomain, TCategory>)ControlOwner)
                    .Positive;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategories),  "Kategorie:" },
                            { nameof(GrpColumns),     "Atributy:" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleEditor<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategories),  "Kategorie:" },
                            { nameof(GrpColumns),     "Atributy:" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleEditor<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dicNationalSubPopulation)
                                ? dicNationalSubPopulation
                                : [],

                    _ => [],
                },

                LanguageVersion.International =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategories),  "Categories:" },
                            { nameof(GrpColumns),     "Attributes:" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleEditor<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(GrpCategories),  "Categories:" },
                            { nameof(GrpColumns),     "Attributes:" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleEditor<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BackColor = System.Drawing.SystemColors.Control;

            ControlOwner = controlOwner;

            onEdit = false;

            LoadContent();

            InitializeControls();

            InitializeLabels();

            Reset();

            TxtClassificationRule.TextChanged += new EventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        TextChange();
                    }
                });

            LblIN.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblComma.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblRightBracket.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblLeftBracket.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblLessOrEqual.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblGreaterOrEqual.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblLess.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblGreater.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblNotEqual.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblEqual.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblNOT.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblOR.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
            LblAND.MouseUp += new System.Windows.Forms.MouseEventHandler(Operator_MouseUp);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacích prvků</para>
        /// <para lang="en">Initializing user controls</para>
        /// </summary>
        private void InitializeControls()
        {
            LblInfo.Text = String.Empty;

            SplColumns.Panel1Collapsed = false;
            SplCategories.Panel1Collapsed = true;

            // pnlColumns
            GrpColumns.Controls.Clear();
            pnlColumns = new System.Windows.Forms.Panel()
            {
                BackColor = notSelectedItemBackColor,
                Dock = DockStyle.Fill,
                ForeColor = notSelectedItemForeColor,
                Font = new System.Drawing.Font(
                    familyName: "Courier New",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Bold,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: ((byte)(238))),
                Margin = new Padding(left: 0, top: 0, right: 0, bottom: 0),
                Padding = new Padding(left: 0, top: 0, right: 0, bottom: 0),
                Tag = null
            };
            pnlColumns.Resize += (sender, e) =>
            {
                foreach (Control control in pnlColumns.Controls)
                {
                    control.Width = pnlColumns.Width;
                }
            };
            pnlColumns.MouseUp += (sender, e) =>
            {
                HighlightSelectedItem(
                    panel: pnlColumns,
                    label: null);
                SetInfo(column: null);
                InitializeCategoriesList(
                    fkey: null);
            };
            GrpColumns.Controls.Add(value: pnlColumns);

            // pnlCategories
            GrpCategories.Controls.Clear();
            pnlCategories = new System.Windows.Forms.Panel()
            {
                BackColor = notSelectedItemBackColor,
                Dock = DockStyle.Fill,
                ForeColor = notSelectedItemForeColor,
                Font = new System.Drawing.Font(
                    familyName: "Courier New",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Bold,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: ((byte)(238))),
                Margin = new Padding(left: 0, top: 0, right: 0, bottom: 0),
                Padding = new Padding(left: 0, top: 0, right: 0, bottom: 0),
                Tag = null
            };
            pnlCategories.Resize += (sender, e) =>
            {
                foreach (Control control in pnlCategories.Controls)
                {
                    control.Width = pnlCategories.Width;
                }
            };
            pnlCategories.MouseUp += (sender, e) =>
            {
                HighlightSelectedItem(
                    panel: pnlCategories,
                    label: null);
            };
            GrpCategories.Controls.Add(value: pnlCategories);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing the control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            GrpCategories.Text =
                labels.TryGetValue(key: nameof(GrpCategories),
                    out string grpCategoriesText)
                        ? grpCategoriesText
                        : String.Empty;

            GrpColumns.Text =
                labels.TryGetValue(key: nameof(GrpColumns),
                    out string grpColumnsText)
                        ? grpColumnsText
                        : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu sloupečků tabulky</para>
        /// <para lang="en">Initializing the list of table columns</para>
        /// </summary>
        private void InitializeColumnsList()
        {
            SplCategories.Panel1Collapsed = true;
            pnlColumns.Controls.Clear();

            if (LDsityObjectForADSP == null)
            {
                // Není uveden objekt lokální hustoty pro třídění
                return;
            }

            if (String.IsNullOrEmpty(
                value: LDsityObjectForADSP.TableName))
            {
                // Jméno zdrojové databázové tabulky
                // pro objekt lokální hustoty není uvedeno
                return;
            }

            string[] parts = LDsityObjectForADSP.TableName.Split(
                separator: new char[] { (char)46 },
                options: StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length != 2)
            {
                // Jméno zdrojové databázové tabulky
                // pro objekt lokální hustoty není ve správném formátu
                return;
            }

            DBSchema schema =
                Database.Postgres.Catalog.Schemas.Items
                .Where(a => a.Name == parts[0].Trim())
                .FirstOrDefault();

            if (schema == null)
            {
                // Zvolené schéma v databázi neexistuje
                return;
            }

            DBTable table = schema.Tables.Items
               .Where(a => a.Name == parts[1].Trim())
               .FirstOrDefault();

            if (table == null)
            {
                // Zvolená tabulka v databázi neexistuje
                return;
            }

            int i = (-1);
            foreach (DBColumn column in
                table.Columns.Items
                .OrderBy(a => a.Name))
            {
                System.Windows.Forms.Label label
                    = new()
                    {
                        AutoSize = false,
                        BackColor = notSelectedItemBackColor,
                        ForeColor = notSelectedItemForeColor,
                        Height = itemHeight,
                        Left = 0,
                        Padding = new Padding(
                        left: leftPad, top: 0, right: rightPad, bottom: 0),
                        Tag = column,
                        Text = column.Name,
                        Top = (++i) * itemHeight,
                        Width = pnlColumns.Width
                    };
                label.MouseUp += (sender, e) =>
                {
                    switch (e.Button)
                    {
                        case MouseButtons.Left:
                            HighlightSelectedItem(
                                panel: pnlColumns,
                                label: label);
                            SetInfo(column:
                                (DBColumn)label.Tag);
                            InitializeCategoriesList(
                                fkey: table.ForeignKeys.Items
                                    .Where(a => a.ConstraintColumns.Count == 1)
                                    .Where(a => a.ConstraintColumns[0].Name == ((DBColumn)label.Tag).Name)
                                    .FirstOrDefault());
                            break;

                        default:
                            break;
                    }
                };
                label.MouseDoubleClick += (sender, e) =>
                {
                    InsertToClassificationRule(
                              text: label.Text);
                };
                pnlColumns.Controls.Add(
                    value: label);
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu číselníkových kategorií</para>
        /// <para lang="en">Initializing the list of lookup table categories</para>
        /// </summary>
        /// <param name="fkey">
        /// <para lang="cs">Cizí klíč na číselník</para>
        /// <para lang="en">Foreign key to lookup table</para>
        /// </param>
        private void InitializeCategoriesList(DBForeignKey fkey)
        {
            SplCategories.Panel1Collapsed = true;
            pnlCategories.Controls.Clear();

            if (fkey == null)
            {
                return;
            }

            if (fkey.ForeignTable == null)
            {
                return;
            }

            if (String.IsNullOrEmpty(
                value: fkey.ForeignTable.Name))
            {
                return;
            }

            if (fkey.ForeignTable.Schema == null)
            {
                return;
            }

            if (String.IsNullOrEmpty(
                value: fkey.ForeignTable.Schema.Name))
            {
                return;
            }

            if (fkey.ForeignTable.Columns == null)
            {
                return;
            }

            if (fkey.ForeignTable.Columns.Items == null)
            {
                return;
            }

            if (!fkey.ForeignTable.Columns.Items
                .Where(a => a.Name == "id")
                .Any())
            {
                return;
            }

            if (!fkey.ForeignTable.Columns.Items
                .Where(a => a.Name == "label")
                .Any())
            {
                return;
            }

            SplCategories.Panel1Collapsed = false;

            string sql =
                fkey.ForeignTable.Columns.Items
                .Where(a => a.Name == "label_en")
                .Any()
                ? String.Concat(
                    $"SELECT {Environment.NewLine}",
                    $"    id        AS id,{Environment.NewLine}",
                    $"    label     AS label_cs,{Environment.NewLine}",
                    $"    label_en  AS label_en{Environment.NewLine}",
                    $"FROM{Environment.NewLine}",
                    $"    {fkey.ForeignTable.Schema.Name}.{fkey.ForeignTable.Name}{Environment.NewLine}",
                    $"ORDER BY{Environment.NewLine}",
                    $"    id;")
                : String.Concat(
                    $"SELECT {Environment.NewLine}",
                    $"    id        AS id,{Environment.NewLine}",
                    $"    label     AS label_cs,{Environment.NewLine}",
                    $"    label     AS label_en{Environment.NewLine}",
                    $"FROM{Environment.NewLine}",
                    $"    {fkey.ForeignTable.Schema.Name}.{fkey.ForeignTable.Name}{Environment.NewLine}",
                    $"ORDER BY{Environment.NewLine}",
                    $"    id;");

            DataTable table =
                Database.Postgres.ExecuteQuery(sqlCommand: sql);

            int i = (-1);
            foreach (DataRow row in table.Rows)
            {
                Nullable<int> id = Functions.GetNIntArg(
                    row: row,
                    name: "id",
                    defaultValue: null);

                string text = String.Empty;

                text = LanguageVersion switch
                {
                    LanguageVersion.National =>
                        Functions.GetStringArg(
                            row: row,
                            name: "label_cs",
                            defaultValue: null),
                    LanguageVersion.International =>
                        Functions.GetStringArg(
                            row: row,
                            name: "label_en",
                            defaultValue: null),
                    _ =>
                        Functions.GetStringArg(
                            row: row,
                            name: "label_en",
                            defaultValue: null),
                };

                // Skip records with null identifier
                if (id == null)
                    continue;

                string sid = id.ToString();

                string caption = (text == null) ? sid : $"{sid} - {text}";

                System.Windows.Forms.Label label
                    = new()
                    {
                        AutoSize = false,
                        BackColor = notSelectedItemBackColor,
                        ForeColor = notSelectedItemForeColor,
                        Height = itemHeight,
                        Left = 0,
                        Padding = new Padding(
                            left: leftPad, top: 0, right: rightPad, bottom: 0),
                        Tag = sid,
                        Text = caption,
                        Top = (++i) * itemHeight,
                        Width = pnlCategories.Width
                    };
                label.MouseUp += (sender, e) =>
                {
                    switch (e.Button)
                    {
                        case MouseButtons.Left:
                            HighlightSelectedItem(
                               panel: pnlCategories,
                               label: label);
                            break;

                        default:
                            break;
                    }
                };

                label.MouseDoubleClick += (sender, e) =>
                {
                    InsertToClassificationRule(
                        text: label.Tag.ToString());
                };

                pnlCategories.Controls.Add(
                    value: label);
            }
        }

        /// <summary>
        /// <para lang="cs">Zvýrazní vybranou položku v seznamu</para>
        /// <para lang="en">Highlight selected item in list</para>
        /// </summary>
        /// <param name="panel">
        /// <para lang="cs">Panel seznamu</para>
        /// <para lang="en">Panel for list</para>
        /// </param>
        /// <param name="label">
        /// <para lang="cs">Vybraná položka</para>
        /// <para lang="en">Selected item</para>
        /// </param>
        private static void HighlightSelectedItem(
            System.Windows.Forms.Panel panel,
            System.Windows.Forms.Label label = null)
        {
            panel.Tag = label;

            if (label == null)
            {
                foreach (Label item in panel.Controls)
                {
                    item.BackColor = notSelectedItemBackColor;
                    item.ForeColor = notSelectedItemForeColor;
                }
            }
            else
            {
                foreach (Label item in panel.Controls)
                {
                    item.BackColor = (item.Equals(label)) ?
                        selectedItemBackColor :
                        notSelectedItemBackColor;
                    item.ForeColor = (item.Equals(label)) ?
                        selectedItemForeColor :
                        notSelectedItemForeColor;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vloží text do klasifikačního pravidla</para>
        /// <para lang="en">Insert text into classification rule</para>
        /// </summary>
        /// <param name="text">
        /// <para lang="cs">Text</para>
        /// <para lang="en">Text</para>
        /// </param>
        private void InsertToClassificationRule(string text)
        {
            onEdit = true;
            int index = TxtClassificationRule.SelectionStart;
            string val = $"{text} ";

            System.Text.StringBuilder sb = new(
                value: TxtClassificationRule.Text);
            sb.Insert(index: index, value: val);
            index += val.Length;

            TxtClassificationRule.Text = sb.ToString();

            TxtClassificationRule.Select(start: index, length: 0);
            TxtClassificationRule.Focus();
            TxtClassificationRule.ScrollToCaret();

            RtbClassificationRule.Text = TxtClassificationRule.Text;

            onEdit = false;
            TextChange();
        }

        /// <summary>
        /// <para lang="cs">Zobrazuje informace o vybraném sloupci datové tabulky</para>
        /// <para lang="en">Display informations about selected data table column</para>
        /// </summary>
        /// <param name="column">
        /// <para lang="cs">Vybraný sloupec datové tabulky</para>
        /// <para lang="en">Selected data table column</para>
        /// </param>
        private void SetInfo(DBColumn column)
        {
            LblInfo.Text = String.Empty;

            if (column == null)
            {
                return;
            }

            if (String.IsNullOrEmpty(column.Name))
            {
                return;
            }

            string separator = "    ";

            string notNull =
                (bool)column.NotNull ? "not null" : "null";

            LblInfo.Text = String.Concat(
                $"{column.Name}{separator}",
                $"{column.TypeName}{separator}",
                $"{notNull}");

            if (column.Table == null)
            {
                return;
            }

            if (String.IsNullOrEmpty(value: column.Table.Name))
            {
                return;
            }

            if (column.Table.Schema == null)
            {
                return;
            }

            if (String.IsNullOrEmpty(value: column.Table.Schema.Name))
            {
                return;
            }

            if (
                !(new List<string>()
                { "float4", "float8", "int2", "int4", "int8", "money", "numeric", "oid" })
                .Contains(item: column.TypeName))
            {
                return;
            }

            string sql = String.Concat(
                $"SELECT{Environment.NewLine}",
                $"    min({column.Name}) AS min_val{Environment.NewLine},",
                $"    max({column.Name}) AS max_val{Environment.NewLine}",
                $"FROM{Environment.NewLine}",
                $"    {column.Table.Schema.Name}.{column.Table.Name};");
            DataTable data = Database.Postgres.ExecuteQuery(
                sqlCommand: sql);

            if (data.Rows.Count == 0)
            {
                return;
            }

            Nullable<int> minVal = Functions.GetNIntArg(
                row: data.Rows[0], name: "min_val", defaultValue: null);
            Nullable<int> maxVal = Functions.GetNIntArg(
                row: data.Rows[0], name: "max_val", defaultValue: null);
            string strMinVal = (minVal == null) ? "null" : minVal.ToString();
            string strMaxVal = (maxVal == null) ? "null" : maxVal.ToString();

            LblInfo.Text = String.Concat(
                $"{LblInfo.Text}{separator}",
                $"min: {strMinVal}{separator}",
                $"max: {strMaxVal}");
        }

        /// <summary>
        /// <para lang="cs">Nastavení ovládacího prvku</para>
        /// <para lang="en">Reset control</para>
        /// </summary>
        public void Reset()
        {
            InitializeColumnsList();

            if (!Base.ReadOnlyMode)
            {
                // Umožněna editace

                // Výběr sloupců je dostupný
                SplColumns.Panel1Collapsed = false;
                GrpColumns.Visible = true;
                GrpColumns.Enabled = true;

                // Výběr kategorií je dostupný
                GrpCategories.Visible = true;
                GrpCategories.Enabled = true;

                // Výběr operátorů je dostupný
                SplOperators.Panel2Collapsed = false;
                TlpOperators.Visible = true;
                TlpOperators.Enabled = true;

                // Otevřen text box pro editaci klasifikačního pravidla
                SplClassificationRule.Panel1Collapsed = false;
                TxtClassificationRule.Visible = true;
                TxtClassificationRule.Enabled = true;

                SplClassificationRule.Panel2Collapsed = true;
                RtbClassificationRule.Visible = false;
                RtbClassificationRule.Enabled = false;

                // Status bar
                SsMain.Visible = true;
            }
            else
            {
                // Read Only

                // Výběr sloupců není dostupný
                SplColumns.Panel1Collapsed = true;
                GrpColumns.Visible = false;
                GrpColumns.Enabled = false;

                // Výběr kategorií je dostupný
                GrpCategories.Visible = false;
                GrpCategories.Enabled = false;

                // Výběr operátorů je dostupný
                SplOperators.Panel2Collapsed = true;
                TlpOperators.Visible = false;
                TlpOperators.Enabled = false;

                // Zavřen text box pro editaci klasifikačního pravidla
                SplClassificationRule.Panel1Collapsed = true;
                TxtClassificationRule.Visible = false;
                TxtClassificationRule.Enabled = false;

                SplClassificationRule.Panel2Collapsed = false;
                RtbClassificationRule.Visible = true;
                RtbClassificationRule.Enabled = true;

                // Status bar
                SsMain.Visible = false;
            }

            if (ClassificationRulePair == null)
            {
                onEdit = true;
                TxtClassificationRule.Text = String.Empty;
                RtbClassificationRule.Text = TxtClassificationRule.Text;
                onEdit = false;
                return;
            }

            if (Positive)
            {
                if (ClassificationRulePair.Positive == null)
                {
                    onEdit = true;
                    TxtClassificationRule.Text = String.Empty;
                    RtbClassificationRule.Text = TxtClassificationRule.Text;
                    onEdit = false;
                    return;
                }
                onEdit = true;
                TxtClassificationRule.Text =
                    ClassificationRulePair.Positive.Text ?? String.Empty;
                RtbClassificationRule.Text = TxtClassificationRule.Text;
                // Functions.HighlightSQLSyntax(box: RtbClassificationRule);
                onEdit = false;
            }
            else
            {
                if (ClassificationRulePair.Negative == null)
                {
                    onEdit = true;
                    TxtClassificationRule.Text = String.Empty;
                    RtbClassificationRule.Text = TxtClassificationRule.Text;
                    onEdit = false;
                    return;
                }
                onEdit = true;
                TxtClassificationRule.Text =
                    ClassificationRulePair.Negative.Text ?? String.Empty;
                RtbClassificationRule.Text = TxtClassificationRule.Text;
                // Functions.HighlightSQLSyntax(box: RtbClassificationRule);
                onEdit = false;
            }
        }

        /// <summary>
        /// <para lang="cs">"Změna textu klasifikačního pravidla"</para>
        /// <para lang="en">"Classification rule text change"</para>
        /// </summary>
        private void TextChange()
        {
            if (ClassificationRulePair == null)
            {
                return;
            }

            if (Positive)
            {
                if (ClassificationRulePair.Positive == null)
                {
                    return;
                }
                ClassificationRulePair.Positive.Text
                    = TxtClassificationRule.Text.Trim();
            }
            else
            {
                if (ClassificationRulePair.Negative == null)
                {
                    return;
                }
                ClassificationRulePair.Negative.Text
                    = TxtClassificationRule.Text.Trim();
            }

            ClassificationRuleTextChanged?.Invoke(sender: this, e: new EventArgs());
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události
        /// "Kliknutí na popisek operátoru"</para>
        /// <para lang="en">Handling the event
        /// "Click on operator label"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Label)</para>
        /// <para lang="en">Object that sends the event (Label)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Operator_MouseUp(object sender, MouseEventArgs e)
        {
            if (sender is Label label)
            {
                switch (e.Button)
                {
                    case MouseButtons.Left:

                        InsertToClassificationRule(
                            text: label.Text.ToString());

                        break;

                    default:
                        break;
                }
            }
        }

        #endregion Event Handlers

    }

}