﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Informace o příspěvku lokální hustoty"</para>
    /// <para lang="en">Control "Information about local density contribution"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTDLDsityInformation
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Cílová proměnná</para>
        /// <para lang="en">Target variable</para>
        /// </summary>
        private TDTargetVariable targetVariable;

        /// <summary>
        /// <para lang="cs">Příspěvek lokální hustoty</para>
        /// <para lang="en">Local density contribution</para>
        /// </summary>
        private TDLDsity ldsity;

        private string msgEmptyLookupTable = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">DataGridView "Kategorie plošné domény"</para>
        /// <para lang="en">DataGridView "Area domain categories"</para>
        /// </summary>
        private DataGridView dgvAreaDomainCategory;

        /// <summary>
        /// <para lang="cs">DataGridView "Definiční varianta"</para>
        /// <para lang="en">DataGridView "Definition variant"</para>
        /// </summary>
        private DataGridView dgvDefinitionVariant;

        /// <summary>
        /// <para lang="cs">DataGridView "Kategorie subpopulace"</para>
        /// <para lang="en">DataGridView "Subpopulation categories"</para>
        /// </summary>
        private DataGridView dgvSubPopulationCategory;

        /// <summary>
        /// <para lang="cs">DataGridView "Jednotka měření"</para>
        /// <para lang="en">DataGridView "Unit of measure"</para>
        /// </summary>
        private DataGridView dgvUnitOfMeasure;

        /// <summary>
        /// <para lang="cs">DataGridView "Verze"</para>
        /// <para lang="en">DataGridView "Versions"</para>
        /// </summary>
        private DataGridView dgvVersion;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTDLDsityInformation(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Cílová proměnná</para>
        /// <para lang="en">Target variable</para>
        /// </summary>
        private TDTargetVariable TargetVariable
        {
            get
            {
                return targetVariable;
            }
            set
            {
                targetVariable = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Příspěvek lokální hustoty</para>
        /// <para lang="en">Local density contribution</para>
        /// </summary>
        private TDLDsity LDsity
        {
            get
            {
                return ldsity;
            }
            set
            {
                ldsity = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnAreaDomainCategoryEdit),      "Editovat kategorii plošné domény" },
                        { nameof(btnDefinitionVariantEdit),       "Editovat definiční variantu" },
                        { nameof(btnSubPopulationCategoryEdit),   "Editovat kategorii subpopulace" },
                        { nameof(btnUnitOfMeasureEdit),           "Editovat jednotku" },
                        { nameof(btnVersionEdit),                 "Editovat verzi" },
                        { nameof(grpAreaDomainCategory),          "Kategorie plošné domény:" },
                        { nameof(grpDefinitionVariant),           "Definiční varianta:" },
                        { nameof(grpSubPopulationCategory),       "Kategorie subpopulace:" },
                        { nameof(grpUnitOfMeasure),               "Jednotka:" },
                        { nameof(grpVersion),                     "Verze:" },
                        { nameof(tsrAreaDomainCategory),          String.Empty },
                        { nameof(tsrDefinitionVariant),           String.Empty },
                        { nameof(tsrSubPopulationCategory),       String.Empty },
                        { nameof(tsrUnitOfMeasure),               String.Empty },
                        { nameof(tsrVersion),                     String.Empty },
                        { nameof(msgEmptyLookupTable),            "Tabulka je prázdná." },

                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColId.Name}",
                            "ID" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelCs.Name}",
                            "Zkratka" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelCs.Name}",
                            "Zkratka [cs]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionCs.Name}",
                            "Popis" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionCs.Name}",
                            "Popis [cs]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelEn.Name}",
                            "Zkratka" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelEn.Name}",
                            "Zkratka [en]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionEn.Name}",
                            "Popis" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionEn.Name}",
                            "Popis [en]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityObjectGroupId.Name}",
                            "ID skupiny objektů lokálních hustot" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityObjectGroupId.Name}",
                            "ID skupiny objektů lokálních hustot" },

                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColId.Name}",
                            "ID" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelCs.Name}",
                            "Zkratka" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelCs.Name}",
                            "Zkratka [cs]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionCs.Name}",
                            "Popis" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionCs.Name}",
                            "Popis [cs]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelEn.Name}",
                            "Zkratka" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelEn.Name}",
                            "Zkratka [en]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionEn.Name}",
                            "Popis" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionEn.Name}",
                            "Popis [en]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },

                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColId.Name}",
                            "ID" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelCs.Name}",
                            "Zkratka" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelCs.Name}",
                            "Zkratka [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionCs.Name}",
                            "Popis" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionCs.Name}",
                            "Popis [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColAreaDomainId.Name}",
                            "ID plošné domény" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColAreaDomainId.Name}",
                            "ID plošné domény" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelEn.Name}",
                            "Zkratka" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelEn.Name}",
                            "Zkratka [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionEn.Name}",
                            "Popis" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionEn.Name}",
                            "Popis [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColTargetVariableId.Name}",
                            "ID cílové proměnné" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColTargetVariableId.Name}",
                            "ID cílové proměnné" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectId.Name}",
                            "ID objektu lokální hustoty" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectId.Name}",
                            "ID objektu lokální hustoty" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Zkratka objektu lokální hustoty" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Zkratka objektu lokální hustoty [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Popis objektu lokální hustoty" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Popis objektu lokální hustoty [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Zkratka objektu lokální hustoty" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Zkratka objektu lokální hustoty [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Popis objektu lokální hustoty" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Popis objektu lokální hustoty [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColUseNegative.Name}",
                            "Záporný příspěvek lokální hustoty" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColUseNegative.Name}",
                            "Záporný příspěvek lokální hustoty" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColRestriction.Name}",
                            "Definováno uživatelem" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColRestriction.Name}",
                            "Definováno uživatelem" },

                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColId.Name}",
                            "ID" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelCs.Name}",
                            "Zkratka" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelCs.Name}",
                            "Zkratka [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionCs.Name}",
                            "Popis" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionCs.Name}",
                            "Popis [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColSubPopulationId.Name}",
                            "ID subpopulace" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColSubPopulationId.Name}",
                            "ID subpopulace" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelEn.Name}",
                            "Zkratka" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelEn.Name}",
                            "Zkratka [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionEn.Name}",
                            "Popis" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionEn.Name}",
                            "Popis [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColTargetVariableId.Name}",
                            "ID cílové proměnné" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColTargetVariableId.Name}",
                            "ID cílové proměnné" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectId.Name}",
                            "ID objektu lokální hustoty" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectId.Name}",
                            "ID objektu lokální hustoty" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Zkratka objektu lokální hustoty" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Zkratka objektu lokální hustoty [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Popis objektu lokální hustoty" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Popis objektu lokální hustoty [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Zkratka objektu lokální hustoty" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Zkratka objektu lokální hustoty [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Popis objektu lokální hustoty" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Popis objektu lokální hustoty [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColUseNegative.Name}",
                            "Záporný příspěvek lokální hustoty" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColUseNegative.Name}",
                            "Záporný příspěvek lokální hustoty" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColRestriction.Name}",
                            "Definováno uživatelem" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColRestriction.Name}",
                            "Definováno uživatelem" },

                        { $"col-{TDVersionList.Name}.{TDVersionList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColId.Name}",
                            "ID" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColLabelCs.Name}",
                            "Zkratka" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColLabelCs.Name}",
                            "Zkratka [cs]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColDescriptionCs.Name}",
                            "Popis" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColDescriptionCs.Name}",
                            "Popis [cs]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColLabelEn.Name}",
                            "Zkratka" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColLabelEn.Name}",
                            "Zkratka [en]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColDescriptionEn.Name}",
                            "Popis" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColDescriptionEn.Name}",
                            "Popis [en]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColTargetVariableId.Name}",
                            "ID cílové proměnné" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColTargetVariableId.Name}",
                            "ID cílové proměnné" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColLDsityId.Name}",
                            "ID příspěvku lokální hustoty" },
                            { $"col-{TDVersionList.Name}.{TDVersionList.ColUseNegative.Name}",
                            "Záporný příspěvek lokální hustoty"  },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColUseNegative.Name}",
                            "Záporný příspěvek lokální hustoty"  }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDLDsityInformation),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnAreaDomainCategoryEdit),      "Edit area domain category" },
                        { nameof(btnDefinitionVariantEdit),       "Edit definition variant" },
                        { nameof(btnSubPopulationCategoryEdit),   "Edit subpopulation category" },
                        { nameof(btnUnitOfMeasureEdit),           "Edit unit of measure" },
                        { nameof(btnVersionEdit),                 "Edit version" },
                        { nameof(grpAreaDomainCategory),          "Area domain category:" },
                        { nameof(grpDefinitionVariant),           "Definition variant:" },
                        { nameof(grpSubPopulationCategory),       "Subpopulation category:" },
                        { nameof(grpUnitOfMeasure),               "Unit of measure:" },
                        { nameof(grpVersion),                     "Version:" },
                        { nameof(tsrAreaDomainCategory),          String.Empty },
                        { nameof(tsrDefinitionVariant),           String.Empty },
                        { nameof(tsrSubPopulationCategory),       String.Empty },
                        { nameof(tsrUnitOfMeasure),               String.Empty },
                        { nameof(tsrVersion),                     String.Empty },
                        { nameof(msgEmptyLookupTable),            "Table is empty." },

                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColId.Name}",
                            "ID" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelCs.Name}",
                            "Label" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelCs.Name}",
                            "Label [cs]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionCs.Name}",
                            "Description" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionCs.Name}",
                            "Description [cs]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelEn.Name}",
                            "Label" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLabelEn.Name}",
                            "Label [en]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionEn.Name}",
                            "Description" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColDescriptionEn.Name}",
                            "Description [en]" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"col-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityObjectGroupId.Name}",
                            "Local density object group ID" },
                        { $"tip-{TDUnitOfMeasureList.Name}.{TDUnitOfMeasureList.ColLDsityObjectGroupId.Name}",
                            "Local density object group ID" },

                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColId.Name}",
                            "ID" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelCs.Name}",
                            "Label" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelCs.Name}",
                            "Label [cs]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionCs.Name}",
                            "Description" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionCs.Name}",
                            "Description [cs]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelEn.Name}",
                            "Label" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLabelEn.Name}",
                            "Label [en]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionEn.Name}",
                            "Description" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColDescriptionEn.Name}",
                            "Description [en]" },
                        { $"col-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"tip-{TDDefinitionVariantList.Name}.{TDDefinitionVariantList.ColLDsityId.Name}",
                            "Local density contribution ID" },

                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColId.Name}",
                            "ID" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelCs.Name}",
                            "Label" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelCs.Name}",
                            "Label [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionCs.Name}",
                            "Description" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionCs.Name}",
                            "Description [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColAreaDomainId.Name}",
                            "Area domain ID" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColAreaDomainId.Name}",
                            "Area domain ID" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelEn.Name}",
                            "Label" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLabelEn.Name}",
                            "Label [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionEn.Name}",
                            "Description" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColDescriptionEn.Name}",
                            "Description [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColTargetVariableId.Name}",
                            "Target variable ID" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColTargetVariableId.Name}",
                            "Target variable ID" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectId.Name}",
                            "Local density object ID" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectId.Name}",
                            "Local density object ID" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Local density object label" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Local density object label [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Local density object description" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Local density object description [cs]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Local density object label" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Local density object label [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Local density object description" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Local density object description [en]" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColUseNegative.Name}",
                            "Negative local density contribution" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColUseNegative.Name}",
                            "Negative local density contribution" },
                        { $"col-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColRestriction.Name}",
                            "User defined" },
                        { $"tip-{TDAreaDomainCategoryList.Name}.{TDAreaDomainCategoryList.ColRestriction.Name}",
                            "User defined" },

                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColId.Name}",
                            "ID" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelCs.Name}",
                            "Label" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelCs.Name}",
                            "Label [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionCs.Name}",
                            "Description" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionCs.Name}",
                            "Description [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColSubPopulationId.Name}",
                            "Subpopulation ID" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColSubPopulationId.Name}",
                            "Subpopulation ID" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelEn.Name}",
                            "Label" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLabelEn.Name}",
                            "Label [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionEn.Name}",
                            "Description" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColDescriptionEn.Name}",
                            "Description [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColTargetVariableId.Name}",
                            "Target variable ID" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColTargetVariableId.Name}",
                            "Target variable ID" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectId.Name}",
                            "Local density object ID" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectId.Name}",
                            "Local density object ID" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Local density object label" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelCs.Name}",
                            "Local density object label [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Local density object description" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.Name}",
                            "Local density object description [cs]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Local density object label" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectLabelEn.Name}",
                            "Local density object label [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Local density object description" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.Name}",
                            "Local density object description [en]" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColUseNegative.Name}",
                            "Negative local density contribution" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColUseNegative.Name}",
                            "Negative local density contribution" },
                        { $"col-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColRestriction.Name}",
                            "User defined" },
                        { $"tip-{TDSubPopulationCategoryList.Name}.{TDSubPopulationCategoryList.ColRestriction.Name}",
                            "User defined"},

                        { $"col-{TDVersionList.Name}.{TDVersionList.ColId.Name}",
                            "ID" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColId.Name}",
                            "ID" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColLabelCs.Name}",
                            "Label" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColLabelCs.Name}",
                            "Label [cs]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColDescriptionCs.Name}",
                            "Description" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColDescriptionCs.Name}",
                            "Description [cs]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColLabelEn.Name}",
                            "Label" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColLabelEn.Name}",
                            "Label [en]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColDescriptionEn.Name}",
                            "Description" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColDescriptionEn.Name}",
                            "Description [en]" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColTargetVariableId.Name}",
                            "Target variable ID" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColTargetVariableId.Name}",
                            "Target variable ID" },
                        { $"col-{TDVersionList.Name}.{TDVersionList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColLDsityId.Name}",
                            "Local density contribution ID" },
                            { $"col-{TDVersionList.Name}.{TDVersionList.ColUseNegative.Name}",
                            "Negative local density contribution"  },
                        { $"tip-{TDVersionList.Name}.{TDVersionList.ColUseNegative.Name}",
                            "Negative local density contribution"  },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDLDsityInformation),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            TargetVariable = null;

            LDsity = null;

            InitializeControls();

            InitializeLabels();

            BackColor = pnlMain.BackColor;

            btnAreaDomainCategoryEdit.Click += new EventHandler(
                (sender, e) => { UpdateAreaDomainCategory(); });

            btnDefinitionVariantEdit.Click += new EventHandler(
                (sender, e) => { UpdateDefinitionVariant(); });

            btnSubPopulationCategoryEdit.Click += new EventHandler(
                (sender, e) => { UpdateSubPopulationCategory(); });

            btnUnitOfMeasureEdit.Click += new EventHandler(
                (sender, e) => { UpdateUnitOfMeasure(); });

            btnVersionEdit.Click += new EventHandler(
                (sender, e) => { UpdateVersion(); });

            pnlMain.Resize += new EventHandler(
                (sender, e) => { SetControlDesign(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        private void InitializeControls()
        {
            pnlUnitOfMeasure.Controls.Clear();
            dgvUnitOfMeasure = new DataGridView()
            {
                Dock = DockStyle.Fill,
                Visible = false
            };
            dgvUnitOfMeasure.SelectionChanged +=
                (sender, e) => { dgvUnitOfMeasure.ClearSelection(); };
            pnlUnitOfMeasure.Controls.Add(
                value: dgvUnitOfMeasure);

            pnlDefinitionVariant.Controls.Clear();
            dgvDefinitionVariant = new DataGridView()
            {
                Dock = DockStyle.Fill,
                Visible = false
            };
            dgvDefinitionVariant.SelectionChanged +=
                (sender, e) => { dgvDefinitionVariant.ClearSelection(); };
            pnlDefinitionVariant.Controls.Add(
                value: dgvDefinitionVariant);

            pnlAreaDomainCategory.Controls.Clear();
            dgvAreaDomainCategory = new DataGridView()
            {
                Dock = DockStyle.Fill,
                Visible = false
            };
            dgvAreaDomainCategory.SelectionChanged +=
                (sender, e) => { dgvAreaDomainCategory.ClearSelection(); };
            pnlAreaDomainCategory.Controls.Add(
                value: dgvAreaDomainCategory);

            pnlSubPopulationCategory.Controls.Clear();
            dgvSubPopulationCategory = new DataGridView()
            {
                Dock = DockStyle.Fill,
                Visible = false
            };
            dgvSubPopulationCategory.SelectionChanged +=
                (sender, e) => { dgvSubPopulationCategory.ClearSelection(); };
            pnlSubPopulationCategory.Controls.Add(
                value: dgvSubPopulationCategory);

            pnlVersion.Controls.Clear();
            dgvVersion = new DataGridView()
            {
                Dock = DockStyle.Fill,
                Visible = false
            };
            dgvVersion.SelectionChanged +=
                (sender, e) => { dgvVersion.ClearSelection(); };
            pnlVersion.Controls.Add(
                value: dgvVersion);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            #region Dictionary

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsCs = Dictionary(
                languageVersion: LanguageVersion.National,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsEn = Dictionary(
                languageVersion: LanguageVersion.International,
                languageFile: LanguageFile);

            #endregion Dictionary

            #region Controls

            btnAreaDomainCategoryEdit.Text =
                labels.TryGetValue(key: nameof(btnAreaDomainCategoryEdit),
                    out string btnAreaDomainCategoryEditText)
                        ? btnAreaDomainCategoryEditText
                        : String.Empty;

            btnDefinitionVariantEdit.Text =
                labels.TryGetValue(key: nameof(btnDefinitionVariantEdit),
                    out string btnDefinitionVariantEditText)
                        ? btnDefinitionVariantEditText
                        : String.Empty;

            btnSubPopulationCategoryEdit.Text =
                labels.TryGetValue(key: nameof(btnSubPopulationCategoryEdit),
                    out string btnSubPopulationCategoryEditText)
                        ? btnSubPopulationCategoryEditText
                        : String.Empty;

            btnUnitOfMeasureEdit.Text =
                labels.TryGetValue(key: nameof(btnUnitOfMeasureEdit),
                    out string btnUnitOfMeasureEditText)
                        ? btnUnitOfMeasureEditText
                        : String.Empty;

            btnVersionEdit.Text =
                labels.TryGetValue(key: nameof(btnVersionEdit),
                    out string btnVersionEditText)
                        ? btnVersionEditText
                        : String.Empty;

            grpAreaDomainCategory.Text =
                labels.TryGetValue(key: nameof(grpAreaDomainCategory),
                    out string grpAreaDomainCategoryText)
                        ? grpAreaDomainCategoryText
                        : String.Empty;

            grpDefinitionVariant.Text =
                labels.TryGetValue(key: nameof(grpDefinitionVariant),
                    out string grpDefinitionVariantText)
                        ? grpDefinitionVariantText
                        : String.Empty;

            grpSubPopulationCategory.Text =
                labels.TryGetValue(key: nameof(grpSubPopulationCategory),
                    out string grpSubPopulationCategoryText)
                        ? grpSubPopulationCategoryText
                        : String.Empty;

            grpUnitOfMeasure.Text =
                labels.TryGetValue(key: nameof(grpUnitOfMeasure),
                    out string grpUnitOfMeasureText)
                        ? grpUnitOfMeasureText
                        : String.Empty;

            grpVersion.Text =
                labels.TryGetValue(key: nameof(grpVersion),
                    out string grpVersionText)
                        ? grpVersionText
                        : String.Empty;

            tsrAreaDomainCategory.Text =
                labels.TryGetValue(key: nameof(tsrAreaDomainCategory),
                    out string tsrAreaDomainCategoryText)
                        ? tsrAreaDomainCategoryText
                        : String.Empty;

            tsrDefinitionVariant.Text =
                labels.TryGetValue(key: nameof(tsrDefinitionVariant),
                    out string tsrDefinitionVariantText)
                        ? tsrDefinitionVariantText
                        : String.Empty;

            tsrSubPopulationCategory.Text =
                labels.TryGetValue(key: nameof(tsrSubPopulationCategory),
                    out string tsrSubPopulationCategoryText)
                        ? tsrSubPopulationCategoryText
                        : String.Empty;

            tsrUnitOfMeasure.Text =
                labels.TryGetValue(key: nameof(tsrUnitOfMeasure),
                    out string tsrUnitOfMeasureText)
                        ? tsrUnitOfMeasureText
                        : String.Empty;

            tsrVersion.Text =
                labels.TryGetValue(key: nameof(tsrVersion),
                    out string tsrVersionText)
                        ? tsrVersionText
                        : String.Empty;

            msgEmptyLookupTable =
                labels.TryGetValue(key: nameof(msgEmptyLookupTable),
                    out msgEmptyLookupTable)
                        ? msgEmptyLookupTable
                        : String.Empty;

            #endregion Controls

            #region Visible Columns

            switch (LanguageVersion)
            {
                case LanguageVersion.National:

                    TDUnitOfMeasureList.SetColumnsVisibility(
                        TDUnitOfMeasureList.ColId.Name,
                        TDUnitOfMeasureList.ColLabelCs.Name,
                        TDUnitOfMeasureList.ColDescriptionCs.Name);

                    TDDefinitionVariantList.SetColumnsVisibility(
                        TDDefinitionVariantList.ColId.Name,
                        TDDefinitionVariantList.ColLabelCs.Name,
                        TDDefinitionVariantList.ColDescriptionCs.Name);

                    TDAreaDomainCategoryList.SetColumnsVisibility(
                        TDAreaDomainCategoryList.ColId.Name,
                        TDAreaDomainCategoryList.ColLabelCs.Name,
                        TDAreaDomainCategoryList.ColDescriptionCs.Name,
                        TDAreaDomainCategoryList.ColLDsityObjectLabelCs.Name,
                        TDAreaDomainCategoryList.ColRestriction.Name);

                    TDSubPopulationCategoryList.SetColumnsVisibility(
                        TDSubPopulationCategoryList.ColId.Name,
                        TDSubPopulationCategoryList.ColLabelCs.Name,
                        TDSubPopulationCategoryList.ColDescriptionCs.Name,
                        TDSubPopulationCategoryList.ColLDsityObjectLabelCs.Name,
                        TDSubPopulationCategoryList.ColRestriction.Name);

                    TDVersionList.SetColumnsVisibility(
                        TDVersionList.ColId.Name,
                        TDVersionList.ColLabelCs.Name,
                        TDVersionList.ColDescriptionCs.Name);

                    break;

                case LanguageVersion.International:

                    TDUnitOfMeasureList.SetColumnsVisibility(
                        TDUnitOfMeasureList.ColId.Name,
                        TDUnitOfMeasureList.ColLabelEn.Name,
                        TDUnitOfMeasureList.ColDescriptionEn.Name);

                    TDDefinitionVariantList.SetColumnsVisibility(
                        TDDefinitionVariantList.ColId.Name,
                        TDDefinitionVariantList.ColLabelEn.Name,
                        TDDefinitionVariantList.ColDescriptionEn.Name);

                    TDAreaDomainCategoryList.SetColumnsVisibility(
                        TDAreaDomainCategoryList.ColId.Name,
                        TDAreaDomainCategoryList.ColLabelEn.Name,
                        TDAreaDomainCategoryList.ColDescriptionEn.Name,
                        TDAreaDomainCategoryList.ColLDsityObjectLabelEn.Name,
                        TDAreaDomainCategoryList.ColRestriction.Name);

                    TDSubPopulationCategoryList.SetColumnsVisibility(
                        TDSubPopulationCategoryList.ColId.Name,
                        TDSubPopulationCategoryList.ColLabelEn.Name,
                        TDSubPopulationCategoryList.ColDescriptionEn.Name,
                        TDSubPopulationCategoryList.ColLDsityObjectLabelEn.Name,
                        TDSubPopulationCategoryList.ColRestriction.Name);

                    TDVersionList.SetColumnsVisibility(
                        TDVersionList.ColId.Name,
                        TDVersionList.ColLabelEn.Name,
                        TDVersionList.ColDescriptionEn.Name);

                    break;

                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(LanguageVersion)} unknown value.",
                        paramName: nameof(LanguageVersion));
            }

            #endregion Visible Columns

            #region TDUnitOfMeasureList

            foreach (ColumnMetadata column in TDUnitOfMeasureList.Cols.Values)
            {
                TDUnitOfMeasureList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{TDUnitOfMeasureList.Name}.{column.Name}") ?
                    labelsCs[$"col-{TDUnitOfMeasureList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{TDUnitOfMeasureList.Name}.{column.Name}") ?
                    labelsEn[$"col-{TDUnitOfMeasureList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{TDUnitOfMeasureList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{TDUnitOfMeasureList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{TDUnitOfMeasureList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{TDUnitOfMeasureList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion TDUnitOfMeasureList

            #region TDDefinitionVariantList

            foreach (ColumnMetadata column in TDDefinitionVariantList.Cols.Values)
            {
                TDDefinitionVariantList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{TDDefinitionVariantList.Name}.{column.Name}") ?
                    labelsCs[$"col-{TDDefinitionVariantList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{TDDefinitionVariantList.Name}.{column.Name}") ?
                    labelsEn[$"col-{TDDefinitionVariantList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{TDDefinitionVariantList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{TDDefinitionVariantList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{TDDefinitionVariantList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{TDDefinitionVariantList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion TDDefinitionVariantList

            #region TDAreaDomainCategoryList

            foreach (ColumnMetadata column in TDAreaDomainCategoryList.Cols.Values)
            {
                TDAreaDomainCategoryList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{TDAreaDomainCategoryList.Name}.{column.Name}") ?
                    labelsCs[$"col-{TDAreaDomainCategoryList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{TDAreaDomainCategoryList.Name}.{column.Name}") ?
                    labelsEn[$"col-{TDAreaDomainCategoryList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{TDAreaDomainCategoryList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{TDAreaDomainCategoryList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{TDAreaDomainCategoryList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{TDAreaDomainCategoryList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion TDAreaDomainCategoryList

            #region TDSubPopulationCategoryList

            foreach (ColumnMetadata column in TDSubPopulationCategoryList.Cols.Values)
            {
                TDSubPopulationCategoryList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{TDSubPopulationCategoryList.Name}.{column.Name}") ?
                    labelsCs[$"col-{TDSubPopulationCategoryList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{TDSubPopulationCategoryList.Name}.{column.Name}") ?
                    labelsEn[$"col-{TDSubPopulationCategoryList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{TDSubPopulationCategoryList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{TDSubPopulationCategoryList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{TDSubPopulationCategoryList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{TDSubPopulationCategoryList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion TDSubPopulationCategoryList

            #region TDVersionList

            foreach (ColumnMetadata column in TDVersionList.Cols.Values)
            {
                TDVersionList.SetColumnHeader(
                columnName:
                    column.Name,
                headerTextCs:
                    labelsCs.ContainsKey(key: $"col-{TDVersionList.Name}.{column.Name}") ?
                    labelsCs[$"col-{TDVersionList.Name}.{column.Name}"] : String.Empty,
                headerTextEn:
                    labelsEn.ContainsKey(key: $"col-{TDVersionList.Name}.{column.Name}") ?
                    labelsEn[$"col-{TDVersionList.Name}.{column.Name}"] : String.Empty,
                toolTipTextCs:
                    labelsCs.ContainsKey(key: $"tip-{TDVersionList.Name}.{column.Name}") ?
                    labelsCs[$"tip-{TDVersionList.Name}.{column.Name}"] : String.Empty,
                toolTipTextEn:
                    labelsEn.ContainsKey(key: $"tip-{TDVersionList.Name}.{column.Name}") ?
                    labelsEn[$"tip-{TDVersionList.Name}.{column.Name}"] : String.Empty);
            }

            #endregion TDVersionList

        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazí informace o příspěvku lokální hustoty</para>
        /// <para lang="en">Show information about local density contribution</para>
        /// </summary>
        /// <param name="targetVariable">
        /// <para lang="cs">Cílová proměnná</para>
        /// <para lang="en">Target variable</para>
        /// </param>
        /// <param name="ldsity">
        /// <para lang="cs">Příspěvek lokální hustoty</para>
        /// <para lang="en">Local density contribution</para>
        /// </param>
        public void ShowInformation(
            TDTargetVariable targetVariable,
            TDLDsity ldsity)
        {
            TargetVariable = targetVariable;
            LDsity = ldsity;

            if (TargetVariable == null)
            {
                grpAreaDomainCategory.Visible = false;
                grpDefinitionVariant.Visible = false;
                grpSubPopulationCategory.Visible = false;
                grpUnitOfMeasure.Visible = false;
                grpVersion.Visible = false;
                dgvAreaDomainCategory.Visible = false;
                dgvDefinitionVariant.Visible = false;
                dgvSubPopulationCategory.Visible = false;
                dgvUnitOfMeasure.Visible = false;
                dgvVersion.Visible = false;
                return;
            }

            if (LDsity == null)
            {
                grpAreaDomainCategory.Visible = false;
                grpDefinitionVariant.Visible = false;
                grpSubPopulationCategory.Visible = false;
                grpUnitOfMeasure.Visible = false;
                grpVersion.Visible = false;
                dgvAreaDomainCategory.Visible = false;
                dgvDefinitionVariant.Visible = false;
                dgvSubPopulationCategory.Visible = false;
                dgvUnitOfMeasure.Visible = false;
                dgvVersion.Visible = false;
                return;
            }

            grpAreaDomainCategory.Visible = true;
            dgvAreaDomainCategory.Visible = true;
            TDAreaDomainCategoryList fnAreaDomainCategory =
                TDFunctions.FnGetAreaDomainCategory.Execute(
                    database: Database,
                    targetVariableId: TargetVariable.Id,
                    ldsityId: LDsity.Id,
                    useNegative: LDsity.UseNegative);
            fnAreaDomainCategory = new TDAreaDomainCategoryList(
                database: Database,
                rows: fnAreaDomainCategory.Data.AsEnumerable()
                    .Where(a => a.Field<Nullable<int>>(columnName: TDAreaDomainCategoryList.ColId.Name) != null));
            fnAreaDomainCategory.SetDataGridViewDataSource(
                dataGridView: dgvAreaDomainCategory);
            fnAreaDomainCategory.FormatDataGridView(
                dataGridView: dgvAreaDomainCategory);
            dgvAreaDomainCategory.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            dgvAreaDomainCategory.ColumnHeadersVisible
                = dgvAreaDomainCategory.Rows.Count > 0;

            grpDefinitionVariant.Visible = true;
            dgvDefinitionVariant.Visible = true;
            TDDefinitionVariantList fnDefinitionVariant =
                TDFunctions.FnGetDefinitionVariantForLDsity.Execute(
                    database: Database,
                    ldsityId: LDsity.Id);
            fnDefinitionVariant = new TDDefinitionVariantList(
                database: Database,
                rows: fnDefinitionVariant.Data.AsEnumerable()
                    .Where(a => a.Field<Nullable<int>>(columnName: TDDefinitionVariantList.ColId.Name) != null));
            fnDefinitionVariant.SetDataGridViewDataSource(
                dataGridView: dgvDefinitionVariant);
            fnDefinitionVariant.FormatDataGridView(
                dataGridView: dgvDefinitionVariant);
            dgvDefinitionVariant.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            dgvDefinitionVariant.ColumnHeadersVisible
                = dgvDefinitionVariant.Rows.Count > 0;

            grpSubPopulationCategory.Visible = true;
            dgvSubPopulationCategory.Visible = true;
            TDSubPopulationCategoryList fnSubPopulationCategory =
                TDFunctions.FnGetSubPopulationCategory.Execute(
                    database: Database,
                    targetVariableId: TargetVariable.Id,
                    ldsityId: LDsity.Id,
                    useNegative: LDsity.UseNegative);
            fnSubPopulationCategory = new TDSubPopulationCategoryList(
                database: Database,
                rows: fnSubPopulationCategory.Data.AsEnumerable()
                     .Where(a => a.Field<Nullable<int>>(columnName: TDSubPopulationCategoryList.ColId.Name) != null));
            fnSubPopulationCategory.SetDataGridViewDataSource(
                dataGridView: dgvSubPopulationCategory);
            fnSubPopulationCategory.FormatDataGridView(
                dataGridView: dgvSubPopulationCategory);
            dgvSubPopulationCategory.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            dgvSubPopulationCategory.ColumnHeadersVisible
                = dgvSubPopulationCategory.Rows.Count > 0;

            grpUnitOfMeasure.Visible = true;
            dgvUnitOfMeasure.Visible = true;
            TDUnitOfMeasureList fnUnitOfMeasure =
               TDFunctions.FnGetUnitOfMeasure.Execute(
                    database: Database,
                    ldsityId: LDsity.Id);
            fnUnitOfMeasure = new TDUnitOfMeasureList(
                database: Database,
                rows: fnUnitOfMeasure.Data.AsEnumerable()
                    .Where(a => a.Field<Nullable<int>>(columnName: TDUnitOfMeasureList.ColId.Name) != null));
            fnUnitOfMeasure.SetDataGridViewDataSource(
                dataGridView: dgvUnitOfMeasure);
            fnUnitOfMeasure.FormatDataGridView(
                dataGridView: dgvUnitOfMeasure);
            dgvUnitOfMeasure.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            dgvUnitOfMeasure.ColumnHeadersVisible
                = dgvUnitOfMeasure.Rows.Count > 0;

            grpVersion.Visible = true;
            dgvVersion.Visible = true;
            TDVersionList fnVersion =
                TDFunctions.FnGetVersion.Execute(
                    database: Database,
                    targetVariableId: TargetVariable.Id,
                    ldsityId: LDsity.Id,
                    useNegative: LDsity.UseNegative);
            fnVersion = new TDVersionList(
                database: Database,
                rows: fnVersion.Data.AsEnumerable()
                 .Where(a => a.Field<Nullable<int>>(columnName: TDVersionList.ColId.Name) != null));
            fnVersion.SetDataGridViewDataSource(
                dataGridView: dgvVersion);
            fnVersion.FormatDataGridView(
                dataGridView: dgvVersion);
            dgvVersion.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            dgvVersion.ColumnHeadersVisible
                = dgvVersion.Rows.Count > 0;

            SetControlDesign();
        }

        private void SetControlDesign()
        {
            // Pozn. Hlavičku tabulky budu počítat jako 3 řádky
            // Pokud DataGridView má 0 řádků, pak se hlavička nezobrazuje a žádné extra řádky nepřipočítávám.
            int totalCount =
                ((dgvUnitOfMeasure.Rows.Count == 0) ? 0 : (dgvUnitOfMeasure.Rows.Count + 3)) +
                ((dgvDefinitionVariant.Rows.Count == 0) ? 0 : (dgvDefinitionVariant.Rows.Count + 3)) +
                ((dgvAreaDomainCategory.Rows.Count == 0) ? 0 : (dgvAreaDomainCategory.Rows.Count + 3)) +
                ((dgvSubPopulationCategory.Rows.Count == 0) ? 0 : (dgvSubPopulationCategory.Rows.Count + 3)) +
                ((dgvVersion.Rows.Count == 0) ? 0 : (dgvVersion.Rows.Count + 3));

            // Pokud vyška splContaineru klesne pod 10, polohu Splitteru přestanu měnit.
            // Poloha Splitteru musi být větší než 0 a zároveň menší než výška splContaineru.
            // Tuto podmínku nelze dodržet, pokud v důsledku Dockování klesne výška splContaineru na 0.
            splUnitOfMeasure.Panel1Collapsed =
                dgvUnitOfMeasure.Rows.Count == 0;
            if (splUnitOfMeasure.Height > 10)
            {
                splUnitOfMeasure.SplitterDistance = CalculateSplitterDistance(
                    rowCount: dgvUnitOfMeasure.Rows.Count,
                    totalCount: totalCount,
                    splHeight: splUnitOfMeasure.Height,
                    totalHeight: pnlMain.Height);
            }

            splDefinitionVariant.Panel1Collapsed =
                dgvDefinitionVariant.Rows.Count == 0;
            if (splDefinitionVariant.Height > 10)
            {
                splDefinitionVariant.SplitterDistance = CalculateSplitterDistance(
                    rowCount: dgvDefinitionVariant.Rows.Count,
                    totalCount: totalCount,
                    splHeight: splDefinitionVariant.Height,
                    totalHeight: pnlMain.Height);
            }

            splAreaDomainCategory.Panel1Collapsed =
               dgvAreaDomainCategory.Rows.Count == 0;
            if (splAreaDomainCategory.Height > 10)
            {
                splAreaDomainCategory.SplitterDistance = CalculateSplitterDistance(
                    rowCount: dgvAreaDomainCategory.Rows.Count,
                    totalCount: totalCount,
                    splHeight: splAreaDomainCategory.Height,
                    totalHeight: pnlMain.Height);
            }

            splSubPopulationCategory.Panel1Collapsed =
              dgvSubPopulationCategory.Rows.Count == 0;
            if (splSubPopulationCategory.Height > 10)
            {
                splSubPopulationCategory.SplitterDistance = CalculateSplitterDistance(
                    rowCount: dgvSubPopulationCategory.Rows.Count,
                    totalCount: totalCount,
                    splHeight: splSubPopulationCategory.Height,
                    totalHeight: pnlMain.Height);
            }

            splVersion.Panel1Collapsed =
              dgvVersion.Rows.Count == 0;
            splVersion.Panel2Collapsed = totalCount != 0;
        }

        private static int CalculateSplitterDistance(
            int rowCount, int totalCount, int splHeight, int totalHeight)
        {
            if (rowCount == 0)
            {
                return 0;
            }

            if (totalCount == 0)
            {
                return 0;
            }

            // (rowCount + 2) - připočítává 3 řádky na hlavičku tabulky
            int result = (totalHeight / totalCount) * (rowCount + 3);

            if (result <= 0)
            {
                return 0;
            }
            else if (result >= splHeight)
            {
                result = splHeight - 1;

                if (result <= 0)
                {
                    return 0;
                }
                else
                {
                    return result;
                }
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků kategorie plošné domény</para>
        /// <para lang="en">Editing labels of an area domain category</para>
        /// </summary>
        private void UpdateAreaDomainCategory()
        {
            List<Nullable<int>> displayedItemsIds =
                dgvAreaDomainCategory.Rows
                    .OfType<DataGridViewRow>()
                    .Select(a => a.Cells[TDAreaDomainCategoryList.ColId.Name].Value)
                    .Where(a => a != DBNull.Value)
                    .Select(a => (Nullable<int>)a)
                    .ToList<Nullable<int>>();

            if (displayedItemsIds.Count == 0)
            {
                MessageBox.Show(
                    text: msgEmptyLookupTable,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CAreaDomainCategory,
                displayedItemsIds: displayedItemsIds);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                ShowInformation(targetVariable: TargetVariable, ldsity = LDsity);
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků definiční varianty</para>
        /// <para lang="en">Modification of definitional variant labels</para>
        /// </summary>
        private void UpdateDefinitionVariant()
        {
            List<Nullable<int>> displayedItemsIds =
                dgvDefinitionVariant.Rows
                    .OfType<DataGridViewRow>()
                    .Select(a => a.Cells[TDDefinitionVariantList.ColId.Name].Value)
                    .Where(a => a != DBNull.Value)
                    .Select(a => (Nullable<int>)a)
                    .ToList<Nullable<int>>();

            if (displayedItemsIds.Count == 0)
            {
                MessageBox.Show(
                    text: msgEmptyLookupTable,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CDefinitionVariant,
                displayedItemsIds: displayedItemsIds);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                ShowInformation(targetVariable: TargetVariable, ldsity = LDsity);
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků kategorie subpopulace</para>
        /// <para lang="en">Editing labels of a subpopulation category</para>
        /// </summary>
        private void UpdateSubPopulationCategory()
        {
            List<Nullable<int>> displayedItemsIds =
                dgvSubPopulationCategory.Rows
                    .OfType<DataGridViewRow>()
                    .Select(a => a.Cells[TDSubPopulationCategoryList.ColId.Name].Value)
                    .Where(a => a != DBNull.Value)
                    .Select(a => (Nullable<int>)a)
                    .ToList<Nullable<int>>();

            if (displayedItemsIds.Count == 0)
            {
                MessageBox.Show(
                    text: msgEmptyLookupTable,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CSubPopulationCategory,
                displayedItemsIds: displayedItemsIds);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                ShowInformation(targetVariable: TargetVariable, ldsity = LDsity);
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků jednotky měření</para>
        /// <para lang="en">Editing measurement unit labels</para>
        /// </summary>
        private void UpdateUnitOfMeasure()
        {
            List<Nullable<int>> displayedItemsIds =
                dgvUnitOfMeasure.Rows
                    .OfType<DataGridViewRow>()
                    .Select(a => a.Cells[TDUnitOfMeasureList.ColId.Name].Value)
                    .Where(a => a != DBNull.Value)
                    .Select(a => (Nullable<int>)a)
                    .ToList<Nullable<int>>();

            if (displayedItemsIds.Count == 0)
            {
                MessageBox.Show(
                   text: msgEmptyLookupTable,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CUnitOfMeasure,
                displayedItemsIds: displayedItemsIds);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                ShowInformation(targetVariable: TargetVariable, ldsity = LDsity);
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků verze</para>
        /// <para lang="en">Editing labels of a version</para>
        /// </summary>
        private void UpdateVersion()
        {
            List<Nullable<int>> displayedItemsIds =
                dgvVersion.Rows
                    .OfType<DataGridViewRow>()
                    .Select(a => a.Cells[TDVersionList.ColId.Name].Value)
                    .Where(a => a != DBNull.Value)
                    .Select(a => (Nullable<int>)a)
                    .ToList<Nullable<int>>();

            if (displayedItemsIds.Count == 0)
            {
                MessageBox.Show(
                    text: msgEmptyLookupTable,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CVersion,
                displayedItemsIds: displayedItemsIds);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                ShowInformation(targetVariable: TargetVariable, ldsity = LDsity);
            }
        }

        #endregion Methods

    }

}