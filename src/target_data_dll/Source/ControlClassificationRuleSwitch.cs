﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{
    /// <summary>
    /// <para lang="cs">Ovládací prvek - přepínač klasifikačních pravidel - třída pro Designer</para>
    /// <para lang="en">Control - classification rule switch - class for Designer</para>
    /// </summary>
    internal partial class ControlClassificationRuleSwitchDesign
        : UserControl
    {
        protected ControlClassificationRuleSwitchDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.ToolStripButton BtnNegative => btnNegative;
        protected System.Windows.Forms.ToolStripButton BtnPositive => btnPositive;
        protected System.Windows.Forms.Panel PnlNegative => pnlNegative;
        protected System.Windows.Forms.Panel PnlPositive => pnlPositive;
        protected System.Windows.Forms.SplitContainer SplClassificationRule => splClassificationRule;
        protected System.Windows.Forms.SplitContainer SplMain => splMain;
        protected System.Windows.Forms.ToolStrip TsrMain => tsrMain;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek - přepínač klasifikačních pravidel</para>
    /// <para lang="en">Control - classification rule switch</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlClassificationRuleSwitch<TDomain, TCategory>
            : ControlClassificationRuleSwitchDesign, INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Stav zobrazení klasifikačních pravidel</para>
        /// <para lang="en">Classification rules display status</para>
        /// </summary>
        private ClassificationRuleSwitchDisplayStatus displayStatus;

        /// <summary>
        /// <para lang="cs">Kladné (true) nebo záporné (false) klasifikační pravidlo je zobrazeno</para>
        /// <para lang="en">Positive (true) or negative (false) classification rule is displayed</para>
        /// </summary>
        private bool positive;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Data klasifikačních pravidel
        /// pro kladné příspěvky lokálních hustot</para>
        /// <para lang="en">Data of the classification rules
        /// for positive local density contributions</para>
        /// </summary>
        private ControlClassificationRuleData<TDomain, TCategory>
            ctrPositiveClassificationRuleData;

        /// <summary>
        /// <para lang="cs">Data klasifikačních pravidel
        /// pro záporné příspěvky lokálních hustot</para>
        /// <para lang="en">Data of the classification rules
        /// for negative local density contributions</para>
        /// </summary>
        private ControlClassificationRuleData<TDomain, TCategory>
            ctrNegativeClassificationRuleData;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna textu klasifikačního pravidla"</para>
        /// <para lang="en">Event of the
        /// "Classification rule text change"</para>
        /// </summary>
        public event EventHandler ClassificationRuleTextChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlClassificationRuleSwitch(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not IArealOrPopulationForm<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(IArealOrPopulationForm<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not IArealOrPopulationForm<TDomain, TCategory>)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(IArealOrPopulationForm<TDomain, TCategory>)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Základní formulář</para>
        /// <para lang="en">Base form</para>
        /// </summary>
        public IArealOrPopulationForm<TDomain, TCategory> Base
        {
            get
            {
                return
                    (IArealOrPopulationForm<TDomain, TCategory>)ControlOwner;
            }
        }

        /// <summary>
        /// <para lang="cs">Pár klasifikačních pravidel pro kladný a záporný příspěvek (read-only)</para>
        /// <para lang="en">Classification rule pair for positive and negative contribution (read-only)</para>
        /// </summary>
        private ClassificationRulePair<TDomain, TCategory> ClassificationRulePair
        {
            get
            {
                return Base.SelectedClassificationRulePair;
            }
        }

        /// <summary>
        /// <para lang="cs">Je aktuální objekt lokální hustoty pro třídění
        /// zahrnutý mezi objekty pro uložení do databáze? (read-only)</para>
        /// <para lang="en">Is current local density object for classification
        /// included among objects for saving in database? (read-only)</para>
        /// </summary>
        private bool LDsityObjectForADSPIncluded
        {
            get
            {
                return Base.SelectedLDsityObjectForADSPIncluded;
            }
        }

        /// <summary>
        /// <para lang="cs">Stav zobrazení klasifikačních pravidel</para>
        /// <para lang="en">Classification rules display status</para>
        /// </summary>
        private ClassificationRuleSwitchDisplayStatus DisplayStatus
        {
            get
            {
                return displayStatus;
            }
            set
            {
                displayStatus = value;
                switch (DisplayStatus)
                {
                    case ClassificationRuleSwitchDisplayStatus.NoClassificationRule:
                        Visible = false;
                        BtnPositive.BackColor = System.Drawing.SystemColors.Control;
                        BtnNegative.BackColor = System.Drawing.SystemColors.Control;
                        SplMain.Panel1Collapsed = true;
                        SplMain.Panel2Collapsed = false;
                        SplClassificationRule.Panel1Collapsed = false;
                        SplClassificationRule.Panel2Collapsed = true;
                        break;

                    case ClassificationRuleSwitchDisplayStatus.OneClassificationRule:
                        Visible = true;
                        BtnPositive.BackColor = System.Drawing.SystemColors.Control;
                        BtnNegative.BackColor = System.Drawing.SystemColors.Control;
                        SplMain.Panel1Collapsed = true;
                        SplMain.Panel2Collapsed = false;
                        SplClassificationRule.Panel1Collapsed = false;
                        SplClassificationRule.Panel2Collapsed = true;
                        break;

                    case ClassificationRuleSwitchDisplayStatus.PositiveClassificationRule:
                        Visible = true;
                        BtnPositive.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
                        BtnNegative.BackColor = System.Drawing.SystemColors.Control;
                        SplMain.Panel1Collapsed = false;
                        SplMain.Panel2Collapsed = false;
                        SplClassificationRule.Panel1Collapsed = false;
                        SplClassificationRule.Panel2Collapsed = true;
                        break;

                    case ClassificationRuleSwitchDisplayStatus.NegativeClassificationRule:
                        Visible = true;
                        BtnPositive.BackColor = System.Drawing.SystemColors.Control;
                        BtnNegative.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
                        SplMain.Panel1Collapsed = false;
                        SplMain.Panel2Collapsed = false;
                        SplClassificationRule.Panel1Collapsed = true;
                        SplClassificationRule.Panel2Collapsed = false;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(
                        message: $"Unknown value of the {nameof(DisplayStatus)}.",
                        paramName: nameof(DisplayStatus));
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané klasifikační pravidlo(read-only)</para>
        /// <para lang="en">Selected classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> ClassificationRule
        {
            get
            {
                if (ClassificationRulePair == null)
                {
                    return null;
                }

                if (Positive)
                {
                    return ClassificationRulePair.Positive;
                }
                else
                {
                    return ClassificationRulePair.Negative;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Kladné (true) nebo záporné (false) klasifikační pravidlo je zobrazeno</para>
        /// <para lang="en">Positive (true) or negative (false) classification rule is displayed</para>
        /// </summary>
        public bool Positive
        {
            get
            {
                return positive;
            }
            set
            {
                positive = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Data klasifikačních pravidel
        /// pro kladné příspěvky lokálních hustot</para>
        /// <para lang="en">Data of the classification rules
        /// for positive local density contributions</para>
        /// </summary>
        public ControlClassificationRuleData<TDomain, TCategory> PositiveClassificationRuleData
            => ctrPositiveClassificationRuleData;

        /// <summary>
        /// <para lang="cs">Data klasifikačních pravidel
        /// pro záporné příspěvky lokálních hustot</para>
        /// <para lang="en">Data of the classification rules
        /// for negative local density contributions</para>
        /// </summary>
        public ControlClassificationRuleData<TDomain, TCategory> NegativeClassificationRuleData
            => ctrNegativeClassificationRuleData;

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnNegative),    "Záporné příspěvky lokálních hustot" },
                            { nameof(BtnPositive),    "Kladné příspěvky lokálních hustot"  },
                            { nameof(TsrMain),        String.Empty }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleSwitch<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnNegative),    "Záporné příspěvky lokálních hustot" },
                            { nameof(BtnPositive),    "Kladné příspěvky lokálních hustot"  },
                            { nameof(TsrMain),        String.Empty }
                        } :
                        languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleSwitch<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictNationalSubPopulation)
                                ? dictNationalSubPopulation
                                : [],

                    _ => [],
                },

                LanguageVersion.International =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnNegative),    "Negative local density contributions" },
                            { nameof(BtnPositive),    "Positive local density contributions" },
                            { nameof(TsrMain),        String.Empty }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleSwitch<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(BtnNegative),    "Negative local density contributions" },
                            { nameof(BtnPositive),    "Positive local density contributions" },
                            { nameof(TsrMain),        String.Empty }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlClassificationRuleSwitch<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BackColor = System.Drawing.SystemColors.Control;

            ControlOwner = controlOwner;

            DisplayStatus = ClassificationRuleSwitchDisplayStatus.NoClassificationRule;

            Positive = true;

            LoadContent();

            InitializeControls();

            InitializeLabels();

            Reset();

            BtnPositive.Click += new EventHandler(
                (sender, e) =>
                {
                    Positive = true;
                    Reset();
                });

            BtnNegative.Click += new EventHandler(
                (sender, e) =>
                {
                    Positive = false;
                    Reset();
                });
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacích prvků</para>
        /// <para lang="en">Initializing user controls</para>
        /// </summary>
        private void InitializeControls()
        {
            // ControlPositiveClassificationRuleData
            PnlPositive.Controls.Clear();
            ctrPositiveClassificationRuleData =
                new ControlClassificationRuleData<TDomain, TCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrPositiveClassificationRuleData.ClassificationRuleTextChanged +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ClassificationRuleTextChanged?.Invoke(sender: sender, e: e);
                    });
            PnlPositive.Controls.Add(
                value: ctrPositiveClassificationRuleData);

            // ControlNegativeClassificationRuleData
            PnlNegative.Controls.Clear();
            ctrNegativeClassificationRuleData =
                new ControlClassificationRuleData<TDomain, TCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrNegativeClassificationRuleData.ClassificationRuleTextChanged +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ClassificationRuleTextChanged?.Invoke(sender: sender, e: e);
                    });
            PnlNegative.Controls.Add(
                value: ctrNegativeClassificationRuleData);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing user control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            BtnNegative.Text =
                labels.TryGetValue(key: nameof(BtnNegative),
                    out string btnNegativeText)
                        ? btnNegativeText
                        : String.Empty;

            BtnPositive.Text =
                labels.TryGetValue(key: nameof(BtnPositive),
                    out string btnPositiveText)
                        ? btnPositiveText
                        : String.Empty;

            TsrMain.Text =
                labels.TryGetValue(key: nameof(TsrMain),
                    out string tsrMainText)
                        ? tsrMainText
                        : String.Empty;

            ctrPositiveClassificationRuleData?.InitializeLabels();

            ctrNegativeClassificationRuleData?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Nastavení ovládacího prvku</para>
        /// <para lang="en">Reset control</para>
        /// </summary>
        public void Reset()
        {
            if (ClassificationRulePair == null)
            {
                DisplayStatus = ClassificationRuleSwitchDisplayStatus.NoClassificationRule;
                return;
            }

            if (ClassificationRulePair.LDsityObjectForADSP == null)
            {
                DisplayStatus = ClassificationRuleSwitchDisplayStatus.NoClassificationRule;
                return;
            }

            if (!LDsityObjectForADSPIncluded)
            {
                DisplayStatus = ClassificationRuleSwitchDisplayStatus.NoClassificationRule;
                return;
            }

            DisplayStatus = Base.ClassificationType switch
            {
                TDClassificationTypeEnum.Standard =>
                    ClassificationRuleSwitchDisplayStatus.OneClassificationRule,

                TDClassificationTypeEnum.ChangeOrDynamic =>
                    Positive
                        ? ClassificationRuleSwitchDisplayStatus.PositiveClassificationRule
                        : ClassificationRuleSwitchDisplayStatus.NegativeClassificationRule,

                _ =>
                    throw new ArgumentOutOfRangeException(
                        message: $"Unknown value of the {nameof(Base.ClassificationType)}.",
                        paramName: nameof(Base.ClassificationType)),
            };

            ctrPositiveClassificationRuleData?.Reset();

            ctrNegativeClassificationRuleData?.Reset();
        }

        #endregion Methods

    }

    /// <summary>
    /// <para lang="cs">Stav zobrazení klasifikačních pravidel</para>
    /// <para lang="en">Classification rules display status</para>
    /// </summary>
    internal enum ClassificationRuleSwitchDisplayStatus
    {
        /// <summary>
        /// <para lang="cs">Žádné klasifikační pravidlo</para>
        /// <para lang="en">No classification rule</para>
        /// </summary>
        NoClassificationRule = 0,

        /// <summary>
        /// <para lang="cs">Není rozlišeno kladné a záporné klasifikační pravidlo</para>
        /// <para lang="en">No distinction between positive and negative classification rule</para>
        /// </summary>
        OneClassificationRule = 100,

        /// <summary>
        /// <para lang="cs">Zobrazeno kladné klasifikační pravidlo</para>
        /// <para lang="en">Positive classification rule displayed</para>
        /// </summary>
        PositiveClassificationRule = 200,

        /// <summary>
        /// <para lang="cs">Zobrazeno záporné klasifikační pravidlo</para>
        /// <para lang="en">Negative classification rule displayed</para>
        /// </summary>
        NegativeClassificationRule = 300
    }

}