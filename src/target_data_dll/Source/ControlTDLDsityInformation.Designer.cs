﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlTDLDsityInformation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlTDLDsityInformation));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splUnitOfMeasure = new System.Windows.Forms.SplitContainer();
            this.grpUnitOfMeasure = new System.Windows.Forms.GroupBox();
            this.tlpUnitOfMeasure = new System.Windows.Forms.TableLayoutPanel();
            this.tsrUnitOfMeasure = new System.Windows.Forms.ToolStrip();
            this.btnUnitOfMeasureEdit = new System.Windows.Forms.ToolStripButton();
            this.pnlUnitOfMeasure = new System.Windows.Forms.Panel();
            this.splDefinitionVariant = new System.Windows.Forms.SplitContainer();
            this.grpDefinitionVariant = new System.Windows.Forms.GroupBox();
            this.tlpDefinitionVariant = new System.Windows.Forms.TableLayoutPanel();
            this.tsrDefinitionVariant = new System.Windows.Forms.ToolStrip();
            this.btnDefinitionVariantEdit = new System.Windows.Forms.ToolStripButton();
            this.pnlDefinitionVariant = new System.Windows.Forms.Panel();
            this.splAreaDomainCategory = new System.Windows.Forms.SplitContainer();
            this.grpAreaDomainCategory = new System.Windows.Forms.GroupBox();
            this.tlpAreaDomainCategory = new System.Windows.Forms.TableLayoutPanel();
            this.tsrAreaDomainCategory = new System.Windows.Forms.ToolStrip();
            this.btnAreaDomainCategoryEdit = new System.Windows.Forms.ToolStripButton();
            this.pnlAreaDomainCategory = new System.Windows.Forms.Panel();
            this.splSubPopulationCategory = new System.Windows.Forms.SplitContainer();
            this.grpSubPopulationCategory = new System.Windows.Forms.GroupBox();
            this.tlpSubPopulationCategory = new System.Windows.Forms.TableLayoutPanel();
            this.tsrSubPopulationCategory = new System.Windows.Forms.ToolStrip();
            this.btnSubPopulationCategoryEdit = new System.Windows.Forms.ToolStripButton();
            this.pnlSubPopulationCategory = new System.Windows.Forms.Panel();
            this.splVersion = new System.Windows.Forms.SplitContainer();
            this.grpVersion = new System.Windows.Forms.GroupBox();
            this.tlpVersion = new System.Windows.Forms.TableLayoutPanel();
            this.tsrVersion = new System.Windows.Forms.ToolStrip();
            this.btnVersionEdit = new System.Windows.Forms.ToolStripButton();
            this.pnlVersion = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splUnitOfMeasure)).BeginInit();
            this.splUnitOfMeasure.Panel1.SuspendLayout();
            this.splUnitOfMeasure.Panel2.SuspendLayout();
            this.splUnitOfMeasure.SuspendLayout();
            this.grpUnitOfMeasure.SuspendLayout();
            this.tlpUnitOfMeasure.SuspendLayout();
            this.tsrUnitOfMeasure.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splDefinitionVariant)).BeginInit();
            this.splDefinitionVariant.Panel1.SuspendLayout();
            this.splDefinitionVariant.Panel2.SuspendLayout();
            this.splDefinitionVariant.SuspendLayout();
            this.grpDefinitionVariant.SuspendLayout();
            this.tlpDefinitionVariant.SuspendLayout();
            this.tsrDefinitionVariant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splAreaDomainCategory)).BeginInit();
            this.splAreaDomainCategory.Panel1.SuspendLayout();
            this.splAreaDomainCategory.Panel2.SuspendLayout();
            this.splAreaDomainCategory.SuspendLayout();
            this.grpAreaDomainCategory.SuspendLayout();
            this.tlpAreaDomainCategory.SuspendLayout();
            this.tsrAreaDomainCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splSubPopulationCategory)).BeginInit();
            this.splSubPopulationCategory.Panel1.SuspendLayout();
            this.splSubPopulationCategory.Panel2.SuspendLayout();
            this.splSubPopulationCategory.SuspendLayout();
            this.grpSubPopulationCategory.SuspendLayout();
            this.tlpSubPopulationCategory.SuspendLayout();
            this.tsrSubPopulationCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splVersion)).BeginInit();
            this.splVersion.Panel1.SuspendLayout();
            this.splVersion.SuspendLayout();
            this.grpVersion.SuspendLayout();
            this.tlpVersion.SuspendLayout();
            this.tsrVersion.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.Controls.Add(this.splUnitOfMeasure);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(498, 498);
            this.pnlMain.TabIndex = 0;
            // 
            // splUnitOfMeasure
            // 
            this.splUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splUnitOfMeasure.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splUnitOfMeasure.Location = new System.Drawing.Point(0, 0);
            this.splUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            this.splUnitOfMeasure.Name = "splUnitOfMeasure";
            this.splUnitOfMeasure.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splUnitOfMeasure.Panel1
            // 
            this.splUnitOfMeasure.Panel1.Controls.Add(this.grpUnitOfMeasure);
            this.splUnitOfMeasure.Panel1MinSize = 0;
            // 
            // splUnitOfMeasure.Panel2
            // 
            this.splUnitOfMeasure.Panel2.Controls.Add(this.splDefinitionVariant);
            this.splUnitOfMeasure.Panel2MinSize = 0;
            this.splUnitOfMeasure.Size = new System.Drawing.Size(498, 498);
            this.splUnitOfMeasure.SplitterDistance = 90;
            this.splUnitOfMeasure.SplitterWidth = 1;
            this.splUnitOfMeasure.TabIndex = 2;
            // 
            // grpUnitOfMeasure
            // 
            this.grpUnitOfMeasure.Controls.Add(this.tlpUnitOfMeasure);
            this.grpUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpUnitOfMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpUnitOfMeasure.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpUnitOfMeasure.Location = new System.Drawing.Point(0, 0);
            this.grpUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            this.grpUnitOfMeasure.Name = "grpUnitOfMeasure";
            this.grpUnitOfMeasure.Size = new System.Drawing.Size(498, 90);
            this.grpUnitOfMeasure.TabIndex = 2;
            this.grpUnitOfMeasure.TabStop = false;
            this.grpUnitOfMeasure.Text = "grpUnitOfMeasure";
            // 
            // tlpUnitOfMeasure
            // 
            this.tlpUnitOfMeasure.ColumnCount = 1;
            this.tlpUnitOfMeasure.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpUnitOfMeasure.Controls.Add(this.tsrUnitOfMeasure, 0, 0);
            this.tlpUnitOfMeasure.Controls.Add(this.pnlUnitOfMeasure, 0, 1);
            this.tlpUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpUnitOfMeasure.Location = new System.Drawing.Point(3, 18);
            this.tlpUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            this.tlpUnitOfMeasure.Name = "tlpUnitOfMeasure";
            this.tlpUnitOfMeasure.RowCount = 2;
            this.tlpUnitOfMeasure.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpUnitOfMeasure.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpUnitOfMeasure.Size = new System.Drawing.Size(492, 69);
            this.tlpUnitOfMeasure.TabIndex = 1;
            // 
            // tsrUnitOfMeasure
            // 
            this.tsrUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrUnitOfMeasure.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnUnitOfMeasureEdit});
            this.tsrUnitOfMeasure.Location = new System.Drawing.Point(0, 0);
            this.tsrUnitOfMeasure.Name = "tsrUnitOfMeasure";
            this.tsrUnitOfMeasure.Padding = new System.Windows.Forms.Padding(0);
            this.tsrUnitOfMeasure.Size = new System.Drawing.Size(492, 25);
            this.tsrUnitOfMeasure.TabIndex = 0;
            this.tsrUnitOfMeasure.Text = "tsrUnitOfMeasure";
            // 
            // btnUnitOfMeasureEdit
            // 
            this.btnUnitOfMeasureEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUnitOfMeasureEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnUnitOfMeasureEdit.Image")));
            this.btnUnitOfMeasureEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUnitOfMeasureEdit.Name = "btnUnitOfMeasureEdit";
            this.btnUnitOfMeasureEdit.Size = new System.Drawing.Size(23, 22);
            this.btnUnitOfMeasureEdit.Text = "btnUnitOfMeasureEdit";
            // 
            // pnlUnitOfMeasure
            // 
            this.pnlUnitOfMeasure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUnitOfMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlUnitOfMeasure.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlUnitOfMeasure.Location = new System.Drawing.Point(0, 25);
            this.pnlUnitOfMeasure.Margin = new System.Windows.Forms.Padding(0);
            this.pnlUnitOfMeasure.Name = "pnlUnitOfMeasure";
            this.pnlUnitOfMeasure.Size = new System.Drawing.Size(492, 44);
            this.pnlUnitOfMeasure.TabIndex = 1;
            // 
            // splDefinitionVariant
            // 
            this.splDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splDefinitionVariant.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splDefinitionVariant.Location = new System.Drawing.Point(0, 0);
            this.splDefinitionVariant.Margin = new System.Windows.Forms.Padding(0);
            this.splDefinitionVariant.Name = "splDefinitionVariant";
            this.splDefinitionVariant.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splDefinitionVariant.Panel1
            // 
            this.splDefinitionVariant.Panel1.Controls.Add(this.grpDefinitionVariant);
            this.splDefinitionVariant.Panel1MinSize = 0;
            // 
            // splDefinitionVariant.Panel2
            // 
            this.splDefinitionVariant.Panel2.Controls.Add(this.splAreaDomainCategory);
            this.splDefinitionVariant.Panel2MinSize = 0;
            this.splDefinitionVariant.Size = new System.Drawing.Size(498, 407);
            this.splDefinitionVariant.SplitterDistance = 90;
            this.splDefinitionVariant.SplitterWidth = 1;
            this.splDefinitionVariant.TabIndex = 0;
            // 
            // grpDefinitionVariant
            // 
            this.grpDefinitionVariant.Controls.Add(this.tlpDefinitionVariant);
            this.grpDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDefinitionVariant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpDefinitionVariant.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDefinitionVariant.Location = new System.Drawing.Point(0, 0);
            this.grpDefinitionVariant.Margin = new System.Windows.Forms.Padding(0);
            this.grpDefinitionVariant.Name = "grpDefinitionVariant";
            this.grpDefinitionVariant.Size = new System.Drawing.Size(498, 90);
            this.grpDefinitionVariant.TabIndex = 3;
            this.grpDefinitionVariant.TabStop = false;
            this.grpDefinitionVariant.Text = "grpDefinitionVariant";
            // 
            // tlpDefinitionVariant
            // 
            this.tlpDefinitionVariant.ColumnCount = 1;
            this.tlpDefinitionVariant.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDefinitionVariant.Controls.Add(this.tsrDefinitionVariant, 0, 0);
            this.tlpDefinitionVariant.Controls.Add(this.pnlDefinitionVariant, 0, 1);
            this.tlpDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDefinitionVariant.Location = new System.Drawing.Point(3, 18);
            this.tlpDefinitionVariant.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDefinitionVariant.Name = "tlpDefinitionVariant";
            this.tlpDefinitionVariant.RowCount = 2;
            this.tlpDefinitionVariant.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpDefinitionVariant.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDefinitionVariant.Size = new System.Drawing.Size(492, 69);
            this.tlpDefinitionVariant.TabIndex = 0;
            // 
            // tsrDefinitionVariant
            // 
            this.tsrDefinitionVariant.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDefinitionVariantEdit});
            this.tsrDefinitionVariant.Location = new System.Drawing.Point(0, 0);
            this.tsrDefinitionVariant.Name = "tsrDefinitionVariant";
            this.tsrDefinitionVariant.Size = new System.Drawing.Size(492, 25);
            this.tsrDefinitionVariant.TabIndex = 0;
            this.tsrDefinitionVariant.Text = "tsrDefinitionVariant";
            // 
            // btnDefinitionVariantEdit
            // 
            this.btnDefinitionVariantEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDefinitionVariantEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnDefinitionVariantEdit.Image")));
            this.btnDefinitionVariantEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDefinitionVariantEdit.Name = "btnDefinitionVariantEdit";
            this.btnDefinitionVariantEdit.Size = new System.Drawing.Size(23, 22);
            this.btnDefinitionVariantEdit.Text = "btnDefinitionVariantEdit";
            // 
            // pnlDefinitionVariant
            // 
            this.pnlDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDefinitionVariant.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlDefinitionVariant.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlDefinitionVariant.Location = new System.Drawing.Point(0, 25);
            this.pnlDefinitionVariant.Margin = new System.Windows.Forms.Padding(0);
            this.pnlDefinitionVariant.Name = "pnlDefinitionVariant";
            this.pnlDefinitionVariant.Size = new System.Drawing.Size(492, 44);
            this.pnlDefinitionVariant.TabIndex = 1;
            // 
            // splAreaDomainCategory
            // 
            this.splAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splAreaDomainCategory.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splAreaDomainCategory.Location = new System.Drawing.Point(0, 0);
            this.splAreaDomainCategory.Margin = new System.Windows.Forms.Padding(0);
            this.splAreaDomainCategory.Name = "splAreaDomainCategory";
            this.splAreaDomainCategory.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splAreaDomainCategory.Panel1
            // 
            this.splAreaDomainCategory.Panel1.Controls.Add(this.grpAreaDomainCategory);
            this.splAreaDomainCategory.Panel1MinSize = 0;
            // 
            // splAreaDomainCategory.Panel2
            // 
            this.splAreaDomainCategory.Panel2.Controls.Add(this.splSubPopulationCategory);
            this.splAreaDomainCategory.Panel2MinSize = 0;
            this.splAreaDomainCategory.Size = new System.Drawing.Size(498, 316);
            this.splAreaDomainCategory.SplitterDistance = 90;
            this.splAreaDomainCategory.SplitterWidth = 1;
            this.splAreaDomainCategory.TabIndex = 0;
            // 
            // grpAreaDomainCategory
            // 
            this.grpAreaDomainCategory.Controls.Add(this.tlpAreaDomainCategory);
            this.grpAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAreaDomainCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpAreaDomainCategory.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpAreaDomainCategory.Location = new System.Drawing.Point(0, 0);
            this.grpAreaDomainCategory.Margin = new System.Windows.Forms.Padding(0);
            this.grpAreaDomainCategory.Name = "grpAreaDomainCategory";
            this.grpAreaDomainCategory.Size = new System.Drawing.Size(498, 90);
            this.grpAreaDomainCategory.TabIndex = 5;
            this.grpAreaDomainCategory.TabStop = false;
            this.grpAreaDomainCategory.Text = "grpAreaDomainCategory";
            // 
            // tlpAreaDomainCategory
            // 
            this.tlpAreaDomainCategory.ColumnCount = 1;
            this.tlpAreaDomainCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAreaDomainCategory.Controls.Add(this.tsrAreaDomainCategory, 0, 0);
            this.tlpAreaDomainCategory.Controls.Add(this.pnlAreaDomainCategory, 0, 1);
            this.tlpAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAreaDomainCategory.Location = new System.Drawing.Point(3, 18);
            this.tlpAreaDomainCategory.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAreaDomainCategory.Name = "tlpAreaDomainCategory";
            this.tlpAreaDomainCategory.RowCount = 2;
            this.tlpAreaDomainCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpAreaDomainCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAreaDomainCategory.Size = new System.Drawing.Size(492, 69);
            this.tlpAreaDomainCategory.TabIndex = 0;
            // 
            // tsrAreaDomainCategory
            // 
            this.tsrAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrAreaDomainCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAreaDomainCategoryEdit});
            this.tsrAreaDomainCategory.Location = new System.Drawing.Point(0, 0);
            this.tsrAreaDomainCategory.Name = "tsrAreaDomainCategory";
            this.tsrAreaDomainCategory.Size = new System.Drawing.Size(492, 25);
            this.tsrAreaDomainCategory.TabIndex = 0;
            this.tsrAreaDomainCategory.Text = "tsrAreaDomainCategory";
            // 
            // btnAreaDomainCategoryEdit
            // 
            this.btnAreaDomainCategoryEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAreaDomainCategoryEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnAreaDomainCategoryEdit.Image")));
            this.btnAreaDomainCategoryEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAreaDomainCategoryEdit.Name = "btnAreaDomainCategoryEdit";
            this.btnAreaDomainCategoryEdit.Size = new System.Drawing.Size(23, 22);
            this.btnAreaDomainCategoryEdit.Text = "btnAreaDomainCategoryEdit";
            // 
            // pnlAreaDomainCategory
            // 
            this.pnlAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAreaDomainCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlAreaDomainCategory.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlAreaDomainCategory.Location = new System.Drawing.Point(0, 25);
            this.pnlAreaDomainCategory.Margin = new System.Windows.Forms.Padding(0);
            this.pnlAreaDomainCategory.Name = "pnlAreaDomainCategory";
            this.pnlAreaDomainCategory.Size = new System.Drawing.Size(492, 44);
            this.pnlAreaDomainCategory.TabIndex = 1;
            // 
            // splSubPopulationCategory
            // 
            this.splSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splSubPopulationCategory.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splSubPopulationCategory.Location = new System.Drawing.Point(0, 0);
            this.splSubPopulationCategory.Margin = new System.Windows.Forms.Padding(0);
            this.splSubPopulationCategory.Name = "splSubPopulationCategory";
            this.splSubPopulationCategory.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splSubPopulationCategory.Panel1
            // 
            this.splSubPopulationCategory.Panel1.Controls.Add(this.grpSubPopulationCategory);
            this.splSubPopulationCategory.Panel1MinSize = 0;
            // 
            // splSubPopulationCategory.Panel2
            // 
            this.splSubPopulationCategory.Panel2.Controls.Add(this.splVersion);
            this.splSubPopulationCategory.Panel2MinSize = 0;
            this.splSubPopulationCategory.Size = new System.Drawing.Size(498, 225);
            this.splSubPopulationCategory.SplitterDistance = 90;
            this.splSubPopulationCategory.SplitterWidth = 1;
            this.splSubPopulationCategory.TabIndex = 0;
            // 
            // grpSubPopulationCategory
            // 
            this.grpSubPopulationCategory.Controls.Add(this.tlpSubPopulationCategory);
            this.grpSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSubPopulationCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpSubPopulationCategory.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpSubPopulationCategory.Location = new System.Drawing.Point(0, 0);
            this.grpSubPopulationCategory.Margin = new System.Windows.Forms.Padding(0);
            this.grpSubPopulationCategory.Name = "grpSubPopulationCategory";
            this.grpSubPopulationCategory.Size = new System.Drawing.Size(498, 90);
            this.grpSubPopulationCategory.TabIndex = 9;
            this.grpSubPopulationCategory.TabStop = false;
            this.grpSubPopulationCategory.Text = "grpSubPopulationCategory";
            // 
            // tlpSubPopulationCategory
            // 
            this.tlpSubPopulationCategory.ColumnCount = 1;
            this.tlpSubPopulationCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSubPopulationCategory.Controls.Add(this.tsrSubPopulationCategory, 0, 0);
            this.tlpSubPopulationCategory.Controls.Add(this.pnlSubPopulationCategory, 0, 1);
            this.tlpSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSubPopulationCategory.Location = new System.Drawing.Point(3, 18);
            this.tlpSubPopulationCategory.Margin = new System.Windows.Forms.Padding(0);
            this.tlpSubPopulationCategory.Name = "tlpSubPopulationCategory";
            this.tlpSubPopulationCategory.RowCount = 2;
            this.tlpSubPopulationCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpSubPopulationCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSubPopulationCategory.Size = new System.Drawing.Size(492, 69);
            this.tlpSubPopulationCategory.TabIndex = 0;
            // 
            // tsrSubPopulationCategory
            // 
            this.tsrSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrSubPopulationCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSubPopulationCategoryEdit});
            this.tsrSubPopulationCategory.Location = new System.Drawing.Point(0, 0);
            this.tsrSubPopulationCategory.Name = "tsrSubPopulationCategory";
            this.tsrSubPopulationCategory.Size = new System.Drawing.Size(492, 25);
            this.tsrSubPopulationCategory.TabIndex = 0;
            this.tsrSubPopulationCategory.Text = "tsrSubPopulationCategory";
            // 
            // btnSubPopulationCategoryEdit
            // 
            this.btnSubPopulationCategoryEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSubPopulationCategoryEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnSubPopulationCategoryEdit.Image")));
            this.btnSubPopulationCategoryEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSubPopulationCategoryEdit.Name = "btnSubPopulationCategoryEdit";
            this.btnSubPopulationCategoryEdit.Size = new System.Drawing.Size(23, 22);
            this.btnSubPopulationCategoryEdit.Text = "btnSubPopulationCategoryEdit";
            // 
            // pnlSubPopulationCategory
            // 
            this.pnlSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSubPopulationCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlSubPopulationCategory.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlSubPopulationCategory.Location = new System.Drawing.Point(0, 25);
            this.pnlSubPopulationCategory.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSubPopulationCategory.Name = "pnlSubPopulationCategory";
            this.pnlSubPopulationCategory.Size = new System.Drawing.Size(492, 44);
            this.pnlSubPopulationCategory.TabIndex = 1;
            // 
            // splVersion
            // 
            this.splVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splVersion.Location = new System.Drawing.Point(0, 0);
            this.splVersion.Margin = new System.Windows.Forms.Padding(0);
            this.splVersion.Name = "splVersion";
            this.splVersion.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splVersion.Panel1
            // 
            this.splVersion.Panel1.Controls.Add(this.grpVersion);
            this.splVersion.Panel1MinSize = 0;
            this.splVersion.Panel2MinSize = 0;
            this.splVersion.Size = new System.Drawing.Size(498, 134);
            this.splVersion.SplitterDistance = 90;
            this.splVersion.SplitterWidth = 1;
            this.splVersion.TabIndex = 0;
            // 
            // grpVersion
            // 
            this.grpVersion.Controls.Add(this.tlpVersion);
            this.grpVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpVersion.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpVersion.Location = new System.Drawing.Point(0, 0);
            this.grpVersion.Margin = new System.Windows.Forms.Padding(0);
            this.grpVersion.Name = "grpVersion";
            this.grpVersion.Size = new System.Drawing.Size(498, 90);
            this.grpVersion.TabIndex = 11;
            this.grpVersion.TabStop = false;
            this.grpVersion.Text = "grpVersion";
            // 
            // tlpVersion
            // 
            this.tlpVersion.ColumnCount = 1;
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpVersion.Controls.Add(this.tsrVersion, 0, 0);
            this.tlpVersion.Controls.Add(this.pnlVersion, 0, 1);
            this.tlpVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpVersion.Location = new System.Drawing.Point(3, 18);
            this.tlpVersion.Margin = new System.Windows.Forms.Padding(0);
            this.tlpVersion.Name = "tlpVersion";
            this.tlpVersion.RowCount = 2;
            this.tlpVersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpVersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpVersion.Size = new System.Drawing.Size(492, 69);
            this.tlpVersion.TabIndex = 0;
            // 
            // tsrVersion
            // 
            this.tsrVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrVersion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnVersionEdit});
            this.tsrVersion.Location = new System.Drawing.Point(0, 0);
            this.tsrVersion.Name = "tsrVersion";
            this.tsrVersion.Size = new System.Drawing.Size(492, 25);
            this.tsrVersion.TabIndex = 0;
            this.tsrVersion.Text = "toolStrip1";
            // 
            // btnVersionEdit
            // 
            this.btnVersionEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnVersionEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnVersionEdit.Image")));
            this.btnVersionEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnVersionEdit.Name = "btnVersionEdit";
            this.btnVersionEdit.Size = new System.Drawing.Size(23, 22);
            this.btnVersionEdit.Text = "btnVersionEdit";
            // 
            // pnlVersion
            // 
            this.pnlVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlVersion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlVersion.Location = new System.Drawing.Point(0, 25);
            this.pnlVersion.Margin = new System.Windows.Forms.Padding(0);
            this.pnlVersion.Name = "pnlVersion";
            this.pnlVersion.Size = new System.Drawing.Size(492, 44);
            this.pnlVersion.TabIndex = 1;
            // 
            // ControlTDLDsityInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlTDLDsityInformation";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(500, 500);
            this.pnlMain.ResumeLayout(false);
            this.splUnitOfMeasure.Panel1.ResumeLayout(false);
            this.splUnitOfMeasure.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splUnitOfMeasure)).EndInit();
            this.splUnitOfMeasure.ResumeLayout(false);
            this.grpUnitOfMeasure.ResumeLayout(false);
            this.tlpUnitOfMeasure.ResumeLayout(false);
            this.tlpUnitOfMeasure.PerformLayout();
            this.tsrUnitOfMeasure.ResumeLayout(false);
            this.tsrUnitOfMeasure.PerformLayout();
            this.splDefinitionVariant.Panel1.ResumeLayout(false);
            this.splDefinitionVariant.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splDefinitionVariant)).EndInit();
            this.splDefinitionVariant.ResumeLayout(false);
            this.grpDefinitionVariant.ResumeLayout(false);
            this.tlpDefinitionVariant.ResumeLayout(false);
            this.tlpDefinitionVariant.PerformLayout();
            this.tsrDefinitionVariant.ResumeLayout(false);
            this.tsrDefinitionVariant.PerformLayout();
            this.splAreaDomainCategory.Panel1.ResumeLayout(false);
            this.splAreaDomainCategory.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splAreaDomainCategory)).EndInit();
            this.splAreaDomainCategory.ResumeLayout(false);
            this.grpAreaDomainCategory.ResumeLayout(false);
            this.tlpAreaDomainCategory.ResumeLayout(false);
            this.tlpAreaDomainCategory.PerformLayout();
            this.tsrAreaDomainCategory.ResumeLayout(false);
            this.tsrAreaDomainCategory.PerformLayout();
            this.splSubPopulationCategory.Panel1.ResumeLayout(false);
            this.splSubPopulationCategory.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splSubPopulationCategory)).EndInit();
            this.splSubPopulationCategory.ResumeLayout(false);
            this.grpSubPopulationCategory.ResumeLayout(false);
            this.tlpSubPopulationCategory.ResumeLayout(false);
            this.tlpSubPopulationCategory.PerformLayout();
            this.tsrSubPopulationCategory.ResumeLayout(false);
            this.tsrSubPopulationCategory.PerformLayout();
            this.splVersion.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splVersion)).EndInit();
            this.splVersion.ResumeLayout(false);
            this.grpVersion.ResumeLayout(false);
            this.tlpVersion.ResumeLayout(false);
            this.tlpVersion.PerformLayout();
            this.tsrVersion.ResumeLayout(false);
            this.tsrVersion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splUnitOfMeasure;
        private System.Windows.Forms.GroupBox grpUnitOfMeasure;
        private System.Windows.Forms.TableLayoutPanel tlpUnitOfMeasure;
        private System.Windows.Forms.ToolStrip tsrUnitOfMeasure;
        private System.Windows.Forms.ToolStripButton btnUnitOfMeasureEdit;
        private System.Windows.Forms.Panel pnlUnitOfMeasure;
        private System.Windows.Forms.SplitContainer splDefinitionVariant;
        private System.Windows.Forms.GroupBox grpDefinitionVariant;
        private System.Windows.Forms.TableLayoutPanel tlpDefinitionVariant;
        private System.Windows.Forms.ToolStrip tsrDefinitionVariant;
        private System.Windows.Forms.ToolStripButton btnDefinitionVariantEdit;
        private System.Windows.Forms.Panel pnlDefinitionVariant;
        private System.Windows.Forms.SplitContainer splAreaDomainCategory;
        private System.Windows.Forms.GroupBox grpAreaDomainCategory;
        private System.Windows.Forms.TableLayoutPanel tlpAreaDomainCategory;
        private System.Windows.Forms.ToolStrip tsrAreaDomainCategory;
        private System.Windows.Forms.ToolStripButton btnAreaDomainCategoryEdit;
        private System.Windows.Forms.Panel pnlAreaDomainCategory;
        private System.Windows.Forms.SplitContainer splSubPopulationCategory;
        private System.Windows.Forms.GroupBox grpSubPopulationCategory;
        private System.Windows.Forms.TableLayoutPanel tlpSubPopulationCategory;
        private System.Windows.Forms.ToolStrip tsrSubPopulationCategory;
        private System.Windows.Forms.ToolStripButton btnSubPopulationCategoryEdit;
        private System.Windows.Forms.Panel pnlSubPopulationCategory;
        private System.Windows.Forms.SplitContainer splVersion;
        private System.Windows.Forms.GroupBox grpVersion;
        private System.Windows.Forms.TableLayoutPanel tlpVersion;
        private System.Windows.Forms.ToolStrip tsrVersion;
        private System.Windows.Forms.ToolStripButton btnVersionEdit;
        private System.Windows.Forms.Panel pnlVersion;
    }

}