﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{
    partial class ControlTDSelectionTargetVariableDivision
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlTDSelectionTargetVariableDivision));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlNext = new System.Windows.Forms.Panel();
            this.btnNext = new System.Windows.Forms.Button();
            this.pnlPrevious = new System.Windows.Forms.Panel();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.pnlWorkSpace = new System.Windows.Forms.Panel();
            this.tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            this.grpDecision = new System.Windows.Forms.GroupBox();
            this.rdoDecisionYes = new System.Windows.Forms.RadioButton();
            this.rdoDecisionNo = new System.Windows.Forms.RadioButton();
            this.splTargetVariable = new System.Windows.Forms.SplitContainer();
            this.grpTargetVariable = new System.Windows.Forms.GroupBox();
            this.tlpTargetVariable = new System.Windows.Forms.TableLayoutPanel();
            this.tsrTargetVariable = new System.Windows.Forms.ToolStrip();
            this.btnTargetVariableAdd = new System.Windows.Forms.ToolStripButton();
            this.btnTargetVariableDelete = new System.Windows.Forms.ToolStripButton();
            this.splTargetVariableList = new System.Windows.Forms.SplitContainer();
            this.pnlTargetVariableCore = new System.Windows.Forms.Panel();
            this.lstTargetVariableCore = new System.Windows.Forms.ListBox();
            this.pnlTargetVariableDivision = new System.Windows.Forms.Panel();
            this.lstTargetVariableDivision = new System.Windows.Forms.ListBox();
            this.splLDsityObject = new System.Windows.Forms.SplitContainer();
            this.grpLDsityObject = new System.Windows.Forms.GroupBox();
            this.tlpLDsityObject = new System.Windows.Forms.TableLayoutPanel();
            this.tsrLDsityObject = new System.Windows.Forms.ToolStrip();
            this.btnLDsityObjectEdit = new System.Windows.Forms.ToolStripButton();
            this.splLDsityObjectList = new System.Windows.Forms.SplitContainer();
            this.pnlLDsityObjectCore = new System.Windows.Forms.Panel();
            this.lstLDsityObjectCore = new System.Windows.Forms.ListBox();
            this.pnlLDsityObjectDivision = new System.Windows.Forms.Panel();
            this.lstLDsityObjectDivision = new System.Windows.Forms.ListBox();
            this.splLDsity = new System.Windows.Forms.SplitContainer();
            this.grpLDsity = new System.Windows.Forms.GroupBox();
            this.tlpLDsity = new System.Windows.Forms.TableLayoutPanel();
            this.tsrLDsity = new System.Windows.Forms.ToolStrip();
            this.btnLDsityEdit = new System.Windows.Forms.ToolStripButton();
            this.splLDsityList = new System.Windows.Forms.SplitContainer();
            this.pnlLDsityCore = new System.Windows.Forms.Panel();
            this.lstLDsityCore = new System.Windows.Forms.ListBox();
            this.pnlLDsityDivision = new System.Windows.Forms.Panel();
            this.lstLDsityDivision = new System.Windows.Forms.ListBox();
            this.splInformationList = new System.Windows.Forms.SplitContainer();
            this.pnlInformationCore = new System.Windows.Forms.Panel();
            this.pnlInformationDivision = new System.Windows.Forms.Panel();
            this.pnlCaption = new System.Windows.Forms.Panel();
            this.splCaption = new System.Windows.Forms.SplitContainer();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.tlpCaption = new System.Windows.Forms.TableLayoutPanel();
            this.lblSelectedLDsityObjectGroupValue = new System.Windows.Forms.Label();
            this.lblSelectedLDsityObjectGroupCaption = new System.Windows.Forms.Label();
            this.lblSelectedTargetVariableCaption = new System.Windows.Forms.Label();
            this.lblSelectedTargetVariableValue = new System.Windows.Forms.Label();
            this.btnTargetVariableCopy = new System.Windows.Forms.ToolStripButton();
            this.pnlMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlNext.SuspendLayout();
            this.pnlPrevious.SuspendLayout();
            this.pnlWorkSpace.SuspendLayout();
            this.tlpWorkSpace.SuspendLayout();
            this.grpDecision.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splTargetVariable)).BeginInit();
            this.splTargetVariable.Panel1.SuspendLayout();
            this.splTargetVariable.Panel2.SuspendLayout();
            this.splTargetVariable.SuspendLayout();
            this.grpTargetVariable.SuspendLayout();
            this.tlpTargetVariable.SuspendLayout();
            this.tsrTargetVariable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splTargetVariableList)).BeginInit();
            this.splTargetVariableList.Panel1.SuspendLayout();
            this.splTargetVariableList.Panel2.SuspendLayout();
            this.splTargetVariableList.SuspendLayout();
            this.pnlTargetVariableCore.SuspendLayout();
            this.pnlTargetVariableDivision.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObject)).BeginInit();
            this.splLDsityObject.Panel1.SuspendLayout();
            this.splLDsityObject.Panel2.SuspendLayout();
            this.splLDsityObject.SuspendLayout();
            this.grpLDsityObject.SuspendLayout();
            this.tlpLDsityObject.SuspendLayout();
            this.tsrLDsityObject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObjectList)).BeginInit();
            this.splLDsityObjectList.Panel1.SuspendLayout();
            this.splLDsityObjectList.Panel2.SuspendLayout();
            this.splLDsityObjectList.SuspendLayout();
            this.pnlLDsityObjectCore.SuspendLayout();
            this.pnlLDsityObjectDivision.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splLDsity)).BeginInit();
            this.splLDsity.Panel1.SuspendLayout();
            this.splLDsity.Panel2.SuspendLayout();
            this.splLDsity.SuspendLayout();
            this.grpLDsity.SuspendLayout();
            this.tlpLDsity.SuspendLayout();
            this.tsrLDsity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityList)).BeginInit();
            this.splLDsityList.Panel1.SuspendLayout();
            this.splLDsityList.Panel2.SuspendLayout();
            this.splLDsityList.SuspendLayout();
            this.pnlLDsityCore.SuspendLayout();
            this.pnlLDsityDivision.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splInformationList)).BeginInit();
            this.splInformationList.Panel1.SuspendLayout();
            this.splInformationList.Panel2.SuspendLayout();
            this.splInformationList.SuspendLayout();
            this.pnlCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).BeginInit();
            this.splCaption.Panel1.SuspendLayout();
            this.splCaption.Panel2.SuspendLayout();
            this.splCaption.SuspendLayout();
            this.tlpCaption.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.tlpMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 2);
            this.tlpMain.Controls.Add(this.pnlWorkSpace, 0, 1);
            this.tlpMain.Controls.Add(this.pnlCaption, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 3;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.Controls.Add(this.pnlNext, 2, 0);
            this.tlpButtons.Controls.Add(this.pnlPrevious, 1, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 500);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(960, 40);
            this.tlpButtons.TabIndex = 15;
            // 
            // pnlNext
            // 
            this.pnlNext.Controls.Add(this.btnNext);
            this.pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNext.Location = new System.Drawing.Point(800, 0);
            this.pnlNext.Margin = new System.Windows.Forms.Padding(0);
            this.pnlNext.Name = "pnlNext";
            this.pnlNext.Padding = new System.Windows.Forms.Padding(5);
            this.pnlNext.Size = new System.Drawing.Size(160, 40);
            this.pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnNext.Location = new System.Drawing.Point(5, 5);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 15;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            this.pnlPrevious.Controls.Add(this.btnPrevious);
            this.pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrevious.Location = new System.Drawing.Point(640, 0);
            this.pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPrevious.Name = "pnlPrevious";
            this.pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPrevious.Size = new System.Drawing.Size(160, 40);
            this.pnlPrevious.TabIndex = 17;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnPrevious.Location = new System.Drawing.Point(5, 5);
            this.btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(150, 30);
            this.btnPrevious.TabIndex = 15;
            this.btnPrevious.Text = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            this.pnlWorkSpace.Controls.Add(this.tlpWorkSpace);
            this.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkSpace.Location = new System.Drawing.Point(3, 93);
            this.pnlWorkSpace.Name = "pnlWorkSpace";
            this.pnlWorkSpace.Size = new System.Drawing.Size(954, 404);
            this.pnlWorkSpace.TabIndex = 9;
            // 
            // tlpWorkSpace
            // 
            this.tlpWorkSpace.ColumnCount = 1;
            this.tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWorkSpace.Controls.Add(this.grpDecision, 0, 0);
            this.tlpWorkSpace.Controls.Add(this.splTargetVariable, 0, 1);
            this.tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpWorkSpace.Location = new System.Drawing.Point(0, 0);
            this.tlpWorkSpace.Name = "tlpWorkSpace";
            this.tlpWorkSpace.RowCount = 2;
            this.tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWorkSpace.Size = new System.Drawing.Size(954, 404);
            this.tlpWorkSpace.TabIndex = 0;
            // 
            // grpDecision
            // 
            this.grpDecision.Controls.Add(this.rdoDecisionYes);
            this.grpDecision.Controls.Add(this.rdoDecisionNo);
            this.grpDecision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDecision.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpDecision.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDecision.Location = new System.Drawing.Point(3, 3);
            this.grpDecision.Name = "grpDecision";
            this.grpDecision.Size = new System.Drawing.Size(948, 74);
            this.grpDecision.TabIndex = 0;
            this.grpDecision.TabStop = false;
            this.grpDecision.Text = "grpDecision";
            // 
            // rdoDecisionYes
            // 
            this.rdoDecisionYes.AutoSize = true;
            this.rdoDecisionYes.Dock = System.Windows.Forms.DockStyle.Top;
            this.rdoDecisionYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rdoDecisionYes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdoDecisionYes.Location = new System.Drawing.Point(3, 42);
            this.rdoDecisionYes.Name = "rdoDecisionYes";
            this.rdoDecisionYes.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.rdoDecisionYes.Size = new System.Drawing.Size(942, 24);
            this.rdoDecisionYes.TabIndex = 3;
            this.rdoDecisionYes.TabStop = true;
            this.rdoDecisionYes.Text = "rdoDecisionYes";
            this.rdoDecisionYes.UseVisualStyleBackColor = true;
            // 
            // rdoDecisionNo
            // 
            this.rdoDecisionNo.AutoSize = true;
            this.rdoDecisionNo.Dock = System.Windows.Forms.DockStyle.Top;
            this.rdoDecisionNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rdoDecisionNo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdoDecisionNo.Location = new System.Drawing.Point(3, 18);
            this.rdoDecisionNo.Name = "rdoDecisionNo";
            this.rdoDecisionNo.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.rdoDecisionNo.Size = new System.Drawing.Size(942, 24);
            this.rdoDecisionNo.TabIndex = 2;
            this.rdoDecisionNo.TabStop = true;
            this.rdoDecisionNo.Text = "rdoDecisionNo";
            this.rdoDecisionNo.UseVisualStyleBackColor = true;
            // 
            // splTargetVariable
            // 
            this.splTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splTargetVariable.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splTargetVariable.Location = new System.Drawing.Point(0, 80);
            this.splTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            this.splTargetVariable.Name = "splTargetVariable";
            // 
            // splTargetVariable.Panel1
            // 
            this.splTargetVariable.Panel1.Controls.Add(this.grpTargetVariable);
            // 
            // splTargetVariable.Panel2
            // 
            this.splTargetVariable.Panel2.Controls.Add(this.splLDsityObject);
            this.splTargetVariable.Size = new System.Drawing.Size(954, 324);
            this.splTargetVariable.SplitterDistance = 200;
            this.splTargetVariable.SplitterWidth = 1;
            this.splTargetVariable.TabIndex = 1;
            // 
            // grpTargetVariable
            // 
            this.grpTargetVariable.Controls.Add(this.tlpTargetVariable);
            this.grpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpTargetVariable.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpTargetVariable.Location = new System.Drawing.Point(0, 0);
            this.grpTargetVariable.Name = "grpTargetVariable";
            this.grpTargetVariable.Size = new System.Drawing.Size(200, 324);
            this.grpTargetVariable.TabIndex = 5;
            this.grpTargetVariable.TabStop = false;
            this.grpTargetVariable.Text = "grpTargetVariable";
            // 
            // tlpTargetVariable
            // 
            this.tlpTargetVariable.ColumnCount = 1;
            this.tlpTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTargetVariable.Controls.Add(this.tsrTargetVariable, 0, 0);
            this.tlpTargetVariable.Controls.Add(this.splTargetVariableList, 0, 1);
            this.tlpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpTargetVariable.Location = new System.Drawing.Point(3, 18);
            this.tlpTargetVariable.Name = "tlpTargetVariable";
            this.tlpTargetVariable.RowCount = 2;
            this.tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTargetVariable.Size = new System.Drawing.Size(194, 303);
            this.tlpTargetVariable.TabIndex = 0;
            // 
            // tsrTargetVariable
            // 
            this.tsrTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrTargetVariable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnTargetVariableCopy,
            this.btnTargetVariableAdd,
            this.btnTargetVariableDelete});
            this.tsrTargetVariable.Location = new System.Drawing.Point(0, 0);
            this.tsrTargetVariable.Name = "tsrTargetVariable";
            this.tsrTargetVariable.Size = new System.Drawing.Size(194, 25);
            this.tsrTargetVariable.TabIndex = 1;
            this.tsrTargetVariable.Text = "tsrTargetVariable";
            // 
            // btnTargetVariableAdd
            // 
            this.btnTargetVariableAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTargetVariableAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnTargetVariableAdd.Image")));
            this.btnTargetVariableAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTargetVariableAdd.Name = "btnTargetVariableAdd";
            this.btnTargetVariableAdd.Size = new System.Drawing.Size(23, 22);
            this.btnTargetVariableAdd.Text = "btnTargetVariableAdd";
            // 
            // btnTargetVariableDelete
            // 
            this.btnTargetVariableDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTargetVariableDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnTargetVariableDelete.Image")));
            this.btnTargetVariableDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTargetVariableDelete.Name = "btnTargetVariableDelete";
            this.btnTargetVariableDelete.Size = new System.Drawing.Size(23, 22);
            this.btnTargetVariableDelete.Text = "btnTargetVariableDelete";
            // 
            // splTargetVariableList
            // 
            this.splTargetVariableList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splTargetVariableList.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splTargetVariableList.IsSplitterFixed = true;
            this.splTargetVariableList.Location = new System.Drawing.Point(0, 25);
            this.splTargetVariableList.Margin = new System.Windows.Forms.Padding(0);
            this.splTargetVariableList.Name = "splTargetVariableList";
            this.splTargetVariableList.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splTargetVariableList.Panel1
            // 
            this.splTargetVariableList.Panel1.Controls.Add(this.pnlTargetVariableCore);
            // 
            // splTargetVariableList.Panel2
            // 
            this.splTargetVariableList.Panel2.Controls.Add(this.pnlTargetVariableDivision);
            this.splTargetVariableList.Size = new System.Drawing.Size(194, 278);
            this.splTargetVariableList.SplitterDistance = 100;
            this.splTargetVariableList.TabIndex = 2;
            // 
            // pnlTargetVariableCore
            // 
            this.pnlTargetVariableCore.Controls.Add(this.lstTargetVariableCore);
            this.pnlTargetVariableCore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTargetVariableCore.Location = new System.Drawing.Point(0, 0);
            this.pnlTargetVariableCore.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTargetVariableCore.Name = "pnlTargetVariableCore";
            this.pnlTargetVariableCore.Size = new System.Drawing.Size(194, 100);
            this.pnlTargetVariableCore.TabIndex = 0;
            // 
            // lstTargetVariableCore
            // 
            this.lstTargetVariableCore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstTargetVariableCore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstTargetVariableCore.FormattingEnabled = true;
            this.lstTargetVariableCore.ItemHeight = 15;
            this.lstTargetVariableCore.Location = new System.Drawing.Point(0, 0);
            this.lstTargetVariableCore.Margin = new System.Windows.Forms.Padding(0);
            this.lstTargetVariableCore.Name = "lstTargetVariableCore";
            this.lstTargetVariableCore.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstTargetVariableCore.Size = new System.Drawing.Size(194, 100);
            this.lstTargetVariableCore.TabIndex = 1;
            // 
            // pnlTargetVariableDivision
            // 
            this.pnlTargetVariableDivision.Controls.Add(this.lstTargetVariableDivision);
            this.pnlTargetVariableDivision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTargetVariableDivision.Location = new System.Drawing.Point(0, 0);
            this.pnlTargetVariableDivision.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTargetVariableDivision.Name = "pnlTargetVariableDivision";
            this.pnlTargetVariableDivision.Size = new System.Drawing.Size(194, 174);
            this.pnlTargetVariableDivision.TabIndex = 0;
            // 
            // lstTargetVariableDivision
            // 
            this.lstTargetVariableDivision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstTargetVariableDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstTargetVariableDivision.FormattingEnabled = true;
            this.lstTargetVariableDivision.ItemHeight = 15;
            this.lstTargetVariableDivision.Location = new System.Drawing.Point(0, 0);
            this.lstTargetVariableDivision.Margin = new System.Windows.Forms.Padding(0);
            this.lstTargetVariableDivision.Name = "lstTargetVariableDivision";
            this.lstTargetVariableDivision.Size = new System.Drawing.Size(194, 174);
            this.lstTargetVariableDivision.TabIndex = 2;
            // 
            // splLDsityObject
            // 
            this.splLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splLDsityObject.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splLDsityObject.Location = new System.Drawing.Point(0, 0);
            this.splLDsityObject.Margin = new System.Windows.Forms.Padding(0);
            this.splLDsityObject.Name = "splLDsityObject";
            // 
            // splLDsityObject.Panel1
            // 
            this.splLDsityObject.Panel1.Controls.Add(this.grpLDsityObject);
            // 
            // splLDsityObject.Panel2
            // 
            this.splLDsityObject.Panel2.Controls.Add(this.splLDsity);
            this.splLDsityObject.Size = new System.Drawing.Size(753, 324);
            this.splLDsityObject.SplitterDistance = 200;
            this.splLDsityObject.SplitterWidth = 1;
            this.splLDsityObject.TabIndex = 2;
            // 
            // grpLDsityObject
            // 
            this.grpLDsityObject.Controls.Add(this.tlpLDsityObject);
            this.grpLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpLDsityObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpLDsityObject.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpLDsityObject.Location = new System.Drawing.Point(0, 0);
            this.grpLDsityObject.Name = "grpLDsityObject";
            this.grpLDsityObject.Size = new System.Drawing.Size(200, 324);
            this.grpLDsityObject.TabIndex = 7;
            this.grpLDsityObject.TabStop = false;
            this.grpLDsityObject.Text = "grpLDsityObject";
            // 
            // tlpLDsityObject
            // 
            this.tlpLDsityObject.ColumnCount = 1;
            this.tlpLDsityObject.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObject.Controls.Add(this.tsrLDsityObject, 0, 0);
            this.tlpLDsityObject.Controls.Add(this.splLDsityObjectList, 0, 1);
            this.tlpLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLDsityObject.Location = new System.Drawing.Point(3, 18);
            this.tlpLDsityObject.Name = "tlpLDsityObject";
            this.tlpLDsityObject.RowCount = 2;
            this.tlpLDsityObject.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpLDsityObject.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsityObject.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLDsityObject.Size = new System.Drawing.Size(194, 303);
            this.tlpLDsityObject.TabIndex = 0;
            // 
            // tsrLDsityObject
            // 
            this.tsrLDsityObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrLDsityObject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLDsityObjectEdit});
            this.tsrLDsityObject.Location = new System.Drawing.Point(0, 0);
            this.tsrLDsityObject.Name = "tsrLDsityObject";
            this.tsrLDsityObject.Size = new System.Drawing.Size(194, 25);
            this.tsrLDsityObject.TabIndex = 5;
            this.tsrLDsityObject.Text = "tsrLDsityObject";
            // 
            // btnLDsityObjectEdit
            // 
            this.btnLDsityObjectEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLDsityObjectEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnLDsityObjectEdit.Image")));
            this.btnLDsityObjectEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLDsityObjectEdit.Name = "btnLDsityObjectEdit";
            this.btnLDsityObjectEdit.Size = new System.Drawing.Size(23, 22);
            this.btnLDsityObjectEdit.Text = "btnLDsityObjectEdit";
            this.btnLDsityObjectEdit.Visible = false;
            // 
            // splLDsityObjectList
            // 
            this.splLDsityObjectList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splLDsityObjectList.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splLDsityObjectList.IsSplitterFixed = true;
            this.splLDsityObjectList.Location = new System.Drawing.Point(0, 25);
            this.splLDsityObjectList.Margin = new System.Windows.Forms.Padding(0);
            this.splLDsityObjectList.Name = "splLDsityObjectList";
            this.splLDsityObjectList.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splLDsityObjectList.Panel1
            // 
            this.splLDsityObjectList.Panel1.Controls.Add(this.pnlLDsityObjectCore);
            // 
            // splLDsityObjectList.Panel2
            // 
            this.splLDsityObjectList.Panel2.Controls.Add(this.pnlLDsityObjectDivision);
            this.splLDsityObjectList.Size = new System.Drawing.Size(194, 278);
            this.splLDsityObjectList.SplitterDistance = 100;
            this.splLDsityObjectList.TabIndex = 6;
            // 
            // pnlLDsityObjectCore
            // 
            this.pnlLDsityObjectCore.Controls.Add(this.lstLDsityObjectCore);
            this.pnlLDsityObjectCore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLDsityObjectCore.Location = new System.Drawing.Point(0, 0);
            this.pnlLDsityObjectCore.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLDsityObjectCore.Name = "pnlLDsityObjectCore";
            this.pnlLDsityObjectCore.Size = new System.Drawing.Size(194, 100);
            this.pnlLDsityObjectCore.TabIndex = 0;
            // 
            // lstLDsityObjectCore
            // 
            this.lstLDsityObjectCore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLDsityObjectCore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstLDsityObjectCore.FormattingEnabled = true;
            this.lstLDsityObjectCore.ItemHeight = 15;
            this.lstLDsityObjectCore.Location = new System.Drawing.Point(0, 0);
            this.lstLDsityObjectCore.Margin = new System.Windows.Forms.Padding(0);
            this.lstLDsityObjectCore.Name = "lstLDsityObjectCore";
            this.lstLDsityObjectCore.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstLDsityObjectCore.Size = new System.Drawing.Size(194, 100);
            this.lstLDsityObjectCore.TabIndex = 2;
            // 
            // pnlLDsityObjectDivision
            // 
            this.pnlLDsityObjectDivision.Controls.Add(this.lstLDsityObjectDivision);
            this.pnlLDsityObjectDivision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLDsityObjectDivision.Location = new System.Drawing.Point(0, 0);
            this.pnlLDsityObjectDivision.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLDsityObjectDivision.Name = "pnlLDsityObjectDivision";
            this.pnlLDsityObjectDivision.Size = new System.Drawing.Size(194, 174);
            this.pnlLDsityObjectDivision.TabIndex = 0;
            // 
            // lstLDsityObjectDivision
            // 
            this.lstLDsityObjectDivision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLDsityObjectDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstLDsityObjectDivision.FormattingEnabled = true;
            this.lstLDsityObjectDivision.ItemHeight = 15;
            this.lstLDsityObjectDivision.Location = new System.Drawing.Point(0, 0);
            this.lstLDsityObjectDivision.Margin = new System.Windows.Forms.Padding(0);
            this.lstLDsityObjectDivision.Name = "lstLDsityObjectDivision";
            this.lstLDsityObjectDivision.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstLDsityObjectDivision.Size = new System.Drawing.Size(194, 174);
            this.lstLDsityObjectDivision.TabIndex = 2;
            // 
            // splLDsity
            // 
            this.splLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splLDsity.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splLDsity.Location = new System.Drawing.Point(0, 0);
            this.splLDsity.Margin = new System.Windows.Forms.Padding(0);
            this.splLDsity.Name = "splLDsity";
            // 
            // splLDsity.Panel1
            // 
            this.splLDsity.Panel1.Controls.Add(this.grpLDsity);
            // 
            // splLDsity.Panel2
            // 
            this.splLDsity.Panel2.Controls.Add(this.splInformationList);
            this.splLDsity.Size = new System.Drawing.Size(552, 324);
            this.splLDsity.SplitterDistance = 200;
            this.splLDsity.SplitterWidth = 1;
            this.splLDsity.TabIndex = 2;
            // 
            // grpLDsity
            // 
            this.grpLDsity.Controls.Add(this.tlpLDsity);
            this.grpLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpLDsity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpLDsity.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpLDsity.Location = new System.Drawing.Point(0, 0);
            this.grpLDsity.Name = "grpLDsity";
            this.grpLDsity.Size = new System.Drawing.Size(200, 324);
            this.grpLDsity.TabIndex = 6;
            this.grpLDsity.TabStop = false;
            this.grpLDsity.Text = "grpLDsity";
            // 
            // tlpLDsity
            // 
            this.tlpLDsity.ColumnCount = 1;
            this.tlpLDsity.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsity.Controls.Add(this.tsrLDsity, 0, 0);
            this.tlpLDsity.Controls.Add(this.splLDsityList, 0, 1);
            this.tlpLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLDsity.Location = new System.Drawing.Point(3, 18);
            this.tlpLDsity.Name = "tlpLDsity";
            this.tlpLDsity.RowCount = 2;
            this.tlpLDsity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpLDsity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLDsity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLDsity.Size = new System.Drawing.Size(194, 303);
            this.tlpLDsity.TabIndex = 0;
            // 
            // tsrLDsity
            // 
            this.tsrLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrLDsity.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLDsityEdit});
            this.tsrLDsity.Location = new System.Drawing.Point(0, 0);
            this.tsrLDsity.Name = "tsrLDsity";
            this.tsrLDsity.Size = new System.Drawing.Size(194, 25);
            this.tsrLDsity.TabIndex = 5;
            this.tsrLDsity.Text = "tsrLDsity";
            // 
            // btnLDsityEdit
            // 
            this.btnLDsityEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLDsityEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnLDsityEdit.Image")));
            this.btnLDsityEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLDsityEdit.Name = "btnLDsityEdit";
            this.btnLDsityEdit.Size = new System.Drawing.Size(23, 22);
            this.btnLDsityEdit.Text = "btnLDsityEdit";
            this.btnLDsityEdit.Visible = false;
            // 
            // splLDsityList
            // 
            this.splLDsityList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splLDsityList.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splLDsityList.IsSplitterFixed = true;
            this.splLDsityList.Location = new System.Drawing.Point(0, 25);
            this.splLDsityList.Margin = new System.Windows.Forms.Padding(0);
            this.splLDsityList.Name = "splLDsityList";
            this.splLDsityList.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splLDsityList.Panel1
            // 
            this.splLDsityList.Panel1.Controls.Add(this.pnlLDsityCore);
            // 
            // splLDsityList.Panel2
            // 
            this.splLDsityList.Panel2.Controls.Add(this.pnlLDsityDivision);
            this.splLDsityList.Size = new System.Drawing.Size(194, 278);
            this.splLDsityList.SplitterDistance = 100;
            this.splLDsityList.TabIndex = 6;
            // 
            // pnlLDsityCore
            // 
            this.pnlLDsityCore.Controls.Add(this.lstLDsityCore);
            this.pnlLDsityCore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLDsityCore.Location = new System.Drawing.Point(0, 0);
            this.pnlLDsityCore.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLDsityCore.Name = "pnlLDsityCore";
            this.pnlLDsityCore.Size = new System.Drawing.Size(194, 100);
            this.pnlLDsityCore.TabIndex = 0;
            // 
            // lstLDsityCore
            // 
            this.lstLDsityCore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLDsityCore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstLDsityCore.FormattingEnabled = true;
            this.lstLDsityCore.ItemHeight = 15;
            this.lstLDsityCore.Location = new System.Drawing.Point(0, 0);
            this.lstLDsityCore.Margin = new System.Windows.Forms.Padding(0);
            this.lstLDsityCore.Name = "lstLDsityCore";
            this.lstLDsityCore.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstLDsityCore.Size = new System.Drawing.Size(194, 100);
            this.lstLDsityCore.TabIndex = 2;
            // 
            // pnlLDsityDivision
            // 
            this.pnlLDsityDivision.Controls.Add(this.lstLDsityDivision);
            this.pnlLDsityDivision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLDsityDivision.Location = new System.Drawing.Point(0, 0);
            this.pnlLDsityDivision.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLDsityDivision.Name = "pnlLDsityDivision";
            this.pnlLDsityDivision.Size = new System.Drawing.Size(194, 174);
            this.pnlLDsityDivision.TabIndex = 0;
            // 
            // lstLDsityDivision
            // 
            this.lstLDsityDivision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLDsityDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lstLDsityDivision.FormattingEnabled = true;
            this.lstLDsityDivision.ItemHeight = 15;
            this.lstLDsityDivision.Location = new System.Drawing.Point(0, 0);
            this.lstLDsityDivision.Margin = new System.Windows.Forms.Padding(0);
            this.lstLDsityDivision.Name = "lstLDsityDivision";
            this.lstLDsityDivision.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstLDsityDivision.Size = new System.Drawing.Size(194, 174);
            this.lstLDsityDivision.TabIndex = 2;
            // 
            // splInformationList
            // 
            this.splInformationList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splInformationList.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splInformationList.IsSplitterFixed = true;
            this.splInformationList.Location = new System.Drawing.Point(0, 0);
            this.splInformationList.Margin = new System.Windows.Forms.Padding(0);
            this.splInformationList.Name = "splInformationList";
            this.splInformationList.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splInformationList.Panel1
            // 
            this.splInformationList.Panel1.Controls.Add(this.pnlInformationCore);
            // 
            // splInformationList.Panel2
            // 
            this.splInformationList.Panel2.Controls.Add(this.pnlInformationDivision);
            this.splInformationList.Size = new System.Drawing.Size(351, 324);
            this.splInformationList.SplitterDistance = 145;
            this.splInformationList.SplitterWidth = 1;
            this.splInformationList.TabIndex = 7;
            // 
            // pnlInformationCore
            // 
            this.pnlInformationCore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInformationCore.Location = new System.Drawing.Point(0, 0);
            this.pnlInformationCore.Margin = new System.Windows.Forms.Padding(0);
            this.pnlInformationCore.Name = "pnlInformationCore";
            this.pnlInformationCore.Size = new System.Drawing.Size(351, 145);
            this.pnlInformationCore.TabIndex = 2;
            // 
            // pnlInformationDivision
            // 
            this.pnlInformationDivision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInformationDivision.Location = new System.Drawing.Point(0, 0);
            this.pnlInformationDivision.Margin = new System.Windows.Forms.Padding(0);
            this.pnlInformationDivision.Name = "pnlInformationDivision";
            this.pnlInformationDivision.Size = new System.Drawing.Size(351, 178);
            this.pnlInformationDivision.TabIndex = 2;
            // 
            // pnlCaption
            // 
            this.pnlCaption.Controls.Add(this.splCaption);
            this.pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCaption.Location = new System.Drawing.Point(2, 2);
            this.pnlCaption.Margin = new System.Windows.Forms.Padding(2);
            this.pnlCaption.Name = "pnlCaption";
            this.pnlCaption.Size = new System.Drawing.Size(956, 86);
            this.pnlCaption.TabIndex = 10;
            // 
            // splCaption
            // 
            this.splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCaption.IsSplitterFixed = true;
            this.splCaption.Location = new System.Drawing.Point(0, 0);
            this.splCaption.Margin = new System.Windows.Forms.Padding(0);
            this.splCaption.Name = "splCaption";
            this.splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            this.splCaption.Panel1.Controls.Add(this.lblMainCaption);
            // 
            // splCaption.Panel2
            // 
            this.splCaption.Panel2.Controls.Add(this.tlpCaption);
            this.splCaption.Size = new System.Drawing.Size(956, 86);
            this.splCaption.SplitterDistance = 30;
            this.splCaption.TabIndex = 14;
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(0, 0);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(956, 30);
            this.lblMainCaption.TabIndex = 0;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpCaption
            // 
            this.tlpCaption.ColumnCount = 2;
            this.tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaption.Controls.Add(this.lblSelectedLDsityObjectGroupValue, 1, 0);
            this.tlpCaption.Controls.Add(this.lblSelectedLDsityObjectGroupCaption, 0, 0);
            this.tlpCaption.Controls.Add(this.lblSelectedTargetVariableCaption, 0, 1);
            this.tlpCaption.Controls.Add(this.lblSelectedTargetVariableValue, 1, 1);
            this.tlpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCaption.Location = new System.Drawing.Point(0, 0);
            this.tlpCaption.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCaption.Name = "tlpCaption";
            this.tlpCaption.RowCount = 3;
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaption.Size = new System.Drawing.Size(956, 52);
            this.tlpCaption.TabIndex = 5;
            // 
            // lblSelectedLDsityObjectGroupValue
            // 
            this.lblSelectedLDsityObjectGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedLDsityObjectGroupValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedLDsityObjectGroupValue.Location = new System.Drawing.Point(283, 0);
            this.lblSelectedLDsityObjectGroupValue.Name = "lblSelectedLDsityObjectGroupValue";
            this.lblSelectedLDsityObjectGroupValue.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedLDsityObjectGroupValue.Size = new System.Drawing.Size(670, 25);
            this.lblSelectedLDsityObjectGroupValue.TabIndex = 4;
            this.lblSelectedLDsityObjectGroupValue.Text = "lblSelectedLDsityObjectGroupValue";
            this.lblSelectedLDsityObjectGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedLDsityObjectGroupCaption
            // 
            this.lblSelectedLDsityObjectGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedLDsityObjectGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedLDsityObjectGroupCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSelectedLDsityObjectGroupCaption.Location = new System.Drawing.Point(0, 0);
            this.lblSelectedLDsityObjectGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblSelectedLDsityObjectGroupCaption.Name = "lblSelectedLDsityObjectGroupCaption";
            this.lblSelectedLDsityObjectGroupCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedLDsityObjectGroupCaption.Size = new System.Drawing.Size(280, 25);
            this.lblSelectedLDsityObjectGroupCaption.TabIndex = 0;
            this.lblSelectedLDsityObjectGroupCaption.Text = "lblSelectedLDsityObjectGroupCaption";
            this.lblSelectedLDsityObjectGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableCaption
            // 
            this.lblSelectedTargetVariableCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedTargetVariableCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedTargetVariableCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSelectedTargetVariableCaption.Location = new System.Drawing.Point(0, 25);
            this.lblSelectedTargetVariableCaption.Margin = new System.Windows.Forms.Padding(0);
            this.lblSelectedTargetVariableCaption.Name = "lblSelectedTargetVariableCaption";
            this.lblSelectedTargetVariableCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedTargetVariableCaption.Size = new System.Drawing.Size(280, 25);
            this.lblSelectedTargetVariableCaption.TabIndex = 5;
            this.lblSelectedTargetVariableCaption.Text = "lblSelectedTargetVariableCaption";
            this.lblSelectedTargetVariableCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableValue
            // 
            this.lblSelectedTargetVariableValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSelectedTargetVariableValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSelectedTargetVariableValue.Location = new System.Drawing.Point(283, 25);
            this.lblSelectedTargetVariableValue.Name = "lblSelectedTargetVariableValue";
            this.lblSelectedTargetVariableValue.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblSelectedTargetVariableValue.Size = new System.Drawing.Size(670, 25);
            this.lblSelectedTargetVariableValue.TabIndex = 6;
            this.lblSelectedTargetVariableValue.Text = "lblSelectedTargetVariableValue";
            this.lblSelectedTargetVariableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnTargetVariableCopy
            // 
            this.btnTargetVariableCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTargetVariableCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnTargetVariableCopy.Image")));
            this.btnTargetVariableCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTargetVariableCopy.Name = "btnTargetVariableCopy";
            this.btnTargetVariableCopy.Size = new System.Drawing.Size(23, 22);
            this.btnTargetVariableCopy.Text = "btnTargetVariableCopy";
            // 
            // ControlTDSelectionTargetVariableDivision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Name = "ControlTDSelectionTargetVariableDivision";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlNext.ResumeLayout(false);
            this.pnlPrevious.ResumeLayout(false);
            this.pnlWorkSpace.ResumeLayout(false);
            this.tlpWorkSpace.ResumeLayout(false);
            this.grpDecision.ResumeLayout(false);
            this.grpDecision.PerformLayout();
            this.splTargetVariable.Panel1.ResumeLayout(false);
            this.splTargetVariable.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splTargetVariable)).EndInit();
            this.splTargetVariable.ResumeLayout(false);
            this.grpTargetVariable.ResumeLayout(false);
            this.tlpTargetVariable.ResumeLayout(false);
            this.tlpTargetVariable.PerformLayout();
            this.tsrTargetVariable.ResumeLayout(false);
            this.tsrTargetVariable.PerformLayout();
            this.splTargetVariableList.Panel1.ResumeLayout(false);
            this.splTargetVariableList.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splTargetVariableList)).EndInit();
            this.splTargetVariableList.ResumeLayout(false);
            this.pnlTargetVariableCore.ResumeLayout(false);
            this.pnlTargetVariableDivision.ResumeLayout(false);
            this.splLDsityObject.Panel1.ResumeLayout(false);
            this.splLDsityObject.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObject)).EndInit();
            this.splLDsityObject.ResumeLayout(false);
            this.grpLDsityObject.ResumeLayout(false);
            this.tlpLDsityObject.ResumeLayout(false);
            this.tlpLDsityObject.PerformLayout();
            this.tsrLDsityObject.ResumeLayout(false);
            this.tsrLDsityObject.PerformLayout();
            this.splLDsityObjectList.Panel1.ResumeLayout(false);
            this.splLDsityObjectList.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityObjectList)).EndInit();
            this.splLDsityObjectList.ResumeLayout(false);
            this.pnlLDsityObjectCore.ResumeLayout(false);
            this.pnlLDsityObjectDivision.ResumeLayout(false);
            this.splLDsity.Panel1.ResumeLayout(false);
            this.splLDsity.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splLDsity)).EndInit();
            this.splLDsity.ResumeLayout(false);
            this.grpLDsity.ResumeLayout(false);
            this.tlpLDsity.ResumeLayout(false);
            this.tlpLDsity.PerformLayout();
            this.tsrLDsity.ResumeLayout(false);
            this.tsrLDsity.PerformLayout();
            this.splLDsityList.Panel1.ResumeLayout(false);
            this.splLDsityList.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splLDsityList)).EndInit();
            this.splLDsityList.ResumeLayout(false);
            this.pnlLDsityCore.ResumeLayout(false);
            this.pnlLDsityDivision.ResumeLayout(false);
            this.splInformationList.Panel1.ResumeLayout(false);
            this.splInformationList.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splInformationList)).EndInit();
            this.splInformationList.ResumeLayout(false);
            this.pnlCaption.ResumeLayout(false);
            this.splCaption.Panel1.ResumeLayout(false);
            this.splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCaption)).EndInit();
            this.splCaption.ResumeLayout(false);
            this.tlpCaption.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.GroupBox grpDecision;
        private System.Windows.Forms.RadioButton rdoDecisionNo;
        private System.Windows.Forms.TableLayoutPanel tlpCaption;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupValue;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableValue;
        private System.Windows.Forms.RadioButton rdoDecisionYes;
        private System.Windows.Forms.SplitContainer splTargetVariable;
        private System.Windows.Forms.GroupBox grpTargetVariable;
        private System.Windows.Forms.TableLayoutPanel tlpTargetVariable;
        private System.Windows.Forms.ToolStrip tsrTargetVariable;
        private System.Windows.Forms.ToolStripButton btnTargetVariableAdd;
        private System.Windows.Forms.ToolStripButton btnTargetVariableDelete;
        private System.Windows.Forms.SplitContainer splTargetVariableList;
        private System.Windows.Forms.Panel pnlTargetVariableCore;
        private System.Windows.Forms.ListBox lstTargetVariableCore;
        private System.Windows.Forms.Panel pnlTargetVariableDivision;
        private System.Windows.Forms.ListBox lstTargetVariableDivision;
        private System.Windows.Forms.SplitContainer splLDsityObject;
        private System.Windows.Forms.SplitContainer splLDsity;
        private System.Windows.Forms.GroupBox grpLDsity;
        private System.Windows.Forms.TableLayoutPanel tlpLDsity;
        private System.Windows.Forms.ToolStrip tsrLDsity;
        private System.Windows.Forms.ToolStripButton btnLDsityEdit;
        private System.Windows.Forms.SplitContainer splLDsityList;
        private System.Windows.Forms.Panel pnlLDsityCore;
        private System.Windows.Forms.ListBox lstLDsityCore;
        private System.Windows.Forms.Panel pnlLDsityDivision;
        private System.Windows.Forms.ListBox lstLDsityDivision;
        private System.Windows.Forms.GroupBox grpLDsityObject;
        private System.Windows.Forms.TableLayoutPanel tlpLDsityObject;
        private System.Windows.Forms.ToolStrip tsrLDsityObject;
        private System.Windows.Forms.ToolStripButton btnLDsityObjectEdit;
        private System.Windows.Forms.SplitContainer splLDsityObjectList;
        private System.Windows.Forms.Panel pnlLDsityObjectCore;
        private System.Windows.Forms.ListBox lstLDsityObjectCore;
        private System.Windows.Forms.Panel pnlLDsityObjectDivision;
        private System.Windows.Forms.ListBox lstLDsityObjectDivision;
        private System.Windows.Forms.SplitContainer splInformationList;
        private System.Windows.Forms.Panel pnlInformationCore;
        private System.Windows.Forms.Panel pnlInformationDivision;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.ToolStripButton btnTargetVariableCopy;
    }

}
