﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlTDSelectionTargetVariableCore
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlTDSelectionTargetVariableCore));
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            pnlWorkSpace = new System.Windows.Forms.Panel();
            splTargetVariable = new System.Windows.Forms.SplitContainer();
            grpTargetVariable = new System.Windows.Forms.GroupBox();
            tlpTargetVariable = new System.Windows.Forms.TableLayoutPanel();
            pnlTargetVariable = new System.Windows.Forms.Panel();
            lstTargetVariable = new System.Windows.Forms.ListBox();
            tsrTargetVariable = new System.Windows.Forms.ToolStrip();
            btnTargetVariableEdit = new System.Windows.Forms.ToolStripButton();
            btnTargetVariableCopy = new System.Windows.Forms.ToolStripButton();
            btnTargetVariableAdd = new System.Windows.Forms.ToolStripButton();
            btnTargetVariableDelete = new System.Windows.Forms.ToolStripButton();
            pnlStateOrChange = new System.Windows.Forms.Panel();
            cboStateOrChange = new System.Windows.Forms.ComboBox();
            splLDsity = new System.Windows.Forms.SplitContainer();
            grpLDsity = new System.Windows.Forms.GroupBox();
            tlpLDsity = new System.Windows.Forms.TableLayoutPanel();
            tsrLDsity = new System.Windows.Forms.ToolStrip();
            btnLDsityEdit = new System.Windows.Forms.ToolStripButton();
            pnlLDsity = new System.Windows.Forms.Panel();
            lstLDsity = new System.Windows.Forms.ListBox();
            pnlLDsityInformation = new System.Windows.Forms.Panel();
            pnlCaption = new System.Windows.Forms.Panel();
            splCaption = new System.Windows.Forms.SplitContainer();
            lblCaptionMain = new System.Windows.Forms.TableLayoutPanel();
            lblMainCaption = new System.Windows.Forms.Label();
            tlpCaption = new System.Windows.Forms.TableLayoutPanel();
            lblSelectedLDsityObjectGroupValue = new System.Windows.Forms.Label();
            lblSelectedLDsityObjectGroupCaption = new System.Windows.Forms.Label();
            lblSelectedTargetVariableCaption = new System.Windows.Forms.Label();
            lblSelectedTargetVariableValue = new System.Windows.Forms.Label();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            pnlWorkSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splTargetVariable).BeginInit();
            splTargetVariable.Panel1.SuspendLayout();
            splTargetVariable.Panel2.SuspendLayout();
            splTargetVariable.SuspendLayout();
            grpTargetVariable.SuspendLayout();
            tlpTargetVariable.SuspendLayout();
            pnlTargetVariable.SuspendLayout();
            tsrTargetVariable.SuspendLayout();
            pnlStateOrChange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splLDsity).BeginInit();
            splLDsity.Panel1.SuspendLayout();
            splLDsity.Panel2.SuspendLayout();
            splLDsity.SuspendLayout();
            grpLDsity.SuspendLayout();
            tlpLDsity.SuspendLayout();
            tsrLDsity.SuspendLayout();
            pnlLDsity.SuspendLayout();
            pnlCaption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splCaption).BeginInit();
            splCaption.Panel1.SuspendLayout();
            splCaption.Panel2.SuspendLayout();
            splCaption.SuspendLayout();
            lblCaptionMain.SuspendLayout();
            tlpCaption.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(1120, 623);
            pnlMain.TabIndex = 2;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 2);
            tlpMain.Controls.Add(pnlWorkSpace, 0, 1);
            tlpMain.Controls.Add(pnlCaption, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1120, 623);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 577);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(1120, 46);
            tlpButtons.TabIndex = 14;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(933, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(6);
            pnlNext.Size = new System.Drawing.Size(187, 46);
            pnlNext.TabIndex = 16;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnNext.Location = new System.Drawing.Point(6, 6);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(175, 34);
            btnNext.TabIndex = 15;
            btnNext.Text = "btnNext";
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(746, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(6);
            pnlPrevious.Size = new System.Drawing.Size(187, 46);
            pnlPrevious.TabIndex = 17;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnPrevious.Location = new System.Drawing.Point(6, 6);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(175, 34);
            btnPrevious.TabIndex = 15;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // pnlWorkSpace
            // 
            pnlWorkSpace.Controls.Add(splTargetVariable);
            pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlWorkSpace.Location = new System.Drawing.Point(4, 107);
            pnlWorkSpace.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlWorkSpace.Name = "pnlWorkSpace";
            pnlWorkSpace.Size = new System.Drawing.Size(1112, 467);
            pnlWorkSpace.TabIndex = 9;
            // 
            // splTargetVariable
            // 
            splTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            splTargetVariable.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splTargetVariable.Location = new System.Drawing.Point(0, 0);
            splTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            splTargetVariable.Name = "splTargetVariable";
            // 
            // splTargetVariable.Panel1
            // 
            splTargetVariable.Panel1.Controls.Add(grpTargetVariable);
            // 
            // splTargetVariable.Panel2
            // 
            splTargetVariable.Panel2.Controls.Add(splLDsity);
            splTargetVariable.Size = new System.Drawing.Size(1112, 467);
            splTargetVariable.SplitterDistance = 321;
            splTargetVariable.SplitterWidth = 5;
            splTargetVariable.TabIndex = 9;
            // 
            // grpTargetVariable
            // 
            grpTargetVariable.Controls.Add(tlpTargetVariable);
            grpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            grpTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpTargetVariable.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpTargetVariable.Location = new System.Drawing.Point(0, 0);
            grpTargetVariable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpTargetVariable.Name = "grpTargetVariable";
            grpTargetVariable.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpTargetVariable.Size = new System.Drawing.Size(321, 467);
            grpTargetVariable.TabIndex = 4;
            grpTargetVariable.TabStop = false;
            grpTargetVariable.Text = "grpTargetVariable";
            // 
            // tlpTargetVariable
            // 
            tlpTargetVariable.ColumnCount = 1;
            tlpTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpTargetVariable.Controls.Add(pnlTargetVariable, 0, 2);
            tlpTargetVariable.Controls.Add(tsrTargetVariable, 0, 0);
            tlpTargetVariable.Controls.Add(pnlStateOrChange, 0, 1);
            tlpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpTargetVariable.Location = new System.Drawing.Point(4, 18);
            tlpTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            tlpTargetVariable.Name = "tlpTargetVariable";
            tlpTargetVariable.RowCount = 3;
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpTargetVariable.Size = new System.Drawing.Size(313, 446);
            tlpTargetVariable.TabIndex = 0;
            // 
            // pnlTargetVariable
            // 
            pnlTargetVariable.Controls.Add(lstTargetVariable);
            pnlTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTargetVariable.Location = new System.Drawing.Point(0, 58);
            pnlTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            pnlTargetVariable.Name = "pnlTargetVariable";
            pnlTargetVariable.Size = new System.Drawing.Size(313, 388);
            pnlTargetVariable.TabIndex = 2;
            // 
            // lstTargetVariable
            // 
            lstTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            lstTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lstTargetVariable.FormattingEnabled = true;
            lstTargetVariable.ItemHeight = 15;
            lstTargetVariable.Location = new System.Drawing.Point(0, 0);
            lstTargetVariable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            lstTargetVariable.Name = "lstTargetVariable";
            lstTargetVariable.Size = new System.Drawing.Size(313, 388);
            lstTargetVariable.TabIndex = 3;
            // 
            // tsrTargetVariable
            // 
            tsrTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrTargetVariable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnTargetVariableEdit, btnTargetVariableCopy, btnTargetVariableAdd, btnTargetVariableDelete });
            tsrTargetVariable.Location = new System.Drawing.Point(0, 0);
            tsrTargetVariable.Name = "tsrTargetVariable";
            tsrTargetVariable.Size = new System.Drawing.Size(313, 29);
            tsrTargetVariable.TabIndex = 0;
            tsrTargetVariable.Text = "tsrTargetVariable";
            // 
            // btnTargetVariableEdit
            // 
            btnTargetVariableEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnTargetVariableEdit.Image = (System.Drawing.Image)resources.GetObject("btnTargetVariableEdit.Image");
            btnTargetVariableEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnTargetVariableEdit.Name = "btnTargetVariableEdit";
            btnTargetVariableEdit.Size = new System.Drawing.Size(23, 26);
            btnTargetVariableEdit.Text = "btnTargetVariableEdit";
            // 
            // btnTargetVariableCopy
            // 
            btnTargetVariableCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnTargetVariableCopy.Image = (System.Drawing.Image)resources.GetObject("btnTargetVariableCopy.Image");
            btnTargetVariableCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnTargetVariableCopy.Name = "btnTargetVariableCopy";
            btnTargetVariableCopy.Size = new System.Drawing.Size(23, 26);
            btnTargetVariableCopy.Text = "btnTargetVariableCopy";
            // 
            // btnTargetVariableAdd
            // 
            btnTargetVariableAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnTargetVariableAdd.Image = (System.Drawing.Image)resources.GetObject("btnTargetVariableAdd.Image");
            btnTargetVariableAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnTargetVariableAdd.Name = "btnTargetVariableAdd";
            btnTargetVariableAdd.Size = new System.Drawing.Size(23, 26);
            btnTargetVariableAdd.Text = "btnTargetVariableAdd";
            // 
            // btnTargetVariableDelete
            // 
            btnTargetVariableDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnTargetVariableDelete.Image = (System.Drawing.Image)resources.GetObject("btnTargetVariableDelete.Image");
            btnTargetVariableDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnTargetVariableDelete.Name = "btnTargetVariableDelete";
            btnTargetVariableDelete.Size = new System.Drawing.Size(23, 26);
            btnTargetVariableDelete.Text = "btnTargetVariableDelete";
            // 
            // pnlStateOrChange
            // 
            pnlStateOrChange.Controls.Add(cboStateOrChange);
            pnlStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlStateOrChange.Location = new System.Drawing.Point(0, 29);
            pnlStateOrChange.Margin = new System.Windows.Forms.Padding(0);
            pnlStateOrChange.Name = "pnlStateOrChange";
            pnlStateOrChange.Size = new System.Drawing.Size(313, 29);
            pnlStateOrChange.TabIndex = 3;
            // 
            // cboStateOrChange
            // 
            cboStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            cboStateOrChange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboStateOrChange.FormattingEnabled = true;
            cboStateOrChange.Location = new System.Drawing.Point(0, 0);
            cboStateOrChange.Margin = new System.Windows.Forms.Padding(0);
            cboStateOrChange.Name = "cboStateOrChange";
            cboStateOrChange.Size = new System.Drawing.Size(313, 24);
            cboStateOrChange.TabIndex = 2;
            // 
            // splLDsity
            // 
            splLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            splLDsity.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splLDsity.Location = new System.Drawing.Point(0, 0);
            splLDsity.Margin = new System.Windows.Forms.Padding(0);
            splLDsity.Name = "splLDsity";
            // 
            // splLDsity.Panel1
            // 
            splLDsity.Panel1.Controls.Add(grpLDsity);
            // 
            // splLDsity.Panel2
            // 
            splLDsity.Panel2.Controls.Add(pnlLDsityInformation);
            splLDsity.Size = new System.Drawing.Size(786, 467);
            splLDsity.SplitterDistance = 321;
            splLDsity.SplitterWidth = 5;
            splLDsity.TabIndex = 0;
            // 
            // grpLDsity
            // 
            grpLDsity.Controls.Add(tlpLDsity);
            grpLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLDsity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpLDsity.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpLDsity.Location = new System.Drawing.Point(0, 0);
            grpLDsity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpLDsity.Name = "grpLDsity";
            grpLDsity.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpLDsity.Size = new System.Drawing.Size(321, 467);
            grpLDsity.TabIndex = 5;
            grpLDsity.TabStop = false;
            grpLDsity.Text = "grpLDsity";
            // 
            // tlpLDsity
            // 
            tlpLDsity.ColumnCount = 1;
            tlpLDsity.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpLDsity.Controls.Add(tsrLDsity, 0, 0);
            tlpLDsity.Controls.Add(pnlLDsity, 0, 1);
            tlpLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpLDsity.Location = new System.Drawing.Point(4, 18);
            tlpLDsity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpLDsity.Name = "tlpLDsity";
            tlpLDsity.RowCount = 2;
            tlpLDsity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpLDsity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpLDsity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpLDsity.Size = new System.Drawing.Size(313, 446);
            tlpLDsity.TabIndex = 0;
            // 
            // tsrLDsity
            // 
            tsrLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrLDsity.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnLDsityEdit });
            tsrLDsity.Location = new System.Drawing.Point(0, 0);
            tsrLDsity.Name = "tsrLDsity";
            tsrLDsity.Size = new System.Drawing.Size(313, 29);
            tsrLDsity.TabIndex = 4;
            tsrLDsity.Text = "tsrLDsity";
            // 
            // btnLDsityEdit
            // 
            btnLDsityEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnLDsityEdit.Image = (System.Drawing.Image)resources.GetObject("btnLDsityEdit.Image");
            btnLDsityEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLDsityEdit.Name = "btnLDsityEdit";
            btnLDsityEdit.Size = new System.Drawing.Size(23, 26);
            btnLDsityEdit.Text = "btnLDsityEdit";
            // 
            // pnlLDsity
            // 
            pnlLDsity.Controls.Add(lstLDsity);
            pnlLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLDsity.Location = new System.Drawing.Point(0, 29);
            pnlLDsity.Margin = new System.Windows.Forms.Padding(0);
            pnlLDsity.Name = "pnlLDsity";
            pnlLDsity.Size = new System.Drawing.Size(313, 417);
            pnlLDsity.TabIndex = 5;
            // 
            // lstLDsity
            // 
            lstLDsity.Dock = System.Windows.Forms.DockStyle.Fill;
            lstLDsity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lstLDsity.FormattingEnabled = true;
            lstLDsity.ItemHeight = 15;
            lstLDsity.Location = new System.Drawing.Point(0, 0);
            lstLDsity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            lstLDsity.Name = "lstLDsity";
            lstLDsity.Size = new System.Drawing.Size(313, 417);
            lstLDsity.TabIndex = 4;
            // 
            // pnlLDsityInformation
            // 
            pnlLDsityInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLDsityInformation.Location = new System.Drawing.Point(0, 0);
            pnlLDsityInformation.Margin = new System.Windows.Forms.Padding(0);
            pnlLDsityInformation.Name = "pnlLDsityInformation";
            pnlLDsityInformation.Size = new System.Drawing.Size(460, 467);
            pnlLDsityInformation.TabIndex = 0;
            // 
            // pnlCaption
            // 
            pnlCaption.Controls.Add(splCaption);
            pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCaption.Location = new System.Drawing.Point(0, 0);
            pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            pnlCaption.Name = "pnlCaption";
            pnlCaption.Size = new System.Drawing.Size(1120, 104);
            pnlCaption.TabIndex = 10;
            // 
            // splCaption
            // 
            splCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            splCaption.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splCaption.IsSplitterFixed = true;
            splCaption.Location = new System.Drawing.Point(0, 0);
            splCaption.Margin = new System.Windows.Forms.Padding(0);
            splCaption.Name = "splCaption";
            splCaption.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splCaption.Panel1
            // 
            splCaption.Panel1.Controls.Add(lblCaptionMain);
            // 
            // splCaption.Panel2
            // 
            splCaption.Panel2.Controls.Add(tlpCaption);
            splCaption.Size = new System.Drawing.Size(1120, 104);
            splCaption.SplitterDistance = 35;
            splCaption.SplitterWidth = 5;
            splCaption.TabIndex = 12;
            // 
            // lblCaptionMain
            // 
            lblCaptionMain.ColumnCount = 1;
            lblCaptionMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1120F));
            lblCaptionMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            lblCaptionMain.Controls.Add(lblMainCaption, 0, 0);
            lblCaptionMain.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCaptionMain.Location = new System.Drawing.Point(0, 0);
            lblCaptionMain.Margin = new System.Windows.Forms.Padding(0);
            lblCaptionMain.Name = "lblCaptionMain";
            lblCaptionMain.RowCount = 1;
            lblCaptionMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            lblCaptionMain.Size = new System.Drawing.Size(1120, 35);
            lblCaptionMain.TabIndex = 0;
            // 
            // lblMainCaption
            // 
            lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblMainCaption.Location = new System.Drawing.Point(4, 0);
            lblMainCaption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblMainCaption.Name = "lblMainCaption";
            lblMainCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblMainCaption.Size = new System.Drawing.Size(1112, 35);
            lblMainCaption.TabIndex = 1;
            lblMainCaption.Text = "lblMainCaption";
            lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpCaption
            // 
            tlpCaption.ColumnCount = 2;
            tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 327F));
            tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCaption.Controls.Add(lblSelectedLDsityObjectGroupValue, 1, 0);
            tlpCaption.Controls.Add(lblSelectedLDsityObjectGroupCaption, 0, 0);
            tlpCaption.Controls.Add(lblSelectedTargetVariableCaption, 0, 1);
            tlpCaption.Controls.Add(lblSelectedTargetVariableValue, 1, 1);
            tlpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpCaption.Location = new System.Drawing.Point(0, 0);
            tlpCaption.Margin = new System.Windows.Forms.Padding(0);
            tlpCaption.Name = "tlpCaption";
            tlpCaption.RowCount = 3;
            tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCaption.Size = new System.Drawing.Size(1120, 64);
            tlpCaption.TabIndex = 4;
            // 
            // lblSelectedLDsityObjectGroupValue
            // 
            lblSelectedLDsityObjectGroupValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedLDsityObjectGroupValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedLDsityObjectGroupValue.Location = new System.Drawing.Point(331, 0);
            lblSelectedLDsityObjectGroupValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblSelectedLDsityObjectGroupValue.Name = "lblSelectedLDsityObjectGroupValue";
            lblSelectedLDsityObjectGroupValue.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedLDsityObjectGroupValue.Size = new System.Drawing.Size(785, 29);
            lblSelectedLDsityObjectGroupValue.TabIndex = 4;
            lblSelectedLDsityObjectGroupValue.Text = "lblSelectedLDsityObjectGroupValue";
            lblSelectedLDsityObjectGroupValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedLDsityObjectGroupCaption
            // 
            lblSelectedLDsityObjectGroupCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedLDsityObjectGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedLDsityObjectGroupCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblSelectedLDsityObjectGroupCaption.Location = new System.Drawing.Point(0, 0);
            lblSelectedLDsityObjectGroupCaption.Margin = new System.Windows.Forms.Padding(0);
            lblSelectedLDsityObjectGroupCaption.Name = "lblSelectedLDsityObjectGroupCaption";
            lblSelectedLDsityObjectGroupCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedLDsityObjectGroupCaption.Size = new System.Drawing.Size(327, 29);
            lblSelectedLDsityObjectGroupCaption.TabIndex = 0;
            lblSelectedLDsityObjectGroupCaption.Text = "lblSelectedLDsityObjectGroupCaption";
            lblSelectedLDsityObjectGroupCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableCaption
            // 
            lblSelectedTargetVariableCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedTargetVariableCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedTargetVariableCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblSelectedTargetVariableCaption.Location = new System.Drawing.Point(0, 29);
            lblSelectedTargetVariableCaption.Margin = new System.Windows.Forms.Padding(0);
            lblSelectedTargetVariableCaption.Name = "lblSelectedTargetVariableCaption";
            lblSelectedTargetVariableCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedTargetVariableCaption.Size = new System.Drawing.Size(327, 29);
            lblSelectedTargetVariableCaption.TabIndex = 5;
            lblSelectedTargetVariableCaption.Text = "lblSelectedTargetVariableCaption";
            lblSelectedTargetVariableCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSelectedTargetVariableValue
            // 
            lblSelectedTargetVariableValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedTargetVariableValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedTargetVariableValue.Location = new System.Drawing.Point(331, 29);
            lblSelectedTargetVariableValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblSelectedTargetVariableValue.Name = "lblSelectedTargetVariableValue";
            lblSelectedTargetVariableValue.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblSelectedTargetVariableValue.Size = new System.Drawing.Size(785, 29);
            lblSelectedTargetVariableValue.TabIndex = 6;
            lblSelectedTargetVariableValue.Text = "lblSelectedTargetVariableValue";
            lblSelectedTargetVariableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlTDSelectionTargetVariableCore
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(pnlMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "ControlTDSelectionTargetVariableCore";
            Size = new System.Drawing.Size(1120, 623);
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            pnlWorkSpace.ResumeLayout(false);
            splTargetVariable.Panel1.ResumeLayout(false);
            splTargetVariable.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splTargetVariable).EndInit();
            splTargetVariable.ResumeLayout(false);
            grpTargetVariable.ResumeLayout(false);
            tlpTargetVariable.ResumeLayout(false);
            tlpTargetVariable.PerformLayout();
            pnlTargetVariable.ResumeLayout(false);
            tsrTargetVariable.ResumeLayout(false);
            tsrTargetVariable.PerformLayout();
            pnlStateOrChange.ResumeLayout(false);
            splLDsity.Panel1.ResumeLayout(false);
            splLDsity.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splLDsity).EndInit();
            splLDsity.ResumeLayout(false);
            grpLDsity.ResumeLayout(false);
            tlpLDsity.ResumeLayout(false);
            tlpLDsity.PerformLayout();
            tsrLDsity.ResumeLayout(false);
            tsrLDsity.PerformLayout();
            pnlLDsity.ResumeLayout(false);
            pnlCaption.ResumeLayout(false);
            splCaption.Panel1.ResumeLayout(false);
            splCaption.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splCaption).EndInit();
            splCaption.ResumeLayout(false);
            lblCaptionMain.ResumeLayout(false);
            tlpCaption.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlWorkSpace;
        private System.Windows.Forms.SplitContainer splTargetVariable;
        private System.Windows.Forms.SplitContainer splLDsity;
        private System.Windows.Forms.GroupBox grpTargetVariable;
        private System.Windows.Forms.GroupBox grpLDsity;
        private System.Windows.Forms.TableLayoutPanel tlpTargetVariable;
        private System.Windows.Forms.ToolStrip tsrTargetVariable;
        private System.Windows.Forms.ToolStripButton btnTargetVariableAdd;
        private System.Windows.Forms.TableLayoutPanel tlpLDsity;
        private System.Windows.Forms.ToolStripButton btnTargetVariableEdit;
        private System.Windows.Forms.ToolStripButton btnTargetVariableDelete;
        private System.Windows.Forms.ToolStrip tsrLDsity;
        private System.Windows.Forms.ToolStripButton btnLDsityEdit;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.SplitContainer splCaption;
        private System.Windows.Forms.TableLayoutPanel tlpCaption;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupValue;
        private System.Windows.Forms.Label lblSelectedLDsityObjectGroupCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableCaption;
        private System.Windows.Forms.Label lblSelectedTargetVariableValue;
        private System.Windows.Forms.Panel pnlLDsityInformation;
        private System.Windows.Forms.Panel pnlLDsity;
        private System.Windows.Forms.ListBox lstLDsity;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.TableLayoutPanel lblCaptionMain;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.Panel pnlTargetVariable;
        private System.Windows.Forms.ListBox lstTargetVariable;
        private System.Windows.Forms.Panel pnlStateOrChange;
        private System.Windows.Forms.ComboBox cboStateOrChange;
        private System.Windows.Forms.ToolStripButton btnTargetVariableCopy;
    }

}
