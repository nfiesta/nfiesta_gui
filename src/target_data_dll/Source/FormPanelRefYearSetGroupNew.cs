﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nové skupiny kombinací panelů a roků měření</para>
    /// <para lang="en">Form to create a new group of panel and reference-yearset combinations</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormPanelRefYearSetGroupNew
            : Form, INfiEstaControl, ITargetDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Vhodné páry panelů a roků měření</para>
        /// <para lang="en">Eligible panel and reference year set combinations</para>
        /// </summary>
        private TDFnGetEligiblePanelRefYearSetCombinationsTypeList eligiblePanelRefYearSetCombinations;

        private string msgDuplicateEntry = String.Empty;
        private string msgNoCombinationSelected = String.Empty;
        private string msgLabelCsIsEmpty = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnExists = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro vytvoření nové skupiny kombinací panelů a roků měření</para>
        /// <para lang="en">Form constructor to create a new group of panel and reference-yearset combinations</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormPanelRefYearSetGroupNew(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionPanelRefYearSetGroup)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionPanelRefYearSetGroup)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionPanelRefYearSetGroup)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionPanelRefYearSetGroup)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPanelRefYearSetGroupNew),  "Nová skupina panelů a roků měření" },
                        { nameof(btnOK),                        "Vytvořit" },
                        { nameof(btnCancel),                    "Zrušit" },
                        { nameof(grpPanelRefYearSetList),       "Seznam panelů a roků měření:" },
                        { nameof(grpPanelRefYearSetGroup),      "Skupina panelů a roků měření:" },
                        { nameof(lblLabelCs),                   "Zkratka (národní):" },
                        { nameof(lblDescriptionCs),             "Popis (národní):" },
                        { nameof(lblLabelEn),                   "Zkratka (anglicky):" },
                        { nameof(lblDescriptionEn),             "Popis (anglicky):" },
                        { nameof(msgDuplicateEntry),            "Záznam nesmí být duplicitní" },
                        { nameof(msgNoCombinationSelected),     "Není vybrána žádná kombinace panelů a roků měření." },
                        { nameof(msgLabelCsIsEmpty),            "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgDescriptionCsIsEmpty),      "Popis (národní) musí být vyplněn." },
                        { nameof(msgLabelEnIsEmpty),            "Zkratka (anglicky) musí být vyplněna." },
                        { nameof(msgDescriptionEnIsEmpty),      "Popis (anglicky) musí být vyplněn." },
                        { nameof(msgLabelCsExists),             "Použitá zkratka (národní) již existuje pro jinou skupinu panelů a roků měření." },
                        { nameof(msgDescriptionCsExists),       "Použitý popis (národní) již existuje pro jinou skupinu panelů a roků měření." },
                        { nameof(msgLabelEnExists),             "Použitá zkratka (anglická) již existuje pro jinou skupinu panelů a roků měření." },
                        { nameof(msgDescriptionEnExists),       "Použitý popis (anglický) již existuje pro jinou skupinu panelů a roků měření." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormPanelRefYearSetGroupNew),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormPanelRefYearSetGroupNew),  "New group of panels and reference-yearsets" },
                        { nameof(btnOK),                        "Insert" },
                        { nameof(btnCancel),                    "Cancel" },
                        { nameof(grpPanelRefYearSetList),       "List of panels and reference-yearsets:" },
                        { nameof(grpPanelRefYearSetGroup),      String.Empty },
                        { nameof(lblLabelCs),                   "Label (national):" },
                        { nameof(lblDescriptionCs),             "Description (national):" },
                        { nameof(lblLabelEn),                   "Label (English):" },
                        { nameof(lblDescriptionEn),             "Description (English):" },
                        { nameof(msgDuplicateEntry),            "Duplicate entry is not allowed" },
                        { nameof(msgNoCombinationSelected),     "No combination of panels and reference-yearsets is selected." },
                        { nameof(msgLabelCsIsEmpty),            "Label (national) cannot be empty." },
                        { nameof(msgDescriptionCsIsEmpty),      "Description (national) cannot be empty." },
                        { nameof(msgLabelEnIsEmpty),            "Label (English) cannot be empty." },
                        { nameof(msgDescriptionEnIsEmpty),      "Description (English) cannot be empty." },
                        { nameof(msgLabelCsExists),             "This label (national) already exists for another group of panels and reference-yearsets." },
                        { nameof(msgDescriptionCsExists),       "This description (national) already exists for another group of panels and reference-yearsets." },
                        { nameof(msgLabelEnExists),             "This label (English) already exists for another group of panels and reference-yearsets." },
                        { nameof(msgDescriptionEnExists),       "This description (English) already exists for another group of panels and reference-yearsets." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormPanelRefYearSetGroupNew),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro vytvoření nové skupiny kombinací panelů a roků měření</para>
        /// <para lang="en">Initializing the form to create a new group of panel and reference-yearset combinations</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            LoadContent();

            cklPanelRefYearSetList.DataSource =
                eligiblePanelRefYearSetCombinations.Items;

            InitializeLabels();
            SetStyle();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            cklPanelRefYearSetList.Format += new ListControlConvertEventHandler(
                (sender, e) =>
                {
                    SetDisplayMember(
                        listItem: (TDFnGetEligiblePanelRefYearSetCombinationsType)e.ListItem,
                        e: e);
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !SavePyrGroup();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });

            btnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro vytvoření nové skupiny kombinací panelů a roků měření</para>
        /// <para lang="en">Initializing form labels to create a new group of panel and reference-yearset combinations</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: nameof(FormPanelRefYearSetGroupNew),
                    out string frmPanelRefYearSetGroupNewText)
                        ? frmPanelRefYearSetGroupNewText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(
                    key: nameof(btnOK),
                    out string btnOKText)
                ? btnOKText
                : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(
                    key: nameof(btnCancel),
                    out string btnCancelText)
                ? btnCancelText
                : String.Empty;

            grpPanelRefYearSetList.Text =
                labels.TryGetValue(
                    key: nameof(grpPanelRefYearSetList),
                    out string grpPanelRefYearSetListText)
                ? grpPanelRefYearSetListText
                : String.Empty;

            grpPanelRefYearSetGroup.Text =
                labels.TryGetValue(
                    key: nameof(grpPanelRefYearSetGroup),
                    out string grpPanelRefYearSetGroupText)
                ? grpPanelRefYearSetGroupText
                : String.Empty;

            lblLabelCs.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelCs),
                    out string lblLabelCsText)
                ? lblLabelCsText
                : String.Empty;

            lblDescriptionCs.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionCs),
                    out string lblDescriptionCsText)
                ? lblDescriptionCsText
                : String.Empty;

            lblLabelEn.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelEn),
                    out string lblLabelEnText)
                ? lblLabelEnText
                : String.Empty;

            lblDescriptionEn.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionEn),
                    out string lblDescriptionEnText)
                ? lblDescriptionEnText
                : String.Empty;

            msgDuplicateEntry =
              labels.TryGetValue(key: nameof(msgDuplicateEntry),
                      out msgDuplicateEntry)
                          ? msgDuplicateEntry
                          : String.Empty;

            msgNoCombinationSelected =
              labels.TryGetValue(key: nameof(msgNoCombinationSelected),
                      out msgNoCombinationSelected)
                          ? msgNoCombinationSelected
                          : String.Empty;

            msgLabelCsIsEmpty =
              labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                      out msgLabelCsIsEmpty)
                          ? msgLabelCsIsEmpty
                          : String.Empty;

            msgDescriptionCsIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                      out msgDescriptionCsIsEmpty)
                          ? msgDescriptionCsIsEmpty
                          : String.Empty;

            msgLabelEnIsEmpty =
              labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                      out msgLabelEnIsEmpty)
                          ? msgLabelEnIsEmpty
                          : String.Empty;

            msgDescriptionEnIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                      out msgDescriptionEnIsEmpty)
                          ? msgDescriptionEnIsEmpty
                          : String.Empty;

            msgLabelCsExists =
              labels.TryGetValue(key: nameof(msgLabelCsExists),
                      out msgLabelCsExists)
                          ? msgLabelCsExists
                          : String.Empty;

            msgDescriptionCsExists =
              labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                      out msgDescriptionCsExists)
                          ? msgDescriptionCsExists
                          : String.Empty;

            msgLabelEnExists =
              labels.TryGetValue(key: nameof(msgLabelEnExists),
                      out msgLabelEnExists)
                          ? msgLabelEnExists
                          : String.Empty;

            msgDescriptionEnExists =
              labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                      out msgDescriptionEnExists)
                          ? msgDescriptionEnExists
                          : String.Empty;
        }


        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            TableLayoutPanelCellPosition pos;

            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;


            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;


            grpPanelRefYearSetGroup.Font = Setting.GroupBoxFont;
            grpPanelRefYearSetGroup.ForeColor = Setting.GroupBoxForeColor;

            grpPanelRefYearSetList.Font = Setting.GroupBoxFont;
            grpPanelRefYearSetList.ForeColor = Setting.GroupBoxForeColor;


            cklPanelRefYearSetList.Font = Setting.CheckedListBoxFont;
            cklPanelRefYearSetList.ForeColor = Setting.CheckedListBoxForeColor;


            lblLabelCs.Font = Setting.LabelFont;
            lblLabelCs.ForeColor = Setting.LabelForeColor;

            lblDescriptionCs.Font = Setting.LabelFont;
            lblDescriptionCs.ForeColor = Setting.LabelForeColor;

            lblLabelEn.Font = Setting.LabelFont;
            lblLabelEn.ForeColor = Setting.LabelForeColor;

            lblDescriptionEn.Font = Setting.LabelFont;
            lblDescriptionEn.ForeColor = Setting.LabelForeColor;


            txtLabelCs.Font = Setting.TextBoxFont;
            txtLabelCs.ForeColor = Setting.TextBoxForeColor;
            pos = tlpPanelRefYearSetGroup.GetCellPosition(control: txtLabelCs);
            txtLabelCs.Margin = new Padding(
                left: 0, top: (int)((tlpPanelRefYearSetGroup.GetRowHeights()[pos.Row] - txtLabelCs.Height) / 2), right: 0, bottom: 0);

            txtDescriptionCs.Font = Setting.TextBoxFont;
            txtDescriptionCs.ForeColor = Setting.TextBoxForeColor;
            pos = tlpPanelRefYearSetGroup.GetCellPosition(control: txtDescriptionCs);
            txtDescriptionCs.Margin = new Padding(
                left: 0, top: (int)((tlpPanelRefYearSetGroup.GetRowHeights()[pos.Row] - txtDescriptionCs.Height) / 2), right: 0, bottom: 0);

            txtLabelEn.Font = Setting.TextBoxFont;
            txtLabelEn.ForeColor = Setting.TextBoxForeColor;
            pos = tlpPanelRefYearSetGroup.GetCellPosition(control: txtLabelEn);
            txtLabelEn.Margin = new Padding(
                left: 0, top: (int)((tlpPanelRefYearSetGroup.GetRowHeights()[pos.Row] - txtLabelEn.Height) / 2), right: 0, bottom: 0);

            txtDescriptionEn.Font = Setting.TextBoxFont;
            txtDescriptionEn.ForeColor = Setting.TextBoxForeColor;
            pos = tlpPanelRefYearSetGroup.GetCellPosition(control: txtDescriptionEn);
            txtDescriptionEn.Margin = new Padding(
                left: 0, top: (int)((tlpPanelRefYearSetGroup.GetRowHeights()[pos.Row] - txtDescriptionEn.Height) / 2), right: 0, bottom: 0);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            eligiblePanelRefYearSetCombinations =
                TDFunctions.FnGetEligiblePanelRefYearSetCombinationsA.Execute(
                    database: Database,
                    targetVariableId: ((ControlTDSelectionPanelRefYearSetGroup)ControlOwner).SelectedTargetVariable.Id,
                    categorizationSetupIds: ((ControlTDSelectionPanelRefYearSetGroup)ControlOwner).CategorizationSetupIds);
        }

        /// <summary>
        /// <para lang="cs">Zobrazení id, popisku panelu a refyearsetu v cklPanelRefYearSetList</para>
        /// <para lang="en">Display of id, panel and refyearset label in cklPanelRefYearSetList</para>
        /// </summary>
        /// <param name="listItem">
        /// <para lang="cs">Položka v seznamu cklPanelRefYearSetList</para>
        /// <para lang="en">Item in list cklPanelRefYearSetList</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void SetDisplayMember(
            TDFnGetEligiblePanelRefYearSetCombinationsType listItem,
            ListControlConvertEventArgs e)
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    e.Value = String.Concat(
                        listItem.Id, " - ", listItem.PanelLabelCs, " / ", listItem.ReferenceYearSetLabelCs);
                    return;

                case LanguageVersion.International:
                    e.Value = String.Concat(
                         listItem.Id, " - ", listItem.PanelLabelEn, " / ", listItem.ReferenceYearSetLabelEn);
                    return;

                default:
                    e.Value = String.Concat(
                        listItem.Id, " - ", listItem.Id, " / ", listItem.Id);
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Uloží skupinu panel a rok měření do databázové tabulky</para>
        /// <para lang="en">It saves group panel and reference year set into database table</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud bylo možné uložit skupinu panel a rok měření, jinak false</para>
        /// <para lang="en">It returns true if it was possible to save the group panel
        /// and reference year set into database table, else false</para>
        /// </returns>
        private bool SavePyrGroup()
        {
            List<TDFnGetEligiblePanelRefYearSetCombinationsType> panelRefYearSetCombinations =
                cklPanelRefYearSetList.CheckedItems
                .OfType<TDFnGetEligiblePanelRefYearSetCombinationsType>()
                .ToList<TDFnGetEligiblePanelRefYearSetCombinationsType>();

            TDPanelRefYearSetGroupList panelRefYearSetGroups =
                TDFunctions.FnGetPanelRefYearSetGroup.Execute(
                    database: Database,
                    panelRefYearSetGroupId: null);

            string labelCs = txtLabelCs.Text.Trim();
            string descriptionCs = txtDescriptionCs.Text.Trim();
            string labelEn = txtLabelEn.Text.Trim();
            string descriptionEn = txtDescriptionEn.Text.Trim();

            if (panelRefYearSetCombinations.Count == 0)
            {
                MessageBox.Show(
                    text: msgNoCombinationSelected,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if ((bool)TDFunctions.FnTryPyrGroupRefYearSetToPanelMapping.Execute(
                    database: Database,
                    refYearSetToPanelIds: panelRefYearSetCombinations
                                            .Select(a => (Nullable<int>)a.Id)
                                            .ToList<Nullable<int>>()))
            {
                MessageBox.Show(
                 text: msgDuplicateEntry,
                 caption: String.Empty,
                 buttons: MessageBoxButtons.OK,
                 icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: labelCs))
            {
                MessageBox.Show(
                     text: msgLabelCsIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                MessageBox.Show(
                     text: msgDescriptionCsIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                MessageBox.Show(
                     text: msgLabelEnIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                MessageBox.Show(
                     text: msgDescriptionEnIsEmpty,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (panelRefYearSetGroups.Items
                    .Where(a => a.LabelCs == labelCs)
                    .Any())
            {
                MessageBox.Show(
                     text: msgLabelCsExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (panelRefYearSetGroups.Items
                     .Where(a => a.DescriptionCs == descriptionCs)
                     .Any())
            {
                MessageBox.Show(
                     text: msgDescriptionCsExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (panelRefYearSetGroups.Items
                    .Where(a => a.LabelEn == labelEn)
                    .Any())
            {
                MessageBox.Show(
                     text: msgLabelEnExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            if (panelRefYearSetGroups.Items
                     .Where(a => a.DescriptionEn == descriptionEn)
                     .Any())
            {
                MessageBox.Show(
                     text: msgDescriptionEnExists,
                     caption: String.Empty,
                     buttons: MessageBoxButtons.OK,
                     icon: MessageBoxIcon.Information);
                return false;
            }

            TDFunctions.FnSavePyrGroup.Execute(
                        database: Database,
                        labelCs: labelCs,
                        descriptionCs: descriptionCs,
                        labelEn: labelEn,
                        descriptionEn: descriptionEn,
                        refYearSetToPanelsIds:
                            panelRefYearSetCombinations
                                .Select(a => (Nullable<int>)a.Id)
                                .ToList<Nullable<int>>());

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: TDFunctions.FnSavePyrGroup.CommandText,
                    caption: TDFunctions.FnSavePyrGroup.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            return !Database.Postgres.ExceptionFlag;
        }

        #endregion Methods

    }

}