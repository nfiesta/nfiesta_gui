﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class FormDomainNewDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            grpDomain = new System.Windows.Forms.GroupBox();
            tlpDomain = new System.Windows.Forms.TableLayoutPanel();
            txtLabelCsValue = new System.Windows.Forms.TextBox();
            lblDescriptionEnCaption = new System.Windows.Forms.Label();
            lblLabelEnCaption = new System.Windows.Forms.Label();
            lblDescriptionCsCaption = new System.Windows.Forms.Label();
            lblLabelCsCaption = new System.Windows.Forms.Label();
            txtDescriptionEnValue = new System.Windows.Forms.TextBox();
            txtLabelEnValue = new System.Windows.Forms.TextBox();
            lblClassificationTypeCaption = new System.Windows.Forms.Label();
            cboClassificationType = new System.Windows.Forms.ComboBox();
            txtDescriptionCsValue = new System.Windows.Forms.TextBox();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            grpDomain.SuspendLayout();
            tlpDomain.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(944, 501);
            pnlMain.TabIndex = 6;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(grpDomain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(4, 458);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(936, 40);
            tlpButtons.TabIndex = 13;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(776, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnCancel.ForeColor = System.Drawing.Color.Black;
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(616, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnOK.ForeColor = System.Drawing.Color.Black;
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // grpDomain
            // 
            grpDomain.Controls.Add(tlpDomain);
            grpDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpDomain.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpDomain.ForeColor = System.Drawing.Color.MediumBlue;
            grpDomain.Location = new System.Drawing.Point(4, 3);
            grpDomain.Margin = new System.Windows.Forms.Padding(0);
            grpDomain.Name = "grpDomain";
            grpDomain.Padding = new System.Windows.Forms.Padding(5);
            grpDomain.Size = new System.Drawing.Size(936, 455);
            grpDomain.TabIndex = 6;
            grpDomain.TabStop = false;
            grpDomain.Text = "grpDomain";
            // 
            // tlpDomain
            // 
            tlpDomain.ColumnCount = 2;
            tlpDomain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpDomain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpDomain.Controls.Add(txtLabelCsValue, 1, 0);
            tlpDomain.Controls.Add(lblDescriptionEnCaption, 0, 4);
            tlpDomain.Controls.Add(lblLabelEnCaption, 0, 3);
            tlpDomain.Controls.Add(lblDescriptionCsCaption, 0, 1);
            tlpDomain.Controls.Add(lblLabelCsCaption, 0, 0);
            tlpDomain.Controls.Add(txtDescriptionEnValue, 1, 4);
            tlpDomain.Controls.Add(txtLabelEnValue, 1, 3);
            tlpDomain.Controls.Add(lblClassificationTypeCaption, 0, 6);
            tlpDomain.Controls.Add(cboClassificationType, 1, 6);
            tlpDomain.Controls.Add(txtDescriptionCsValue, 1, 1);
            tlpDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpDomain.ForeColor = System.Drawing.SystemColors.ControlText;
            tlpDomain.Location = new System.Drawing.Point(5, 25);
            tlpDomain.Margin = new System.Windows.Forms.Padding(0);
            tlpDomain.Name = "tlpDomain";
            tlpDomain.RowCount = 7;
            tlpDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpDomain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpDomain.Size = new System.Drawing.Size(926, 425);
            tlpDomain.TabIndex = 2;
            // 
            // txtLabelCsValue
            // 
            txtLabelCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCsValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelCsValue.ForeColor = System.Drawing.Color.Black;
            txtLabelCsValue.Location = new System.Drawing.Point(150, 0);
            txtLabelCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCsValue.Name = "txtLabelCsValue";
            txtLabelCsValue.Size = new System.Drawing.Size(776, 16);
            txtLabelCsValue.TabIndex = 18;
            // 
            // lblDescriptionEnCaption
            // 
            lblDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionEnCaption.Location = new System.Drawing.Point(0, 227);
            lblDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEnCaption.Name = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.Size = new System.Drawing.Size(150, 152);
            lblDescriptionEnCaption.TabIndex = 3;
            lblDescriptionEnCaption.Text = "lblDescriptionEnCaption";
            // 
            // lblLabelEnCaption
            // 
            lblLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelEnCaption.Location = new System.Drawing.Point(0, 197);
            lblLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEnCaption.Name = "lblLabelEnCaption";
            lblLabelEnCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelEnCaption.TabIndex = 2;
            lblLabelEnCaption.Text = "lblLabelEnCaption";
            lblLabelEnCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCsCaption
            // 
            lblDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCsCaption.Location = new System.Drawing.Point(0, 30);
            lblDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCsCaption.Name = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.Size = new System.Drawing.Size(150, 152);
            lblDescriptionCsCaption.TabIndex = 1;
            lblDescriptionCsCaption.Text = "lblDescriptionCsCaption";
            // 
            // lblLabelCsCaption
            // 
            lblLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCsCaption.Location = new System.Drawing.Point(0, 0);
            lblLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCsCaption.Name = "lblLabelCsCaption";
            lblLabelCsCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelCsCaption.TabIndex = 0;
            lblLabelCsCaption.Text = "lblLabelCsCaption";
            lblLabelCsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescriptionEnValue
            // 
            txtDescriptionEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEnValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtDescriptionEnValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionEnValue.Location = new System.Drawing.Point(150, 227);
            txtDescriptionEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEnValue.Multiline = true;
            txtDescriptionEnValue.Name = "txtDescriptionEnValue";
            txtDescriptionEnValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionEnValue.Size = new System.Drawing.Size(776, 152);
            txtDescriptionEnValue.TabIndex = 7;
            // 
            // txtLabelEnValue
            // 
            txtLabelEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelEnValue.ForeColor = System.Drawing.Color.Black;
            txtLabelEnValue.Location = new System.Drawing.Point(150, 197);
            txtLabelEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEnValue.Name = "txtLabelEnValue";
            txtLabelEnValue.Size = new System.Drawing.Size(776, 14);
            txtLabelEnValue.TabIndex = 6;
            // 
            // lblClassificationTypeCaption
            // 
            lblClassificationTypeCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblClassificationTypeCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblClassificationTypeCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblClassificationTypeCaption.Location = new System.Drawing.Point(0, 394);
            lblClassificationTypeCaption.Margin = new System.Windows.Forms.Padding(0);
            lblClassificationTypeCaption.Name = "lblClassificationTypeCaption";
            lblClassificationTypeCaption.Size = new System.Drawing.Size(150, 31);
            lblClassificationTypeCaption.TabIndex = 8;
            lblClassificationTypeCaption.Text = "lblClassificationTypeCaption";
            lblClassificationTypeCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboClassificationType
            // 
            cboClassificationType.Dock = System.Windows.Forms.DockStyle.Fill;
            cboClassificationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboClassificationType.Font = new System.Drawing.Font("Segoe UI", 9F);
            cboClassificationType.ForeColor = System.Drawing.Color.Black;
            cboClassificationType.FormattingEnabled = true;
            cboClassificationType.Location = new System.Drawing.Point(150, 394);
            cboClassificationType.Margin = new System.Windows.Forms.Padding(0);
            cboClassificationType.Name = "cboClassificationType";
            cboClassificationType.Size = new System.Drawing.Size(776, 23);
            cboClassificationType.TabIndex = 17;
            // 
            // txtDescriptionCsValue
            // 
            txtDescriptionCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCsValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtDescriptionCsValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionCsValue.Location = new System.Drawing.Point(150, 30);
            txtDescriptionCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCsValue.Multiline = true;
            txtDescriptionCsValue.Name = "txtDescriptionCsValue";
            txtDescriptionCsValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionCsValue.Size = new System.Drawing.Size(776, 152);
            txtDescriptionCsValue.TabIndex = 5;
            // 
            // FormDomainNewDesign
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(pnlMain);
            Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormDomainNewDesign";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormDomainNew";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            grpDomain.ResumeLayout(false);
            tlpDomain.ResumeLayout(false);
            tlpDomain.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpDomain;
        private System.Windows.Forms.TableLayoutPanel tlpDomain;
        private System.Windows.Forms.Label lblDescriptionEnCaption;
        private System.Windows.Forms.Label lblLabelEnCaption;
        private System.Windows.Forms.Label lblDescriptionCsCaption;
        private System.Windows.Forms.Label lblLabelCsCaption;
        private System.Windows.Forms.TextBox txtDescriptionEnValue;
        private System.Windows.Forms.TextBox txtLabelEnValue;
        private System.Windows.Forms.TextBox txtDescriptionCsValue;
        private System.Windows.Forms.Label lblClassificationTypeCaption;
        private System.Windows.Forms.ComboBox cboClassificationType;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtLabelCsValue;
    }

}