﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{
    partial class FormPanelRefYearSetGroupNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            splMain = new System.Windows.Forms.SplitContainer();
            grpPanelRefYearSetGroup = new System.Windows.Forms.GroupBox();
            tlpPanelRefYearSetGroup = new System.Windows.Forms.TableLayoutPanel();
            lblDescriptionEn = new System.Windows.Forms.Label();
            lblLabelEn = new System.Windows.Forms.Label();
            lblDescriptionCs = new System.Windows.Forms.Label();
            lblLabelCs = new System.Windows.Forms.Label();
            txtDescriptionEn = new System.Windows.Forms.TextBox();
            txtLabelEn = new System.Windows.Forms.TextBox();
            txtDescriptionCs = new System.Windows.Forms.TextBox();
            txtLabelCs = new System.Windows.Forms.TextBox();
            grpPanelRefYearSetList = new System.Windows.Forms.GroupBox();
            cklPanelRefYearSetList = new System.Windows.Forms.CheckedListBox();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            grpPanelRefYearSetGroup.SuspendLayout();
            tlpPanelRefYearSetGroup.SuspendLayout();
            grpPanelRefYearSetList.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(splMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 6;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 16;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(784, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(624, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.Location = new System.Drawing.Point(0, 0);
            splMain.Margin = new System.Windows.Forms.Padding(0);
            splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(grpPanelRefYearSetGroup);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(grpPanelRefYearSetList);
            splMain.Size = new System.Drawing.Size(944, 461);
            splMain.SplitterDistance = 500;
            splMain.SplitterWidth = 1;
            splMain.TabIndex = 15;
            // 
            // grpPanelRefYearSetGroup
            // 
            grpPanelRefYearSetGroup.BackColor = System.Drawing.SystemColors.Control;
            grpPanelRefYearSetGroup.Controls.Add(tlpPanelRefYearSetGroup);
            grpPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanelRefYearSetGroup.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpPanelRefYearSetGroup.ForeColor = System.Drawing.Color.MediumBlue;
            grpPanelRefYearSetGroup.Location = new System.Drawing.Point(0, 0);
            grpPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            grpPanelRefYearSetGroup.Name = "grpPanelRefYearSetGroup";
            grpPanelRefYearSetGroup.Padding = new System.Windows.Forms.Padding(5);
            grpPanelRefYearSetGroup.Size = new System.Drawing.Size(500, 461);
            grpPanelRefYearSetGroup.TabIndex = 2;
            grpPanelRefYearSetGroup.TabStop = false;
            grpPanelRefYearSetGroup.Text = "grpPanelRefYearSetGroup";
            // 
            // tlpPanelRefYearSetGroup
            // 
            tlpPanelRefYearSetGroup.ColumnCount = 2;
            tlpPanelRefYearSetGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpPanelRefYearSetGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanelRefYearSetGroup.Controls.Add(lblDescriptionEn, 0, 4);
            tlpPanelRefYearSetGroup.Controls.Add(lblLabelEn, 0, 3);
            tlpPanelRefYearSetGroup.Controls.Add(lblDescriptionCs, 0, 1);
            tlpPanelRefYearSetGroup.Controls.Add(lblLabelCs, 0, 0);
            tlpPanelRefYearSetGroup.Controls.Add(txtDescriptionEn, 1, 4);
            tlpPanelRefYearSetGroup.Controls.Add(txtLabelEn, 1, 3);
            tlpPanelRefYearSetGroup.Controls.Add(txtDescriptionCs, 1, 1);
            tlpPanelRefYearSetGroup.Controls.Add(txtLabelCs, 1, 0);
            tlpPanelRefYearSetGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpPanelRefYearSetGroup.Location = new System.Drawing.Point(5, 25);
            tlpPanelRefYearSetGroup.Margin = new System.Windows.Forms.Padding(0);
            tlpPanelRefYearSetGroup.Name = "tlpPanelRefYearSetGroup";
            tlpPanelRefYearSetGroup.RowCount = 5;
            tlpPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpPanelRefYearSetGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpPanelRefYearSetGroup.Size = new System.Drawing.Size(490, 431);
            tlpPanelRefYearSetGroup.TabIndex = 1;
            // 
            // lblDescriptionEn
            // 
            lblDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionEn.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionEn.Location = new System.Drawing.Point(0, 253);
            lblDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEn.Name = "lblDescriptionEn";
            lblDescriptionEn.Size = new System.Drawing.Size(150, 178);
            lblDescriptionEn.TabIndex = 7;
            lblDescriptionEn.Text = "lblDescriptionEn";
            // 
            // lblLabelEn
            // 
            lblLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelEn.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelEn.Location = new System.Drawing.Point(0, 223);
            lblLabelEn.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEn.Name = "lblLabelEn";
            lblLabelEn.Size = new System.Drawing.Size(150, 30);
            lblLabelEn.TabIndex = 6;
            lblLabelEn.Text = "lblLabelEn";
            lblLabelEn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCs
            // 
            lblDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionCs.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCs.Location = new System.Drawing.Point(0, 30);
            lblDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCs.Name = "lblDescriptionCs";
            lblDescriptionCs.Size = new System.Drawing.Size(150, 178);
            lblDescriptionCs.TabIndex = 2;
            lblDescriptionCs.Text = "lblDescriptionCs";
            // 
            // lblLabelCs
            // 
            lblLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelCs.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCs.Location = new System.Drawing.Point(0, 0);
            lblLabelCs.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCs.Name = "lblLabelCs";
            lblLabelCs.Size = new System.Drawing.Size(150, 30);
            lblLabelCs.TabIndex = 1;
            lblLabelCs.Text = "lblLabelCs";
            lblLabelCs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescriptionEn
            // 
            txtDescriptionEn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtDescriptionEn.ForeColor = System.Drawing.Color.Black;
            txtDescriptionEn.Location = new System.Drawing.Point(150, 253);
            txtDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEn.Multiline = true;
            txtDescriptionEn.Name = "txtDescriptionEn";
            txtDescriptionEn.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionEn.Size = new System.Drawing.Size(340, 178);
            txtDescriptionEn.TabIndex = 9;
            // 
            // txtLabelEn
            // 
            txtLabelEn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelEn.ForeColor = System.Drawing.Color.Black;
            txtLabelEn.Location = new System.Drawing.Point(150, 223);
            txtLabelEn.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEn.Name = "txtLabelEn";
            txtLabelEn.Size = new System.Drawing.Size(340, 16);
            txtLabelEn.TabIndex = 8;
            // 
            // txtDescriptionCs
            // 
            txtDescriptionCs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtDescriptionCs.ForeColor = System.Drawing.Color.Black;
            txtDescriptionCs.Location = new System.Drawing.Point(150, 30);
            txtDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCs.Multiline = true;
            txtDescriptionCs.Name = "txtDescriptionCs";
            txtDescriptionCs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionCs.Size = new System.Drawing.Size(340, 178);
            txtDescriptionCs.TabIndex = 5;
            // 
            // txtLabelCs
            // 
            txtLabelCs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelCs.ForeColor = System.Drawing.Color.Black;
            txtLabelCs.Location = new System.Drawing.Point(150, 0);
            txtLabelCs.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCs.Name = "txtLabelCs";
            txtLabelCs.Size = new System.Drawing.Size(340, 16);
            txtLabelCs.TabIndex = 4;
            // 
            // grpPanelRefYearSetList
            // 
            grpPanelRefYearSetList.Controls.Add(cklPanelRefYearSetList);
            grpPanelRefYearSetList.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanelRefYearSetList.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpPanelRefYearSetList.ForeColor = System.Drawing.Color.MediumBlue;
            grpPanelRefYearSetList.Location = new System.Drawing.Point(0, 0);
            grpPanelRefYearSetList.Margin = new System.Windows.Forms.Padding(0);
            grpPanelRefYearSetList.Name = "grpPanelRefYearSetList";
            grpPanelRefYearSetList.Padding = new System.Windows.Forms.Padding(5);
            grpPanelRefYearSetList.Size = new System.Drawing.Size(443, 461);
            grpPanelRefYearSetList.TabIndex = 1;
            grpPanelRefYearSetList.TabStop = false;
            grpPanelRefYearSetList.Text = "grpPanelRefYearSetList";
            // 
            // cklPanelRefYearSetList
            // 
            cklPanelRefYearSetList.Dock = System.Windows.Forms.DockStyle.Fill;
            cklPanelRefYearSetList.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            cklPanelRefYearSetList.ForeColor = System.Drawing.Color.Black;
            cklPanelRefYearSetList.FormattingEnabled = true;
            cklPanelRefYearSetList.HorizontalScrollbar = true;
            cklPanelRefYearSetList.Location = new System.Drawing.Point(5, 25);
            cklPanelRefYearSetList.Margin = new System.Windows.Forms.Padding(0);
            cklPanelRefYearSetList.Name = "cklPanelRefYearSetList";
            cklPanelRefYearSetList.Size = new System.Drawing.Size(433, 431);
            cklPanelRefYearSetList.TabIndex = 0;
            // 
            // FormPanelRefYearSetGroupNew
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormPanelRefYearSetGroupNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            grpPanelRefYearSetGroup.ResumeLayout(false);
            tlpPanelRefYearSetGroup.ResumeLayout(false);
            tlpPanelRefYearSetGroup.PerformLayout();
            grpPanelRefYearSetList.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.GroupBox grpPanelRefYearSetGroup;
        private System.Windows.Forms.GroupBox grpPanelRefYearSetList;
        private System.Windows.Forms.CheckedListBox cklPanelRefYearSetList;
        private System.Windows.Forms.TableLayoutPanel tlpPanelRefYearSetGroup;
        private System.Windows.Forms.Label lblDescriptionEn;
        private System.Windows.Forms.Label lblLabelEn;
        private System.Windows.Forms.Label lblDescriptionCs;
        private System.Windows.Forms.Label lblLabelCs;
        private System.Windows.Forms.TextBox txtDescriptionEn;
        private System.Windows.Forms.TextBox txtLabelEn;
        private System.Windows.Forms.TextBox txtDescriptionCs;
        private System.Windows.Forms.TextBox txtLabelCs;
    }
}