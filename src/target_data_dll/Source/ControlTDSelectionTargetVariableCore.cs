﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
    /// <para lang="en">Control "Selection of the target variable"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlTDSelectionTargetVariableCore
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        public bool onEdit;


        // Datové položky získané z uložených procedur:
        // Data items obtained from stored procedures:

        /// <summary>
        /// <para lang="cs">Vybrané cílové proměnné (pro skupinu objektů lokálních hustot)</para>
        /// <para lang="en">Selected target variables (for a group of local density objects)</para>
        /// </summary>
        private TDFnGetTargetVariableTypeList fnTargetVariable;


        // Výsledkem výběru v této komponentě je vybraná skupina cílových proměnných a jejich příspěvky lokálních hustot:
        // The result of the selection in this component are selected target variables and their local density contributions:

        /// <summary>
        /// <para lang="cs">Vybraná skupina cílových proměnných</para>
        /// <para lang="en">Selected target variable group</para>
        /// </summary>
        private TDFnGetTargetVariableType selectedTargetVariableGroup;

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná</para>
        /// <para lang="en">Selected target variable</para>
        /// </summary>
        private TDTargetVariable selectedTargetVariable;

        /// <summary>
        /// <para lang="cs">Vybrané příspěvky lokálních hustot pro vybranou skupinu cílových proměnných</para>
        /// <para lang="en">Selected local density contributions for the selected target variable group</para>
        /// </summary>
        private TDLDsityList selectedLDsities;

        /// <summary>
        /// <para lang="cs">Vybraný příspěvek lokálních hustot pro vybranou skupinu cílových proměnných</para>
        /// <para lang="en">Selected local density contribution for the selected target variable group</para>
        /// </summary>
        private TDLDsity selectedLDsity;

        private string msgNone = String.Empty;
        private string msgNoneSelectedTargetVariableGroup = String.Empty;
        private string msgSelectedTargetVariableNotExist = String.Empty;
        private string msgCannotDeleteSelectedTargetVariable = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Informace o příspěvku lokální hustoty"</para>
        /// <para lang="en">Control "Information about local density contribution"</para>
        /// </summary>
        private ControlTDLDsityInformation ctrTDLDsityInformation;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event EventHandler NextClick;

        /// <summary>
        /// <para lang="cs">Událost změny vybrané skupiny cílových proměnných</para>
        /// <para lang="en">Event of the selected target variable group change</para>
        /// </summary>
        public event EventHandler SelectedTargetVariableGroupChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTDSelectionTargetVariableCore(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTargetData)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetData)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Vybraná skupina objektů lokálních hustot (vybraná na 1.formuláři) (read-only)</para>
        /// <para lang="en">Selected group of local density objects (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectGroup SelectedLocalDensityObjectGroup
        {
            get
            {
                return ((ControlTargetData)ControlOwner)
                    .CtrTDSelectionLDsityObjectGroup
                    .SelectedLocalDensityObjectGroup;
            }
        }

        // Výsledkem výběru v této komponentě je vybraná skupina cílových proměnných a jejich příspěvky lokálních hustot:
        // The result of the selection in this component are selected target variables and their local density contributions:

        /// <summary>
        /// <para lang="cs">Vybraná skupina cílových proměnných</para>
        /// <para lang="en">Selected target variable group</para>
        /// </summary>
        public TDFnGetTargetVariableType SelectedTargetVariableGroup
        {
            get
            {
                return selectedTargetVariableGroup;
            }
            set
            {
                selectedTargetVariableGroup = value;

                selectedTargetVariable =
                    (SelectedTargetVariableGroup == null) ? null :
                    (SelectedTargetVariableGroup.TargetVariableIdList == null) ? null :
                    (SelectedTargetVariableGroup.TargetVariableIdList.Count == 0) ? null :
                    Database.STargetData.CTargetVariable[SelectedTargetVariableGroup.TargetVariableIdList.FirstOrDefault() ?? 0];

                selectedLDsities =
                    (SelectedTargetVariableGroup == null) ? null :
                        TDFunctions.FnGetLDsity.Execute(
                            database: Database,
                            ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Core,
                            targetVariableIds: SelectedTargetVariableGroup.TargetVariableIdList
                        );

                EnableButtonNext();

                InitializeListBoxBoxLDsity();

                SetCaption();

                SelectedTargetVariableGroupChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná - první ve skupině (read-only)</para>
        /// <para lang="en">Selected target variable - first in the group (read-only)</para>
        /// </summary>
        public TDTargetVariable SelectedTargetVariable
        {
            get
            {
                return selectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané příspěvky lokálních hustot pro vybranou cílovou proměnnou (read-only)</para>
        /// <para lang="en">Selected local density contributions for the selected target variable (read-only)</para>
        /// </summary>
        public TDLDsityList SelectedLDsities
        {
            get
            {
                return selectedLDsities;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný příspěvek lokálních hustot pro vybranou skupinu cílových proměnných</para>
        /// <para lang="en">Selected local density contribution for the selected target variable group</para>
        /// </summary>
        public TDLDsity SelectedLDsity
        {
            get
            {
                return selectedLDsity;
            }
            set
            {
                selectedLDsity = value;
                EnableButtonNext();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnTargetVariableEdit),                  "Editovat cílovou proměnnou" },
                        { nameof(btnTargetVariableCopy),                  "Vytvořit kopii vybrané cílové proměnné" },
                        { nameof(btnTargetVariableAdd),                   "Vytvořit novou cílovou proměnnou" },
                        { nameof(btnTargetVariableDelete),                "Smazat cílovou proměnnou" },
                        { nameof(btnLDsityEdit),                          "Editovat lokální hustotu" },
                        { nameof(btnPrevious),                            "Předchozí" },
                        { nameof(btnNext),                                "Další" },
                        { nameof(grpTargetVariable),                      "Cílová proměnná:" },
                        { nameof(grpLDsity),                              "Lokální hustota:" },
                        { nameof(lblMainCaption),                         "Volba cílové proměnné" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),    "Vybraná skupina objektů lokálních hustot:" },
                        { nameof(lblSelectedTargetVariableCaption),       "Vybraná cílová proměnná:" },
                        { nameof(cboStateOrChange),                       "ExtendedLabelCs" },
                        { nameof(lstTargetVariable),                      "ExtendedLabelCs" },
                        { nameof(lstLDsity),                              "ExtendedLabelCs" },
                        { nameof(tsrTargetVariable),                      String.Empty },
                        { nameof(tsrLDsity),                              String.Empty },
                        { nameof(msgNone),                                "žádná" },
                        { nameof(msgNoneSelectedTargetVariableGroup),     "Žádná skupina cílových proměnných není vybraná." },
                        { nameof(msgSelectedTargetVariableNotExist),      "Cílová proměnná $1 neexistuje v databázové tabulce." },
                        { nameof(msgCannotDeleteSelectedTargetVariable),  "Vybranou cílovou proměnnou $1 nelze smazat." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionTargetVariableCore),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnTargetVariableEdit),                  "Edit target variable" },
                        { nameof(btnTargetVariableCopy),                  "Create a copy of the selected target variable" },
                        { nameof(btnTargetVariableAdd),                   "Create new target variable" },
                        { nameof(btnTargetVariableDelete),                "Delete selected target variable" },
                        { nameof(btnLDsityEdit),                          "Edit local density" },
                        { nameof(btnPrevious),                            "Previous" },
                        { nameof(btnNext),                                "Next" },
                        { nameof(grpTargetVariable),                      "Target variable:" },
                        { nameof(grpLDsity),                              "Local density:" },
                        { nameof(lblMainCaption),                         "Selection of the target variable" },
                        { nameof(lblSelectedLDsityObjectGroupCaption),    "Selected local density object group:" },
                        { nameof(lblSelectedTargetVariableCaption),       "Selected target variable:" },
                        { nameof(cboStateOrChange),                       "ExtendedLabelEn" },
                        { nameof(lstTargetVariable),                      "ExtendedLabelEn" },
                        { nameof(lstLDsity),                              "ExtendedLabelEn" },
                        { nameof(tsrTargetVariable),                      String.Empty },
                        { nameof(tsrLDsity),                              String.Empty },
                        { nameof(msgNone),                                "none" },
                        { nameof(msgNoneSelectedTargetVariableGroup),     "Target variable group is not selected." },
                        { nameof(msgSelectedTargetVariableNotExist),      "Selected target variable $1 doesn't exist in database table." },
                        { nameof(msgCannotDeleteSelectedTargetVariable),  "Selected target variable $1 cannot be deleted."  }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTDSelectionTargetVariableCore),
                        out Dictionary<string, string> value)
                            ? value
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            fnTargetVariable = null;

            selectedTargetVariableGroup = null;
            selectedTargetVariable = null;
            selectedLDsities = null;
            selectedLDsity = null;

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            pnlLDsityInformation.Controls.Clear();
            ctrTDLDsityInformation =
                new ControlTDLDsityInformation(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            pnlLDsityInformation.Controls.Add(
                value: ctrTDLDsityInformation);

            InitializeLabels();

            SelectedTargetVariableGroup = null;

            btnTargetVariableEdit.Click += new EventHandler(
                (sender, e) => { UpdateTargetVariable(); });

            btnTargetVariableCopy.Click += new EventHandler(
                (sender, e) => { CopyTargetVariable(); });

            btnTargetVariableAdd.Click += new EventHandler(
                (sender, e) => { InsertTargetVariable(); });

            btnTargetVariableDelete.Click += new EventHandler(
                (sender, e) => { DeleteTargetVariable(); });

            btnLDsityEdit.Click += new EventHandler(
                (sender, e) => { UpdateLDsity(); });

            btnPrevious.Click += new EventHandler(
                (sender, e) => { PreviousClick?.Invoke(sender: sender, e: e); });

            btnNext.Click += new EventHandler(
                (sender, e) => { NextClick?.Invoke(sender: sender, e: e); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnTargetVariableEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnTargetVariableEdit),
                    out string btnTargetVariableEditText)
                        ? btnTargetVariableEditText
                        : String.Empty;

            btnTargetVariableCopy.Text =
                labels.TryGetValue(
                    key: nameof(btnTargetVariableCopy),
                    out string btnTargetVariableCopyText)
                        ? btnTargetVariableCopyText
                        : String.Empty;

            btnTargetVariableAdd.Text =
                labels.TryGetValue(
                    key: nameof(btnTargetVariableAdd),
                    out string btnTargetVariableAddText)
                        ? btnTargetVariableAddText
                        : String.Empty;

            btnTargetVariableDelete.Text =
                labels.TryGetValue(
                    key: nameof(btnTargetVariableDelete),
                    out string btnTargetVariableDeleteText)
                        ? btnTargetVariableDeleteText
                        : String.Empty;

            btnLDsityEdit.Text =
                labels.TryGetValue(
                    key: nameof(btnLDsityEdit),
                    out string btnLDsityEditText)
                        ? btnLDsityEditText
                        : String.Empty;

            btnPrevious.Text =
                labels.TryGetValue(
                    key: nameof(btnPrevious),
                    out string btnPreviousText)
                        ? btnPreviousText
                        : String.Empty;

            btnNext.Text =
                labels.TryGetValue(
                    key: nameof(btnNext),
                    out string btnNextText)
                        ? btnNextText
                        : String.Empty;

            grpTargetVariable.Text =
                labels.TryGetValue(
                    key: nameof(grpTargetVariable),
                    out string grpTargetVariableText)
                        ? grpTargetVariableText
                        : String.Empty;

            grpLDsity.Text =
                labels.TryGetValue(
                    key: nameof(grpLDsity),
                    out string grpLDsityText)
                        ? grpLDsityText
                        : String.Empty;

            lblMainCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblMainCaption),
                    out string lblMainCaptionText)
                        ? lblMainCaptionText
                        : String.Empty;

            lblSelectedLDsityObjectGroupCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblSelectedLDsityObjectGroupCaption),
                    out string lblSelectedLDsityObjectGroupCaptionText)
                        ? lblSelectedLDsityObjectGroupCaptionText
                        : String.Empty;

            lblSelectedTargetVariableCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblSelectedTargetVariableCaption),
                    out string lblSelectedTargetVariableCaptionText)
                        ? lblSelectedTargetVariableCaptionText
                        : String.Empty;

            tsrTargetVariable.Text =
                labels.TryGetValue(
                    key: nameof(tsrTargetVariable),
                    out string tsrTargetVariableText)
                        ? tsrTargetVariableText
                        : String.Empty;

            tsrLDsity.Text = labels.TryGetValue(
                key: nameof(tsrLDsity),
                out string tsrLDsityText)
                    ? tsrLDsityText
                    : String.Empty;

            msgNone =
                labels.TryGetValue(key: nameof(msgNone),
                        out msgNone)
                            ? msgNone
                            : String.Empty;

            msgNoneSelectedTargetVariableGroup =
                labels.TryGetValue(key: nameof(msgNoneSelectedTargetVariableGroup),
                        out msgNoneSelectedTargetVariableGroup)
                            ? msgNoneSelectedTargetVariableGroup
                            : String.Empty;

            msgSelectedTargetVariableNotExist =
                labels.TryGetValue(key: nameof(msgSelectedTargetVariableNotExist),
                        out msgSelectedTargetVariableNotExist)
                            ? msgSelectedTargetVariableNotExist
                            : String.Empty;

            msgCannotDeleteSelectedTargetVariable =
                labels.TryGetValue(key: nameof(msgCannotDeleteSelectedTargetVariable),
                        out msgCannotDeleteSelectedTargetVariable)
                            ? msgCannotDeleteSelectedTargetVariable
                            : String.Empty;

            onEdit = true;

            cboStateOrChange.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboStateOrChange),
                    out string cboStateOrChangeDisplayMember)
                        ? cboStateOrChangeDisplayMember
                        : ID;

            lstTargetVariable.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstTargetVariable),
                    out string lstTargetVariableDisplayMember)
                        ? lstTargetVariableDisplayMember
                        : ID;

            lstLDsity.DisplayMember =
                labels.TryGetValue(
                    key: nameof(lstLDsity),
                    out string lstLDsityDisplayMember)
                        ? lstLDsityDisplayMember
                        : ID;

            onEdit = false;

            SetCaption();

            ctrTDLDsityInformation.InitializeLabels();

            SelectLDsity();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Database.STargetData.CTargetVariable.ReLoad();

            Database.STargetData.CLDsity =
                TDFunctions.FnGetLDsity.Execute(
                    database: Database,
                    ldsityObjectTypeId: (int)TDLDsityObjectTypeEnum.Core,
                    targetVariableIds: null);

            Database.STargetData.CUnitOfMeasure =
                TDFunctions.FnGetUnitOfMeasure.Execute(
                    database: Database,
                    ldsityId: null);

            Database.STargetData.CDefinitionVariant =
                TDFunctions.FnGetDefinitionVariant.Execute(
                    database: Database,
                    definitionVariantId: null);

            Database.STargetData.CAreaDomainCategory =
                TDFunctions.FnGetAreaDomainCategory.Execute(
                    database: Database,
                    targetVariableId: null,
                    ldsityId: null,
                    useNegative: null);

            Database.STargetData.CSubPopulationCategory =
                TDFunctions.FnGetSubPopulationCategory.Execute(
                    database: Database,
                    targetVariableId: null,
                    ldsityId: null,
                    useNegative: null);

            Database.STargetData.CVersion =
                TDFunctions.FnGetVersion.Execute(
                    database: Database,
                    targetVariableId: null,
                    ldsityId: null,
                    useNegative: null);

            //pro FormTargetVariableNew
            Database.STargetData.CStateOrChange =
                TDFunctions.FnGetStateOrChange.Execute(
                    database: Database);

            // pro FormTargetVariableNew
            Database.STargetData.CLDsityObjectType =
                TDFunctions.FnGetLDsityObjectType.Execute(
                    database: Database);

            fnTargetVariable =
                TDFunctions.FnGetTargetVariable.Execute(
                    database: Database,
                    ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id);

            InitializeComboBoxStateOrChange();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis ovládacího prvku</para>
        /// <para lang="en">Sets the control caption</para>
        /// </summary>
        private void SetCaption()
        {
            string selectedLocalDensityObjectGroupId;
            string selectedLocalDensityObjectGroupCaption;
            string selectedTargetVariableGroupId;
            string selectedTargetVariableGroupCaption;

            if (SelectedLocalDensityObjectGroup != null)
            {
                selectedLocalDensityObjectGroupId = SelectedLocalDensityObjectGroup.Id.ToString();
                selectedLocalDensityObjectGroupCaption =
                    (LanguageVersion == LanguageVersion.National) ? SelectedLocalDensityObjectGroup.LabelCs :
                    (LanguageVersion == LanguageVersion.International) ? SelectedLocalDensityObjectGroup.LabelEn :
                    String.Empty;
                lblSelectedLDsityObjectGroupValue.Text =
                    $"({selectedLocalDensityObjectGroupId}) {selectedLocalDensityObjectGroupCaption}";
            }
            else
            {
                lblSelectedLDsityObjectGroupValue.Text = msgNone;
            }

            if (SelectedTargetVariableGroup != null)
            {
                selectedTargetVariableGroupId = SelectedTargetVariableGroup.TargetVariableIds;
                selectedTargetVariableGroupCaption =
                   (LanguageVersion == LanguageVersion.National) ? SelectedTargetVariableGroup.LabelCs :
                   (LanguageVersion == LanguageVersion.International) ? SelectedTargetVariableGroup.LabelEn : String.Empty;

                lblSelectedTargetVariableValue.Text =
                    $"({selectedTargetVariableGroupId}) {selectedTargetVariableGroupCaption}";
            }
            else
            {
                lblSelectedTargetVariableValue.Text = msgNone;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost tlačítka "Další"</para>
        /// <para lang="en">Enable or Disable button "Next"</para>
        /// </summary>
        private void EnableButtonNext()
        {
            btnNext.Enabled =
                (SelectedTargetVariableGroup != null) &&
                (SelectedTargetVariable != null) &&
                (SelectedLDsities != null) &&
                 SelectedLDsities.Items.Count != 0 &&
                (SelectedLDsity != null);
        }

        /// <summary>
        /// <para lang="cs">Do ComboBoxu StateOrChange vyplní seznam typů cílových proměnných</para>
        /// <para lang="en">Fills the StateOrChange ComboBox with the list of target variable types</para>
        /// </summary>
        private void InitializeComboBoxStateOrChange()
        {
            onEdit = true;
            cboStateOrChange = new ComboBox()
            {
                DataSource = Database.STargetData.CStateOrChange.Items,
                DisplayMember = Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                                 .TryGetValue(
                                     key: nameof(cboStateOrChange),
                                     out string cboStateOrChangeDisplayMember)
                                        ? cboStateOrChangeDisplayMember
                                        : ID,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                Tag = Database.STargetData.CStateOrChange
            };
            cboStateOrChange.CreateControl();
            cboStateOrChange.SelectedIndexChanged += (sender, e) =>
            {
                if (!onEdit)
                {
                    InitializeListBoxTargetVariable();
                }
            };
            onEdit = false;

            pnlStateOrChange.Controls.Clear();
            pnlStateOrChange.Controls.Add(
                value: cboStateOrChange);

            InitializeListBoxTargetVariable();
        }

        /// <summary>
        /// <para lang="cs">Do ListBoxu TargetVariable vyplní seznam cílových proměnných</para>
        /// <para lang="en">Fills the TargetVariable ListBox with the list of target variables</para>
        /// </summary>
        private void InitializeListBoxTargetVariable()
        {
            TDStateOrChange soc =
                (cboStateOrChange.SelectedItem == null) ? null :
                (TDStateOrChange)cboStateOrChange.SelectedItem;

            onEdit = true;
            lstTargetVariable = new ListBox()
            {
                DataSource =
                    (soc == null) ? fnTargetVariable?.Items :
                    fnTargetVariable?.Items
                    .Where(a => a.StateOrChangeId == soc.Id)
                    .ToList<TDFnGetTargetVariableType>(),
                DisplayMember = Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                                 .TryGetValue(key: nameof(lstTargetVariable),
                                 out string lstTargetVariableDisplayMember)
                                    ? lstTargetVariableDisplayMember
                                    : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = fnTargetVariable
            };
            lstTargetVariable.CreateControl();

            lstTargetVariable.SelectedIndexChanged += (sender, e) =>
            {
                if (!onEdit)
                {
                    SelectTargetVariableGroup();
                }
            };
            onEdit = false;

            pnlTargetVariable.Controls.Clear();
            pnlTargetVariable.Controls.Add(
                value: lstTargetVariable);

            SelectTargetVariableGroup();
        }

        /// <summary>
        /// <para lang="cs">Do ListBoxu LDsity doplní seznam lokálních hustot pro vybranou cílovou proměnnou</para>
        /// <para lang="en">Adds a list of local densities for the selected target variable to the LDsity ListBox</para>
        /// </summary>
        private void InitializeListBoxBoxLDsity()
        {
            onEdit = true;
            lstLDsity = new ListBox()
            {
                DataSource = SelectedLDsities?.Items,
                DisplayMember =
                     Dictionary(languageVersion: LanguageVersion, languageFile: LanguageFile)
                         .TryGetValue(key: nameof(lstLDsity),
                         out string lstLDsityDisplayMember)
                            ? lstLDsityDisplayMember
                            : ID,
                Dock = DockStyle.Fill,
                Font = new System.Drawing.Font(
                    familyName: "Microsoft Sans Serif",
                    emSize: 9F,
                    style: System.Drawing.FontStyle.Regular,
                    unit: System.Drawing.GraphicsUnit.Point,
                    gdiCharSet: (byte)238),
                Margin = new Padding(all: 0),
                SelectionMode = SelectionMode.One,
                Tag = SelectedLDsities
            };
            lstLDsity.CreateControl();

            lstLDsity.SelectedIndexChanged += (sender, e) =>
            {
                if (!onEdit)
                {
                    SelectLDsity();
                }
            };
            onEdit = false;

            pnlLDsity.Controls.Clear();
            pnlLDsity.Controls.Add(
                value: lstLDsity);

            SelectLDsity();
        }

        /// <summary>
        /// <para lang="cs">Spouští se v případě, že v ListBoxu TargetVariable byla vybrána cílová proměnná</para>
        /// <para lang="en">Fires if the target variable has been selected in the TargetVariable ListBox</para>
        /// </summary>
        private void SelectTargetVariableGroup()
        {
            if (lstTargetVariable.SelectedItem != null)
            {
                SelectedTargetVariableGroup =
                    (TDFnGetTargetVariableType)lstTargetVariable.SelectedItem;
            }
            else
            {
                SelectedTargetVariableGroup = null;
            }
        }

        /// <summary>
        /// <para lang="cs">Provede výběr TargetVariableGroup v ListBoxu</para>
        /// <para lang="en">It selects item in ListBox TargetVariableGroup</para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikátor skupiny cílových proměnných</para>
        /// <para lang="en">Target variable group identifier</para>
        /// </param>
        private void SelectTargetVariableGroup(int id)
        {
            // Výběr editované položky po aktualizaci databáze
            // Selecting an edited item after updating the database
            if (lstTargetVariable.Items
                    .OfType<TDFnGetTargetVariableType>()
                    .Where(a => a.Id == id)
                    .Any())
            {
                lstTargetVariable.SelectedItem =
                    lstTargetVariable.Items
                    .OfType<TDFnGetTargetVariableType>()
                    .Where(a => a.Id == id)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// <para lang="cs">Spouští se v případě, že v ListBoxu LDsity byla vybrána lokální hustota</para>
        /// <para lang="en">Triggers if the local density was selected in the LDsity ListBox</para>
        /// </summary>
        private void SelectLDsity()
        {
            // Není vybraná cílová proměnná,
            // pak nemůže být vybraný ani příspěvek lokální hustoty
            if (SelectedTargetVariable == null)
            {
                SelectedLDsity = null;
            }
            else
            {
                SelectedLDsity =
                    (lstLDsity.SelectedItem == null) ? null :
                    (TDLDsity)lstLDsity.SelectedItem;
            }

            // Není vybraný příspěvek lokální hustoty, pak není co zobrazovat
            if (SelectedLDsity == null)
            {
                ctrTDLDsityInformation.Visible = false;
            }
            else
            {
                ctrTDLDsityInformation.Visible = true;
                ctrTDLDsityInformation.ShowInformation(
                    targetVariable: SelectedTargetVariable,
                    ldsity: SelectedLDsity);
            }
        }

        /// <summary>
        /// <para lang="cs">Provede výběr LDsity v ListBoxu</para>
        /// <para lang="en">It selects item in ListBox LDsity</para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikátor příspěvku lokální hustoty</para>
        /// <para lang="en">Local denisty contribution identifier</para>
        /// </param>
        private void SelectLDsity(int id)
        {
            // Výběr editované položky po aktualizaci databáze
            // Selecting an edited item after updating the database
            if (lstLDsity.Items
                    .OfType<TDLDsity>()
                    .Where(a => a.Id == id)
                    .Any())
            {
                lstLDsity.SelectedItem =
                    lstLDsity.Items
                    .OfType<TDLDsity>()
                    .Where(a => a.Id == id)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků cílové proměnné</para>
        /// <para lang="en">Editing target variable labels</para>
        /// </summary>
        private void UpdateTargetVariable()
        {
            if (SelectedTargetVariableGroup == null)
            {
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CTargetVariable,
                displayedItemsIds: SelectedTargetVariableGroup.TargetVariableIdList);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                int selectedTargetVariableGroupId =
                    (SelectedTargetVariableGroup != null) ?
                    SelectedTargetVariableGroup.Id : 0;
                int selectedLDsityId =
                    (SelectedLDsity != null) ?
                    SelectedLDsity.Id : 0;
                LoadContent();
                SelectTargetVariableGroup(id: selectedTargetVariableGroupId);
                SelectLDsity(id: selectedLDsityId);
            }
        }

        /// <summary>
        /// <para lang="cs">Vložení nové cílové proměnné</para>
        /// <para lang="en">Inserting a new target variable</para>
        /// </summary>
        private void InsertTargetVariable()
        {
            FormTargetVariableCoreNew frm =
                new(
                    controlOwner: this,
                    useCopy: false);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Vložení kopie cílové proměnné</para>
        /// <para lang="en">Inserting a copy of target variable</para>
        /// </summary>
        private void CopyTargetVariable()
        {
            FormTargetVariableCoreNew frm =
                new(
                    controlOwner: this,
                    useCopy: true);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání vybrané cílové proměnné</para>
        /// <para lang="en">Delete selected target variable</para>
        /// </summary>
        private void DeleteTargetVariable()
        {
            if
            (
                (SelectedTargetVariableGroup == null) ||
                (SelectedTargetVariableGroup.TargetVariableIdList == null) ||
                (SelectedTargetVariableGroup.TargetVariableIdList.Count == 0)
            )
            {
                // Žádná skupina cílových proměnných není vybraná nebo vybraná skupina je prázdná
                // No group of target variables selected or selected group is empty
                MessageBox.Show(
                    text: msgNoneSelectedTargetVariableGroup,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            foreach (Nullable<int> targetVariableId in
                SelectedTargetVariableGroup.TargetVariableIdList)
            {
                if ((targetVariableId == null) ||
                    (!Database.STargetData.CTargetVariable.Items
                    .Where(a => a.Id == (int)targetVariableId)
                    .Any()))
                {
                    // Vybraná cílová proměnná není v databázové tabulce (nemůže nastat)
                    // The selected target variable is not in the database table (cannot occur)
                    string strTargetVariableId =
                        (targetVariableId == null)
                            ? Functions.StrNull
                            : targetVariableId.ToString();

                    MessageBox.Show(
                        text: msgSelectedTargetVariableNotExist,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return;
                }
            }

            foreach (Nullable<int> targetVariableId in
                SelectedTargetVariableGroup.TargetVariableIdList)
            {
                if (!TDFunctions.FnTryDeleteTargetVariable.Execute(
                        database: Database,
                        targetVariableId: targetVariableId))
                {
                    // Vybranou cílovou proměnnou nelze smazat
                    // The selected target variable cannot be deleted
                    string strTargetVariableId =
                        (targetVariableId == null)
                            ? Functions.StrNull
                            : targetVariableId.ToString();

                    MessageBox.Show(
                        text: msgCannotDeleteSelectedTargetVariable
                            .Replace("$1", strTargetVariableId),
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return;
                }

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnTryDeleteTargetVariable.CommandText,
                        caption: TDFunctions.FnTryDeleteTargetVariable.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            foreach (Nullable<int> targetVariableId in
               SelectedTargetVariableGroup.TargetVariableIdList)
            {
                TDFunctions.FnDeleteTargetVariable.Execute(
                    database: Database,
                    targetVariableId: targetVariableId);

                if (Setting.Verbose)
                {
                    MessageBox.Show(
                        text: TDFunctions.FnDeleteTargetVariable.CommandText,
                        caption: TDFunctions.FnDeleteTargetVariable.Name,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Úprava popisků lokální hustoty</para>
        /// <para lang="en">Editing local density labels</para>
        /// </summary>
        private void UpdateLDsity()
        {
            if (SelectedTargetVariableGroup == null)
            {
                return;
            }

            if (SelectedLDsity == null)
            {
                return;
            }

            FormLookupTable frmLookupTable = new(
                controlOwner: this,
                lookupTable: Database.STargetData.CLDsity,
                displayedItemsIds: [SelectedLDsity.Id]);

            if (frmLookupTable.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
                SelectTargetVariableGroup(id: SelectedTargetVariableGroup.Id);
                SelectLDsity(id: SelectedLDsity.Id);
            }
        }

        #endregion Methods

    }

}