﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Klasifikační pravidlo</para>
        /// <para lang="en">Classification rule</para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        [SupportedOSPlatform("windows")]
        internal class ClassificationRule<TDomain, TCategory>
            : INfiEstaObject, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            private object owner;

            /// <summary>
            /// <para lang="cs">Text klasifikačního pravidla</para>
            /// <para lang="en">Classification rule text</para>
            /// </summary>
            private string text;

            /// <summary>
            /// <para lang="cs">Validita klasifikačního pravidla</para>
            /// <para lang="en">Classification rule validity</para>
            /// </summary>
            private bool isValid;

            private string msgIsValid = String.Empty;
            private string msgIsNotValid = String.Empty;

            #endregion Private Fields


            #region Contructor

            /// <summary>
            /// <para lang="cs">Konstruktor klasifikačního pravidla</para>
            /// <para lang="en">Classification rule constructor</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            public ClassificationRule(object owner)
            {
                Initialize(owner: owner);
            }

            #endregion Constructor


            #region Common Properties

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            public object Owner
            {
                get
                {
                    if (owner == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                    }

                    if (owner is not ClassificationRulePair<TDomain, TCategory>)
                    {
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(Owner)} must be type of ",
                                $"{nameof(ClassificationRulePair<TDomain, TCategory>)}<TDomain,TCategory>."),
                            paramName: nameof(Owner));
                    }

                    return owner;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Owner)} must not be null.",
                            paramName: nameof(Owner));
                    }

                    if (value is not ClassificationRulePair<TDomain, TCategory>)
                    {
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(Owner)} must be type of ",
                                $"{nameof(ClassificationRulePair<TDomain, TCategory>)}<TDomain,TCategory>."),
                            paramName: nameof(Owner));
                    }

                    owner = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Databázové tabulky (read-only)</para>
            /// <para lang="en">Database tables(read-only)</para>
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).Database;
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyková verze (read-only)</para>
            /// <para lang="en">Language version (read-only)</para>
            /// </summary>
            public LanguageVersion LanguageVersion
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).LanguageVersion;
                }
            }

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            public LanguageFile LanguageFile
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).LanguageFile;
                }
            }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
            /// <para lang="en">Module for target data setting (read-only)</para>
            /// </summary>
            public Setting Setting
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).Setting;
                }
            }

            #endregion Common Properties


            #region Properties

            /// <summary>
            /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
            /// <para lang="en">Domain and its categories type (read-only)</para>
            /// </summary>
            public static TDArealOrPopulationEnum ArealOrPopulation
            {
                get
                {
                    return typeof(TDomain).FullName switch
                    {
                        "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    TDArealOrPopulationEnum.AreaDomain,

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                            typeof(TCategory).FullName switch
                            {
                                "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Incompatible types ",
                                            $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                        paramName: nameof(TCategory)),

                                "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                    TDArealOrPopulationEnum.Population,

                                _ =>
                                    throw new ArgumentException(
                                        message: String.Concat(
                                            $"Argument {nameof(TCategory)} must be type of ",
                                            $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                        paramName: nameof(TCategory)),
                            },

                        _ =>
                            throw new ArgumentException(
                                message: String.Concat(
                                    $"Argument {nameof(TDomain)} must be type of ",
                                    $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                                paramName: nameof(TDomain))
                    };
                }
            }

            /// <summary>
            /// <para lang="cs">Uzel v TreeView (read-only)</para>
            /// <para lang="en">Node in TreeView (read-only)</para>
            /// </summary>
            public TreeNode Node
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).Node;
                }
            }

            /// <summary>
            /// <para lang="cs">TreeView nemá hlubší úrovně než 0 (read-only)</para>
            /// <para lang="en">TreeView has only TreeNode level 0 (read-only)</para>
            /// </summary>
            private bool Flat
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).Flat;
                }
            }

            /// <summary>
            /// <para lang="cs">Kategorie plošné domény nebo subpopulace (read-only)</para>
            /// <para lang="en">Area domain category or subpopulation category (read-only)</para>
            /// </summary>
            public TCategory Category
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).Category;
                }
            }

            /// <summary>
            /// <para lang="cs">Objekt s příspěvkem do lokální hustoty (read-only)</para>
            /// <para lang="en">Object with a contribution to a local density (read-only)</para>
            /// </summary>
            public TDLDsityObject LDsityObject
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).LDsityObject;
                }
            }

            /// <summary>
            /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění (read-only)</para>
            /// <para lang="en">Local density object used for classification (read-only)</para>
            /// </summary>
            public TDLDsityObject LDsityObjectForADSP
            {
                get
                {
                    return ((ClassificationRulePair<TDomain, TCategory>)Owner).LDsityObjectForADSP;
                }
            }

            /// <summary>
            /// <para lang="cs">Text klasifikačního pravidla</para>
            /// <para lang="en">Classification rule text</para>
            /// </summary>
            public string Text
            {
                get
                {
                    return ReplaceLineEnding(s: text);
                }
                set
                {
                    // Každá změna textu klasifikačního pravidla vyžaduje
                    // spuštění validace
                    text = ReplaceLineEnding(s: value);
                    isValid = false;
                }
            }

            /// <summary>
            /// <para lang="cs">Nahradí zakončení řádků v textu</para>
            /// <para lang="en">Replace line endings in text</para>
            /// </summary>
            /// <param name="s">Text</param>
            /// <returns>Text with replaced line endings</returns>
            private static string ReplaceLineEnding(string s)
            {
                if (String.IsNullOrEmpty(value: s))
                {
                    return s;
                }

                return s
                    .Replace(oldValue: $"{(char)13}{(char)10}", newValue: "$NewLine")
                    .Replace(oldValue: $"{(char)13}", newValue: "$NewLine")
                    .Replace(oldValue: $"{(char)10}", newValue: "$NewLine")
                    .Replace(oldValue: $"$NewLine", newValue: Environment.NewLine);
            }

            /// <summary>
            /// <para lang="cs">Validita klasifikačního pravidla (read-only)</para>
            /// <para lang="en">Classification rule validity (read-only)</para>
            /// </summary>
            public bool IsValid
            {
                get
                {
                    if (Flat)
                    {
                        return isValid;
                    }
                    else
                    {
                        switch (Node.Level)
                        {
                            case 0:
                            case 1:
                                List<bool> validity =
                                    Node.Nodes
                                        .OfType<TreeNode>()
                                        .Where(a => a.Tag != null)
                                        .Select(a => a.Tag)
                                        .OfType<ClassificationRulePair<TDomain, TCategory>>()
                                        .Select(a =>
                                            (a.Negative == null) ?
                                                a.Positive.IsValid :
                                                a.Positive.IsValid && a.Negative.IsValid)
                                        .ToList<bool>();
                                return validity.Count != 0 &&
                                    validity.Aggregate((a, b) => a && b);

                            case 2:
                                return isValid;

                            default:
                                throw new ArgumentException(
                                  message: "Tree node must have level 0, 1 or 2.",
                                  paramName: nameof(Node));
                        }
                    }
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Zvolený jazyk</para>
            /// <para lang="en">Selected language</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">File with control labels for national and international version</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts in selected language</para>
            /// </returns>
            public static Dictionary<string, string> Dictionary(
                LanguageVersion languageVersion,
                LanguageFile languageFile)
            {
                return languageVersion switch
                {
                    LanguageVersion.National =>
                    ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                            ? new Dictionary<string, string>()
                            {
                                { nameof(msgIsValid),    "Klasifikační pravidlo má validní syntaxi." },
                                { nameof(msgIsNotValid), "Klasifikační pravidlo nemá validní syntaxi." }
                            }
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(ClassificationRule<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                                out Dictionary<string, string> dictNationalAreaDomain)
                                    ? dictNationalAreaDomain
                                    : [],

                        TDArealOrPopulationEnum.Population => (languageFile == null)
                            ? new Dictionary<string, string>()
                            {
                                { nameof(msgIsValid),    "Klasifikační pravidlo má validní syntaxi." },
                                { nameof(msgIsNotValid), "Klasifikační pravidlo nemá validní syntaxi." }
                            }
                            : languageFile.NationalVersion.Data.TryGetValue(
                                key: $"{nameof(ClassificationRule<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                out Dictionary<string, string> dictNationalSubPopulation)
                                    ? dictNationalSubPopulation
                                    : [],

                        _ => [],
                    },

                    LanguageVersion.International =>
                    ArealOrPopulation switch
                    {
                        TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                            ? new Dictionary<string, string>()
                            {
                                { nameof(msgIsValid),    "Syntax of the classification rule is valid." },
                                { nameof(msgIsNotValid), "Syntax of the classification rule is not valid." }
                            }
                            : languageFile.InternationalVersion.Data.TryGetValue(
                                key: $"{nameof(ClassificationRule<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                                out Dictionary<string, string> dictInternationalAreaDomain)
                                    ? dictInternationalAreaDomain
                                    : [],

                        TDArealOrPopulationEnum.Population => (languageFile == null)
                            ? new Dictionary<string, string>()
                            {
                                { nameof(msgIsValid),    "Syntax of the classification rule is valid." },
                                { nameof(msgIsNotValid), "Syntax of the classification rule is not valid." }
                            }
                            : languageFile.InternationalVersion.Data.TryGetValue(
                                key: $"{nameof(ClassificationRule<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                                out Dictionary<string, string> dictInternationalSubPopulation)
                                    ? dictInternationalSubPopulation
                                    : [],

                        _ => [],
                    },

                    _ => [],
                };
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Inicializace objektu klasifikačního pravidla</para>
            /// <para lang="en">Initializing the classification rule object</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            private void Initialize(object owner)
            {
                Owner = owner;
                Text = String.Empty;
            }

            /// <summary>
            /// <para lang="cs">Kontroluje syntaxi vybraného klasifikačního pravidla</para>
            /// <para lang="en">It checks the syntax of the selected classification rule</para>
            /// </summary>
            /// <param name="verbose">
            /// <para lang="cs">Zobrazení hlášení o validitě klasifikačního pravidla</para>
            /// <para lang="en">To display a classification rule validity report</para>
            /// </param>
            public void ValidateSyntax(bool verbose)
            {
                if ((!Flat) && (Node.Level != 2))
                {
                    return;
                }

                isValid =
                    TDFunctions.FnCheckClassificationRuleSyntax.Execute(
                        database: Database,
                        ldsityObjectId: LDsityObjectForADSP.Id,
                        rule: Text) ?? false;

                if (verbose)
                {
                    Dictionary<string, string> messages = Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: LanguageFile);
                    MessageBox.Show(
                        text: IsValid
                           ? messages.TryGetValue(
                               key: nameof(msgIsValid), out msgIsValid)
                                    ? msgIsValid
                                    : String.Empty
                           : messages.TryGetValue(
                               key: nameof(msgIsNotValid), out msgIsNotValid)
                                    ? msgIsNotValid
                                    : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnCheckClassificationRuleSyntax.CommandText,
                            caption: TDFunctions.FnCheckClassificationRuleSyntax.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Textový popis objektu klasifikačního pravidla</para>
            /// <para lang="en">Text representation of classification rule object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Textový popis objektu klasifikačního pravidla</para>
            /// <para lang="en">Text representation of classification rule object</para>
            /// </returns>
            public override string ToString()
            {
                string strNodeLevel =
                    (Node == null) ? Functions.StrNull :
                     Node.Level.ToString();

                string strCategory =
                    (Category == null) ? Functions.StrNull :
                     Category.ToString();

                string strLDsityObject =
                    (LDsityObject == null) ? Functions.StrNull :
                     LDsityObject.ToString();

                string strLDsityObjectForADSP =
                    (LDsityObjectForADSP == null) ? Functions.StrNull :
                     LDsityObjectForADSP.ToString();

                string strClassificationRule =
                    String.IsNullOrEmpty(value: Text) ? Functions.StrNull :
                    Text;

                return String.Concat(
                    $"Level: {strNodeLevel}; ",
                    $"Category: {strCategory}; ",
                    $"LDsityObject: {strLDsityObject}; ",
                    $"LDsityObjectForADSP: {strLDsityObjectForADSP}; ",
                    $"Text: {strClassificationRule};");
            }

            /// <summary>
            /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
            /// <para lang="en">Determines whether specified object is equal to the current object</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Zadaný objekt</para>
            /// <para lang="en">Speicified object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not ClassificationRule<TCategory> Type, return False
                if (obj is not ClassificationRule<TDomain, TCategory>)
                {
                    return false;
                }

                return
                    Category.Equals(
                        obj: ((ClassificationRule<TDomain, TCategory>)obj).Category) &&

                    LDsityObject.Equals(
                        obj: ((ClassificationRule<TDomain, TCategory>)obj).LDsityObject) &&

                    LDsityObjectForADSP.Equals(
                        obj: ((ClassificationRule<TDomain, TCategory>)obj).LDsityObjectForADSP) &&

                    Text.Equals(
                        obj: ((ClassificationRule<TDomain, TCategory>)obj).Text);
            }

            /// <summary>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </returns>
            public override int GetHashCode()
            {
                return
                    ((Category == null) ? 0 : Category.GetHashCode()) ^
                    ((LDsityObject == null) ? 0 : LDsityObject.GetHashCode()) ^
                    ((LDsityObjectForADSP == null) ? 0 : LDsityObjectForADSP.GetHashCode()) ^
                    ((Text == null) ? 0 : Text.GetHashCode());
            }

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">Třída pro porovnání dvou klasifikačních pravidel.
        /// Klasifikační pravidla jsou stejná pro stejnou kategorii plošné domény nebo subpopulace
        /// a pro stejný objekt lokální hustoty pro třídění
        /// </para>
        /// <para lang="en">Class for comparing two classifiction rules.
        /// Classification rules are equal for the same area domain or subpopulation category and
        /// for the same local density object for classification</para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        internal class ClassificationRuleComparer<TDomain, TCategory>
            : IEqualityComparer<ClassificationRule<TDomain, TCategory>>
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Methods

            /// <summary>
            /// <para lang="cs">Určuje zda zadané dva objekty jsou stejné</para>
            /// <para lang="en">Determines whether two specified objects are equal</para>
            /// </summary>
            /// <param name="classificationRuleA">
            /// <para lang="cs">První objekt klasifikačního pravidla</para>
            /// <para lang="en">First classification rule object</para>
            /// </param>
            /// <param name="classificationRuleB">
            /// <para lang="cs">Druhý objekt klasifikačního pravidla</para>
            /// <para lang="en">Second classification rule object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public bool Equals(
                ClassificationRule<TDomain, TCategory> classificationRuleA,
                ClassificationRule<TDomain, TCategory> classificationRuleB)
            {

                if (Object.ReferenceEquals(classificationRuleA, classificationRuleB))
                    return true;

                if (classificationRuleA == null)
                    return false;

                if (classificationRuleB == null)
                    return false;

                if (OperatingSystem.IsWindows())
                {
                    return
                        (classificationRuleA.Category.Id == classificationRuleB.Category.Id) &&
                        (classificationRuleA.LDsityObjectForADSP.Id == classificationRuleB.LDsityObjectForADSP.Id);
                }

                return false;
            }

            /// <summary>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </returns>
            public int GetHashCode(ClassificationRule<TDomain, TCategory> classificationRule)
            {
                if (classificationRule == null)
                    return 0;

                if (OperatingSystem.IsWindows())
                {
                    if (classificationRule.Category == null)
                        return 0;

                    if (classificationRule.LDsityObjectForADSP == null)
                        return 0;

                    return
                        classificationRule.Category.Id.GetHashCode() ^
                        classificationRule.LDsityObjectForADSP.Id.GetHashCode();
                }

                return 0;
            }

            #endregion Methods

        }

    }
}