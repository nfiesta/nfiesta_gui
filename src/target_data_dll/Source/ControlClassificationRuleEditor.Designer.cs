﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlClassificationRuleEditorDesign
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splOperators = new System.Windows.Forms.SplitContainer();
            this.splColumns = new System.Windows.Forms.SplitContainer();
            this.grpColumns = new System.Windows.Forms.GroupBox();
            this.splCategories = new System.Windows.Forms.SplitContainer();
            this.grpCategories = new System.Windows.Forms.GroupBox();
            this.splClassificationRule = new System.Windows.Forms.SplitContainer();
            this.txtClassificationRule = new System.Windows.Forms.TextBox();
            this.rtbClassificationRule = new System.Windows.Forms.RichTextBox();
            this.tlpOperators = new System.Windows.Forms.TableLayoutPanel();
            this.lblIN = new System.Windows.Forms.Label();
            this.lblComma = new System.Windows.Forms.Label();
            this.lblRightBracket = new System.Windows.Forms.Label();
            this.lblLeftBracket = new System.Windows.Forms.Label();
            this.lblLessOrEqual = new System.Windows.Forms.Label();
            this.lblGreaterOrEqual = new System.Windows.Forms.Label();
            this.lblLess = new System.Windows.Forms.Label();
            this.lblGreater = new System.Windows.Forms.Label();
            this.lblNotEqual = new System.Windows.Forms.Label();
            this.lblEqual = new System.Windows.Forms.Label();
            this.lblNOT = new System.Windows.Forms.Label();
            this.lblOR = new System.Windows.Forms.Label();
            this.lblAND = new System.Windows.Forms.Label();
            this.ssMain = new System.Windows.Forms.StatusStrip();
            this.lblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splOperators)).BeginInit();
            this.splOperators.Panel1.SuspendLayout();
            this.splOperators.Panel2.SuspendLayout();
            this.splOperators.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splColumns)).BeginInit();
            this.splColumns.Panel1.SuspendLayout();
            this.splColumns.Panel2.SuspendLayout();
            this.splColumns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splCategories)).BeginInit();
            this.splCategories.Panel1.SuspendLayout();
            this.splCategories.Panel2.SuspendLayout();
            this.splCategories.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splClassificationRule)).BeginInit();
            this.splClassificationRule.Panel1.SuspendLayout();
            this.splClassificationRule.Panel2.SuspendLayout();
            this.splClassificationRule.SuspendLayout();
            this.tlpOperators.SuspendLayout();
            this.ssMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.splOperators);
            this.pnlMain.Controls.Add(this.ssMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(598, 298);
            this.pnlMain.TabIndex = 2;
            // 
            // splOperators
            // 
            this.splOperators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splOperators.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splOperators.IsSplitterFixed = true;
            this.splOperators.Location = new System.Drawing.Point(0, 0);
            this.splOperators.Margin = new System.Windows.Forms.Padding(0);
            this.splOperators.Name = "splOperators";
            this.splOperators.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splOperators.Panel1
            // 
            this.splOperators.Panel1.Controls.Add(this.splColumns);
            // 
            // splOperators.Panel2
            // 
            this.splOperators.Panel2.Controls.Add(this.tlpOperators);
            this.splOperators.Panel2MinSize = 20;
            this.splOperators.Size = new System.Drawing.Size(596, 274);
            this.splOperators.SplitterDistance = 248;
            this.splOperators.SplitterWidth = 1;
            this.splOperators.TabIndex = 1;
            // 
            // splColumns
            // 
            this.splColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splColumns.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splColumns.Location = new System.Drawing.Point(0, 0);
            this.splColumns.Margin = new System.Windows.Forms.Padding(0);
            this.splColumns.Name = "splColumns";
            // 
            // splColumns.Panel1
            // 
            this.splColumns.Panel1.Controls.Add(this.grpColumns);
            // 
            // splColumns.Panel2
            // 
            this.splColumns.Panel2.Controls.Add(this.splCategories);
            this.splColumns.Size = new System.Drawing.Size(596, 248);
            this.splColumns.SplitterDistance = 150;
            this.splColumns.SplitterWidth = 1;
            this.splColumns.TabIndex = 3;
            // 
            // grpColumns
            // 
            this.grpColumns.BackColor = System.Drawing.SystemColors.Control;
            this.grpColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpColumns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpColumns.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpColumns.Location = new System.Drawing.Point(0, 0);
            this.grpColumns.Margin = new System.Windows.Forms.Padding(0);
            this.grpColumns.Name = "grpColumns";
            this.grpColumns.Padding = new System.Windows.Forms.Padding(5);
            this.grpColumns.Size = new System.Drawing.Size(150, 248);
            this.grpColumns.TabIndex = 8;
            this.grpColumns.TabStop = false;
            this.grpColumns.Text = "grpColumns";
            // 
            // splCategories
            // 
            this.splCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splCategories.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splCategories.Location = new System.Drawing.Point(0, 0);
            this.splCategories.Margin = new System.Windows.Forms.Padding(0);
            this.splCategories.Name = "splCategories";
            // 
            // splCategories.Panel1
            // 
            this.splCategories.Panel1.Controls.Add(this.grpCategories);
            // 
            // splCategories.Panel2
            // 
            this.splCategories.Panel2.Controls.Add(this.splClassificationRule);
            this.splCategories.Size = new System.Drawing.Size(445, 248);
            this.splCategories.SplitterDistance = 150;
            this.splCategories.SplitterWidth = 1;
            this.splCategories.TabIndex = 0;
            // 
            // grpCategories
            // 
            this.grpCategories.BackColor = System.Drawing.SystemColors.Control;
            this.grpCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpCategories.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpCategories.Location = new System.Drawing.Point(0, 0);
            this.grpCategories.Margin = new System.Windows.Forms.Padding(0);
            this.grpCategories.Name = "grpCategories";
            this.grpCategories.Padding = new System.Windows.Forms.Padding(5);
            this.grpCategories.Size = new System.Drawing.Size(150, 248);
            this.grpCategories.TabIndex = 9;
            this.grpCategories.TabStop = false;
            this.grpCategories.Text = "grpCategories";
            // 
            // splClassificationRule
            // 
            this.splClassificationRule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splClassificationRule.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splClassificationRule.IsSplitterFixed = true;
            this.splClassificationRule.Location = new System.Drawing.Point(0, 0);
            this.splClassificationRule.Margin = new System.Windows.Forms.Padding(0);
            this.splClassificationRule.Name = "splClassificationRule";
            this.splClassificationRule.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splClassificationRule.Panel1
            // 
            this.splClassificationRule.Panel1.Controls.Add(this.txtClassificationRule);
            // 
            // splClassificationRule.Panel2
            // 
            this.splClassificationRule.Panel2.Controls.Add(this.rtbClassificationRule);
            this.splClassificationRule.Panel2Collapsed = true;
            this.splClassificationRule.Size = new System.Drawing.Size(294, 248);
            this.splClassificationRule.SplitterDistance = 150;
            this.splClassificationRule.TabIndex = 0;
            // 
            // txtClassificationRule
            // 
            this.txtClassificationRule.AcceptsReturn = true;
            this.txtClassificationRule.AcceptsTab = true;
            this.txtClassificationRule.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtClassificationRule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtClassificationRule.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtClassificationRule.Location = new System.Drawing.Point(0, 0);
            this.txtClassificationRule.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.txtClassificationRule.Multiline = true;
            this.txtClassificationRule.Name = "txtClassificationRule";
            this.txtClassificationRule.Size = new System.Drawing.Size(294, 248);
            this.txtClassificationRule.TabIndex = 4;
            // 
            // rtbClassificationRule
            // 
            this.rtbClassificationRule.AcceptsTab = true;
            this.rtbClassificationRule.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbClassificationRule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbClassificationRule.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbClassificationRule.Location = new System.Drawing.Point(0, 0);
            this.rtbClassificationRule.Margin = new System.Windows.Forms.Padding(0);
            this.rtbClassificationRule.Name = "rtbClassificationRule";
            this.rtbClassificationRule.ReadOnly = true;
            this.rtbClassificationRule.Size = new System.Drawing.Size(294, 94);
            this.rtbClassificationRule.TabIndex = 0;
            this.rtbClassificationRule.Text = "";
            // 
            // tlpOperators
            // 
            this.tlpOperators.BackColor = System.Drawing.SystemColors.Control;
            this.tlpOperators.ColumnCount = 14;
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpOperators.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOperators.Controls.Add(this.lblIN, 9, 0);
            this.tlpOperators.Controls.Add(this.lblComma, 8, 0);
            this.tlpOperators.Controls.Add(this.lblRightBracket, 7, 0);
            this.tlpOperators.Controls.Add(this.lblLeftBracket, 6, 0);
            this.tlpOperators.Controls.Add(this.lblLessOrEqual, 5, 0);
            this.tlpOperators.Controls.Add(this.lblGreaterOrEqual, 4, 0);
            this.tlpOperators.Controls.Add(this.lblLess, 3, 0);
            this.tlpOperators.Controls.Add(this.lblGreater, 2, 0);
            this.tlpOperators.Controls.Add(this.lblNotEqual, 1, 0);
            this.tlpOperators.Controls.Add(this.lblEqual, 0, 0);
            this.tlpOperators.Controls.Add(this.lblNOT, 10, 0);
            this.tlpOperators.Controls.Add(this.lblOR, 11, 0);
            this.tlpOperators.Controls.Add(this.lblAND, 12, 0);
            this.tlpOperators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpOperators.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlpOperators.ForeColor = System.Drawing.Color.Black;
            this.tlpOperators.Location = new System.Drawing.Point(0, 0);
            this.tlpOperators.Margin = new System.Windows.Forms.Padding(0);
            this.tlpOperators.Name = "tlpOperators";
            this.tlpOperators.RowCount = 1;
            this.tlpOperators.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpOperators.Size = new System.Drawing.Size(596, 25);
            this.tlpOperators.TabIndex = 5;
            // 
            // lblIN
            // 
            this.lblIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblIN.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIN.Location = new System.Drawing.Point(195, 0);
            this.lblIN.Margin = new System.Windows.Forms.Padding(0);
            this.lblIN.Name = "lblIN";
            this.lblIN.Padding = new System.Windows.Forms.Padding(1);
            this.lblIN.Size = new System.Drawing.Size(30, 25);
            this.lblIN.TabIndex = 9;
            this.lblIN.Text = "IN";
            this.lblIN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblComma
            // 
            this.lblComma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComma.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblComma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblComma.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComma.Location = new System.Drawing.Point(175, 0);
            this.lblComma.Margin = new System.Windows.Forms.Padding(0);
            this.lblComma.Name = "lblComma";
            this.lblComma.Padding = new System.Windows.Forms.Padding(1);
            this.lblComma.Size = new System.Drawing.Size(20, 25);
            this.lblComma.TabIndex = 8;
            this.lblComma.Text = ",";
            this.lblComma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
           
            // 
            // lblRightBracket
            // 
            this.lblRightBracket.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRightBracket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRightBracket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblRightBracket.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRightBracket.Location = new System.Drawing.Point(155, 0);
            this.lblRightBracket.Margin = new System.Windows.Forms.Padding(0);
            this.lblRightBracket.Name = "lblRightBracket";
            this.lblRightBracket.Padding = new System.Windows.Forms.Padding(1);
            this.lblRightBracket.Size = new System.Drawing.Size(20, 25);
            this.lblRightBracket.TabIndex = 7;
            this.lblRightBracket.Text = ")";
            this.lblRightBracket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblLeftBracket
            // 
            this.lblLeftBracket.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLeftBracket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLeftBracket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLeftBracket.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeftBracket.Location = new System.Drawing.Point(135, 0);
            this.lblLeftBracket.Margin = new System.Windows.Forms.Padding(0);
            this.lblLeftBracket.Name = "lblLeftBracket";
            this.lblLeftBracket.Padding = new System.Windows.Forms.Padding(1);
            this.lblLeftBracket.Size = new System.Drawing.Size(20, 25);
            this.lblLeftBracket.TabIndex = 6;
            this.lblLeftBracket.Text = "(";
            this.lblLeftBracket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblLessOrEqual
            // 
            this.lblLessOrEqual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLessOrEqual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLessOrEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLessOrEqual.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLessOrEqual.Location = new System.Drawing.Point(110, 0);
            this.lblLessOrEqual.Margin = new System.Windows.Forms.Padding(0);
            this.lblLessOrEqual.Name = "lblLessOrEqual";
            this.lblLessOrEqual.Padding = new System.Windows.Forms.Padding(1);
            this.lblLessOrEqual.Size = new System.Drawing.Size(25, 25);
            this.lblLessOrEqual.TabIndex = 5;
            this.lblLessOrEqual.Text = "<=";
            this.lblLessOrEqual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblGreaterOrEqual
            // 
            this.lblGreaterOrEqual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGreaterOrEqual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGreaterOrEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblGreaterOrEqual.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGreaterOrEqual.Location = new System.Drawing.Point(85, 0);
            this.lblGreaterOrEqual.Margin = new System.Windows.Forms.Padding(0);
            this.lblGreaterOrEqual.Name = "lblGreaterOrEqual";
            this.lblGreaterOrEqual.Padding = new System.Windows.Forms.Padding(1);
            this.lblGreaterOrEqual.Size = new System.Drawing.Size(25, 25);
            this.lblGreaterOrEqual.TabIndex = 4;
            this.lblGreaterOrEqual.Text = ">=";
            this.lblGreaterOrEqual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblLess
            // 
            this.lblLess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLess.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLess.Location = new System.Drawing.Point(65, 0);
            this.lblLess.Margin = new System.Windows.Forms.Padding(0);
            this.lblLess.Name = "lblLess";
            this.lblLess.Padding = new System.Windows.Forms.Padding(1);
            this.lblLess.Size = new System.Drawing.Size(20, 25);
            this.lblLess.TabIndex = 3;
            this.lblLess.Text = "<";
            this.lblLess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblGreater
            // 
            this.lblGreater.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGreater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGreater.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblGreater.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGreater.Location = new System.Drawing.Point(45, 0);
            this.lblGreater.Margin = new System.Windows.Forms.Padding(0);
            this.lblGreater.Name = "lblGreater";
            this.lblGreater.Padding = new System.Windows.Forms.Padding(1);
            this.lblGreater.Size = new System.Drawing.Size(20, 25);
            this.lblGreater.TabIndex = 2;
            this.lblGreater.Text = ">";
            this.lblGreater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblNotEqual
            // 
            this.lblNotEqual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNotEqual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNotEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNotEqual.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotEqual.Location = new System.Drawing.Point(20, 0);
            this.lblNotEqual.Margin = new System.Windows.Forms.Padding(0);
            this.lblNotEqual.Name = "lblNotEqual";
            this.lblNotEqual.Padding = new System.Windows.Forms.Padding(1);
            this.lblNotEqual.Size = new System.Drawing.Size(25, 25);
            this.lblNotEqual.TabIndex = 1;
            this.lblNotEqual.Text = "!=";
            this.lblNotEqual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblEqual
            // 
            this.lblEqual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEqual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblEqual.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEqual.Location = new System.Drawing.Point(0, 0);
            this.lblEqual.Margin = new System.Windows.Forms.Padding(0);
            this.lblEqual.Name = "lblEqual";
            this.lblEqual.Padding = new System.Windows.Forms.Padding(1);
            this.lblEqual.Size = new System.Drawing.Size(20, 25);
            this.lblEqual.TabIndex = 0;
            this.lblEqual.Text = "=";
            this.lblEqual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblNOT
            // 
            this.lblNOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNOT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNOT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNOT.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNOT.Location = new System.Drawing.Point(225, 0);
            this.lblNOT.Margin = new System.Windows.Forms.Padding(0);
            this.lblNOT.Name = "lblNOT";
            this.lblNOT.Padding = new System.Windows.Forms.Padding(1);
            this.lblNOT.Size = new System.Drawing.Size(35, 25);
            this.lblNOT.TabIndex = 10;
            this.lblNOT.Text = "NOT";
            this.lblNOT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblOR
            // 
            this.lblOR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblOR.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOR.Location = new System.Drawing.Point(260, 0);
            this.lblOR.Margin = new System.Windows.Forms.Padding(0);
            this.lblOR.Name = "lblOR";
            this.lblOR.Padding = new System.Windows.Forms.Padding(1);
            this.lblOR.Size = new System.Drawing.Size(30, 25);
            this.lblOR.TabIndex = 11;
            this.lblOR.Text = "OR";
            this.lblOR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // lblAND
            // 
            this.lblAND.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAND.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAND.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblAND.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAND.Location = new System.Drawing.Point(290, 0);
            this.lblAND.Margin = new System.Windows.Forms.Padding(0);
            this.lblAND.Name = "lblAND";
            this.lblAND.Padding = new System.Windows.Forms.Padding(1);
            this.lblAND.Size = new System.Drawing.Size(35, 25);
            this.lblAND.TabIndex = 12;
            this.lblAND.Text = "AND";
            this.lblAND.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            
            // 
            // ssMain
            // 
            this.ssMain.BackColor = System.Drawing.SystemColors.Control;
            this.ssMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblInfo});
            this.ssMain.Location = new System.Drawing.Point(0, 274);
            this.ssMain.Name = "ssMain";
            this.ssMain.Size = new System.Drawing.Size(596, 22);
            this.ssMain.TabIndex = 0;
            this.ssMain.Text = "ssMain";
            // 
            // lblInfo
            // 
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(41, 17);
            this.lblInfo.Text = "lblInfo";
            // 
            // ControlClassificationRuleEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Controls.Add(this.pnlMain);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlClassificationRuleEditor";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(600, 300);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.splOperators.Panel1.ResumeLayout(false);
            this.splOperators.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splOperators)).EndInit();
            this.splOperators.ResumeLayout(false);
            this.splColumns.Panel1.ResumeLayout(false);
            this.splColumns.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splColumns)).EndInit();
            this.splColumns.ResumeLayout(false);
            this.splCategories.Panel1.ResumeLayout(false);
            this.splCategories.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splCategories)).EndInit();
            this.splCategories.ResumeLayout(false);
            this.splClassificationRule.Panel1.ResumeLayout(false);
            this.splClassificationRule.Panel1.PerformLayout();
            this.splClassificationRule.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splClassificationRule)).EndInit();
            this.splClassificationRule.ResumeLayout(false);
            this.tlpOperators.ResumeLayout(false);
            this.ssMain.ResumeLayout(false);
            this.ssMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splOperators;
        private System.Windows.Forms.SplitContainer splColumns;
        private System.Windows.Forms.GroupBox grpColumns;
        private System.Windows.Forms.SplitContainer splCategories;
        private System.Windows.Forms.GroupBox grpCategories;
        private System.Windows.Forms.TableLayoutPanel tlpOperators;
        private System.Windows.Forms.Label lblIN;
        private System.Windows.Forms.Label lblComma;
        private System.Windows.Forms.Label lblRightBracket;
        private System.Windows.Forms.Label lblLeftBracket;
        private System.Windows.Forms.Label lblLessOrEqual;
        private System.Windows.Forms.Label lblGreaterOrEqual;
        private System.Windows.Forms.Label lblLess;
        private System.Windows.Forms.Label lblGreater;
        private System.Windows.Forms.Label lblNotEqual;
        private System.Windows.Forms.Label lblEqual;
        private System.Windows.Forms.Label lblNOT;
        private System.Windows.Forms.Label lblOR;
        private System.Windows.Forms.Label lblAND;
        private System.Windows.Forms.StatusStrip ssMain;
        private System.Windows.Forms.ToolStripStatusLabel lblInfo;
        private System.Windows.Forms.SplitContainer splClassificationRule;
        private System.Windows.Forms.TextBox txtClassificationRule;
        private System.Windows.Forms.RichTextBox rtbClassificationRule;
    }

}
