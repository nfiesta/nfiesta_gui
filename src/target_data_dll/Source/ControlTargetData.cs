﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.SDesign;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Modul pro cílová data"</para>
    /// <para lang="en">Control "Module for target data"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class ControlTargetData
            : UserControl, INfiEstaControl, ITargetDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Databázové tabulky</para>
        /// <para lang="en">Database tables</para>
        /// </summary>
        private NfiEstaDB database;

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        private LanguageVersion languageVersion;

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        private LanguageFile languageFile;

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data</para>
        /// <para lang="en">Module for target data setting</para>
        /// </summary>
        private Setting setting;

        /// <summary>
        /// <para lang="cs">Počet záznamů v error-logu</para>
        /// <para lang="en">Number of entries in error-log</para>
        /// </summary>
        private int errorLogEntryNumber;

        private string msgReset = String.Empty;
        private string msgSQLCommandsDisplayEnabled = String.Empty;
        private string msgSQLCommandsDisplayDisabled = String.Empty;
        private string msgNoDatabaseConnection = String.Empty;
        private string msgNoTargetDataDbExtension = String.Empty;
        private string msgNoTargetDataDbSchema = String.Empty;
        private string msgInvalidTargetDataDbVersion = String.Empty;
        private string msgValidTargetDataDbVersion = String.Empty;
        private string msgNotImplementedCategorizationSetup = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba objektů lokálních hustot"</para>
        /// <para lang="en">Control "Selection of local density objects"</para>
        /// </summary>
        private ControlTDSelectionLDsityObjectGroup ctrTDSelectionLDsityObjectGroup;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba objektů lokálních hustot" (read-only)</para>
        /// <para lang="en">Control "Selection of local density objects" (read-only)</para>
        /// </summary>
        internal ControlTDSelectionLDsityObjectGroup CtrTDSelectionLDsityObjectGroup
        {
            get
            {
                return ctrTDSelectionLDsityObjectGroup ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrTDSelectionLDsityObjectGroup)} is null.",
                        paramName: nameof(CtrTDSelectionLDsityObjectGroup));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
        /// <para lang="en">Control "Selection of the target variable"</para>
        /// </summary>
        private ControlTDSelectionTargetVariableCore ctrTDSelectionTargetVariableCore;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné" (read-only)</para>
        /// <para lang="en">Control "Selection of the target variable" (read-only)</para>
        /// </summary>
        internal ControlTDSelectionTargetVariableCore CtrTDSelectionTargetVariableCore
        {
            get
            {
                return ctrTDSelectionTargetVariableCore ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrTDSelectionTargetVariableCore)} is null.",
                        paramName: nameof(CtrTDSelectionTargetVariableCore));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Rozhodnutí o třídění subpopulacemi"</para>
        /// <para lang="en">Control "Decision about classification by subpopulations"</para>
        /// </summary>
        private ControlTDSelectionTargetVariableDivision ctrTDSelectionTargetVariableDivision;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Rozhodnutí o třídění subpopulacemi" (read-only)</para>
        /// <para lang="en">Control "Decision about classification by subpopulations" (read-only)</para>
        /// </summary>
        internal ControlTDSelectionTargetVariableDivision CtrTDSelectionTargetVariableDivision
        {
            get
            {
                return ctrTDSelectionTargetVariableDivision ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrTDSelectionTargetVariableDivision)} is null.",
                        paramName: nameof(CtrTDSelectionTargetVariableDivision));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba třídění lokální hustoty"</para>
        /// <para lang="en">Control "Selection of local density classification"</para>
        /// </summary>
        private ControlTDSelectionAttributeCategory ctrTDSelectionAttributeCategory;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba třídění lokální hustoty" (read-only)</para>
        /// <para lang="en">Control "Selection of local density classification" (read-only)</para>
        /// </summary>
        internal ControlTDSelectionAttributeCategory CtrTDSelectionAttributeCategory
        {
            get
            {
                return ctrTDSelectionAttributeCategory ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrTDSelectionAttributeCategory)} is null.",
                        paramName: nameof(CtrTDSelectionAttributeCategory));
            }
        }


        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba skupiny panelů a roků měření"</para>
        /// <para lang="en">Control "Selection of panels and reference year sets"</para>
        /// </summary>
        private ControlTDSelectionPanelRefYearSetGroup ctrTDSelectionPanelRefYearSetGroup;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba skupiny panelů a roků měření" (read-only)</para>
        /// <para lang="en">Control "Selection of panels and reference year sets" (read-only)</para>
        /// </summary>
        internal ControlTDSelectionPanelRefYearSetGroup CtrTDSelectionPanelRefYearSetGroup
        {
            get
            {
                return ctrTDSelectionPanelRefYearSetGroup ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrTDSelectionPanelRefYearSetGroup)} is null.",
                        paramName: nameof(CtrTDSelectionPanelRefYearSetGroup));
            }
        }

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        public ControlTargetData()
        {
            ErrorLogEntryNumber = 0;

            InitializeComponent();

            Initialize(
                controlOwner: null,
                languageVersion: LanguageVersion.International,
                languageFile: new LanguageFile(),
                setting: new Setting(),
                withEventHandlers: true);
        }

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro cílová data</para>
        /// <para lang="en">Module for target data setting</para>
        /// </param>
        public ControlTargetData(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting)
        {
            ErrorLogEntryNumber = 0;

            InitializeComponent();

            Initialize(
                controlOwner: controlOwner,
                languageVersion: languageVersion,
                languageFile: languageFile,
                setting: setting,
                withEventHandlers: true);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                return controlOwner;
            }
            set
            {
                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return languageVersion;
            }
            set
            {
                languageVersion = value;
                Setting.DBSetting.LanguageVersion = LanguageVersion;
                Database.Postgres.Setting = Setting.DBSetting;

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return languageFile ??
                    new LanguageFile();
            }
            set
            {
                languageFile = value ??
                    new LanguageFile();

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data</para>
        /// <para lang="en">Module for target data setting</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return setting ??
                    new Setting();
            }
            set
            {
                setting = value ??
                    new Setting();
                Database.Postgres.Setting = Setting.DBSetting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Počet záznamů v error-logu</para>
        /// <para lang="en">Number of entries in error-log</para>
        /// </summary>
        internal int ErrorLogEntryNumber
        {
            get
            {
                return errorLogEntryNumber;
            }
            set
            {
                errorLogEntryNumber =
                    (value <= 0)
                    ? 0 :
                    value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                  ? new Dictionary<string, string>()
                    {
                        { nameof(tsmiDatabase),                         "Databáze" },
                        { nameof(tsmiConnect),                          "Připojit" },
                        { nameof(tsmiSDesignExtension),                 "Data extenze $1" },
                        { nameof(tsmiTargetDataExtension),              "Data extenze $1" },
                        { nameof(btnStop),                              "Zastavit výpočet" },
                        { nameof(msgReset),                             "Nastavení modulu pro terénní data bylo resetováno na výchozí hodnoty." },
                        { nameof(msgSQLCommandsDisplayEnabled),         "Povoleno zobrazování SQL příkazů." },
                        { nameof(msgSQLCommandsDisplayDisabled),        "Zakázáno zobrazování SQL příkazů." },
                        { nameof(msgNoDatabaseConnection),              "Aplikace není připojena k databázi." },
                        { nameof(msgNoTargetDataDbExtension),           "V databázi není nainstalovaná extenze $1." },
                        { nameof(msgNoTargetDataDbSchema),              "V databázi neexistuje schéma $1." },
                        { nameof(msgInvalidTargetDataDbVersion),        "Nainstalovaná verze databázové extenze $1 $2 se neshoduje s očekávanou verzí $3." },
                        { nameof(msgValidTargetDataDbVersion),          "Databázová extenze $1 $2 [$3]." },
                        { nameof(msgNotImplementedCategorizationSetup), String.Concat(
                                                                        "Uložená procedura $1 v databázi nfiesta není implementována ",
                                                                        "pro variantu změnová nebo dynamická cílová proměnná se tříděním subpopulacemi.") }
                    }
                  : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlTargetData),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                  ? new Dictionary<string, string>()
                    {
                        { nameof(tsmiDatabase),                          "Database" },
                        { nameof(tsmiConnect),                           "Connect" },
                        { nameof(tsmiSDesignExtension),                  "Data from extension $1" },
                        { nameof(tsmiTargetDataExtension),               "Data from extension $1" },
                        { nameof(btnStop),                               "Stop calculation" },
                        { nameof(msgReset),                              "Field data module settings have been reset to default values." },
                        { nameof(msgSQLCommandsDisplayEnabled),          "Display of SQL statements enabled." },
                        { nameof(msgSQLCommandsDisplayDisabled),         "Display of SQL statements disabled." },
                        { nameof(msgNoDatabaseConnection),               "Application is not connected to database." },
                        { nameof(msgNoTargetDataDbExtension),            "Database extension $1 is not installed." },
                        { nameof(msgNoTargetDataDbSchema),               "Schema $1 does not exist in the database." },
                        { nameof(msgInvalidTargetDataDbVersion),         "Installed version of the database extension $1 $2 does not correspond with expected version $3." },
                        { nameof(msgValidTargetDataDbVersion),           "Database extension $1 $2 [$3]." },
                        { nameof(msgNotImplementedCategorizationSetup),  String.Concat(
                                                                         "The stored procedure $1 is not implemented in the nfiesta database ",
                                                                         "for the variant change or dynamic target variable with classification by subpopulations." ) }
                    }
                  : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlTargetData),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro cílová data</para>
        /// <para lang="en">Module for target data setting</para>
        /// </param>
        /// <param name="withEventHandlers">
        /// <para lang="cs">Inicializace včetně obsluhy událostí tlačítek</para>
        /// <para lang="en">Initialize with button event handlers</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting,
            bool withEventHandlers)
        {
            // Controls
            // Pozn: Při inicializaci objektu je třeba Controls nastavit na null,
            // tj. před vytvořením objektu připojení k databázi,
            // jinak v nich dochází k přístupu k databázi, ke které ještě neexistuje připojení
            ctrTDSelectionLDsityObjectGroup = null;
            ctrTDSelectionTargetVariableCore = null;
            ctrTDSelectionTargetVariableDivision = null;
            ctrTDSelectionAttributeCategory = null;
            ctrTDSelectionPanelRefYearSetGroup = null;
            pnlControls.Controls.Clear();

            // ControlOwner
            ControlOwner = controlOwner;

            // Database
            Disconnect();
            database = new NfiEstaDB();
            database.Postgres.StayConnected = true;

            Database.Postgres.ConnectionError +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.ConnectionErrorHandler(
                    (sender, e) =>
                    {
                        MessageBox.Show(
                            text: e.ToString(),
                            caption: e.MessageBoxCaption,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Error);
                    });

            Database.Postgres.ExceptionReceived +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionReceivedHandler(
                    (sender, e) =>
                    {
                        if (!Setting.DBSuppressError)
                        {
                            MessageBox.Show(
                                text: e.ToString(),
                                caption: e.MessageBoxCaption,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Error);
                        }
                    });

            Database.Postgres.NoticeReceived +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.NoticeReceivedHandler(
                (sender, e) =>
                {
                    if (!Setting.DBSuppressWarning)
                    {
                        MessageBox.Show(
                            text: $"{e.Notice.MessageText}",
                            caption: $"{e.Notice.Severity}",
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                });

            // LanguageVersion
            LanguageVersion = languageVersion;

            // LanguageFile
            LanguageFile = languageFile;

            // Setting
            Setting = setting;

            // StatusStrip
            ssrMain.Visible = Setting.StatusStripVisible;

            // Thread buttons
            Enable();

            if (withEventHandlers)
            {
                tsmiConnect.Click += new EventHandler(
                    (sender, e) => { Connect(); });

                tsmiSDesignExtension.Click += new EventHandler(
                    (sender, e) => { SDesignExtension(); });

                tsmiTargetDataExtension.Click += new EventHandler(
                    (sender, e) => { TargetDataExtension(); });

                btnStop.Click += new EventHandler(
                   (sender, e) =>
                   {
                       ctrTDSelectionPanelRefYearSetGroup?.CtrPanelRefYearSetList?.ControlThread?.Stop();
                   });
            }

#if DEBUG
            tsmiSeparator.Visible = true;
            tsmiSDesignExtension.Visible = true;
            tsmiTargetDataExtension.Visible = true;
#else
            tsmiSeparator.Visible = false;
            tsmiSDesignExtension.Visible = false;
            tsmiTargetDataExtension.Visible = false;
#endif
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing module control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            tsmiDatabase.Text =
                labels.TryGetValue(key: nameof(tsmiDatabase),
                out string tsmiDatabaseText)
                    ? tsmiDatabaseText
                    : String.Empty;

            tsmiConnect.Text =
                labels.TryGetValue(key: nameof(tsmiConnect),
                out string tsmiConnectText)
                    ? tsmiConnectText
                    : String.Empty;

            tsmiSDesignExtension.Text =
                (labels.TryGetValue(key: nameof(tsmiSDesignExtension),
                out string tsmiSDesignExtensionText)
                    ? tsmiSDesignExtensionText
                    : String.Empty)
                .Replace(oldValue: "$1", newValue: SDesignSchema.ExtensionName);

            tsmiTargetDataExtension.Text =
                (labels.TryGetValue(key: nameof(tsmiTargetDataExtension),
                out string tsmiTargetDataExtensionText)
                    ? tsmiTargetDataExtensionText
                    : String.Empty)
                .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName);

            btnStop.Text =
                labels.TryGetValue(key: nameof(btnStop),
                out string btnStopText)
                    ? btnStopText
                    : String.Empty;

            msgReset =
               labels.TryGetValue(key: nameof(msgReset),
                       out msgReset)
                           ? msgReset
                           : String.Empty;

            msgSQLCommandsDisplayEnabled =
              labels.TryGetValue(key: nameof(msgSQLCommandsDisplayEnabled),
                      out msgSQLCommandsDisplayEnabled)
                          ? msgSQLCommandsDisplayEnabled
                          : String.Empty;

            msgSQLCommandsDisplayDisabled =
              labels.TryGetValue(key: nameof(msgSQLCommandsDisplayDisabled),
                      out msgSQLCommandsDisplayDisabled)
                          ? msgSQLCommandsDisplayDisabled
                          : String.Empty;

            msgNoDatabaseConnection =
              labels.TryGetValue(key: nameof(msgNoDatabaseConnection),
                      out msgNoDatabaseConnection)
                          ? msgNoDatabaseConnection
                          : String.Empty;

            msgNoTargetDataDbExtension =
              labels.TryGetValue(key: nameof(msgNoTargetDataDbExtension),
                      out msgNoTargetDataDbExtension)
                          ? msgNoTargetDataDbExtension
                          : String.Empty;

            msgNoTargetDataDbSchema =
              labels.TryGetValue(key: nameof(msgNoTargetDataDbSchema),
                      out msgNoTargetDataDbSchema)
                          ? msgNoTargetDataDbSchema
                          : String.Empty;

            msgInvalidTargetDataDbVersion =
              labels.TryGetValue(key: nameof(msgInvalidTargetDataDbVersion),
                      out msgInvalidTargetDataDbVersion)
                          ? msgInvalidTargetDataDbVersion
                          : String.Empty;

            msgValidTargetDataDbVersion =
              labels.TryGetValue(key: nameof(msgValidTargetDataDbVersion),
                      out msgValidTargetDataDbVersion)
                          ? msgValidTargetDataDbVersion
                          : String.Empty;

            msgNotImplementedCategorizationSetup =
              labels.TryGetValue(key: nameof(msgNotImplementedCategorizationSetup),
                      out msgNotImplementedCategorizationSetup)
                          ? msgNotImplementedCategorizationSetup
                          : String.Empty;

            ctrTDSelectionLDsityObjectGroup?.InitializeLabels();

            ctrTDSelectionTargetVariableCore?.InitializeLabels();

            ctrTDSelectionTargetVariableDivision?.InitializeLabels();

            ctrTDSelectionAttributeCategory?.InitializeLabels();

            ctrTDSelectionPanelRefYearSetGroup?.InitializeLabels();

            DisplayTargetDataExtensionStatus();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Resetuje nastavení modulu pro terénní data na výchozí hodnoty</para>
        /// <para lang="en">Resets field data module settings to default values</para>
        /// </summary>
        /// <param name="displayMessage">
        /// <para lang="cs">Zobrazení zprávy o provedeném resetu nastavení modulu</para>
        /// <para lang="en">Display a message about the reset of the module settings</para>
        /// </param>
        public void Reset(bool displayMessage)
        {
            Setting = new Setting();

            if (displayMessage)
            {
                MessageBox.Show(
                    text: msgReset,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Připojit k databázi</para>
        /// <para lang="en">Connect to a database</para>
        /// </summary>
        private void Connect()
        {
            Initialize(
                controlOwner: ControlOwner,
                languageVersion: LanguageVersion,
                languageFile: LanguageFile,
                setting: Setting,
                withEventHandlers: false);

            Database.Postgres.Setting = Setting.DBSetting;
            Database.Connect(
                applicationName: Setting.DBSetting.ApplicationName);
            Setting.DBSetting = Database.Postgres.Setting;

            pnlControls.Controls.Clear();

            if (DisplayTargetDataExtensionStatus())
            {
                ctrTDSelectionLDsityObjectGroup =
                    new ControlTDSelectionLDsityObjectGroup(controlOwner: this)
                    {
                        Dock = DockStyle.Fill
                    };

                CtrTDSelectionLDsityObjectGroup.NextClick +=
                    new EventHandler(
                        (sender, e) =>
                        { ControlTDSelectionLDsityObjectGroup_ShowNext(); });

                LoadContent();
                CtrTDSelectionLDsityObjectGroup.LoadContent();

                pnlControls.Controls.Add(value: CtrTDSelectionLDsityObjectGroup);
            }
        }

        /// <summary>
        /// <para lang="cs">Odpojit od databáze</para>
        /// <para lang="en">Disconnect from database</para>
        /// </summary>
        public void Disconnect()
        {
            Database?.Postgres?.CloseConnection();
        }

        /// <summary>
        /// <para lang="cs">ThreadSave povolení přístupu k ovládacímu prvku</para>
        /// <para lang="en">ThreadSave enable access to the control</para>
        /// </summary>
        public void Enable()
        {
            if (InvokeRequired)
            {
                BeginInvoke(
                    method: (MethodInvoker)delegate ()
                    {
                        pnlMain.Enabled = true;
                        tlpMain.RowStyles[1].Height = 0;
                    });
            }
            else
            {
                pnlMain.Enabled = true;
                tlpMain.RowStyles[1].Height = 0;
            }
        }

        /// <summary>
        /// <para lang="cs">ThreadSave zakázání přístupu k ovládacímu prvku</para>
        /// <para lang="en">ThreadSave disable access to the control</para>
        /// </summary>
        public void Disable()
        {
            if (InvokeRequired)
            {
                BeginInvoke(
                    method: (MethodInvoker)delegate ()
                    {
                        pnlMain.Enabled = false;
                        tlpMain.RowStyles[1].Height = 40;
                    });
            }
            else
            {
                pnlMain.Enabled = false;
                tlpMain.RowStyles[1].Height = 40;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stav instalace extenze nfiesta_target_data</para>
        /// <para lang="en">Method displays installation status of the database extension nfiesta_target_data</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud v připojené databázi je nainstalovaná extenze nfiesta_target_data
        /// v kompatibilní verzi s aplikací. Jinak false.</para>
        /// <para lang="en">It returns true, when there is installed database extension nfiesta_target_data
        /// in compatible version with application.</para>
        /// </returns>
        private bool DisplayTargetDataExtensionStatus()
        {
            lblStatus.Text = Database.Postgres.ConnectionInfo();

            // Není připojeno k databázi
            // No database connection
            if (!Database.Postgres.Initialized)
            {
                lblTargetDataExtension.Text = String.Empty;
                return false;
            }

            // Extenze target_data neexistuje
            // The target_data extension does not exist
            if (Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName] == null)
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text = msgNoTargetDataDbExtension
                        .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName);
                return false;
            }

            // Extenze existuje
            ExtensionVersion dbExtensionVersion = new(
                version: Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Version);

            // Schéma neexistuje
            if (Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Schema == null)
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text = msgNoTargetDataDbSchema
                        .Replace(oldValue: "$1", newValue: TDSchema.Name);
                return false;
            }

            // Neshoduje se minor verze
            if (!TDSchema.ExtensionVersion.MinorEquals(obj: dbExtensionVersion))
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text = msgInvalidTargetDataDbVersion
                    .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: TDSchema.ExtensionVersion.ToString());
                return false;
            }

            // Neshoduje se patch verze
            if (!TDSchema.ExtensionVersion.Equals(obj: dbExtensionVersion))
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text = msgInvalidTargetDataDbVersion
                    .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: TDSchema.ExtensionVersion.ToString());
                return true;
            }

            // Extenze je OK
            lblTargetDataExtension.ForeColor = System.Drawing.SystemColors.ControlText;
            lblTargetDataExtension.Text = msgValidTargetDataDbVersion
                    .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: TDSchema.ExtensionVersion.ToString())
                    .Replace(oldValue: "$3", newValue: TDSchema.Name);
            return true;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí nebo skryje stavový řádek</para>
        /// <para lang="en">Shows or hides status strip</para>
        /// </summary>
        public void DisplayStatusStrip()
        {
            if (Setting.StatusStripVisible)
            {
                ShowStatusStrip();
            }
            else
            {
                HideStatusStrip();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Show status strip</para>
        /// </summary>
        public void ShowStatusStrip()
        {
            Setting.StatusStripVisible = true;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Hide status strip</para>
        /// </summary>
        public void HideStatusStrip()
        {
            Setting.StatusStripVisible = false;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Povolí nebo zakáže zobrazování SQL příkazů</para>
        /// <para lang="en">Enables or disables the display of SQL statements</para>
        /// </summary>
        public void SwitchDisplaySQLCommands()
        {
            Setting.Verbose =
                !Setting.Verbose;

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: msgSQLCommandsDisplayEnabled,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    text: msgSQLCommandsDisplayDisabled,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_sdesign</para>
        /// <para lang="en">Displays data from extension nfiesta_sdesign"</para>
        /// </summary>
        private void SDesignExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgNoDatabaseConnection,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            List<DBStoredProcedure> storedProcedures =
                [.. Database.Postgres.Catalog
                    .Schemas[SDesignSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)];

            FormSDesign frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_target_data</para>
        /// <para lang="en">Displays data from extension nfiesta_target_data"</para>
        /// </summary>
        private void TargetDataExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgNoDatabaseConnection,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            List<DBStoredProcedure> storedProcedures =
                [.. Database.Postgres.Catalog
                    .Schemas[TDSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)];

            FormTargetData frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba objektů lokálních hustot"</para>
        /// <para lang="en">Displays the control following
        /// the control "Selection of local density objects"</para>
        /// </summary>
        private void ControlTDSelectionLDsityObjectGroup_ShowNext()
        {
            if (ctrTDSelectionLDsityObjectGroup == null)
            {
                return;
            }

            ctrTDSelectionTargetVariableCore =
                new ControlTDSelectionTargetVariableCore(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            CtrTDSelectionTargetVariableCore.PreviousClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlTDSelectionTargetVariableCore_ShowPrevious();
                    });

            CtrTDSelectionTargetVariableCore.NextClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlTDSelectionTargetVariableCore_ShowNext();
                    });

            CtrTDSelectionTargetVariableCore.LoadContent();

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrTDSelectionTargetVariableCore);
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Volba cílové proměnné"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Selection of the target variable"</para>
        /// </summary>
        private void ControlTDSelectionTargetVariableCore_ShowPrevious()
        {
            if (ctrTDSelectionLDsityObjectGroup == null)
            {
                return;
            }

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrTDSelectionLDsityObjectGroup);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacím prvku "Volba cílové proměnné"</para>
        /// <para lang="en">Displays the control following
        /// the control "Selection of the target variable"</para>
        /// </summary>
        private void ControlTDSelectionTargetVariableCore_ShowNext()
        {
            if (ctrTDSelectionTargetVariableCore == null)
            {
                return;
            }

            if (CtrTDSelectionTargetVariableCore.SelectedTargetVariableGroup == null)
            {
                return;
            }

            switch (CtrTDSelectionTargetVariableCore.SelectedTargetVariableGroup.ArealOrPopulationValue)
            {
                case TDArealOrPopulationEnum.AreaDomain:

                    ctrTDSelectionTargetVariableDivision =
                        new ControlTDSelectionTargetVariableDivision(controlOwner: this)
                        {
                            Dock = DockStyle.Fill
                        };

                    CtrTDSelectionTargetVariableDivision.PreviousClick +=
                        new EventHandler(
                            (sender, e) =>
                            {
                                ControlTDSelectionTargetVariableDivision_ShowPrevious();
                            });

                    CtrTDSelectionTargetVariableDivision.NextClick +=
                        new EventHandler(
                            (sender, e) =>
                            {
                                ControlTDSelectionTargetVariableDivision_ShowNext();
                            });

                    CtrTDSelectionTargetVariableDivision.LoadContent();

                    pnlControls.Controls.Clear();
                    pnlControls.Controls.Add(value: CtrTDSelectionTargetVariableDivision);

                    return;

                case TDArealOrPopulationEnum.Population:

                    ctrTDSelectionAttributeCategory =
                        new ControlTDSelectionAttributeCategory(controlOwner: this)
                        {
                            Dock = DockStyle.Fill
                        };

                    CtrTDSelectionAttributeCategory.PreviousClick +=
                        new EventHandler(
                            (sender, e) =>
                            {
                                ControlTDSelectionAttributeCategory_ShowPrevious();
                            });

                    CtrTDSelectionAttributeCategory.NextClick +=
                        new EventHandler(
                            (sender, e) =>
                            {
                                ControlTDSelectionAttributeCategory_ShowNext();
                            });

                    if (ctrTDSelectionAttributeCategory.LDsityObjectsForTargetVariable != null)
                    {
                        pnlControls.Controls.Clear();
                        pnlControls.Controls.Add(value: CtrTDSelectionAttributeCategory);
                    }

                    return;

                default:
                    return;
            }
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Rozhodnutí o třídění subpopulacemi"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Decision about classification by subpopulations"</para>
        /// </summary>
        private void ControlTDSelectionTargetVariableDivision_ShowPrevious()
        {
            if (ctrTDSelectionTargetVariableCore == null)
            {
                return;
            }

            CtrTDSelectionTargetVariableCore.LoadContent();

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrTDSelectionTargetVariableCore);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Rozhodnutí o třídění subpopulacemi"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Decision about classification by subpopulations"</para>
        /// </summary>
        private void ControlTDSelectionTargetVariableDivision_ShowNext()
        {
            if (ctrTDSelectionTargetVariableDivision == null)
            {
                return;
            }

            if ((CtrTDSelectionTargetVariableDivision.SelectedTargetVariable.StateOrChangeValue == TDStateOrChangeEnum.ChangeVariable) &&
                  CtrTDSelectionTargetVariableDivision.ClassifyBySubPopulations)
            {
                MessageBox.Show(
                    text: msgNotImplementedCategorizationSetup
                        .Replace(oldValue: "$1", newValue: TDFunctions.FnGetCategorizationSetup.Name),
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            ctrTDSelectionAttributeCategory =
                new ControlTDSelectionAttributeCategory(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            CtrTDSelectionAttributeCategory.PreviousClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlTDSelectionAttributeCategory_ShowPrevious();
                    });

            CtrTDSelectionAttributeCategory.NextClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlTDSelectionAttributeCategory_ShowNext();
                    });

            if (ctrTDSelectionAttributeCategory.LDsityObjectsForTargetVariable != null)
            {
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrTDSelectionAttributeCategory);
            }
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímí prvku "Volba třídění lokální hustoty"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Selection of local density classification"</para>
        /// </summary>
        private void ControlTDSelectionAttributeCategory_ShowPrevious()
        {
            if (ctrTDSelectionAttributeCategory == null)
            {
                return;
            }

            if (CtrTDSelectionAttributeCategory.SelectedTargetVariableGroup == null)
            {
                return;
            }

            switch (CtrTDSelectionAttributeCategory.SelectedTargetVariableGroup.ArealOrPopulationValue)
            {
                case TDArealOrPopulationEnum.AreaDomain:
                    if (ctrTDSelectionTargetVariableDivision == null)
                    {
                        return;
                    }
                    pnlControls.Controls.Clear();
                    pnlControls.Controls.Add(value: CtrTDSelectionTargetVariableDivision);
                    return;

                case TDArealOrPopulationEnum.Population:
                    if (ctrTDSelectionTargetVariableCore == null)
                    {
                        return;
                    }
                    pnlControls.Controls.Clear();
                    pnlControls.Controls.Add(value: CtrTDSelectionTargetVariableCore);
                    return;

                default:
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba třídění lokální hustoty"
        /// </para>
        /// <para lang="en">
        /// Displays the control following
        /// the control "Selection of local density classification"
        /// </para>
        /// </summary>
        private void ControlTDSelectionAttributeCategory_ShowNext()
        {
            if (ctrTDSelectionAttributeCategory == null)
            {
                return;
            }

            ctrTDSelectionPanelRefYearSetGroup =
                new ControlTDSelectionPanelRefYearSetGroup(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

            CtrTDSelectionPanelRefYearSetGroup.PreviousClick +=
                new EventHandler(
                    (sender, e) =>
                    {
                        ControlTDSelectionPanelRefYearSet_ShowPrevious();
                    });

            CtrTDSelectionPanelRefYearSetGroup.LoadContent();

            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrTDSelectionPanelRefYearSetGroup);

            CtrTDSelectionPanelRefYearSetGroup.InsertPanelRefYearSetGroupIfNoneExists();
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí se zpět na volbu cílové proměnné
        /// </para>
        /// <para lang="en">
        /// Returns back to the target variable selection
        /// </para>
        /// </summary>
        internal void ControlTDSelectionAttributeCategory_GoToTargetVariable()
        {
            ctrTDSelectionTargetVariableCore = null;
            ctrTDSelectionTargetVariableDivision = null;
            ctrTDSelectionAttributeCategory = null;
            ctrTDSelectionPanelRefYearSetGroup = null;

            ControlTDSelectionLDsityObjectGroup_ShowNext();
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Volba skupiny panelů a roků měření"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Selection of panels and reference year sets"</para>
        /// </summary>
        private void ControlTDSelectionPanelRefYearSet_ShowPrevious()
        {
            if (ctrTDSelectionAttributeCategory == null)
            {
                return;
            }
            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: CtrTDSelectionAttributeCategory);
        }

        #endregion Methods

    }

}