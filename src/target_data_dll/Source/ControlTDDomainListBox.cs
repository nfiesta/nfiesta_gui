﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek - seznam plošných domén nebo subpopulací - třída pro Designer</para>
    /// <para lang="en">Control - a list of area domains or subpopulations - class for Designer</para>
    /// </summary>
    internal partial class ControlTDDomainListBoxDesign
        : UserControl
    {
        protected ControlTDDomainListBoxDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Panel PnlHeader => pnlHeader;
        protected System.Windows.Forms.Panel PnlItems => pnlItems;
    }

    /// <summary>
    /// <para lang="cs">Ovládací prvek - seznam plošných domén nebo subpopulací</para>
    /// <para lang="en">Control - a list of area domains or subpopulations</para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class ControlTDDomainListBox<TDomain, TCategory>
            : ControlTDDomainListBoxDesign, INfiEstaControl, ITargetDataControl
             where TDomain : IArealOrPopulationDomain
             where TCategory : IArealOrPopulationCategory
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Předpokládaná šířka ScrollBaru v případě,
        /// že se položky v seznamu nezobrazují všechny</para>
        /// <para lang="en">Estimated width of the ScrollBar
        /// in case items are not all displayed in the list</para>
        /// </summary>
        private const int ScrollBarWidth = 20;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Seznam plošných domén nebo subpopulací
        /// doplněný o vybraný objekt pro třídění pro každý objekt lokální hustoty
        /// a každou plošnou doménu nebo subpopulaci</para>
        /// <para lang="en">List of area domains and subpopulations
        /// with selected object for classification for each local density object
        /// and each area domain or subpopulation</para>
        /// </summary>
        private Dictionary<TDomain,
            Dictionary<
                TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                TDLDsityObject>>
            items;

        /// <summary>
        /// <para lang="cs">Seznam ovládacích prvků</para>
        /// <para lang="en">List of controls</para>
        /// </summary>
        private List<ControlTDDomainListBoxItem<TDomain, TCategory>>
            controls;

        #endregion Private Fields


        #region Controls

        private System.Windows.Forms.Panel pnlSpaceHolder;

        private System.Windows.Forms.Panel pnlDomain;
        private System.Windows.Forms.Label lblDomain;

        private System.Windows.Forms.Panel pnlLDsityObjects;
        private System.Windows.Forms.Label lblLDsityObjects;

        private System.Windows.Forms.Panel pnlLDsityObjectsForADSP;
        private System.Windows.Forms.Label lblLDsityObjectsForADSP;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna výběru objektu lokální hustoty pro třídění v kategorii plošné domény nebo subpopulace"</para>
        /// <para lang="en">Event of the
        /// "Change of the selection of the local density object for classification in area domain or subpopulation category"</para>
        /// </summary>
        public event EventHandler ResultChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTDDomainListBox(Control controlOwner)
            : base()
        {
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionAttributeCategory)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Objekty lokálních hustot pro vybranou cílovou proměnnou (read-only)
        /// (všechny Core + Division)
        /// (setříděno podle klíče)
        /// </para>
        /// <para lang="en">
        /// Local density objects for selected target variable (read-only)
        /// (all Core + Division)
        /// (sorted by key)
        /// </para>
        /// </summary>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            LDsityObjectsForTargetVariable
        {
            get
            {
                if (((ControlTDSelectionAttributeCategory)ControlOwner)
                    .LDsityObjectsForTargetVariable == null)
                {
                    return null;
                }

                return
                    ((ControlTDSelectionAttributeCategory)ControlOwner)
                    .LDsityObjectsForTargetVariable
                    .OrderBy(a => a.Key)
                    .ToDictionary(a => a.Key, a => a.Value);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam plošných domén nebo subpopulací
        /// doplněný o vybraný objekt pro třídění pro každý objekt lokální hustoty
        /// a každou plošnou doménu nebo subpopulaci
        /// (seřazeno podle identifikátoru plošné domény nebo subpopulace) (read-only)</para>
        /// <para lang="en">List of area domains and subpopulations
        /// with selected object for classification for each local density object
        /// and each area domain or subpopulation
        /// (sorted by area domain or subpopulation identifier) (read-only)</para>
        /// </summary>
        public Dictionary<TDomain,
               Dictionary<TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier, TDLDsityObject>>
            Items
        {
            get
            {
                return items
                    .OrderBy(a => a.Key.Id)
                    .ToDictionary(a => a.Key, a => a.Value);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam plošných domén nebo subpopulací
        /// (seřazeno podle identifikátoru plošné domény nebo subpopulace) (read-only)</para>
        /// <para lang="en">List of area domains and subpopulations
        /// (sorted by area domain or subpopulation identifier) (read-only)</para>
        /// </summary>
        public List<TDomain> Domains
        {
            get
            {
                return Items
                    .Select(a => a.Key)
                    .ToList<TDomain>();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný ovládací prvek (read-only)</para>
        /// <para lang="en">Selected control (read-only)</para>
        /// </summary>
        private ControlTDDomainListBoxItem<TDomain, TCategory> SelectedControl
        {
            get
            {
                if (controls == null)
                {
                    return null;
                }

                if (!controls.Where(a => a.Selected).Any())
                {
                    return null;
                }

                return
                    controls.Where(a => a.Selected).First();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná plošná doména nebo subpopulace (read-only)</para>
        /// <para lang="en">Selected area domain or subpopulation (read-only)</para>
        /// </summary>
        public TDomain SelectedItem
        {
            get
            {
                if (SelectedControl == null)
                {
                    return default;
                }

                return
                    SelectedControl.Domain;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National =>
                ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(lblDomain),                  "Plošná doména" },
                            { nameof(lblLDsityObjects),           "Objekt lokální hustoty" },
                            { nameof(lblLDsityObjectsForADSP),    "Objekt lokální hustoty pro třídění" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBox<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictNationalAreaDomain)
                                ? dictNationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(lblDomain),                  "Subpopulace" },
                            { nameof(lblLDsityObjects),           "Objekt lokální hustoty" },
                            { nameof(lblLDsityObjectsForADSP),    "Objekt lokální hustoty pro třídění" }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBox<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictNationalSubPopulation)
                                ? dictNationalSubPopulation
                                : [],

                    _ => [],
                },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(lblDomain),                  "Area domain" },
                            { nameof(lblLDsityObjects),           "Local density object" },
                            { nameof(lblLDsityObjectsForADSP),    "Local density object for classification" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBox<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> dictInternationalAreaDomain)
                                ? dictInternationalAreaDomain
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { nameof(lblDomain),                  "Subpopulation"  },
                            { nameof(lblLDsityObjects),           "Local density object" },
                            { nameof(lblLDsityObjectsForADSP),    "Local density object for classification" }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(ControlTDDomainListBox<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> dictInternationalSubPopulation)
                                ? dictInternationalSubPopulation
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            items = [];

            controls = [];

            InitializeControls();

            InitializeLabels();

            PnlItems.Click += new EventHandler(
                (sender, e) =>
                {
                    foreach (ControlTDDomainListBoxItem<TDomain, TCategory> control in controls)
                    {
                        control.Selected = false;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace obsahu ovládacího prvku</para>
        /// <para lang="en">Initializing content of the control</para>
        /// </summary>
        private void InitializeControls()
        {
            PnlHeader.Controls.Clear();

            // pnlSpaceHolder
            pnlSpaceHolder = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.None,
                Height = PnlHeader.Height,
                Left = 0,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Top = 0,
                Width = 25,
            };
            pnlSpaceHolder.CreateControl();
            PnlHeader.Controls.Add(value: pnlSpaceHolder);

            // pnlDomain
            pnlDomain = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.None,
                Height = PnlHeader.Height,
                Left = pnlSpaceHolder.Width,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Top = 0,
                Width = 200,
            };
            pnlDomain.CreateControl();
            PnlHeader.Controls.Add(value: pnlDomain);

            // pnlLDsityObjects
            pnlLDsityObjects = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.None,
                Height = PnlHeader.Height,
                Left = (pnlSpaceHolder.Width + pnlDomain.Width),
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Top = 0,
                Width = 200
            };
            pnlLDsityObjects.CreateControl();
            PnlHeader.Controls.Add(value: pnlLDsityObjects);

            // pnlLDsityObjectsForADSP
            pnlLDsityObjectsForADSP = new System.Windows.Forms.Panel()
            {
                AutoScroll = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.None,
                Height = PnlHeader.Height,
                Left = (pnlSpaceHolder.Width + pnlDomain.Width + pnlLDsityObjects.Width),
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Tag = null,
                Top = 0,
                Width = PnlHeader.Width - (pnlSpaceHolder.Width + pnlDomain.Width + pnlLDsityObjects.Width)
            };
            pnlLDsityObjectsForADSP.CreateControl();
            PnlHeader.Controls.Add(value: pnlLDsityObjectsForADSP);

            // lblDomain
            lblDomain = new System.Windows.Forms.Label()
            {
                AutoSize = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Text = String.Empty,
                TextAlign = ContentAlignment.TopLeft
            };
            lblDomain.CreateControl();
            pnlDomain.Controls.Add(value: lblDomain);

            // lblLDsityObjects
            lblLDsityObjects = new System.Windows.Forms.Label()
            {
                AutoSize = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Text = String.Empty,
                TextAlign = ContentAlignment.TopLeft
            };
            lblLDsityObjects.CreateControl();
            pnlLDsityObjects.Controls.Add(value: lblLDsityObjects);

            // lblLDsityObjectsForADSP
            lblLDsityObjectsForADSP = new System.Windows.Forms.Label()
            {
                AutoSize = false,
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0),
                Text = String.Empty,
                TextAlign = ContentAlignment.TopLeft
            };
            lblLDsityObjectsForADSP.CreateControl();
            pnlLDsityObjectsForADSP.Controls.Add(value: lblLDsityObjectsForADSP);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing user control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            lblDomain.Text =
                labels.TryGetValue(
                    key: nameof(lblDomain),
                    out string lblDomainText)
                        ? lblDomainText
                        : String.Empty;

            lblLDsityObjects.Text =
                labels.TryGetValue(
                    key: nameof(lblLDsityObjects),
                    out string lblLDsityObjectsText)
                        ? lblLDsityObjectsText
                        : String.Empty;

            lblLDsityObjectsForADSP.Text =
                labels.TryGetValue(key: nameof(lblLDsityObjectsForADSP),
                    out string lblLDsityObjectsForADSPText)
                        ? lblLDsityObjectsForADSPText
                        : String.Empty;

            foreach (ControlTDDomainListBoxItem<TDomain, TCategory> control in controls)
            {
                control.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Přidá plošnou doménu nebo subpopulaci do seznamu</para>
        /// <para lang="en">It adds the area domain or subpopulation to the list</para>
        /// </summary>
        /// <param name="domain">
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </param>
        public void Add(TDomain domain)
        {
            if (items.ContainsKey(key: domain))
            {
                // Plošná doména nebo subpopulace už ve slovníku existuje
                // The area domain or subpopulation already exists in the dictionary
                return;
            }

            // Pokud plošná doména nebo subpopulace ve slovníku neexistuje,
            // pak se do slovníku přidá.
            // If the area domain or subpopulation does not exist in the dictionary,
            // then it is added to the dictionary

            ControlTDDomainListBoxItem<TDomain, TCategory> newControl
                = new(
                        controlOwner: this,
                        index: controls.Count,
                        domain: domain,
                        ldsityObjects: LDsityObjectsForTargetVariableSelect()
                    )
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                    Left = 0,
                    Top = controls.Sum(a => a.Height),
                    Width = PnlItems.Width - ScrollBarWidth
                };

            newControl.ExtendedOrCollapsed += (sender, e) =>
            {
                ReLocate();
            };

            newControl.ResultChanged += (sender, e) =>
            {
                items[((ControlTDDomainListBoxItem<TDomain, TCategory>)sender).Domain] =
                    ((ControlTDDomainListBoxItem<TDomain, TCategory>)sender).DictResult;

                ResultChanged?.Invoke(sender: this, e: new EventArgs());
            };

            newControl.SelectedIndexChanged += (sender, e) =>
            {
                foreach (ControlTDDomainListBoxItem<TDomain, TCategory> c in controls)
                {
                    c.Selected = (c == (ControlTDDomainListBoxItem<TDomain, TCategory>)sender);
                }
            };

            newControl.CreateControl();

            items.Add(
               key: domain,
               value: newControl.DictResult);

            controls.Add(
                item: newControl);

            PnlItems.Controls.Add(
                value: newControl);
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybere objekty lokálních hustot pro zobrazení ve druhém sloupci seznamu
        /// </para>
        /// <para lang="en">
        /// Select local density objects for display in second column of the list
        /// </para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">
        /// Vybere objekty lokálních hustot pro zobrazení ve druhém sloupci seznamu
        /// </para>
        /// <para lang="en">
        /// Select Local density objects for display in second column of the list
        /// </para>
        /// </returns>
        public Dictionary<
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
            TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
            LDsityObjectsForTargetVariableSelect()
        {
            return
                ((ControlTDSelectionAttributeCategory)ControlOwner)
                .LDsityObjectsForTargetVariableSelect(
                    domainType: ArealOrPopulation);
        }

        /// <summary>
        /// <para lang="cs">Vybere plošnou doménu nebo subpopulaci v seznamu</para>
        /// <para lang="en">It selects the area domain or subpopulation in the list</para>
        /// </summary>
        /// <param name="domain">
        /// <para lang="cs">Vybraná plošná doména nebo subpopulace</para>
        /// <para lang="en">Selected area domain or subpopulation</para>
        /// </param>
        public void Select(TDomain domain)
        {
            foreach (ControlTDDomainListBoxItem<TDomain, TCategory> c in controls)
            {
                if (c.Domain.Id == domain.Id)
                {
                    c.Selected = true;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Odebere vybranou plošnou doménu nebo subpopulaci ze seznamu</para>
        /// <para lang="en">It removes the selected area domain or subpopulation from the list</para>
        /// </summary>
        public void Remove()
        {
            if (SelectedItem == null)
            {
                // není nic vybráno, co by se dalo odebrat
                // there is nothing selected that can be removed
                return;
            }

            if (SelectedControl == null)
            {
                // není nic vybráno, co by se dalo odebrat
                // there is nothing selected that can be removed
                return;
            }

            // Je nutné dodržet pořadí odebírání položek ze seznamů!!!
            // Protože po odebrání z controls je pak SelectedControl a SelectedItem null a už se nic neodebere.
            // It is necessary to follow the order of removing items from the lists!!!
            // Because after removing it from the controls,
            // the SelectedControl and SelectedItem are null and nothing is removed.
            PnlItems.Controls.Remove(value: SelectedControl);
            items.Remove(key: SelectedItem);
            controls.Remove(item: SelectedControl);

            ReIndex();
            ReLocate();
        }

        /// <summary>
        /// <para lang="cs">Přenastaví indexy v seznamu,
        /// pokud nějaká položka byla odebrána</para>
        /// <para lang="en">It resets the indexes in the list
        /// if an item has been removed</para>
        /// </summary>
        private void ReIndex()
        {
            int i = -1;
            foreach (ControlTDDomainListBoxItem<TDomain, TCategory> c in controls.OrderBy(a => a.Index))
            {
                c.Index = ++i;
            }
        }

        /// <summary>
        /// <para lang="cs">Přemístí položky v seznamu,
        /// pokud došlo ke změně velikosti některé z položek nebo k odebrání položky v seznamu</para>
        /// <para lang="en">It moves items in the list
        /// if an item has been resized or removed from the list.</para>
        /// </summary>
        private void ReLocate()
        {
            foreach (ControlTDDomainListBoxItem<TDomain, TCategory> c in controls.OrderBy(a => a.Index))
            {
                c.Top = controls.Where(a => a.Index < c.Index).Sum(a => a.Height);
            }
        }

        /// <summary>
        /// <para lang="cs">Vrací identifikátory plošné domény nebo subpopulace
        /// (zachovává se pořadí vložení plošné domény nebo subpopulace do seznamu)
        /// (pokud není žádná plošná doména nebo subpopulace vybraná vrátí null)</para>
        /// <para lang="en">Returns area domain or subpopulation identifiers
        /// (maintains order of insertion area domain or subpopulation into list)
        /// (if area domain or subpopulation is not selected then it returns null)</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací identifikátory plošné domény nebo subpopulace</para>
        /// <para lang="en">Returns area domain or subpopulation identifiers</para>
        /// </returns>
        public List<Nullable<int>> GetDomainIdentifiers()
        {
            if (controls.Count == 0)
            {
                return null;
            }

            // Identifikátory plošné domény nebo subpopulace musí být seřazeny
            // podle pořadí v ovládacím prvku seznamu plošných domén nebo subpopulací (podle Index)
            return
                controls
                    .OrderBy(a => a.Index)
                    .Select(a => (Nullable<int>)a.Domain.Id)
                    .ToList<Nullable<int>>();
        }

        /// <summary>
        /// <para lang="cs">Vrací identifikátory vybraného objektu lokální hustoty pro třídění
        /// pro každou plošnou doménu nebo subpopulaci (první úroveň) a každý objekt lokální hustoty (druhá úroveň)
        /// (zachovává se pořadí vložení plošné domény nebo subpopulace do seznamu)
        /// (pokud není žádná plošná doména nebo subpopulace vybraná vrátí null)</para>
        /// <para lang="en">Returns local density object for adsp identifiers
        /// for each area domain or subpopulation (first level) and each local density object (second level)
        /// (maintains order of insertion area domain or subpopulation into list)
        /// (if area domain or subpopulation is not selected then it returns null)</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací identifikátory vybraného objektu lokální hustoty pro třídění</para>
        /// <para lang="en">>Returns local density object for adsp identifiers</para>
        /// </returns>
        public List<List<Nullable<int>>> GetSelectedLDsityObjectForADSPIdentifiers()
        {
            if (controls.Count == 0)
            {
                return null;
            }

            if (LDsityObjectsForTargetVariable == null)
            {
                return null;
            }

            List<List<Nullable<int>>> result = [];

            // Prvky ve vnějším poli musí být seřazeny
            // podle identifikátoru lokální hustoty a use negative (podle klíče slovníku)
            foreach (TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier key in
                LDsityObjectsForTargetVariable
                .OrderBy(a => a.Key)
                .ToDictionary(a => a.Key, a => a.Value)
                .Keys)
            {
                List<Nullable<int>> inner = [];

                // Prvky vnitřním poli musí být seřazeny
                // podle pořadí v ovládacím prvku seznamu plošných domén nebo subpopulací
                foreach (ControlTDDomainListBoxItem<TDomain, TCategory> control in
                        controls.OrderBy(a => a.Index))
                {
                    if (control.DictResult.TryGetValue(key: key, out TDLDsityObject ldsityObject))
                    {
                        inner.Add(item:
                            (ldsityObject == null) ?
                                (Nullable<int>)null :
                                (Nullable<int>)ldsityObject.Id);
                    }
                    else
                    {
                        inner.Add(item: null);
                    }
                }

                result.Add(item: inner);
            }

            return result;
        }

        /// <summary>
        /// <para lang="cs">Přenačte hodnoty domain po změně v databázi</para>
        /// <para lang="en">Refresh domain values after change in database</para>
        /// </summary>
        public void RefreshDomains()
        {
            for (int i = 0; i < controls.Count; i++)
            {
                ControlTDDomainListBoxItem<TDomain, TCategory> control = controls[i];

                switch (ArealOrPopulation)
                {
                    case TDArealOrPopulationEnum.AreaDomain:
                        control.Domain = Database.STargetData.CAreaDomain.Items
                            .Where(a => a.Id == control.Domain.Id)
                            .Select(a => (TDomain)Convert.ChangeType(
                                value: a,
                                conversionType: typeof(TDomain)))
                            .FirstOrDefault();
                        break;

                    case TDArealOrPopulationEnum.Population:
                        control.Domain = Database.STargetData.CSubPopulation.Items
                            .Where(a => a.Id == control.Domain.Id)
                            .Select(a => (TDomain)Convert.ChangeType(
                                value: a,
                                conversionType: typeof(TDomain)))
                            .FirstOrDefault();
                        break;

                    default:
                        break;
                }
            }
        }

        #endregion Methods

    }

}