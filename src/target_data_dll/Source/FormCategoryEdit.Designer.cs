﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class FormCategoryEditDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            grpCategory = new System.Windows.Forms.GroupBox();
            tlpCategory = new System.Windows.Forms.TableLayoutPanel();
            lblDescriptionEnCaption = new System.Windows.Forms.Label();
            lblLabelEnCaption = new System.Windows.Forms.Label();
            lblDescriptionCsCaption = new System.Windows.Forms.Label();
            lblLabelCsCaption = new System.Windows.Forms.Label();
            txtDescriptionEnValue = new System.Windows.Forms.TextBox();
            txtLabelEnValue = new System.Windows.Forms.TextBox();
            txtDescriptionCsValue = new System.Windows.Forms.TextBox();
            txtLabelCsValue = new System.Windows.Forms.TextBox();
            lblIDValue = new System.Windows.Forms.Label();
            lblIDCaption = new System.Windows.Forms.Label();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            grpCategory.SuspendLayout();
            tlpCategory.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(944, 501);
            pnlMain.TabIndex = 5;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(grpCategory, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.Padding = new System.Windows.Forms.Padding(3);
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(3, 458);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(938, 40);
            tlpButtons.TabIndex = 7;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(778, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(618, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // grpCategory
            // 
            grpCategory.Controls.Add(tlpCategory);
            grpCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCategory.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpCategory.ForeColor = System.Drawing.Color.MediumBlue;
            grpCategory.Location = new System.Drawing.Point(3, 3);
            grpCategory.Margin = new System.Windows.Forms.Padding(0);
            grpCategory.Name = "grpCategory";
            grpCategory.Padding = new System.Windows.Forms.Padding(5);
            grpCategory.Size = new System.Drawing.Size(938, 455);
            grpCategory.TabIndex = 6;
            grpCategory.TabStop = false;
            grpCategory.Text = "grpCategory";
            // 
            // tlpCategory
            // 
            tlpCategory.ColumnCount = 2;
            tlpCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCategory.Controls.Add(lblDescriptionEnCaption, 0, 6);
            tlpCategory.Controls.Add(lblLabelEnCaption, 0, 5);
            tlpCategory.Controls.Add(lblDescriptionCsCaption, 0, 3);
            tlpCategory.Controls.Add(lblLabelCsCaption, 0, 2);
            tlpCategory.Controls.Add(txtDescriptionEnValue, 1, 6);
            tlpCategory.Controls.Add(txtLabelEnValue, 1, 5);
            tlpCategory.Controls.Add(txtDescriptionCsValue, 1, 3);
            tlpCategory.Controls.Add(txtLabelCsValue, 1, 2);
            tlpCategory.Controls.Add(lblIDValue, 1, 0);
            tlpCategory.Controls.Add(lblIDCaption, 0, 0);
            tlpCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpCategory.ForeColor = System.Drawing.Color.Black;
            tlpCategory.Location = new System.Drawing.Point(5, 25);
            tlpCategory.Margin = new System.Windows.Forms.Padding(0);
            tlpCategory.Name = "tlpCategory";
            tlpCategory.RowCount = 7;
            tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpCategory.Size = new System.Drawing.Size(928, 425);
            tlpCategory.TabIndex = 2;
            // 
            // lblDescriptionEnCaption
            // 
            lblDescriptionEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionEnCaption.Location = new System.Drawing.Point(0, 272);
            lblDescriptionEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEnCaption.Name = "lblDescriptionEnCaption";
            lblDescriptionEnCaption.Size = new System.Drawing.Size(150, 153);
            lblDescriptionEnCaption.TabIndex = 3;
            lblDescriptionEnCaption.Text = "lblDescriptionEnCaption";
            // 
            // lblLabelEnCaption
            // 
            lblLabelEnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelEnCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelEnCaption.Location = new System.Drawing.Point(0, 242);
            lblLabelEnCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEnCaption.Name = "lblLabelEnCaption";
            lblLabelEnCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelEnCaption.TabIndex = 2;
            lblLabelEnCaption.Text = "lblLabelEnCaption";
            lblLabelEnCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCsCaption
            // 
            lblDescriptionCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDescriptionCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCsCaption.Location = new System.Drawing.Point(0, 75);
            lblDescriptionCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCsCaption.Name = "lblDescriptionCsCaption";
            lblDescriptionCsCaption.Size = new System.Drawing.Size(150, 152);
            lblDescriptionCsCaption.TabIndex = 1;
            lblDescriptionCsCaption.Text = "lblDescriptionCsCaption";
            // 
            // lblLabelCsCaption
            // 
            lblLabelCsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCsCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLabelCsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCsCaption.Location = new System.Drawing.Point(0, 45);
            lblLabelCsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCsCaption.Name = "lblLabelCsCaption";
            lblLabelCsCaption.Size = new System.Drawing.Size(150, 30);
            lblLabelCsCaption.TabIndex = 0;
            lblLabelCsCaption.Text = "lblLabelCsCaption";
            lblLabelCsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescriptionEnValue
            // 
            txtDescriptionEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEnValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtDescriptionEnValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionEnValue.Location = new System.Drawing.Point(150, 272);
            txtDescriptionEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEnValue.Multiline = true;
            txtDescriptionEnValue.Name = "txtDescriptionEnValue";
            txtDescriptionEnValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionEnValue.Size = new System.Drawing.Size(778, 153);
            txtDescriptionEnValue.TabIndex = 7;
            // 
            // txtLabelEnValue
            // 
            txtLabelEnValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelEnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelEnValue.ForeColor = System.Drawing.Color.Black;
            txtLabelEnValue.Location = new System.Drawing.Point(150, 242);
            txtLabelEnValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEnValue.Name = "txtLabelEnValue";
            txtLabelEnValue.Size = new System.Drawing.Size(778, 16);
            txtLabelEnValue.TabIndex = 6;
            // 
            // txtDescriptionCsValue
            // 
            txtDescriptionCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDescriptionCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionCsValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtDescriptionCsValue.ForeColor = System.Drawing.Color.Black;
            txtDescriptionCsValue.Location = new System.Drawing.Point(150, 75);
            txtDescriptionCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionCsValue.Multiline = true;
            txtDescriptionCsValue.Name = "txtDescriptionCsValue";
            txtDescriptionCsValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtDescriptionCsValue.Size = new System.Drawing.Size(778, 152);
            txtDescriptionCsValue.TabIndex = 5;
            // 
            // txtLabelCsValue
            // 
            txtLabelCsValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLabelCsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelCsValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtLabelCsValue.ForeColor = System.Drawing.Color.Black;
            txtLabelCsValue.Location = new System.Drawing.Point(150, 45);
            txtLabelCsValue.Margin = new System.Windows.Forms.Padding(0);
            txtLabelCsValue.Name = "txtLabelCsValue";
            txtLabelCsValue.Size = new System.Drawing.Size(778, 16);
            txtLabelCsValue.TabIndex = 4;
            // 
            // lblIDValue
            // 
            lblIDValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIDValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblIDValue.ForeColor = System.Drawing.Color.Black;
            lblIDValue.Location = new System.Drawing.Point(150, 0);
            lblIDValue.Margin = new System.Windows.Forms.Padding(0);
            lblIDValue.Name = "lblIDValue";
            lblIDValue.Size = new System.Drawing.Size(778, 30);
            lblIDValue.TabIndex = 9;
            lblIDValue.Text = "lblIDValue";
            lblIDValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblIDCaption
            // 
            lblIDCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIDCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblIDCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblIDCaption.Location = new System.Drawing.Point(0, 0);
            lblIDCaption.Margin = new System.Windows.Forms.Padding(0);
            lblIDCaption.Name = "lblIDCaption";
            lblIDCaption.Size = new System.Drawing.Size(150, 30);
            lblIDCaption.TabIndex = 8;
            lblIDCaption.Text = "lblIDCaption";
            lblIDCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormCategoryEditDesign
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(pnlMain);
            Font = new System.Drawing.Font("Segoe UI", 9F);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormCategoryEditDesign";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormCategoryEdit";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            grpCategory.ResumeLayout(false);
            tlpCategory.ResumeLayout(false);
            tlpCategory.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpCategory;
        private System.Windows.Forms.TableLayoutPanel tlpCategory;
        private System.Windows.Forms.Label lblDescriptionEnCaption;
        private System.Windows.Forms.Label lblLabelEnCaption;
        private System.Windows.Forms.Label lblDescriptionCsCaption;
        private System.Windows.Forms.Label lblLabelCsCaption;
        private System.Windows.Forms.TextBox txtDescriptionEnValue;
        private System.Windows.Forms.TextBox txtLabelEnValue;
        private System.Windows.Forms.TextBox txtDescriptionCsValue;
        private System.Windows.Forms.TextBox txtLabelCsValue;
        private System.Windows.Forms.Label lblIDValue;
        private System.Windows.Forms.Label lblIDCaption;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
    }

}