﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">Formulář pro definici nové cílové proměnné (typ Core)</para>
    /// <para lang="en">Form for defining a new target variable (type Core)</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormTargetVariableCoreNew
            : Form, INfiEstaControl, ITargetDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        private string msgLabelCsIsEmpty = String.Empty;
        private string msgLabelCsExists = String.Empty;
        private string msgDescriptionCsIsEmpty = String.Empty;
        private string msgDescriptionCsExists = String.Empty;
        private string msgLabelEnIsEmpty = String.Empty;
        private string msgLabelEnExists = String.Empty;
        private string msgDescriptionEnIsEmpty = String.Empty;
        private string msgDescriptionEnExists = String.Empty;
        private string msgStateOrChangeUnknown = String.Empty;
        private string msgPositiveLDsitiesIsEmpty = String.Empty;
        private string msgPositiveLDsitiesObjectTypesIsEmpty = String.Empty;
        private string msgPositiveLDsityVersionIsEmpty = String.Empty;
        private string msgNegativeLDsitiesIsEmpty = String.Empty;
        private string msgNegativeLDsitiesObjectTypesIsEmpty = String.Empty;
        private string msgNegativeLDsityVersionIsEmpty = String.Empty;
        private string msgNoCommonUnit = String.Empty;
        private string msgNoCommonPositiveVersion = String.Empty;
        private string msgNoCommonNegativeVersion = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="useCopy">
        /// <para lang="cs">Použít kopii vybrané cílové proměnné</para>
        /// <para lang="en">Use copy of selected target variable</para>
        /// </param>
        public FormTargetVariableCoreNew(
            Control controlOwner,
            bool useCopy)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                useCopy: useCopy);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionTargetVariableCore)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionTargetVariableCore)}."),
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionTargetVariableCore)
                {
                    throw new ArgumentException(
                        message: String.Concat(
                            $"Argument {nameof(ControlOwner)} must be type of ",
                            $"{nameof(ControlTDSelectionTargetVariableCore)}."),
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vybraná skupina objektů lokálních hustot (vybraná na prvním formuláři) (read-only)</para>
        /// <para lang="en">Selected group of local density objects (selected on the first form) (read-only)</para>
        /// </summary>
        private TDLDsityObjectGroup SelectedLocalDensityObjectGroup
        {
            get
            {
                return ((ControlTDSelectionTargetVariableCore)ControlOwner).SelectedLocalDensityObjectGroup;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná cílová proměnná - první ve skupině (read-only)</para>
        /// <para lang="en">Selected target variable - first in the group (read-only)</para>
        /// </summary>
        private TDTargetVariable SelectedTargetVariable
        {
            get
            {
                return ((ControlTDSelectionTargetVariableCore)ControlOwner).SelectedTargetVariable;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané příspěvky lokálních hustot pro vybranou cílovou proměnnou (read-only)</para>
        /// <para lang="en">Selected local density contributions for the selected target variable (read-only)</para>
        /// </summary>
        private TDLDsityList SelectedLDsities
        {
            get
            {
                return ((ControlTDSelectionTargetVariableCore)ControlOwner).SelectedLDsities;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný typ pro novou cílovou proměnnou (read-only)</para>
        /// <para lang="en">Selected target variable type for new target variable(read-only)</para>
        /// </summary>
        private TDStateOrChangeEnum StateOrChange
        {
            get
            {
                foreach (RadioButton radioButton in pnlStateOrChange.Controls.OfType<RadioButton>())
                {
                    if (radioButton.Checked)
                    {
                        TDStateOrChange stateOrChange = (TDStateOrChange)radioButton.Tag;
                        return stateOrChange.Id switch
                        {
                            (int)TDStateOrChangeEnum.StateVariable =>
                                TDStateOrChangeEnum.StateVariable,

                            (int)TDStateOrChangeEnum.ChangeVariable =>
                                TDStateOrChangeEnum.ChangeVariable,

                            (int)TDStateOrChangeEnum.DynamicVariable =>
                                TDStateOrChangeEnum.DynamicVariable,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(StateOrChange)} must be ",
                                        $"{nameof(StateOrChange.StateVariable)}, ",
                                        $"{nameof(StateOrChange.ChangeVariable)} or ",
                                        $"{nameof(StateOrChange.DynamicVariable)}."),
                                    paramName: nameof(StateOrChange)),
                        };
                    }
                }

                throw new ArgumentException(
                    message: String.Concat(
                        $"Argument {nameof(StateOrChange)} must be ",
                        $"{nameof(StateOrChange.StateVariable)}, ",
                        $"{nameof(StateOrChange.ChangeVariable)} or ",
                        $"{nameof(StateOrChange.DynamicVariable)}."),
                    paramName: nameof(StateOrChange));
            }
        }


        /// <summary>
        /// <para lang="cs">Seznam vybraných kladných příspěvků lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en">List of selected positive local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<TDLDsity> PositiveLDsityContributions
        {
            get
            {
                List<TDLDsity> result = [];

                foreach (ControlTDLDsityObject control in pnlLDsityObjectsPositive.Controls)
                {
                    result.AddRange(control.SelectedLDsities);
                }

                return result
                    .Where(a => a != null)
                    .ToList<TDLDsity>();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných záporných příspěvků lokálních hustot
        /// pro novou cílovou proměnnou (read-only)</para>
        /// <para lang="en">List of selected negative local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<TDLDsity> NegativeLDsityContributions
        {
            get
            {
                List<TDLDsity> result = [];

                foreach (ControlTDLDsityObject control in pnlLDsityObjectsNegative.Controls)
                {
                    result.AddRange(control.SelectedLDsities);
                }

                return result
                    .Where(a => a != null)
                    .ToList<TDLDsity>();
            }
        }


        /// <summary>
        /// <para lang="cs">Seznam identifikátorů vybraných kladných příspěvků lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en">List of identifiers of selected positive local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> PositiveLDsityContributionIdentifiers
        {
            get
            {
                return PositiveLDsityContributions
                    .Select(a => (Nullable<int>)a.Id)
                    .ToList<Nullable<int>>();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam identifikátorů vybraných záporných příspěvků lokálních hustot
        /// pro novou cílovou proměnnou (read-only)</para>
        /// <para lang="en">List of identifiers of selected negative local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> NegativeLDsityContributionIdentifiers
        {
            get
            {
                return NegativeLDsityContributions
                        .Select(a => (Nullable<int>)a.Id)
                        .ToList<Nullable<int>>();
            }
        }


        /// <summary>
        /// <para lang="cs">Seznam identifikátorů typů vybraných kladných příspěvků lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en">List of identifiers of types of selected positive local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> PositiveLDsityObjectTypeIdentifiers
        {
            get
            {
                // Funkce fn_get_ldsity4object nevrací hodnotu ldsity_object_type_id,
                // sloupeček v tabulce je vždy null
                // Typ příspěvku lokální hustoty je v tomto případě vždy 100 - core

                // Function fn_get_ldsity4object does not return value of ldsity_object_type_id,
                // this column in database table is always null
                // Type of local density contribution is in this case always of  100 - core

                return PositiveLDsityContributions
                    .Select(a => (Nullable<int>)TDLDsityObjectTypeEnum.Core)
                    .ToList<Nullable<int>>();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam identifikátorů typů vybraných záporných příspěvků lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en">List of identifiers of types of selected negative local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> NegativeLDsityObjectTypeIdentifiers
        {
            get
            {
                // Funkce fn_get_ldsity4object nevrací hodnotu ldsity_object_type_id,
                // sloupeček v tabulce je vždy null
                // Typ příspěvku lokální hustoty je v tomto případě vždy 100 - core

                // Function fn_get_ldsity4object does not return value of ldsity_object_type_id,
                // this column in database table is always null
                // Type of local density contribution is in this case always of  100 - core

                return NegativeLDsityContributions
                    .Select(a => (Nullable<int>)TDLDsityObjectTypeEnum.Core)
                    .ToList<Nullable<int>>();
            }
        }


        /// <summary>
        /// <para lang="cs">Seznam identifikačních čísel verzí pro kladné příspěvky lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en"> List of version identifiers for positive local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> PositiveLDsityObjectVersionIdentifiers
        {
            get
            {
                if (cboVersionPositive.SelectedItem != null)
                {
                    TDVersion version = (TDVersion)cboVersionPositive.SelectedItem;
                    return PositiveLDsityContributions
                        .Select(a => (Nullable<int>)version.Id)
                        .ToList<Nullable<int>>();
                }
                else
                {
                    return [];
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam identifikačních čísel verzí pro kladné příspěvky lokálních hustot
        /// pro novou cílovou proměnou (read-only)</para>
        /// <para lang="en"> List of version identifiers for positive local density contributions
        /// for new target variable (read-only)</para>
        /// </summary>
        private List<Nullable<int>> NegativeLDsityObjectVersionIdentifiers
        {
            get
            {
                if (cboVersionNegative.SelectedItem != null)
                {
                    TDVersion version = (TDVersion)cboVersionNegative.SelectedItem;
                    return NegativeLDsityContributions
                        .Select(a => (Nullable<int>)version.Id)
                        .ToList<Nullable<int>>();
                }
                else
                {
                    return [];
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTargetVariableCoreNew),              "Nová cílová proměnná typu Core" },
                        { nameof(btnCancel),                              "Zrušit" },
                        { nameof(btnOK),                                  "Zapsat do databáze" },
                        { nameof(cboUnitOfMeasure),                       "LabelCs" },
                        { nameof(cboVersionPositive),                     "LabelCs" },
                        { nameof(cboVersionNegative),                     "LabelCs" },
                        { nameof(grpTargetVariable),                      "Cílová proměnná:" },
                        { nameof(grpLDsityObjectsPositive),               "Příspěvky lokálních hustot:" },
                        { nameof(grpLDsityObjectsNegative),               "Záporné příspěvky lokálních hustot:" },
                        { nameof(lblLabelCsCaption),                      "Zkratka (národní):" },
                        { nameof(lblDescriptionCsCaption),                "Popis (národní):" },
                        { nameof(lblLabelEnCaption),                      "Zkratka (anglická):" },
                        { nameof(lblDescriptionEnCaption),                "Popis (anglický):" },
                        { nameof(lblUnitOfMeasure),                       "Jednotka:" },
                        { nameof(lblVersionPositive),                     "Verze příspěvků lokálních hustot:" },
                        { nameof(lblVersionNegative),                     "Verze záporných příspěvků lokálních hustot:" },
                        { nameof(lblStateOrChangeCaption),                "Typ cílové proměnné:" },
                        { nameof(msgLabelCsIsEmpty),                      "Zkratka (národní) musí být vyplněna." },
                        { nameof(msgLabelCsExists),                       "Použitá zkratka (národní) již existuje pro jinou cílovou proměnnou." },
                        { nameof(msgDescriptionCsIsEmpty),                "Popis (národní) musí být vyplněn." },
                        { nameof(msgDescriptionCsExists),                 "Použitý popis (národní) již existuje pro jinou cílovou proměnnou." },
                        { nameof(msgLabelEnIsEmpty),                      "Zkratka (anglická) musí být vyplněna." },
                        { nameof(msgLabelEnExists),                       "Použitá zkratka (anglická) již existuje pro jinou cílovou proměnnou." },
                        { nameof(msgDescriptionEnIsEmpty),                "Popis (anglický) musí být vyplněn." },
                        { nameof(msgDescriptionEnExists),                 "Použitý popis (anglický) již existuje pro jinou cílovou proměnnou." },
                        { nameof(msgStateOrChangeUnknown),                "Neznámý typ cílové proměnné." },
                        { nameof(msgPositiveLDsitiesIsEmpty),             "Nejsou zvoleny žádné kladné příspěvky lokálních hustot."},
                        { nameof(msgPositiveLDsitiesObjectTypesIsEmpty),  "Nejsou zvoleny žádné typy objektů kladných příspěvků lokálních hustot nebo obsahují NULL hodnoty." },
                        { nameof(msgPositiveLDsityVersionIsEmpty),        "Není zvolena žádná verze pro kladné příspěvky lokálních hustot." },
                        { nameof(msgNegativeLDsitiesIsEmpty),             "Nejsou zvoleny žádné záporné příspěvky lokálních hustot."},
                        { nameof(msgNegativeLDsitiesObjectTypesIsEmpty),  "Nejsou zvoleny žádné typy objektů záporných příspěvků lokálních hustot nebo obsahují NULL hodnoty." },
                        { nameof(msgNegativeLDsityVersionIsEmpty),        "Není zvolena žádná verze pro záporné příspěvky lokálních hustot." },
                        { nameof(msgNoCommonUnit),                        "Příspěvky lokálních hustot této cílové proměnné nemají stejnou jednotku!" },
                        { nameof(msgNoCommonPositiveVersion),             "Kladné příspěvky lokálních hustot této cílové proměnné nemají stejnou verzi!" },
                        { nameof(msgNoCommonNegativeVersion),             "Záporné příspěvky lokálních hustot této cílové proměnné nemají stejnou verzi!" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormTargetVariableCoreNew),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTargetVariableCoreNew),              "New target variable type of Core" },
                        { nameof(btnCancel),                              "Cancel" },
                        { nameof(btnOK),                                  "Write to database" },
                        { nameof(cboUnitOfMeasure),                       "LabelEn" },
                        { nameof(cboVersionPositive),                     "LabelEn" },
                        { nameof(cboVersionNegative),                     "LabelEn" },
                        { nameof(grpTargetVariable),                      "Target variable:" },
                        { nameof(grpLDsityObjectsPositive),               "Local density contributions:" },
                        { nameof(grpLDsityObjectsNegative),               "Negative local density contributions:" },
                        { nameof(lblLabelCsCaption),                      "Label (national):" },
                        { nameof(lblDescriptionCsCaption),                "Description (national):" },
                        { nameof(lblLabelEnCaption),                      "Label (English):" },
                        { nameof(lblDescriptionEnCaption),                "Description (English):" },
                        { nameof(lblUnitOfMeasure),                       "Unit:" },
                        { nameof(lblVersionPositive),                     "Version of local density contributions:" },
                        { nameof(lblVersionNegative),                     "Version of negative local density contributons:" },
                        { nameof(lblStateOrChangeCaption),                "Target variable type:" },
                        { nameof(msgLabelCsIsEmpty),                      "Label (national) cannot be empty." },
                        { nameof(msgLabelCsExists),                       "This label (national) already exists for another target variable." },
                        { nameof(msgDescriptionCsIsEmpty),                "Description (national) cannot be empty." },
                        { nameof(msgDescriptionCsExists),                 "This description (national) already exists for another target variable." },
                        { nameof(msgLabelEnIsEmpty),                      "Label (English) cannot be empty." },
                        { nameof(msgLabelEnExists),                       "This label (English) already exists for another target variable." },
                        { nameof(msgDescriptionEnIsEmpty),                "Description (English) cannot be empty." },
                        { nameof(msgDescriptionEnExists),                 "This description (English) already exists for another target variable." },
                        { nameof(msgStateOrChangeUnknown),                "Unknown type of the target variable." },
                        { nameof(msgPositiveLDsitiesIsEmpty),             "There are not selected any positive local density contributions."},
                        { nameof(msgPositiveLDsitiesObjectTypesIsEmpty),  "There are not selected any object types of positive local density contributions or they contain NULL values." },
                        { nameof(msgPositiveLDsityVersionIsEmpty),        "There is not selected any version for positive local density contributions." },
                        { nameof(msgNegativeLDsitiesIsEmpty),             "There are not selected any negative local density contributions."},
                        { nameof(msgNegativeLDsitiesObjectTypesIsEmpty),  "There are not selected any object types of negative local density contributions or they contain NULL values." },
                        { nameof(msgNegativeLDsityVersionIsEmpty),        "There is not selected any version for negative local density contributions." },
                        { nameof(msgNoCommonUnit),                        "The local density contributions of this target variable do not have the same unit!" },
                        { nameof(msgNoCommonPositiveVersion),             "The positive local density contributions of this target variable do not have the same version!" },
                        { nameof(msgNoCommonNegativeVersion),             "The negative local density contributions of this target variable do not have the same version!"  }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormTargetVariableCoreNew),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="useCopy">
        /// <para lang="cs">Použít kopii vybrané cílové proměnné</para>
        /// <para lang="en">Use copy of selected target variable</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            bool useCopy)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            InitializeComboBoxUnitOfMeasure();

            InitializeComboBoxVersionPositive();

            InitializeComboBoxVersionNegative();

            InitializeRadioButtonListStateOrChange();

            InitializeLabels();

            SetStyle();

            if ((SelectedTargetVariable != null) && useCopy)
            {
                txtLabelCsValue.Enabled = false;
                txtLabelEnValue.Enabled = false;
                txtDescriptionCsValue.Enabled = false;
                txtDescriptionEnValue.Enabled = false;

                txtDescriptionCsValue.ScrollBars = ScrollBars.None;
                txtDescriptionEnValue.ScrollBars = ScrollBars.None;

                UseCopy();
            }
            else
            {
                txtLabelCsValue.Enabled = true;
                txtLabelEnValue.Enabled = true;
                txtDescriptionCsValue.Enabled = true;
                txtDescriptionEnValue.Enabled = true;

                txtDescriptionCsValue.ScrollBars = ScrollBars.Vertical;
                txtDescriptionEnValue.ScrollBars = ScrollBars.Vertical;

                SelectUnitOfMeasure();
            }

            btnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře </para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: nameof(FormTargetVariableCoreNew),
                    out string frmTargetVariableCoreNewText)
                        ? frmTargetVariableCoreNewText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(
                    key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            btnOK.Text =
                labels.TryGetValue(
                    key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            grpTargetVariable.Text =
                labels.TryGetValue(
                    key: nameof(grpTargetVariable),
                    out string grpTargetVariableText)
                        ? grpTargetVariableText
                        : String.Empty;

            grpLDsityObjectsPositive.Text =
                labels.TryGetValue(
                    key: nameof(grpLDsityObjectsPositive),
                    out string grpLDsityObjectsPositiveText)
                        ? grpLDsityObjectsPositiveText
                        : String.Empty;

            grpLDsityObjectsNegative.Text =
                labels.TryGetValue(
                    key: nameof(grpLDsityObjectsNegative),
                    out string grpLDsityObjectsNegativeText)
                        ? grpLDsityObjectsNegativeText
                        : String.Empty;

            lblLabelCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelCsCaption),
                    out string lblLabelCsCaptionText)
                        ? lblLabelCsCaptionText
                        : String.Empty;

            lblDescriptionCsCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionCsCaption),
                    out string lblDescriptionCsCaptionText)
                        ? lblDescriptionCsCaptionText
                        : String.Empty;

            lblLabelEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblLabelEnCaption),
                    out string lblLabelEnCaptionText)
                        ? lblLabelEnCaptionText
                        : String.Empty;

            lblDescriptionEnCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblDescriptionEnCaption),
                    out string lblDescriptionEnCaptionText)
                        ? lblDescriptionEnCaptionText
                        : String.Empty;

            lblUnitOfMeasure.Text =
                labels.TryGetValue(
                    key: nameof(lblUnitOfMeasure),
                    out string lblUnitOfMeasureText)
                        ? lblUnitOfMeasureText
                        : String.Empty;

            lblVersionPositive.Text =
                labels.TryGetValue(
                    key: nameof(lblVersionPositive),
                    out string lblVersionPositiveText)
                        ? lblVersionPositiveText
                        : String.Empty;

            lblVersionNegative.Text =
                labels.TryGetValue(
                    key: nameof(lblVersionNegative),
                    out string lblVersionNegativeText)
                        ? lblVersionNegativeText
                        : String.Empty;

            lblStateOrChangeCaption.Text =
                labels.TryGetValue(
                    key: nameof(lblStateOrChangeCaption),
                    out string lblStateOrChangeCaptionText)
                        ? lblStateOrChangeCaptionText
                        : String.Empty;

            msgLabelCsIsEmpty =
              labels.TryGetValue(key: nameof(msgLabelCsIsEmpty),
                      out msgLabelCsIsEmpty)
                          ? msgLabelCsIsEmpty
                          : String.Empty;

            msgLabelCsExists =
              labels.TryGetValue(key: nameof(msgLabelCsExists),
                      out msgLabelCsExists)
                          ? msgLabelCsExists
                          : String.Empty;

            msgDescriptionCsIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionCsIsEmpty),
                      out msgDescriptionCsIsEmpty)
                          ? msgDescriptionCsIsEmpty
                          : String.Empty;

            msgDescriptionCsExists =
              labels.TryGetValue(key: nameof(msgDescriptionCsExists),
                      out msgDescriptionCsExists)
                          ? msgDescriptionCsExists
                          : String.Empty;

            msgLabelEnIsEmpty =
              labels.TryGetValue(key: nameof(msgLabelEnIsEmpty),
                      out msgLabelEnIsEmpty)
                          ? msgLabelEnIsEmpty
                          : String.Empty;

            msgLabelEnExists =
              labels.TryGetValue(key: nameof(msgLabelEnExists),
                      out msgLabelEnExists)
                          ? msgLabelEnExists
                          : String.Empty;

            msgDescriptionEnIsEmpty =
              labels.TryGetValue(key: nameof(msgDescriptionEnIsEmpty),
                      out msgDescriptionEnIsEmpty)
                          ? msgDescriptionEnIsEmpty
                          : String.Empty;

            msgDescriptionEnExists =
              labels.TryGetValue(key: nameof(msgDescriptionEnExists),
                      out msgDescriptionEnExists)
                          ? msgDescriptionEnExists
                          : String.Empty;

            msgStateOrChangeUnknown =
              labels.TryGetValue(key: nameof(msgStateOrChangeUnknown),
                      out msgStateOrChangeUnknown)
                          ? msgStateOrChangeUnknown
                          : String.Empty;

            msgPositiveLDsitiesIsEmpty =
              labels.TryGetValue(key: nameof(msgPositiveLDsitiesIsEmpty),
                      out msgPositiveLDsitiesIsEmpty)
                          ? msgPositiveLDsitiesIsEmpty
                          : String.Empty;

            msgPositiveLDsitiesObjectTypesIsEmpty =
              labels.TryGetValue(key: nameof(msgPositiveLDsitiesObjectTypesIsEmpty),
                      out msgPositiveLDsitiesObjectTypesIsEmpty)
                          ? msgPositiveLDsitiesObjectTypesIsEmpty
                          : String.Empty;

            msgPositiveLDsityVersionIsEmpty =
              labels.TryGetValue(key: nameof(msgPositiveLDsityVersionIsEmpty),
                      out msgPositiveLDsityVersionIsEmpty)
                          ? msgPositiveLDsityVersionIsEmpty
                          : String.Empty;

            msgNegativeLDsitiesIsEmpty =
              labels.TryGetValue(key: nameof(msgNegativeLDsitiesIsEmpty),
                      out msgNegativeLDsitiesIsEmpty)
                          ? msgNegativeLDsitiesIsEmpty
                          : String.Empty;

            msgNegativeLDsitiesObjectTypesIsEmpty =
              labels.TryGetValue(key: nameof(msgNegativeLDsitiesObjectTypesIsEmpty),
                      out msgNegativeLDsitiesObjectTypesIsEmpty)
                          ? msgNegativeLDsitiesObjectTypesIsEmpty
                          : String.Empty;

            msgNegativeLDsityVersionIsEmpty =
              labels.TryGetValue(key: nameof(msgNegativeLDsityVersionIsEmpty),
                      out msgNegativeLDsityVersionIsEmpty)
                          ? msgNegativeLDsityVersionIsEmpty
                          : String.Empty;

            msgNoCommonUnit =
              labels.TryGetValue(key: nameof(msgNoCommonUnit),
                      out msgNoCommonUnit)
                          ? msgNoCommonUnit
                          : String.Empty;

            msgNoCommonPositiveVersion =
              labels.TryGetValue(key: nameof(msgNoCommonPositiveVersion),
                      out msgNoCommonPositiveVersion)
                          ? msgNoCommonPositiveVersion
                          : String.Empty;

            msgNoCommonNegativeVersion =
              labels.TryGetValue(key: nameof(msgNoCommonNegativeVersion),
                      out msgNoCommonNegativeVersion)
                          ? msgNoCommonNegativeVersion
                          : String.Empty;

            onEdit = true;

            cboUnitOfMeasure.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboUnitOfMeasure),
                    out string cboUnitOfMeasureDisplayMember)
                        ? cboUnitOfMeasureDisplayMember
                        : ID;

            cboVersionPositive.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboVersionPositive),
                    out string cboVersionPositiveDisplayMember)
                        ? cboVersionPositiveDisplayMember
                        : ID;

            cboVersionNegative.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboVersionNegative),
                    out string cboVersionNegativeDisplayMember)
                        ? cboVersionNegativeDisplayMember
                        : ID;

            onEdit = false;

            foreach (RadioButton radioButton in pnlStateOrChange.Controls.OfType<RadioButton>())
            {
                TDStateOrChange soc = (TDStateOrChange)radioButton.Tag;

                radioButton.Text =
                   (soc == null)
                        ? String.Empty
                        : (LanguageVersion == LanguageVersion.National)
                            ? soc.LabelCs
                            : (LanguageVersion == LanguageVersion.International)
                                ? soc.LabelEn
                                : soc.Id.ToString();
            }

            foreach (ControlTDLDsityObject control in pnlLDsityObjectsPositive.Controls)
            {
                control.InitializeLabels();
            }

            foreach (ControlTDLDsityObject control in pnlLDsityObjectsNegative.Controls)
            {
                control.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            TableLayoutPanelCellPosition pos;

            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            btnOK.Font = Setting.ButtonFont;
            btnOK.ForeColor = Setting.ButtonForeColor;

            btnCancel.Font = Setting.ButtonFont;
            btnCancel.ForeColor = Setting.ButtonForeColor;

            grpTargetVariable.Font = Setting.GroupBoxFont;
            grpTargetVariable.ForeColor = Setting.GroupBoxForeColor;

            grpLDsityObjectsPositive.Font = Setting.GroupBoxFont;
            grpLDsityObjectsPositive.ForeColor = Setting.GroupBoxForeColor;

            grpLDsityObjectsNegative.Font = Setting.GroupBoxFont;
            grpLDsityObjectsNegative.ForeColor = Setting.GroupBoxForeColor;

            lblLabelCsCaption.Font = Setting.LabelFont;
            lblLabelCsCaption.ForeColor = Setting.LabelForeColor;

            lblLabelEnCaption.Font = Setting.LabelFont;
            lblLabelEnCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionCsCaption.Font = Setting.LabelFont;
            lblDescriptionCsCaption.ForeColor = Setting.LabelForeColor;

            lblDescriptionEnCaption.Font = Setting.LabelFont;
            lblDescriptionEnCaption.ForeColor = Setting.LabelForeColor;

            lblUnitOfMeasure.Font = Setting.LabelFont;
            lblUnitOfMeasure.ForeColor = Setting.LabelForeColor;

            lblVersionNegative.Font = Setting.LabelFont;
            lblVersionNegative.ForeColor = Setting.LabelForeColor;

            lblVersionPositive.Font = Setting.LabelFont;
            lblVersionPositive.ForeColor = Setting.LabelForeColor;

            lblStateOrChangeCaption.Font = Setting.LabelFont;
            lblStateOrChangeCaption.ForeColor = Setting.LabelForeColor;

            txtLabelCsValue.Font = Setting.TextBoxFont;
            txtLabelCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtLabelCsValue);
            txtLabelCsValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtLabelCsValue.Height) / 2), right: 0, bottom: 0);

            txtDescriptionCsValue.Font = Setting.TextBoxFont;
            txtDescriptionCsValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtDescriptionCsValue);
            txtDescriptionCsValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtDescriptionCsValue.Height) / 2), right: 0, bottom: 0);

            txtLabelEnValue.Font = Setting.TextBoxFont;
            txtLabelEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtLabelEnValue);
            txtLabelEnValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtLabelEnValue.Height) / 2), right: 0, bottom: 0);

            txtDescriptionEnValue.Font = Setting.TextBoxFont;
            txtDescriptionEnValue.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: txtDescriptionEnValue);
            txtDescriptionEnValue.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - txtDescriptionEnValue.Height) / 2), right: 0, bottom: 0);

            cboUnitOfMeasure.Font = Setting.TextBoxFont;
            cboUnitOfMeasure.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: cboUnitOfMeasure);
            cboUnitOfMeasure.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - cboUnitOfMeasure.Height) / 2), right: 0, bottom: 0);

            cboVersionPositive.Font = Setting.TextBoxFont;
            cboVersionPositive.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: cboVersionPositive);
            cboVersionPositive.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - cboVersionPositive.Height) / 2), right: 0, bottom: 0);

            cboVersionNegative.Font = Setting.TextBoxFont;
            cboVersionNegative.ForeColor = Setting.TextBoxForeColor;
            pos = tlpTargetVariable.GetCellPosition(control: cboVersionNegative);
            cboVersionNegative.Margin = new Padding(
                left: 0, top: (int)((tlpTargetVariable.GetRowHeights()[pos.Row] - cboVersionNegative.Height) / 2), right: 0, bottom: 0);

            foreach (RadioButton radioButton in pnlStateOrChange.Controls.OfType<RadioButton>())
            {
                radioButton.Font = Setting.RadioButtonFont;
                radioButton.ForeColor = Setting.RadioButtonForeColor;
            }
        }

        /// <summary>
        /// <para lang="cs">Použít kopii vybrané cílové proměnné</para>
        /// <para lang="en">Use copy of selected target variable</para>
        /// </summary>
        private void UseCopy()
        {
            onEdit = true;

            #region Nastavení TextBoxů podle hodnot vybrané cílové proměnné
            txtLabelCsValue.Text = SelectedTargetVariable.LabelCs;
            txtLabelEnValue.Text = SelectedTargetVariable.LabelEn;
            txtDescriptionCsValue.Text = SelectedTargetVariable.DescriptionCs;
            txtDescriptionEnValue.Text = SelectedTargetVariable.DescriptionEn;
            #endregion Nastavení TextBoxů podle hodnot vybrané cílové proměnné

            #region Vyběr RadioButon podle StateOrChange vybrané cílové proměnné
            foreach (RadioButton radioButton
                in pnlStateOrChange.Controls.OfType<RadioButton>())
            {
                TDStateOrChange soc = (TDStateOrChange)radioButton.Tag;

                if (soc == null)
                {
                    radioButton.Checked = false;
                }

                if (SelectedTargetVariable.StateOrChangeId == soc.Id)
                {
                    radioButton.Checked = true;
                }
                else
                {
                    radioButton.Checked = false;
                }
            }
            #endregion Vyběr RadioButon podle StateOrChange vybrané cílové proměnné

            onEdit = false;

            SelectStateOrChange();

            if ((SelectedLDsities != null) && SelectedLDsities.Items.Count != 0)
            {
                onEdit = true;

                #region Jednotka vybrané cílové proměnné
                int unitOfMeasureId = SelectedLDsities.Items.First<TDLDsity>().UnitOfMeasureId;

                if (SelectedLDsities.Items.Where(a => a.UnitOfMeasureId != unitOfMeasureId).Any())
                {
                    MessageBox.Show(
                        text: msgNoCommonUnit,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                TDUnitOfMeasure unitOfMeasureObject =
                    cboUnitOfMeasure.Items
                    .OfType<TDUnitOfMeasure>()
                    .Where(a => a.Id == unitOfMeasureId)
                    .FirstOrDefault<TDUnitOfMeasure>();

                if (unitOfMeasureObject != null)
                {
                    cboUnitOfMeasure.SelectedItem = unitOfMeasureObject;
                }
                #endregion Jednotka vybrané cílové proměnné

                #region Verze kladných příspěvků lokálních hustot
                List<TDLDsity> positiveLDsities =
                        SelectedLDsities.Items
                        .Where(a => a.UseNegative == false)
                        .ToList<TDLDsity>();

                int positiveVersionId =
                        positiveLDsities.Count != 0
                            ? positiveLDsities.First<TDLDsity>().VersionId ?? 0
                            : 0;

                if (positiveLDsities.Where(a => (a.VersionId ?? 0) != positiveVersionId).Any())
                {
                    MessageBox.Show(
                        text: msgNoCommonPositiveVersion,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                TDVersion positiveVersionObject =
                    cboVersionPositive.Items
                    .OfType<TDVersion>()
                    .Where(a => a.Id == positiveVersionId)
                    .FirstOrDefault<TDVersion>();

                if (positiveVersionObject != null)
                {
                    cboVersionPositive.SelectedItem = positiveVersionObject;
                }
                #endregion Verze kladných příspěvků lokálních hustot

                #region Verze záporných příspěvků lokálních hustot
                List<TDLDsity> negativeLDsities =
                        SelectedLDsities.Items
                        .Where(a => a.UseNegative == true)
                        .ToList<TDLDsity>();

                int negativeVersionId =
                    negativeLDsities.Count != 0
                        ? negativeLDsities.First<TDLDsity>().VersionId ?? 0
                        : 0;

                if (negativeLDsities.Where(a => (a.VersionId ?? 0) != negativeVersionId).Any())
                {
                    MessageBox.Show(
                        text: msgNoCommonNegativeVersion,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }

                TDVersion negativeVersionObject =
                    cboVersionNegative.Items
                    .OfType<TDVersion>()
                    .Where(a => a.Id == negativeVersionId)
                    .FirstOrDefault<TDVersion>();

                if (negativeVersionObject != null)
                {
                    cboVersionNegative.SelectedItem = negativeVersionObject;
                }
                #endregion Verze záporných příspěvků lokálních hustot

                onEdit = false;
            }

            SelectUnitOfMeasure();

            if ((SelectedLDsities != null) && SelectedLDsities.Items.Count != 0)
            {
                onEdit = true;

                #region Kladné příspěvky lokálních hustot
                List<TDLDsity> positiveLDsities =
                    SelectedLDsities.Items
                    .Where(a => a.UseNegative == false)
                    .ToList<TDLDsity>();

                foreach (ControlTDLDsityObject control in
                    pnlLDsityObjectsPositive.Controls.OfType<ControlTDLDsityObject>())
                {
                    control.SelectedLDsities = positiveLDsities;
                }
                #endregion Kladné příspěvky lokálních hustot

                #region Záporné příspěvky lokálních hustot
                List<TDLDsity> negativeLDsities =
                    SelectedLDsities.Items
                    .Where(a => a.UseNegative == true)
                    .ToList<TDLDsity>();

                foreach (ControlTDLDsityObject control in
                   pnlLDsityObjectsNegative.Controls.OfType<ControlTDLDsityObject>())
                {
                    control.SelectedLDsities = negativeLDsities;
                }
                #endregion Záporné příspěvky lokálních hustot

                onEdit = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ComboBox "Unit of measure"</para>
        /// <para lang="en">Initializing items in ComboBox "Unit of measure"</para>
        /// </summary>
        private void InitializeComboBoxUnitOfMeasure()
        {
            onEdit = true;

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            cboUnitOfMeasure.DataSource =
                Database.STargetData.CUnitOfMeasure.Items;

            cboUnitOfMeasure.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboUnitOfMeasure),
                    out string cboUnitOfMeasureDisplayMember)
                        ? cboUnitOfMeasureDisplayMember
                        : ID;

            if (cboUnitOfMeasure.Items.Count > 0)
            {
                cboUnitOfMeasure.SelectedIndex = 0;
            }

            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ComboBox "Version Positive"</para>
        /// <para lang="en">Initializing items in ComboBox "Version Positive"</para>
        /// </summary>
        private void InitializeComboBoxVersionPositive()
        {
            onEdit = true;

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            cboVersionPositive.DataSource =
                Database.STargetData.CVersion.Items;

            cboVersionPositive.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboVersionPositive),
                    out string cboVersionPositiveDisplayMember)
                        ? cboVersionPositiveDisplayMember
                        : ID;

            if (cboVersionPositive.Items.Count > 0)
            {
                cboVersionPositive.SelectedIndex = 0;
            }

            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v ComboBox "Version Negative"</para>
        /// <para lang="en">Initializing items in ComboBox "Version Negative"</para>
        /// </summary>
        private void InitializeComboBoxVersionNegative()
        {
            onEdit = true;

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            cboVersionNegative.DataSource =
                Database.STargetData.CVersion.Items;

            cboVersionNegative.DisplayMember =
                labels.TryGetValue(
                    key: nameof(cboVersionNegative),
                    out string cboVersionNegativeDisplayMember)
                        ? cboVersionNegativeDisplayMember
                        : ID;

            if (cboVersionNegative.Items.Count > 0)
            {
                cboVersionNegative.SelectedIndex = 0;
            }

            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace položek v RadioButtonList "State or Change"</para>
        /// <para lang="en">Initializing items in RadioButtonList "State or Change"</para>
        /// </summary>
        private void InitializeRadioButtonListStateOrChange()
        {
            onEdit = true;
            pnlStateOrChange.Controls.Clear();
            int i = -1;
            foreach (TDStateOrChange item in Database.STargetData.CStateOrChange.Items)
            {
                RadioButton radioButton = new()
                {
                    Font = Setting.RadioButtonFont,
                    ForeColor = Setting.RadioButtonForeColor,
                    Left = 10,
                    Tag = item,
                    Text =
                        (LanguageVersion == LanguageVersion.National) ? item.LabelCs :
                        (LanguageVersion == LanguageVersion.International) ? item.LabelEn :
                        item.Id.ToString(),
                    Top = 10 + 20 * (++i),
                    Width = pnlStateOrChange.Width - 20
                };
                radioButton.CheckedChanged += RdoListStateOrChange_CheckedChanged;
                radioButton.CreateControl();
                pnlStateOrChange.Controls.Add(value: radioButton);
            }
            if (pnlStateOrChange.Controls.Count > 0)
            {
                ((RadioButton)pnlStateOrChange.Controls[0]).Checked = true;
            }
            onEdit = false;

            SelectStateOrChange();
        }

        /// <summary>
        /// <para lang="cs">Inicializuje ovládací prvky pro výběr kladných příspěvků lokálních hustot</para>
        /// <para lang="en">It initializes controls for selecting positive local density contributions</para>
        /// </summary>
        /// <param name="selectedUnitOfMeasure">
        /// <para lang="cs">Vybraná jednotka příspěvků lokálních hustot</para>
        /// <para lang="en">Selected unit of local density contributions</para>
        /// </param>
        /// <param name="selectedVersion">
        /// <para lang="cs">Vybraná verze</para>
        /// <para lang="en">Selected version</para>
        /// </param>
        private void InitializePositiveLDsityObjectControls(
            TDUnitOfMeasure selectedUnitOfMeasure,
            TDVersion selectedVersion)
        {
            if (selectedUnitOfMeasure == null)
            {
                pnlLDsityObjectsPositive.Controls.Clear();
                return;
            }

            if (selectedVersion == null)
            {
                pnlLDsityObjectsPositive.Controls.Clear();
                return;
            }

            if (SelectedLocalDensityObjectGroup == null)
            {
                pnlLDsityObjectsPositive.Controls.Clear();
                return;
            }

            TDLDsityObjectList ldsityObjectList =
                    TDFunctions.FnGetLDsityObjectForLDsityObjectGroup.Execute(
                        database: Database,
                        ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id);

            pnlLDsityObjectsPositive.Controls.Clear();
            int top = 10;
            onEdit = true;
            foreach (TDLDsityObject ldsityObject in ldsityObjectList.Items)
            {
                TDLDsityList ldsityList = TDFunctions.FnGetLDsityForObject.Execute(
                    database: Database,
                    ldsityObjectId: ldsityObject.Id,
                    unitOfMeasureId: selectedUnitOfMeasure.Id);

                ldsityList = new TDLDsityList(
                    database: Database,
                    rows: ldsityList.Data.AsEnumerable()
                        .Where(a => (a.Field<Nullable<int>>(TDLDsityList.ColVersionId.Name) ?? 0) == selectedVersion.Id));

                ControlTDLDsityObject control = new(
                    controlOwner: this,
                    ldsityObject: ldsityObject,
                    ldsities: ldsityList)
                {
                    EnableEmptySelectedLDsities = false,
                    Left = 10,
                    Top = top,
                    Width = pnlLDsityObjectsPositive.Width - 40
                };
                control.SelectionChanged += PositiveContributionsChanged;
                control.CreateControl();
                pnlLDsityObjectsPositive.Controls.Add(value: control);
                top = control.Top + control.Height + 5;
            }
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializuje ovládací prvky pro výběr záporných příspěvků lokálních hustot</para>
        /// <para lang="en">It initializes controls for selecting negative local density contributions</para>
        /// </summary>
        /// <param name="selectedUnitOfMeasure">
        /// <para lang="cs">Vybraná jednotka příspěvků lokálních hustot</para>
        /// <para lang="en">Selected unit of local density contributions</para>
        /// </param>
        /// <param name="selectedVersion">
        /// <para lang="cs">Vybraná verze</para>
        /// <para lang="en">Selected version</para>
        /// </param>
        private void InitializeNegativeLDsityObjectControls(
            TDUnitOfMeasure selectedUnitOfMeasure,
            TDVersion selectedVersion)
        {
            if (selectedUnitOfMeasure == null)
            {
                pnlLDsityObjectsNegative.Controls.Clear();
                return;
            }

            if (selectedVersion == null)
            {
                pnlLDsityObjectsNegative.Controls.Clear();
                return;
            }

            if (SelectedLocalDensityObjectGroup == null)
            {
                pnlLDsityObjectsNegative.Controls.Clear();
                return;
            }

            TDLDsityObjectList ldsityObjectList =
               TDFunctions.FnGetLDsityObjectForLDsityObjectGroup.Execute(
                    database: Database,
                    ldsityObjectGroupId: SelectedLocalDensityObjectGroup.Id);

            pnlLDsityObjectsNegative.Controls.Clear();
            int top = 10;
            onEdit = true;
            foreach (TDLDsityObject ldsityObject in ldsityObjectList.Items)
            {
                TDLDsityList ldsityList = TDFunctions.FnGetLDsityForObject.Execute(
                    database: Database,
                    ldsityObjectId: ldsityObject.Id,
                    unitOfMeasureId: selectedUnitOfMeasure.Id);

                ldsityList = new TDLDsityList(
                   database: Database,
                   rows: ldsityList.Data.AsEnumerable()
                       .Where(a => (a.Field<Nullable<int>>(TDLDsityList.ColVersionId.Name) ?? 0) == selectedVersion.Id));

                ControlTDLDsityObject control = new(
                    controlOwner: this,
                    ldsityObject: ldsityObject,
                    ldsities: ldsityList)
                {
                    EnableEmptySelectedLDsities = false,
                    Left = 10,
                    Top = top,
                    Width = pnlLDsityObjectsPositive.Width - 40
                };
                control.SelectionChanged += NegativeContributionsChanged;
                control.CreateControl();
                pnlLDsityObjectsNegative.Controls.Add(value: control);
                top = control.Top + control.Height + 5;
            }
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Při změně výběru unit of measure
        /// znovu načte ovládací prvky pro výběr kladných a záporných příspěvků lokálních hustot</para>
        /// <para lang="en">It initializes controls for selecting positive and negative local density contributions
        /// when selection of the unit of measure is changed.</para>
        /// </summary>
        private void SelectUnitOfMeasure()
        {
            if (cboUnitOfMeasure.SelectedItem != null)
            {
                if (cboVersionPositive.SelectedItem != null)
                {
                    InitializePositiveLDsityObjectControls(
                        selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                        selectedVersion: (TDVersion)cboVersionPositive.SelectedItem);
                }
                else
                {
                    InitializePositiveLDsityObjectControls(
                        selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                        selectedVersion: null);
                }

                if (cboVersionNegative.SelectedItem != null)
                {
                    InitializeNegativeLDsityObjectControls(
                        selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                        selectedVersion: (TDVersion)cboVersionNegative.SelectedItem);
                }
                else
                {
                    InitializeNegativeLDsityObjectControls(
                        selectedUnitOfMeasure: (TDUnitOfMeasure)cboUnitOfMeasure.SelectedItem,
                        selectedVersion: null);
                }
            }
            else
            {
                InitializePositiveLDsityObjectControls(
                    selectedUnitOfMeasure: null,
                    selectedVersion: null);

                InitializeNegativeLDsityObjectControls(
                   selectedUnitOfMeasure: null,
                   selectedVersion: null);
            }
        }

        /// <summary>
        /// <para lang="cs">Při změně výběru state target variable nebo change target variable
        /// zobrazí nebo skryje ovládací prvky pro change target variable</para>
        /// <para lang="en">It shows or hides the controls for the change target variable
        /// when the selection of a state target variable or change target variable is changed.</para>
        /// </summary>
        private void SelectStateOrChange()
        {
            switch (StateOrChange)
            {
                case TDStateOrChangeEnum.StateVariable:
                    lblVersionNegative.Visible = false;
                    cboVersionNegative.Visible = false;
                    splPositiveNegative.Visible = true;
                    splPositiveNegative.Panel2Collapsed = true;
                    grpLDsityObjectsNegative.Visible = false;
                    break;

                case TDStateOrChangeEnum.ChangeVariable:
                    lblVersionNegative.Visible = true;
                    cboVersionNegative.Visible = true;
                    splPositiveNegative.Visible = true;
                    splPositiveNegative.Panel2Collapsed = false;
                    grpLDsityObjectsNegative.Visible = true;
                    break;

                case TDStateOrChangeEnum.DynamicVariable:
                    lblVersionNegative.Visible = false;
                    cboVersionNegative.Visible = false;
                    splPositiveNegative.Visible = true;
                    splPositiveNegative.Panel2Collapsed = true;
                    grpLDsityObjectsNegative.Visible = false;
                    break;

                default:
                    lblVersionNegative.Visible = false;
                    cboVersionNegative.Visible = false;
                    splPositiveNegative.Visible = false;
                    splPositiveNegative.Panel2Collapsed = true;
                    grpLDsityObjectsNegative.Visible = false;
                    break;
            }
            PnlLDsityObjectsPositive_Resize(sender: null, e: null);
            PnlLDsityObjectsNegative_Resize(sender: null, e: null);
        }

        /// <summary>
        /// <para lang="cs">Uloží cílovou proměnnou do databázové tabulky</para>
        /// <para lang="en">It saves target variable into database table</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud bylo možné uložit cílovou proměnnou do databázové tabulky, jinak false</para>
        /// <para lang="en">It returns true if it was possible to save the target variable into database table, else false</para>
        /// </returns>
        private bool SaveTargetVariableCore()
        {
            string labelCs = txtLabelCsValue.Text.Trim();
            string descriptionCs = txtDescriptionCsValue.Text.Trim();
            string labelEn = txtLabelEnValue.Text.Trim();
            string descriptionEn = txtDescriptionEnValue.Text.Trim();

            if (String.IsNullOrEmpty(value: labelCs))
            {
                MessageBox.Show(
                    text: msgLabelCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionCs))
            {
                MessageBox.Show(
                    text: msgDescriptionCsIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: labelEn))
            {
                MessageBox.Show(
                    text: msgLabelEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (String.IsNullOrEmpty(value: descriptionEn))
            {
                MessageBox.Show(
                    text: msgDescriptionEnIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (PositiveLDsityContributionIdentifiers.Count == 0)
            {
                // Kladné příspěvky lokálních hustot jsou prázdné
                // Positive contributions of local densities are empty
                MessageBox.Show(
                    text: msgPositiveLDsitiesIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if ((PositiveLDsityObjectTypeIdentifiers.Count == 0) ||
                (PositiveLDsityObjectTypeIdentifiers.Where(a => a == 0).Any()))
            {
                // Typy objektů kladných příspěvků lokálních hustot jsou prázdné nebo obsahují null hodnoty
                // Object types of positive local density contributions are empty or contain null values
                MessageBox.Show(
                    text: msgPositiveLDsitiesObjectTypesIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (PositiveLDsityObjectVersionIdentifiers.Count == 0)
            {
                // Není vybrána verze pro kladné příspěvky lokálních hustot
                // Version is not selected for positive contributions of local densities
                MessageBox.Show(
                    text: msgPositiveLDsityVersionIsEmpty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            switch (StateOrChange)
            {
                case TDStateOrChangeEnum.StateVariable:
                    #region StateVariable
                    TDFunctions.FnSaveTargetVariable.Execute(
                            database: Database,
                            stateOrChange: (int)StateOrChange,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            ldsityIds: PositiveLDsityContributionIdentifiers,
                            ldsityObjectTypeIds: PositiveLDsityObjectTypeIdentifiers,
                            versionIds: PositiveLDsityObjectVersionIdentifiers,
                            targetVariableIds: null);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveTargetVariable.CommandText,
                            caption: TDFunctions.FnSaveTargetVariable.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }

                    return !Database.Postgres.ExceptionFlag;
                #endregion StateVariable

                case TDStateOrChangeEnum.DynamicVariable:
                    #region DynamicVariable
                    List<Nullable<int>> result =
                        TDFunctions.FnSaveTargetVariable.Execute(
                            database: Database,
                            stateOrChange: (int)StateOrChange,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            ldsityIds: PositiveLDsityContributionIdentifiers,
                            ldsityObjectTypeIds: PositiveLDsityObjectTypeIdentifiers,
                            versionIds: PositiveLDsityObjectVersionIdentifiers,
                            targetVariableIds: null);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveTargetVariable.CommandText,
                            caption: TDFunctions.FnSaveTargetVariable.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }

                    return !Database.Postgres.ExceptionFlag;
                #endregion DynamicVariable

                case TDStateOrChangeEnum.ChangeVariable:
                    #region ChangeVariable
                    if (NegativeLDsityContributionIdentifiers.Count == 0)
                    {
                        // Příspěvky záporných lokálních hustot jsou prázdné
                        // Negative contributions of local densities are empty
                        MessageBox.Show(
                            text: msgNegativeLDsitiesIsEmpty,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if ((NegativeLDsityObjectTypeIdentifiers.Count == 0) ||
                        (NegativeLDsityObjectTypeIdentifiers.Where(a => a == 0).Any()))
                    {
                        // Typy objektů záporných lokálních hustot jsou prázdné nebo obsahují null hodnoty
                        // Object types of negative local density contributions are empty or contain null values
                        MessageBox.Show(
                            text: msgNegativeLDsitiesObjectTypesIsEmpty,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    if (NegativeLDsityObjectVersionIdentifiers.Count == 0)
                    {
                        // Není vybrána verze pro záporné příspěvky lokálních hustot
                        // Version is not selected for negative contributions of local densities
                        MessageBox.Show(
                            text: msgNegativeLDsityVersionIsEmpty,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        return false;
                    }

                    TDFunctions.FnSaveChangeTargetVariable.Execute(
                            database: Database,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            ldsityIds: PositiveLDsityContributionIdentifiers,
                            ldsityObjectTypeIds: PositiveLDsityObjectTypeIdentifiers,
                            versionIds: PositiveLDsityObjectVersionIdentifiers,
                            ldsityNegatIds: NegativeLDsityContributionIdentifiers,
                            ldsityObjectTypeNegatIds: NegativeLDsityObjectTypeIdentifiers,
                            versionNegatIds: NegativeLDsityObjectVersionIdentifiers,
                            targetVariableId: null);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveChangeTargetVariable.CommandText,
                            caption: TDFunctions.FnSaveChangeTargetVariable.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }

                    return !Database.Postgres.ExceptionFlag;
                #endregion ChangeVariable

                default:
                    #region Unknown

                    MessageBox.Show(
                        text: msgStateOrChangeUnknown,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return false;
                    #endregion Unknown
            }
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události "Zavření formuláře"</para>
        /// <para lang="en">Handling the "Form closing" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (FormTargetVariableCoreNew)</para>
        /// <para lang="en">Object that sends the event (FormTargetVariableCoreNew)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void FormTargetVariableCoreNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                e.Cancel = !SaveTargetVariableCore();
            }
            else
            {
                e.Cancel = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna výběru kladných příspěvků lokálních hustot"</para>
        /// <para lang="en">Handling the "Selection of the positive local density contributions has been changed" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ControlTDLDsityObject)</para>
        /// <para lang="en">Object that sends the event (ControlTDLDsityObject)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void PositiveContributionsChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna výběru záporných příspěvků lokálních hustot"</para>
        /// <para lang="en">Handling the "Selection of the negative local density contributions has been changed" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ControlTDLDsityObject)</para>
        /// <para lang="en">Object that sends the event (ControlTDLDsityObject)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void NegativeContributionsChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna výběru jednotky"</para>
        /// <para lang="en">Handling the "Change of unit selection" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CboUnitOfMeasure_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
                SelectUnitOfMeasure();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna výběru verze pro pozitivní příspěvky lokálních hustot"</para>
        /// <para lang="en">Handling the "Change of the version selection for positive local density contributions" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CboVersionPositive_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
                SelectUnitOfMeasure();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna výběru verze pro negativní příspěvky lokálních hustot"</para>
        /// <para lang="en">Handling the "Change of the version selection for negative local density contributions" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CboVersionNegative_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
                SelectUnitOfMeasure();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Výběr položky stavová nebo změnová cílová proměnná"</para>
        /// <para lang="en">Handling the "Selecting an item of a state or change target variable" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (RadioButton)</para>
        /// <para lang="en">Object that sends the event (RadioButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RdoListStateOrChange_CheckedChanged(object sender, EventArgs e)
        {
            if (!onEdit)
            {
                SelectStateOrChange();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna velikosti panelu s typem cílové proměnné"</para>
        /// <para lang="en">Handling the "Change of the panel size with target variable type" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Panel)</para>
        /// <para lang="en">Object that sends the event (Panel)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void PnlStateOrChange_Resize(object sender, EventArgs e)
        {
            foreach (RadioButton rdo in pnlStateOrChange.Controls)
            {
                rdo.Width = pnlStateOrChange.Width - 20;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna velikosti panelu s kladnými příspěvky lokálních hustot"
        /// Při změně velikosti panelu s ovládacími prvky pro výběr příspěvků lokálních hustot
        /// zarovná velikost ovládacího prvku do velikosti panelu</para>
        /// <para lang="en">Handling the "Resizing the panel with positive local density contributions" event
        /// When resizing the panel with controls for selection of local density contributions, the control size aligns to the panel size</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Panel)</para>
        /// <para lang="en">Object that sends the event (Panel)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void PnlLDsityObjectsPositive_Resize(object sender, EventArgs e)
        {
            foreach (ControlTDLDsityObject control in pnlLDsityObjectsPositive.Controls)
            {
                control.Width = pnlLDsityObjectsPositive.Width - 40;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Změna velikosti panelu se zápornými příspěvky lokálních hustot"
        /// Při změně velikosti panelu s ovládacími prvky pro výběr příspěvků lokálních hustot
        /// zarovná velikost ovládacího prvku do velikosti panelu</para>
        /// <para lang="en">Handling the "Resizing the panel with negative local density contributions" event
        /// When resizing the panel with controls for selection of local density contributions, the control size aligns to the panel size</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Panel)</para>
        /// <para lang="en">Object that sends the event (Panel)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void PnlLDsityObjectsNegative_Resize(object sender, EventArgs e)
        {
            foreach (ControlTDLDsityObject control in pnlLDsityObjectsNegative.Controls)
            {
                control.Width = pnlLDsityObjectsNegative.Width - 40;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zrušit"</para>
        /// <para lang="en">Handling the "Cancel" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zapsat do databáze"</para>
        /// <para lang="en">Handling the "Write to database" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        #endregion Event Handlers

    }

}