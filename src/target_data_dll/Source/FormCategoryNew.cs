﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleTargetData
{

    /// <summary>
    /// <para lang="cs">
    /// Formulář pro vytvoření nových kategorií plošných domén a subpopulací
    /// a definice jejich klasifikačních pravidel a závislostí - třída pro Designer
    /// </para>
    /// <para lang="en">
    /// Form to create new categories of area domains or subpopulations
    /// and to define theirs classification rules and dependences - class for Designer
    /// </para>
    /// </summary>
    internal partial class FormCategoryNewDesign
        : Form
    {
        protected FormCategoryNewDesign()
        {
            InitializeComponent();
        }
        protected System.Windows.Forms.Button BtnCancel => btnCancel;
        protected System.Windows.Forms.ToolStripButton BtnCategoryAdd => btnCategoryAdd;
        protected System.Windows.Forms.ToolStripButton BtnCategoryDelete => btnCategoryDelete;
        protected System.Windows.Forms.ToolStripButton BtnCategoryEdit => btnCategoryEdit;
        protected System.Windows.Forms.ToolStripButton BtnDependenceDefine => btnDependenceDefine;
        protected System.Windows.Forms.Button BtnOK => btnOK;
        protected System.Windows.Forms.GroupBox GrpCategory => grpCategory;
        protected System.Windows.Forms.GroupBox GrpWorkSpace => grpWorkSpace;
        protected System.Windows.Forms.Label LblCaption => lblCaption;
        protected System.Windows.Forms.Label LblClassificationRuleCaption => lblClassificationRuleCaption;
        protected System.Windows.Forms.Label LblClassificationRuleSwitch => lblClassificationRuleSwitch;
        protected System.Windows.Forms.Label LblDependenceEditor => lblDependenceEditor;
        protected System.Windows.Forms.Panel PnlClassificationRuleCaption => pnlClassificationRuleCaption;
        protected System.Windows.Forms.Panel PnlClassificationRuleSwitch => pnlClassificationRuleSwitch;
        protected System.Windows.Forms.Panel PnlDependenceEditor => pnlDependenceEditor;
        protected System.Windows.Forms.SplitContainer SplClassificationRule => splClassificationRule;
        protected System.Windows.Forms.ToolStrip TsrCategory => tsrCategory;
        protected System.Windows.Forms.TreeView TvwCategory => tvwCategory;
    }

    /// <summary>
    /// <para lang="cs">
    /// Formulář pro vytvoření nových kategorií plošných domén a subpopulací
    /// a definice jejich klasifikačních pravidel a závislostí
    /// </para>
    /// <para lang="en">
    /// Form to create new categories of area domains or subpopulations
    /// and to define theirs classification rules and dependences
    /// </para>
    /// </summary>
    /// <typeparam name="TDomain">
    /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain or subpopulation type</para>
    /// </typeparam>
    /// <typeparam name="TCategory">
    /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
    /// <para lang="en"> Area domain category or subpopulation category type</para>
    /// </typeparam>
    [SupportedOSPlatform("windows")]
    internal class FormCategoryNew<TDomain, TCategory>
            : FormCategoryNewDesign,
            IArealOrPopulationForm<TDomain, TCategory>,
            INfiEstaControl, ITargetDataControl
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </summary>
        private TDomain domain;

        /// <summary>
        /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
        /// <para lang="en">Changes in classification rules are not allowed</para>
        /// </summary>
        private bool readOnlyMode;

        /// <summary>
        /// <para lang="cs">Seznam všech uzlů v TreeView kategorií</para>
        /// <para lang="en">List of all nodes in TreeView of categories</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Uzel předcházející vybranému uzlu</para>
        /// <para lang="en">TreeNode previous selected TreeNode</para>
        /// </summary>
        private TreeNode previousNode;

        /// <summary>
        /// <para lang="cs">Seznam objektů lokálních hustot (obsah číselníku c_ldsity_object)</para>
        /// <para lang="en">List of local density objects (content of the lookup table c_ldsity_object)</para>
        /// </summary>
        private TDLDsityObjectList cLDsityObjects;

        /// <summary>
        /// <para lang="cs">Skupina klasifikačních pravidel pro jednu doménu, načtená z json souboru</para>
        /// <para lang="en">Collection of classification rules for one domain, loaded from json file</para>
        /// </summary>
        private ClassificationRuleCollection<TDomain, TCategory> collection;

        /// <summary>
        /// <para lang="cs">
        /// Párování objektů lokálních hustot pro třídění,
        /// klíč: objekt lokální hustoty pro třídění z databáze,
        /// hodnota: objekt lokální hustoty pro třídění z json souboru</para>
        /// <para lang="en">
        /// Pairing local density objects for classification,
        /// key: local density object for classification from database,
        /// value: local density object for classification from json file</para>
        /// </summary>
        public ReplacingLocalDensityObject<TDomain, TCategory> pairing;

        private string nodeCategoryToolTipText = String.Empty;
        private string nodeLDsityObjectToolTipText = String.Empty;
        private string nodeLDsityObjectForADSPToolTipText = String.Empty;
        private string msgNoLDsityContribForLDsityObject = String.Empty;
        private string msgTooManyLDsityContribForLDsityObject = String.Empty;
        private string msgInvalidClassificationRules = String.Empty;
        private string msgNoCategoryForSave = String.Empty;
        private string msgNoLDsityObjectsForADSPForSave = String.Empty;
        private string msgNoClassificationRules = String.Empty;
        private string msgNoSuccesfullyCategorizedPanels = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek - nadpis pro klasifikační pravidla</para>
        /// <para lang="en">Control - caption for classification rules</para>
        /// </summary>
        private ControlClassificationRuleCaption<TDomain, TCategory> ctrClassificationRuleCaption;

        /// <summary>
        /// <para lang="cs">Ovládací prvek - přepínač klasifikačních pravidel</para>
        /// <para lang="en">Control - classification rule switch</para>
        /// </summary>
        private ControlClassificationRuleSwitch<TDomain, TCategory> ctrClassificationRuleSwitch;

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro definici závislostí
        /// mezi kategoriemi plošné domény nebo subpopulace</para>
        /// <para lang="en">Control for definition dependencies
        /// among area domain or subpopulation categories</para>
        /// </summary>
        private ControlDependenceEditor<TDomain, TCategory> ctrDependenceEditor;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </param>
        public FormCategoryNew(
            Control controlOwner,
            TDomain domain)
            : base()
        {
            Initialize(
                controlOwner: controlOwner,
                domain: domain);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                ITargetDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTDSelectionAttributeCategory)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                ITargetDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlTDSelectionAttributeCategory)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTDSelectionAttributeCategory)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro cílová data (read-only)</para>
        /// <para lang="en">Module for target data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ITargetDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Typ domény a jejích kategorií (read-only)</para>
        /// <para lang="en">Domain and its categories type (read-only)</para>
        /// </summary>
        public static TDArealOrPopulationEnum ArealOrPopulation
        {
            get
            {
                return typeof(TDomain).FullName switch
                {
                    "ZaJi.NfiEstaPg.TargetData.TDAreaDomain" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                TDArealOrPopulationEnum.AreaDomain,

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDAreaDomain)} and {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    "ZaJi.NfiEstaPg.TargetData.TDSubPopulation" =>
                        typeof(TCategory).FullName switch
                        {
                            "ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategory" =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Incompatible types ",
                                        $"{nameof(TDSubPopulation)} and {nameof(TDAreaDomainCategory)}."),
                                    paramName: nameof(TCategory)),

                            "ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategory" =>
                                TDArealOrPopulationEnum.Population,

                            _ =>
                                throw new ArgumentException(
                                    message: String.Concat(
                                        $"Argument {nameof(TCategory)} must be type of ",
                                        $"{nameof(TDAreaDomainCategory)} or {nameof(TDSubPopulationCategory)}."),
                                    paramName: nameof(TCategory)),
                        },

                    _ =>
                        throw new ArgumentException(
                            message: String.Concat(
                                $"Argument {nameof(TDomain)} must be type of ",
                                $"{nameof(TDAreaDomain)} or {nameof(TDSubPopulation)}."),
                            paramName: nameof(TDomain))
                };
            }
        }


        /// <summary>
        /// <para lang="cs">Klasifikační typ (read-only)</para>
        /// <para lang="en">Classification type (read-only)</para>
        /// </summary>
        public TDClassificationTypeEnum ClassificationType
        {
            get
            {
                return Domain.ClassificationTypeValue;
            }
        }


        /// <summary>
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation</para>
        /// </summary>
        public TDomain Domain
        {
            get
            {
                if (domain == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Domain)} must not be null.",
                        paramName: nameof(Domain));
                }
                return domain;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(Domain)} must not be null.",
                        paramName: nameof(Domain));
                }
                domain = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Skupina klasifikačních pravidel pro jednu doménu, načtená z json souboru</para>
        /// <para lang="en">Collection of classification rules for one domain, loaded from json file</para>
        /// </summary>
        public ClassificationRuleCollection<TDomain, TCategory> Collection
        {
            get
            {
                return collection;
            }
            set
            {
                collection = value;
                InsertCategoriesFromJson();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Párování objektů lokálních hustot pro třídění,
        /// klíč: objekt lokální hustoty pro třídění z databáze,
        /// hodnota: objekt lokální hustoty pro třídění z json souboru</para>
        /// <para lang="en">
        /// Pairing local density objects for classification,
        /// key: local density object for classification from database,
        /// value: local density object for classification from json file</para>
        /// </summary>
        public ReplacingLocalDensityObject<TDomain, TCategory> Pairing
        {
            get
            {
                return pairing;
            }
            set
            {
                pairing = value;
                InsertCategoriesFromJson();
            }
        }


        /// <summary>
        /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
        /// <para lang="en">Changes in classification rules are not allowed</para>
        /// </summary>
        public bool ReadOnlyMode
        {
            get
            {
                return readOnlyMode;
            }
            set
            {
                readOnlyMode = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Úroveň uzlů v TreeView, kde jsou uloženy páry klasifikačních pravidel (read-only)</para>
        /// <para lang="en">TreeView Node level, where classification rule pairs are stored (read-only)</para>
        /// </summary>
        public static int Level
        {
            get
            {
                return 2;
            }
        }

        /// <summary>
        /// <para lang="cs">Je aktuální objekt lokální hustoty pro třídění
        /// zahrnutý mezi objekty pro uložení do databáze? (read-only)</para>
        /// <para lang="en">Is current local density object for classification
        /// included among objects for saving in database? (read-only)</para>
        /// </summary>
        public bool SelectedLDsityObjectForADSPIncluded
        {
            get
            {
                return
                    ctrClassificationRuleCaption
                    .LDsityObjectForADSPIncluded;
            }
        }


        /// <summary>
        /// <para lang="cs">Seznam všech uzlů v TreeView kategorií</para>
        /// <para lang="en">List of all nodes in TreeView of categories</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzel předcházející vybranému uzlu</para>
        /// <para lang="en">TreeNode previous selected TreeNode</para>
        /// </summary>
        private TreeNode PreviousNode
        {
            get
            {
                return previousNode;
            }
            set
            {
                previousNode = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam objektů lokálních hustot (obsah číselníku c_ldsity_object)</para>
        /// <para lang="en">List of local density objects (content of the lookup table c_ldsity_object)</para>
        /// </summary>
        private TDLDsityObjectList CLDsityObjects
        {
            get
            {
                return cLDsityObjects ??
                    new TDLDsityObjectList(database: Database);
            }
            set
            {
                cLDsityObjects = value ??
                    new TDLDsityObjectList(database: Database);
            }
        }


        /// <summary>
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace (read-only)</para>
        /// <para lang="en">Categories of the area domain or subpopulation (read-only)</para>
        /// </summary>
        public List<TCategory> Categories
        {
            get
            {
                return [.. Nodes
                    .Where(a => a.Level == 0)
                    .Select(a => (ClassificationRulePair<TDomain, TCategory>)a.Tag)
                    .Select(a => a.Category)
                    .Distinct()
                    .OrderBy(a => a.Id)];
            }
        }

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot pro třídění (read-only)</para>
        /// <para lang="en">Local density objects for classification (read-only)</para>
        /// </summary>
        public List<TDLDsityObject> LDsityObjectsForADSP
        {
            get
            {
                return [.. Nodes
                    .Where(a => a.Level == Level)
                    .Select(a => (ClassificationRulePair<TDomain, TCategory>)a.Tag)
                    .Select(a => a.LDsityObjectForADSP)
                    .Distinct()
                    .OrderBy(a => a.Id)];
            }
        }


        /// <summary>
        /// <para lang="cs">Páry klasifikačních pravidel(read-only)</para>
        /// <para lang="en">Classification rule pairs(read-only)</para>
        /// </summary>
        public List<ClassificationRulePair<TDomain, TCategory>> ClassificationRulePairs
        {
            get
            {
                return [.. Nodes
                    .Where(a => a.Level == Level)
                    .Select(a => (ClassificationRulePair<TDomain, TCategory>)a.Tag)
                    .Distinct()
                    .OrderBy(a => a.Category.Id)
                    .ThenBy(a => a.LDsityObject.Id)
                    .ThenBy(a => a.LDsityObjectForADSP.Id)];
            }
        }

        /// <summary>
        /// <para lang="cs">Klasifikační pravidla kategorií plošných domén nebo subpopulací
        /// pro kladné příspěvky lokálních hustot(read-only)</para>
        /// <para lang="en">Classification rules for area domain or subpopulation categories
        /// for positive local density contributions(read-only)</para>
        /// </summary>
        public List<ClassificationRule<TDomain, TCategory>> ClassificationRulesPositive
        {
            get
            {
                return ClassificationRulePairs
                    .Select(a => a.Positive)
                    .Distinct()
                    .ToList<ClassificationRule<TDomain, TCategory>>();
            }
        }

        /// <summary>
        /// <para lang="cs">Klasifikační pravidla kategorií plošných domén nebo subpopulací
        /// pro záporné příspěvky lokálních hustot(read-only)</para>
        /// <para lang="en">Classification rules for area domain or subpopulation categories
        /// for negative local density contributions(read-only)</para>
        /// </summary>
        public List<ClassificationRule<TDomain, TCategory>> ClassificationRulesNegative
        {
            get
            {
                return ClassificationRulePairs
                    .Select(a => a.Negative)
                    .Distinct()
                    .ToList<ClassificationRule<TDomain, TCategory>>();
            }
        }


        /// <summary>
        /// <para lang="cs">Vybraný pár klasifikačních pravidel (read-only)</para>
        /// <para lang="en">Selected classification rule pair (read-only)</para>
        /// </summary>
        public ClassificationRulePair<TDomain, TCategory> SelectedClassificationRulePair
        {
            get
            {
                if (TvwCategory.SelectedNode == null)
                {
                    return null;
                }

                if (TvwCategory.SelectedNode.Tag == null)
                {
                    return null;
                }

                return
                    (ClassificationRulePair<TDomain, TCategory>)
                        TvwCategory.SelectedNode.Tag;
            }
        }

        /// <summary>
        /// <para lang="cs">Předcházející pár klasifikačních pravidel (read-only)</para>
        /// <para lang="en">Previous classification rule pair (read-only)</para>
        /// </summary>
        public ClassificationRulePair<TDomain, TCategory> PreviousClassificationRulePair
        {
            get
            {
                if (PreviousNode == null)
                {
                    return null;
                }

                if (PreviousNode.Tag == null)
                {
                    return null;
                }

                return
                    (ClassificationRulePair<TDomain, TCategory>)
                        PreviousNode.Tag;
            }
        }


        /// <summary>
        /// <para lang="cs">Vybrané kladné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Selected positive classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> SelectedClassificationRulePositive
        {
            get
            {
                if (SelectedClassificationRulePair == null)
                {
                    return null;
                }

                if (TvwCategory.SelectedNode.Level != Level)
                {
                    return null;
                }

                return
                    SelectedClassificationRulePair.Positive;
            }
        }

        /// <summary>
        /// <para lang="cs">Předcházející kladné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Previous positive classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> PreviousClassificationRulePositive
        {
            get
            {
                if (PreviousClassificationRulePair == null)
                {
                    return null;
                }

                if (previousNode.Level != Level)
                {
                    return null;
                }

                return
                    PreviousClassificationRulePair.Positive;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané záporné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Selected negative classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> SelectedClassificationRuleNegative
        {
            get
            {
                if (ClassificationType != TDClassificationTypeEnum.ChangeOrDynamic)
                {
                    return null;
                }

                if (SelectedClassificationRulePair == null)
                {
                    return null;
                }

                if (TvwCategory.SelectedNode.Level != Level)
                {
                    return null;
                }

                return
                    SelectedClassificationRulePair.Negative;
            }
        }

        /// <summary>
        /// <para lang="cs">Předcházející záporné klasifikační pravidlo (read-only)</para>
        /// <para lang="en">Previous negative classification rule (read-only)</para>
        /// </summary>
        public ClassificationRule<TDomain, TCategory> PreviousClassificationRuleNegative
        {
            get
            {
                if (ClassificationType != TDClassificationTypeEnum.ChangeOrDynamic)
                {
                    return null;
                }

                if (PreviousClassificationRulePair == null)
                {
                    return null;
                }

                if (previousNode.Level != Level)
                {
                    return null;
                }

                return
                    PreviousClassificationRulePair.Negative;
            }
        }


        /// <summary>
        /// <para lang="cs">Pořadové číslo pro novou kategorii plošné domény nebo subpopulace (read-only)</para>
        /// <para lang="en">Serial number for new area domain or subpopulation category (read-only)</para>
        /// </summary>
        public int SerialNumber
        {
            get
            {
                return
                    Categories.Count != 0 ?
                        Categories.Max(a => a.Id) + 1
                        : 1;
            }
        }


        // Výsledky výběru z předchozích komponent:
        // Selection results from previous components:

        /// <summary>
        /// <para lang="cs">Objekty lokálních hustot ve vybrané skupině
        /// (vybrané na 1.formuláři) (read-only)</para>
        /// <para lang="en">Local density objects in the selected group
        /// (selected on the 1st form) (read-only)</para>
        /// </summary>
        public TDLDsityObjectList SelectedLocalDensityObjects
        {
            get
            {
                Dictionary<
                    TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObjectIdentifier,
                    TDFunctions.FnGetLDsityObjectsForTargetVariable.DataObject>
                    ldsityObjectsForTargetVariable =
                        ((ControlTDSelectionAttributeCategory)ControlOwner)
                            .LDsityObjectsForTargetVariableSelect(
                                domainType: ArealOrPopulation);

                List<int> listLDsityObjectsForTargetVariableIds
                    = ldsityObjectsForTargetVariable.Values
                    .Select(b => b.LDsityObject.Id)
                    .ToList<int>();

                TDLDsityObjectList result = new(
                    database: Database,
                    rows:
                        CLDsityObjects.Data.AsEnumerable()
                        .Where(a => listLDsityObjectsForTargetVariableIds
                        .Contains(Functions.GetIntArg(
                            row: a,
                            name: TDLDsityObjectList.ColId.Name,
                            defaultValue: 0))));

                return result;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané příspěvky lokálních hustot
        /// pro vybranou skupinu cílových proměnných
        /// (vybrané na 2.formuláři) (read-only)</para>
        /// <para lang="en">Selected local density contributions
        /// for the selected target variable group
        /// (selected on the 2nd form) (read-only)</para>
        /// </summary>
        public TDLDsityList SelectedLDsities
        {
            get
            {
                return ((ControlTDSelectionAttributeCategory)ControlOwner)
                    .LDsitiesForTargetVariableSelect(
                        domainType: ArealOrPopulation);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryNew<TDomain, TCategory>)}",    "Nové kategorie plošné domény" },
                            { nameof(BtnCancel),                                   "Zrušit" },
                            { nameof(BtnCategoryAdd),                              "Přidat novou kategorii plošné domény" },
                            { nameof(BtnCategoryDelete),                           "Smazat vybranou kategorii plošné domény"},
                            { nameof(BtnCategoryEdit),                             "Editovat vybranou kategorii plošné domény" },
                            { nameof(BtnDependenceDefine),                         "Definovat závislosti" },
                            { nameof(BtnOK),                                       "Zapsat do databáze" },
                            { nameof(GrpCategory),                                 "Kategorie plošné domény" },
                            { nameof(GrpWorkSpace),                                String.Empty },
                            { nameof(nodeCategoryToolTipText),                     "Kategorie plošné domény: $1" },
                            { nameof(nodeLDsityObjectToolTipText),                 "Objekt lokální hustoty: $1" },
                            { nameof(nodeLDsityObjectForADSPToolTipText),          "Objekt lokální hustoty pro třídění: $1" },
                            { nameof(TsrCategory),                                 String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),           "Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),      "Pro vybraný objekt lokální hustoty v databázi existuje více než jeden příspěvek lokální hustoty."  },
                            { nameof(msgInvalidClassificationRules),               "Některé kategorie plošné domény mají neplatná klasifikační pravidla." },
                            { nameof(msgNoCategoryForSave),                        "Není definovaná žádná kategorie plošné domény pro uložení do databáze." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),            "Nejsou zvolené žádné objekty lokálních hustot pro třídění." },
                            { nameof(msgNoClassificationRules),                    "Nejsou zvolena žádná klasifikační pravidla pro uložení do databáze." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),           "Neexistují žádné panely s plně a úspěšně zařazenými objekty podle klasifikačních pravidel." }
                         }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryNew<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryNew<TDomain, TCategory>)}",    "Nové kategorie subpopulace" },
                            { nameof(BtnCancel),                                   "Zrušit" },
                            { nameof(BtnCategoryAdd),                              "Přidat novou kategorii subpopulace" },
                            { nameof(BtnCategoryDelete),                           "Smazat vybranou kategorii subpopulace"},
                            { nameof(BtnCategoryEdit),                             "Editovat vybranou kategorii subpopulace" },
                            { nameof(BtnDependenceDefine),                         "Definovat závislosti" },
                            { nameof(BtnOK),                                       "Zapsat do databáze" },
                            { nameof(GrpCategory),                                 "Kategorie subpopulace" },
                            { nameof(GrpWorkSpace),                                String.Empty },
                            { nameof(nodeCategoryToolTipText),                     "Kategorie subpopulace: $1" },
                            { nameof(nodeLDsityObjectToolTipText),                 "Objekt lokální hustoty: $1" },
                            { nameof(nodeLDsityObjectForADSPToolTipText),          "Objekt lokální hustoty pro třídění: $1" },
                            { nameof(TsrCategory),                                 String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),           "Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),      "Pro vybraný objekt lokální hustoty v databázi existuje více než jeden příspěvek lokální hustoty."  },
                            { nameof(msgInvalidClassificationRules),               "Některé kategorie subpopulace mají neplatná klasifikační pravidla." },
                            { nameof(msgNoCategoryForSave),                        "Není definovaná žádná kategorie subpopulace pro uložení do databáze." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),            "Nejsou zvolené žádné objekty lokálních hustot pro třídění." },
                            { nameof(msgNoClassificationRules),                    "Nejsou zvolena žádná klasifikační pravidla pro uložení do databáze." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),           "Neexistují žádné panely s plně a úspěšně zařazenými objekty podle klasifikačních pravidel." }
                        }
                        : languageFile.NationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryNew<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    _ => [],
                },

                LanguageVersion.International => ArealOrPopulation switch
                {
                    TDArealOrPopulationEnum.AreaDomain => (languageFile == null) ?
                        new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryNew<TDomain, TCategory>)}",    "New area domain categories" },
                            { nameof(BtnCancel),                                   "Cancel" },
                            { nameof(BtnCategoryAdd),                              "Add a new area domain category" },
                            { nameof(BtnCategoryDelete),                           "Delete the selected area domain category" },
                            { nameof(BtnCategoryEdit),                             "Edit the selected area domain category" },
                            { nameof(BtnDependenceDefine),                         "Define dependences" },
                            { nameof(BtnOK),                                       "Write to database" },
                            { nameof(GrpCategory),                                 "Area domain categories" },
                            { nameof(GrpWorkSpace),                                String.Empty },
                            { nameof(nodeCategoryToolTipText),                     "Area domain category: $1"},
                            { nameof(nodeLDsityObjectToolTipText),                 "Local density object: $1" },
                            { nameof(nodeLDsityObjectForADSPToolTipText),          "Local density object for classification: $1" },
                            { nameof(TsrCategory),                                 String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),           "There is no local density contribution in database for selected local density object." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),      "There are more than one local density contribution in database for selected local density object." },
                            { nameof(msgInvalidClassificationRules),               "Some area domain categories contain invalid classification rules." },
                            { nameof(msgNoCategoryForSave),                        "There is no defined area domain category for inserting into database." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),            "There are no selected local density objects for classification." },
                            { nameof(msgNoClassificationRules),                    "There are no selected classification rules for inserting into database." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),           "There are no panels with completely and succesfully categorized objects by given classification rules." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryNew<TDAreaDomain, TDAreaDomainCategory>)}{nameof(TDAreaDomain)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    TDArealOrPopulationEnum.Population => (languageFile == null) ?
                        new Dictionary<string, string>()
                        {
                            { $"{nameof(FormCategoryNew<TDomain, TCategory>)}",    "New subpopulation categories"  },
                            { nameof(BtnCancel),                                   "Cancel" },
                            { nameof(BtnCategoryAdd),                              "Add a new subpopulation category"},
                            { nameof(BtnCategoryDelete),                           "Delete the selected subpopulation category"},
                            { nameof(BtnCategoryEdit),                             "Edit the selected subpopulation category" },
                            { nameof(BtnDependenceDefine),                         "Define dependences" },
                            { nameof(BtnOK),                                       "Write to database" },
                            { nameof(GrpCategory),                                 "Subpopulation categories" },
                            { nameof(GrpWorkSpace),                                String.Empty },
                            { nameof(nodeCategoryToolTipText),                     "Subpopulation category: $1"},
                            { nameof(nodeLDsityObjectToolTipText),                 "Local density object: $1" },
                            { nameof(nodeLDsityObjectForADSPToolTipText),          "Local density object for classification: $1" },
                            { nameof(TsrCategory),                                 String.Empty },
                            { nameof(msgNoLDsityContribForLDsityObject),           "There is no local density contribution in database for selected local density object." },
                            { nameof(msgTooManyLDsityContribForLDsityObject),      "There are more than one local density contribution in database for selected local density object." },
                            { nameof(msgInvalidClassificationRules),               "Some subpopulation categories contain invalid classification rules." },
                            { nameof(msgNoCategoryForSave),                        "There is no defined subpopulation category for inserting into database." },
                            { nameof(msgNoLDsityObjectsForADSPForSave),            "There are no selected local density objects for classification." },
                            { nameof(msgNoClassificationRules),                    "There are no selected classification rules for inserting into database." },
                            { nameof(msgNoSuccesfullyCategorizedPanels),           "There are no panels with completely and succesfully categorized objects by given classification rules." }
                        }
                        : languageFile.InternationalVersion.Data.TryGetValue(
                            key: $"{nameof(FormCategoryNew<TDSubPopulation, TDSubPopulationCategory>)}{nameof(TDSubPopulation)}",
                            out Dictionary<string, string> value)
                                ? value
                                : [],

                    _ => [],
                },

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="domain">
        /// <para lang="cs">Plošná doména nebo subpopulace</para>
        /// <para lang="en">Area domain or subpopulation for that new categories will be created</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            TDomain domain)
        {
            ControlOwner = controlOwner;

            Domain = domain;

            Collection = null;

            Pairing = null;

            ReadOnlyMode = false;

            Nodes = null;

            PreviousNode = null;

            CLDsityObjects = null;

            LoadContent();

            InitializeToolStripIcons();

            InitializeControls();

            InitializeLabels();

            SetStyle();

            SelectCategory();

            BtnCancel.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.Cancel;
                   Close();
               });

            BtnOK.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.OK;
                   Close();
               });

            BtnCategoryEdit.Click += new EventHandler(
               (sender, e) =>
               {
                   UpdateCategory();
               });

            BtnCategoryAdd.Click += new EventHandler(
               (sender, e) =>
               {
                   InsertCategory();
               });

            BtnCategoryDelete.Click += new EventHandler(
               (sender, e) =>
               {
                   DeleteCategory();
               });

            BtnDependenceDefine.Click += new EventHandler(
               (sender, e) =>
               {
                   ShowDependencies();
               });

            TvwCategory.AfterSelect += new TreeViewEventHandler(
                (sender, e) =>
                {
                    SelectCategory();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !SaveCategoriesAndRules();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });

            BtnOK.Focus();
        }


        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent()
        {
            CLDsityObjects = new TDLDsityObjectList(database: Database);
            CLDsityObjects.ReLoad();

            switch (ArealOrPopulation)
            {
                case TDArealOrPopulationEnum.AreaDomain:
                    Database.STargetData.CAreaDomain.ReLoad();
                    Database.STargetData.CAreaDomainCategory.ReLoad();
                    Database.STargetData.CLDsityObject.ReLoad();
                    return;

                case TDArealOrPopulationEnum.Population:
                    Database.STargetData.CSubPopulation.ReLoad();
                    Database.STargetData.CSubPopulationCategory.ReLoad();
                    Database.STargetData.CLDsityObject.ReLoad();
                    return;

                default:
                    return;
            }
        }


        /// <summary>
        /// <para lang="cs">Nastavení stylu formuláře</para>
        /// <para lang="en">Setting form style</para>
        /// </summary>
        private void SetStyle()
        {
            Font = Setting.FormFont;
            ForeColor = Setting.FormForeColor;

            BtnOK.Font = Setting.ButtonFont;
            BtnOK.ForeColor = Setting.ButtonForeColor;

            BtnCancel.Font = Setting.ButtonFont;
            BtnCancel.ForeColor = Setting.ButtonForeColor;

            BtnCategoryAdd.Font = Setting.ToolStripButtonFont;
            BtnCategoryAdd.ForeColor = Setting.ToolStripButtonForeColor;

            BtnCategoryDelete.Font = Setting.ToolStripButtonFont;
            BtnCategoryDelete.ForeColor = Setting.ToolStripButtonForeColor;

            BtnCategoryEdit.Font = Setting.ToolStripButtonFont;
            BtnCategoryEdit.ForeColor = Setting.ToolStripButtonForeColor;

            BtnDependenceDefine.Font = Setting.ToolStripButtonFont;
            BtnDependenceDefine.ForeColor = Setting.ToolStripButtonForeColor;

            GrpCategory.Font = Setting.GroupBoxFont;
            GrpCategory.ForeColor = Setting.GroupBoxForeColor;

            GrpWorkSpace.Font = Setting.GroupBoxFont;
            GrpWorkSpace.ForeColor = Setting.GroupBoxForeColor;

            LblCaption.Font = Setting.LabelMainFont;
            LblCaption.ForeColor = Setting.LabelMainForeColor;

            LblClassificationRuleCaption.Font = Setting.LabelValueFont;
            LblClassificationRuleCaption.ForeColor = Setting.LabelValueForeColor;

            LblClassificationRuleSwitch.Font = Setting.LabelValueFont;
            LblClassificationRuleSwitch.ForeColor = Setting.LabelValueForeColor;

            LblDependenceEditor.Font = Setting.LabelValueFont;
            LblDependenceEditor.ForeColor = Setting.LabelValueForeColor;
        }

        /// <summary>
        /// <para lang="cs">Inicializace ikon tlačítek</para>
        /// <para lang="en">Initializing the button icons</para>
        /// </summary>
        private void InitializeToolStripIcons()
        {
            ControlResources resources = new();

            BtnCategoryEdit.Image = resources.IconPencil.Image;
            BtnCategoryEdit.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnCategoryAdd.Image = resources.IconPlusGreen.Image;
            BtnCategoryAdd.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnCategoryDelete.Image = resources.IconMinusRed.Image;
            BtnCategoryDelete.DisplayStyle = ToolStripItemDisplayStyle.Image;

            BtnDependenceDefine.Image = resources.IconSchema.Image;
            BtnDependenceDefine.DisplayStyle = ToolStripItemDisplayStyle.Image;

            ImageList ilCategoryStatus = new();

            ilCategoryStatus.Images.Add(
                key: "blue",
                image: resources.IconBlueBall.Image);

            ilCategoryStatus.Images.Add(
                key: "green",
                image: resources.IconGreenBall.Image);

            ilCategoryStatus.Images.Add(
                key: "red", image:
                resources.IconRedBall.Image);

            TvwCategory.ImageList = ilCategoryStatus;
            TvwCategory.SelectedImageKey = "blue";
        }

        /// <summary>
        /// <para lang="cs">Inicializace ovládacích prvků</para>
        /// <para lang="en">Initializing user controls</para>
        /// </summary>
        private void InitializeControls()
        {
            // TreeView Categories
            TvwCategory.Nodes.Clear();
            Nodes = null;

            HideDependencies();

            // ControlClassificationRuleCaption
            PnlClassificationRuleCaption.Controls.Clear();
            ctrClassificationRuleCaption =
                new ControlClassificationRuleCaption<TDomain, TCategory>(
                    controlOwner: this)
                {
                    DisplayCheckBox = true,
                    Dock = DockStyle.Fill
                };
            ctrClassificationRuleCaption.LDsityObjectForADSPExcluded
                += new EventHandler(
                    (sender, e) =>
                    {
                        ctrClassificationRuleSwitch?.Reset();
                        ctrDependenceEditor?.Reset();
                    });
            PnlClassificationRuleCaption.Controls.Add(
                value: ctrClassificationRuleCaption);

            // ControlClassificationRuleSwitch
            PnlClassificationRuleSwitch.Controls.Clear();
            ctrClassificationRuleSwitch =
                new ControlClassificationRuleSwitch<TDomain, TCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrClassificationRuleSwitch.ClassificationRuleTextChanged +=
                new EventHandler(
                    (sender, e) =>
                    {
                        SetClassificationRules();
                    });
            PnlClassificationRuleSwitch.Controls.Add(
                value: ctrClassificationRuleSwitch);

            ctrClassificationRuleSwitch.PositiveClassificationRuleData.BtnImportJson.Visible = false;
            ctrClassificationRuleSwitch.NegativeClassificationRuleData.BtnImportJson.Visible = false;

            // ControlDependenceEditor
            PnlDependenceEditor.Controls.Clear();
            ctrDependenceEditor =
                new ControlDependenceEditor<TDomain, TCategory>(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrDependenceEditor.DependenceEditorClosed +=
                new EventHandler(
                    (sender, e) =>
                    {
                        HideDependencies();
                    });
            PnlDependenceEditor.Controls.Add(
                value: ctrDependenceEditor);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing the form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(
                    key: $"{nameof(FormCategoryNew<TDomain, TCategory>)}",
                    out string frmCategoryNewText)
                        ? frmCategoryNewText
                        : String.Empty;

            BtnCancel.Text =
                labels.TryGetValue(
                    key: nameof(BtnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            BtnCategoryAdd.Text =
               labels.TryGetValue(
                   key: nameof(BtnCategoryAdd),
                   out string btnCategoryAddText)
                        ? btnCategoryAddText
                        : String.Empty;

            BtnCategoryDelete.Text =
                labels.TryGetValue(
                    key: nameof(BtnCategoryDelete),
                    out string btnCategoryDeleteText)
                        ? btnCategoryDeleteText
                        : String.Empty;

            BtnCategoryEdit.Text =
                labels.TryGetValue(
                    key: nameof(BtnCategoryEdit),
                    out string btnCategoryEditText)
                        ? btnCategoryEditText
                        : String.Empty;

            BtnDependenceDefine.Text =
                labels.TryGetValue(
                    key: nameof(BtnDependenceDefine),
                    out string btnDependenceDefineText)
                        ? btnDependenceDefineText
                        : String.Empty;

            BtnOK.Text =
                labels.TryGetValue(
                    key: nameof(BtnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            GrpCategory.Text =
                labels.TryGetValue(
                    key: nameof(GrpCategory),
                    out string grpCategoryText)
                        ? grpCategoryText
                        : String.Empty;

            GrpWorkSpace.Text =
                labels.TryGetValue(
                    key: nameof(GrpWorkSpace),
                    out string grpWorkSpaceText)
                        ? grpWorkSpaceText
                        : String.Empty;

            LblCaption.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? Domain.ExtendedLabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? Domain.ExtendedLabelCs
                        : Domain.ExtendedLabelEn;

            TsrCategory.Text =
                labels.TryGetValue(
                    key: nameof(TsrCategory),
                    out string tsrCategoryText)
                        ? tsrCategoryText
                        : String.Empty;

            nodeCategoryToolTipText =
              labels.TryGetValue(key: nameof(nodeCategoryToolTipText),
                      out nodeCategoryToolTipText)
                          ? nodeCategoryToolTipText
                          : String.Empty;

            nodeLDsityObjectToolTipText =
              labels.TryGetValue(key: nameof(nodeLDsityObjectToolTipText),
                      out nodeLDsityObjectToolTipText)
                          ? nodeLDsityObjectToolTipText
                          : String.Empty;

            nodeLDsityObjectForADSPToolTipText =
              labels.TryGetValue(key: nameof(nodeLDsityObjectForADSPToolTipText),
                      out nodeLDsityObjectForADSPToolTipText)
                          ? nodeLDsityObjectForADSPToolTipText
                          : String.Empty;

            msgNoLDsityContribForLDsityObject =
               labels.TryGetValue(key: nameof(msgNoLDsityContribForLDsityObject),
                       out msgNoLDsityContribForLDsityObject)
                           ? msgNoLDsityContribForLDsityObject
                           : String.Empty;

            msgTooManyLDsityContribForLDsityObject =
               labels.TryGetValue(key: nameof(msgTooManyLDsityContribForLDsityObject),
                       out msgTooManyLDsityContribForLDsityObject)
                           ? msgTooManyLDsityContribForLDsityObject
                           : String.Empty;

            msgInvalidClassificationRules =
               labels.TryGetValue(key: nameof(msgInvalidClassificationRules),
                       out msgInvalidClassificationRules)
                           ? msgInvalidClassificationRules
                           : String.Empty;

            msgNoCategoryForSave =
               labels.TryGetValue(key: nameof(msgNoCategoryForSave),
                       out msgNoCategoryForSave)
                           ? msgNoCategoryForSave
                           : String.Empty;

            msgNoLDsityObjectsForADSPForSave =
               labels.TryGetValue(key: nameof(msgNoLDsityObjectsForADSPForSave),
                       out msgNoLDsityObjectsForADSPForSave)
                           ? msgNoLDsityObjectsForADSPForSave
                           : String.Empty;

            msgNoClassificationRules =
               labels.TryGetValue(key: nameof(msgNoClassificationRules),
                       out msgNoClassificationRules)
                           ? msgNoClassificationRules
                           : String.Empty;

            msgNoSuccesfullyCategorizedPanels =
               labels.TryGetValue(key: nameof(msgNoSuccesfullyCategorizedPanels),
                       out msgNoSuccesfullyCategorizedPanels)
                           ? msgNoSuccesfullyCategorizedPanels
                           : String.Empty;

            ctrClassificationRuleCaption?.InitializeLabels();

            ctrClassificationRuleSwitch?.InitializeLabels();

            ctrDependenceEditor?.InitializeLabels();
        }


        /// <summary>
        /// <para lang="cs">Vrátí novou prázdnou kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en">Returns new empty area domain or subpopulation category</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrátí novou prázdnou kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en">Returns new empty area domain or subpopulation category</para>
        /// </returns>
        private TCategory CreateEmptyCategory()
        {
            TCategory newCategory;

            switch (ArealOrPopulation)
            {
                case TDArealOrPopulationEnum.AreaDomain:
                    TDAreaDomainCategory newAreaDomainCategory = new()
                    {
                        Id = SerialNumber,
                        AreaDomainId = Domain.Id
                    };
                    newCategory = (TCategory)Convert.ChangeType(
                        value: newAreaDomainCategory,
                        conversionType: typeof(TCategory));
                    break;

                case TDArealOrPopulationEnum.Population:
                    TDSubPopulationCategory newSubPopulationCategory = new()
                    {
                        Id = SerialNumber,
                        SubPopulationId = Domain.Id
                    };
                    newCategory = (TCategory)Convert.ChangeType(
                        value: newSubPopulationCategory,
                        conversionType: typeof(TCategory));
                    break;

                default:
                    throw new ArgumentException(
                           message: "Argument category must be type of TDAreaDomainCategory or TDSubPopulationCategory.",
                           paramName: "category");
            }

            return newCategory;
        }

        /// <summary>
        /// <para lang="cs">Přidání kategorie plošné domény nebo subpopulace do TreeView</para>
        /// <para lang="en">Add area domain or subpopulation category into TreeView</para>
        /// </summary>
        /// <param name="category">
        /// <para lang="cs">Kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Area domain or subpoulation category</para>
        /// </param>
        private void AddCategoryIntoTreeView(TCategory category)
        {
            if (category == null)
            {
                return;
            }

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            // Kořeny - kategorie plošné domény nebo subpopulace
            // Root level - area domain or subpulation category
            TreeNode nodeCategory = new()
            {
                Text =
                    (LanguageVersion == LanguageVersion.National) ? category.ExtendedLabelCs :
                    (LanguageVersion == LanguageVersion.International) ? category.ExtendedLabelEn :
                    category.Id.ToString()
            };

            TvwCategory.Nodes.Add(node: nodeCategory);
            Nodes.Add(item: nodeCategory);

            nodeCategory.Tag = new ClassificationRulePair<TDomain, TCategory>(
                owner: this,
                node: nodeCategory,
                flat: false,
                classificationType: ClassificationType,
                category: category,
                ldsityObject: null,
                ldsityObjectForADSP: null);

            nodeCategory.ToolTipText
                = (labels.TryGetValue(
                    key: nameof(nodeCategoryToolTipText),
                    out nodeCategoryToolTipText)
                        ? nodeCategoryToolTipText
                        : String.Empty)
                   .Replace(
                        oldValue: "$1",
                        newValue: nodeCategory.Text);

            // První úroveň - objekty lokální hustoty
            // First level - local density objects
            foreach (TDLDsityObject ldsityObject
                in SelectedLocalDensityObjects.Items)
            {
                TreeNode nodeLDsityObject = new()
                {
                    Text =
                        (LanguageVersion == LanguageVersion.National) ? ldsityObject.ExtendedLabelCs :
                        (LanguageVersion == LanguageVersion.International) ? ldsityObject.ExtendedLabelEn :
                        ldsityObject.Id.ToString()
                };

                nodeCategory.Nodes.Add(node: nodeLDsityObject);
                Nodes.Add(item: nodeLDsityObject);

                nodeLDsityObject.Tag = new ClassificationRulePair<TDomain, TCategory>(
                    owner: this,
                    node: nodeLDsityObject,
                    flat: false,
                    classificationType: ClassificationType,
                    category: category,
                    ldsityObject: ldsityObject,
                    ldsityObjectForADSP: null);

                nodeLDsityObject.ToolTipText
                    = (labels.TryGetValue(
                        key: nameof(nodeLDsityObjectToolTipText),
                        out nodeLDsityObjectToolTipText)
                            ? nodeLDsityObjectToolTipText
                            : String.Empty)
                       .Replace(
                            oldValue: "$1",
                            newValue: nodeLDsityObject.Text);

                // Druhá úroveň - objekty lokální hustoty pro třídění
                // Second level - local density objects for classification
                TDLDsityObjectList fnGetLDsityObjectForADSP =
                    TDFunctions.FnGetLDsityObjectForADSP.Execute(
                        database: Database,
                        ldsityObjectId: ldsityObject.Id,
                        arealOrPopulationId: (int)ArealOrPopulation);

                foreach (TDLDsityObject ldsityObjectForADSP in
                    fnGetLDsityObjectForADSP.Items)
                {
                    TreeNode nodeLDsityObjectForADSP = new()
                    {
                        Text =
                            (LanguageVersion == LanguageVersion.National) ? ldsityObjectForADSP.ExtendedLabelCs :
                            (LanguageVersion == LanguageVersion.International) ? ldsityObjectForADSP.ExtendedLabelEn :
                            ldsityObjectForADSP.Id.ToString()
                    };

                    nodeLDsityObject.Nodes.Add(node: nodeLDsityObjectForADSP);
                    Nodes.Add(item: nodeLDsityObjectForADSP);

                    nodeLDsityObjectForADSP.Tag = new ClassificationRulePair<TDomain, TCategory>(
                        owner: this,
                        node: nodeLDsityObjectForADSP,
                        flat: false,
                        classificationType: ClassificationType,
                        category: category,
                        ldsityObject: ldsityObject,
                        ldsityObjectForADSP: ldsityObjectForADSP);

                    nodeLDsityObjectForADSP.ToolTipText
                        = (labels.TryGetValue(
                            key: nameof(nodeLDsityObjectForADSPToolTipText),
                            out nodeLDsityObjectForADSPToolTipText)
                                ? nodeLDsityObjectForADSPToolTipText
                                : String.Empty)
                           .Replace(
                                oldValue: "$1",
                                newValue: nodeLDsityObjectForADSP.Text);
                }
            }

            TvwCategory.SelectedNode = nodeCategory;

            SelectCategory();
        }

        /// <summary>
        /// <para lang="cs">Přidání kategorií plošné domény nebo subpopulace do TreeView ze souboru JSON</para>
        /// <para lang="en">Add area domain or subpopulation categories into TreeView from JSON</para>
        /// </summary>
        private void InsertCategoriesFromJson()
        {
            if (Collection == null)
            {
                return;
            }

            if (Collection.IsEmpty)
            {
                return;
            }

            if (Pairing == null)
            {
                return;
            }

            if (Pairing.IsEmpty)
            {
                return;
            }

            TvwCategory.Nodes.Clear();
            Nodes.Clear();

            foreach (ClassificationRuleCollection<TDomain, TCategory>.CategoryJSON jsonCategory in Collection.Categories)
            {
                TCategory category = CreateEmptyCategory();

                category.Id = jsonCategory.CategoryId;
                category.LabelCs = jsonCategory.CategoryLabelCs;
                category.LabelEn = jsonCategory.CategoryLabelEn;
                category.DescriptionCs = jsonCategory.CategoryDescriptionCs;
                category.DescriptionEn = jsonCategory.CategoryDescriptionEn;

                AddCategoryIntoTreeView(category: category);
            }

            foreach (TreeNode node in Nodes.Where(a => a.Level == Level))
            {
                if (node.Tag == null) { continue; }

                ClassificationRulePair<TDomain, TCategory> item =
                    (ClassificationRulePair<TDomain, TCategory>)node.Tag;
                if (item == null) { continue; }

                List<ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON>
                    list = Collection.Categories
                        .Where(json => item.Category.Id == json.CategoryId)
                        .SelectMany(json => json.LDsityObjectsForADSP)
                        .ToList<ClassificationRuleCollection<TDomain, TCategory>.LocalDensityObjectJSON>();

                string jsonPositiveClassificationRule = list
                        .Where(json => json.LocalDensityObjectId == Pairing.GetIdentifier(id: item.LDsityObjectForADSP.Id))
                        .Select(json => json.PositiveClassificationRule)
                        .FirstOrDefault<string>();

                string jsonNegativeClassificationRule = list
                        .Where(json => json.LocalDensityObjectId == Pairing.GetIdentifier(id: item.LDsityObjectForADSP.Id))
                        .Select(json => json.NegativeClassificationRule)
                        .FirstOrDefault<string>();

                if (item.Positive != null)
                {
                    item.Positive.Text = jsonPositiveClassificationRule;
                }

                if (item.Negative != null)
                {
                    item.Negative.Text = jsonNegativeClassificationRule;
                }

                node.Tag = item;
            }

            // Validace všech uzlů
            foreach (ClassificationRulePair<TDomain, TCategory> crp in ClassificationRulePairs)
            {
                crp.ValidateSyntax(verbose: false);
            }
        }


        /// <summary>
        /// <para lang="cs">Vytvoření nové kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Create a new area domain or subpopulation category</para>
        /// </summary>
        private void InsertCategory()
        {
            TCategory newCategory = CreateEmptyCategory();

            FormCategoryEdit<TDomain, TCategory> frm =
                new(
                    controlOwner: ControlOwner,
                    category: newCategory,
                    categories: Categories,
                    categoryIsNew: true);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                AddCategoryIntoTreeView(category: newCategory);

                foreach (SuperiorDomain<TDomain, TCategory> dep in ctrDependenceEditor.Dependences.Values)
                {
                    dep.AddInferiorCategory(inferior: newCategory);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Změna popisků kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">Change of the labels of the area domain or subpopulation category</para>
        /// </summary>
        private void UpdateCategory()
        {
            TreeNode selectedNode = TvwCategory.SelectedNode;

            if (selectedNode != null)
            {
                TreeNode rootNode = selectedNode.Level switch
                {
                    0 => selectedNode,
                    1 => selectedNode.Parent,
                    2 => selectedNode.Parent.Parent,
                    _ => selectedNode,
                };
                ClassificationRule<TDomain, TCategory> selectedClassificationRule =
                    ((ClassificationRulePair<TDomain, TCategory>)rootNode.Tag).Positive;

                FormCategoryEdit<TDomain, TCategory> frm =
                    new(
                        controlOwner: ControlOwner,
                        category: selectedClassificationRule.Category,
                        categories: Categories,
                        categoryIsNew: false);

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    rootNode.Text =
                        (LanguageVersion == LanguageVersion.National) ?
                            selectedClassificationRule.Category.ExtendedLabelCs :
                        (LanguageVersion == LanguageVersion.International) ?
                            selectedClassificationRule.Category.ExtendedLabelEn :
                        rootNode.Name;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání kategorie plošné domény nebo subpopulace z TreeView</para>
        /// <para lang="en">Remove an area domain or subpopulation category from the TreeView</para>
        /// </summary>
        private void DeleteCategory()
        {
            TreeNode selectedNode = TvwCategory.SelectedNode;
            TreeNode nodeForDelete;

            if (selectedNode == null)
            {
                return;
            }

            switch (selectedNode.Level)
            {
                case 0:
                    nodeForDelete = selectedNode;
                    break;
                case 1:
                    nodeForDelete = selectedNode.Parent;
                    break;
                case 2:
                    nodeForDelete = selectedNode.Parent.Parent;
                    break;
                default:
                    return;
            }

            if (nodeForDelete == null)
            {
                return;
            }

            if (nodeForDelete.Tag == null)
            {
                return;
            }

            ClassificationRulePair<TDomain, TCategory> pairForDelete =
                    (ClassificationRulePair<TDomain, TCategory>)nodeForDelete.Tag;

            if (pairForDelete == null)
            {
                return;
            }

            TvwCategory.Nodes.Remove(node: nodeForDelete);

            List<TreeNode> removedNodes = Nodes
                .Where(a =>
                    ((ClassificationRulePair<TDomain, TCategory>)a.Tag).Positive.Category.Id
                    == pairForDelete.Positive.Category.Id)
                .ToList<TreeNode>();

            foreach (TreeNode node in removedNodes)
            {
                Nodes.Remove(item: node);
            }

            foreach (SuperiorDomain<TDomain, TCategory> dep in ctrDependenceEditor.Dependences.Values)
            {
                dep.RemoveInferiorCategory(inferior: pairForDelete.Positive.Category);
            }

            SelectCategory();
        }


        /// <summary>
        /// <para lang="cs">Spouští se v případě, že byla v TreeView vybrána kategorie plošné domény nebo subpopulace</para>
        /// <para lang="en">It fires if an area domain or subpopulation category was selected in the TreeView</para>
        /// </summary>
        private void SelectCategory()
        {
            if (TvwCategory.SelectedNode == null)
            {
                return;
            }

            ShowOrHideButtonDependenceDefine();

            ValidatePreviousNode();
            ColorTreeNodes();

            previousNode = TvwCategory.SelectedNode;

            //
            // Nový vybraný uzel -> musí se přenastavit nadpis a klasifikační pravidla
            //
            ctrClassificationRuleCaption?.Reset();
            ctrClassificationRuleSwitch?.Reset();
            ctrDependenceEditor?.Reset();
        }

        /// <summary>
        /// <para lang="cs">
        /// Kontrola validity klasifikačních pravidel předchozího uzlu
        /// </para>
        /// <para lang="en">
        /// Classification rules validation for previous TreeView Node
        /// </para>
        /// </summary>
        private void ValidatePreviousNode()
        {
            if (PreviousClassificationRulePair == null)
            {
                return;
            }

            PreviousClassificationRulePair
                .ValidateSyntax(verbose: false);

            if (ClassificationRulePairs == null)
            {
                return;
            }

            if (PreviousClassificationRulePair.Category == null)
            {
                return;
            }

            if (PreviousClassificationRulePair.LDsityObjectForADSP == null)
            {
                return;
            }

            // Validace všech uzlů v ramci stejné kategorie
            // a stejného objektu lokální hustoty pro třídění
            foreach (ClassificationRulePair<TDomain, TCategory> crp in
                ClassificationRulePairs
                    .Where(a => a.Category.Id == PreviousClassificationRulePair.Category.Id)
                    .Where(a => a.LDsityObjectForADSP.Id == PreviousClassificationRulePair.LDsityObjectForADSP.Id))
            {
                crp.ValidateSyntax(verbose: false);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Obarví červeně uzly v TreeView s nevalidním klasifikačním pravidlem
        /// a zeleně uzly s validním klasifikačním pravidlem
        /// </para>
        /// <para lang="en">
        /// Color red TreeNodes with not valid classification rulus
        /// and green TreeNodes with valid classification rules
        /// </para>
        /// </summary>
        private void ColorTreeNodes()
        {
            foreach (TreeNode node in Nodes)
            {
                if (node.Tag == null)
                {
                    node.ImageKey = "red";
                    continue;
                }

                ClassificationRulePair<TDomain, TCategory> pair =
                    (ClassificationRulePair<TDomain, TCategory>)node.Tag;

                if (pair == null)
                {
                    node.ImageKey = "red";
                    continue;
                }

                node.ImageKey =
                    pair.IsValid
                        ? "green"
                        : "red";
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Zobrazení ovládacího prvku pro definici závislostí
        /// mezi kategoriemi plošné domény nebo subpopulace </para>
        /// <para lang="en">
        /// Display control for definition dependencies
        /// among area domain or subpopulation categories
        /// </para>
        /// </summary>
        private void ShowDependencies()
        {
            SplClassificationRule.Panel1Collapsed = true;
            SplClassificationRule.Panel2Collapsed = false;
        }

        /// <summary>
        /// <para lang="cs">
        /// Skrytí ovládacího prvku pro definici závislostí
        /// mezi kategoriemi plošné domény nebo subpopulace </para>
        /// <para lang="en">
        /// Hide control for definition dependencies
        /// among area domain or subpopulation categories
        /// </para>
        /// </summary>
        private void HideDependencies()
        {
            SplClassificationRule.Panel1Collapsed = false;
            SplClassificationRule.Panel2Collapsed = true;

            ShowOrHideButtonDependenceDefine();
        }

        /// <summary>
        /// <para lang="cs">
        /// Zobrazí nebo skryje tlačítko pro definici závislostí </para>
        /// <para lang="en">
        /// Show or hide buttun for define dependencies
        /// </para>
        /// </summary>
        private void ShowOrHideButtonDependenceDefine()
        {
            // Závislosti se definují pro kategorie v rámci
            // objektů lokálních hustot pro třídění (tj. nesmí být null)

            if (SelectedClassificationRulePair == null)
            {
                BtnDependenceDefine.Enabled = false;
                return;
            }

            if (SelectedClassificationRulePair.LDsityObjectForADSP == null)
            {
                BtnDependenceDefine.Enabled = false;
                return;
            }

            BtnDependenceDefine.Enabled = true;
        }

        /// <summary>
        /// <para lang="cs">Nastaví klasifikační pravidla pro vybrané uzely</para>
        /// <para lang="en">Set classifications rule for selected TreeNodes</para>
        /// </summary>
        private void SetClassificationRules()
        {
            if (SelectedClassificationRulePair == null)
            {
                return;
            }

            if (ClassificationRulePairs == null)
            {
                return;
            }

            // Stejné klasifikační pravidlo musí být nastaveno pro všechny ldsityObjects
            // v rámci jedné kategorie plošné domény nebo subpopulace
            // Same classification rule must be set for all ldsityObjects
            // in one area domain or subpopulation category
            foreach (ClassificationRulePair<TDomain, TCategory> crp in ClassificationRulePairs
                .Where(a => a.Category.Id == SelectedClassificationRulePair.Category.Id)
                .Where(a => a.LDsityObjectForADSP.Id == SelectedClassificationRulePair.LDsityObjectForADSP.Id))
            {
                if (crp.Positive != null)
                {
                    crp.Positive.Text =
                        (SelectedClassificationRulePair.Positive == null)
                            ? String.Empty
                            : SelectedClassificationRulePair.Positive.Text;
                }

                if (crp.Negative != null)
                {
                    crp.Negative.Text =
                        (SelectedClassificationRulePair.Negative == null)
                            ? String.Empty
                            : SelectedClassificationRulePair.Negative.Text;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Kontroluje počet vybraných příspěvků lokálních hustot</para>
        /// <para lang="en">Method checks number of selected local density contributions</para>
        /// </summary>
        /// <param name="ldsities">
        /// <para lang="cs">Seznam vybraných příspěvků lokálních hustot</para>
        /// <para lang="en">List of selected local density contributions</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrácí true pokud existuje právě jeden</para>
        /// <para lang="en">Returns true when just on local density contribution exists</para>
        /// </returns>
        public bool LDsityCountIsValid(List<TDLDsity> ldsities)
        {
            if (ldsities == null)
            {
                // Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty
                // There is none local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgNoLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            TDLDsity ldsity = ldsities.FirstOrDefault();

            if (ldsity == null)
            {
                // Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty
                // There is none local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgNoLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (ldsities.Count < 1)
            {
                // Pro vybraný objekt lokální hustoty v databázi neexistuje žádný příspěvek lokální hustoty
                // There is none local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgNoLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (ldsities.Count > 1)
            {
                // Pro vybraný objekt lokální hustoty v databázi existuje více než jeden příspěvek lokální hustoty
                // There are more than one local density contribution in database for selected local density object
                MessageBox.Show(
                    text: msgTooManyLDsityContribForLDsityObject,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            return true;
        }


        /// <summary>
        /// <para lang="cs">
        /// Připraví vstupní argument pro uložené procedury,
        /// který obsahuje popis závislostí mezi kategoriemi plošných domén
        /// </para>
        /// <para lang="en">
        /// Prepares the input argument for the stored procedures,
        /// which contains a description of the dependencies between the area domain categories
        /// </para>
        /// </summary>
        /// <param name="ldsityObjectForADSP">
        /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
        /// <para lang="en">Local density object used for classification</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrací pole polí,
        /// vnitřní pole jsou pro jednotivé podřízené kategorie
        /// a obsahují seznam identifikátorů svých nadřízených kategorií
        /// </para>
        /// <para lang="en">
        /// Returns an array of arrays,
        /// the internal arrays are for each inferior area domain category
        /// and contain a list of identifiers of their superior area domain categories</para>
        /// </returns>
        public List<List<Nullable<int>>> PrepareParamADC(
            TDLDsityObject ldsityObjectForADSP)
        {
            if (ldsityObjectForADSP == null)
            {
                return null;
            }

            if (ArealOrPopulation != TDArealOrPopulationEnum.AreaDomain)
            {
                return null;
            }

            SuperiorDomain<TDomain, TCategory> dependencies =
                ctrDependenceEditor.Dependences.TryGetValue(
                    key: ldsityObjectForADSP,
                    out SuperiorDomain<TDomain, TCategory> value)
                        ? value
                        : null;

            return
                dependencies?.ToListOfListOfInt(
                    inferiorCategories: [.. Categories.OrderBy(a => a.Id)]);
        }

        /// <summary>
        /// <para lang="cs">
        /// Připraví vstupní argument pro uložené procedury,
        /// který obsahuje popis závislostí mezi kategoriemi subpopulací
        /// </para>
        /// <para lang="en">
        /// Prepares the input argument for the stored procedures,
        /// which contains a description of the dependencies between the subpopulation categories
        /// </para>
        /// </summary>
        /// <param name="ldsityObjectForADSP">
        /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
        /// <para lang="en">Local density object used for classification</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrací pole polí,
        /// vnitřní pole jsou pro jednotivé podřízené kategorie
        /// a obsahují seznam identifikátorů svých nadřízených kategorií
        /// </para>
        /// <para lang="en">
        /// Returns an array of arrays,
        /// the internal arrays are for each inferior subpopulation category
        /// and contain a list of identifiers of their superior subpopulation categories</para>
        /// </returns>
        public List<List<Nullable<int>>> PrepareParamSPC(
            TDLDsityObject ldsityObjectForADSP)
        {
            if (ldsityObjectForADSP == null)
            {
                return null;
            }

            if (ArealOrPopulation != TDArealOrPopulationEnum.Population)
            {
                return null;
            }

            SuperiorDomain<TDomain, TCategory> dependencies =
                ctrDependenceEditor.Dependences.TryGetValue(
                    key: ldsityObjectForADSP,
                    out SuperiorDomain<TDomain, TCategory> value)
                        ? value
                        : null;

            return
                dependencies?.ToListOfListOfInt(
                    inferiorCategories: [.. Categories.OrderBy(a => a.Id)]);
        }


        /// <summary>
        /// <para lang="cs">Výsledek validace celé sady klasifikačních pravidel</para>
        /// <para lang="en">Result of the validation for whole set of classification rules</para>
        /// </summary>
        /// <param name="classificationRule">
        /// <para lang="cs">Klasifikační pravidlo</para>
        /// <para lang="en">Classification rule</para>
        /// </param>
        /// <param name="useNegative">
        /// <para lang="cs">Záporné příspěvky lokálních hustot</para>
        /// <para lang="en">Negative local density contributions</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vrací výsledek validace celé sady klasifikačních pravidel</para>
        /// <para lang="en">Returns result of the validation for whole set of classification rules</para>
        /// </returns>
        public TDVwClassificationRuleCheckList GetClassificationRuleCheckList(
            ClassificationRule<TDomain, TCategory> classificationRule,
            bool useNegative)
        {
            if (classificationRule == null)
            {
                return null;
            }

            // Příspěvek lokální hustoty pro vybraný objekt lokální hustoty
            // Pro každný objekt lokální hustoty by měl existovat právě jeden příspěvek lokální hustoty
            // Local density contribution for selected local density object
            // For each local density object should exist only and just one local density contribution
            List<TDLDsity> ldsities =
                SelectedLDsities.Items
                    .Where(a => a.LDsityObjectId == classificationRule.LDsityObject.Id)
                    .Where(a => a.UseNegative == useNegative)
                    .ToList<TDLDsity>();
            TDLDsity ldsity = ldsities.FirstOrDefault();

            if (!LDsityCountIsValid(ldsities: ldsities))
            {
                return null;
            }

            List<ClassificationRule<TDomain, TCategory>> classificationRulesForAllCategories;
            if (!useNegative)
            {
                classificationRulesForAllCategories =
                    ClassificationRulesPositive
                    .Where(a => a.LDsityObject.Id == classificationRule.LDsityObject.Id)
                    .Where(a => a.LDsityObjectForADSP.Id == classificationRule.LDsityObjectForADSP.Id)
                    .ToList<ClassificationRule<TDomain, TCategory>>();
            }
            else
            {
                classificationRulesForAllCategories =
                    ClassificationRulesNegative
                    .Where(a => a.LDsityObject.Id == classificationRule.LDsityObject.Id)
                    .Where(a => a.LDsityObjectForADSP.Id == classificationRule.LDsityObjectForADSP.Id)
                    .ToList<ClassificationRule<TDomain, TCategory>>();
            }

            if (!classificationRulesForAllCategories
                .Select(a => a.IsValid).Aggregate((a, b) => a && b))
            {
                MessageBox.Show(
                       text: msgInvalidClassificationRules,
                       caption: String.Empty,
                       buttons: MessageBoxButtons.OK,
                       icon: MessageBoxIcon.Information);
                return null;
            }

            List<string> paramRules =
                classificationRulesForAllCategories
                    .Select(a => a.Text)
                    .ToList<string>();

            List<List<Nullable<int>>> paramADC =
                    PrepareParamADC(
                        ldsityObjectForADSP: classificationRule.LDsityObjectForADSP);

            List<List<Nullable<int>>> paramSPC =
                PrepareParamSPC(
                    ldsityObjectForADSP: classificationRule.LDsityObjectForADSP);

            TDFnGetPanelRefYearSetTypeList fnPanelRefYearSets =
                TDFunctions.FnGetPanelRefYearSet.Execute(
                    database: Database,
                    panelRefYearSetId: null);

            DataTable dtClassificationRuleChecks =
                TDFnCheckClassificationRulesTypeList.EmptyDataTable();

            // fn_check_classification_rules
            foreach (TDFnGetPanelRefYearSetType fnPanelRefYearSet in fnPanelRefYearSets.Items)
            {
                dtClassificationRuleChecks.Merge(
                    table: TDFunctions.FnCheckClassificationRules.Execute(
                                database: Database,
                                ldsityId: ldsity.Id,
                                ldsityObjectId: classificationRule.LDsityObjectForADSP.Id,
                                rules: paramRules,
                                panelRefYearSetId: fnPanelRefYearSet.Id,
                                adc: paramADC,
                                spc: paramSPC,
                                useNegative: useNegative)
                            .Data);

                if (Database.Postgres.ExceptionFlag)
                {
                    // Přerušení cyklu v případě chyby v SQL příkazu
                    // Breaks loop in case of error in SQL command
                    break;
                }
            }

            if (Setting.Verbose)
            {
                // Výpis posledního příkazu v cyklu
                MessageBox.Show(
                    text: TDFunctions.FnCheckClassificationRules.CommandText,
                    caption: TDFunctions.FnCheckClassificationRules.Name,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            TDFnCheckClassificationRulesTypeList fnClassificationRuleChecks =
                new(
                    database: Database,
                    data: dtClassificationRuleChecks);

            TDVwClassificationRuleCheckList vwClassificationRuleChecks =
                new(database: Database);

            vwClassificationRuleChecks.ReLoad(
                fnPanelRefYearSet: fnPanelRefYearSets,
                fnClassificationRuleCheck: fnClassificationRuleChecks);

            return vwClassificationRuleChecks;
        }


        /// <summary>
        /// <para lang="cs">Uloží novou plošnou doménu nebo subpopulaci, její kategorie, klasifikační pravidla a hierarchie do databáze</para>
        /// <para lang="en">Writes new area domain or subpopulation, its categories, classification rules and hierarchies into database</para>
        /// </summary>
        private bool SaveCategoriesAndRules()
        {
            previousNode = TvwCategory.SelectedNode;
            ValidatePreviousNode();

            // Kategorie plošné domény nebo subpopulace
            if ((Categories == null) ||
                (Categories.Count == 0))
            {
                // "Není definovaná žádná kategorie plošné domény nebo subpopulace pro uložení do databáze."
                MessageBox.Show(
                    text: msgNoCategoryForSave,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Objekty lokálních hustot pro třídění
            if ((LDsityObjectsForADSP == null) ||
                (LDsityObjectsForADSP.Count == 0))
            {
                // "Nejsou zvolené žádné objekty lokálních hustot pro třídění."
                MessageBox.Show(
                    text: msgNoLDsityObjectsForADSPForSave,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Výběr objektů lokálních hustot pro třídění:
            List<TDLDsityObject> includedLDsityObjectsForADSP =
                LDsityObjectsForADSP
                    .Where(a => ctrClassificationRuleCaption.IsIncludedLDsityObjectForADSP(
                        ldsityObjectForADSP: a))
                    .ToList<TDLDsityObject>();

            if ((includedLDsityObjectsForADSP == null) ||
                (includedLDsityObjectsForADSP.Count == 0))
            {
                // "Nejsou zvolené žádné objekty lokálních hustot pro třídění."
                MessageBox.Show(
                    text: msgNoLDsityObjectsForADSPForSave,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Vybraná kladná klasifikační pravidla
            List<ClassificationRule<TDomain, TCategory>> includedClassificationRulesPositive =
                ClassificationRulesPositive
                    .Where(a => includedLDsityObjectsForADSP.Contains(item: a.LDsityObjectForADSP))
                    .ToList<ClassificationRule<TDomain, TCategory>>();

            if ((includedClassificationRulesPositive == null) ||
                (includedClassificationRulesPositive.Count == 0))
            {
                // "Nejsou zvolena žádná kladná klasifikační pravidla pro uložení do databáze."
                MessageBox.Show(
                    text: msgNoClassificationRules,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (!includedClassificationRulesPositive
                    .Select(a => a.IsValid)
                    .Aggregate((a, b) => a && b))
            {
                // "Některé kategorie plošné domény nebo subpopulace mají neplatná kladná klasifikační pravidla."
                MessageBox.Show(
                    text: msgInvalidClassificationRules,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Kontroly kladných klasifikačních pravidel
            Dictionary<int, List<ClassificationRule<TDomain, TCategory>>> positiveRulesForLDsityObjectForADSP = [];

            Dictionary<int, TDVwClassificationRuleCheckList> positiveRuleChecks = [];

            for (int i = 0; i < includedLDsityObjectsForADSP.Count; i++)
            {
                positiveRulesForLDsityObjectForADSP.Add(
                    key: includedLDsityObjectsForADSP[i].Id,
                    value: ClassificationRulesPositive
                        .OrderBy(a => a.Category.Id)
                        .ThenBy(a => a.LDsityObject.Id)
                        .ThenBy(a => a.LDsityObjectForADSP.Id)
                        .Where(a => a.LDsityObjectForADSP.Id == includedLDsityObjectsForADSP[i].Id)
                        .Distinct(comparer: new ClassificationRuleComparer<TDomain, TCategory>())
                        .ToList<ClassificationRule<TDomain, TCategory>>());

                positiveRuleChecks.Add(
                    key: includedLDsityObjectsForADSP[i].Id,
                    value: GetClassificationRuleCheckList(
                        classificationRule:
                            positiveRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                            .FirstOrDefault(),
                        useNegative: false) ??
                        new TDVwClassificationRuleCheckList(database: Database));
            }

            if (!positiveRuleChecks.Values
                .Select(a => a.Items.Where(b => b.Result ?? false).Any())
                .Aggregate((a, b) => a && b))
            {
                MessageBox.Show(
                    text: msgNoSuccesfullyCategorizedPanels,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            // Vybraná záporná klasifikační pravidla
            List<ClassificationRule<TDomain, TCategory>> includedClassificationRulesNegative = [];

            Dictionary<int, List<ClassificationRule<TDomain, TCategory>>> negativeRulesForLDsityObjectForADSP = [];

            Dictionary<int, TDVwClassificationRuleCheckList> negativeRuleChecks = [];

            if (ClassificationType == TDClassificationTypeEnum.ChangeOrDynamic)
            {
                includedClassificationRulesNegative =
                ClassificationRulesNegative
                    .Where(a => includedLDsityObjectsForADSP.Contains(item: a.LDsityObjectForADSP))
                    .ToList<ClassificationRule<TDomain, TCategory>>();

                if ((includedClassificationRulesNegative == null) ||
                    (includedClassificationRulesNegative.Count == 0))
                {
                    // "Nejsou zvolena žádná záporná klasifikační pravidla pro uložení do databáze."
                    MessageBox.Show(
                        text: msgNoClassificationRules,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return false;
                }

                if (!includedClassificationRulesNegative
                        .Select(a => a.IsValid)
                        .Aggregate((a, b) => a && b))
                {
                    // "Některé kategorie plošné domény nebo subpopulace mají neplatná záporná klasifikační pravidla."
                    MessageBox.Show(
                        text: msgInvalidClassificationRules,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return false;
                }

                // Kontroly záporných klasifikačních pravidel
                for (int i = 0; i < includedLDsityObjectsForADSP.Count; i++)
                {
                    negativeRulesForLDsityObjectForADSP.Add(
                        key: includedLDsityObjectsForADSP[i].Id,
                        value: ClassificationRulesNegative
                            .OrderBy(a => a.Category.Id)
                            .ThenBy(a => a.LDsityObject.Id)
                            .ThenBy(a => a.LDsityObjectForADSP.Id)
                            .Where(a => a.LDsityObjectForADSP.Id == includedLDsityObjectsForADSP[i].Id)
                            .Distinct(comparer: new ClassificationRuleComparer<TDomain, TCategory>())
                            .ToList<ClassificationRule<TDomain, TCategory>>());

                    negativeRuleChecks.Add(
                        key: includedLDsityObjectsForADSP[i].Id,
                        value: GetClassificationRuleCheckList(
                            classificationRule:
                                negativeRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                                .FirstOrDefault(),
                            useNegative: true) ??
                            new TDVwClassificationRuleCheckList(database: Database));
                }

                if (!negativeRuleChecks.Values
                    .Select(a => a.Items.Where(b => b.Result ?? false).Any())
                    .Aggregate((a, b) => a && b))
                {
                    MessageBox.Show(
                        text: msgNoSuccesfullyCategorizedPanels,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return false;
                }
            }

            DataTable dtFnSaveCategoriesAndRules = null;
            DataTable dtFnSaveRules = null;

            NpgsqlTransaction transaction =
                   Database.Postgres.BeginTransaction();
            for (int i = 0; i < includedLDsityObjectsForADSP.Count; i++)
            {
                int paramLDsityObjectId = includedLDsityObjectsForADSP[i].Id;
                int paramArealOrPopulationId = (int)ArealOrPopulation;
                int paramClassificationTypeId = (int)ClassificationType;

                string paramLabelCs = Domain.LabelCs;
                string paramDescriptionCs = Domain.DescriptionCs;
                string paramLabelEn = Domain.LabelEn;
                string paramDescriptionEn = Domain.DescriptionEn;

                List<string> paramCategoryLabelCs = Categories
                    .OrderBy(a => a.Id)
                    .Select(a => a.LabelCs)
                    .ToList<string>();

                List<string> paramCategoryDescriptionCs = Categories
                    .OrderBy(a => a.Id)
                    .Select(a => a.DescriptionCs)
                    .ToList<string>();

                List<string> paramCategoryLabelEn = Categories
                    .OrderBy(a => a.Id)
                    .Select(a => a.LabelEn)
                    .ToList<string>();

                List<string> paramCategoryDescriptionEn = Categories
                    .OrderBy(a => a.Id)
                    .Select(a => a.DescriptionEn)
                    .ToList<string>();

                // Klasifikační pravidla:

                List<string> paramRules = [];
                List<Nullable<bool>> paramUseNegative = [];
                List<List<Nullable<int>>> paramRefYearSetIds = [];

                switch (ClassificationType)
                {
                    case TDClassificationTypeEnum.Standard:

                        paramRules =
                            positiveRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                                .Select(a => a.Text)
                                .ToList<string>();

                        paramUseNegative =
                            positiveRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                                .Select(a => (Nullable<bool>)false)
                                .ToList<Nullable<bool>>();

                        paramRefYearSetIds.Add(item:
                            positiveRuleChecks[includedLDsityObjectsForADSP[i].Id].Items
                                .Where(a => a.Result ?? false)
                                .Select(a => (Nullable<int>)a.Id)
                                .ToList<Nullable<int>>());

                        break;

                    case TDClassificationTypeEnum.ChangeOrDynamic:

                        paramRules =
                            positiveRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                                .Select(a => a.Text)
                            .Concat(
                            negativeRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                                .Select(a => a.Text))
                            .ToList<string>();

                        paramUseNegative =
                            positiveRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                                .Select(a => (Nullable<bool>)false)
                            .Concat(
                            negativeRulesForLDsityObjectForADSP[includedLDsityObjectsForADSP[i].Id]
                                .Select(a => (Nullable<bool>)true))
                            .ToList<Nullable<bool>>();

                        paramRefYearSetIds.Add(item:
                            positiveRuleChecks[includedLDsityObjectsForADSP[i].Id].Items
                                .Where(a => a.Result ?? false)
                                .Select(a => (Nullable<int>)a.Id)
                                .ToList<Nullable<int>>());

                        paramRefYearSetIds.Add(item:
                            negativeRuleChecks[includedLDsityObjectsForADSP[i].Id].Items
                                .Where(a => a.Result ?? false)
                                .Select(a => (Nullable<int>)a.Id)
                                .ToList<Nullable<int>>());

                        break;
                }

                // Závislosti:
                // Dependencies:

                List<List<Nullable<int>>> paramADC =
                    PrepareParamADC(
                        ldsityObjectForADSP: includedLDsityObjectsForADSP[i]);

                List<List<Nullable<int>>> paramSPC =
                    PrepareParamSPC(
                        ldsityObjectForADSP: includedLDsityObjectsForADSP[i]);

                // Uložení klasifikačních pravidel pro první objekt lokální hustoty pro třídění:
                // Saving classification rules for first local density object for classification:
                if (i == 0)
                {
                    dtFnSaveCategoriesAndRules =
                        TDFunctions.FnSaveCategoriesAndRules.ExecuteQuery(
                            database: Database,
                            ldsityObjectId: paramLDsityObjectId,
                            arealOrPopulationId: paramArealOrPopulationId,
                            classificationTypeId: paramClassificationTypeId,
                            labelCs: paramLabelCs,
                            descriptionCs: paramDescriptionCs,
                            labelEn: paramLabelEn,
                            descriptionEn: paramDescriptionEn,
                            categoryLabelCs: paramCategoryLabelCs,
                            categoryDescriptionCs: paramCategoryDescriptionCs,
                            categoryLabelEn: paramCategoryLabelEn,
                            categoryDescriptionEn: paramCategoryDescriptionEn,
                            rules: paramRules,
                            useNegative: paramUseNegative,
                            panelRefYearSetIds: paramRefYearSetIds,
                            adc: paramADC,
                            spc: paramSPC,
                            transaction: transaction);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveCategoriesAndRules.CommandText,
                            caption: TDFunctions.FnSaveCategoriesAndRules.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                // Uložení klasifikačních pravidel pro další objekty lokálních hustot pro třídění:
                // Saving classification rules for next local density objects for classification:

                else
                {
                    List<Nullable<int>> paramCategoryIds =
                        (dtFnSaveCategoriesAndRules == null)
                        ? null
                        : (!dtFnSaveCategoriesAndRules.Columns.Contains("category_id"))
                            ? null
                            : dtFnSaveCategoriesAndRules.Rows.OfType<DataRow>()
                        .Select(a =>
                        {
                            if (Int32.TryParse(a["category_id"].ToString(), out int val))
                                return (Nullable<int>)val;
                            else
                                return (Nullable<int>)0;
                        })
                        .Where(a => a != 0)
                        .Distinct()
                        .ToList<Nullable<int>>();

                    if (ClassificationType == TDClassificationTypeEnum.ChangeOrDynamic)
                    {
                        paramCategoryIds.AddRange(collection: paramCategoryIds);
                    }

                    dtFnSaveRules = TDFunctions.FnSaveRules.ExecuteQuery(
                        database: Database,
                        ldsityObjectId: paramLDsityObjectId,
                        arealOrPopulationId: paramArealOrPopulationId,
                        classificationTypeId: paramClassificationTypeId,
                        categoryIds: paramCategoryIds,
                        rules: paramRules,
                        useNegative: paramUseNegative,
                        panelRefYearSetIds: paramRefYearSetIds,
                        adc: paramADC,
                        spc: paramSPC,
                        transaction: transaction);

                    if (Setting.Verbose)
                    {
                        MessageBox.Show(
                            text: TDFunctions.FnSaveRules.CommandText,
                            caption: TDFunctions.FnSaveRules.Name,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }
            }
            transaction?.Commit();

            // Znovu načtení nových uložených plošných domén nebo subpopulací, kategorií,
            // klasifikačních pravidel a hierarchií z databáze:
            // Reload new inserted area domains or subpopulations, categories,
            // classification rules and hierarchies from database :
            LoadContent();

            return true;
        }

        #endregion Methods

    }

}