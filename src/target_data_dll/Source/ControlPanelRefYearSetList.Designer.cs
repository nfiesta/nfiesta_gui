﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleTargetData
{

    partial class ControlPanelRefYearSetList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splIdentifier = new System.Windows.Forms.SplitContainer();
            this.pnlIdentifier = new System.Windows.Forms.Panel();
            this.splPanel = new System.Windows.Forms.SplitContainer();
            this.pnlPanel = new System.Windows.Forms.Panel();
            this.splReferenceYearSet = new System.Windows.Forms.SplitContainer();
            this.pnlReferenceYearSet = new System.Windows.Forms.Panel();
            this.splStatus = new System.Windows.Forms.SplitContainer();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.pnlPlaceHolder = new System.Windows.Forms.Panel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splIdentifier)).BeginInit();
            this.splIdentifier.Panel1.SuspendLayout();
            this.splIdentifier.Panel2.SuspendLayout();
            this.splIdentifier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splPanel)).BeginInit();
            this.splPanel.Panel1.SuspendLayout();
            this.splPanel.Panel2.SuspendLayout();
            this.splPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splReferenceYearSet)).BeginInit();
            this.splReferenceYearSet.Panel1.SuspendLayout();
            this.splReferenceYearSet.Panel2.SuspendLayout();
            this.splReferenceYearSet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splStatus)).BeginInit();
            this.splStatus.Panel1.SuspendLayout();
            this.splStatus.Panel2.SuspendLayout();
            this.splStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Window;
            this.pnlMain.Controls.Add(this.splIdentifier);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(1, 1);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(898, 448);
            this.pnlMain.TabIndex = 0;
            // 
            // splIdentifier
            // 
            this.splIdentifier.BackColor = System.Drawing.SystemColors.Control;
            this.splIdentifier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splIdentifier.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splIdentifier.Location = new System.Drawing.Point(0, 0);
            this.splIdentifier.Margin = new System.Windows.Forms.Padding(0);
            this.splIdentifier.Name = "splIdentifier";
            // 
            // splIdentifier.Panel1
            // 
            this.splIdentifier.Panel1.Controls.Add(this.pnlIdentifier);
            // 
            // splIdentifier.Panel2
            // 
            this.splIdentifier.Panel2.Controls.Add(this.splPanel);
            this.splIdentifier.Size = new System.Drawing.Size(898, 448);
            this.splIdentifier.SplitterWidth = 1;
            this.splIdentifier.TabIndex = 0;
            // 
            // pnlIdentifier
            // 
            this.pnlIdentifier.BackColor = System.Drawing.SystemColors.Control;
            this.pnlIdentifier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlIdentifier.Location = new System.Drawing.Point(0, 0);
            this.pnlIdentifier.Margin = new System.Windows.Forms.Padding(0);
            this.pnlIdentifier.Name = "pnlIdentifier";
            this.pnlIdentifier.Size = new System.Drawing.Size(50, 448);
            this.pnlIdentifier.TabIndex = 0;
            // 
            // splPanel
            // 
            this.splPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splPanel.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splPanel.Location = new System.Drawing.Point(0, 0);
            this.splPanel.Margin = new System.Windows.Forms.Padding(0);
            this.splPanel.Name = "splPanel";
            // 
            // splPanel.Panel1
            // 
            this.splPanel.Panel1.Controls.Add(this.pnlPanel);
            // 
            // splPanel.Panel2
            // 
            this.splPanel.Panel2.Controls.Add(this.splReferenceYearSet);
            this.splPanel.Size = new System.Drawing.Size(847, 448);
            this.splPanel.SplitterDistance = 200;
            this.splPanel.SplitterWidth = 1;
            this.splPanel.TabIndex = 1;
            // 
            // pnlPanel
            // 
            this.pnlPanel.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPanel.Location = new System.Drawing.Point(0, 0);
            this.pnlPanel.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPanel.Name = "pnlPanel";
            this.pnlPanel.Size = new System.Drawing.Size(200, 448);
            this.pnlPanel.TabIndex = 1;
            // 
            // splReferenceYearSet
            // 
            this.splReferenceYearSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splReferenceYearSet.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splReferenceYearSet.Location = new System.Drawing.Point(0, 0);
            this.splReferenceYearSet.Margin = new System.Windows.Forms.Padding(0);
            this.splReferenceYearSet.Name = "splReferenceYearSet";
            // 
            // splReferenceYearSet.Panel1
            // 
            this.splReferenceYearSet.Panel1.Controls.Add(this.pnlReferenceYearSet);
            // 
            // splReferenceYearSet.Panel2
            // 
            this.splReferenceYearSet.Panel2.Controls.Add(this.splStatus);
            this.splReferenceYearSet.Size = new System.Drawing.Size(646, 448);
            this.splReferenceYearSet.SplitterDistance = 100;
            this.splReferenceYearSet.SplitterWidth = 1;
            this.splReferenceYearSet.TabIndex = 1;
            // 
            // pnlReferenceYearSet
            // 
            this.pnlReferenceYearSet.BackColor = System.Drawing.SystemColors.Control;
            this.pnlReferenceYearSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlReferenceYearSet.Location = new System.Drawing.Point(0, 0);
            this.pnlReferenceYearSet.Margin = new System.Windows.Forms.Padding(0);
            this.pnlReferenceYearSet.Name = "pnlReferenceYearSet";
            this.pnlReferenceYearSet.Size = new System.Drawing.Size(100, 448);
            this.pnlReferenceYearSet.TabIndex = 1;
            // 
            // splStatus
            // 
            this.splStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splStatus.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splStatus.Location = new System.Drawing.Point(0, 0);
            this.splStatus.Margin = new System.Windows.Forms.Padding(0);
            this.splStatus.Name = "splStatus";
            // 
            // splStatus.Panel1
            // 
            this.splStatus.Panel1.Controls.Add(this.pnlStatus);
            // 
            // splStatus.Panel2
            // 
            this.splStatus.Panel2.Controls.Add(this.pnlPlaceHolder);
            this.splStatus.Panel2Collapsed = true;
            this.splStatus.Size = new System.Drawing.Size(545, 448);
            this.splStatus.SplitterDistance = 100;
            this.splStatus.SplitterWidth = 1;
            this.splStatus.TabIndex = 2;
            // 
            // pnlStatus
            // 
            this.pnlStatus.BackColor = System.Drawing.SystemColors.Control;
            this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStatus.Location = new System.Drawing.Point(0, 0);
            this.pnlStatus.Margin = new System.Windows.Forms.Padding(0);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(545, 448);
            this.pnlStatus.TabIndex = 1;
            // 
            // pnlPlaceHolder
            // 
            this.pnlPlaceHolder.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPlaceHolder.Location = new System.Drawing.Point(0, 0);
            this.pnlPlaceHolder.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPlaceHolder.Name = "pnlPlaceHolder";
            this.pnlPlaceHolder.Size = new System.Drawing.Size(494, 448);
            this.pnlPlaceHolder.TabIndex = 2;
            // 
            // toolTip
            // 
            this.toolTip.ShowAlways = true;
            // 
            // ControlPanelRefYearSetList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlPanelRefYearSetList";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(900, 450);
            this.pnlMain.ResumeLayout(false);
            this.splIdentifier.Panel1.ResumeLayout(false);
            this.splIdentifier.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splIdentifier)).EndInit();
            this.splIdentifier.ResumeLayout(false);
            this.splPanel.Panel1.ResumeLayout(false);
            this.splPanel.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splPanel)).EndInit();
            this.splPanel.ResumeLayout(false);
            this.splReferenceYearSet.Panel1.ResumeLayout(false);
            this.splReferenceYearSet.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splReferenceYearSet)).EndInit();
            this.splReferenceYearSet.ResumeLayout(false);
            this.splStatus.Panel1.ResumeLayout(false);
            this.splStatus.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splStatus)).EndInit();
            this.splStatus.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splIdentifier;
        private System.Windows.Forms.SplitContainer splPanel;
        private System.Windows.Forms.SplitContainer splReferenceYearSet;
        private System.Windows.Forms.SplitContainer splStatus;
        private System.Windows.Forms.Panel pnlPlaceHolder;
        public System.Windows.Forms.Panel pnlIdentifier;
        public System.Windows.Forms.Panel pnlPanel;
        public System.Windows.Forms.Panel pnlReferenceYearSet;
        public System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.ToolTip toolTip;
    }

}
