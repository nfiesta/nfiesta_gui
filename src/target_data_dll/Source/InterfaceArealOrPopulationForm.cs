﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;

namespace ZaJi
{
    namespace ModuleTargetData
    {

        /// <summary>
        /// <para lang="cs">Rozhraní formulářů pro editace klasifikačních pravidel</para>
        /// <para lang="en">Interface of the forms for classification rule editing</para>
        /// </summary>
        /// <typeparam name="TDomain">
        /// <para lang="cs">Typ pro plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain or subpopulation type</para>
        /// </typeparam>
        /// <typeparam name="TCategory">
        /// <para lang="cs">Typ pro kategorii plošné domény nebo subpopulace</para>
        /// <para lang="en"> Area domain category or subpopulation category type</para>
        /// </typeparam>
        internal interface IArealOrPopulationForm<TDomain, TCategory>
            where TDomain : IArealOrPopulationDomain
            where TCategory : IArealOrPopulationCategory
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Klasifikační typ (read-only)</para>
            /// <para lang="en">Classification type (read-only)</para>
            /// </summary>
            TDClassificationTypeEnum ClassificationType { get; }


            /// <summary>
            /// <para lang="cs">Plošná doména nebo subpopulace</para>
            /// <para lang="en">Area domain or subpopulation</para>
            /// </summary>
            TDomain Domain { get; set; }


            /// <summary>
            /// <para lang="cs">Kategorie plošné domény nebo subpopulace (read-only)</para>
            /// <para lang="en">Categories of the area domain or subpopulation (read-only)</para>
            /// </summary>
            List<TCategory> Categories { get; }

            /// <summary>
            /// <para lang="cs">Objekty lokálních hustot pro třídění (read-only)</para>
            /// <para lang="en">Local density objects for classification (read-only)</para>
            /// </summary>
            List<TDLDsityObject> LDsityObjectsForADSP { get; }

            /// <summary>
            /// <para lang="cs">Páry klasifikačních pravidel (read-only)</para>
            /// <para lang="en">Classification rule pairs (read-only)</para>
            /// </summary>
            List<ClassificationRulePair<TDomain, TCategory>> ClassificationRulePairs { get; }

            /// <summary>
            /// <para lang="cs">Klasifikační pravidla kategorií plošných domén nebo subpopulací
            /// pro kladné příspěvky lokálních hustot(read-only)</para>
            /// <para lang="en">Classification rules for area domain or subpopulation categories
            /// for positive local density contributions(read-only)</para>
            /// </summary>
            List<ClassificationRule<TDomain, TCategory>> ClassificationRulesPositive { get; }

            /// <summary>
            /// <para lang="cs">Klasifikační pravidla kategorií plošných domén nebo subpopulací
            /// pro záporné příspěvky lokálních hustot(read-only)</para>
            /// <para lang="en">Classification rules for area domain or subpopulation categories
            /// for negative local density contributions(read-only)</para>
            /// </summary>
            List<ClassificationRule<TDomain, TCategory>> ClassificationRulesNegative { get; }


            /// <summary>
            /// <para lang="cs">Vybraný pár klasifikačních pravidel (read-only)</para>
            /// <para lang="en">Selected classification rule pair (read-only)</para>
            /// </summary>
            ClassificationRulePair<TDomain, TCategory> SelectedClassificationRulePair { get; }

            /// <summary>
            /// <para lang="cs">Předcházející pár klasifikačních pravidel (read-only)</para>
            /// <para lang="en">Previous classification rule pair (read-only)</para>
            /// </summary>
            ClassificationRulePair<TDomain, TCategory> PreviousClassificationRulePair { get; }

            /// <summary>
            /// <para lang="cs">Je aktuální objekt lokální hustoty pro třídění
            /// zahrnutý mezi objekty pro uložení do databáze? (read-only)</para>
            /// <para lang="en">Is current local density object for classification
            /// included among objects for saving in database? (read-only)</para>
            /// </summary>
            bool SelectedLDsityObjectForADSPIncluded { get; }


            /// <summary>
            /// <para lang="cs">Vybrané kladné klasifikační pravidlo (read-only)</para>
            /// <para lang="en">Selected positive classification rule (read-only)</para>
            /// </summary>
            ClassificationRule<TDomain, TCategory> SelectedClassificationRulePositive { get; }

            /// <summary>
            /// <para lang="cs">Předcházející kladné klasifikační pravidlo (read-only)</para>
            /// <para lang="en">Previous positive classification rule (read-only)</para>
            /// </summary>
            ClassificationRule<TDomain, TCategory> PreviousClassificationRulePositive { get; }


            /// <summary>
            /// <para lang="cs">Vybrané záporné klasifikační pravidlo (read-only)</para>
            /// <para lang="en">Selected negative classification rule (read-only)</para>
            /// </summary>
            ClassificationRule<TDomain, TCategory> SelectedClassificationRuleNegative { get; }

            /// <summary>
            /// <para lang="cs">Předcházející záporné klasifikační pravidlo (read-only)</para>
            /// <para lang="en">Previous negative classification rule (read-only)</para>
            /// </summary>
            ClassificationRule<TDomain, TCategory> PreviousClassificationRuleNegative { get; }


            /// <summary>
            /// <para lang="cs">Editace klasifikačních pravidel není povolena</para>
            /// <para lang="en">Changes in classification rules are not allowed</para>
            /// </summary>
            bool ReadOnlyMode { get; }


            /// <summary>
            /// <para lang="cs">Objekty lokálních hustot ve vybrané skupině
            /// (vybrané na 1.formuláři) (read-only)</para>
            /// <para lang="en">Local density objects in the selected group
            /// (selected on the 1st form) (read-only)</para>
            /// </summary>
            TDLDsityObjectList SelectedLocalDensityObjects { get; }

            /// <summary>
            /// <para lang="cs">Vybrané příspěvky lokálních hustot
            /// pro vybranou skupinu cílových proměnných
            /// (vybrané na 2.formuláři) (read-only)</para>
            /// <para lang="en">Selected local density contributions
            /// for the selected target variable group
            /// (selected on the 2nd form) (read-only)</para>
            /// </summary>
            TDLDsityList SelectedLDsities { get; }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Kontroluje počet vybraných příspěvků lokálních hustot</para>
            /// <para lang="en">Method checks number of selected local density contributions</para>
            /// </summary>
            /// <param name="ldsities">
            /// <para lang="cs">Seznam vybraných příspěvků lokálních hustot</para>
            /// <para lang="en">List of selected local density contributions</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrácí true pokud existuje právě jeden</para>
            /// <para lang="en">Returns true when just on local density contribution exists</para>
            /// </returns>
            bool LDsityCountIsValid(List<TDLDsity> ldsities);

            /// <summary>
            /// <para lang="cs">
            /// Připraví vstupní argument pro uložené procedury,
            /// který obsahuje popis závislostí mezi kategoriemi plošných domén
            /// </para>
            /// <para lang="en">
            /// Prepares the input argument for the stored procedures,
            /// which contains a description of the dependencies between the area domain categories
            /// </para>
            /// </summary>
            /// <param name="ldsityObjectForADSP">
            /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
            /// <para lang="en">Local density object used for classification</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">
            /// Vrací pole polí,
            /// vnitřní pole jsou pro jednotivé podřízené kategorie
            /// a obsahují seznam identifikátorů svých nadřízených kategorií
            /// </para>
            /// <para lang="en">
            /// Returns an array of arrays,
            /// the internal arrays are for each inferior area domain category
            /// and contain a list of identifiers of their superior area domain categories</para>
            /// </returns>
            List<List<Nullable<int>>> PrepareParamADC(TDLDsityObject ldsityObjectForADSP);

            /// <summary>
            /// <para lang="cs">
            /// Připraví vstupní argument pro uložené procedury,
            /// který obsahuje popis závislostí mezi kategoriemi subpopulací
            /// </para>
            /// <para lang="en">
            /// Prepares the input argument for the stored procedures,
            /// which contains a description of the dependencies between the subpopulation categories
            /// </para>
            /// </summary>
            /// <param name="ldsityObjectForADSP">
            /// <para lang="cs">Objekt lokální hustoty sloužící ke třídění</para>
            /// <para lang="en">Local density object used for classification</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">
            /// Vrací pole polí,
            /// vnitřní pole jsou pro jednotivé podřízené kategorie
            /// a obsahují seznam identifikátorů svých nadřízených kategorií
            /// </para>
            /// <para lang="en">
            /// Returns an array of arrays,
            /// the internal arrays are for each inferior subpopulation category
            /// and contain a list of identifiers of their superior subpopulation categories</para>
            /// </returns>
            List<List<Nullable<int>>> PrepareParamSPC(TDLDsityObject ldsityObjectForADSP);

            /// <summary>
            /// <para lang="cs">Výsledek validace celé sady klasifikačních pravidel</para>
            /// <para lang="en">Result of the validation for whole set of classification rules</para>
            /// </summary>
            /// <param name="classificationRule">
            /// <para lang="cs">Klasifikační pravidlo</para>
            /// <para lang="en">Classification rule</para>
            /// </param>
            /// <param name="useNegative">
            /// <para lang="cs">Záporné příspěvky lokálních hustot</para>
            /// <para lang="en">Negative local density contributions</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací výsledek validace celé sady klasifikačních pravidel</para>
            /// <para lang="en">Returns result of the validation for whole set of classification rules</para>
            /// </returns>
            TDVwClassificationRuleCheckList GetClassificationRuleCheckList(
                ClassificationRule<TDomain, TCategory> classificationRule,
                bool useNegative);

            #endregion Methods

        }

    }
}
