﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;


namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba kombinací panelů a refyearsetů"</para>
    /// <para lang="en">Control "Select panel and refyearset combinations"</para>
    /// </summary>
    internal partial class ControlPanelRefyearset
           : UserControl, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Téma"</para>
        /// <para lang="en">Control "Topic"</para>
        /// </summary>
        private ControlTopic controlTopic;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Kontrola atributových proměnných"</para>
        /// <para lang="en">Control "Checking attribute variables"</para>
        /// </summary>
        private ControlAttributeVariables controlAttributeVariables;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL subpopulace a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of subpopulation and its catgories"</para>
        /// </summary>
        private ControlSubPopulation controlSubPopulation;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL plošné domény a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of area domain and its catgories"</para>
        /// </summary>
        private ControlAreaDomain controlAreaDomain;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
        /// <para lang="en">Selection of the target variable"</para>
        /// </summary>
        private ControlTargetVariable controlTargetVariable;

        /// <summary>
        /// <para lang="cs">Id vrácené funkcí fn_etl_save_target_variable, volané v ControlTargetVariable. Je uloženo v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id returned by the fn_etl_save_target_variable function called in the ControlTargetVariable. It is saved in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        private readonly int? _idEtlFromTargetVariable;

        /// <summary>
        /// <para lang="cs">Id vrácené funkcí fn_etl_save_target_variable, volané v ControlTargetVariableCompare. Je uloženo v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id returned by the fn_etl_save_target_variable function called in the ControlTargetVariableCompare. It is saved in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        private readonly int? _idEtlFromTargetVariableCompare;

        /// <summary>
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        private readonly int? _idEtl;

        /// <summary>
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů, který vrací fce fn_etl_get_target_variable</para>
        /// <para lang="en">List of panel and refyearset combination ids returned by fn_etl_get_target_variable</para>
        /// </summary>
        private readonly List<int?> _refYearSetToPanelMapping;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Atomické plošné domény</para>
        /// <para lang="en">Atomic area domains</para>
        /// </summary>
        private readonly bool? _atomicAreaDomains;

        /// <summary>
        /// <para lang="cs">Atomické subpopulace</para>
        /// <para lang="en">Atomic subpopulations</para>
        /// </summary>
        private readonly bool? _atomicSubPopulations;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelCs;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelEn;

        /// <summary>
        /// <para lang="cs">Výsledek volání funkce FnEtlGetAreaDomainJson</para>
        /// <para lang="en">A result of calling the FnEtlGetAreaDomainJson function</para>
        /// </summary>
        private string areaDomains;

        /// <summary>
        /// <para lang="cs">Výsledek volání funkce FnEtlGetSubPopulationJson</para>
        /// <para lang="en">A result of calling the FnEtlGetSubPopulationJson function</para>
        /// </summary>
        private string subPopulations;

        /// <summary>
        /// <para lang="cs">String jako výsledek volání funkce FnEtlExportAreaDomainCategoryMapping</para>
        /// <para lang="en">String as a result of calling the FnEtlExportAreaDomainCategoryMapping function</para>
        /// </summary>
        private string adcMapping;

        /// <summary>
        /// <para lang="cs">String jako výsledek volání funkce FnEtlExportSubPopulationCategoryMapping</para>
        /// <para lang="en">String as a result of calling the FnEtlExportSubPopulationCategoryMapping function</para>
        /// </summary>
        private string spcMapping;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="idEtlFromTargetVariable">
        /// <para lang="cs">Id vrácené funkcí fn_etl_save_target_variable, volané v ControlTargetVariable. Je uloženo v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id returned by the fn_etl_save_target_variable function called in the ControlTargetVariable. It is saved in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="idEtlFromTargetVariableCompare">
        /// <para lang="cs">Id vrácené funkcí fn_etl_save_target_variable, volané v ControlTargetVariableCompare. Je uloženo v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id returned by the fn_etl_save_target_variable function called in the ControlTargetVariableCompare. It is saved in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="idEtl">
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="refYearSetToPanelMapping">
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů, který vrací fce fn_etl_get_target_variable</para>
        /// <para lang="en">List of panel and refyearset combination ids returned by fn_etl_get_target_variable</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="atomicAreaDomains">
        /// <para lang="cs">Atomické plošné domény</para>
        /// <para lang="en">Atomic aread domains</para>
        /// </param>
        /// <param name="atomicSubPopulations">
        /// <para lang="cs">Atomické subpopulace</para>
        /// <para lang="en">Atomic subpopulations</para>
        /// </param>
        /// <param name="selectedIndicatorLabelCs">
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </param>
        /// <param name="selectedIndicatorLabelEn">
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public ControlPanelRefyearset(
            Control controlOwner,
            int? idEtlFromTargetVariable,
            int? idEtlFromTargetVariableCompare,
            int? idEtl,
            List<int?> refYearSetToPanelMapping,
            int selectedExportConnectionId,
            bool? atomicAreaDomains,
            bool? atomicSubPopulations,
            string selectedIndicatorLabelCs,
            string selectedIndicatorLabelEn,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction
            )
        {
            InitializeComponent();

            _idEtlFromTargetVariable = idEtlFromTargetVariable;
            _idEtlFromTargetVariableCompare = idEtlFromTargetVariableCompare;
            _idEtl = idEtl;
            _refYearSetToPanelMapping = refYearSetToPanelMapping;
            _selectedExportConnectionId = selectedExportConnectionId;
            _atomicAreaDomains = atomicAreaDomains;
            _atomicSubPopulations = atomicSubPopulations;
            _selectedIndicatorLabelCs = selectedIndicatorLabelCs;
            _selectedIndicatorLabelEn = selectedIndicatorLabelEn;
            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;

            Initialize(controlOwner: controlOwner);

            ControlEtl.LanguageChanged += OnLanguageChanged;
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                //return ((INfiEstaControl)ControlOwner).LanguageVersion;
                return CtrEtl.LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlEtl ctrEtl)
                {
                    return ctrEtl;
                }

                else if (ControlOwner is ControlTargetVariable ctrTargetVariable)
                {
                    return ctrTargetVariable.CtrEtl;
                }

                else if (ControlOwner is FormCheckMetadata frmCheckMetadata)
                {
                    return frmCheckMetadata.CtrEtl;
                }

                else if (ControlOwner is ControlTargetVariableCompare ctrTargetVariableCompare)
                {
                    return ctrTargetVariableCompare.CtrEtl;
                }

                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Id z tabulky target_data.t_etl_target_variable</para>
        /// <para lang="en">Id from the target_data.t_etl_target_variable table</para>
        /// </summary>
        public int? EtlId { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolené kombinace panelů a roků měření</para>
        /// <para lang="en">Selected combinations of panels and reference-yearsets</para>
        /// </summary>
        public List<int?> SelectedRefyearsetsToPanels { get; set; }

        /// <summary>
        /// <para lang="cs">Označení, zda byla instance třídy vytvořena z ControlTargetVariableCompare</para>
        /// <para lang="en">Indicates whether the instance was created from ControlTargetVariableCompare</para>
        /// </summary>
        public bool FromTargetVariableCompare { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                 "Volba kombinací panelů a roků měření" },
                            { "grpPanelsRefyearsets",           "Kombinace dostupných panelů a roků měření:" },
                            { "btnSelectAll",                   "vybrat/odebrat vše"},
                            { "btnNext",                        "Další" },
                            { "btnPrevious",                    "Zpět na výběr cílové proměnné" },
                            { "msgNoCombinationSelected",       "Není vybrána žádná kombinace panelů a roků měření." },
                            { "yes",                            "ano"},
                            { "no",                             "ne"},
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlPanelRefyearset") ?
                    languageFile.NationalVersion.Data["ControlPanelRefyearset"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                 "Select panel and reference-yearset combinations" },
                            { "grpPanelsRefyearsets",           "Available panel and reference-yearset combinations:" },
                            { "btnSelectAll",                   "select/deselect all"},
                            { "btnNext",                        "Next" },
                            { "btnPrevious",                    "Back to target variable selection" },
                            { "msgNoCombinationSelected",       "No combination of panels and reference-yearsets is selected." },
                            { "yes",                            "yes"},
                            { "no",                             "no"},
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlPanelRefyearset") ?
                    languageFile.InternationalVersion.Data["ControlPanelRefyearset"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            grpPanelsRefyearsets.Text =
                labels.ContainsKey(key: "grpPanelsRefyearsets") ?
                labels["grpPanelsRefyearsets"] : String.Empty;

            btnSelectAll.Text =
                labels.ContainsKey(key: "btnSelectAll") ?
                labels["btnSelectAll"] : String.Empty;

            btnNext.Text =
               labels.ContainsKey(key: "btnNext") ?
               labels["btnNext"] : String.Empty;

            btnPrevious.Text =
               labels.ContainsKey(key: "btnPrevious") ?
               labels["btnPrevious"] : String.Empty;

            LoadContent();

            controlTopic?.InitializeLabels();
            controlAttributeVariables?.InitializeLabels();
            controlSubPopulation?.InitializeLabels();
            controlAreaDomain?.InitializeLabels();
            controlTargetVariable?.InitializeLabels();

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            TDFnEtlGetPanelRefYearSetCombinationsTypeList result = null;

            if (_idEtlFromTargetVariable != null)
            {
                result = TDFunctions.FnEtlGetPanelRefYearSetCombinations.Execute(
                    database: Database,
                    idTEtlTargetVariable: _idEtlFromTargetVariable,
                    refYearSetToPanelMapping: null,
                    refYearSetToPanelMappingModule: null,
                    transaction: SourceTransaction);
            }
            else if (_idEtlFromTargetVariableCompare != null)
            {
                result = TDFunctions.FnEtlGetPanelRefYearSetCombinations.Execute(
                    database: Database,
                    idTEtlTargetVariable: _idEtlFromTargetVariableCompare,
                    refYearSetToPanelMapping: null,
                    refYearSetToPanelMappingModule: null,
                    transaction: SourceTransaction);
            }
            else if (_idEtl != null)
            {
                result = TDFunctions.FnEtlGetPanelRefYearSetCombinations.Execute(
                    database: Database,
                    idTEtlTargetVariable: _idEtl,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    refYearSetToPanelMappingModule: null,
                    transaction: SourceTransaction);
            }

            if (result != null)
            {
                TDFnEtlGetPanelRefYearSetCombinationsTypeList.SetColumnsVisibility(
                    (from a in TDFnEtlGetPanelRefYearSetCombinationsTypeList.Cols
                     orderby a.Value.DisplayIndex
                     select a.Value.Name).ToArray<string>());

                dgvPanelsRefyearsets.DataSource = result.Data;

                result.FormatDataGridView(dgvPanelsRefyearsets);

                dgvPanelsRefyearsets.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.AllCells;

                dgvPanelsRefyearsets.Columns["refyearset2panel"].Visible = false;

                //insert the checkedboxColumn in the dataGridView
                bool selectColumnExists = false;
                foreach (DataGridViewColumn column in dgvPanelsRefyearsets.Columns)
                {
                    if (column.Name == "Select" && column is DataGridViewCheckBoxColumn) //because it appeared more times
                    {
                        selectColumnExists = true;
                        break;
                    }
                }

                if (!selectColumnExists)
                {
                    DataGridViewCheckBoxColumn checkboxColumn = new DataGridViewCheckBoxColumn()
                    {
                        Name = "Select",
                        HeaderText = "",
                        Width = 50
                    };

                    dgvPanelsRefyearsets.Columns.Insert(0, checkboxColumn); //insert the column at the first position
                }
                else
                {
                    dgvPanelsRefyearsets.Columns["Select"].DisplayIndex = 0; //move the existing "Select" column to the first position
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ControlAttributeVariables nebo ControlTopic</para>
        /// <para lang="en">ControlAttributeVariables or ControlTopic is shown</para>
        /// </summary>
        private void ShowControlAttributeVariablesOrTopic()
        {
            string variablesSource =
                TDFunctions.FnEtlGetVariables.Execute(
                    database: Database,
                    refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                    idTEtlTargetVariable: EtlId,
                    transaction: SourceTransaction);

            string variablesTarget =
                NfiEstaFunctions.FnEtlCheckVariables.ExecuteScalar(
                    database: FormUserPassword.TargetDatabase,
                    variables: variablesSource,
                    TargetTransaction);

            if (string.IsNullOrEmpty(variablesTarget))
            {
                controlTopic = new ControlTopic(
                    controlOwner: this,
                    idEtl: EtlId,
                    refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                    selectedExportConnectionId: _selectedExportConnectionId,
                    selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                    sourceTransaction: SourceTransaction,
                    targetTransaction: TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                controlTopic.LoadContent();
                controlTopic.InitializeLabels();

                CtrEtl?.ShowControl(control: controlTopic);
            }

            else
            {
                controlAttributeVariables = new ControlAttributeVariables(
                    controlOwner: this,
                    idEtl: EtlId,
                    refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                    selectedExportConnectionId: _selectedExportConnectionId,
                    selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                    sourceTransaction: SourceTransaction,
                    targetTransaction: TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                controlAttributeVariables.LoadContent();
                controlAttributeVariables.InitializeLabels();

                CtrEtl?.ShowControl(control: controlAttributeVariables);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí zprávu msgNoCombinationSelected</para>
        /// <para lang="en">Message msgNoCombinationSelected is shown</para>
        /// </summary>
        private void ShowMessageNoCombinationSelected()
        {
            Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

            MessageBox.Show(
                text: messages.ContainsKey(key: "msgNoCombinationSelected") ?
                        messages["msgNoCombinationSelected"] : String.Empty,
                caption: String.Empty,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ControlSubPopulation</para>
        /// <para lang="en">ControlSubPopulation is shown</para>
        /// </summary>
        private void ShowControlSubPopulation()
        {
            controlSubPopulation = new ControlSubPopulation(
                            controlOwner: this,
                            idEtl: EtlId,
                            refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                            selectedExportConnectionId: _selectedExportConnectionId,
                            atomicAreaDomains: _atomicAreaDomains,
                            atomicSubPopulations: _atomicSubPopulations,
                            selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                            selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                            sourceTransaction: SourceTransaction,
                            targetTransaction: TargetTransaction)
            {
                Dock = DockStyle.Fill
            };

            controlSubPopulation.LoadContent();
            controlSubPopulation.InitializeLabels();

            CtrEtl?.ShowControl(control: controlSubPopulation);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ControlAreaDomain</para>
        /// <para lang="en">ControlAreaDomain is shown</para>
        /// </summary>
        private void ShowControlAreaDomain()
        {
            controlAreaDomain = new ControlAreaDomain(
                            controlOwner: this,
                            idEtl: EtlId,
                            refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                            selectedExportConnectionId: _selectedExportConnectionId,
                            atomicAreaDomains: _atomicAreaDomains,
                            atomicSubPopulations: _atomicSubPopulations,
                            selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                            selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                            sourceTransaction: SourceTransaction,
                            targetTransaction: TargetTransaction)
            {
                Dock = DockStyle.Fill
            };

            controlAreaDomain.LoadContent();
            controlAreaDomain.InitializeLabels();

            CtrEtl?.ShowControl(control: controlAreaDomain);
        }

        /// <summary>
        /// <para lang="cs">Provede se FnEtlCheckAreaDomain a FnEtlCheckSubPopulation</para>
        /// <para lang="en">FnEtlCheckAreaDomain and FnEtlCheckSubPopulation are executed</para>
        /// </summary>
        private void ExecuteFnEtlGetAreaDomainJsonAndFnEtlGetSubPopulationJson()
        {
            areaDomains = TDFunctions.FnEtlGetAreaDomainJson.Execute(
                                    database: Database,
                                    refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                                    idTEtlTargetVariable: EtlId,
                                    etl: false,
                                    transaction: SourceTransaction);

            subPopulations = TDFunctions.FnEtlGetSubPopulationJson.Execute(
                                        database: Database,
                                        refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                                        idTEtlTargetVariable: EtlId,
                                        etl: false,
                                        transaction: SourceTransaction);
        }

        /// <summary>
        /// <para lang="cs">Provede se FnEtlExportAreaDomainCategoryMapping</para>
        /// <para lang="en">FnEtlExportAreaDomainCategoryMapping is executed</para>
        /// </summary>
        private void ExecuteFnEtlExportAreaDomainCategoryMapping()
        {
            adcMapping = TDFunctions.FnEtlExportAreaDomainCategoryMapping.Execute(
                            database: Database,
                            refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                            idTEtlTargetVariable: EtlId,
                            transaction: SourceTransaction);
        }

        /// <summary>
        /// <para lang="cs">Provede se ExecuteFnEtlExportSubPopulationCategoryMapping</para>
        /// <para lang="en">ExecuteFnEtlExportSubPopulationCategoryMapping is executed</para>
        /// </summary>
        private void ExecuteFnEtlExportSubPopulationCategoryMapping()
        {
            spcMapping = TDFunctions.FnEtlExportSubPopulationCategoryMapping.Execute(
                            database: Database,
                            refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                            idTEtlTargetVariable: EtlId,
                            transaction: SourceTransaction);
        }

        /// <summary>
        /// <para lang="cs">Provede se FnEtlImportAreaDomainCategoryMapping</para>
        /// <para lang="en">FnEtlImportAreaDomainCategoryMapping is executed</para>
        /// </summary>
        private void ExecuteFnEtlImportAreaDomainCategoryMapping()
        {
            NfiEstaFunctions.FnEtlImportAreaDomainCategoryMapping.Execute(
                                database: FormUserPassword.TargetDatabase,
                                adcMapping: adcMapping,
                                TargetTransaction);
        }

        /// <summary>
        /// <para lang="cs">Provede se FnEtlImportSubPopulationCategoryMapping</para>
        /// <para lang="en">FnEtlImportSubPopulationCategoryMapping is executed</para>
        /// </summary>
        private void ExecuteFnEtlImportSubPopulationCategoryMapping()
        {
            NfiEstaFunctions.FnEtlImportSubPopulationCategoryMapping.Execute(
                                database: FormUserPassword.TargetDatabase,
                                spcMapping: spcMapping,
                                TargetTransaction);
        }

        /// <summary>
        /// <para lang="cs">Provede se oddíl 10 Mapování kategorií plošných domén a subpopulací schématu</para>
        /// <para lang="en">Section 10 Area domain category nad subpopulation category mapping of schema is executed</para>
        /// </summary>
        private void ExecuteSection10AreaDomainCategoryAndSubPopulationCategoryMappingOfSchema()
        {
            // section 10 Area domain category and subpopulation category mapping of schema

            ExecuteFnEtlExportAreaDomainCategoryMapping();

            if (!string.IsNullOrEmpty(adcMapping))
            {
                // adcMapping is not empty

                ExecuteFnEtlImportAreaDomainCategoryMapping();

                ExecuteFnEtlExportSubPopulationCategoryMapping();

                //spcMapping is not empty
                if (!string.IsNullOrEmpty(spcMapping))
                {
                    ExecuteFnEtlImportSubPopulationCategoryMapping();

                    ShowControlAttributeVariablesOrTopic();
                }
                else
                {
                    //spcMapping is null or empty

                    ShowControlAttributeVariablesOrTopic();
                }
            }
            else
            {
                // adcMapping is null or empty

                ExecuteFnEtlExportSubPopulationCategoryMapping();

                //spcMapping is not empty
                if (!string.IsNullOrEmpty(spcMapping))
                {
                    ExecuteFnEtlImportSubPopulationCategoryMapping();

                    ShowControlAttributeVariablesOrTopic();
                }
                else
                {
                    //spcMapping is null or empty

                    ShowControlAttributeVariablesOrTopic();
                }
            }
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události překreslení zobrazení buněk v DataGridView dgvPanelsRefyearsets</para>
        /// <para lang="en">Event handler of the cell view redraw in DataGridView dgvPanelsRefyearsets</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvPanelsRefyearsets_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //change the standard display of boolean results in the passed_by_module column to text
            if (e.ColumnIndex == dgvPanelsRefyearsets.Columns["passed_by_module"].Index && e.RowIndex >= 0)
            {
                e.PaintBackground(e.CellBounds, false);

                Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                bool passedByModule = (bool)e.Value;
                string text =
                    (LanguageVersion == LanguageVersion.National && passedByModule) ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty
                  : (LanguageVersion == LanguageVersion.National && !passedByModule) ? labels.ContainsKey(key: "no") ? labels["no"] : String.Empty
                  : passedByModule ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty : labels.ContainsKey(key: "no") ? labels["no"] : String.Empty;

                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    //set the font to Microsoft Sans Serif, 9.75pt
                    e.CellStyle.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular);

                    //move the text lower by 3 pixels
                    Rectangle textRect = new Rectangle(e.CellBounds.X, e.CellBounds.Y + 3,
                                                        e.CellBounds.Width, e.CellBounds.Height - 3);
                    e.Graphics.DrawString(text, e.CellStyle.Font, brush, textRect);
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události formátování buněk v DataGridView dgvPanelsRefyearsets</para>
        /// <para lang="en">Event handler of the cell formatting in DataGridView dgvPanelsRefyearsets</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvPanelsRefyearsets_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // automatically check cells in "Select" column based on value of "passed_by_module" column
            if (e.ColumnIndex == dgvPanelsRefyearsets.Columns["Select"].Index && e.RowIndex >= 0)
            {
                bool passedByModule = (bool)dgvPanelsRefyearsets.Rows[e.RowIndex].Cells["passed_by_module"].Value;
                if (passedByModule)
                {
                    e.Value = true;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na obsah buňky v DataGridView dgvPanelsRefyearsets</para>
        /// <para lang="en">Event handler of the cell content click formatting in DataGridView dgvPanelsRefyearsets</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvPanelsRefyearsets_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // allow the user to check or uncheck cells in "Select" column
            dgvPanelsRefyearsets.CellFormatting -= DgvPanelsRefyearsets_CellFormatting;

            // To set the checkbox value correctly
            if (e.ColumnIndex == dgvPanelsRefyearsets.Columns["Select"].Index && e.RowIndex >= 0)
            {
                DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)dgvPanelsRefyearsets.Rows[e.RowIndex].Cells["Select"];
                if (checkbox.Value == null || checkbox.Value == DBNull.Value)
                {
                    checkbox.Value = true; // Set true, if null
                }
                else
                {
                    checkbox.Value = !(bool)checkbox.Value; // Switch the value
                }

                // DataGridView refresh to show the change correctly
                dgvPanelsRefyearsets.RefreshEdit();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zpět"</para>
        /// <para lang="en">Handling the "Back" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnPrevious_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            SourceTransaction.Rollback();
            TargetTransaction.Rollback();

            controlTargetVariable = new ControlTargetVariable(
                controlOwner: this,
                selectedExportConnectionId: _selectedExportConnectionId)
            {
                Dock = DockStyle.Fill
            };
            controlTargetVariable.LoadContent();
            controlTargetVariable.InitializeLabels();
            CtrEtl?.ShowControl(control: controlTargetVariable);

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Vybrat vše"</para>
        /// <para lang="en">Handling the "Select all" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnSelectAll_Click(object sender, EventArgs e)
        {
            // Check if all checkboxes are checked
            bool allChecked = true;
            foreach (DataGridViewRow row in dgvPanelsRefyearsets.Rows)
            {
                if (row.Cells["Select"].Value == null || !(bool)row.Cells["Select"].Value)
                {
                    allChecked = false;
                    break;
                }
            }

            // Check or uncheck all checkboxes based on the previous state
            foreach (DataGridViewRow row in dgvPanelsRefyearsets.Rows)
            {
                row.Cells["Select"].Value = !allChecked;
            }

            dgvPanelsRefyearsets.RefreshEdit();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události při změně jazyka</para>
        /// <para lang="en">Handles the event when the language changes</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který vyvolal událost (ControlEtl)</para>
        /// <para lang="en">The object that raised the event (ControlEtl)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Argumenty události</para>
        /// <para lang="en">Event arguments</para>
        /// </param>
        private void OnLanguageChanged(object sender, EventArgs e)
        {
            InitializeLabels();
        }

        #endregion Event Handlers


        #region Events

        #region NextClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">The delegate of the function handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void NextClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event NextClickHandler NextClick;

        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Invoking a click event on the "Next" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaiseNextClick(object sender, EventArgs e)
        {
            NextClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            //pass EtlId (three etl id parameters form previous forms or user controls merged into one)
            if (_idEtlFromTargetVariableCompare != null)
            {
                EtlId = _idEtlFromTargetVariableCompare;
            }
            else if (_idEtlFromTargetVariable != null)
            {
                EtlId = _idEtlFromTargetVariable;
            }
            else if (_idEtl != null)
            {
                EtlId = _idEtl;
            }

            SelectedRefyearsetsToPanels = new List<int?>();
            foreach (DataGridViewRow row in dgvPanelsRefyearsets.Rows)
            {
                if (row.Cells["Select"].Value != null && (bool)row.Cells["Select"].Value == true)
                {
                    int id = (int)row.Cells["refyearset2panel"].Value;
                    SelectedRefyearsetsToPanels.Add(id);
                }
            }

            if (!SelectedRefyearsetsToPanels.Any())
            {
                ShowMessageNoCombinationSelected();
            }
            else
            {
                //pass EtlId and selectedRefyearsetsToPanels

                if (ControlOwner is ControlTargetVariable)
                {
                    ExecuteFnEtlGetAreaDomainJsonAndFnEtlGetSubPopulationJson();

                    if (string.IsNullOrEmpty(areaDomains) && string.IsNullOrEmpty(subPopulations))
                    {
                        ExecuteSection10AreaDomainCategoryAndSubPopulationCategoryMappingOfSchema();
                    }
                    else if (string.IsNullOrEmpty(areaDomains) && !string.IsNullOrEmpty(subPopulations))
                    {
                        ShowControlSubPopulation();
                    }
                    else if (!string.IsNullOrEmpty(areaDomains))
                    {
                        ShowControlAreaDomain();
                    }
                }
                else if (ControlOwner is ControlEtl && FromTargetVariableCompare)
                {
                    ExecuteFnEtlGetAreaDomainJsonAndFnEtlGetSubPopulationJson();

                    if (string.IsNullOrEmpty(areaDomains) && string.IsNullOrEmpty(subPopulations))
                    {
                        ExecuteSection10AreaDomainCategoryAndSubPopulationCategoryMappingOfSchema();
                    }
                    else if (string.IsNullOrEmpty(areaDomains) && !string.IsNullOrEmpty(subPopulations))
                    {
                        ShowControlSubPopulation();
                    }
                    else if (!string.IsNullOrEmpty(areaDomains))
                    {
                        RaiseNextClick(sender, e);
                    }
                }
                else
                {
                    ExecuteFnEtlGetAreaDomainJsonAndFnEtlGetSubPopulationJson();

                    if (string.IsNullOrEmpty(areaDomains) && string.IsNullOrEmpty(subPopulations))
                    {
                        ExecuteSection10AreaDomainCategoryAndSubPopulationCategoryMappingOfSchema();
                    }
                    else if (string.IsNullOrEmpty(areaDomains) && !string.IsNullOrEmpty(subPopulations))
                    {
                        ShowControlSubPopulation();
                    }
                    else if (!string.IsNullOrEmpty(areaDomains))
                    {
                        ShowControlAreaDomain();
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        #endregion NextClick Event

        #endregion Events
    }

}