﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Controls;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou"</para>
    /// <para lang="en">Control "Comparison of the selected target variable with target variables being already in Estimates"</para>
    /// </summary>
    internal partial class ControlTargetVariableCompare
            : UserControl, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
        /// <para lang="en">Selection of the target variable"</para>
        /// </summary>
        private ControlTargetVariable controlTargetVariable;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba kombinací panelů a refyearsetů"</para>
        /// <para lang="en">Control "Select panel and refyearset combinations"</para>
        /// </summary>
        private ControlPanelRefyearset ctrPanelRefyearset;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private int selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Hodnoty cílové proměnné, zvolené v ControlTargetVariable</para>
        /// <para lang="en">Values of the target variable selected in the ControlTargetVariable</para>
        /// </summary>
        private SelectedTargetVariableValues selectedTargetVariableValues;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelCs;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelEn;

        #endregion Private Fields

        #region Controls


        #endregion Controls

        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="selectedTargetVariableValues">
        /// <para lang="cs">Hodnoty cílové proměnné, zvolené v ControlTargetVariable</para>
        /// <para lang="en">Values of the target variable selected in the ControlTargetVariable</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="selectedIndicatorLabelCs">
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </param>
        /// <param name="selectedIndicatorLabelEn">
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public ControlTargetVariableCompare(
            Control controlOwner,
            SelectedTargetVariableValues selectedTargetVariableValues,
            int selectedExportConnectionId,
            string selectedIndicatorLabelCs,
            string selectedIndicatorLabelEn,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                selectedTargetVariableValues: selectedTargetVariableValues,
                selectedExportConnectionId: selectedExportConnectionId
                );

            _selectedIndicatorLabelCs = selectedIndicatorLabelCs;
            _selectedIndicatorLabelEn = selectedIndicatorLabelEn;

            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlEtl ctrEtl)
                {
                    return ctrEtl;
                }
                else if (ControlOwner is ControlTargetVariable ctrTargetVariable)
                {
                    return ctrTargetVariable.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Hodnoty cílové proměnné, zvolené v ControlTargetVariable</para>
        /// <para lang="en">Values of the target variable selected in the ControlTargetVariable</para>
        /// </summary>
        public SelectedTargetVariableValues SelectedTargetVariableValues
        {
            get
            {
                return selectedTargetVariableValues;
            }
            set
            {
                selectedTargetVariableValues = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        public int SelectedExportConnectionId
        {
            get
            {
                return selectedExportConnectionId;
            }
            set
            {
                selectedExportConnectionId = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Id vrácené funkcí fn_etl_save_target_variable. Je uloženo v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id returned by the fn_etl_save_target_variable function. It is saved in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        public int? IdEtlFromControlTargetVariableCompare { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #region Private Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Control "Target variable selection from json file of metadata"</para>
        /// </summary>
        private ControlTargetVariableSelector CtrTargetVariableSelector
        {
            get
            {
                if (pnlTargetVariableSelector.Controls.Count != 1)
                {
                    CreateNewTargetVariableSelector();
                }
                if (!(pnlTargetVariableSelector.Controls[0] is ControlTargetVariableSelector))
                {
                    CreateNewTargetVariableSelector();
                }
                return (ControlTargetVariableSelector)pnlTargetVariableSelector.Controls[0];
            }
        }

        #endregion Private Controls

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                         "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou" },
                            { "lblIndicator",                           "indikátor" },
                            { "lblStateOrChange",                       "stav nebo změna" },
                            { "lblUnit",                                "jednotka" },
                            { "lblLocalDensity",                        "příspěvek lokální hustoty" },
                            { "lblVersion",                             "verze" },
                            { "lblDefinitionVariant",                   "definiční varianta" },
                            { "lblAreaDomain",                          "plošná doména" },
                            { "lblPopulation",                          "populace" },
                            { "grpSelectedTargetVariable",              "Zvolená cílová proměnná:" },
                            { "ctrMetadataElementsText",                "Cílové proměnné, které už v modulu Odhady jsou:" },
                            { "btnPrevious",                            "Předchozí" },
                            { "btnNext",                                "Další" },
                            { "msgCannotDeleteSelectedConnection",      "Vybrané připojení nelze smazat." },
                            { "nationalLanguageVersion",                "cs"},
                            { "internationalLanguageVersion",           "en"},
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlTargetVariableCompare") ?
                    languageFile.NationalVersion.Data["ControlTargetVariableCompare"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                         "Comparison of the selected target variable with target variables being already in Estimates" },
                            { "lblIndicator",                           "indicator" },
                            { "lblStateOrChange",                       "state or change" },
                            { "lblUnit",                                "unit" },
                            { "lblLocalDensity",                        "local density contribution" },
                            { "lblVersion",                             "version" },
                            { "lblDefinitionVariant",                   "definition variant" },
                            { "lblAreaDomain",                          "area domain" },
                            { "lblPopulation",                          "population" },
                            { "grpSelectedTargetVariable",              "Selected target variable:" },
                            { "ctrMetadataElementsText",                "Target variables being already in Estimates:" },
                            { "btnPrevious",                            "Previous" },
                            { "btnNext",                                "Next" },
                            { "msgCannotDeleteSelectedConnection",      "The selected connection cannot be deleted." },
                            { "nationalLanguageVersion",                "cs"},
                            { "internationalLanguageVersion",           "en"},
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlTargetVariableCompare") ?
                    languageFile.InternationalVersion.Data["ControlTargetVariableCompare"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="selectedTargetVariableValues">
        /// <para lang="cs">Hodnoty cílové proměnné, zvolené v ControlTargetVariable</para>
        /// <para lang="en">Values of the target variable selected in the ControlTargetVariable</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            SelectedTargetVariableValues selectedTargetVariableValues,
            int selectedExportConnectionId)
        {
            ControlOwner = controlOwner;

            SelectedTargetVariableValues = selectedTargetVariableValues;

            SelectedExportConnectionId = selectedExportConnectionId;

            nationalLang = new NationalLang();

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            lblIndicator.Text =
                labels.ContainsKey(key: "lblIndicator") ?
                labels["lblIndicator"] : String.Empty;

            lblStateOrChange.Text =
               labels.ContainsKey(key: "lblStateOrChange") ?
               labels["lblStateOrChange"] : String.Empty;

            lblUnit.Text =
              labels.ContainsKey(key: "lblUnit") ?
              labels["lblUnit"] : String.Empty;

            lblLocalDensity.Text =
              labels.ContainsKey(key: "lblLocalDensity") ?
              labels["lblLocalDensity"] : String.Empty;

            lblVersion.Text =
              labels.ContainsKey(key: "lblVersion") ?
              labels["lblVersion"] : String.Empty;

            lblDefinitionVariant.Text =
              labels.ContainsKey(key: "lblDefinitionVariant") ?
              labels["lblDefinitionVariant"] : String.Empty;

            lblAreaDomain.Text =
              labels.ContainsKey(key: "lblAreaDomain") ?
              labels["lblAreaDomain"] : String.Empty;

            lblPopulation.Text =
              labels.ContainsKey(key: "lblPopulation") ?
              labels["lblPopulation"] : String.Empty;

            grpSelectedTargetVariable.Text =
              labels.ContainsKey(key: "grpSelectedTargetVariable") ?
              labels["grpSelectedTargetVariable"] : String.Empty;

            btnPrevious.Text =
              labels.ContainsKey(key: "btnPrevious") ?
              labels["btnPrevious"] : String.Empty;

            btnNext.Text =
              labels.ContainsKey(key: "btnNext") ?
              labels["btnNext"] : String.Empty;

            CtrTargetVariableSelector.MetadataElementsText =
                labels.ContainsKey(key: "ctrMetadataElementsText") ?
                labels["ctrMetadataElementsText"] : String.Empty;

            CtrTargetVariableSelector.InitializeLabels();

            LoadContent();

            controlTargetVariable?.InitializeLabels();
            ctrPanelRefyearset?.InitializeLabels();

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            TDFnEtlGetTargetVariableTypeList tdFnEtlGetTargetVariableList =
                TDFunctions.FnEtlGetTargetVariable.Execute(
                    database: Database,
                    exportConnection: SelectedExportConnectionId,
                    nationalLanguage: nationalLang.NatLang,
                    etl: false,
                    transaction: SourceTransaction);

            TDFnEtlGetTargetVariableMetadataList tdFnEtlGetTargetVariablesExtendedList =
                new TDFnEtlGetTargetVariableMetadataList(
                    database: Database);
            tdFnEtlGetTargetVariablesExtendedList.ReLoad(
                fnEtlGetTargetVariable: tdFnEtlGetTargetVariableList);

            TDFnEtlGetTargetVariableMetadataList tdFnEtlGetTargetVariableExtendedListAggregated =
                tdFnEtlGetTargetVariablesExtendedList.Aggregated();

            Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

            string lng =
            //(LanguageVersion == LanguageVersion.National) ? "cs" : "en";
            (LanguageVersion == LanguageVersion.National)
            ? (labels.ContainsKey("nationalLanguageVersion")
                ? labels["nationalLanguageVersion"]
                : (labels.ContainsKey("internationalLanguageVersion")
                    ? labels["internationalLanguageVersion"]
                    : string.Empty))
            : (labels.ContainsKey("internationalLanguageVersion")
                ? labels["internationalLanguageVersion"]
                : (labels.ContainsKey("nationalLanguageVersion")
                    ? labels["nationalLanguageVersion"]
                    : string.Empty));

            //if (ControlOwner is not ControlEtl)
            //{
            //this code enables switching between languages
            txtSelectedIndicator.Text =
                tdFnEtlGetTargetVariablesExtendedList.Data
                .AsEnumerable()
                .Where(a => a.Field<int>("target_variable_id") == SelectedTargetVariableValues.SelectedIndicatorLabel)
                .FirstOrDefault()?
                .Field<string>($"indicator_label_{lng}");
            tipSelectedTV.SetToolTip(txtSelectedIndicator, txtSelectedIndicator.Text);

            var stateOrChangeAndUnitMetadata = tdFnEtlGetTargetVariablesExtendedList.Data.AsEnumerable()
                .Select(a => new
                {
                    StateOrChangeLabel = a.Field<string>($"state_or_change_label_{lng}"),
                    UnitLabel = a.Field<string>($"unit_label_{lng}"),
                    TargetVariableId = a.Field<int>("target_variable_id")
                })
                .Distinct()
                .ToList();

            txtSelectedStateOrChange.Text = stateOrChangeAndUnitMetadata
                .AsEnumerable()
                .Single(a => a.TargetVariableId == SelectedTargetVariableValues.SelectedStateOrChange)
                .StateOrChangeLabel;
            tipSelectedTV.SetToolTip(txtSelectedStateOrChange, txtSelectedStateOrChange.Text);

            txtSelectedUnit.Text = stateOrChangeAndUnitMetadata
                .AsEnumerable()
                .Single(a => a.TargetVariableId == SelectedTargetVariableValues.SelectedUnit)
                .UnitLabel;
            tipSelectedTV.SetToolTip(txtSelectedUnit, txtSelectedUnit.Text);

            txtSelectedLocalDensity.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
                .AsEnumerable()
                .Single(a => a.Field<int>("target_variable_id") == SelectedTargetVariableValues.SelectedLocalDensity)
                .Field<string>($"ldsity_label_{lng}");
            tipSelectedTV.SetToolTip(txtSelectedLocalDensity, txtSelectedLocalDensity.Text);

            txtSelectedVersion.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
                .AsEnumerable()
                .Single(a => a.Field<int>("target_variable_id") == SelectedTargetVariableValues.SelectedVersion)
                .Field<string>($"version_label_{lng}");
            tipSelectedTV.SetToolTip(txtSelectedVersion, txtSelectedVersion.Text);

            txtSelectedDefinitionVariant.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
                .AsEnumerable()
                .Single(a => a.Field<int>("target_variable_id") == SelectedTargetVariableValues.SelectedDefinitionVariant)
                .Field<string>($"definition_variant_label_{lng}");
            tipSelectedTV.SetToolTip(txtSelectedDefinitionVariant, txtSelectedDefinitionVariant.Text);

            txtSelectedAreaDomain.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
                .AsEnumerable()
                .Single(a => a.Field<int>("target_variable_id") == SelectedTargetVariableValues.SelectedAreaDomain)
                .Field<string>($"area_domain_label_{lng}");
            tipSelectedTV.SetToolTip(txtSelectedAreaDomain, txtSelectedAreaDomain.Text);

            txtSelectedPopulation.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
                .AsEnumerable()
                .Single(a => a.Field<int>("target_variable_id") == SelectedTargetVariableValues.SelectedPopulation)
                .Field<string>($"population_label_{lng}");
            tipSelectedTV.SetToolTip(txtSelectedPopulation, txtSelectedPopulation.Text);
            //}
            //else
            //{
            //    //this code enables switching between languages
            //    txtSelectedIndicator.Text =
            //        tdFnEtlGetTargetVariablesExtendedList.Data
            //        .AsEnumerable()
            //        .Where(a => a.Field<int>("target_variable_id") == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .FirstOrDefault()?
            //        .Field<string>($"indicator_label_{lng}");
            //    tipSelectedTV.SetToolTip(txtSelectedIndicator, txtSelectedIndicator.Text);

            //    var stateOrChangeAndUnitMetadata = tdFnEtlGetTargetVariablesExtendedList.Data.AsEnumerable()
            //        .Select(a => new
            //        {
            //            StateOrChangeLabel = a.Field<string>($"state_or_change_label_{lng}"),
            //            UnitLabel = a.Field<string>($"unit_label_{lng}"),
            //            TargetVariableId = a.Field<int>("target_variable_id")
            //        })
            //        .Distinct()
            //        .ToList();

            //    txtSelectedStateOrChange.Text = stateOrChangeAndUnitMetadata
            //        .AsEnumerable()
            //        .Single(a => a.TargetVariableId == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .StateOrChangeLabel;
            //    tipSelectedTV.SetToolTip(txtSelectedStateOrChange, txtSelectedStateOrChange.Text);

            //    txtSelectedUnit.Text = stateOrChangeAndUnitMetadata
            //        .AsEnumerable()
            //        .Single(a => a.TargetVariableId == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .UnitLabel;
            //    tipSelectedTV.SetToolTip(txtSelectedUnit, txtSelectedUnit.Text);

            //    txtSelectedLocalDensity.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
            //        .AsEnumerable()
            //        .Single(a => a.Field<int>("target_variable_id") == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .Field<string>($"ldsity_label_{lng}");
            //    tipSelectedTV.SetToolTip(txtSelectedLocalDensity, txtSelectedLocalDensity.Text);

            //    txtSelectedVersion.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
            //        .AsEnumerable()
            //        .Single(a => a.Field<int>("target_variable_id") == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .Field<string>($"version_label_{lng}");
            //    tipSelectedTV.SetToolTip(txtSelectedVersion, txtSelectedVersion.Text);

            //    txtSelectedDefinitionVariant.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
            //        .AsEnumerable()
            //        .Single(a => a.Field<int>("target_variable_id") == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .Field<string>($"definition_variant_label_{lng}");
            //    tipSelectedTV.SetToolTip(txtSelectedDefinitionVariant, txtSelectedDefinitionVariant.Text);

            //    txtSelectedAreaDomain.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
            //        .AsEnumerable()
            //        .Single(a => a.Field<int>("target_variable_id") == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .Field<string>($"area_domain_label_{lng}");
            //    tipSelectedTV.SetToolTip(txtSelectedAreaDomain, txtSelectedAreaDomain.Text);

            //    txtSelectedPopulation.Text = tdFnEtlGetTargetVariableExtendedListAggregated.Data
            //        .AsEnumerable()
            //        .Single(a => a.Field<int>("target_variable_id") == CtrEtl.CtrTargetVariable.CtrTargetVariableSelector.SelectedTargetVariableMetadata.TargetVariableId)
            //        .Field<string>($"population_label_{lng}");
            //    tipSelectedTV.SetToolTip(txtSelectedPopulation, txtSelectedPopulation.Text);
            //}

            List<int?> alreadyEtledTargetVariableEtlIds =
                    TDFunctions.FnEtlGetEtlIdsOfTargetVariable.Execute(
                        database: Database,
                        exportConnection: SelectedExportConnectionId);

            CtrTargetVariableSelector.Data =
                NfiEstaFunctions.FnEtlGetTargetVariable.Execute(
                    FormUserPassword.TargetDatabase,
                    nationalLang.NatLang,
                    alreadyEtledTargetVariableEtlIds,
                    TargetTransaction);
            CtrTargetVariableSelector.LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nový ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Creates new control "Target variable selection from json file of metadata"</para>
        /// </summary>
        private void CreateNewTargetVariableSelector()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            pnlTargetVariableSelector.Controls.Clear();

            ControlTargetVariableSelector ctrTargetVariableSelector =
                new ControlTargetVariableSelector(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    Database = FormUserPassword.TargetDatabase,
                    Design = TargetVariableSelectorDesign.Simple,
                    Spacing = new List<int>() { 4 },
                    MetadataElementsText =
                        labels.ContainsKey(key: "ctrMetadataElementsText") ?
                        labels["ctrMetadataElementsText"] : String.Empty,
                    NotAssignedCategory = true
                };

            ctrTargetVariableSelector.SelectedTargetVariableChanged +=
                new EventHandler((sender, e) =>
                {
                    //ctrTargetVariableSelector.CTargetVariable.ReLoad();
                    ITargetVariable tv = ctrTargetVariableSelector.SelectedTargetVariable;
                    ITargetVariableMetadata tvm = ctrTargetVariableSelector.SelectedTargetVariableMetadata;

                    SelectedTargetVariableChanged?.Invoke(
                        sender: this,
                        e: new EventArgs());
                });

            ctrTargetVariableSelector.CreateControl();
            pnlTargetVariableSelector.Controls.Add(
                value: ctrTargetVariableSelector);
        }

        #endregion Methods

        #region Events

        #region PreviousClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">The delegate of the function handling the "Previous" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void PreviousClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event PreviousClickHandler PreviousClick;


        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Invoking a click event on the "Previous" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaisePreviousClick(object sender, EventArgs e)
        {
            PreviousClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Handling the "Previous" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnPrevious_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            SourceTransaction.Rollback();
            TargetTransaction.Rollback();

            if (ControlOwner is not ControlEtl)
            {
                controlTargetVariable = new ControlTargetVariable(
                controlOwner: this,
                selectedExportConnectionId: SelectedExportConnectionId)
                {
                    Dock = DockStyle.Fill
                };
                controlTargetVariable.LoadContent();
                controlTargetVariable.InitializeLabels();
                CtrEtl?.ShowControl(control: controlTargetVariable);
            }
            else
            {
                RaisePreviousClick(sender, e);
            }

            Cursor = Cursors.Default;
        }

        #endregion PreviousClick Event

        #region NextClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">The delegate of the function handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void NextClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event NextClickHandler NextClick;

        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Invoking a click event on the "Next" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaiseNextClick(object sender, EventArgs e)
        {
            NextClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            int? selectedTargetDbTargetVariableId = CtrTargetVariableSelector.SelectedTargetVariable?.Id;

            if (selectedTargetDbTargetVariableId != 0) //match found
            {
                string metadata =
                    TDFunctions.FnEtlGetTargetVariableMetadata.Execute(
                        database: Database,
                        targetVariable: SelectedTargetVariableValues.SelectedTargetVariableId,
                        nationalLanguage: nationalLang.NatLang,
                        transaction: SourceTransaction);

                NfiEstaFunctions.FnEtlImportTargetVariableMetadata.Execute(
                database: FormUserPassword.TargetDatabase,
                id: CtrTargetVariableSelector.SelectedTargetVariable.Id,
                metadata: metadata,
                transaction: TargetTransaction);

                IdEtlFromControlTargetVariableCompare =
                    TDFunctions.FnEtlSaveTargetVariable.Execute(
                        database: Database,
                        exportConnection: SelectedExportConnectionId,
                        targetVariable: SelectedTargetVariableValues.SelectedTargetVariableId,
                        etlId: CtrTargetVariableSelector.SelectedTargetVariable.Id,  //result in the target_data.t_etl_target_variable table
                        transaction: SourceTransaction);
            }
            else //no match found
            {
                string metadata =
                    TDFunctions.FnEtlGetTargetVariableMetadata.Execute(
                        database: Database,
                        targetVariable: SelectedTargetVariableValues.SelectedTargetVariableId,
                        nationalLanguage: nationalLang.NatLang,
                        transaction: SourceTransaction);

                int? id = NfiEstaFunctions.FnEtlImportTargetVariable.Execute(
                    FormUserPassword.TargetDatabase,
                    metadata,
                    TargetTransaction);

                IdEtlFromControlTargetVariableCompare =
                    TDFunctions.FnEtlSaveTargetVariable.Execute(
                        database: Database,
                        exportConnection: SelectedExportConnectionId,
                        targetVariable: SelectedTargetVariableValues.SelectedTargetVariableId,
                        etlId: id,
                        transaction: SourceTransaction);
            }

            if (ControlOwner is ControlTargetVariable) //btnPrevious from ControlPanelRefyearset
            {
                ctrPanelRefyearset = new ControlPanelRefyearset(
                    controlOwner: this,
                    idEtlFromTargetVariable: null,
                    idEtlFromTargetVariableCompare: IdEtlFromControlTargetVariableCompare,
                    idEtl: null,
                    refYearSetToPanelMapping: null,
                    selectedExportConnectionId: SelectedExportConnectionId,
                    atomicAreaDomains: null,
                    atomicSubPopulations: null,
                    selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                    sourceTransaction: SourceTransaction,
                    targetTransaction: TargetTransaction
                    )
                {
                    Dock = DockStyle.Fill,
                    FromTargetVariableCompare = true
                };
                ctrPanelRefyearset.LoadContent();
                ctrPanelRefyearset.InitializeLabels();
                CtrEtl.ShowControl(control: ctrPanelRefyearset);
            }
            else
            {
                RaiseNextClick(sender, e);
            }

            Cursor = Cursors.Default;
        }

        #endregion NextClick Event

        #endregion Events


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Událost "Změna vybraného indikátoru"</para>
        /// <para lang="en">"Selected indicator changed" event</para>
        /// </summary>
        public event EventHandler SelectedTargetVariableChanged;

        #endregion Event Handlers

    }

}