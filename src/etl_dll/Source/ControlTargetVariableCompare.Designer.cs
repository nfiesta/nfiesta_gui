﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class ControlTargetVariableCompare
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            btnNext = new System.Windows.Forms.Button();
            lblMainCaption = new System.Windows.Forms.Label();
            tlpMiddlePart = new System.Windows.Forms.TableLayoutPanel();
            grpSelectedTargetVariable = new System.Windows.Forms.GroupBox();
            tlpSelectedTargetVariable = new System.Windows.Forms.TableLayoutPanel();
            lblStateOrChange = new System.Windows.Forms.Label();
            lblUnit = new System.Windows.Forms.Label();
            lblLocalDensity = new System.Windows.Forms.Label();
            lblVersion = new System.Windows.Forms.Label();
            lblDefinitionVariant = new System.Windows.Forms.Label();
            lblAreaDomain = new System.Windows.Forms.Label();
            lblPopulation = new System.Windows.Forms.Label();
            lblIndicator = new System.Windows.Forms.Label();
            txtSelectedIndicator = new System.Windows.Forms.TextBox();
            txtSelectedStateOrChange = new System.Windows.Forms.TextBox();
            txtSelectedUnit = new System.Windows.Forms.TextBox();
            txtSelectedLocalDensity = new System.Windows.Forms.TextBox();
            txtSelectedDefinitionVariant = new System.Windows.Forms.TextBox();
            txtSelectedPopulation = new System.Windows.Forms.TextBox();
            txtSelectedVersion = new System.Windows.Forms.TextBox();
            txtSelectedAreaDomain = new System.Windows.Forms.TextBox();
            pnlTargetVariableSelector = new System.Windows.Forms.Panel();
            tipSelectedTV = new System.Windows.Forms.ToolTip(components);
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpMiddlePart.SuspendLayout();
            grpSelectedTargetVariable.SuspendLayout();
            tlpSelectedTargetVariable.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 2);
            tlpMain.Controls.Add(lblMainCaption, 0, 0);
            tlpMain.Controls.Add(tlpMiddlePart, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1120, 623);
            tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnPrevious);
            pnlButtons.Controls.Add(btnNext);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(4, 580);
            pnlButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(1112, 40);
            pnlButtons.TabIndex = 0;
            // 
            // btnPrevious
            // 
            btnPrevious.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnPrevious.Location = new System.Drawing.Point(751, 4);
            btnPrevious.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(175, 35);
            btnPrevious.TabIndex = 1;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.UseVisualStyleBackColor = true;
            btnPrevious.Click += BtnPrevious_Click;
            // 
            // btnNext
            // 
            btnNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnNext.Location = new System.Drawing.Point(933, 4);
            btnNext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(175, 35);
            btnNext.TabIndex = 0;
            btnNext.Text = "btnNext";
            btnNext.UseVisualStyleBackColor = true;
            btnNext.Click += BtnNext_Click;
            // 
            // lblMainCaption
            // 
            lblMainCaption.AutoSize = true;
            lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblMainCaption.Location = new System.Drawing.Point(4, 0);
            lblMainCaption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblMainCaption.Name = "lblMainCaption";
            lblMainCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblMainCaption.Size = new System.Drawing.Size(1112, 35);
            lblMainCaption.TabIndex = 1;
            lblMainCaption.Text = "lblMainCaption";
            lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpMiddlePart
            // 
            tlpMiddlePart.ColumnCount = 2;
            tlpMiddlePart.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpMiddlePart.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tlpMiddlePart.Controls.Add(grpSelectedTargetVariable, 0, 0);
            tlpMiddlePart.Controls.Add(pnlTargetVariableSelector, 1, 0);
            tlpMiddlePart.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMiddlePart.Location = new System.Drawing.Point(4, 38);
            tlpMiddlePart.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMiddlePart.Name = "tlpMiddlePart";
            tlpMiddlePart.RowCount = 1;
            tlpMiddlePart.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMiddlePart.Size = new System.Drawing.Size(1112, 536);
            tlpMiddlePart.TabIndex = 2;
            // 
            // grpSelectedTargetVariable
            // 
            grpSelectedTargetVariable.Controls.Add(tlpSelectedTargetVariable);
            grpSelectedTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSelectedTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpSelectedTargetVariable.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSelectedTargetVariable.Location = new System.Drawing.Point(4, 13);
            grpSelectedTargetVariable.Margin = new System.Windows.Forms.Padding(4, 13, 4, 3);
            grpSelectedTargetVariable.Name = "grpSelectedTargetVariable";
            grpSelectedTargetVariable.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpSelectedTargetVariable.Size = new System.Drawing.Size(548, 520);
            grpSelectedTargetVariable.TabIndex = 0;
            grpSelectedTargetVariable.TabStop = false;
            // 
            // tlpSelectedTargetVariable
            // 
            tlpSelectedTargetVariable.ColumnCount = 2;
            tlpSelectedTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 198F));
            tlpSelectedTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedTargetVariable.Controls.Add(lblStateOrChange, 0, 1);
            tlpSelectedTargetVariable.Controls.Add(lblUnit, 0, 2);
            tlpSelectedTargetVariable.Controls.Add(lblLocalDensity, 0, 3);
            tlpSelectedTargetVariable.Controls.Add(lblVersion, 0, 4);
            tlpSelectedTargetVariable.Controls.Add(lblDefinitionVariant, 0, 5);
            tlpSelectedTargetVariable.Controls.Add(lblAreaDomain, 0, 6);
            tlpSelectedTargetVariable.Controls.Add(lblPopulation, 0, 7);
            tlpSelectedTargetVariable.Controls.Add(lblIndicator, 0, 0);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedIndicator, 1, 0);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedStateOrChange, 1, 1);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedUnit, 1, 2);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedLocalDensity, 1, 3);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedDefinitionVariant, 1, 5);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedPopulation, 1, 7);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedVersion, 1, 4);
            tlpSelectedTargetVariable.Controls.Add(txtSelectedAreaDomain, 1, 6);
            tlpSelectedTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpSelectedTargetVariable.Location = new System.Drawing.Point(4, 18);
            tlpSelectedTargetVariable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpSelectedTargetVariable.Name = "tlpSelectedTargetVariable";
            tlpSelectedTargetVariable.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            tlpSelectedTargetVariable.RowCount = 9;
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpSelectedTargetVariable.Size = new System.Drawing.Size(540, 499);
            tlpSelectedTargetVariable.TabIndex = 0;
            // 
            // lblStateOrChange
            // 
            lblStateOrChange.AutoSize = true;
            lblStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            lblStateOrChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblStateOrChange.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblStateOrChange.Location = new System.Drawing.Point(4, 48);
            lblStateOrChange.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblStateOrChange.Name = "lblStateOrChange";
            lblStateOrChange.Size = new System.Drawing.Size(190, 33);
            lblStateOrChange.TabIndex = 1;
            lblStateOrChange.Text = "lblStateOrChange";
            lblStateOrChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUnit
            // 
            lblUnit.AutoSize = true;
            lblUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            lblUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblUnit.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblUnit.Location = new System.Drawing.Point(4, 81);
            lblUnit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblUnit.Name = "lblUnit";
            lblUnit.Size = new System.Drawing.Size(190, 33);
            lblUnit.TabIndex = 2;
            lblUnit.Text = "lblUnit";
            lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLocalDensity
            // 
            lblLocalDensity.AutoSize = true;
            lblLocalDensity.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLocalDensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblLocalDensity.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblLocalDensity.Location = new System.Drawing.Point(4, 114);
            lblLocalDensity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblLocalDensity.Name = "lblLocalDensity";
            lblLocalDensity.Size = new System.Drawing.Size(190, 33);
            lblLocalDensity.TabIndex = 3;
            lblLocalDensity.Text = "lblLocalDensity";
            lblLocalDensity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblVersion
            // 
            lblVersion.AutoSize = true;
            lblVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblVersion.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblVersion.Location = new System.Drawing.Point(4, 147);
            lblVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblVersion.Name = "lblVersion";
            lblVersion.Size = new System.Drawing.Size(190, 33);
            lblVersion.TabIndex = 4;
            lblVersion.Text = "lblVersion";
            lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDefinitionVariant
            // 
            lblDefinitionVariant.AutoSize = true;
            lblDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDefinitionVariant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblDefinitionVariant.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblDefinitionVariant.Location = new System.Drawing.Point(4, 180);
            lblDefinitionVariant.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblDefinitionVariant.Name = "lblDefinitionVariant";
            lblDefinitionVariant.Size = new System.Drawing.Size(190, 33);
            lblDefinitionVariant.TabIndex = 5;
            lblDefinitionVariant.Text = "lblDefinitionVariant";
            lblDefinitionVariant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAreaDomain
            // 
            lblAreaDomain.AutoSize = true;
            lblAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblAreaDomain.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblAreaDomain.Location = new System.Drawing.Point(4, 213);
            lblAreaDomain.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblAreaDomain.Name = "lblAreaDomain";
            lblAreaDomain.Size = new System.Drawing.Size(190, 33);
            lblAreaDomain.TabIndex = 6;
            lblAreaDomain.Text = "lblAreaDomain";
            lblAreaDomain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPopulation
            // 
            lblPopulation.AutoSize = true;
            lblPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblPopulation.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblPopulation.Location = new System.Drawing.Point(4, 246);
            lblPopulation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblPopulation.Name = "lblPopulation";
            lblPopulation.Size = new System.Drawing.Size(190, 33);
            lblPopulation.TabIndex = 7;
            lblPopulation.Text = "lblPopulation";
            lblPopulation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblIndicator
            // 
            lblIndicator.AutoSize = true;
            lblIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            lblIndicator.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblIndicator.Location = new System.Drawing.Point(4, 15);
            lblIndicator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblIndicator.Name = "lblIndicator";
            lblIndicator.Size = new System.Drawing.Size(190, 33);
            lblIndicator.TabIndex = 0;
            lblIndicator.Text = "lblIndicator";
            lblIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSelectedIndicator
            // 
            txtSelectedIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedIndicator.Location = new System.Drawing.Point(202, 18);
            txtSelectedIndicator.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedIndicator.Name = "txtSelectedIndicator";
            txtSelectedIndicator.ReadOnly = true;
            txtSelectedIndicator.Size = new System.Drawing.Size(334, 21);
            txtSelectedIndicator.TabIndex = 8;
            // 
            // txtSelectedStateOrChange
            // 
            txtSelectedStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedStateOrChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedStateOrChange.Location = new System.Drawing.Point(202, 51);
            txtSelectedStateOrChange.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedStateOrChange.Name = "txtSelectedStateOrChange";
            txtSelectedStateOrChange.ReadOnly = true;
            txtSelectedStateOrChange.Size = new System.Drawing.Size(334, 21);
            txtSelectedStateOrChange.TabIndex = 9;
            // 
            // txtSelectedUnit
            // 
            txtSelectedUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedUnit.Location = new System.Drawing.Point(202, 84);
            txtSelectedUnit.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedUnit.Name = "txtSelectedUnit";
            txtSelectedUnit.ReadOnly = true;
            txtSelectedUnit.Size = new System.Drawing.Size(334, 21);
            txtSelectedUnit.TabIndex = 10;
            // 
            // txtSelectedLocalDensity
            // 
            txtSelectedLocalDensity.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedLocalDensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedLocalDensity.Location = new System.Drawing.Point(202, 117);
            txtSelectedLocalDensity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedLocalDensity.Name = "txtSelectedLocalDensity";
            txtSelectedLocalDensity.ReadOnly = true;
            txtSelectedLocalDensity.Size = new System.Drawing.Size(334, 21);
            txtSelectedLocalDensity.TabIndex = 11;
            // 
            // txtSelectedDefinitionVariant
            // 
            txtSelectedDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedDefinitionVariant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedDefinitionVariant.Location = new System.Drawing.Point(202, 183);
            txtSelectedDefinitionVariant.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedDefinitionVariant.Name = "txtSelectedDefinitionVariant";
            txtSelectedDefinitionVariant.ReadOnly = true;
            txtSelectedDefinitionVariant.Size = new System.Drawing.Size(334, 21);
            txtSelectedDefinitionVariant.TabIndex = 13;
            // 
            // txtSelectedPopulation
            // 
            txtSelectedPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedPopulation.Location = new System.Drawing.Point(202, 249);
            txtSelectedPopulation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedPopulation.Name = "txtSelectedPopulation";
            txtSelectedPopulation.ReadOnly = true;
            txtSelectedPopulation.Size = new System.Drawing.Size(334, 21);
            txtSelectedPopulation.TabIndex = 15;
            // 
            // txtSelectedVersion
            // 
            txtSelectedVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedVersion.Location = new System.Drawing.Point(202, 150);
            txtSelectedVersion.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedVersion.Name = "txtSelectedVersion";
            txtSelectedVersion.ReadOnly = true;
            txtSelectedVersion.Size = new System.Drawing.Size(334, 21);
            txtSelectedVersion.TabIndex = 16;
            // 
            // txtSelectedAreaDomain
            // 
            txtSelectedAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSelectedAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtSelectedAreaDomain.Location = new System.Drawing.Point(202, 216);
            txtSelectedAreaDomain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSelectedAreaDomain.Name = "txtSelectedAreaDomain";
            txtSelectedAreaDomain.ReadOnly = true;
            txtSelectedAreaDomain.Size = new System.Drawing.Size(334, 21);
            txtSelectedAreaDomain.TabIndex = 17;
            // 
            // pnlTargetVariableSelector
            // 
            pnlTargetVariableSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTargetVariableSelector.Location = new System.Drawing.Point(560, 3);
            pnlTargetVariableSelector.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlTargetVariableSelector.Name = "pnlTargetVariableSelector";
            pnlTargetVariableSelector.Size = new System.Drawing.Size(548, 530);
            pnlTargetVariableSelector.TabIndex = 1;
            // 
            // ControlTargetVariableCompare
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(tlpMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "ControlTargetVariableCompare";
            Size = new System.Drawing.Size(1120, 623);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            pnlButtons.ResumeLayout(false);
            tlpMiddlePart.ResumeLayout(false);
            grpSelectedTargetVariable.ResumeLayout(false);
            tlpSelectedTargetVariable.ResumeLayout(false);
            tlpSelectedTargetVariable.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        public System.Windows.Forms.Button btnPrevious;
        public System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.TableLayoutPanel tlpMiddlePart;
        private System.Windows.Forms.GroupBox grpSelectedTargetVariable;
        private System.Windows.Forms.Label lblIndicator;
        private System.Windows.Forms.Label lblStateOrChange;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.Label lblLocalDensity;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblDefinitionVariant;
        private System.Windows.Forms.Label lblAreaDomain;
        private System.Windows.Forms.Label lblPopulation;
        private System.Windows.Forms.Panel pnlTargetVariableSelector;
        private System.Windows.Forms.ToolTip tipSelectedTV;
        private System.Windows.Forms.TableLayoutPanel tlpSelectedTargetVariable;
        private System.Windows.Forms.TextBox txtSelectedIndicator;
        private System.Windows.Forms.TextBox txtSelectedStateOrChange;
        private System.Windows.Forms.TextBox txtSelectedUnit;
        private System.Windows.Forms.TextBox txtSelectedLocalDensity;
        private System.Windows.Forms.TextBox txtSelectedDefinitionVariant;
        private System.Windows.Forms.TextBox txtSelectedPopulation;
        private System.Windows.Forms.TextBox txtSelectedVersion;
        private System.Windows.Forms.TextBox txtSelectedAreaDomain;
    }

}
