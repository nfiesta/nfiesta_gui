﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova
{
    namespace ModuleEtl
    {
        partial class ControlTopic
        {
            /// <summary> 
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Component Designer generated code

            /// <summary> 
            /// Required method for Designer support - do not modify 
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlTopic));
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.grpTopicAll = new System.Windows.Forms.GroupBox();
            this.tlpTopicAll = new System.Windows.Forms.TableLayoutPanel();
            this.dgvTopicAll = new System.Windows.Forms.DataGridView();
            this.tsrTopicAll = new System.Windows.Forms.ToolStrip();
            this.btnTopicAllAdd = new System.Windows.Forms.ToolStripButton();
            this.btnTopicAllUpdate = new System.Windows.Forms.ToolStripButton();
            this.btnTopicAllDelete = new System.Windows.Forms.ToolStripButton();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnNext = new System.Windows.Forms.Button();
            this.tlpCaption = new System.Windows.Forms.TableLayoutPanel();
            this.tlpCaptionSelectedValues = new System.Windows.Forms.TableLayoutPanel();
            this.lblTargetVariableValue = new System.Windows.Forms.Label();
            this.lblTargetVariable = new System.Windows.Forms.Label();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.tlpMain.SuspendLayout();
            this.grpTopicAll.SuspendLayout();
            this.tlpTopicAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTopicAll)).BeginInit();
            this.tsrTopicAll.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.tlpCaption.SuspendLayout();
            this.tlpCaptionSelectedValues.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.grpTopicAll, 0, 1);
            this.tlpMain.Controls.Add(this.pnlButtons, 0, 2);
            this.tlpMain.Controls.Add(this.tlpCaption, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.TabIndex = 2;
            // 
            // grpTopicAll
            // 
            this.grpTopicAll.Controls.Add(this.tlpTopicAll);
            this.grpTopicAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpTopicAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpTopicAll.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpTopicAll.Location = new System.Drawing.Point(3, 63);
            this.grpTopicAll.Name = "grpTopicAll";
            this.grpTopicAll.Size = new System.Drawing.Size(954, 434);
            this.grpTopicAll.TabIndex = 0;
            this.grpTopicAll.TabStop = false;
            this.grpTopicAll.Text = "grpTopicAll";
            // 
            // tlpTopicAll
            // 
            this.tlpTopicAll.ColumnCount = 1;
            this.tlpTopicAll.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTopicAll.Controls.Add(this.dgvTopicAll, 0, 1);
            this.tlpTopicAll.Controls.Add(this.tsrTopicAll, 0, 0);
            this.tlpTopicAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpTopicAll.Location = new System.Drawing.Point(3, 18);
            this.tlpTopicAll.Name = "tlpTopicAll";
            this.tlpTopicAll.RowCount = 2;
            this.tlpTopicAll.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpTopicAll.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTopicAll.Size = new System.Drawing.Size(948, 413);
            this.tlpTopicAll.TabIndex = 0;
            // 
            // dgvTopicAll
            // 
            this.dgvTopicAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTopicAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTopicAll.Location = new System.Drawing.Point(3, 28);
            this.dgvTopicAll.Name = "dgvTopicAll";
            this.dgvTopicAll.Size = new System.Drawing.Size(942, 382);
            this.dgvTopicAll.TabIndex = 0;
            this.dgvTopicAll.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTopicAll_CellContentClick);
            this.dgvTopicAll.SelectionChanged += new System.EventHandler(this.DgvTopicAll_SelectionChanged);
            // 
            // tsrTopicAll
            // 
            this.tsrTopicAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrTopicAll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnTopicAllAdd,
            this.btnTopicAllUpdate,
            this.btnTopicAllDelete});
            this.tsrTopicAll.Location = new System.Drawing.Point(0, 0);
            this.tsrTopicAll.Name = "tsrTopicAll";
            this.tsrTopicAll.Size = new System.Drawing.Size(948, 25);
            this.tsrTopicAll.TabIndex = 1;
            this.tsrTopicAll.Text = "tsrTopicAll";
            // 
            // btnTopicAllAdd
            // 
            this.btnTopicAllAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTopicAllAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnTopicAllAdd.Image")));
            this.btnTopicAllAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTopicAllAdd.Name = "btnTopicAllAdd";
            this.btnTopicAllAdd.Size = new System.Drawing.Size(23, 22);
            this.btnTopicAllAdd.Text = "btnTopicAllAdd";
            this.btnTopicAllAdd.Click += new System.EventHandler(this.BtnTopicAllAdd_Click);
            // 
            // btnTopicAllUpdate
            // 
            this.btnTopicAllUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTopicAllUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnTopicAllUpdate.Image")));
            this.btnTopicAllUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTopicAllUpdate.Name = "btnTopicAllUpdate";
            this.btnTopicAllUpdate.Size = new System.Drawing.Size(23, 22);
            this.btnTopicAllUpdate.Text = "btnTopicAllUpdate";
            this.btnTopicAllUpdate.Click += new System.EventHandler(this.BtnTopicAllUpdate_Click);
            // 
            // btnTopicAllDelete
            // 
            this.btnTopicAllDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTopicAllDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnTopicAllDelete.Image")));
            this.btnTopicAllDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTopicAllDelete.Name = "btnTopicAllDelete";
            this.btnTopicAllDelete.Size = new System.Drawing.Size(23, 22);
            this.btnTopicAllDelete.Text = "btnTopicAllDelete";
            this.btnTopicAllDelete.Click += new System.EventHandler(this.BtnTopicAllDelete_Click);
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(3, 503);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(954, 34);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(801, 1);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // tlpCaption
            // 
            this.tlpCaption.ColumnCount = 1;
            this.tlpCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCaption.Controls.Add(this.tlpCaptionSelectedValues, 0, 1);
            this.tlpCaption.Controls.Add(this.lblMainCaption, 0, 0);
            this.tlpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCaption.Location = new System.Drawing.Point(3, 3);
            this.tlpCaption.Name = "tlpCaption";
            this.tlpCaption.RowCount = 2;
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCaption.Size = new System.Drawing.Size(954, 54);
            this.tlpCaption.TabIndex = 1;
            // 
            // tlpCaptionSelectedValues
            // 
            this.tlpCaptionSelectedValues.ColumnCount = 2;
            this.tlpCaptionSelectedValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpCaptionSelectedValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaptionSelectedValues.Controls.Add(this.lblTargetVariableValue, 1, 0);
            this.tlpCaptionSelectedValues.Controls.Add(this.lblTargetVariable, 0, 0);
            this.tlpCaptionSelectedValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCaptionSelectedValues.Location = new System.Drawing.Point(3, 30);
            this.tlpCaptionSelectedValues.Name = "tlpCaptionSelectedValues";
            this.tlpCaptionSelectedValues.RowCount = 1;
            this.tlpCaptionSelectedValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCaptionSelectedValues.Size = new System.Drawing.Size(948, 21);
            this.tlpCaptionSelectedValues.TabIndex = 0;
            // 
            // lblTargetVariableValue
            // 
            this.lblTargetVariableValue.AutoSize = true;
            this.lblTargetVariableValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTargetVariableValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTargetVariableValue.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTargetVariableValue.Location = new System.Drawing.Point(163, 0);
            this.lblTargetVariableValue.Name = "lblTargetVariableValue";
            this.lblTargetVariableValue.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblTargetVariableValue.Size = new System.Drawing.Size(782, 21);
            this.lblTargetVariableValue.TabIndex = 3;
            this.lblTargetVariableValue.Text = "lblTargetVariableValue";
            this.lblTargetVariableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTargetVariable
            // 
            this.lblTargetVariable.AutoSize = true;
            this.lblTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTargetVariable.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblTargetVariable.Location = new System.Drawing.Point(3, 0);
            this.lblTargetVariable.Name = "lblTargetVariable";
            this.lblTargetVariable.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.lblTargetVariable.Size = new System.Drawing.Size(154, 21);
            this.lblTargetVariable.TabIndex = 2;
            this.lblTargetVariable.Text = "lblTargetVariable";
            this.lblTargetVariable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.AutoSize = true;
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(3, 0);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(948, 27);
            this.lblMainCaption.TabIndex = 1;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlTopic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpMain);
            this.Name = "ControlTopic";
            this.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.ResumeLayout(false);
            this.grpTopicAll.ResumeLayout(false);
            this.tlpTopicAll.ResumeLayout(false);
            this.tlpTopicAll.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTopicAll)).EndInit();
            this.tsrTopicAll.ResumeLayout(false);
            this.tsrTopicAll.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.tlpCaption.ResumeLayout(false);
            this.tlpCaption.PerformLayout();
            this.tlpCaptionSelectedValues.ResumeLayout(false);
            this.tlpCaptionSelectedValues.PerformLayout();
            this.ResumeLayout(false);

            }

            #endregion

            private System.Windows.Forms.TableLayoutPanel tlpMain;
            private System.Windows.Forms.Panel pnlButtons;
            private System.Windows.Forms.Label lblMainCaption;
            private System.Windows.Forms.GroupBox grpTopicAll;
            private System.Windows.Forms.TableLayoutPanel tlpTopicAll;
            private System.Windows.Forms.ToolStrip tsrTopicAll;
            private System.Windows.Forms.ToolStripButton btnTopicAllAdd;
            private System.Windows.Forms.ToolStripButton btnTopicAllUpdate;
            private System.Windows.Forms.ToolStripButton btnTopicAllDelete;
            private System.Windows.Forms.DataGridView dgvTopicAll;

            private System.Windows.Forms.TableLayoutPanel tlpCaption;
            private System.Windows.Forms.TableLayoutPanel tlpCaptionSelectedValues;
            private System.Windows.Forms.Label lblTargetVariableValue;
            private System.Windows.Forms.Label lblTargetVariable;

            /// <summary>
            /// btnNext
            /// </summary>
            public System.Windows.Forms.Button btnNext;
        }
    }
}
