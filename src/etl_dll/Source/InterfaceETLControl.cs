﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova
{
    namespace ModuleEtl
    {

        /// <summary>
        /// <para lang="cs">Rozhraní ovládacích prvků modulu pro ETL</para>
        /// <para lang="en">Interface of the controls in the module for ETL</para>
        /// </summary>
        internal interface IETLControl
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">Files with control labels for national and international version</para>
            /// </summary>
            LanguageFile LanguageFile { get; }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro ETL</para>
            /// <para lang="en">Module for the ETL</para>
            /// </summary>
            Setting Setting { get; }

            #endregion Properties

        }

    }
}
