﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova
{
    namespace ModuleEtl
    {

        /// <summary>
        /// <para lang="cs">Hodnoty cílové proměnné, zvolené v ControlTargetVariable</para>
        /// <para lang="en">Values of the target variable selected in the ControlTargetVariable</para>
        /// </summary>
        public class SelectedTargetVariableValues
        {

            /// <summary>
            /// <para lang="cs">Id cílové proměnné, zvolené v ControlTargetVariable</para>
            /// <para lang="en">Id of the target variable selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedTargetVariableId { get; set; }

            /// <summary>
            /// <para lang="cs">Popisek indikátoru, zvolené v ControlTargetVariable</para>
            /// <para lang="en">Label of the indicator selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedIndicatorLabel { get; set; }

            /// <summary>
            /// <para lang="cs">Stav, nebo změna, zvolená v ControlTargetVariable</para>
            /// <para lang="en">State or change selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedStateOrChange { get; set; }

            /// <summary>
            /// <para lang="cs">Jednotka, zvolená v ControlTargetVariable</para>
            /// <para lang="en">Unit selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedUnit { get; set; }

            /// <summary>
            /// <para lang="cs">Příspěvek lokální hustoty, zvolený v ControlTargetVariable</para>
            /// <para lang="en">Local density contribution selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedLocalDensity { get; set; }

            /// <summary>
            /// <para lang="cs">Verze, zvolená v ControlTargetVariable</para>
            /// <para lang="en">Version selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedVersion { get; set; }

            /// <summary>
            /// <para lang="cs">Definiční varianta, zvolená v ControlTargetVariable</para>
            /// <para lang="en">Definition variant selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedDefinitionVariant { get; set; }

            /// <summary>
            /// <para lang="cs">Plošná doména, zvolená v ControlTargetVariable</para>
            /// <para lang="en">Area domain selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedAreaDomain { get; set; }

            /// <summary>
            /// <para lang="cs">Populace, zvolená v ControlTargetVariable</para>
            /// <para lang="en">Population selected in the ControlTargetVariable</para>
            /// </summary>
            public int SelectedPopulation { get; set; }
        }

    }
}