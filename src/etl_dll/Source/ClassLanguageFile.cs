﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace Hanakova
{
    namespace ModuleEtl
    {

        /// <summary>
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">Files with control labels for national and international version</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class LanguageFile
        {

            /// <summary>
            /// <para lang="cs">Data jazykové verze</para>
            /// <para lang="en">Language version data</para>
            /// </summary>
            public class Version
            {

                #region Private Fields

                /// <summary>
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </summary>
                private LanguageVersion languageVersion;

                /// <summary>
                /// <para lang="cs">Jazyk </para>
                /// <para lang="en">Language</para>
                /// </summary>
                private Language language;

                /// <summary>
                /// <para lang="cs">Slovník s popisky ovládacích prvků</para>
                /// <para lang="en">Dictionary with control labels</para>
                /// </summary>
                private Dictionary<string, Dictionary<string, string>> data;

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                private JsonSerializerOptions serializerOptions;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// <para lang="cs">Konstruktor objektu (default: angličtina)</para>
                /// <para lang="en">Object constructor (default: English)</para>
                /// </summary>
                public Version()
                {
                    SerializerOptions = null;
                    LanguageVersion = LanguageVersion.International;
                    Language = Language.EN;
                    Data = LanguageFile.Dictionary(
                        languageVersion: LanguageVersion.International,
                        languageFile: null);
                }

                /// <summary>
                /// <para lang="cs">Konstruktor objektu</para>
                /// <para lang="en">Object constructor</para>
                /// </summary>
                /// <param name="languageVersion">
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </param>
                /// <param name="language">
                /// <para lang="cs">Jazyk </para>
                /// <para lang="en">Language</para>
                /// </param>
                /// <param name="data">
                /// <para lang="cs">Slovník s popisky ovládacích prvků</para>
                /// <para lang="en">Dictionary with control labels</para>
                /// </param>
                public Version(
                    LanguageVersion languageVersion,
                    Language language,
                    Dictionary<string, Dictionary<string, string>> data)
                {
                    SerializerOptions = null;
                    LanguageVersion = languageVersion;
                    Language = language;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </summary>
                [JsonIgnore]
                public LanguageVersion LanguageVersion
                {
                    get
                    {
                        return languageVersion;
                    }
                    set
                    {
                        languageVersion = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Jazyk </para>
                /// <para lang="en">Language</para>
                /// </summary>
                public Language Language
                {
                    get
                    {
                        return language;
                    }
                    set
                    {
                        language = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Slovník s popisky ovládacích prvků</para>
                /// <para lang="en">Dictionary with control labels</para>
                /// </summary>
                public Dictionary<string, Dictionary<string, string>> Data
                {
                    get
                    {
                        return data ??
                            LanguageFile.Dictionary(
                            languageVersion: LanguageVersion,
                            languageFile: null);
                    }
                    set
                    {
                        data = value ??
                            LanguageFile.Dictionary(
                            languageVersion: LanguageVersion,
                            languageFile: null);
                    }
                }

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                [JsonIgnore]
                private JsonSerializerOptions SerializerOptions
                {
                    get
                    {
                        return serializerOptions ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = true
                            };
                    }
                    set
                    {
                        serializerOptions = value ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = true
                            };
                    }
                }

                /// <summary>
                /// <para lang="cs">Serializace objektu do formátu JSON</para>
                /// <para lang="en">Object serialization to JSON format</para>
                /// </summary>
                [JsonIgnore]
                public string JSON
                {
                    get
                    {
                        return JsonSerializer.Serialize(
                            value: this,
                            options: SerializerOptions);
                    }
                }

                #endregion Properties


                #region Static Methods

                /// <summary>
                /// <para lang="cs">Obnovení objektu z formátu JSON</para>
                /// <para lang="en">Restoring an object from JSON format</para>
                /// </summary>
                /// <param name="languageVersion">
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </param>
                /// <param name="json">
                /// <para lang="cs">JSON text</para>
                /// <para lang="en">JSON text</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">Data jazykové verze</para>
                /// <para lang="en">Language version data</para>
                /// </returns>
                public static Version Deserialize(
                    LanguageVersion languageVersion,
                    string json)
                {
                    try
                    {
                        Version version =
                            JsonSerializer.Deserialize<Version>(json: json);
                        version.LanguageVersion = languageVersion;
                        return version;
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            text: exception.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Exclamation);

                        return new Version()
                        { LanguageVersion = languageVersion };
                    }
                }

                /// <summary>
                /// <para lang="cs">Načte objekt ze souboru json</para>
                /// <para lang="en">Loads object from json file</para>
                /// </summary>
                /// <param name="languageVersion">
                /// <para lang="cs">Jazyková verze</para>
                /// <para lang="en">Language version</para>
                /// </param>
                /// <param name="path">
                /// <para lang="cs">Cesta k json souboru</para>
                /// <para lang="en">Path to json file</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">Data jazykové verze</para>
                /// <para lang="en">Language version data</para>
                /// </returns>
                public static Version LoadFromFile(
                    LanguageVersion languageVersion,
                    string path)
                {
                    try
                    {
                        return Deserialize(
                            languageVersion: languageVersion,
                            json: File.ReadAllText(
                                path: path,
                                encoding: Encoding.UTF8));
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            text: exception.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);

                        return new Version()
                        { LanguageVersion = languageVersion };
                    }
                }

                #endregion Static Methods


                #region Methods

                /// <summary>
                /// <para lang="cs">Uloží objekt do souboru json</para>
                /// <para lang="en">Save object into json file</para>
                /// </summary>
                /// <param name="path">
                /// <para lang="cs">Cesta k json souboru</para>
                /// <para lang="en">Path to json file</para>
                /// </param>
                public void SaveToFile(string path)
                {
                    try
                    {
                        File.WriteAllText(
                            path: path,
                            contents: JSON,
                            encoding: Encoding.UTF8);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            text: exception.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                }

                #endregion Methods

            }


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Národní verze</para>
            /// <para lang="en">National version</para>
            /// </summary>
            private Version nationalVersion;

            /// <summary>
            /// <para lang="cs">Mezinárodní verze</para>
            /// <para lang="en">International version</para>
            /// </summary>
            private Version internationalVersion;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu
            /// (výchozí verze EN/CS : použijí se popisky ze zdrojového kódu)</para>
            /// <para lang="en">Object constructor
            /// (default version EN/CS : using labels from source code)</para>
            /// </summary>
            public LanguageFile()
            {
                NationalVersion = new Version(
                    languageVersion: LanguageVersion.National,
                    language: Language.CS,
                    data: LanguageFile.Dictionary(
                            languageVersion: LanguageVersion.National,
                            languageFile: null));

                InternationalVersion = new Version(
                    languageVersion: LanguageVersion.International,
                    language: Language.EN,
                    data: LanguageFile.Dictionary(
                            languageVersion: LanguageVersion.International,
                            languageFile: null));
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Národní verze</para>
            /// <para lang="en">National version</para>
            /// </summary>
            public Version NationalVersion
            {
                get
                {
                    return nationalVersion ?? new Version(
                        languageVersion: LanguageVersion.National,
                        language: Language.CS,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null));
                }
                set
                {
                    nationalVersion = value ?? new Version(
                        languageVersion: LanguageVersion.National,
                        language: Language.CS,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null));
                }
            }

            /// <summary>
            /// <para lang="cs">Mezinárodní verze</para>
            /// <para lang="en">International version</para>
            /// </summary>
            public Version InternationalVersion
            {
                get
                {
                    return internationalVersion ?? new Version(
                        languageVersion: LanguageVersion.International,
                        language: Language.EN,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null));
                }
                set
                {
                    internationalVersion = value ?? new Version(
                        languageVersion: LanguageVersion.International,
                        language: Language.EN,
                        data: LanguageFile.Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null));
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Zvolený jazyk</para>
            /// <para lang="en">Selected language</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">File with control labels for national and international version</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts in selected language</para>
            /// </returns>
            public static Dictionary<string, Dictionary<string, string>>
                Dictionary(
                    LanguageVersion languageVersion,
                    LanguageFile languageFile)
            {
                return
                    new Dictionary<string, Dictionary<string, string>>()
                    {
                        { nameof(ControlAreaDomain),
                            ControlAreaDomain.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlAttributeVariables),
                            ControlAttributeVariables.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlConnection),
                            ControlConnection.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlEtl),
                            ControlEtl.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlPanelRefyearset),
                            ControlPanelRefyearset.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlSDesignSync),
                            ControlSDesignSync.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlSubPopulation),
                            ControlSubPopulation.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlTargetVariable),
                            ControlTargetVariable.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlTargetVariableCompare),
                            ControlTargetVariableCompare.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(ControlTopic),
                            ControlTopic.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormCheckMetadata),
                            FormCheckMetadata.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormCommit),
                            FormCommit.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormConnectionEdit),
                            FormConnectionEdit.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormConnectionNew),
                            FormConnectionNew.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormEtledTargetVariables),
                            FormEtledTargetVariables.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormEtledTVCheckMetadata),
                            FormEtledTVCheckMetadata.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormTopicNew),
                            FormTopicNew.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormTopicUpdate),
                            FormTopicUpdate.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormUserPassword),
                            FormUserPassword.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(FormCheckAdSp),
                            FormCheckAdSp.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                        { nameof(PGWrapper),
                            PGWrapper.Dictionary(
                                languageVersion: languageVersion,
                                languageFile: languageFile) },
                    };
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </returns>
            public override string ToString()
            {
                return String.Concat(
                    $"National version language : ",
                    $"{LanguageList.Name(language: NationalVersion.Language)} {Environment.NewLine}",
                    $"International version language : ",
                    $"{LanguageList.Name(language: InternationalVersion.Language)} {Environment.NewLine}");
            }

            #endregion Methods

        }

    }
}