﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class ControlAreaDomain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            lblMainCaption = new System.Windows.Forms.Label();
            splMain = new System.Windows.Forms.SplitContainer();
            splMiddlePanelWithButtons = new System.Windows.Forms.SplitContainer();
            splAreaDomain = new System.Windows.Forms.SplitContainer();
            grpAreaDomain = new System.Windows.Forms.GroupBox();
            dgvAreaDomain = new System.Windows.Forms.DataGridView();
            grpAreaDomainEtlIdNull = new System.Windows.Forms.GroupBox();
            dgvAreaDomainEtlIdNull = new System.Windows.Forms.DataGridView();
            tlpMiddlePanelWithButtons = new System.Windows.Forms.TableLayoutPanel();
            btnNoMatch = new System.Windows.Forms.Button();
            btnAuto = new System.Windows.Forms.Button();
            btnMatch = new System.Windows.Forms.Button();
            splAreaDomainCategory = new System.Windows.Forms.SplitContainer();
            grpADC = new System.Windows.Forms.GroupBox();
            dgvADC = new System.Windows.Forms.DataGridView();
            toolTip = new System.Windows.Forms.ToolTip(components);
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMiddlePanelWithButtons).BeginInit();
            splMiddlePanelWithButtons.Panel1.SuspendLayout();
            splMiddlePanelWithButtons.Panel2.SuspendLayout();
            splMiddlePanelWithButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splAreaDomain).BeginInit();
            splAreaDomain.Panel1.SuspendLayout();
            splAreaDomain.Panel2.SuspendLayout();
            splAreaDomain.SuspendLayout();
            grpAreaDomain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvAreaDomain).BeginInit();
            grpAreaDomainEtlIdNull.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvAreaDomainEtlIdNull).BeginInit();
            tlpMiddlePanelWithButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splAreaDomainCategory).BeginInit();
            splAreaDomainCategory.Panel1.SuspendLayout();
            splAreaDomainCategory.SuspendLayout();
            grpADC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvADC).BeginInit();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 2);
            tlpMain.Controls.Add(lblMainCaption, 0, 0);
            tlpMain.Controls.Add(splMain, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1120, 623);
            tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnNext);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(4, 580);
            pnlButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(1112, 40);
            pnlButtons.TabIndex = 0;
            // 
            // btnNext
            // 
            btnNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnNext.Enabled = false;
            btnNext.Location = new System.Drawing.Point(933, 4);
            btnNext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(175, 35);
            btnNext.TabIndex = 0;
            btnNext.Text = "btnNext";
            btnNext.UseVisualStyleBackColor = true;
            btnNext.Visible = false;
            btnNext.Click += BtnNext_Click;
            // 
            // lblMainCaption
            // 
            lblMainCaption.AutoSize = true;
            lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblMainCaption.Location = new System.Drawing.Point(4, 0);
            lblMainCaption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblMainCaption.Name = "lblMainCaption";
            lblMainCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblMainCaption.Size = new System.Drawing.Size(1112, 35);
            lblMainCaption.TabIndex = 1;
            lblMainCaption.Text = "lblMainCaption";
            lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.Location = new System.Drawing.Point(4, 38);
            splMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splMain.Name = "splMain";
            splMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(splMiddlePanelWithButtons);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(splAreaDomainCategory);
            splMain.Size = new System.Drawing.Size(1112, 536);
            splMain.SplitterDistance = 284;
            splMain.SplitterWidth = 5;
            splMain.TabIndex = 2;
            // 
            // splMiddlePanelWithButtons
            // 
            splMiddlePanelWithButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            splMiddlePanelWithButtons.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            splMiddlePanelWithButtons.Location = new System.Drawing.Point(0, 0);
            splMiddlePanelWithButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splMiddlePanelWithButtons.Name = "splMiddlePanelWithButtons";
            splMiddlePanelWithButtons.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMiddlePanelWithButtons.Panel1
            // 
            splMiddlePanelWithButtons.Panel1.Controls.Add(splAreaDomain);
            // 
            // splMiddlePanelWithButtons.Panel2
            // 
            splMiddlePanelWithButtons.Panel2.Controls.Add(tlpMiddlePanelWithButtons);
            splMiddlePanelWithButtons.Size = new System.Drawing.Size(1112, 284);
            splMiddlePanelWithButtons.SplitterDistance = 231;
            splMiddlePanelWithButtons.SplitterWidth = 5;
            splMiddlePanelWithButtons.TabIndex = 0;
            // 
            // splAreaDomain
            // 
            splAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            splAreaDomain.Location = new System.Drawing.Point(0, 0);
            splAreaDomain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splAreaDomain.Name = "splAreaDomain";
            // 
            // splAreaDomain.Panel1
            // 
            splAreaDomain.Panel1.Controls.Add(grpAreaDomain);
            // 
            // splAreaDomain.Panel2
            // 
            splAreaDomain.Panel2.Controls.Add(grpAreaDomainEtlIdNull);
            splAreaDomain.Panel2Collapsed = true;
            splAreaDomain.Size = new System.Drawing.Size(1112, 231);
            splAreaDomain.SplitterDistance = 413;
            splAreaDomain.SplitterWidth = 5;
            splAreaDomain.TabIndex = 0;
            // 
            // grpAreaDomain
            // 
            grpAreaDomain.Controls.Add(dgvAreaDomain);
            grpAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAreaDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpAreaDomain.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpAreaDomain.Location = new System.Drawing.Point(0, 0);
            grpAreaDomain.Name = "grpAreaDomain";
            grpAreaDomain.Size = new System.Drawing.Size(1112, 231);
            grpAreaDomain.TabIndex = 0;
            grpAreaDomain.TabStop = false;
            // 
            // dgvAreaDomain
            // 
            dgvAreaDomain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvAreaDomain.Location = new System.Drawing.Point(3, 18);
            dgvAreaDomain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvAreaDomain.Name = "dgvAreaDomain";
            dgvAreaDomain.Size = new System.Drawing.Size(1106, 210);
            dgvAreaDomain.TabIndex = 2;
            dgvAreaDomain.CellBeginEdit += DgvAreaDomain_CellBeginEdit;
            dgvAreaDomain.CellContentClick += DgvAreaDomain_CellContentClick;
            dgvAreaDomain.CellPainting += DgvAreaDomain_CellPainting;
            dgvAreaDomain.CellToolTipTextNeeded += dgvAreaDomain_CellToolTipTextNeeded;
            dgvAreaDomain.CellValueChanged += DgvAreaDomain_CellValueChanged;
            dgvAreaDomain.CurrentCellDirtyStateChanged += DgvAreaDomain_CurrentCellDirtyStateChanged;
            dgvAreaDomain.SelectionChanged += DgvAreaDomain_SelectionChanged;
            // 
            // grpAreaDomainEtlIdNull
            // 
            grpAreaDomainEtlIdNull.Controls.Add(dgvAreaDomainEtlIdNull);
            grpAreaDomainEtlIdNull.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAreaDomainEtlIdNull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpAreaDomainEtlIdNull.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpAreaDomainEtlIdNull.Location = new System.Drawing.Point(0, 0);
            grpAreaDomainEtlIdNull.Name = "grpAreaDomainEtlIdNull";
            grpAreaDomainEtlIdNull.Size = new System.Drawing.Size(96, 100);
            grpAreaDomainEtlIdNull.TabIndex = 0;
            grpAreaDomainEtlIdNull.TabStop = false;
            // 
            // dgvAreaDomainEtlIdNull
            // 
            dgvAreaDomainEtlIdNull.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvAreaDomainEtlIdNull.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvAreaDomainEtlIdNull.Location = new System.Drawing.Point(3, 18);
            dgvAreaDomainEtlIdNull.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvAreaDomainEtlIdNull.Name = "dgvAreaDomainEtlIdNull";
            dgvAreaDomainEtlIdNull.ReadOnly = true;
            dgvAreaDomainEtlIdNull.Size = new System.Drawing.Size(90, 79);
            dgvAreaDomainEtlIdNull.TabIndex = 0;
            dgvAreaDomainEtlIdNull.CellPainting += DgvAreaDomainEtlIdNull_CellPainting;
            dgvAreaDomainEtlIdNull.CellToolTipTextNeeded += dgvAreaDomainEtlIdNull_CellToolTipTextNeeded;
            dgvAreaDomainEtlIdNull.SelectionChanged += dgvAreaDomainEtlIdNull_SelectionChanged;
            // 
            // tlpMiddlePanelWithButtons
            // 
            tlpMiddlePanelWithButtons.ColumnCount = 5;
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            tlpMiddlePanelWithButtons.Controls.Add(btnNoMatch, 3, 0);
            tlpMiddlePanelWithButtons.Controls.Add(btnAuto, 1, 0);
            tlpMiddlePanelWithButtons.Controls.Add(btnMatch, 2, 0);
            tlpMiddlePanelWithButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMiddlePanelWithButtons.Location = new System.Drawing.Point(0, 0);
            tlpMiddlePanelWithButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpMiddlePanelWithButtons.Name = "tlpMiddlePanelWithButtons";
            tlpMiddlePanelWithButtons.RowCount = 1;
            tlpMiddlePanelWithButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMiddlePanelWithButtons.Size = new System.Drawing.Size(1112, 48);
            tlpMiddlePanelWithButtons.TabIndex = 0;
            // 
            // btnNoMatch
            // 
            btnNoMatch.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnNoMatch.Enabled = false;
            btnNoMatch.Location = new System.Drawing.Point(627, 7);
            btnNoMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnNoMatch.Name = "btnNoMatch";
            btnNoMatch.Size = new System.Drawing.Size(175, 33);
            btnNoMatch.TabIndex = 1;
            btnNoMatch.Text = "btnNoMatch";
            btnNoMatch.UseVisualStyleBackColor = true;
            btnNoMatch.Click += BtnNoMatch_Click;
            // 
            // btnAuto
            // 
            btnAuto.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnAuto.Location = new System.Drawing.Point(253, 7);
            btnAuto.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnAuto.Name = "btnAuto";
            btnAuto.Size = new System.Drawing.Size(175, 33);
            btnAuto.TabIndex = 3;
            btnAuto.Text = "btnAuto";
            btnAuto.UseVisualStyleBackColor = true;
            btnAuto.Click += btnAuto_Click;
            // 
            // btnMatch
            // 
            btnMatch.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnMatch.Enabled = false;
            btnMatch.Location = new System.Drawing.Point(440, 7);
            btnMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnMatch.Name = "btnMatch";
            btnMatch.Size = new System.Drawing.Size(175, 33);
            btnMatch.TabIndex = 2;
            btnMatch.Text = "btnMatch";
            btnMatch.UseVisualStyleBackColor = true;
            btnMatch.Click += BtnMatch_Click;
            // 
            // splAreaDomainCategory
            // 
            splAreaDomainCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            splAreaDomainCategory.Location = new System.Drawing.Point(0, 0);
            splAreaDomainCategory.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splAreaDomainCategory.Name = "splAreaDomainCategory";
            // 
            // splAreaDomainCategory.Panel1
            // 
            splAreaDomainCategory.Panel1.AutoScroll = true;
            splAreaDomainCategory.Panel1.BackColor = System.Drawing.SystemColors.Window;
            splAreaDomainCategory.Panel1.Controls.Add(grpADC);
            // 
            // splAreaDomainCategory.Panel2
            // 
            splAreaDomainCategory.Panel2.AutoScroll = true;
            splAreaDomainCategory.Panel2.BackColor = System.Drawing.SystemColors.Window;
            splAreaDomainCategory.Size = new System.Drawing.Size(1112, 247);
            splAreaDomainCategory.SplitterDistance = 412;
            splAreaDomainCategory.SplitterWidth = 5;
            splAreaDomainCategory.TabIndex = 0;
            // 
            // grpADC
            // 
            grpADC.Controls.Add(dgvADC);
            grpADC.Dock = System.Windows.Forms.DockStyle.Fill;
            grpADC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpADC.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpADC.Location = new System.Drawing.Point(0, 0);
            grpADC.Name = "grpADC";
            grpADC.Size = new System.Drawing.Size(412, 247);
            grpADC.TabIndex = 0;
            grpADC.TabStop = false;
            // 
            // dgvADC
            // 
            dgvADC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvADC.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvADC.Location = new System.Drawing.Point(3, 18);
            dgvADC.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvADC.Name = "dgvADC";
            dgvADC.ReadOnly = true;
            dgvADC.Size = new System.Drawing.Size(406, 226);
            dgvADC.TabIndex = 0;
            dgvADC.CellToolTipTextNeeded += DgvADC_CellToolTipTextNeeded;
            // 
            // ControlAreaDomain
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(tlpMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "ControlAreaDomain";
            Size = new System.Drawing.Size(1120, 623);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            pnlButtons.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            splMiddlePanelWithButtons.Panel1.ResumeLayout(false);
            splMiddlePanelWithButtons.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMiddlePanelWithButtons).EndInit();
            splMiddlePanelWithButtons.ResumeLayout(false);
            splAreaDomain.Panel1.ResumeLayout(false);
            splAreaDomain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splAreaDomain).EndInit();
            splAreaDomain.ResumeLayout(false);
            grpAreaDomain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvAreaDomain).EndInit();
            grpAreaDomainEtlIdNull.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvAreaDomainEtlIdNull).EndInit();
            tlpMiddlePanelWithButtons.ResumeLayout(false);
            splAreaDomainCategory.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splAreaDomainCategory).EndInit();
            splAreaDomainCategory.ResumeLayout(false);
            grpADC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvADC).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;

        /// <summary>
        /// btnNext
        /// </summary>
        public System.Windows.Forms.Button btnNext;

        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.DataGridView dgvAreaDomain;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.DataGridView dgvAreaDomainEtlIdNull;

        /// <summary>
        /// btnNoMatch
        /// </summary>
        public System.Windows.Forms.Button btnNoMatch;

        /// <summary>
        /// btnMatch
        /// </summary>
        public System.Windows.Forms.Button btnMatch;

        private System.Windows.Forms.SplitContainer splAreaDomain;
        private System.Windows.Forms.SplitContainer splAreaDomainCategory;
        private System.Windows.Forms.DataGridView dgvADC;
        private System.Windows.Forms.SplitContainer splMiddlePanelWithButtons;

        /// <summary>
        /// TODO
        /// </summary>
        public System.Windows.Forms.Button btnAuto;

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TableLayoutPanel tlpMiddlePanelWithButtons;
        private System.Windows.Forms.GroupBox grpAreaDomain;
        private System.Windows.Forms.GroupBox grpAreaDomainEtlIdNull;
        private System.Windows.Forms.GroupBox grpADC;
    }

}