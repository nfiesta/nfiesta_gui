﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Atributové proměnné"</para>
    /// <para lang="en">Control "Attribute variables"</para>
    /// </summary>
    public partial class ControlAttributeVariables
            : UserControl, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Téma"</para>
        /// <para lang="en">Control "Topic"</para>
        /// </summary>
        private ControlTopic controlTopic;

        /// <summary>
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table</para>
        /// </summary>
        private readonly int? _idEtl;

        /// <summary>
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů</para>
        /// <para lang="en">List of panel and refyearset combination ids</para>
        /// </summary>
        private readonly List<int?> _refYearSetToPanelMapping;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        /// <summary>
        /// <para lang="cs">JSON, který vrací fn_etl_check_variables v cílové databázi</para>
        /// <para lang="en">JSON returned by fn_etl_check_variables in the target database</para>
        /// </summary>
        private string variablesTarget;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelCs;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelEn;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="idEtl">
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="refYearSetToPanelMapping">
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů</para>
        /// <para lang="en">List of panel and refyearset combination ids</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="selectedIndicatorLabelCs">
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </param>
        /// <param name="selectedIndicatorLabelEn">
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public ControlAttributeVariables(
            Control controlOwner,
            int? idEtl,
            List<int?> refYearSetToPanelMapping,
            int selectedExportConnectionId,
            string selectedIndicatorLabelCs,
            string selectedIndicatorLabelEn,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction)
        {
            InitializeComponent();

            _idEtl = idEtl;
            _refYearSetToPanelMapping = refYearSetToPanelMapping;
            _selectedExportConnectionId = selectedExportConnectionId;
            _selectedIndicatorLabelCs = selectedIndicatorLabelCs;
            _selectedIndicatorLabelEn = selectedIndicatorLabelEn;
            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;

            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlEtl ctrEtl)
                {
                    return ctrEtl;
                }

                else if (ControlOwner is ControlPanelRefyearset ctrPanelRefYearSet)
                {
                    return ctrPanelRefYearSet.CtrEtl;
                }

                else if (ControlOwner is ControlAreaDomain ctrAreaDomain)
                {
                    return ctrAreaDomain.CtrEtl;
                }

                else if (ControlOwner is ControlSubPopulation ctrSubPopulation)
                {
                    return ctrSubPopulation.CtrEtl;
                }

                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Zvolené kombinace panelů a roků měření</para>
        /// <para lang="en">Selected combinations of panels and reference-yearsets</para>
        /// </summary>
        public List<int?> SelectedRefyearsetsToPanels { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }


        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                                { "lblMainCaption",             "Kontrola minimálního rozsahu atributových členění" },
                                { "btnNext",                    "Další" },
                                { "btnCheck",                   "Zobrazit" },
                                { "msgNoCombinationSelected",   "Není vybrána žádná kombinace panelů a roků měření." },
                                { "msgCheckAllowed",            "Lze zobrazit pouze jednu zvolenou kombinaci panelů a roků měření, která není použitelná pro přenos dat." },
                                { "msgNotApplicableForEtl",     "Nelze zvolit kombinaci panelů a roků měření, která není použitelná pro přenos dat." },
                                { "referenceYearSet",           "Roky měření"},
                                { "useableForEtl",              "Použitelné pro přenos dat?"},
                                { "yes",                        "ano"},
                                { "no",                         "ne"},
                                { "subPopulation",              "Subpopulace"},
                                { "SPC",                        "Kategorie subpopulace"},
                                { "areaDomain",                 "Plošná doména"},
                                { "ADC",                        "Kategorie plošné domény"},
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlAttributeVariables") ?
                    languageFile.NationalVersion.Data["ControlAttributeVariables"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                                { "lblMainCaption",             "Kontrola minimálního rozsahu atributových členění" },
                                { "btnNext",                    "Další" },
                                { "btnCheck",                   "Zobrazit" },
                                { "msgNoCombinationSelected",   "No combination of panels and reference-yearsets is selected." },
                                { "msgCheckAllowed",            "Only one selected combination of panels and reference-yearsets can be viewed, which is not applicable for the data transfer." },
                                { "msgNotApplicableForEtl",     "It is not allowed to select a combination of panels and reference-yearsets that is not applicable to the data transfer." },
                                { "referenceYearSet",           "Reference-yearset"},
                                { "useableForEtl",              "Applicable to the data transfer?"},
                                { "yes",                        "yes"},
                                { "no",                         "no"},
                                { "subPopulation",              "Subpopulation"},
                                { "SPC",                        "Subpopulation category"},
                                { "areaDomain",                 "Area domain"},
                                { "ADC",                        "Area domain category"},
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlAttributeVariables") ?
                    languageFile.InternationalVersion.Data["ControlAttributeVariables"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            nationalLang = new NationalLang();

            Visible = (Database != null) &&
                 (Database.Postgres != null) &&
                 Database.Postgres.Initialized;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            btnNext.Text =
                labels.ContainsKey(key: "btnNext") ?
                labels["btnNext"] : String.Empty;

            btnCheck.Text =
                labels.ContainsKey(key: "btnCheck") ?
                labels["btnCheck"] : String.Empty;

            controlTopic?.InitializeLabels();

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            string variablesSource =
                TDFunctions.FnEtlGetVariables.Execute(
                    database: Database,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    idTEtlTargetVariable: _idEtl,
                    transaction: SourceTransaction);

            variablesTarget = NfiEstaFunctions.FnEtlCheckVariables.ExecuteScalar(
                FormUserPassword.TargetDatabase,
                variablesSource,
                TargetTransaction);

            DataTable dt = TDFunctions.FnEtlGetPanelRefYearSetCombinationsFalse.ExecuteQuery(
                database: Database,
                variables: variablesTarget,
                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                transaction: SourceTransaction);

            dgvPanelRefyearset.DataSource = dt;

            FormatDataGridView(dgvPanelRefyearset);

            dgvPanelRefyearset.Columns["refyearset2panel"].Visible = false;

            int panelColumnWidth = CalculateColumnWidth(dgvPanelRefyearset, "panel");
            int refyearsetColumnWidth = CalculateColumnWidth(dgvPanelRefyearset, "reference_year_set");

            dgvPanelRefyearset.Columns["panel"].Width = panelColumnWidth;
            dgvPanelRefyearset.Columns["reference_year_set"].Width = refyearsetColumnWidth;

            dgvPanelRefyearset.Columns["panel"].HeaderText = "Panel";

            Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    dgvPanelRefyearset.Columns["reference_year_set"].HeaderText = labels.ContainsKey(key: "referenceYearSet") ? labels["referenceYearSet"] : String.Empty;
                    dgvPanelRefyearset.Columns["useable4etl"].HeaderText = labels.ContainsKey(key: "useableForEtl") ? labels["useableForEtl"] : String.Empty;
                    break;

                case LanguageVersion.International:
                    dgvPanelRefyearset.Columns["reference_year_set"].HeaderText = labels.ContainsKey(key: "referenceYearSet") ? labels["referenceYearSet"] : String.Empty;
                    dgvPanelRefyearset.Columns["useable4etl"].HeaderText = labels.ContainsKey(key: "useableForEtl") ? labels["useableForEtl"] : String.Empty;
                    break;
            }

            //insert the checkedboxColumn in the dataGridView
            bool selectColumnExists = false;
            foreach (DataGridViewColumn column in dgvPanelRefyearset.Columns)
            {
                if (column.Name == "Select" && column is DataGridViewCheckBoxColumn) //because it appeared more times
                {
                    selectColumnExists = true;
                    break;
                }
            }

            if (!selectColumnExists)
            {
                DataGridViewCheckBoxColumn checkboxColumn = new DataGridViewCheckBoxColumn()
                {
                    Name = "Select",
                    HeaderText = "",
                    Width = 50
                };
                dgvPanelRefyearset.Columns.Insert(0, checkboxColumn); //insert the column at the first position;
            }
            else
            {
                dgvPanelRefyearset.Columns["Select"].DisplayIndex = 0; //move the existing "Select" column to the first position
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví záhlaví, viditelnost, formáty podle typu dat a zarovnání ve sloupcích daného DataGridView</para>
        /// <para lang="en">Sets header, visibility, formats by data type and alignment in columns of given DataGridView</para>
        /// </summary>
        /// <param name="dataGridView">
        /// <para lang="cs">Objekt DataGridView, který se má formátovat</para>
        /// <para lang="en">The DataGridView object to be formatted</para>
        /// </param>
        public void FormatDataGridView(DataGridView dataGridView)
        {
            // General:
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeRows = false;

            dataGridView.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle()
            {
                BackColor = Color.LightCyan
            };

            dataGridView.AutoGenerateColumns = false;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            dataGridView.BackgroundColor = SystemColors.Window;
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.None;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;

            dataGridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.DarkBlue,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = Color.White,
                SelectionBackColor = Color.DarkBlue,
                SelectionForeColor = Color.White,
                WrapMode = DataGridViewTriState.True
            };

            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            dataGridView.DefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.White,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = SystemColors.ControlText,
                Format = "N3",
                NullValue = "NULL",
                SelectionBackColor = Color.Bisque,
                SelectionForeColor = SystemColors.ControlText,
                WrapMode = DataGridViewTriState.False
            };

            dataGridView.EnableHeadersVisualStyles = false;
            dataGridView.MultiSelect = true;
            dataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView.RowHeadersVisible = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        /// <summary>
        /// <para lang="cs">Vypočítá šířku sloupce v DataGridView na základě nejdelšího textu ve sloupci</para>
        /// <para lang="en">Calculates the column width in a DataGridView based on the longest text in the column</para>
        /// </summary>
        /// <param name="dataGridView">
        /// <para lang="cs">DataGridView, ve kterém se nachází sloupec pro výpočet šířky</para>
        /// <para lang="en">The DataGridView that contains the column for width calculation</para>
        /// </param>
        /// <param name="columnName">
        /// <para lang="cs">Jméno sloupce, pro který se vypočítává šířka</para>
        /// <para lang="en">The name of the column for which the width is calculated</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Vypočítaná maximální šířka sloupce plus malý buffer</para>
        /// <para lang="en">The calculated maximum width of the column plus a small buffer</para>
        /// </returns>
        private int CalculateColumnWidth(DataGridView dataGridView, string columnName)
        {
            int maxWidth = 0;
            using (Graphics g = dataGridView.CreateGraphics())
            {
                SizeF size;
                Font font = dataGridView.DefaultCellStyle.Font;

                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    string cellText = row.Cells[columnName].Value.ToString();
                    size = g.MeasureString(cellText, font);
                    maxWidth = (int)Math.Max(maxWidth, size.Width);
                }

                size = g.MeasureString(dataGridView.Columns[columnName].HeaderText, font);
                maxWidth = (int)Math.Max(maxWidth, size.Width);
            }

            return maxWidth + 10; // Adding a small buffer
        }

        #endregion Methods


        #region Events

        #region NextClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">The delegate of the function handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void NextClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event NextClickHandler NextClick;

        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Invoking a click event on the "Next" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaiseNextClick(object sender, EventArgs e)
        {
            NextClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            SelectedRefyearsetsToPanels = new List<int?>();
            bool hasUsable4etlFalse = false;  // New variable to track if any selected row has useable4etl as false
            foreach (DataGridViewRow row in dgvPanelRefyearset.Rows)
            {
                if (row.Cells["Select"].Value != null && (bool)row.Cells["Select"].Value == true)
                {
                    // Check if the current selected row has useable4etl as false
                    if ((bool)row.Cells["useable4etl"].Value == false)
                    {
                        hasUsable4etlFalse = true;
                    }

                    int id = (int)row.Cells["refyearset2panel"].Value;
                    SelectedRefyearsetsToPanels.Add(id);
                }
            }

            Dictionary<string, string> messages = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);
            if (!SelectedRefyearsetsToPanels.Any())
            {
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoCombinationSelected") ?
                            messages["msgNoCombinationSelected"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
            else if (hasUsable4etlFalse)  // If any selected row has useable4etl as false, show a message box
            {
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNotApplicableForEtl") ?
                            messages["msgNotApplicableForEtl"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
            else
            {
                if (ControlOwner is ControlEtl)
                {
                    RaiseNextClick(sender, e);
                }
                else
                {
                    controlTopic = new ControlTopic(
                        controlOwner: this,
                        idEtl: _idEtl,
                        refYearSetToPanelMapping: SelectedRefyearsetsToPanels,
                        selectedExportConnectionId: _selectedExportConnectionId,
                        selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                        selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                        sourceTransaction: SourceTransaction,
                        targetTransaction: TargetTransaction)
                    {
                        Dock = DockStyle.Fill
                    };

                    controlTopic.LoadContent();
                    controlTopic.InitializeLabels();

                    CtrEtl?.ShowControl(control: controlTopic);
                }
            }
        }


        #endregion NextClick Event

        #endregion Events


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události překreslení zobrazení buněk v DataGridView dgvPanelRefyearset</para>
        /// <para lang="en">Event handler of the cell view redraw in DataGridView dgvPanelRefyearset</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvPanelRefyearset_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //change the standard display of boolean results in the useable4etl column to text
            if (e.ColumnIndex == dgvPanelRefyearset.Columns["useable4etl"].Index && e.RowIndex >= 0)
            {
                e.PaintBackground(e.CellBounds, false);

                Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                bool useableForEtl = (bool)e.Value;
                string text = (LanguageVersion == LanguageVersion.National && useableForEtl) ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty
                  : (LanguageVersion == LanguageVersion.National && !useableForEtl) ? labels.ContainsKey(key: "no") ? labels["no"] : String.Empty
                  : useableForEtl ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty : labels.ContainsKey(key: "no") ? labels["no"] : String.Empty;

                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    //set the font to Microsoft Sans Serif, 9.75pt
                    e.CellStyle.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular);

                    //move the text lower by 3 pixels
                    Rectangle textRect = new Rectangle(e.CellBounds.X, e.CellBounds.Y + 3,
                                                        e.CellBounds.Width, e.CellBounds.Height - 3);
                    e.Graphics.DrawString(text, e.CellStyle.Font, brush, textRect);
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zkontroluj"</para>
        /// <para lang="en">Handling the "Check" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCheck_Click(object sender, EventArgs e)
        {
            int checkedCount = 0;
            bool hasUseable4etlTrue = false;
            DataGridViewRow selectedRow = null;

            // Iterate through all the rows in the DataGridView
            foreach (DataGridViewRow row in dgvPanelRefyearset.Rows)
            {
                // Check if the checkbox is checked
                if (Convert.ToBoolean(row.Cells["Select"].Value))
                {
                    checkedCount++;

                    // Get the value in the "useable4etl" column for the selected row
                    bool isUseable4etl = Convert.ToBoolean(row.Cells["useable4etl"].Value);

                    // Check if "useable4etl" is true
                    if (isUseable4etl)
                    {
                        hasUseable4etlTrue = true;
                    }

                    selectedRow = row;
                }
            }

            Dictionary<string, string> messages = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);
            if (checkedCount == 0)
            {
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoCombinationSelected") ?
                            messages["msgNoCombinationSelected"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (checkedCount > 1)
            {
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgCheckAllowed") ?
                            messages["msgCheckAllowed"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (hasUseable4etlTrue)
            {
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgCheckAllowed") ?
                            messages["msgCheckAllowed"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            // If execution has reached this point, it means only one row was selected and it has "useable4etl" as false
            if (selectedRow != null)
            {
                // Get the value from the "panel" and "reference_year_set" column for the checked row
                string panel = selectedRow.Cells["panel"].Value.ToString();
                string referenceYearSet = selectedRow.Cells["reference_year_set"].Value.ToString();

                DataTable result = TDFunctions.FnEtlGetVariablesFalse.ExecuteQuery(
                    database: Database,
                    variables: variablesTarget,
                    panel: panel,
                    referenceYearSet: referenceYearSet,
                    nationalLang.NatLang,
                    transaction: SourceTransaction);

                dgvAttrVariables.DataSource = result;

                FormatDataGridView(dgvAttrVariables);

                switch (LanguageVersion)
                {
                    case LanguageVersion.National:
                        dgvAttrVariables.Columns["sub_population_en"].Visible = false;
                        dgvAttrVariables.Columns["sub_population_category_en"].Visible = false;
                        dgvAttrVariables.Columns["area_domain_en"].Visible = false;
                        dgvAttrVariables.Columns["area_domain_category_en"].Visible = false;
                        dgvAttrVariables.Columns["sub_population"].HeaderText = messages.ContainsKey(key: "subPopulation") ? messages["subPopulation"] : String.Empty;
                        dgvAttrVariables.Columns["sub_population_category"].HeaderText = messages.ContainsKey(key: "SPC") ? messages["SPC"] : String.Empty;
                        dgvAttrVariables.Columns["area_domain"].HeaderText = messages.ContainsKey(key: "areaDomain") ? messages["areaDomain"] : String.Empty;
                        dgvAttrVariables.Columns["area_domain_category"].HeaderText = messages.ContainsKey(key: "ADC") ? messages["ADC"] : String.Empty;
                        break;

                    case LanguageVersion.International:
                        dgvAttrVariables.Columns["sub_population"].Visible = false;
                        dgvAttrVariables.Columns["sub_population_category"].Visible = false;
                        dgvAttrVariables.Columns["area_domain"].Visible = false;
                        dgvAttrVariables.Columns["area_domain_category"].Visible = false;
                        dgvAttrVariables.Columns["sub_population_en"].HeaderText = messages.ContainsKey(key: "subPopulation") ? messages["subPopulation"] : String.Empty;
                        dgvAttrVariables.Columns["sub_population_category_en"].HeaderText = messages.ContainsKey(key: "SPC") ? messages["SPC"] : String.Empty;
                        dgvAttrVariables.Columns["area_domain_en"].HeaderText = messages.ContainsKey(key: "areaDomain") ? messages["areaDomain"] : String.Empty;
                        dgvAttrVariables.Columns["area_domain_category_en"].HeaderText = messages.ContainsKey(key: "ADC") ? messages["ADC"] : String.Empty;
                        break;
                }

                splMain.Panel2Collapsed = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost změny hodnoty buňky v DataGridView.</para>
        /// <para lang="en">Handles the CellValueChanged event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvPanelRefyearset_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // Ensure that the event is fired after initialization
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                // Check if the change was on the "Select" column
                if (dgvPanelRefyearset.Columns[e.ColumnIndex].Name == "Select")
                {
                    splMain.Panel2Collapsed = true;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost změny stavu "špinavé" buňky (buňky s neuloženými změnami) v DataGridView.</para>
        /// <para lang="en">Handles the CurrentCellDirtyStateChanged event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvPanelRefyearset_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            // Also respond when the same checkbox that was checked is unchecked
            if (dgvPanelRefyearset.IsCurrentCellDirty)
            {
                // Commit the new value.
                dgvPanelRefyearset.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        #endregion Event Handlers


    }

}