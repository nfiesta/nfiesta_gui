﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class FormCheckMetadata
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.splHorizontal = new System.Windows.Forms.SplitContainer();
            this.grpDiff = new System.Windows.Forms.GroupBox();
            this.dgvDiff = new System.Windows.Forms.DataGridView();
            this.splMiddlePart = new System.Windows.Forms.SplitContainer();
            this.grpSelectedTargetVariable = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tlpSelectedTargetVariable = new System.Windows.Forms.TableLayoutPanel();
            this.lblUseNegative = new System.Windows.Forms.Label();
            this.lblPopulationDes = new System.Windows.Forms.Label();
            this.lblAreaDomainDes = new System.Windows.Forms.Label();
            this.lblDefinitionVariantDes = new System.Windows.Forms.Label();
            this.lblVersionDes = new System.Windows.Forms.Label();
            this.lblObjectDes = new System.Windows.Forms.Label();
            this.lblObject = new System.Windows.Forms.Label();
            this.lblObjectTypeDes = new System.Windows.Forms.Label();
            this.lblObjectType = new System.Windows.Forms.Label();
            this.lblLocalDensityDes = new System.Windows.Forms.Label();
            this.lblUnitDes = new System.Windows.Forms.Label();
            this.lblStateOrChangeDes = new System.Windows.Forms.Label();
            this.lblIndicatorDes = new System.Windows.Forms.Label();
            this.lblIndicator = new System.Windows.Forms.Label();
            this.lblStateOrChange = new System.Windows.Forms.Label();
            this.lblUnit = new System.Windows.Forms.Label();
            this.lblLocalDensity = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblDefinitionVariant = new System.Windows.Forms.Label();
            this.lblAreaDomain = new System.Windows.Forms.Label();
            this.lblPopulation = new System.Windows.Forms.Label();
            this.rtxSelectedIndicator = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedIndicatorDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedStateOrChange = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedStateOrChangeDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedUnit = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedUnitDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedLocalDensity = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedLocalDensityDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedObjectType = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedObjectTypeDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedObject = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedObjectDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedVersion = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedVersionDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedDefinitionVariant = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedDefinitionVariantDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedAreaDomain = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedAreaDomainDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedPopulation = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedPopulationDes = new System.Windows.Forms.RichTextBox();
            this.rtxSelectedUseNegative = new System.Windows.Forms.RichTextBox();
            this.grpTargetVariablesInTargetDatabase = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tlpTargetVariablesInTargetDatabase = new System.Windows.Forms.TableLayoutPanel();
            this.rtxTargetDBIndicator = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBIndicatorDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBStateOrChange = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBStateOrChangeDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBUnit = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBUnitDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBLocalDensity = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBLocalDensityDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBObjectType = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBObjectTypeDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBObject = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBObjectDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBVersion = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBVersionDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBDefinitionVariant = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBDefinitionVariantDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBAreaDomain = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBAreaDomainDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBPopulation = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBPopulationDes = new System.Windows.Forms.RichTextBox();
            this.rtxTargetDBUseNegative = new System.Windows.Forms.RichTextBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.tsmiLanguage = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tlpMain.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splHorizontal)).BeginInit();
            this.splHorizontal.Panel1.SuspendLayout();
            this.splHorizontal.Panel2.SuspendLayout();
            this.splHorizontal.SuspendLayout();
            this.grpDiff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splMiddlePart)).BeginInit();
            this.splMiddlePart.Panel1.SuspendLayout();
            this.splMiddlePart.Panel2.SuspendLayout();
            this.splMiddlePart.SuspendLayout();
            this.grpSelectedTargetVariable.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tlpSelectedTargetVariable.SuspendLayout();
            this.grpTargetVariablesInTargetDatabase.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tlpTargetVariablesInTargetDatabase.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.pnlButtons, 0, 3);
            this.tlpMain.Controls.Add(this.lblMainCaption, 0, 1);
            this.tlpMain.Controls.Add(this.splHorizontal, 0, 2);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 4;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(944, 501);
            this.tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnInsert);
            this.pnlButtons.Controls.Add(this.btnUpdate);
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(3, 464);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(938, 34);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnInsert
            // 
            this.btnInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInsert.Enabled = false;
            this.btnInsert.Location = new System.Drawing.Point(470, 4);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(150, 30);
            this.btnInsert.TabIndex = 4;
            this.btnInsert.Text = "btnInsert";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Location = new System.Drawing.Point(626, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(150, 30);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "btnUpdate";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(782, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.AutoSize = true;
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(3, 20);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(938, 1);
            this.lblMainCaption.TabIndex = 1;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splHorizontal
            // 
            this.splHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splHorizontal.Location = new System.Drawing.Point(3, 24);
            this.splHorizontal.Name = "splHorizontal";
            this.splHorizontal.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splHorizontal.Panel1
            // 
            this.splHorizontal.Panel1.Controls.Add(this.grpDiff);
            // 
            // splHorizontal.Panel2
            // 
            this.splHorizontal.Panel2.Controls.Add(this.splMiddlePart);
            this.splHorizontal.Panel2Collapsed = true;
            this.splHorizontal.Size = new System.Drawing.Size(938, 434);
            this.splHorizontal.SplitterDistance = 106;
            this.splHorizontal.TabIndex = 2;
            // 
            // grpDiff
            // 
            this.grpDiff.Controls.Add(this.dgvDiff);
            this.grpDiff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpDiff.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpDiff.Location = new System.Drawing.Point(0, 0);
            this.grpDiff.Name = "grpDiff";
            this.grpDiff.Size = new System.Drawing.Size(938, 434);
            this.grpDiff.TabIndex = 3;
            this.grpDiff.TabStop = false;
            // 
            // dgvDiff
            // 
            this.dgvDiff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDiff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDiff.Location = new System.Drawing.Point(3, 18);
            this.dgvDiff.Name = "dgvDiff";
            this.dgvDiff.Size = new System.Drawing.Size(932, 413);
            this.dgvDiff.TabIndex = 0;
            this.dgvDiff.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvDiff_CellContentClick);
            this.dgvDiff.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DgvDiff_CellPainting);
            this.dgvDiff.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvDiff_CellValueChanged);
            this.dgvDiff.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvDiff_CurrentCellDirtyStateChanged);
            // 
            // splMiddlePart
            // 
            this.splMiddlePart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splMiddlePart.Location = new System.Drawing.Point(0, 0);
            this.splMiddlePart.Name = "splMiddlePart";
            // 
            // splMiddlePart.Panel1
            // 
            this.splMiddlePart.Panel1.AutoScroll = true;
            this.splMiddlePart.Panel1.Controls.Add(this.grpSelectedTargetVariable);
            // 
            // splMiddlePart.Panel2
            // 
            this.splMiddlePart.Panel2.AutoScroll = true;
            this.splMiddlePart.Panel2.Controls.Add(this.grpTargetVariablesInTargetDatabase);
            this.splMiddlePart.Size = new System.Drawing.Size(150, 46);
            this.splMiddlePart.SplitterDistance = 75;
            this.splMiddlePart.TabIndex = 2;
            // 
            // grpSelectedTargetVariable
            // 
            this.grpSelectedTargetVariable.Controls.Add(this.panel1);
            this.grpSelectedTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSelectedTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpSelectedTargetVariable.ForeColor = System.Drawing.SystemColors.WindowText;
            this.grpSelectedTargetVariable.Location = new System.Drawing.Point(0, 0);
            this.grpSelectedTargetVariable.Name = "grpSelectedTargetVariable";
            this.grpSelectedTargetVariable.Size = new System.Drawing.Size(75, 46);
            this.grpSelectedTargetVariable.TabIndex = 1;
            this.grpSelectedTargetVariable.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.tlpSelectedTargetVariable);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(69, 25);
            this.panel1.TabIndex = 0;
            // 
            // tlpSelectedTargetVariable
            // 
            this.tlpSelectedTargetVariable.AutoScroll = true;
            this.tlpSelectedTargetVariable.ColumnCount = 2;
            this.tlpSelectedTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 245F));
            this.tlpSelectedTargetVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSelectedTargetVariable.Controls.Add(this.lblUseNegative, 0, 20);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblPopulationDes, 0, 19);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblAreaDomainDes, 0, 17);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblDefinitionVariantDes, 0, 15);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblVersionDes, 0, 13);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblObjectDes, 0, 11);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblObject, 0, 10);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblObjectTypeDes, 0, 9);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblObjectType, 0, 8);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblLocalDensityDes, 0, 7);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblUnitDes, 0, 5);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblStateOrChangeDes, 0, 3);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblIndicatorDes, 0, 1);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblIndicator, 0, 0);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblStateOrChange, 0, 2);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblUnit, 0, 4);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblLocalDensity, 0, 6);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblVersion, 0, 12);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblDefinitionVariant, 0, 14);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblAreaDomain, 0, 16);
            this.tlpSelectedTargetVariable.Controls.Add(this.lblPopulation, 0, 18);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedIndicator, 1, 0);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedIndicatorDes, 1, 1);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedStateOrChange, 1, 2);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedStateOrChangeDes, 1, 3);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedUnit, 1, 4);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedUnitDes, 1, 5);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedLocalDensity, 1, 6);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedLocalDensityDes, 1, 7);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedObjectType, 1, 8);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedObjectTypeDes, 1, 9);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedObject, 1, 10);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedObjectDes, 1, 11);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedVersion, 1, 12);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedVersionDes, 1, 13);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedDefinitionVariant, 1, 14);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedDefinitionVariantDes, 1, 15);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedAreaDomain, 1, 16);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedAreaDomainDes, 1, 17);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedPopulation, 1, 18);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedPopulationDes, 1, 19);
            this.tlpSelectedTargetVariable.Controls.Add(this.rtxSelectedUseNegative, 1, 20);
            this.tlpSelectedTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSelectedTargetVariable.Location = new System.Drawing.Point(0, 0);
            this.tlpSelectedTargetVariable.Name = "tlpSelectedTargetVariable";
            this.tlpSelectedTargetVariable.RowCount = 22;
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpSelectedTargetVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSelectedTargetVariable.Size = new System.Drawing.Size(69, 25);
            this.tlpSelectedTargetVariable.TabIndex = 0;
            // 
            // lblUseNegative
            // 
            this.lblUseNegative.AutoSize = true;
            this.lblUseNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUseNegative.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblUseNegative.Location = new System.Drawing.Point(3, 560);
            this.lblUseNegative.Name = "lblUseNegative";
            this.lblUseNegative.Size = new System.Drawing.Size(239, 28);
            this.lblUseNegative.TabIndex = 44;
            this.lblUseNegative.Text = "lblUseNegative";
            this.lblUseNegative.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPopulationDes
            // 
            this.lblPopulationDes.AutoSize = true;
            this.lblPopulationDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPopulationDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblPopulationDes.Location = new System.Drawing.Point(3, 532);
            this.lblPopulationDes.Name = "lblPopulationDes";
            this.lblPopulationDes.Size = new System.Drawing.Size(239, 28);
            this.lblPopulationDes.TabIndex = 42;
            this.lblPopulationDes.Text = "lblPopulationDes";
            this.lblPopulationDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAreaDomainDes
            // 
            this.lblAreaDomainDes.AutoSize = true;
            this.lblAreaDomainDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAreaDomainDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblAreaDomainDes.Location = new System.Drawing.Point(3, 476);
            this.lblAreaDomainDes.Name = "lblAreaDomainDes";
            this.lblAreaDomainDes.Size = new System.Drawing.Size(239, 28);
            this.lblAreaDomainDes.TabIndex = 40;
            this.lblAreaDomainDes.Text = "lblAreaDomainDes";
            this.lblAreaDomainDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDefinitionVariantDes
            // 
            this.lblDefinitionVariantDes.AutoSize = true;
            this.lblDefinitionVariantDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDefinitionVariantDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDefinitionVariantDes.Location = new System.Drawing.Point(3, 420);
            this.lblDefinitionVariantDes.Name = "lblDefinitionVariantDes";
            this.lblDefinitionVariantDes.Size = new System.Drawing.Size(239, 28);
            this.lblDefinitionVariantDes.TabIndex = 38;
            this.lblDefinitionVariantDes.Text = "lblDefinitionVariantDes";
            this.lblDefinitionVariantDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVersionDes
            // 
            this.lblVersionDes.AutoSize = true;
            this.lblVersionDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVersionDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblVersionDes.Location = new System.Drawing.Point(3, 364);
            this.lblVersionDes.Name = "lblVersionDes";
            this.lblVersionDes.Size = new System.Drawing.Size(239, 28);
            this.lblVersionDes.TabIndex = 36;
            this.lblVersionDes.Text = "lblVersionDes";
            this.lblVersionDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblObjectDes
            // 
            this.lblObjectDes.AutoSize = true;
            this.lblObjectDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblObjectDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblObjectDes.Location = new System.Drawing.Point(3, 308);
            this.lblObjectDes.Name = "lblObjectDes";
            this.lblObjectDes.Size = new System.Drawing.Size(239, 28);
            this.lblObjectDes.TabIndex = 34;
            this.lblObjectDes.Text = "lblObjectDes";
            this.lblObjectDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblObject
            // 
            this.lblObject.AutoSize = true;
            this.lblObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblObject.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblObject.Location = new System.Drawing.Point(3, 280);
            this.lblObject.Name = "lblObject";
            this.lblObject.Size = new System.Drawing.Size(239, 28);
            this.lblObject.TabIndex = 32;
            this.lblObject.Text = "lblObject";
            this.lblObject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblObjectTypeDes
            // 
            this.lblObjectTypeDes.AutoSize = true;
            this.lblObjectTypeDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblObjectTypeDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblObjectTypeDes.Location = new System.Drawing.Point(3, 252);
            this.lblObjectTypeDes.Name = "lblObjectTypeDes";
            this.lblObjectTypeDes.Size = new System.Drawing.Size(239, 28);
            this.lblObjectTypeDes.TabIndex = 30;
            this.lblObjectTypeDes.Text = "lblObjectTypeDes";
            this.lblObjectTypeDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblObjectType
            // 
            this.lblObjectType.AutoSize = true;
            this.lblObjectType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblObjectType.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblObjectType.Location = new System.Drawing.Point(3, 224);
            this.lblObjectType.Name = "lblObjectType";
            this.lblObjectType.Size = new System.Drawing.Size(239, 28);
            this.lblObjectType.TabIndex = 28;
            this.lblObjectType.Text = "lblObjectType";
            this.lblObjectType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocalDensityDes
            // 
            this.lblLocalDensityDes.AutoSize = true;
            this.lblLocalDensityDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLocalDensityDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblLocalDensityDes.Location = new System.Drawing.Point(3, 196);
            this.lblLocalDensityDes.Name = "lblLocalDensityDes";
            this.lblLocalDensityDes.Size = new System.Drawing.Size(239, 28);
            this.lblLocalDensityDes.TabIndex = 26;
            this.lblLocalDensityDes.Text = "lblLocalDensityDes";
            this.lblLocalDensityDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnitDes
            // 
            this.lblUnitDes.AutoSize = true;
            this.lblUnitDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnitDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblUnitDes.Location = new System.Drawing.Point(3, 140);
            this.lblUnitDes.Name = "lblUnitDes";
            this.lblUnitDes.Size = new System.Drawing.Size(239, 28);
            this.lblUnitDes.TabIndex = 24;
            this.lblUnitDes.Text = "lblUnitDes";
            this.lblUnitDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateOrChangeDes
            // 
            this.lblStateOrChangeDes.AutoSize = true;
            this.lblStateOrChangeDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStateOrChangeDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblStateOrChangeDes.Location = new System.Drawing.Point(3, 84);
            this.lblStateOrChangeDes.Name = "lblStateOrChangeDes";
            this.lblStateOrChangeDes.Size = new System.Drawing.Size(239, 28);
            this.lblStateOrChangeDes.TabIndex = 22;
            this.lblStateOrChangeDes.Text = "lblStateOrChangeDes";
            this.lblStateOrChangeDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblIndicatorDes
            // 
            this.lblIndicatorDes.AutoSize = true;
            this.lblIndicatorDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIndicatorDes.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblIndicatorDes.Location = new System.Drawing.Point(3, 28);
            this.lblIndicatorDes.Name = "lblIndicatorDes";
            this.lblIndicatorDes.Size = new System.Drawing.Size(239, 28);
            this.lblIndicatorDes.TabIndex = 20;
            this.lblIndicatorDes.Text = "lblIndicatorDes";
            this.lblIndicatorDes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblIndicator
            // 
            this.lblIndicator.AutoSize = true;
            this.lblIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIndicator.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblIndicator.Location = new System.Drawing.Point(3, 0);
            this.lblIndicator.Name = "lblIndicator";
            this.lblIndicator.Size = new System.Drawing.Size(239, 28);
            this.lblIndicator.TabIndex = 0;
            this.lblIndicator.Text = "lblIndicator";
            this.lblIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateOrChange
            // 
            this.lblStateOrChange.AutoSize = true;
            this.lblStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStateOrChange.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblStateOrChange.Location = new System.Drawing.Point(3, 56);
            this.lblStateOrChange.Name = "lblStateOrChange";
            this.lblStateOrChange.Size = new System.Drawing.Size(239, 28);
            this.lblStateOrChange.TabIndex = 1;
            this.lblStateOrChange.Text = "lblStateOrChange";
            this.lblStateOrChange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblUnit.Location = new System.Drawing.Point(3, 112);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(239, 28);
            this.lblUnit.TabIndex = 2;
            this.lblUnit.Text = "lblUnit";
            this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocalDensity
            // 
            this.lblLocalDensity.AutoSize = true;
            this.lblLocalDensity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLocalDensity.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblLocalDensity.Location = new System.Drawing.Point(3, 168);
            this.lblLocalDensity.Name = "lblLocalDensity";
            this.lblLocalDensity.Size = new System.Drawing.Size(239, 28);
            this.lblLocalDensity.TabIndex = 3;
            this.lblLocalDensity.Text = "lblLocalDensity";
            this.lblLocalDensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVersion.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblVersion.Location = new System.Drawing.Point(3, 336);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(239, 28);
            this.lblVersion.TabIndex = 4;
            this.lblVersion.Text = "lblVersion";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDefinitionVariant
            // 
            this.lblDefinitionVariant.AutoSize = true;
            this.lblDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDefinitionVariant.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDefinitionVariant.Location = new System.Drawing.Point(3, 392);
            this.lblDefinitionVariant.Name = "lblDefinitionVariant";
            this.lblDefinitionVariant.Size = new System.Drawing.Size(239, 28);
            this.lblDefinitionVariant.TabIndex = 5;
            this.lblDefinitionVariant.Text = "lblDefinitionVariant";
            this.lblDefinitionVariant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAreaDomain
            // 
            this.lblAreaDomain.AutoSize = true;
            this.lblAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAreaDomain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblAreaDomain.Location = new System.Drawing.Point(3, 448);
            this.lblAreaDomain.Name = "lblAreaDomain";
            this.lblAreaDomain.Size = new System.Drawing.Size(239, 28);
            this.lblAreaDomain.TabIndex = 6;
            this.lblAreaDomain.Text = "lblAreaDomain";
            this.lblAreaDomain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPopulation
            // 
            this.lblPopulation.AutoSize = true;
            this.lblPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPopulation.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblPopulation.Location = new System.Drawing.Point(3, 504);
            this.lblPopulation.Name = "lblPopulation";
            this.lblPopulation.Size = new System.Drawing.Size(239, 28);
            this.lblPopulation.TabIndex = 7;
            this.lblPopulation.Text = "lblPopulation";
            this.lblPopulation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rtxSelectedIndicator
            // 
            this.rtxSelectedIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedIndicator.Location = new System.Drawing.Point(248, 3);
            this.rtxSelectedIndicator.Name = "rtxSelectedIndicator";
            this.rtxSelectedIndicator.ReadOnly = true;
            this.rtxSelectedIndicator.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedIndicator.TabIndex = 46;
            this.rtxSelectedIndicator.Text = "";
            // 
            // rtxSelectedIndicatorDes
            // 
            this.rtxSelectedIndicatorDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedIndicatorDes.Location = new System.Drawing.Point(248, 31);
            this.rtxSelectedIndicatorDes.Name = "rtxSelectedIndicatorDes";
            this.rtxSelectedIndicatorDes.ReadOnly = true;
            this.rtxSelectedIndicatorDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedIndicatorDes.TabIndex = 47;
            this.rtxSelectedIndicatorDes.Text = "";
            // 
            // rtxSelectedStateOrChange
            // 
            this.rtxSelectedStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedStateOrChange.Location = new System.Drawing.Point(248, 59);
            this.rtxSelectedStateOrChange.Name = "rtxSelectedStateOrChange";
            this.rtxSelectedStateOrChange.ReadOnly = true;
            this.rtxSelectedStateOrChange.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedStateOrChange.TabIndex = 48;
            this.rtxSelectedStateOrChange.Text = "";
            // 
            // rtxSelectedStateOrChangeDes
            // 
            this.rtxSelectedStateOrChangeDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedStateOrChangeDes.Location = new System.Drawing.Point(248, 87);
            this.rtxSelectedStateOrChangeDes.Name = "rtxSelectedStateOrChangeDes";
            this.rtxSelectedStateOrChangeDes.ReadOnly = true;
            this.rtxSelectedStateOrChangeDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedStateOrChangeDes.TabIndex = 49;
            this.rtxSelectedStateOrChangeDes.Text = "";
            // 
            // rtxSelectedUnit
            // 
            this.rtxSelectedUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedUnit.Location = new System.Drawing.Point(248, 115);
            this.rtxSelectedUnit.Name = "rtxSelectedUnit";
            this.rtxSelectedUnit.ReadOnly = true;
            this.rtxSelectedUnit.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedUnit.TabIndex = 50;
            this.rtxSelectedUnit.Text = "";
            // 
            // rtxSelectedUnitDes
            // 
            this.rtxSelectedUnitDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedUnitDes.Location = new System.Drawing.Point(248, 143);
            this.rtxSelectedUnitDes.Name = "rtxSelectedUnitDes";
            this.rtxSelectedUnitDes.ReadOnly = true;
            this.rtxSelectedUnitDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedUnitDes.TabIndex = 51;
            this.rtxSelectedUnitDes.Text = "";
            // 
            // rtxSelectedLocalDensity
            // 
            this.rtxSelectedLocalDensity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedLocalDensity.Location = new System.Drawing.Point(248, 171);
            this.rtxSelectedLocalDensity.Name = "rtxSelectedLocalDensity";
            this.rtxSelectedLocalDensity.ReadOnly = true;
            this.rtxSelectedLocalDensity.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedLocalDensity.TabIndex = 52;
            this.rtxSelectedLocalDensity.Text = "";
            // 
            // rtxSelectedLocalDensityDes
            // 
            this.rtxSelectedLocalDensityDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedLocalDensityDes.Location = new System.Drawing.Point(248, 199);
            this.rtxSelectedLocalDensityDes.Name = "rtxSelectedLocalDensityDes";
            this.rtxSelectedLocalDensityDes.ReadOnly = true;
            this.rtxSelectedLocalDensityDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedLocalDensityDes.TabIndex = 53;
            this.rtxSelectedLocalDensityDes.Text = "";
            // 
            // rtxSelectedObjectType
            // 
            this.rtxSelectedObjectType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedObjectType.Location = new System.Drawing.Point(248, 227);
            this.rtxSelectedObjectType.Name = "rtxSelectedObjectType";
            this.rtxSelectedObjectType.ReadOnly = true;
            this.rtxSelectedObjectType.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedObjectType.TabIndex = 54;
            this.rtxSelectedObjectType.Text = "";
            // 
            // rtxSelectedObjectTypeDes
            // 
            this.rtxSelectedObjectTypeDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedObjectTypeDes.Location = new System.Drawing.Point(248, 255);
            this.rtxSelectedObjectTypeDes.Name = "rtxSelectedObjectTypeDes";
            this.rtxSelectedObjectTypeDes.ReadOnly = true;
            this.rtxSelectedObjectTypeDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedObjectTypeDes.TabIndex = 55;
            this.rtxSelectedObjectTypeDes.Text = "";
            // 
            // rtxSelectedObject
            // 
            this.rtxSelectedObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedObject.Location = new System.Drawing.Point(248, 283);
            this.rtxSelectedObject.Name = "rtxSelectedObject";
            this.rtxSelectedObject.ReadOnly = true;
            this.rtxSelectedObject.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedObject.TabIndex = 56;
            this.rtxSelectedObject.Text = "";
            // 
            // rtxSelectedObjectDes
            // 
            this.rtxSelectedObjectDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedObjectDes.Location = new System.Drawing.Point(248, 311);
            this.rtxSelectedObjectDes.Name = "rtxSelectedObjectDes";
            this.rtxSelectedObjectDes.ReadOnly = true;
            this.rtxSelectedObjectDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedObjectDes.TabIndex = 57;
            this.rtxSelectedObjectDes.Text = "";
            // 
            // rtxSelectedVersion
            // 
            this.rtxSelectedVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedVersion.Location = new System.Drawing.Point(248, 339);
            this.rtxSelectedVersion.Name = "rtxSelectedVersion";
            this.rtxSelectedVersion.ReadOnly = true;
            this.rtxSelectedVersion.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedVersion.TabIndex = 58;
            this.rtxSelectedVersion.Text = "";
            // 
            // rtxSelectedVersionDes
            // 
            this.rtxSelectedVersionDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedVersionDes.Location = new System.Drawing.Point(248, 367);
            this.rtxSelectedVersionDes.Name = "rtxSelectedVersionDes";
            this.rtxSelectedVersionDes.ReadOnly = true;
            this.rtxSelectedVersionDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedVersionDes.TabIndex = 59;
            this.rtxSelectedVersionDes.Text = "";
            // 
            // rtxSelectedDefinitionVariant
            // 
            this.rtxSelectedDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedDefinitionVariant.Location = new System.Drawing.Point(248, 395);
            this.rtxSelectedDefinitionVariant.Name = "rtxSelectedDefinitionVariant";
            this.rtxSelectedDefinitionVariant.ReadOnly = true;
            this.rtxSelectedDefinitionVariant.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedDefinitionVariant.TabIndex = 60;
            this.rtxSelectedDefinitionVariant.Text = "";
            // 
            // rtxSelectedDefinitionVariantDes
            // 
            this.rtxSelectedDefinitionVariantDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedDefinitionVariantDes.Location = new System.Drawing.Point(248, 423);
            this.rtxSelectedDefinitionVariantDes.Name = "rtxSelectedDefinitionVariantDes";
            this.rtxSelectedDefinitionVariantDes.ReadOnly = true;
            this.rtxSelectedDefinitionVariantDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedDefinitionVariantDes.TabIndex = 61;
            this.rtxSelectedDefinitionVariantDes.Text = "";
            // 
            // rtxSelectedAreaDomain
            // 
            this.rtxSelectedAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedAreaDomain.Location = new System.Drawing.Point(248, 451);
            this.rtxSelectedAreaDomain.Name = "rtxSelectedAreaDomain";
            this.rtxSelectedAreaDomain.ReadOnly = true;
            this.rtxSelectedAreaDomain.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedAreaDomain.TabIndex = 62;
            this.rtxSelectedAreaDomain.Text = "";
            // 
            // rtxSelectedAreaDomainDes
            // 
            this.rtxSelectedAreaDomainDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedAreaDomainDes.Location = new System.Drawing.Point(248, 479);
            this.rtxSelectedAreaDomainDes.Name = "rtxSelectedAreaDomainDes";
            this.rtxSelectedAreaDomainDes.ReadOnly = true;
            this.rtxSelectedAreaDomainDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedAreaDomainDes.TabIndex = 63;
            this.rtxSelectedAreaDomainDes.Text = "";
            // 
            // rtxSelectedPopulation
            // 
            this.rtxSelectedPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedPopulation.Location = new System.Drawing.Point(248, 507);
            this.rtxSelectedPopulation.Name = "rtxSelectedPopulation";
            this.rtxSelectedPopulation.ReadOnly = true;
            this.rtxSelectedPopulation.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedPopulation.TabIndex = 64;
            this.rtxSelectedPopulation.Text = "";
            // 
            // rtxSelectedPopulationDes
            // 
            this.rtxSelectedPopulationDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedPopulationDes.Location = new System.Drawing.Point(248, 535);
            this.rtxSelectedPopulationDes.Name = "rtxSelectedPopulationDes";
            this.rtxSelectedPopulationDes.ReadOnly = true;
            this.rtxSelectedPopulationDes.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedPopulationDes.TabIndex = 65;
            this.rtxSelectedPopulationDes.Text = "";
            // 
            // rtxSelectedUseNegative
            // 
            this.rtxSelectedUseNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxSelectedUseNegative.Location = new System.Drawing.Point(248, 563);
            this.rtxSelectedUseNegative.Name = "rtxSelectedUseNegative";
            this.rtxSelectedUseNegative.ReadOnly = true;
            this.rtxSelectedUseNegative.Size = new System.Drawing.Size(1, 22);
            this.rtxSelectedUseNegative.TabIndex = 66;
            this.rtxSelectedUseNegative.Text = "";
            // 
            // grpTargetVariablesInTargetDatabase
            // 
            this.grpTargetVariablesInTargetDatabase.Controls.Add(this.panel2);
            this.grpTargetVariablesInTargetDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpTargetVariablesInTargetDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpTargetVariablesInTargetDatabase.ForeColor = System.Drawing.SystemColors.WindowText;
            this.grpTargetVariablesInTargetDatabase.Location = new System.Drawing.Point(0, 0);
            this.grpTargetVariablesInTargetDatabase.Name = "grpTargetVariablesInTargetDatabase";
            this.grpTargetVariablesInTargetDatabase.Size = new System.Drawing.Size(71, 46);
            this.grpTargetVariablesInTargetDatabase.TabIndex = 2;
            this.grpTargetVariablesInTargetDatabase.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.tlpTargetVariablesInTargetDatabase);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 18);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(65, 25);
            this.panel2.TabIndex = 0;
            // 
            // tlpTargetVariablesInTargetDatabase
            // 
            this.tlpTargetVariablesInTargetDatabase.AutoScroll = true;
            this.tlpTargetVariablesInTargetDatabase.ColumnCount = 1;
            this.tlpTargetVariablesInTargetDatabase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBIndicator, 0, 0);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBIndicatorDes, 0, 1);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBStateOrChange, 0, 2);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBStateOrChangeDes, 0, 3);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBUnit, 0, 4);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBUnitDes, 0, 5);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBLocalDensity, 0, 6);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBLocalDensityDes, 0, 7);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBObjectType, 0, 8);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBObjectTypeDes, 0, 9);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBObject, 0, 10);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBObjectDes, 0, 11);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBVersion, 0, 12);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBVersionDes, 0, 13);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBDefinitionVariant, 0, 14);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBDefinitionVariantDes, 0, 15);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBAreaDomain, 0, 16);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBAreaDomainDes, 0, 17);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBPopulation, 0, 18);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBPopulationDes, 0, 19);
            this.tlpTargetVariablesInTargetDatabase.Controls.Add(this.rtxTargetDBUseNegative, 0, 20);
            this.tlpTargetVariablesInTargetDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpTargetVariablesInTargetDatabase.Location = new System.Drawing.Point(0, 0);
            this.tlpTargetVariablesInTargetDatabase.Name = "tlpTargetVariablesInTargetDatabase";
            this.tlpTargetVariablesInTargetDatabase.RowCount = 22;
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpTargetVariablesInTargetDatabase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpTargetVariablesInTargetDatabase.Size = new System.Drawing.Size(65, 25);
            this.tlpTargetVariablesInTargetDatabase.TabIndex = 0;
            // 
            // rtxTargetDBIndicator
            // 
            this.rtxTargetDBIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBIndicator.Location = new System.Drawing.Point(3, 3);
            this.rtxTargetDBIndicator.Name = "rtxTargetDBIndicator";
            this.rtxTargetDBIndicator.ReadOnly = true;
            this.rtxTargetDBIndicator.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBIndicator.TabIndex = 45;
            this.rtxTargetDBIndicator.Text = "";
            // 
            // rtxTargetDBIndicatorDes
            // 
            this.rtxTargetDBIndicatorDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBIndicatorDes.Location = new System.Drawing.Point(3, 31);
            this.rtxTargetDBIndicatorDes.Name = "rtxTargetDBIndicatorDes";
            this.rtxTargetDBIndicatorDes.ReadOnly = true;
            this.rtxTargetDBIndicatorDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBIndicatorDes.TabIndex = 46;
            this.rtxTargetDBIndicatorDes.Text = "";
            // 
            // rtxTargetDBStateOrChange
            // 
            this.rtxTargetDBStateOrChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBStateOrChange.Location = new System.Drawing.Point(3, 59);
            this.rtxTargetDBStateOrChange.Name = "rtxTargetDBStateOrChange";
            this.rtxTargetDBStateOrChange.ReadOnly = true;
            this.rtxTargetDBStateOrChange.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBStateOrChange.TabIndex = 47;
            this.rtxTargetDBStateOrChange.Text = "";
            // 
            // rtxTargetDBStateOrChangeDes
            // 
            this.rtxTargetDBStateOrChangeDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBStateOrChangeDes.Location = new System.Drawing.Point(3, 87);
            this.rtxTargetDBStateOrChangeDes.Name = "rtxTargetDBStateOrChangeDes";
            this.rtxTargetDBStateOrChangeDes.ReadOnly = true;
            this.rtxTargetDBStateOrChangeDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBStateOrChangeDes.TabIndex = 48;
            this.rtxTargetDBStateOrChangeDes.Text = "";
            // 
            // rtxTargetDBUnit
            // 
            this.rtxTargetDBUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBUnit.Location = new System.Drawing.Point(3, 115);
            this.rtxTargetDBUnit.Name = "rtxTargetDBUnit";
            this.rtxTargetDBUnit.ReadOnly = true;
            this.rtxTargetDBUnit.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBUnit.TabIndex = 49;
            this.rtxTargetDBUnit.Text = "";
            // 
            // rtxTargetDBUnitDes
            // 
            this.rtxTargetDBUnitDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBUnitDes.Location = new System.Drawing.Point(3, 143);
            this.rtxTargetDBUnitDes.Name = "rtxTargetDBUnitDes";
            this.rtxTargetDBUnitDes.ReadOnly = true;
            this.rtxTargetDBUnitDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBUnitDes.TabIndex = 50;
            this.rtxTargetDBUnitDes.Text = "";
            // 
            // rtxTargetDBLocalDensity
            // 
            this.rtxTargetDBLocalDensity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBLocalDensity.Location = new System.Drawing.Point(3, 171);
            this.rtxTargetDBLocalDensity.Name = "rtxTargetDBLocalDensity";
            this.rtxTargetDBLocalDensity.ReadOnly = true;
            this.rtxTargetDBLocalDensity.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBLocalDensity.TabIndex = 51;
            this.rtxTargetDBLocalDensity.Text = "";
            // 
            // rtxTargetDBLocalDensityDes
            // 
            this.rtxTargetDBLocalDensityDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBLocalDensityDes.Location = new System.Drawing.Point(3, 199);
            this.rtxTargetDBLocalDensityDes.Name = "rtxTargetDBLocalDensityDes";
            this.rtxTargetDBLocalDensityDes.ReadOnly = true;
            this.rtxTargetDBLocalDensityDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBLocalDensityDes.TabIndex = 52;
            this.rtxTargetDBLocalDensityDes.Text = "";
            // 
            // rtxTargetDBObjectType
            // 
            this.rtxTargetDBObjectType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBObjectType.Location = new System.Drawing.Point(3, 227);
            this.rtxTargetDBObjectType.Name = "rtxTargetDBObjectType";
            this.rtxTargetDBObjectType.ReadOnly = true;
            this.rtxTargetDBObjectType.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBObjectType.TabIndex = 53;
            this.rtxTargetDBObjectType.Text = "";
            // 
            // rtxTargetDBObjectTypeDes
            // 
            this.rtxTargetDBObjectTypeDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBObjectTypeDes.Location = new System.Drawing.Point(3, 255);
            this.rtxTargetDBObjectTypeDes.Name = "rtxTargetDBObjectTypeDes";
            this.rtxTargetDBObjectTypeDes.ReadOnly = true;
            this.rtxTargetDBObjectTypeDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBObjectTypeDes.TabIndex = 54;
            this.rtxTargetDBObjectTypeDes.Text = "";
            // 
            // rtxTargetDBObject
            // 
            this.rtxTargetDBObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBObject.Location = new System.Drawing.Point(3, 283);
            this.rtxTargetDBObject.Name = "rtxTargetDBObject";
            this.rtxTargetDBObject.ReadOnly = true;
            this.rtxTargetDBObject.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBObject.TabIndex = 55;
            this.rtxTargetDBObject.Text = "";
            // 
            // rtxTargetDBObjectDes
            // 
            this.rtxTargetDBObjectDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBObjectDes.Location = new System.Drawing.Point(3, 311);
            this.rtxTargetDBObjectDes.Name = "rtxTargetDBObjectDes";
            this.rtxTargetDBObjectDes.ReadOnly = true;
            this.rtxTargetDBObjectDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBObjectDes.TabIndex = 56;
            this.rtxTargetDBObjectDes.Text = "";
            // 
            // rtxTargetDBVersion
            // 
            this.rtxTargetDBVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBVersion.Location = new System.Drawing.Point(3, 339);
            this.rtxTargetDBVersion.Name = "rtxTargetDBVersion";
            this.rtxTargetDBVersion.ReadOnly = true;
            this.rtxTargetDBVersion.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBVersion.TabIndex = 57;
            this.rtxTargetDBVersion.Text = "";
            // 
            // rtxTargetDBVersionDes
            // 
            this.rtxTargetDBVersionDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBVersionDes.Location = new System.Drawing.Point(3, 367);
            this.rtxTargetDBVersionDes.Name = "rtxTargetDBVersionDes";
            this.rtxTargetDBVersionDes.ReadOnly = true;
            this.rtxTargetDBVersionDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBVersionDes.TabIndex = 58;
            this.rtxTargetDBVersionDes.Text = "";
            // 
            // rtxTargetDBDefinitionVariant
            // 
            this.rtxTargetDBDefinitionVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBDefinitionVariant.Location = new System.Drawing.Point(3, 395);
            this.rtxTargetDBDefinitionVariant.Name = "rtxTargetDBDefinitionVariant";
            this.rtxTargetDBDefinitionVariant.ReadOnly = true;
            this.rtxTargetDBDefinitionVariant.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBDefinitionVariant.TabIndex = 59;
            this.rtxTargetDBDefinitionVariant.Text = "";
            // 
            // rtxTargetDBDefinitionVariantDes
            // 
            this.rtxTargetDBDefinitionVariantDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBDefinitionVariantDes.Location = new System.Drawing.Point(3, 423);
            this.rtxTargetDBDefinitionVariantDes.Name = "rtxTargetDBDefinitionVariantDes";
            this.rtxTargetDBDefinitionVariantDes.ReadOnly = true;
            this.rtxTargetDBDefinitionVariantDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBDefinitionVariantDes.TabIndex = 60;
            this.rtxTargetDBDefinitionVariantDes.Text = "";
            // 
            // rtxTargetDBAreaDomain
            // 
            this.rtxTargetDBAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBAreaDomain.Location = new System.Drawing.Point(3, 451);
            this.rtxTargetDBAreaDomain.Name = "rtxTargetDBAreaDomain";
            this.rtxTargetDBAreaDomain.ReadOnly = true;
            this.rtxTargetDBAreaDomain.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBAreaDomain.TabIndex = 61;
            this.rtxTargetDBAreaDomain.Text = "";
            // 
            // rtxTargetDBAreaDomainDes
            // 
            this.rtxTargetDBAreaDomainDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBAreaDomainDes.Location = new System.Drawing.Point(3, 479);
            this.rtxTargetDBAreaDomainDes.Name = "rtxTargetDBAreaDomainDes";
            this.rtxTargetDBAreaDomainDes.ReadOnly = true;
            this.rtxTargetDBAreaDomainDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBAreaDomainDes.TabIndex = 62;
            this.rtxTargetDBAreaDomainDes.Text = "";
            // 
            // rtxTargetDBPopulation
            // 
            this.rtxTargetDBPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBPopulation.Location = new System.Drawing.Point(3, 507);
            this.rtxTargetDBPopulation.Name = "rtxTargetDBPopulation";
            this.rtxTargetDBPopulation.ReadOnly = true;
            this.rtxTargetDBPopulation.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBPopulation.TabIndex = 63;
            this.rtxTargetDBPopulation.Text = "";
            // 
            // rtxTargetDBPopulationDes
            // 
            this.rtxTargetDBPopulationDes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBPopulationDes.Location = new System.Drawing.Point(3, 535);
            this.rtxTargetDBPopulationDes.Name = "rtxTargetDBPopulationDes";
            this.rtxTargetDBPopulationDes.ReadOnly = true;
            this.rtxTargetDBPopulationDes.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBPopulationDes.TabIndex = 64;
            this.rtxTargetDBPopulationDes.Text = "";
            // 
            // rtxTargetDBUseNegative
            // 
            this.rtxTargetDBUseNegative.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxTargetDBUseNegative.Location = new System.Drawing.Point(3, 563);
            this.rtxTargetDBUseNegative.Name = "rtxTargetDBUseNegative";
            this.rtxTargetDBUseNegative.ReadOnly = true;
            this.rtxTargetDBUseNegative.Size = new System.Drawing.Size(59, 22);
            this.rtxTargetDBUseNegative.TabIndex = 65;
            this.rtxTargetDBUseNegative.Text = "";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLanguage});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(944, 24);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip";
            // 
            // tsmiLanguage
            // 
            this.tsmiLanguage.Name = "tsmiLanguage";
            this.tsmiLanguage.Size = new System.Drawing.Size(94, 20);
            this.tsmiLanguage.Text = "tsmiLanguage";
            this.tsmiLanguage.Click += new System.EventHandler(this.TsmiLanguage_Click);
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 32767;
            this.toolTip.InitialDelay = 0;
            this.toolTip.ReshowDelay = 0;
            // 
            // FormCheckMetadata
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.tlpMain);
            this.MainMenuStrip = this.menuStrip;
            this.MinimizeBox = false;
            this.Name = "FormCheckMetadata";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.splHorizontal.Panel1.ResumeLayout(false);
            this.splHorizontal.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splHorizontal)).EndInit();
            this.splHorizontal.ResumeLayout(false);
            this.grpDiff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiff)).EndInit();
            this.splMiddlePart.Panel1.ResumeLayout(false);
            this.splMiddlePart.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splMiddlePart)).EndInit();
            this.splMiddlePart.ResumeLayout(false);
            this.grpSelectedTargetVariable.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tlpSelectedTargetVariable.ResumeLayout(false);
            this.tlpSelectedTargetVariable.PerformLayout();
            this.grpTargetVariablesInTargetDatabase.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tlpTargetVariablesInTargetDatabase.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.GroupBox grpSelectedTargetVariable;
        private System.Windows.Forms.GroupBox grpTargetVariablesInTargetDatabase;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiLanguage;
        private System.Windows.Forms.TableLayoutPanel tlpTargetVariablesInTargetDatabase;
        private System.Windows.Forms.SplitContainer splMiddlePart;
        private System.Windows.Forms.GroupBox grpDiff;
        private System.Windows.Forms.RichTextBox rtxTargetDBIndicator;
        private System.Windows.Forms.RichTextBox rtxTargetDBIndicatorDes;
        private System.Windows.Forms.TableLayoutPanel tlpSelectedTargetVariable;
        private System.Windows.Forms.Label lblUseNegative;
        private System.Windows.Forms.Label lblPopulationDes;
        private System.Windows.Forms.Label lblAreaDomainDes;
        private System.Windows.Forms.Label lblDefinitionVariantDes;
        private System.Windows.Forms.Label lblVersionDes;
        private System.Windows.Forms.Label lblObjectDes;
        private System.Windows.Forms.Label lblObject;
        private System.Windows.Forms.Label lblObjectTypeDes;
        private System.Windows.Forms.Label lblObjectType;
        private System.Windows.Forms.Label lblLocalDensityDes;
        private System.Windows.Forms.Label lblUnitDes;
        private System.Windows.Forms.Label lblStateOrChangeDes;
        private System.Windows.Forms.Label lblIndicatorDes;
        private System.Windows.Forms.Label lblIndicator;
        private System.Windows.Forms.Label lblStateOrChange;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.Label lblLocalDensity;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblDefinitionVariant;
        private System.Windows.Forms.Label lblAreaDomain;
        private System.Windows.Forms.Label lblPopulation;
        private System.Windows.Forms.RichTextBox rtxSelectedIndicator;
        private System.Windows.Forms.RichTextBox rtxSelectedIndicatorDes;
        private System.Windows.Forms.RichTextBox rtxSelectedStateOrChange;
        private System.Windows.Forms.RichTextBox rtxSelectedStateOrChangeDes;
        private System.Windows.Forms.RichTextBox rtxSelectedUnit;
        private System.Windows.Forms.RichTextBox rtxSelectedUnitDes;
        private System.Windows.Forms.RichTextBox rtxSelectedLocalDensity;
        private System.Windows.Forms.RichTextBox rtxSelectedLocalDensityDes;
        private System.Windows.Forms.RichTextBox rtxSelectedObjectType;
        private System.Windows.Forms.RichTextBox rtxSelectedObjectTypeDes;
        private System.Windows.Forms.RichTextBox rtxSelectedObject;
        private System.Windows.Forms.RichTextBox rtxSelectedObjectDes;
        private System.Windows.Forms.RichTextBox rtxSelectedVersion;
        private System.Windows.Forms.RichTextBox rtxSelectedVersionDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBStateOrChange;
        private System.Windows.Forms.RichTextBox rtxTargetDBStateOrChangeDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBUnit;
        private System.Windows.Forms.RichTextBox rtxTargetDBUnitDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBLocalDensity;
        private System.Windows.Forms.RichTextBox rtxTargetDBLocalDensityDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBObjectType;
        private System.Windows.Forms.RichTextBox rtxTargetDBObjectTypeDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBObject;
        private System.Windows.Forms.RichTextBox rtxTargetDBObjectDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBVersion;
        private System.Windows.Forms.RichTextBox rtxTargetDBVersionDes;
        private System.Windows.Forms.RichTextBox rtxSelectedDefinitionVariant;
        private System.Windows.Forms.RichTextBox rtxSelectedDefinitionVariantDes;
        private System.Windows.Forms.RichTextBox rtxSelectedAreaDomain;
        private System.Windows.Forms.RichTextBox rtxSelectedAreaDomainDes;
        private System.Windows.Forms.RichTextBox rtxSelectedPopulation;
        private System.Windows.Forms.RichTextBox rtxSelectedPopulationDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBDefinitionVariant;
        private System.Windows.Forms.RichTextBox rtxTargetDBDefinitionVariantDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBAreaDomain;
        private System.Windows.Forms.RichTextBox rtxTargetDBAreaDomainDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBPopulation;
        private System.Windows.Forms.RichTextBox rtxSelectedUseNegative;
        private System.Windows.Forms.RichTextBox rtxTargetDBPopulationDes;
        private System.Windows.Forms.RichTextBox rtxTargetDBUseNegative;

        /// <summary>
        /// btnNext
        /// </summary>
        public System.Windows.Forms.Button btnNext;

        /// <summary>
        /// btnInsert
        /// </summary>
        public System.Windows.Forms.Button btnInsert;

        /// <summary>
        /// btnUpdate
        /// </summary>
        public System.Windows.Forms.Button btnUpdate;

        private System.Windows.Forms.DataGridView dgvDiff;
        private System.Windows.Forms.SplitContainer splHorizontal;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }

}