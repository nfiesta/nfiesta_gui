﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class ControlAttributeVariables
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnCheck = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.splMain = new System.Windows.Forms.SplitContainer();
            this.dgvPanelRefyearset = new System.Windows.Forms.DataGridView();
            this.dgvAttrVariables = new System.Windows.Forms.DataGridView();
            this.tlpMain.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).BeginInit();
            this.splMain.Panel1.SuspendLayout();
            this.splMain.Panel2.SuspendLayout();
            this.splMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPanelRefyearset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttrVariables)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.pnlButtons, 0, 2);
            this.tlpMain.Controls.Add(this.lblMainCaption, 0, 0);
            this.tlpMain.Controls.Add(this.splMain, 0, 1);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnCheck);
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(3, 503);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(954, 34);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnCheck
            // 
            this.btnCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheck.Location = new System.Drawing.Point(645, 4);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(150, 30);
            this.btnCheck.TabIndex = 1;
            this.btnCheck.Text = "btnCheck";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.BtnCheck_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(801, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.AutoSize = true;
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(3, 0);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(954, 30);
            this.lblMainCaption.TabIndex = 1;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splMain
            // 
            this.splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splMain.Location = new System.Drawing.Point(3, 33);
            this.splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            this.splMain.Panel1.Controls.Add(this.dgvPanelRefyearset);
            // 
            // splMain.Panel2
            // 
            this.splMain.Panel2.Controls.Add(this.dgvAttrVariables);
            this.splMain.Panel2Collapsed = true;
            this.splMain.Size = new System.Drawing.Size(954, 464);
            this.splMain.SplitterDistance = 476;
            this.splMain.TabIndex = 2;
            // 
            // dgvPanelRefyearset
            // 
            this.dgvPanelRefyearset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPanelRefyearset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPanelRefyearset.Location = new System.Drawing.Point(0, 0);
            this.dgvPanelRefyearset.Name = "dgvPanelRefyearset";
            this.dgvPanelRefyearset.Size = new System.Drawing.Size(954, 464);
            this.dgvPanelRefyearset.TabIndex = 2;
            this.dgvPanelRefyearset.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DgvPanelRefyearset_CellPainting);
            this.dgvPanelRefyearset.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvPanelRefyearset_CellValueChanged);
            this.dgvPanelRefyearset.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvPanelRefyearset_CurrentCellDirtyStateChanged);
            // 
            // dgvAttrVariables
            // 
            this.dgvAttrVariables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttrVariables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAttrVariables.Location = new System.Drawing.Point(0, 0);
            this.dgvAttrVariables.Name = "dgvAttrVariables";
            this.dgvAttrVariables.Size = new System.Drawing.Size(96, 100);
            this.dgvAttrVariables.TabIndex = 0;
            // 
            // ControlAttributeVariables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpMain);
            this.Name = "ControlAttributeVariables";
            this.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.splMain.Panel1.ResumeLayout(false);
            this.splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).EndInit();
            this.splMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPanelRefyearset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttrVariables)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;

        /// <summary>
        /// btnNext
        /// </summary>
        public System.Windows.Forms.Button btnNext;

        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.DataGridView dgvPanelRefyearset;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.DataGridView dgvAttrVariables;

        /// <summary>
        /// btnCheck
        /// </summary>
        public System.Windows.Forms.Button btnCheck;
    }

}
