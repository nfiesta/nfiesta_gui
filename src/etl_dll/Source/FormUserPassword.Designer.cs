﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class FormUserPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            btnOK = new System.Windows.Forms.Button();
            tlpUserPassword = new System.Windows.Forms.TableLayoutPanel();
            lblPassword = new System.Windows.Forms.Label();
            txtPassword = new System.Windows.Forms.TextBox();
            lblUser = new System.Windows.Forms.Label();
            txtUser = new System.Windows.Forms.TextBox();
            lblServer = new System.Windows.Forms.Label();
            lblPort = new System.Windows.Forms.Label();
            lblDatabase = new System.Windows.Forms.Label();
            txtServer = new System.Windows.Forms.TextBox();
            txtPort = new System.Windows.Forms.TextBox();
            txtDatabase = new System.Windows.Forms.TextBox();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpUserPassword.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Controls.Add(tlpUserPassword, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(483, 187);
            tlpMain.TabIndex = 0;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnCancel);
            pnlButtons.Controls.Add(btnOK);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 141);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(483, 46);
            pnlButtons.TabIndex = 5;
            // 
            // btnCancel
            // 
            btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Location = new System.Drawing.Point(121, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 35);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += BtnCancel_Click;
            // 
            // btnOK
            // 
            btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Location = new System.Drawing.Point(302, 6);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(175, 35);
            btnOK.TabIndex = 0;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += BtnOK_Click;
            // 
            // tlpUserPassword
            // 
            tlpUserPassword.ColumnCount = 2;
            tlpUserPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            tlpUserPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpUserPassword.Controls.Add(lblPassword, 0, 4);
            tlpUserPassword.Controls.Add(txtPassword, 1, 4);
            tlpUserPassword.Controls.Add(lblUser, 0, 3);
            tlpUserPassword.Controls.Add(txtUser, 1, 3);
            tlpUserPassword.Controls.Add(lblServer, 0, 0);
            tlpUserPassword.Controls.Add(lblPort, 0, 1);
            tlpUserPassword.Controls.Add(lblDatabase, 0, 2);
            tlpUserPassword.Controls.Add(txtServer, 1, 0);
            tlpUserPassword.Controls.Add(txtPort, 1, 1);
            tlpUserPassword.Controls.Add(txtDatabase, 1, 2);
            tlpUserPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpUserPassword.Location = new System.Drawing.Point(4, 3);
            tlpUserPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpUserPassword.Name = "tlpUserPassword";
            tlpUserPassword.RowCount = 6;
            tlpUserPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpUserPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpUserPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpUserPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpUserPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpUserPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpUserPassword.Size = new System.Drawing.Size(475, 135);
            tlpUserPassword.TabIndex = 0;
            // 
            // lblPassword
            // 
            lblPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblPassword.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblPassword.Location = new System.Drawing.Point(0, 100);
            lblPassword.Margin = new System.Windows.Forms.Padding(0);
            lblPassword.Name = "lblPassword";
            lblPassword.Size = new System.Drawing.Size(140, 25);
            lblPassword.TabIndex = 2;
            lblPassword.Text = "lblPassword";
            lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPassword
            // 
            txtPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            txtPassword.Location = new System.Drawing.Point(140, 100);
            txtPassword.Margin = new System.Windows.Forms.Padding(0);
            txtPassword.Name = "txtPassword";
            txtPassword.Size = new System.Drawing.Size(335, 23);
            txtPassword.TabIndex = 4;
            txtPassword.UseSystemPasswordChar = true;
            // 
            // lblUser
            // 
            lblUser.Dock = System.Windows.Forms.DockStyle.Fill;
            lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblUser.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblUser.Location = new System.Drawing.Point(0, 75);
            lblUser.Margin = new System.Windows.Forms.Padding(0);
            lblUser.Name = "lblUser";
            lblUser.Size = new System.Drawing.Size(140, 25);
            lblUser.TabIndex = 1;
            lblUser.Text = "lblUser";
            lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUser
            // 
            txtUser.Dock = System.Windows.Forms.DockStyle.Fill;
            txtUser.Location = new System.Drawing.Point(140, 75);
            txtUser.Margin = new System.Windows.Forms.Padding(0);
            txtUser.Name = "txtUser";
            txtUser.Size = new System.Drawing.Size(335, 23);
            txtUser.TabIndex = 3;
            // 
            // lblServer
            // 
            lblServer.Dock = System.Windows.Forms.DockStyle.Fill;
            lblServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblServer.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblServer.Location = new System.Drawing.Point(0, 0);
            lblServer.Margin = new System.Windows.Forms.Padding(0);
            lblServer.Name = "lblServer";
            lblServer.Size = new System.Drawing.Size(140, 25);
            lblServer.TabIndex = 5;
            lblServer.Text = "lblServer";
            lblServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPort
            // 
            lblPort.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblPort.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblPort.Location = new System.Drawing.Point(0, 25);
            lblPort.Margin = new System.Windows.Forms.Padding(0);
            lblPort.Name = "lblPort";
            lblPort.Size = new System.Drawing.Size(140, 25);
            lblPort.TabIndex = 6;
            lblPort.Text = "lblPort";
            lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDatabase
            // 
            lblDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblDatabase.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblDatabase.Location = new System.Drawing.Point(0, 50);
            lblDatabase.Margin = new System.Windows.Forms.Padding(0);
            lblDatabase.Name = "lblDatabase";
            lblDatabase.Size = new System.Drawing.Size(140, 25);
            lblDatabase.TabIndex = 7;
            lblDatabase.Text = "lblDatabase";
            lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServer
            // 
            txtServer.Dock = System.Windows.Forms.DockStyle.Fill;
            txtServer.Location = new System.Drawing.Point(140, 0);
            txtServer.Margin = new System.Windows.Forms.Padding(0);
            txtServer.Name = "txtServer";
            txtServer.ReadOnly = true;
            txtServer.Size = new System.Drawing.Size(335, 23);
            txtServer.TabIndex = 8;
            // 
            // txtPort
            // 
            txtPort.Dock = System.Windows.Forms.DockStyle.Fill;
            txtPort.Location = new System.Drawing.Point(140, 25);
            txtPort.Margin = new System.Windows.Forms.Padding(0);
            txtPort.Name = "txtPort";
            txtPort.ReadOnly = true;
            txtPort.Size = new System.Drawing.Size(335, 23);
            txtPort.TabIndex = 9;
            // 
            // txtDatabase
            // 
            txtDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDatabase.Location = new System.Drawing.Point(140, 50);
            txtDatabase.Margin = new System.Windows.Forms.Padding(0);
            txtDatabase.Name = "txtDatabase";
            txtDatabase.ReadOnly = true;
            txtDatabase.Size = new System.Drawing.Size(335, 23);
            txtDatabase.TabIndex = 10;
            // 
            // FormUserPassword
            // 
            AcceptButton = btnOK;
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(483, 187);
            Controls.Add(tlpMain);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormUserPassword";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormUserPassword";
            FormClosing += FormUserPassword_FormClosing;
            Load += FormUserPassword_Load;
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            tlpUserPassword.ResumeLayout(false);
            tlpUserPassword.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpUserPassword;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtDatabase;
    }

}