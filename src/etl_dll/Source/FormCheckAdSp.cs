﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Formulář pro kontrolu a aktualizaci zkratek a popisů plošných domén, subpopulací a jejich kategorií</para>
    /// <para lang="en">Form to check and update labels and descriptions of area domains, subpopulations and their categories</para>
    /// </summary>
    public partial class FormCheckAdSp
            : Form, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro kontrolu a aktualizaci zkratek a popisů plošných domén, subpopulací a jejich kategorií</para>
        /// <para lang="en">Constructor of a form to check and update labels and descriptions of area domains, subpopulations and their categories</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        public FormCheckAdSp(
            Control controlOwner,
            int selectedExportConnectionId)
        {
            InitializeComponent();

            _selectedExportConnectionId = selectedExportConnectionId;

            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is ControlConnection))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlConnection.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is ControlConnection))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlConnection.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((ControlConnection)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((ControlConnection)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ControlConnection)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ControlConnection)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlConnection ctrConnection)
                {
                    return ctrConnection.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                 "Kontrola a aktualizace zkratek a popisů plošných domén, subpopulací a jejich kategorií" },
                            { "lblLabelNatSourceDB",        "zkratka národní - zdrojová DB" },
                            { "lblLabelNatTargetDB",        "zkratka národní - cílová DB" },
                            { "lblDesNatSourceDB",          "popis národní - zdrojová DB" },
                            { "lblDesNatTargetDB",          "popis národní - cílová DB" },
                            { "lblLabelEnSourceDB",         "zkratka anglická - zdrojová DB" },
                            { "lblLabelEnTargetDB",         "zkratka anglická - cílová DB" },
                            { "lblDesEnSourceDB",           "popis anglický - zdrojová DB" },
                            { "lblDesEnTargetDB",           "popis anglický - cílová DB" },
                            { "btnAD",                      "Plošné domény" },
                            { "btnADC",                     "Kategorie plošné domény" },
                            { "btnSP",                      "Subpopulace" },
                            { "btnSPC",                     "Kategorie subpopulace" },
                            { "btnUpdateLabelNat",          "Aktualizovat" },
                            { "btnUpdateDesNat",            "Aktualizovat" },
                            { "btnUpdateLabelEn",           "Aktualizovat" },
                            { "btnUpdateDesEn",             "Aktualizovat" },
                            { "btnUpdateAll",               "Aktualizovat vše" },
                            { "btnClose",                   "Zavřít" },
                            { "msgUpdateLabelNat",          "Aktualizovat národní zkratku a změnu uložit do cílové databáze?" },
                            { "msgUpdateDesNat",            "Aktualizovat národní popis a změnu uložit do cílové databáze?" },
                            { "msgUpdateLabelEn",           "Aktualizovat anglickou zkratku a změnu uložit do cílové databáze?" },
                            { "msgUpdateDesEn",             "Aktualizovat anglický popis a změnu uložit do cílové databáze?" },
                            { "msgUpdateAll",               "Aktualizovat vše a změnu uložit do cílové databáze?" },
                            { "areaDomainLabel",            "Zkratka národní - plošná doména" },
                            { "areaDomainLabelEn",          "Zkratka anglická - plošná doména" },
                            { "subPopulationLabel",         "Zkratka národní - subpopulace" },
                            { "subPopulationLabelEn",       "Zkratka anglická - subpopulace" },
                            { "labelNatSource",             "Zkratka národní - zdrojová DB" },
                            { "labelEnSource",              "Zkratka anglická - zdrojová DB" },
                            { "checkLabel",                 "Liší se zkratka národní?" },
                            { "checkDescription",           "Liší se popis národní?" },
                            { "checkLabelEn",               "Liší se zkratka anglická?" },
                            { "checkDescriptionEn",         "Liší se popis anglický?" },
                            { "yes",                        "ano"},
                            { "no",                         "ne"},
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "FormCheckAdSp") ?
                    languageFile.NationalVersion.Data["FormCheckAdSp"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                 "Check and update labels and descriptions of area domains, subpopulations and their categories" },
                            { "lblLabelNatSourceDB",        "label national - source DB" },
                            { "lblLabelNatTargetDB",        "label national - target DB" },
                            { "lblDesNatSourceDB",          "description national - source DB" },
                            { "lblDesNatTargetDB",          "description national - target DB" },
                            { "lblLabelEnSourceDB",         "label English - source DB" },
                            { "lblLabelEnTargetDB",         "label English - target DB" },
                            { "lblDesEnSourceDB",           "description English - source DB" },
                            { "lblDesEnTargetDB",           "description English - target DB" },
                            { "btnAD",                      "Area domains" },
                            { "btnADC",                     "Area domain categories" },
                            { "btnSP",                      "Subpopulations" },
                            { "btnSPC",                     "Subpopulation categories" },
                            { "btnUpdateLabelNat",          "Update" },
                            { "btnUpdateDesNat",            "Update" },
                            { "btnUpdateLabelEn",           "Update" },
                            { "btnUpdateDesEn",             "Update" },
                            { "btnUpdateAll",               "Update all" },
                            { "btnClose",                   "Close" },
                            { "msgUpdateLabelNat",          "Update the national label and save the change to the target database?" },
                            { "msgUpdateDesNat",            "Update the national description and save the change to the target database?" },
                            { "msgUpdateLabelEn",           "Update the English label and save the change to the target database?" },
                            { "msgUpdateDesEn",             "Update the English description and save the change to the target database?" },
                            { "msgUpdateAll",               "Update all and save the change to the target database?" },
                            { "areaDomainLabel",            "Label national - area domain" },
                            { "areaDomainLabelEn",          "Label English - area domain" },
                            { "subPopulationLabel",         "Label national - subpopulation" },
                            { "subPopulationLabelEn",       "Label English - subpopulation" },
                            { "labelNatSource",             "Label national - source DB" },
                            { "labelEnSource",              "Label English - source DB" },
                            { "checkLabel",                 "Does the national label differ?" },
                            { "checkDescription",           "Does the national description differ?" },
                            { "checkLabelEn",               "Does the English label differ?" },
                            { "checkDescriptionEn",         "Does the English description differ?" },
                            { "yes",                        "yes"},
                            { "no",                         "no"},
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "FormCheckAdSp") ?
                    languageFile.InternationalVersion.Data["FormCheckAdSp"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro kontrolu a aktualizaci zkratek a popisů plošných domén, subpopulací a jejich kategorií</para>
        /// <para lang="en">Initializing a form to check and update labels and descriptions of area domains, subpopulations and their categories</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            nationalLang = new NationalLang();

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro kontrolu a aktualizaci zkratek a popisů plošných domén, subpopulací a jejich kategorií</para>
        /// <para lang="en">Initializing labels of a form to check and update labels and descriptions of area domains, subpopulations and their categories</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text =
               labels.ContainsKey(key: "frmCaption") ?
               labels["frmCaption"] : String.Empty;

            lblLabelNatSourceDB.Text =
                labels.ContainsKey(key: "lblLabelNatSourceDB") ?
                labels["lblLabelNatSourceDB"] : String.Empty;

            lblLabelNatTargetDB.Text =
                labels.ContainsKey(key: "lblLabelNatTargetDB") ?
                labels["lblLabelNatTargetDB"] : String.Empty;

            lblDesNatSourceDB.Text =
                labels.ContainsKey(key: "lblDesNatSourceDB") ?
                labels["lblDesNatSourceDB"] : String.Empty;

            lblDesNatTargetDB.Text =
                labels.ContainsKey(key: "lblDesNatTargetDB") ?
                labels["lblDesNatTargetDB"] : String.Empty;

            lblLabelEnSourceDB.Text =
                labels.ContainsKey(key: "lblLabelEnSourceDB") ?
                labels["lblLabelEnSourceDB"] : String.Empty;

            lblLabelEnTargetDB.Text =
                labels.ContainsKey(key: "lblLabelEnTargetDB") ?
                labels["lblLabelEnTargetDB"] : String.Empty;

            lblDesEnSourceDB.Text =
                labels.ContainsKey(key: "lblDesEnSourceDB") ?
                labels["lblDesEnSourceDB"] : String.Empty;

            lblDesEnTargetDB.Text =
                labels.ContainsKey(key: "lblDesEnTargetDB") ?
                labels["lblDesEnTargetDB"] : String.Empty;

            btnAD.Text =
               labels.ContainsKey(key: "btnAD") ?
               labels["btnAD"] : String.Empty;

            btnADC.Text =
               labels.ContainsKey(key: "btnADC") ?
               labels["btnADC"] : String.Empty;

            btnSP.Text =
               labels.ContainsKey(key: "btnSP") ?
               labels["btnSP"] : String.Empty;

            btnSPC.Text =
               labels.ContainsKey(key: "btnSPC") ?
               labels["btnSPC"] : String.Empty;

            btnUpdateLabelNat.Text =
               labels.ContainsKey(key: "btnUpdateLabelNat") ?
               labels["btnUpdateLabelNat"] : String.Empty;

            btnUpdateDesNat.Text =
               labels.ContainsKey(key: "btnUpdateDesNat") ?
               labels["btnUpdateDesNat"] : String.Empty;

            btnUpdateLabelEn.Text =
               labels.ContainsKey(key: "btnUpdateLabelEn") ?
               labels["btnUpdateLabelEn"] : String.Empty;

            btnUpdateDesEn.Text =
               labels.ContainsKey(key: "btnUpdateDesEn") ?
               labels["btnUpdateDesEn"] : String.Empty;

            btnUpdateAll.Text =
               labels.ContainsKey(key: "btnUpdateAll") ?
               labels["btnUpdateAll"] : String.Empty;

            btnClose.Text =
               labels.ContainsKey(key: "btnClose") ?
               labels["btnClose"] : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        { }

        /// <summary>
        /// <para lang="cs">Nastaví záhlaví, viditelnost, formáty podle typu dat a zarovnání ve sloupcích daného DataGridView</para>
        /// <para lang="en">Sets header, visibility, formats by data type and alignment in columns of given DataGridView</para>
        /// </summary>
        /// <param name="dataGridView">
        /// <para lang="cs">Objekt DataGridView, který se má formátovat</para>
        /// <para lang="en">The DataGridView object to be formatted</para>
        /// </param>
        private void FormatDataGridView(DataGridView dataGridView)
        {
            // General:
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeRows = false;

            dataGridView.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle()
            {
                BackColor = Color.LightCyan
            };

            dataGridView.AutoGenerateColumns = true;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.BackgroundColor = SystemColors.Window;
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.None;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;

            dataGridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.DarkBlue,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = Color.White,
                SelectionBackColor = Color.DarkBlue,
                SelectionForeColor = Color.White,
                WrapMode = DataGridViewTriState.True
            };

            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            dataGridView.DefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.White,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = SystemColors.ControlText,
                Format = "N3",
                NullValue = "NULL",
                SelectionBackColor = Color.Bisque,
                SelectionForeColor = SystemColors.ControlText,
                WrapMode = DataGridViewTriState.False
            };

            dataGridView.EnableHeadersVisualStyles = false;
            dataGridView.MultiSelect = false;
            dataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView.RowHeadersVisible = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek pro plošné domény a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data for the area domains and displaying it in the control</para>
        /// </summary>
        private void LoadContentAD()
        {
            splTables.Panel1Collapsed = true;

            string metadata = TDFunctions.FnEtlGetAreaDomainsForUpdate.Execute(Database, _selectedExportConnectionId);

            if (!string.IsNullOrEmpty(metadata))
            {
                DataTable result = NfiEstaFunctions.FnEtlCheckAreaDomainsForUpdate.ExecuteQuery(
                    FormUserPassword.TargetDatabase,
                    metadata);

                dgv.DataSource = result;

                FormatDataGridView(dgv);

                dgv.Columns["area_domain"].Visible = false;
                dgv.Columns["description_source"].Visible = false;
                dgv.Columns["description_en_source"].Visible = false;
                dgv.Columns["label_target"].Visible = false;
                dgv.Columns["description_target"].Visible = false;
                dgv.Columns["label_en_target"].Visible = false;
                dgv.Columns["description_en_target"].Visible = false;

                UpdateColumnHeaders();
            }
            else
            {
                dgv.DataSource = null;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek pro subpopulace a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data for the subpopulations and displaying it in the control</para>
        /// </summary>
        private void LoadContentSP()
        {
            splTables.Panel1Collapsed = true;

            string metadata = TDFunctions.FnEtlGetSubPopulationsForUpdate.Execute(Database, _selectedExportConnectionId);

            if (!string.IsNullOrEmpty(metadata))
            {
                DataTable result = NfiEstaFunctions.FnEtlCheckSubPopulationsForUpdate.ExecuteQuery(
                    FormUserPassword.TargetDatabase,
                    metadata);

                dgv.DataSource = result;

                FormatDataGridView(dgv);

                dgv.Columns["sub_population"].Visible = false;
                dgv.Columns["description_source"].Visible = false;
                dgv.Columns["description_en_source"].Visible = false;
                dgv.Columns["label_target"].Visible = false;
                dgv.Columns["description_target"].Visible = false;
                dgv.Columns["label_en_target"].Visible = false;
                dgv.Columns["description_en_target"].Visible = false;

                UpdateColumnHeaders();
            }
            else
            {
                dgv.DataSource = null;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek pro plošné domény pro kategorie a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data for the area domains for the categories and displaying it in the control</para>
        /// </summary>
        private void LoadContentADForADC()
        {
            splTables.Panel1Collapsed = false;

            string metadata = TDFunctions.FnEtlGetAreaDomainCategoriesForAreaDomains.Execute(Database, _selectedExportConnectionId);

            if (!string.IsNullOrEmpty(metadata))
            {
                List<int?> areaDomainTarget = NfiEstaFunctions.FnEtlCheckAreaDomainCategoriesForAreaDomains.Execute(
                    FormUserPassword.TargetDatabase,
                    metadata);

                Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                if (areaDomainTarget != null)
                {
                    DataTable areaDomainsWithDifferentCategories = TDFunctions.FnEtlGetListOfAreaDomainsForUpdate.ExecuteQuery(
                        Database,
                        _selectedExportConnectionId,
                        areaDomainTarget);

                    dgvType.DataSource = areaDomainsWithDifferentCategories;

                    FormatDataGridView(dgvType);

                    dgvType.Columns["etl_area_domain"].Visible = false;
                    dgvType.Columns["area_domain_target"].Visible = false;
                    dgvType.Columns["description"].Visible = false;
                    dgvType.Columns["description_en"].Visible = false;

                    switch (LanguageVersion)
                    {
                        case LanguageVersion.National:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "areaDomainLabel") ? labels["areaDomainLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "areaDomainLabelEn") ? labels["areaDomainLabelEn"] : String.Empty;
                            break;

                        case LanguageVersion.International:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "areaDomainLabel") ? labels["areaDomainLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "areaDomainLabelEn") ? labels["areaDomainLabelEn"] : String.Empty;
                            break;
                    }
                }
                else
                {
                    DataTable emptyDataTable = new DataTable();

                    emptyDataTable.Columns.Add("label", typeof(string));
                    emptyDataTable.Columns.Add("label_en", typeof(string));

                    dgvType.DataSource = emptyDataTable;

                    FormatDataGridView(dgvType);

                    switch (LanguageVersion)
                    {
                        case LanguageVersion.National:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "areaDomainLabel") ? labels["areaDomainLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "areaDomainLabelEn") ? labels["areaDomainLabelEn"] : String.Empty;
                            break;

                        case LanguageVersion.International:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "areaDomainLabel") ? labels["areaDomainLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "areaDomainLabelEn") ? labels["areaDomainLabelEn"] : String.Empty;
                            break;
                    }
                }
            }
            else { dgvType.DataSource = null; }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek pro kategorie plošných domén a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data for the area domain categories and displaying it in the control</para>
        /// </summary>
        private void LoadContentADC()
        {
            if (dgvType.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dgvType.SelectedRows[0];

                int etlAreaDomain = Convert.ToInt32(selectedRow.Cells["etl_area_domain"].Value);
                int areaDomainTarget = Convert.ToInt32(selectedRow.Cells["area_domain_target"].Value);

                string metadata = TDFunctions.FnEtlGetAreaDomainCategoriesForUpdate.Execute(
                    Database,
                    etlAreaDomain,
                    areaDomainTarget);

                DataTable result = NfiEstaFunctions.FnEtlCheckAreaDomainCategoriesForUpdate.ExecuteQuery(
                    FormUserPassword.TargetDatabase,
                    metadata);

                dgv.DataSource = result;

                FormatDataGridView(dgv);

                dgv.Columns["area_domain_category"].Visible = false;
                dgv.Columns["description_source"].Visible = false;
                dgv.Columns["description_en_source"].Visible = false;
                dgv.Columns["label_target"].Visible = false;
                dgv.Columns["description_target"].Visible = false;
                dgv.Columns["label_en_target"].Visible = false;
                dgv.Columns["description_en_target"].Visible = false;

                UpdateColumnHeaders();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek pro subpopulace pro kategorie a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data for the subpopulations for the categories and displaying it in the control</para>
        /// </summary>
        private void LoadContentSPForSPC()
        {
            splTables.Panel1Collapsed = false;

            string metadata = TDFunctions.FnEtlGetSubPopulationCategoriesForSubPopulations.Execute(Database, _selectedExportConnectionId);

            if (!string.IsNullOrEmpty(metadata))
            {
                List<int?> subPopulationTarget = NfiEstaFunctions.FnEtlCheckSubPopulationCategoriesForSubPopulations.Execute(
                    FormUserPassword.TargetDatabase,
                    metadata);

                Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                if (subPopulationTarget != null)
                {
                    DataTable subPopulationsWithDifferentCategories = TDFunctions.FnEtlGetListOfSubPopulationsForUpdate.ExecuteQuery(
                        Database,
                        _selectedExportConnectionId,
                        subPopulationTarget);

                    dgvType.DataSource = subPopulationsWithDifferentCategories;

                    FormatDataGridView(dgvType);

                    dgvType.Columns["etl_sub_population"].Visible = false;
                    dgvType.Columns["sub_population_target"].Visible = false;
                    dgvType.Columns["description"].Visible = false;
                    dgvType.Columns["description_en"].Visible = false;

                    switch (LanguageVersion)
                    {
                        case LanguageVersion.National:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "subPopulationLabel") ? labels["subPopulationLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "subPopulationLabelEn") ? labels["subPopulationLabelEn"] : String.Empty;
                            break;

                        case LanguageVersion.International:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "subPopulationLabel") ? labels["subPopulationLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "subPopulationLabelEn") ? labels["subPopulationLabelEn"] : String.Empty;
                            break;
                    }
                }
                else
                {
                    DataTable emptyDataTable = new DataTable();

                    emptyDataTable.Columns.Add("label", typeof(string));
                    emptyDataTable.Columns.Add("label_en", typeof(string));

                    dgvType.DataSource = emptyDataTable;

                    FormatDataGridView(dgvType);

                    switch (LanguageVersion)
                    {
                        case LanguageVersion.National:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "subPopulationLabel") ? labels["subPopulationLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "subPopulationLabelEn") ? labels["subPopulationLabelEn"] : String.Empty;
                            break;

                        case LanguageVersion.International:
                            dgvType.Columns["label"].HeaderText = labels.ContainsKey(key: "subPopulationLabel") ? labels["subPopulationLabel"] : String.Empty;
                            dgvType.Columns["label_en"].HeaderText = labels.ContainsKey(key: "subPopulationLabelEn") ? labels["subPopulationLabelEn"] : String.Empty;
                            break;
                    }
                }
            }
            else { dgvType.DataSource = null; }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek pro kategorie subpopulací a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data for the subpopulation categories and displaying it in the control</para>
        /// </summary>
        private void LoadContentSPC()
        {
            if (dgvType.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dgvType.SelectedRows[0];

                int etlSubPopulation = Convert.ToInt32(selectedRow.Cells["etl_sub_population"].Value);
                int subPopulationTarget = Convert.ToInt32(selectedRow.Cells["sub_population_target"].Value);

                string metadata = TDFunctions.FnEtlGetSubPopulationCategoriesForUpdate.Execute(
                    Database,
                    etlSubPopulation,
                    subPopulationTarget);

                DataTable result = NfiEstaFunctions.FnEtlCheckSubPopulationCategoriesForUpdate.ExecuteQuery(
                    FormUserPassword.TargetDatabase,
                    metadata);

                dgv.DataSource = result;

                FormatDataGridView(dgv);

                dgv.Columns["sub_population_category"].Visible = false;
                dgv.Columns["description_source"].Visible = false;
                dgv.Columns["description_en_source"].Visible = false;
                dgv.Columns["label_target"].Visible = false;
                dgv.Columns["description_target"].Visible = false;
                dgv.Columns["label_en_target"].Visible = false;
                dgv.Columns["description_en_target"].Visible = false;

                UpdateColumnHeaders();
            }
        }

        /// <summary>
        /// <para lang="cs">Přepínání mezi jazyky hlaviček sloupců DataGridView dgv</para>
        /// <para lang="en">Přepínání mezi jazyky hlaviček sloupců in the dgv DataGridView</para>
        /// </summary>
        private void UpdateColumnHeaders()
        {
            Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    dgv.Columns["label_source"].HeaderText = labels.ContainsKey(key: "labelNatSource") ? labels["labelNatSource"] : String.Empty;
                    dgv.Columns["label_en_source"].HeaderText = labels.ContainsKey(key: "labelEnSource") ? labels["labelEnSource"] : String.Empty;
                    dgv.Columns["check_label"].HeaderText = labels.ContainsKey(key: "checkLabel") ? labels["checkLabel"] : String.Empty;
                    dgv.Columns["check_description"].HeaderText = labels.ContainsKey(key: "checkDescription") ? labels["checkDescription"] : String.Empty;
                    dgv.Columns["check_label_en"].HeaderText = labels.ContainsKey(key: "checkLabelEn") ? labels["checkLabelEn"] : String.Empty;
                    dgv.Columns["check_description_en"].HeaderText = labels.ContainsKey(key: "checkDescriptionEn") ? labels["checkDescriptionEn"] : String.Empty;
                    break;

                case LanguageVersion.International:
                    dgv.Columns["label_source"].HeaderText = labels.ContainsKey(key: "labelNatSource") ? labels["labelNatSource"] : String.Empty;
                    dgv.Columns["label_en_source"].HeaderText = labels.ContainsKey(key: "labelEnSource") ? labels["labelEnSource"] : String.Empty;
                    dgv.Columns["check_label"].HeaderText = labels.ContainsKey(key: "checkLabel") ? labels["checkLabel"] : String.Empty;
                    dgv.Columns["check_description"].HeaderText = labels.ContainsKey(key: "checkDescription") ? labels["checkDescription"] : String.Empty;
                    dgv.Columns["check_label_en"].HeaderText = labels.ContainsKey(key: "checkLabelEn") ? labels["checkLabelEn"] : String.Empty;
                    dgv.Columns["check_description_en"].HeaderText = labels.ContainsKey(key: "checkDescriptionEn") ? labels["checkDescriptionEn"] : String.Empty;
                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Změní barvu pozadí pro specifikovaná tlačítka na ControlLight a pro kliknuté tlačítko na bílou.</para>
        /// <para lang="en">Changes the background color of specified buttons to ControlLight and the clicked button to white.</para>
        /// </summary>
        /// <param name="clickedButton">
        /// <para lang="cs">Tlačítko, na které bylo kliknuto, které se má změnit na bílou barvu.</para>
        /// <para lang="en">The button that was clicked, which will change to white color.</para>
        /// </param>
        private void ChangeButtonBackColor(Button clickedButton)
        {
            btnAD.BackColor = SystemColors.ControlLight;
            btnADC.BackColor = SystemColors.ControlLight;
            btnSP.BackColor = SystemColors.ControlLight;
            btnSPC.BackColor = SystemColors.ControlLight;

            clickedButton.BackColor = Color.White;
        }

        /// <summary>
        /// <para lang="cs">Vyčištění textových polí formuláře. Tato metoda nastaví text všech uvedených textových polí na prázdný řetězec.</para>
        /// <para lang="en">Clears the form's text boxes. This method sets the text of all the listed text boxes to an empty string.</para>
        /// </summary>
        private void ClearTextBoxes()
        {
            txtLabelNatSourceDB.Text = String.Empty;
            txtLabelNatTargetDB.Text = String.Empty;
            txtDesNatSourceDB.Text = String.Empty;
            txtDesNatTargetDB.Text = String.Empty;
            txtLabelEnSourceDB.Text = String.Empty;
            txtLabelEnTargetDB.Text = String.Empty;
            txtDesEnSourceDB.Text = String.Empty;
            txtDesEnTargetDB.Text = String.Empty;
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události změny výběru řádku v DataGridView dgv</para>
        /// <para lang="en">Handling the row selection change event in DataGridView dgv</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            ClearTextBoxes();

            if (dgv.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dgv.SelectedRows[0];

                bool checkLabelValue = Convert.ToBoolean(selectedRow.Cells["check_label"].Value);
                bool checkDescriptionValue = Convert.ToBoolean(selectedRow.Cells["check_description"].Value);
                bool checkLabelEnValue = Convert.ToBoolean(selectedRow.Cells["check_label_en"].Value);
                bool checkDescriptionEnValue = Convert.ToBoolean(selectedRow.Cells["check_description_en"].Value);

                if (!checkLabelValue)
                {
                    string labelSourceValue = Convert.ToString(selectedRow.Cells["label_source"].Value);
                    string labelTargetValue = Convert.ToString(selectedRow.Cells["label_target"].Value);

                    txtLabelNatSourceDB.Text = labelSourceValue;
                    txtLabelNatTargetDB.Text = labelTargetValue;
                }

                if (!checkDescriptionValue)
                {
                    string descriptionSourceValue = Convert.ToString(selectedRow.Cells["description_source"].Value);
                    string descriptionTargetValue = Convert.ToString(selectedRow.Cells["description_target"].Value);

                    txtDesNatSourceDB.Text = descriptionSourceValue;
                    txtDesNatTargetDB.Text = descriptionTargetValue;
                }

                if (!checkLabelEnValue)
                {
                    string labelEnSourceValue = Convert.ToString(selectedRow.Cells["label_en_source"].Value);
                    string labelEnTargetValue = Convert.ToString(selectedRow.Cells["label_en_target"].Value);

                    txtLabelEnSourceDB.Text = labelEnSourceValue;
                    txtLabelEnTargetDB.Text = labelEnTargetValue;
                }

                if (!checkDescriptionEnValue)
                {
                    string descriptionSourceEnValue = Convert.ToString(selectedRow.Cells["description_en_source"].Value);
                    string descriptionTargetEnValue = Convert.ToString(selectedRow.Cells["description_en_target"].Value);

                    txtDesEnSourceDB.Text = descriptionSourceEnValue;
                    txtDesEnTargetDB.Text = descriptionTargetEnValue;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události změny výběru řádku v DataGridView dgvType</para>
        /// <para lang="en">Handling the row selection change event in DataGridView dgvType</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void dgvType_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvType.Columns.Contains("etl_area_domain"))
            {
                LoadContentADC();
            }
            else if (dgvType.Columns.Contains("etl_sub_population"))
            {
                LoadContentSPC();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události překreslení zobrazení buněk v DataGridView dgv</para>
        /// <para lang="en">Event handler of the cell view redraw in DataGridView dgv</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void dgv_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // change the standard display of boolean results in certain columns to text
            if ((e.ColumnIndex == dgv.Columns["check_label"].Index
                    || e.ColumnIndex == dgv.Columns["check_description"].Index
                    || e.ColumnIndex == dgv.Columns["check_label_en"].Index
                    || e.ColumnIndex == dgv.Columns["check_description_en"].Index)
                && e.RowIndex >= 0)
            {
                e.PaintBackground(e.CellBounds, false);

                Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                bool cellValue = (bool)e.Value;
                string text = (LanguageVersion == LanguageVersion.National && cellValue) ? labels.ContainsKey(key: "no") ? labels["no"] : String.Empty
                            : (LanguageVersion == LanguageVersion.National && !cellValue) ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty
                            : cellValue ? labels.ContainsKey(key: "no") ? labels["no"] : String.Empty : labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty;

                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    // set the font to Microsoft Sans Serif, 9.75pt
                    e.CellStyle.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular);

                    // move the text lower by 3 pixels
                    Rectangle textRect = new Rectangle(e.CellBounds.X, e.CellBounds.Y + 3,
                                                        e.CellBounds.Width, e.CellBounds.Height - 3);
                    e.Graphics.DrawString(text, e.CellStyle.Font, brush, textRect);
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Plošné domény"</para>
        /// <para lang="en">Handling the "Area domains" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnAD_Click(object sender, EventArgs e)
        {
            ChangeButtonBackColor(btnAD);

            ClearTextBoxes();

            LoadContentAD();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Subpopulace"</para>
        /// <para lang="en">Handling the "Subpopulations" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnSP_Click(object sender, EventArgs e)
        {
            ChangeButtonBackColor(btnSP);

            ClearTextBoxes();

            LoadContentSP();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Kategorie plošných domén"</para>
        /// <para lang="en">Handling the "Area domain categories" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnADC_Click(object sender, EventArgs e)
        {
            ChangeButtonBackColor(btnADC);

            dgv.DataSource = null;
            dgv.Rows.Clear();
            ClearTextBoxes();

            LoadContentADForADC();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Kategorie subpopulací"</para>
        /// <para lang="en">Handling the "Subpopulation categories" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnSPC_Click(object sender, EventArgs e)
        {
            ChangeButtonBackColor(btnSPC);

            dgv.DataSource = null;
            dgv.Rows.Clear();
            ClearTextBoxes();

            LoadContentSPForSPC();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Aktualizovat" zkratku národní</para>
        /// <para lang="en">Handling the "Update" national label button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnUpdateLabelNat_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

            if (dgv.SelectedRows.Count > 0 && !String.IsNullOrEmpty(txtLabelNatSourceDB.Text))
            {
                DataGridViewRow selectedRow = dgv.SelectedRows[0];

                if (dgv.Columns.Contains("area_domain"))
                {
                    int areaDomainValue = Convert.ToInt32(selectedRow.Cells["area_domain"].Value);
                    string labelSourceValue = Convert.ToString(selectedRow.Cells["label_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelNat") ?
                                                    messages["msgUpdateLabelNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainLabel.Execute(
                            FormUserPassword.TargetDatabase,
                            areaDomainValue,
                            nationalLang.NatLang.ToString(),
                            labelSourceValue);

                        txtLabelNatSourceDB.Text = String.Empty;
                        txtLabelNatTargetDB.Text = String.Empty;

                        LoadContentAD();
                    }
                }
                else if (dgv.Columns.Contains("sub_population"))
                {
                    int subPopulationValue = Convert.ToInt32(selectedRow.Cells["sub_population"].Value);
                    string labelSourceValue = Convert.ToString(selectedRow.Cells["label_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelNat") ?
                                                    messages["msgUpdateLabelNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationLabel.Execute(
                            FormUserPassword.TargetDatabase,
                            subPopulationValue,
                            nationalLang.NatLang.ToString(),
                            labelSourceValue);

                        txtLabelNatSourceDB.Text = String.Empty;
                        txtLabelNatTargetDB.Text = String.Empty;

                        LoadContentSP();
                    }
                }
                else if (dgv.Columns.Contains("area_domain_category"))
                {
                    int adcValue = Convert.ToInt32(selectedRow.Cells["area_domain_category"].Value);
                    string labelSourceValue = Convert.ToString(selectedRow.Cells["label_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelNat") ?
                                                    messages["msgUpdateLabelNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryLabel.Execute(
                            FormUserPassword.TargetDatabase,
                            adcValue,
                            nationalLang.NatLang.ToString(),
                            labelSourceValue);

                        txtLabelNatSourceDB.Text = String.Empty;
                        txtLabelNatTargetDB.Text = String.Empty;

                        LoadContentADC();
                        LoadContentADForADC();
                    }
                }
                else if (dgv.Columns.Contains("sub_population_category"))
                {
                    int spcValue = Convert.ToInt32(selectedRow.Cells["sub_population_category"].Value);
                    string labelSourceValue = Convert.ToString(selectedRow.Cells["label_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelNat") ?
                                                    messages["msgUpdateLabelNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryLabel.Execute(
                            FormUserPassword.TargetDatabase,
                            spcValue,
                            nationalLang.NatLang.ToString(),
                            labelSourceValue);

                        txtLabelNatSourceDB.Text = String.Empty;
                        txtLabelNatTargetDB.Text = String.Empty;

                        LoadContentSPC();
                        LoadContentSPForSPC();
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Aktualizovat" popis národní</para>
        /// <para lang="en">Handling the "Update" national description button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnUpdateDesNat_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

            if (dgv.SelectedRows.Count > 0 && !String.IsNullOrEmpty(txtDesNatSourceDB.Text))
            {
                DataGridViewRow selectedRow = dgv.SelectedRows[0];

                if (dgv.Columns.Contains("area_domain"))
                {
                    int areaDomainValue = Convert.ToInt32(selectedRow.Cells["area_domain"].Value);
                    string descriptionSourceValue = Convert.ToString(selectedRow.Cells["description_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesNat") ?
                                                    messages["msgUpdateDesNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        areaDomainValue,
                        nationalLang.NatLang.ToString(),
                        descriptionSourceValue);

                        txtDesNatSourceDB.Text = String.Empty;
                        txtDesNatTargetDB.Text = String.Empty;

                        LoadContentAD();
                    }
                }
                else if (dgv.Columns.Contains("sub_population"))
                {
                    int subPopulationValue = Convert.ToInt32(selectedRow.Cells["sub_population"].Value);
                    string descriptionSourceValue = Convert.ToString(selectedRow.Cells["description_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesNat") ?
                                                    messages["msgUpdateDesNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        subPopulationValue,
                        nationalLang.NatLang.ToString(),
                        descriptionSourceValue);

                        txtDesNatSourceDB.Text = String.Empty;
                        txtDesNatTargetDB.Text = String.Empty;

                        LoadContentSP();
                    }
                }
                else if (dgv.Columns.Contains("area_domain_category"))
                {
                    int adcValue = Convert.ToInt32(selectedRow.Cells["area_domain_category"].Value);
                    string descriptionSourceValue = Convert.ToString(selectedRow.Cells["description_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesNat") ?
                                                    messages["msgUpdateDesNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        adcValue,
                        nationalLang.NatLang.ToString(),
                        descriptionSourceValue);

                        txtDesNatSourceDB.Text = String.Empty;
                        txtDesNatTargetDB.Text = String.Empty;

                        LoadContentADC();
                        LoadContentADForADC();
                    }
                }
                else if (dgv.Columns.Contains("sub_population_category"))
                {
                    int spcValue = Convert.ToInt32(selectedRow.Cells["sub_population_category"].Value);
                    string descriptionSourceValue = Convert.ToString(selectedRow.Cells["description_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesNat") ?
                                                    messages["msgUpdateDesNat"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        spcValue,
                        nationalLang.NatLang.ToString(),
                        descriptionSourceValue);

                        txtDesNatSourceDB.Text = String.Empty;
                        txtDesNatTargetDB.Text = String.Empty;

                        LoadContentSPC();
                        LoadContentSPForSPC();
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Aktualizovat" zkratku anglickou</para>
        /// <para lang="en">Handling the "Update" English label button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnUpdateLabelEn_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

            if (dgv.SelectedRows.Count > 0 && !String.IsNullOrEmpty(txtLabelEnSourceDB.Text))
            {
                DataGridViewRow selectedRow = dgv.SelectedRows[0];

                if (dgv.Columns.Contains("area_domain"))
                {
                    int areaDomainValue = Convert.ToInt32(selectedRow.Cells["area_domain"].Value);
                    string labelEnSourceValue = Convert.ToString(selectedRow.Cells["label_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelEn") ?
                                                    messages["msgUpdateLabelEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainLabel.Execute(
                        FormUserPassword.TargetDatabase,
                        areaDomainValue,
                        "en",
                        labelEnSourceValue);

                        txtLabelEnSourceDB.Text = String.Empty;
                        txtLabelEnTargetDB.Text = String.Empty;

                        LoadContentAD();
                    }
                }
                else if (dgv.Columns.Contains("sub_population"))
                {
                    int subPopulationValue = Convert.ToInt32(selectedRow.Cells["sub_population"].Value);
                    string labelEnSourceValue = Convert.ToString(selectedRow.Cells["label_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelEn") ?
                                                    messages["msgUpdateLabelEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationLabel.Execute(
                        FormUserPassword.TargetDatabase,
                        subPopulationValue,
                        "en",
                        labelEnSourceValue);

                        txtLabelEnSourceDB.Text = String.Empty;
                        txtLabelEnTargetDB.Text = String.Empty;

                        LoadContentSP();
                    }
                }
                else if (dgv.Columns.Contains("area_domain_category"))
                {
                    int adcValue = Convert.ToInt32(selectedRow.Cells["area_domain_category"].Value);
                    string labelEnSourceValue = Convert.ToString(selectedRow.Cells["label_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelEn") ?
                                                    messages["msgUpdateLabelEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryLabel.Execute(
                        FormUserPassword.TargetDatabase,
                        adcValue,
                        "en",
                        labelEnSourceValue);

                        txtLabelEnSourceDB.Text = String.Empty;
                        txtLabelEnTargetDB.Text = String.Empty;

                        LoadContentADC();
                        LoadContentADForADC();
                    }
                }
                else if (dgv.Columns.Contains("sub_population_category"))
                {
                    int spcValue = Convert.ToInt32(selectedRow.Cells["sub_population_category"].Value);
                    string labelEnSourceValue = Convert.ToString(selectedRow.Cells["label_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateLabelEn") ?
                                                    messages["msgUpdateLabelEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryLabel.Execute(
                        FormUserPassword.TargetDatabase,
                        spcValue,
                        "en",
                        labelEnSourceValue);

                        txtLabelEnSourceDB.Text = String.Empty;
                        txtLabelEnTargetDB.Text = String.Empty;

                        LoadContentSPC();
                        LoadContentSPForSPC();
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Aktualizovat" popis anglický</para>
        /// <para lang="en">Handling the "Update" English description button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnUpdateDesEn_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

            if (dgv.SelectedRows.Count > 0 && !String.IsNullOrEmpty(txtDesEnSourceDB.Text))
            {
                DataGridViewRow selectedRow = dgv.SelectedRows[0];

                if (dgv.Columns.Contains("area_domain"))
                {
                    int areaDomainValue = Convert.ToInt32(selectedRow.Cells["area_domain"].Value);
                    string descriptionEnSourceValue = Convert.ToString(selectedRow.Cells["description_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesEn") ?
                                                    messages["msgUpdateDesEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        areaDomainValue,
                        "en",
                        descriptionEnSourceValue);

                        txtDesEnSourceDB.Text = String.Empty;
                        txtDesEnTargetDB.Text = String.Empty;

                        LoadContentAD();
                    }
                }
                else if (dgv.Columns.Contains("sub_population"))
                {
                    int subPopulationValue = Convert.ToInt32(selectedRow.Cells["sub_population"].Value);
                    string descriptionEnSourceValue = Convert.ToString(selectedRow.Cells["description_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesEn") ?
                                                    messages["msgUpdateDesEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        subPopulationValue,
                        "en",
                        descriptionEnSourceValue);

                        txtDesEnSourceDB.Text = String.Empty;
                        txtDesEnTargetDB.Text = String.Empty;

                        LoadContentSP();
                    }
                }
                else if (dgv.Columns.Contains("area_domain_category"))
                {
                    int adcValue = Convert.ToInt32(selectedRow.Cells["area_domain_category"].Value);
                    string descriptionEnSourceValue = Convert.ToString(selectedRow.Cells["description_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesEn") ?
                                                    messages["msgUpdateDesEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        adcValue,
                        "en",
                        descriptionEnSourceValue);

                        txtDesEnSourceDB.Text = String.Empty;
                        txtDesEnTargetDB.Text = String.Empty;

                        LoadContentADC();
                        LoadContentADForADC();
                    }
                }
                else if (dgv.Columns.Contains("sub_population_category"))
                {
                    int spcValue = Convert.ToInt32(selectedRow.Cells["sub_population_category"].Value);
                    string descriptionEnSourceValue = Convert.ToString(selectedRow.Cells["description_en_source"].Value);

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateDesEn") ?
                                                    messages["msgUpdateDesEn"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryDescription.Execute(
                        FormUserPassword.TargetDatabase,
                        spcValue,
                        "en",
                        descriptionEnSourceValue);

                        txtDesEnSourceDB.Text = String.Empty;
                        txtDesEnTargetDB.Text = String.Empty;

                        LoadContentSPC();
                        LoadContentSPForSPC();
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Aktualizovat vše"</para>
        /// <para lang="en">Handling the "Update vše" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnUpdateAll_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

            if (dgv.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dgv.SelectedRows[0];
                string labelSourceValue = Convert.ToString(selectedRow.Cells["label_source"].Value);
                string descriptionSourceValue = Convert.ToString(selectedRow.Cells["description_source"].Value);
                string labelEnSourceValue = Convert.ToString(selectedRow.Cells["label_en_source"].Value);
                string descriptionEnSourceValue = Convert.ToString(selectedRow.Cells["description_en_source"].Value);

                if (dgv.Columns.Contains("area_domain"))
                {
                    int areaDomainValue = Convert.ToInt32(selectedRow.Cells["area_domain"].Value);

                    if (String.IsNullOrEmpty(txtLabelNatSourceDB.Text) && String.IsNullOrEmpty(txtDesNatSourceDB.Text)
                    && String.IsNullOrEmpty(txtLabelEnSourceDB.Text) && String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                    {
                        // All textboxes are empty, the dialog is not shown
                        return;
                    }

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateAll") ?
                                                    messages["msgUpdateAll"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        if (!String.IsNullOrEmpty(txtLabelNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                areaDomainValue,
                                nationalLang.NatLang.ToString(),
                                labelSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainDescription.Execute(
                               FormUserPassword.TargetDatabase,
                               areaDomainValue,
                               nationalLang.NatLang.ToString(),
                               descriptionSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtLabelEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                areaDomainValue,
                                "en",
                                labelEnSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainDescription.Execute(
                                FormUserPassword.TargetDatabase,
                                areaDomainValue,
                                "en",
                                descriptionEnSourceValue);
                        }

                        ClearTextBoxes();
                        LoadContentAD();
                    }
                }
                else if (dgv.Columns.Contains("sub_population"))
                {
                    int subPopulationValue = Convert.ToInt32(selectedRow.Cells["sub_population"].Value);

                    if (String.IsNullOrEmpty(txtLabelNatSourceDB.Text) && String.IsNullOrEmpty(txtDesNatSourceDB.Text)
                    && String.IsNullOrEmpty(txtLabelEnSourceDB.Text) && String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                    {
                        // All textboxes are empty, the dialog is not shown
                        return;
                    }

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateAll") ?
                                                    messages["msgUpdateAll"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        if (!String.IsNullOrEmpty(txtLabelNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                subPopulationValue,
                                nationalLang.NatLang.ToString(),
                                labelSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationDescription.Execute(
                                FormUserPassword.TargetDatabase,
                                subPopulationValue,
                                nationalLang.NatLang.ToString(),
                                descriptionSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtLabelEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                subPopulationValue,
                                "en",
                                labelEnSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationDescription.Execute(
                                FormUserPassword.TargetDatabase,
                                subPopulationValue,
                                "en",
                                descriptionEnSourceValue);
                        }

                        ClearTextBoxes();

                        LoadContentSP();
                    }
                }
                else if (dgv.Columns.Contains("area_domain_category"))
                {
                    int adcValue = Convert.ToInt32(selectedRow.Cells["area_domain_category"].Value);

                    if (String.IsNullOrEmpty(txtLabelNatSourceDB.Text) && String.IsNullOrEmpty(txtDesNatSourceDB.Text)
                    && String.IsNullOrEmpty(txtLabelEnSourceDB.Text) && String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                    {
                        // All textboxes are empty, the dialog is not shown
                        return;
                    }

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateAll") ?
                                                    messages["msgUpdateAll"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        if (!String.IsNullOrEmpty(txtLabelNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                adcValue,
                                nationalLang.NatLang.ToString(),
                                labelSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryDescription.Execute(
                                FormUserPassword.TargetDatabase,
                                adcValue,
                                nationalLang.NatLang.ToString(),
                                descriptionSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtLabelEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                adcValue,
                                "en",
                                labelEnSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateAreaDomainCategoryDescription.Execute(
                                FormUserPassword.TargetDatabase,
                                adcValue,
                                "en",
                                descriptionEnSourceValue);
                        }

                        ClearTextBoxes();

                        LoadContentADC();
                        LoadContentADForADC();
                    }
                }
                else if (dgv.Columns.Contains("sub_population_category"))
                {
                    int spcValue = Convert.ToInt32(selectedRow.Cells["sub_population_category"].Value);

                    if (String.IsNullOrEmpty(txtLabelNatSourceDB.Text) && String.IsNullOrEmpty(txtDesNatSourceDB.Text)
                    && String.IsNullOrEmpty(txtLabelEnSourceDB.Text) && String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                    {
                        // All textboxes are empty, the dialog is not shown
                        return;
                    }

                    DialogResult dialogResult = MessageBox.Show(
                                            text: messages.ContainsKey(key: "msgUpdateAll") ?
                                                    messages["msgUpdateAll"] : String.Empty,
                                            caption: String.Empty,
                                            buttons: MessageBoxButtons.YesNo,
                                            icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        if (!String.IsNullOrEmpty(txtLabelNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                spcValue,
                                nationalLang.NatLang.ToString(),
                                labelSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesNatSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryDescription.Execute(
                               FormUserPassword.TargetDatabase,
                               spcValue,
                               nationalLang.NatLang.ToString(),
                               descriptionSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtLabelEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryLabel.Execute(
                                FormUserPassword.TargetDatabase,
                                spcValue,
                                "en",
                                labelEnSourceValue);
                        }
                        if (!String.IsNullOrEmpty(txtDesEnSourceDB.Text))
                        {
                            NfiEstaFunctions.FnEtlUpdateSubPopulationCategoryDescription.Execute(
                                FormUserPassword.TargetDatabase,
                                spcValue,
                                "en",
                                descriptionEnSourceValue);
                        }

                        ClearTextBoxes();

                        LoadContentSPC();
                        LoadContentSPForSPC();
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zavřít"</para>
        /// <para lang="en">Handling the "Close" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion Event Handlers
    }
}
