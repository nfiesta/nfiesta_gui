﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace Hanakova
{
    namespace ModuleEtl
    {

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class Setting
        {

            #region Constants

#if DEBUG
            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Default value - Database server connection parameters</para>
            /// </summary>
            private static readonly ZaJi.PostgreSQL.Setting DefaultDBSetting = new()
            {
                ApplicationName = "NfiEstaGUI: Data transfer module",
                LanguageVersion = LanguageVersion.International,
                Host = "localhost",
                Hosts = ["localhost", "bran-pga-dev"],
                Port = 5432,
                Ports = [5432, 5433],
                Database = "contrib_regression_target_data_etl",
                Databases = ["contrib_regression_target_data_etl", "nfi_analytical"],
                UserName = "vagrant",
                UserNames = [
                    "KMAdolt", "KMFejfar", "KMHanakova", "KMHejlova",
                    "KMKratena", "KMKohn", "KMZavodskyJ", "KMPiskytlova", "postgres", "vagrant" ],
                CommandTimeout = 0,         // SQL příkazy mají neomezenou dobu na vykonání
                KeepAlive = 180,            // keep alive je posíláno 1x za 3 minuty
                TcpKeepAlive = true,        // tcp keep alive zapnuto
                TcpKeepAliveTime = 60,      // tcp keep alive je posíláno 1x za 1 minutu
                TcpKeepAliveInterval = 5
            };
#else
            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Default value - Database server connection parameters</para>
            /// </summary>
            private static readonly ZaJi.PostgreSQL.Setting DefaultDBSetting = new()
            {
                ApplicationName = "NfiEstaGUI: Data transfer module",
                LanguageVersion = LanguageVersion.International,
                Host = "bran-pga",
                Hosts = ["localhost", "bran-pga-dev", "bran-pga"],
                Port = 5432,
                Ports = [5432, 5433],
                Database = "nfi_analytical",
                Databases = ["contrib_regression_target_data_etl", "nfi_analytical"],
                UserName = "vagrant",
                UserNames = [
                    "KMAdolt", "KMFejfar", "KMHanakova", "KMHejlova",
                    "KMKratena", "KMKohn", "KMZavodskyJ", "KMPiskytlova", "postgres", "vagrant" ],
                CommandTimeout = 0,         // SQL příkazy mají neomezenou dobu na vykonání
                KeepAlive = 180,            // keep alive je posíláno 1x za 3 minuty
                TcpKeepAlive = true,        // tcp keep alive zapnuto
                TcpKeepAliveTime = 60,      // tcp keep alive je posíláno 1x za 1 minutu
                TcpKeepAliveInterval = 5
            };
#endif

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazit stavový řádek</para>
            /// <para lang="en">Default value - Display status strip</para>
            /// </summary>
            private const bool DefaultStatusStripVisible = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Vzdálenost oddělovače mezi indikátorem a položkami metadat
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Default value - Splitter distance between indicator and metadata items
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private const int DefaultSplIndicatorDistance = 250;

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            private ZaJi.PostgreSQL.Setting dbSetting;

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            private Nullable<bool> statusStripVisible;

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače mezi indikátorem a položkami metadat
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance between indicator and metadata items
            /// in the Control "Indicator selection"</para>
            /// </summary>
            private Nullable<int> splIndicatorDistance;

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            private JsonSerializerOptions serializerOptions;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu</para>
            /// <para lang="en">Object constructor</para>
            /// </summary>
            public Setting()
            {
                SerializerOptions = null;
                DBSetting = DefaultDBSetting;
                StatusStripVisible = DefaultStatusStripVisible;
                SplIndicatorDistance = DefaultSplIndicatorDistance;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            public ZaJi.PostgreSQL.Setting DBSetting
            {
                get
                {
                    return dbSetting ?? DefaultDBSetting;
                }
                set
                {
                    dbSetting = value ?? DefaultDBSetting;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            public bool StatusStripVisible
            {
                get
                {
                    return statusStripVisible ?? DefaultStatusStripVisible;
                }
                set
                {
                    statusStripVisible = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vzdálenost oddělovače mezi indikátorem a položkami metadat
            /// na ovládacím prvku "Volba indikátoru"</para>
            /// <para lang="en">Splitter distance between indicator and metadata items
            /// in the Control "Indicator selection"</para>
            /// </summary>
            public int SplIndicatorDistance
            {
                get
                {
                    return splIndicatorDistance ?? DefaultSplIndicatorDistance;
                }
                set
                {
                    splIndicatorDistance = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            [JsonIgnore]
            private JsonSerializerOptions SerializerOptions
            {
                get
                {
                    return serializerOptions ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
                set
                {
                    serializerOptions = value ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
            }

            /// <summary>
            /// <para lang="cs">Serializace objektu do formátu JSON</para>
            /// <para lang="en">Object serialization to JSON format</para>
            /// </summary>
            [JsonIgnore]
            public string JSON
            {
                get
                {
                    return
                        JsonSerializer.Serialize(
                            value: this,
                            options: SerializerOptions);
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Obnovení objektu z formátu JSON</para>
            /// <para lang="en">Restoring an object from JSON format</para>
            /// </summary>
            /// <param name="json">
            /// <para lang="cs">JSON text</para>
            /// <para lang="en">JSON text</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Nastavení modulu</para>
            /// <para lang="en">Module setting</para>
            /// </returns>
            public static Setting Deserialize(string json)
            {
                try
                {
                    return
                        JsonSerializer.Deserialize<Setting>(json: json);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Exclamation);

                    return new Setting();
                }
            }

            /// <summary>
            /// <para lang="cs">Načte objekt nastavení ze souboru json</para>
            /// <para lang="en">Loads setting object from json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací načtený objekt nastavení</para>
            /// <para lang="en">Return loaded setting object</para>
            /// </returns>
            public static Setting LoadFromFile(string path)
            {
                try
                {
                    return Deserialize(
                        json: File.ReadAllText(
                            path: path,
                            encoding: Encoding.UTF8));
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return new Setting();
                }
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Uloží objekt nastavení do souboru json</para>
            /// <para lang="en">Save setting object into json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            public void SaveToFile(string path)
            {
                try
                {
                    File.WriteAllText(
                        path: path,
                        contents: JSON,
                        encoding: Encoding.UTF8);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            #endregion Methods

        }

    }
}
