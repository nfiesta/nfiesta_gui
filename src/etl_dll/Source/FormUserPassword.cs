﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Formulář pro zadání uživatele a hesla</para>
    /// <para lang="en">Form to type a user and password</para>
    /// </summary>
    public partial class FormUserPassword
            : Form, INfiEstaControl, IETLControl
    {

        #region Static Fields

        /// <summary>
        /// <para lang="cs">Vykonání SQL příkazu v databázi skončilo chybou</para>
        /// <para lang="en">SQL command execution on database finished with error</para>
        /// </summary>
        public static bool DbCommandError = false;

        #endregion Static Fields


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Hostitel (server) zvoleného databázového připojení, vybraný na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection host (server), which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly string SelectedExportConnectionHost;

        /// <summary>
        /// <para lang="cs">Název databáze zvoleného databázového připojení, vybraný na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection name, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly string SelectedExportConnectionDbName;

        /// <summary>
        /// <para lang="cs">Port zvoleného databázového připojení, vybraný na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection port, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int SelectedExportConnectionPort;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro zadání uživatele a hesla</para>
        /// <para lang="en">Form constructor to type a user and password</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="_SelectedExportConnectionHost">
        /// <para lang="cs">Hostitel (server) zvoleného databázového připojení, vybraný na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection host (server), which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="_SelectedExportConnectionDbName">
        /// <para lang="cs">Název databáze zvoleného databázového připojení, vybraný na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection name, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="_SelectedExportConnectionPort">
        /// <para lang="cs">Port zvoleného databázového připojení, vybraný na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection port, which is selected on the 1st form with the list of connections</para>
        /// </param>
        public FormUserPassword(
            Control controlOwner,
            string _SelectedExportConnectionHost,
            string _SelectedExportConnectionDbName,
            int _SelectedExportConnectionPort
            )
        {
            InitializeComponent();

            Initialize(controlOwner: controlOwner);

            SelectedExportConnectionHost = _SelectedExportConnectionHost;
            SelectedExportConnectionDbName = _SelectedExportConnectionDbName;
            SelectedExportConnectionPort = _SelectedExportConnectionPort;
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlConnection ctrConnection)
                {
                    return ctrConnection.CtrEtl;
                }

                else if (ControlOwner is FormConnectionEdit frmConnectionEdit)
                {
                    return frmConnectionEdit.CtrEtl;
                }

                else if (ControlOwner is FormConnectionNew frmConnectionNew)
                {
                    return frmConnectionNew.CtrEtl;
                }

                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Cílová databáze</para>
        /// <para lang="en">Target database</para>
        /// </summary>
        public static PGWrapper TargetDatabase { get; set; } // TODO why is it static

        /// <summary>
        /// <para lang="cs">Indikátor, zda bylo připojení k cílové databázi úspěšně inicializováno</para>
        /// <para lang="en">Indicator whether the connection to the target database was successfully initialized</para>
        /// </summary>
        public bool ConnectionInitialized { get; private set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                     "Připojit se do zvolené cílové databáze"},
                            { "lblServer",                      "Server:" },
                            { "lblPort",                        "Port:" },
                            { "lblDatabase",                    "Databáze:" },
                            { "lblUser",                        "Uživatel:" },
                            { "lblPassword",                    "Heslo:" },
                            { "btnOK",                          "Připojit" },
                            { "btnCancel",                      "Zrušit" },
                            { "msgUserIsEmpty",                 "Uživatel musí být vyplněn." },
                            { "msgPasswordIsEmpty",             "Heslo musí být vyplněno." },
                            { "msgDatabaseExceptionReceived",   "Databáze hlásí chybu, nelze dál pokračovat v přenosu dat. Modul bude uzavřen, kontaktujte vývojáře." },
                            { "msgNoNfiEstaDbExtension",        "V databázi není nainstalovaná extenze $1." },
                            { "msgNoNfiEstaDbSchema",           "V databázi neexistuje schéma $1." },
                            { "msgInvalidNfiEstaDbVersion",     "Nainstalovaná verze databázové extenze $1 $2 se neshoduje s očekávanou verzí $3." },
                            { "msgValidNfiEstaDbVersion",       "Databázová extenze $1 $2 [$3]." },
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "FormUserPassword") ?
                    languageFile.NationalVersion.Data["FormUserPassword"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                     "Connect to the selected target database" },
                            { "lblServer",                      "Server:" },
                            { "lblPort",                        "Port:" },
                            { "lblDatabase",                    "Database:" },
                            { "lblUser",                        "User:" },
                            { "lblPassword",                    "Password:" },
                            { "btnOK",                          "Connect" },
                            { "btnCancel",                      "Cancel" },
                            { "msgUserIsEmpty",                 "User cannot be empty." },
                            { "msgPasswordIsEmpty",             "Password cannot be empty." },
                            { "msgDatabaseExceptionReceived",   "The database reports an error, the data transfer cannot continue. The module will be closed, contact the developers." },
                            { "msgNoNfiEstaDbExtension",        "Database extension $1 is not installed."},
                            { "msgNoNfiEstaDbSchema",           "Schema $1 does not exist in the database." },
                            { "msgInvalidNfiEstaDbVersion",     "Installed version of the database extension $1 $2 does not correspond with expected version $3." },
                            { "msgValidNfiEstaDbVersion",       "Database extension $1 $2 [$3]." },
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "FormUserPassword") ?
                    languageFile.InternationalVersion.Data["FormUserPassword"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro zadání uživatele a hesla</para>
        /// <para lang="en">Initializing the form to type a user and password</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro zadání uživatele a hesla</para>
        /// <para lang="en">Initializing form labels to type a user and password</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text =
               labels.ContainsKey(key: "frmCaption") ?
               labels["frmCaption"] : String.Empty;

            lblServer.Text =
                labels.ContainsKey(key: "lblServer") ?
                labels["lblServer"] : String.Empty;

            lblPort.Text =
                labels.ContainsKey(key: "lblPort") ?
                labels["lblPort"] : String.Empty;

            lblDatabase.Text =
               labels.ContainsKey(key: "lblDatabase") ?
               labels["lblDatabase"] : String.Empty;

            lblUser.Text =
                labels.ContainsKey(key: "lblUser") ?
                labels["lblUser"] : String.Empty;

            lblPassword.Text =
                labels.ContainsKey(key: "lblPassword") ?
                labels["lblPassword"] : String.Empty;

            btnOK.Text =
                labels.ContainsKey(key: "btnOK") ?
                labels["btnOK"] : String.Empty;

            btnCancel.Text =
                labels.ContainsKey(key: "btnCancel") ?
                labels["btnCancel"] : String.Empty;

            CtrEtl.lblStatusExportConnection.Text = LanguageVersion.ToString();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Odpojí se od cílové databáze, pokud je nastavena</para>
        /// <para lang="en">Disconnects from the target database if it's set</para>
        /// </summary>
        public static void Disconnect()
        {
            if (TargetDatabase != null)
            {
                TargetDatabase.Postgres?.CloseConnection();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stav instalace extenze nfiesta</para>
        /// <para lang="en">Method displays installation status of the database extension nfiesta</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud v připojené databázi je nainstalovaná extenze nfiesta
        /// v kompatibilní verzi s aplikací. Jinak false.</para>
        /// <para lang="en">It returns true, when there is installed database extension nfiesta_target_data
        /// in compatible version with application.</para>
        /// </returns>
        public bool DisplayNfiEstaExtensionStatus()
        {
            Dictionary<string, string> messages = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            if (!TargetDatabase.Postgres.Initialized)
            {
                CtrEtl.lblNfiestaExtension.Text = String.Empty;
                return false;
            }

            string extensionName = null;
            string extensionVersion = null;

            // Retrieve extension information using SQL query
            using (var connection = new NpgsqlConnection(TargetDatabase.Postgres.ConnectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("SELECT extname, extversion FROM pg_extension WHERE extname = @extensionName", connection))
                {
                    command.Parameters.AddWithValue("extensionName", NfiEstaSchema.ExtensionName);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            extensionName = reader["extname"].ToString();
                            extensionVersion = reader["extversion"].ToString();
                        }
                        else
                        {
                            CtrEtl.lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                            //CtrEtl.lblNfiestaExtension.Text =
                            //    (messages.ContainsKey(key: "msgNoNfiEstaDbExtension") ?
                            //     messages["msgNoNfiEstaDbExtension"] : String.Empty)
                            //    .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName);
                            string messageText = messages.ContainsKey("msgNoNfiEstaDbExtension") ? messages["msgNoNfiEstaDbExtension"] : String.Empty;
                            string replacedMessageText = messageText.Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName);
                            CtrEtl.lblNfiestaExtension.Text = replacedMessageText;
                            MessageBox.Show(
                                 text: replacedMessageText,
                                 caption: String.Empty,
                                 buttons: MessageBoxButtons.OK,
                                 icon: MessageBoxIcon.Information);
                            TargetDatabase.Postgres?.CloseConnection();
                            Disconnect();
                            CtrEtl.pnlControls.Controls.Clear();
                            return false;
                        }
                    }
                }
            }

            ExtensionVersion dbExtensionVersion = new ExtensionVersion(version: extensionVersion);

            // Check if the extension's schema exists
            using (var connection = new NpgsqlConnection(TargetDatabase.Postgres.ConnectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("SELECT schema_name FROM information_schema.schemata WHERE schema_name = @schemaName", connection))
                {
                    command.Parameters.AddWithValue("schemaName", NfiEstaSchema.Name);
                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            CtrEtl.lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                            //CtrEtl.lblNfiestaExtension.Text =
                            //    (messages.ContainsKey(key: "msgNoNfiEstaDbSchema") ?
                            //     messages["msgNoNfiEstaDbSchema"] : String.Empty)
                            //    .Replace(oldValue: "$1", newValue: NfiEstaSchema.Name);
                            string messageText = messages.ContainsKey("msgNoNfiEstaDbSchema") ? messages["msgNoNfiEstaDbSchema"] : String.Empty;
                            string replacedMessageText = messageText.Replace(oldValue: "$1", newValue: NfiEstaSchema.Name);
                            CtrEtl.lblNfiestaExtension.Text = replacedMessageText;
                            MessageBox.Show(
                                 text: replacedMessageText,
                                 caption: String.Empty,
                                 buttons: MessageBoxButtons.OK,
                                 icon: MessageBoxIcon.Information);
                            TargetDatabase.Postgres?.CloseConnection();
                            Disconnect();
                            CtrEtl.pnlControls.Controls.Clear();
                            return false;
                        }
                    }
                }
            }

            if (!NfiEstaSchema.ExtensionVersion.MinorEquals(obj: dbExtensionVersion))
            {
                CtrEtl.lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                //CtrEtl.lblNfiestaExtension.Text =
                //    (messages.ContainsKey(key: "msgInvalidNfiEstaDbVersion") ?
                //     messages["msgInvalidNfiEstaDbVersion"] : String.Empty)
                //    .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                //    .Replace(oldValue: "$2", newValue: extensionVersion)
                //    .Replace(oldValue: "$3", newValue: NfiEstaSchema.ExtensionVersion.ToString());
                string messageText = messages.ContainsKey("msgInvalidNfiEstaDbVersion") ? messages["msgInvalidNfiEstaDbVersion"] : String.Empty;
                string replacedMessageText = messageText
                    .Replace("$1", NfiEstaSchema.ExtensionName)
                    .Replace("$2", extensionVersion)
                    .Replace("$3", NfiEstaSchema.ExtensionVersion.ToString());
                CtrEtl.lblNfiestaExtension.Text = replacedMessageText;
                MessageBox.Show(
                    text: replacedMessageText,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                TargetDatabase.Postgres?.CloseConnection();
                Disconnect();
                CtrEtl.pnlControls.Controls.Clear();
                return false;
            }

            if (!NfiEstaSchema.ExtensionVersion.Equals(obj: dbExtensionVersion))
            {
                CtrEtl.lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                CtrEtl.lblNfiestaExtension.Text =
                    (messages.ContainsKey(key: "msgInvalidNfiEstaDbVersion") ?
                     messages["msgInvalidNfiEstaDbVersion"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: extensionVersion)
                    .Replace(oldValue: "$3", newValue: NfiEstaSchema.ExtensionVersion.ToString());
                return true;
            }

            CtrEtl.lblNfiestaExtension.ForeColor = System.Drawing.SystemColors.ControlText;
            CtrEtl.lblNfiestaExtension.Text =
                (messages.ContainsKey(key: "msgValidNfiEstaDbVersion") ?
                 messages["msgValidNfiEstaDbVersion"] : String.Empty)
                .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                .Replace(oldValue: "$2", newValue: extensionVersion)
                .Replace(oldValue: "$3", newValue: NfiEstaSchema.Name);
            return true;
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události "Načtení dat do formuláře"</para>
        /// <para lang="en">Handling the "Loading data into form" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (FormUserPassword)</para>
        /// <para lang="en">Object that sends the event (FormUserPassword)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void FormUserPassword_Load(object sender, EventArgs e)
        {
            txtServer.Text = SelectedExportConnectionHost;
            txtPort.Text = SelectedExportConnectionPort.ToString();
            txtDatabase.Text = SelectedExportConnectionDbName;
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Zavření formuláře"</para>
        /// <para lang="en">Event handling form closing</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (FormUserPassword)</para>
        /// <para lang="en">Object that sends the event (FormUserPassword)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void FormUserPassword_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                if (String.IsNullOrEmpty(txtUser.Text.Trim()))
                {
                    Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);
                    MessageBox.Show(
                         text:
                            messages.ContainsKey(key: "msgUserIsEmpty") ?
                            messages["msgUserIsEmpty"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (String.IsNullOrEmpty(txtPassword.Text.Trim()))
                {
                    Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);
                    MessageBox.Show(
                        text:
                            messages.ContainsKey(key: "msgPasswordIsEmpty") ?
                            messages["msgPasswordIsEmpty"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    TargetDatabase = new PGWrapper(LanguageVersion, LanguageFile);

                    TargetDatabase.Postgres.ConnectionError += Db_OnConnectionError;
                    TargetDatabase.Postgres.ExceptionReceived += Db_OnExceptionReceived;

                    TargetDatabase.Postgres.SetConnection(
                        applicationName: "NfiEstaGUI: Data transfer module",
                        host: SelectedExportConnectionHost,
                        port: SelectedExportConnectionPort,
                        database: SelectedExportConnectionDbName,
                        userName: txtUser.Text.Trim(),
                        password: txtPassword.Text.Trim(),
                        commandTimeout: 0,
                        keepAlive: 180,
                        tcpKeepAlive: true,
                        tcpKeepAliveTime: 60,
                        tcpKeepAliveInterval: 5);

                    TargetDatabase.Postgres.StayConnected = true;

                    ConnectionInitialized = DialogResult == DialogResult.OK && TargetDatabase.Postgres.Initialized;

                    e.Cancel = !TargetDatabase.Postgres.Initialized;

                    CtrEtl.lblStatusExportConnection.Text =
                        TargetDatabase.ConnectionInfoForEtlModule();

                    if (TargetDatabase != null && TargetDatabase.Postgres.Initialized)
                    {
                        TargetDBConnectionInfo.LastConnectionInfo = TargetDatabase.Postgres.ConnectionInfo();
                    }

                    if (!TargetDatabase.Postgres.Initialized)
                    {
                        CtrEtl.lblStatusExportConnection.Visible = false;
                        CtrEtl.CtrConnection.btnNext.Enabled = false;
                        CtrEtl.CtrConnection.btnCheckMetadata.Enabled = false;
                        CtrEtl.CtrConnection.btnCheckAdSp.Enabled = false;
                        return;
                    }
                    else
                    {
                        CtrEtl.lblStatusExportConnection.Visible = true;

                        if (ControlOwner is ControlConnection controlConnection)
                        {
                            controlConnection.btnNext.Enabled = true;
                            controlConnection.btnCheckMetadata.Enabled = true;
                            controlConnection.btnCheckAdSp.Enabled = true;
                        }
                        else
                        {
                            CtrEtl.CtrConnection.btnNext.Enabled = true;
                            CtrEtl.CtrConnection.btnCheckMetadata.Enabled = true;
                            CtrEtl.CtrConnection.btnCheckAdSp.Enabled = true;
                        }

                        DisplayNfiEstaExtensionStatus();
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zrušit"</para>
        /// <para lang="en">Event handling the click on the "Cancel" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "OK"</para>
        /// <para lang="en">Event handling the click on the "OK" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        #endregion Event Handlers


        #region Database - Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události "Chyba při připojení k databázi"</para>
        /// <para lang="en">Handling the "Database connection error" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (PostgreSQLWrapper)</para>
        /// <para lang="en">Object that sends the event (PostgreSQLWrapper)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Db_OnConnectionError(object sender, ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionEventArgs e)
        {
            MessageBox.Show(e.ToString(), e.MessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Chyba v SQL příkazu"</para>
        /// <para lang="en">Handling the event "Error in SQL statement"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (PostgreSQLWrapper)</para>
        /// <para lang="en">Object that sends the event (PostgreSQLWrapper)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Db_OnExceptionReceived(object sender, ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionEventArgs e)
        {
            DbCommandError = true;

            DialogResult result = MessageBox.Show(
                        text: e.ToString(),
                        caption: e.MessageBoxCaption,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Error);

            if (result == DialogResult.OK)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                DialogResult dialog = MessageBox.Show(
                                        text:
                                            messages.ContainsKey(key: "msgDatabaseExceptionReceived") ?
                                            messages["msgDatabaseExceptionReceived"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.OK,
                                        icon: MessageBoxIcon.Information);

                if (dialog == DialogResult.OK)
                {
                    if (SourceTransaction != null)
                    {
                        SourceTransaction.Rollback();
                    }
                    if (TargetTransaction != null)
                    {
                        TargetTransaction.Rollback();
                    }

                    if (CtrEtl.pnlControls.InvokeRequired)
                    {
                        CtrEtl.pnlControls.Invoke((MethodInvoker)delegate
                        {
                            CtrEtl.pnlControls.Controls.Clear();
                        });
                    }
                    else
                    {
                        CtrEtl.pnlControls.Controls.Clear();
                    }

                    Form formCommit = Application.OpenForms["FormCommit"];
                    if (formCommit != null)
                    {
                        if (formCommit.InvokeRequired)
                        {
                            formCommit.Invoke((MethodInvoker)delegate
                            {
                                formCommit.Close();
                            });
                        }
                        else
                        {
                            formCommit.Close();
                        }
                    }

                    Form formCheckAdSp = Application.OpenForms["FormCheckAdSp"];
                    if (formCheckAdSp != null) { formCheckAdSp.Close(); }

                    Form formCheckMetadata = Application.OpenForms["FormCheckMetadata"];
                    if (formCheckMetadata != null) { formCheckMetadata.Close(); }

                    Form formEtledTargetVariables = Application.OpenForms["FormEtledTargetVariables"];
                    if (formEtledTargetVariables != null) { formEtledTargetVariables.Close(); }

                    Form formEtledTVCheckMetadata = Application.OpenForms["FormEtledTVCheckMetadata"];
                    if (formEtledTVCheckMetadata != null) { formEtledTVCheckMetadata.Close(); }

                    CtrEtl.Disconnect();
                    Disconnect();
                    CtrEtl.lblStatusExportConnection.Visible = false;
                }
            }
        }

        #endregion Database - Event Handlers
    }

}
