﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova
{
    namespace ModuleEtl
    {
        /// <summary>
        /// <para lang="cs">Statická třída pro uchovávání informací o posledním připojení k databázi.</para>
        /// <para lang="en">Static class for storing information about the last database connection.</para>
        /// </summary>
        public static class TargetDBConnectionInfo
        {
            /// <summary>
            /// <para lang="cs">Poslední informace o připojení k databázi.</para>
            /// <para lang="en">Last connection information to the database.</para>
            /// </summary>
            public static string LastConnectionInfo { get; set; }
        }
    }
}
