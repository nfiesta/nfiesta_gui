﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba subpopulace a jejích kategorií"</para>
    /// <para lang="en">Control "Select subpopulation and its categories"</para>
    /// </summary>
    public partial class ControlSubPopulation
           : UserControl, INfiEstaControl, IETLControl
    {
        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Téma"</para>
        /// <para lang="en">Control "Topic"</para>
        /// </summary>
        private ControlTopic controlTopic;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Kontrola atributových proměnných"</para>
        /// <para lang="en">Control "Checking attribute variables"</para>
        /// </summary>
        private ControlAttributeVariables controlAttributeVariables;

        /// <summary>
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table</para>
        /// </summary>
        private readonly int? _idEtl;

        /// <summary>
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů</para>
        /// <para lang="en">List of panel and refyearset combination ids</para>
        /// </summary>
        private readonly List<int?> _refYearSetToPanelMapping;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Atomické plošné domény</para>
        /// <para lang="en">Atomic area domains</para>
        /// </summary>
        private readonly bool? _atomicAreaDomains;

        /// <summary>
        /// <para lang="cs">Atomické subpopulace</para>
        /// <para lang="en">Atomic subpopulations</para>
        /// </summary>
        private readonly bool? _atomicSubPopulations;

        /// <summary>
        /// <para lang="cs">Etl id subpopulace</para>
        /// <para lang="en">Subpopulation etl id</para>
        /// </summary>
        private int? etlIdSubPopulation;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelCs;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelEn;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        /// <summary>
        /// <para lang="cs">Datová tabulka, která je výsledkem volání funkce FnEtlCheckSubPopulationsForEtl a zobrazuje subpopulace z cílové db pro subpopulaci zvolenou ve zdrojové db.</para>
        /// <para lang="en">A data table resulting from the call to FnEtlCheckSubPopulationsForEtl function, displaying subpopulations from the target db for the subpopulation selected in the source db.</para>
        /// </summary>
        private DataTable result;

        /// <summary>
        /// <para lang="cs">Datová tabulka, která je výsledkem volání funkce FnEtlGetSubPopulation a zobrazuje subpopulace ze zdrojové db.</para>
        /// <para lang="en">A data table resulting from the call to FnEtlGetSubPopulation function, displaying subpopulations from the source db.</para>
        /// </summary>
        private DataTable dataSourceForDgvSubPopulation;

        /// <summary>
        /// <para lang="cs">Datová tabulka, která je výsledkem volání funkce FnEtlGetSubPopulationCategory a zobrazuje kategorie subpopulací pro zvolenou subpopulaci ve zdrojové db.</para>
        /// <para lang="en">A data table resulting from the call to FnEtlGetSubPopulationCategory function, displaying subpopulation categories for the selected subpopulation in the source db.</para>
        /// </summary>
        private DataTable resultSPC;

        /// <summary>
        /// <para lang="cs">Boolean, který uvádí, zda byla vyvolána poznámka z db. Výchozí hodnota je false.</para>
        /// <para lang="en">Boolean indicating whether a notice from the db has been triggered. The default value is false.</para>
        /// </summary>
        private bool noticeReceived = false;

        /// <summary>
        /// <para lang="cs">Datová tabulka, která je výsledkem volání funkce FnEtlGetSubPopulationCategoriesForRoller a zobrazuje kategorie subpopulací pro zvolenou subpopulaci v cílové db.</para>
        /// <para lang="en">A data table resulting from the call to FnEtlGetSubPopulationCategoriesForRoller function, displaying subpopulation categories for the selected subpopulation in the target db.</para>
        /// </summary>
        private DataTable categoriesForRoller;

        /// <summary>
        /// <para lang="cs">Datagridview, které je použito pro kontrolu null stavu dgvSubPopulationEtlIdNull (tabulka subpopulací z cílové db).</para>
        /// <para lang="en">Datagridview used for checking the null state of dgvSubPopulationEtlIdNull (subpopulation table from the target db).</para>
        /// </summary>
        private DataGridView dataGridView = null;

        /// <summary>
        /// <para lang="cs">Hodnota etl_id z tabulky, kterou vrací funkce FnEtlGetSubPopulationCategoriesForRoller a která je vybrána pro zvolený comboBox.</para>
        /// <para lang="en">The etl_id value from the table returned by the FnEtlGetSubPopulationCategoriesForRoller function, selected for the chosen comboBox.</para>
        /// </summary>
        private int? selectedEtlIdForCombobox;

        /// <summary>
        /// <para lang="cs">Seznam etl_id z tabulky, kterou vrací funkce FnEtlGetSubPopulationCategoriesForRoller.</para>
        /// <para lang="en">A list of etl_id from the table returned by the FnEtlGetSubPopulationCategoriesForRoller function.</para>
        /// </summary>
        private List<int?> selectedEtlIdsForCombobox = new List<int?>();

        /// <summary>
        /// <para lang="cs">Datová tabulka, která je výsledkem volání funkce FnEtlCheckSubPopulationCategoriesForEtl a zobrazuje kategorie subpopulací pro zvolenou subpopulaci v cílové db.</para>
        /// <para lang="en">A data table resulting from the call to FnEtlCheckSubPopulationCategoriesForEtl function, displaying subpopulation categories for the selected subpopulation in the target db.</para>
        /// </summary>
        private DataTable categories;

        /// <summary>
        /// <para lang="cs">FlowLayoutPanel, na kterém leží ovladače pro zobrazení kategorií pro zvolenou subpopulaci v cílové db.</para>
        /// <para lang="en">FlowLayoutPanel hosting controls for displaying categories for the selected subpopulation in the target db.</para>
        /// </summary>
        private FlowLayoutPanel flpCategoryComboBoxes;

        /// <summary>
        /// <para lang="cs">Vybraná subpopulace ze zdrojové db. Může být neatomická (skládat se z více atomických subpopulací) a tvořit seznam.</para>
        /// <para lang="en">Selected subpopulation from the source db. It can be non-atomic (consist of multiple atomic subpopulations) and form a list.</para>
        /// </summary>
        private List<int?> subPopulation;

        /// <summary>
        /// <para lang="cs">Proměnná používaná pro indikaci, že dojde k inicializaci zdroje dat pro DataGridView 'dgvSubPopulationEtlIdNull'. Tato proměnná zabraňuje vyvolání událostí spojených se změnou výběru během procesu nastavování zdroje dat.</para>
        /// <para lang="en">A flag used to indicate that the data source for the DataGridView 'dgvSubPopulationEtlIdNull' is being initialized. This variable prevents triggering selection change events during the data source setup process.</para>
        /// </summary>
        private bool initializingDataSource;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="idEtl">
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="refYearSetToPanelMapping">
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů</para>
        /// <para lang="en">List of panel and refyearset combination ids</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="atomicAreaDomains">
        /// <para lang="cs">Atomické plošné domény</para>
        /// <para lang="en">Atomic area domains</para>
        /// </param>
        /// <param name="atomicSubPopulations">
        /// <para lang="cs">Atomické subpopulace</para>
        /// <para lang="en">Atomic subpopulations</para>
        /// </param>
        /// <param name="selectedIndicatorLabelCs">
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </param>
        /// <param name="selectedIndicatorLabelEn">
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public ControlSubPopulation(
            Control controlOwner,
            int? idEtl,
            List<int?> refYearSetToPanelMapping,
            int selectedExportConnectionId,
            bool? atomicAreaDomains,
            bool? atomicSubPopulations,
            string selectedIndicatorLabelCs,
            string selectedIndicatorLabelEn,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction
            )
        {
            InitializeComponent();

            _idEtl = idEtl;
            _refYearSetToPanelMapping = refYearSetToPanelMapping;
            _selectedExportConnectionId = selectedExportConnectionId;
            _atomicAreaDomains = atomicAreaDomains;
            _atomicSubPopulations = atomicSubPopulations;
            _selectedIndicatorLabelCs = selectedIndicatorLabelCs;
            _selectedIndicatorLabelEn = selectedIndicatorLabelEn;
            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;

            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlEtl ctrEtl)
                {
                    return ctrEtl;
                }

                else if (ControlOwner is ControlTargetVariable ctrTargetVariable)
                {
                    return ctrTargetVariable.CtrEtl;
                }

                else if (ControlOwner is ControlPanelRefyearset ctrPanelRefYearSet)
                {
                    return ctrPanelRefYearSet.CtrEtl;
                }

                else if (ControlOwner is ControlAreaDomain ctrAreaDomain)
                {
                    return ctrAreaDomain.CtrEtl;
                }

                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                 "Přenos subpopulací" },
                            { "btnNext",                        "Další" },
                            { "btnAuto",                        "Auto" },
                            { "btnMatch",                       "Spárovat" },
                            { "btnNoMatch",                     "Přenést" },
                            { "msgAutoConfirm",                 "Přenést nebo spárovat automaticky subpopulace a jejich kategorie ze zdrojové do cílové DB?" },
                            { "msgPairError",                   "Chyba: Nebyla nalezena žádná subpopulace ke spárování. Proces přenosu dat nelze dokončit, zavři, prosím, tento modul."},
                            { "msgPair",                        "Spárovat zvolenou subpopulaci a její kategorie ze zdrojové DB se zvolenou subpopulací a jejími kategoriemi v cílové DB?"},
                            { "msgTransfer",                    "Přenést zvolenou subpopulaci a její kategorie ze zdrojové DB do cílové DB?"},
                            { "msgDuplicity",                   "Stejná kategorie nesmí být zvolena vícekrát."},
                            { "msgInvalidValue",                "Vyskytuje se zde kategorie s neplatnou hodnotou."},
                            { "msgConfirm",                     "Potvrdit změnu a uložit do cílové databáze?"},
                            { "grpSubPopulation",               "Subpopulace ve zdrojové databázi:"},
                            { "grpSubPopulationEtlIdNull",      "Subpopulace v cílové databázi:"},
                            { "grpSPC",                         "Kategorie zvolené subpopulace ve zdrojové databázi:"},
                            { "grpSPCTargetDB",                 "Kategorie zvolené subpopulace v cílové databázi:"},
                            { "targetDBCategoriesHeader",       "Kategorie subpopulace"},
                            { "dgvSubPopulationLabel",          "Subpopulace - zkratka"},
                            { "dgvSubPopulationDescription",    "Subpopulace - popis"},
                            { "dgvSubPopulationAuto",           "Automaticky přenést?"},
                            { "dgvSPCLabel",                    "Kategorie subpopulace - zkratka"},
                            { "dgvSPCDescription",              "Kategorie subpopulace - popis"},
                            { "dgvSubPopulationEtlIdNullAtomic","Atomický typ"},
                            { "yes",                            "ano"},
                            { "no",                             "ne"},
                            { "EnglishVersionUnavailable",      "Anglická verze není dostupná"},
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlSubPopulation") ?
                    languageFile.NationalVersion.Data["ControlSubPopulation"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                 "Transfer of subpopulations" },
                            { "btnNext",                        "Next" },
                            { "btnAuto",                        "Auto" },
                            { "btnMatch",                       "Pair" },
                            { "btnNoMatch",                     "Transfer" },
                            { "msgAutoConfirm",                 "Transfer or pair automatically subpopulations and their categories from source to target DB?" },
                            { "msgPair",                        "Pair the selected subpopulation and its categories from the source DB with the selected subpopulation and its categories in the target DB?"},
                            { "msgTransfer",                    "Transfer the selected subpopulation and its categories from the source DB to the target DB?"},
                            { "msgDuplicity",                   "The same category may not be selected more than once."},
                            { "msgInvalidValue",                "There is a category with an invalid value."},
                            { "msgConfirm",                     "Confirm the change and save to the target database?"},
                            { "grpSubPopulation",               "Subpopulations in the source database:"},
                            { "grpSubPopulationEtlIdNull",      "Subpopulations in the target database:"},
                            { "grpSPC",                         "Categories of the selected subpopulation in the source database:"},
                            { "grpSPCTargetDB",                 "Categories of the selected subpopulation in the target database:"},
                            { "targetDBCategoriesHeader",       "Subpopulation category"},
                            { "dgvSubPopulationLabel",          "Subpopulation - label"},
                            { "dgvSubPopulationDescription",    "Subpopulation - description"},
                            { "dgvSubPopulationAuto",           "Transfer automatically?"},
                            { "dgvSPCLabel",                    "Subpopulation category - label"},
                            { "dgvSPCDescription",              "Subpopulation category - description"},
                            { "dgvSubPopulationEtlIdNullAtomic","Atomic type"},
                            { "yes",                            "yes"},
                            { "no",                             "no"},
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlSubPopulation") ?
                    languageFile.InternationalVersion.Data["ControlSubPopulation"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            nationalLang = new NationalLang();

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            btnNext.Text =
                labels.ContainsKey(key: "btnNext") ?
                labels["btnNext"] : String.Empty;

            btnAuto.Text =
                labels.ContainsKey(key: "btnAuto") ?
                labels["btnAuto"] : String.Empty;

            btnMatch.Text =
                labels.ContainsKey(key: "btnMatch") ?
                labels["btnMatch"] : String.Empty;

            btnNoMatch.Text =
                labels.ContainsKey(key: "btnNoMatch") ?
                labels["btnNoMatch"] : String.Empty;

            grpSubPopulation.Text =
                labels.ContainsKey(key: "grpSubPopulation") ?
                labels["grpSubPopulation"] : String.Empty;

            grpSubPopulationEtlIdNull.Text =
                labels.ContainsKey(key: "grpSubPopulationEtlIdNull") ?
                labels["grpSubPopulationEtlIdNull"] : String.Empty;

            grpSPC.Text =
                labels.ContainsKey(key: "grpSPC") ?
                labels["grpSPC"] : String.Empty;

            LoadContent();

            controlTopic?.InitializeLabels();
            controlAttributeVariables?.InitializeLabels();

            if (dataGridView != null)
            {
                LoadContentOfDgvSubPopulationEtlIdNull();
            }

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            if (result != null)
            {
                result.Clear();
                result.Columns.Clear();
                dgvSubPopulationEtlIdNull.DataSource = null;
                dgvSubPopulationEtlIdNull.Columns.Clear();
            }

            if (categories != null)
            {
                splSubPopulationCategory.Panel2.Controls.Clear();
                selectedEtlIdsForCombobox.Clear();
                btnMatch.Enabled = false;
                btnNoMatch.Enabled = false;
            }

            string subPopulations =
                TDFunctions.FnEtlGetSubPopulationJson.Execute(
                    database: Database,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    idTEtlTargetVariable: _idEtl,
                    etl: false,
                    transaction: SourceTransaction);

            List<int?> etlIds =
                        TDFunctions.FnEtlGetEtlIdsOfSubPopulation.Execute(
                            database: Database,
                            exportConnectionId: _selectedExportConnectionId,
                            transaction: SourceTransaction);

            Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

            if (!string.IsNullOrEmpty(subPopulations))
            {
                string autoInformation = NfiEstaFunctions.FnEtlCheckSubPopulationsForEtlJson.Execute(
                database: FormUserPassword.TargetDatabase,
                subPopulations: subPopulations,
                etlId: etlIds,
                transaction: TargetTransaction);

                dataSourceForDgvSubPopulation =
                    TDFunctions.FnEtlGetSubPopulation.ExecuteQuery(
                        database: Database,
                        refYearSetToPanelMapping: _refYearSetToPanelMapping,
                        idTEtlTargetVariable: _idEtl,
                        autoInformation: autoInformation,
                        etl: false,
                        transaction: SourceTransaction);

                // Up to FormatDataGridView(dgvSubPopulation), code is added (except for dgvSubPopulation.DataSource = result;) to keep the checkbox checked when switching between languages
                // except of the code with comments - this code is added to hold the selected row
                var clm = dataSourceForDgvSubPopulation.Columns.Add("Select", typeof(bool));
                foreach (DataRow row in dataSourceForDgvSubPopulation.Rows)
                    row["Select"] = false;
                clm.AllowDBNull = false;

                //int[] selected = null;
                //if (dgvSubPopulation.DataSource != null)
                //{
                //    selected = dgvSubPopulation.Rows.Cast<DataGridViewRow>()
                //        .Where(x => Convert.ToBoolean(((DataGridViewCheckBoxCell)x.Cells["Select"]).Value))
                //        .Select(x => (int)x.Cells["id"].Value).ToArray();
                //}

                // Save the index of the currently selected row
                int currentIndex = -1;
                if (dgvSubPopulation.CurrentRow != null)
                    currentIndex = dgvSubPopulation.CurrentRow.Index;

                dgvSubPopulation.DataSource = dataSourceForDgvSubPopulation;

                //if (selected != null)
                //{
                //    foreach (DataGridViewRow row in dgvSubPopulation.Rows)
                //    {
                //        (((DataRowView)row.DataBoundItem)).Row["Select"] = selected.Contains((int)row.Cells["id"].Value);
                //    }
                //}

                // After updating the data set the selected row back
                if (currentIndex != -1 && currentIndex < dgvSubPopulation.Rows.Count)
                {
                    dgvSubPopulation.Rows[currentIndex].Selected = true;

                    // Find the first visible cell to set as the current cell
                    foreach (DataGridViewCell cell in dgvSubPopulation.Rows[currentIndex].Cells)
                    {
                        if (cell.Visible)
                        {
                            dgvSubPopulation.CurrentCell = cell;
                            break;
                        }
                    }
                }

                dgvSubPopulation.Invalidate();

                FormatDataGridView(dgvSubPopulation);
                AutoSizeColumn(dgvSubPopulation, "label");
                AutoSizeColumn(dgvSubPopulation, "label_en");
                AutoSizeColumn(dgvSubPopulation, "auto");

                dgvSubPopulation.Columns["id"].Visible = false;
                dgvSubPopulation.Columns["export_connection"].Visible = false;
                dgvSubPopulation.Columns["sub_population"].Visible = false;
                dgvSubPopulation.Columns["etl_id"].Visible = false;
                dgvSubPopulation.Columns["id_t_etl_sp"].Visible = false;
                dgvSubPopulation.Columns["atomic"].Visible = false;
                dgvSubPopulation.Columns["auto_transfer"].Visible = false;
                dgvSubPopulation.Columns["auto_pair"].Visible = false;

                switch (LanguageVersion)
                {
                    case LanguageVersion.National:
                        dgvSubPopulation.Columns["label"].Visible = true;
                        dgvSubPopulation.Columns["label_en"].Visible = false;
                        dgvSubPopulation.Columns["label"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationLabel") ? labels["dgvSubPopulationLabel"] : String.Empty;
                        dgvSubPopulation.Columns["description"].Visible = true;
                        dgvSubPopulation.Columns["description_en"].Visible = false;
                        dgvSubPopulation.Columns["description"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationDescription") ? labels["dgvSubPopulationDescription"] : String.Empty;
                        dgvSubPopulation.Columns["auto"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationAuto") ? labels["dgvSubPopulationAuto"] : String.Empty;
                        break;

                    case LanguageVersion.International:
                        dgvSubPopulation.Columns["label"].Visible = false;
                        dgvSubPopulation.Columns["label_en"].Visible = true;
                        dgvSubPopulation.Columns["label_en"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationLabel") ? labels["dgvSubPopulationLabel"] : String.Empty;
                        dgvSubPopulation.Columns["description"].Visible = false;
                        dgvSubPopulation.Columns["description_en"].Visible = true;
                        dgvSubPopulation.Columns["description_en"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationDescription") ? labels["dgvSubPopulationDescription"] : String.Empty;
                        dgvSubPopulation.Columns["auto"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationAuto") ? labels["dgvSubPopulationAuto"] : String.Empty;
                        break;
                }

                //added because of this code: var clm = result.Columns.Add("Select", typeof(bool));
                dgvSubPopulation.Columns["Select"].Width = 20;
                dgvSubPopulation.Columns["Select"].HeaderText = "";

                //insert the checkedboxColumn in the dataGridView
                bool selectColumnExists = false;
                foreach (DataGridViewColumn column in dgvSubPopulation.Columns)
                {
                    if (column.Name == "Select" && column is DataGridViewCheckBoxColumn) //because it appeared more times
                    {
                        selectColumnExists = true;
                        break;
                    }
                }

                if (!selectColumnExists)
                {
                    DataGridViewCheckBoxColumn checkboxColumn = new DataGridViewCheckBoxColumn()
                    {
                        Name = "Select",
                        HeaderText = "",
                        Width = 20
                    };
                    dgvSubPopulation.Columns.Insert(0, checkboxColumn); //insert the column at the first position;
                }
                else
                {
                    dgvSubPopulation.Columns["Select"].DisplayIndex = 0; //move the existing "Select" column to the first position
                }

                if (dgvSubPopulation.Rows.Count == 0)
                {
                    btnAuto.Enabled = false;
                    btnMatch.Enabled = false;
                    btnNoMatch.Enabled = false;
                    btnNext.Enabled = true;
                    Next();
                }

                bool autoColumnHasTrue = dgvSubPopulation.Rows
                                            .Cast<DataGridViewRow>()
                                            .Any(row => Convert.ToBoolean(row.Cells["auto"].Value));

                if (autoColumnHasTrue)
                {
                    dgvSubPopulation.Columns["Select"].Visible = false;
                    btnMatch.Enabled = false;
                    btnNoMatch.Enabled = false;
                    btnAuto.Enabled = true;
                }

                if (!autoColumnHasTrue)
                {
                    dgvSubPopulation.Columns["Select"].Visible = true;
                    btnAuto.Enabled = false;
                }
            }
            else
            {
                switch (LanguageVersion)
                {
                    case LanguageVersion.National:
                        dgvSubPopulation.Columns["label"].Visible = true;
                        dgvSubPopulation.Columns["label_en"].Visible = false;
                        dgvSubPopulation.Columns["label"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationLabel") ? labels["dgvSubPopulationLabel"] : String.Empty;
                        dgvSubPopulation.Columns["description"].Visible = true;
                        dgvSubPopulation.Columns["description_en"].Visible = false;
                        dgvSubPopulation.Columns["description"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationDescription") ? labels["dgvSubPopulationDescription"] : String.Empty;
                        dgvSubPopulation.Columns["auto"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationAuto") ? labels["dgvSubPopulationAuto"] : String.Empty;
                        dgvSPC.Columns["label"].Visible = true;
                        dgvSPC.Columns["label_en"].Visible = false;
                        dgvSPC.Columns["label"].HeaderText = labels.ContainsKey(key: "dgvSPCLabel") ? labels["dgvSPCLabel"] : String.Empty;
                        dgvSPC.Columns["description"].Visible = true;
                        dgvSPC.Columns["description_en"].Visible = false;
                        dgvSPC.Columns["description"].HeaderText = labels.ContainsKey(key: "dgvSPCDescription") ? labels["dgvSPCDescription"] : String.Empty;
                        break;

                    case LanguageVersion.International:
                        dgvSubPopulation.Columns["label"].Visible = false;
                        dgvSubPopulation.Columns["label_en"].Visible = true;
                        dgvSubPopulation.Columns["label_en"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationLabel") ? labels["dgvSubPopulationLabel"] : String.Empty;
                        dgvSubPopulation.Columns["description"].Visible = false;
                        dgvSubPopulation.Columns["description_en"].Visible = true;
                        dgvSubPopulation.Columns["description_en"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationDescription") ? labels["dgvSubPopulationDescription"] : String.Empty;
                        dgvSubPopulation.Columns["auto"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationAuto") ? labels["dgvSubPopulationAuto"] : String.Empty;
                        dgvSPC.Columns["label"].Visible = false;
                        dgvSPC.Columns["label_en"].Visible = true;
                        dgvSPC.Columns["label_en"].HeaderText = labels.ContainsKey(key: "dgvSPCLabel") ? labels["dgvSPCLabel"] : String.Empty;
                        dgvSPC.Columns["description"].Visible = false;
                        dgvSPC.Columns["description_en"].Visible = true;
                        dgvSPC.Columns["description_en"].HeaderText = labels.ContainsKey(key: "dgvSPCDescription") ? labels["dgvSPCDescription"] : String.Empty;
                        break;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví záhlaví, viditelnost, formáty podle typu dat a zarovnání ve sloupcích daného DataGridView</para>
        /// <para lang="en">Sets header, visibility, formats by data type and alignment in columns of given DataGridView</para>
        /// </summary>
        /// <param name="dataGridView">
        /// <para lang="cs">Objekt DataGridView, který se má formátovat</para>
        /// <para lang="en">The DataGridView object to be formatted</para>
        /// </param>
        public void FormatDataGridView(DataGridView dataGridView)
        {
            // General:
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeRows = false;

            dataGridView.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle()
            {
                BackColor = Color.LightCyan
            };

            dataGridView.AutoGenerateColumns = true;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            dataGridView.BackgroundColor = SystemColors.Window;
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.None;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;

            dataGridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.DarkBlue,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = Color.White,
                SelectionBackColor = Color.DarkBlue,
                SelectionForeColor = Color.White,
                WrapMode = DataGridViewTriState.False
            };

            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            dataGridView.DefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.White,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = SystemColors.ControlText,
                Format = "N3",
                NullValue = "NULL",
                SelectionBackColor = Color.Bisque,
                SelectionForeColor = SystemColors.ControlText,
                WrapMode = DataGridViewTriState.False
            };

            dataGridView.EnableHeadersVisualStyles = false;
            dataGridView.MultiSelect = false;
            dataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView.RowHeadersVisible = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        /// <summary>
        /// <para lang="cs">Automaticky upravuje šířku sloupce v DataGridView na základě obsahu buněk a záhlaví sloupce.</para>
        /// <para lang="en">Automatically adjusts the width of a column in DataGridView based on the content of the cells and the column header.</para>
        /// </summary>
        /// <param name="dataGridView"><para lang="cs">DataGridView, ve kterém se má šířka sloupce upravit.</para>
        /// <para lang="en">The DataGridView in which the column width is to be adjusted.</para></param>
        /// <param name="columnName"><para lang="cs">Název sloupce, pro který se má šířka upravit.</para>
        /// <para lang="en">The name of the column for which the width is to be adjusted.</para></param>
        public void AutoSizeColumn(DataGridView dataGridView, string columnName)
        {
            int maxWidth = 0;

            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                if (row.Cells[columnName].Value != null)
                {
                    string text = row.Cells[columnName].Value.ToString();
                    Size textSize = TextRenderer.MeasureText(text, dataGridView.Font);
                    maxWidth = Math.Max(maxWidth, textSize.Width);
                }
            }

            string headerText = dataGridView.Columns[columnName].HeaderText;
            Size headerSize = TextRenderer.MeasureText(headerText, dataGridView.ColumnHeadersDefaultCellStyle.Font);
            maxWidth = Math.Max(maxWidth, headerSize.Width);

            dataGridView.Columns[columnName].Width = maxWidth + 10;
        }

        /// <summary>
        /// <para lang="cs">Načte obsah dgvSubPopulationEtlIdNull</para>
        /// <para lang="en">Loads the content of the dgvSubPopulationEtlIdNull</para>
        /// </summary>
        private void LoadContentOfDgvSubPopulationEtlIdNull()
        {
            // Check if the columns exist before modifying their properties
            if (dgvSubPopulationEtlIdNull.Columns.Count > 0)
            {
                Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                // Set the visibility of columns and headers of dgvSubPopulationEtlIdNull
                switch (LanguageVersion)
                {
                    case LanguageVersion.National:
                        if (dgvSubPopulationEtlIdNull.Columns.Contains("label") &&
                            dgvSubPopulationEtlIdNull.Columns.Contains("label_en") &&
                            dgvSubPopulationEtlIdNull.Columns.Contains("description") &&
                            dgvSubPopulationEtlIdNull.Columns.Contains("description_en"))
                        {
                            dgvSubPopulationEtlIdNull.Columns["label"].Visible = true;
                            dgvSubPopulationEtlIdNull.Columns["label_en"].Visible = false;
                            dgvSubPopulationEtlIdNull.Columns["description"].Visible = true;
                            dgvSubPopulationEtlIdNull.Columns["description_en"].Visible = false;
                            dgvSubPopulationEtlIdNull.Columns["label"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationLabel") ? labels["dgvSubPopulationLabel"] : String.Empty;
                            dgvSubPopulationEtlIdNull.Columns["description"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationDescription") ? labels["dgvSubPopulationDescription"] : String.Empty;
                            dgvSubPopulationEtlIdNull.Columns["atomic"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationEtlIdNullAtomic") ? labels["dgvSubPopulationEtlIdNullAtomic"] : String.Empty;
                        }
                        break;

                    case LanguageVersion.International:
                        if (dgvSubPopulationEtlIdNull.Columns.Contains("label") &&
                            dgvSubPopulationEtlIdNull.Columns.Contains("label_en") &&
                            dgvSubPopulationEtlIdNull.Columns.Contains("description") &&
                            dgvSubPopulationEtlIdNull.Columns.Contains("description_en"))
                        {
                            dgvSubPopulationEtlIdNull.Columns["label"].Visible = false;
                            dgvSubPopulationEtlIdNull.Columns["label_en"].Visible = true;
                            dgvSubPopulationEtlIdNull.Columns["description"].Visible = false;
                            dgvSubPopulationEtlIdNull.Columns["description_en"].Visible = true;
                            dgvSubPopulationEtlIdNull.Columns["label_en"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationLabel") ? labels["dgvSubPopulationLabel"] : String.Empty;
                            dgvSubPopulationEtlIdNull.Columns["description_en"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationDescription") ? labels["dgvSubPopulationDescription"] : String.Empty;
                            dgvSubPopulationEtlIdNull.Columns["atomic"].HeaderText = labels.ContainsKey(key: "dgvSubPopulationEtlIdNullAtomic") ? labels["dgvSubPopulationEtlIdNullAtomic"] : String.Empty;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Načte obsah dgvSPC</para>
        /// <para lang="en">Loads the content of the dgvSPC</para>
        /// </summary>
        private void LoadContentOfDgvSPC()
        {
            DataGridViewRow selectedRow = dgvSubPopulation.SelectedRows[0];

            string subPopulationString = selectedRow.Cells["sub_population"].Value.ToString();
            List<int?> subPopulation = subPopulationString.Split(';')
                .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                .ToList();

            resultSPC = TDFunctions.FnEtlGetSubPopulationCategory.ExecuteQuery(
                database: Database,
                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                idTEtlTargetVariable: _idEtl,
                subPopulation: subPopulation,
                transaction: SourceTransaction);

            dgvSPC.DataSource = resultSPC;

            FormatDataGridView(dgvSPC);
            AutoSizeColumn(dgvSPC, "label");
            AutoSizeColumn(dgvSPC, "label_en");
            AutoSizeColumn(dgvSPC, "id");

            dgvSPC.Columns["id"].HeaderText = "";
            dgvSPC.Columns["id"].DefaultCellStyle.Format = "N0";
            dgvSPC.Columns["sub_population"].Visible = false;
            dgvSPC.Columns["label_en_type"].Visible = false;
            dgvSPC.Columns["sub_population_category"].Visible = false;
            dgvSPC.Columns["t_etl_sub_population__id"].Visible = false;
            dgvSPC.Columns["t_etl_sub_population__etl_id"].Visible = false;

            Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    dgvSPC.Columns["label"].Visible = true;
                    dgvSPC.Columns["label_en"].Visible = false;
                    dgvSPC.Columns["label"].HeaderText = labels.ContainsKey(key: "dgvSPCLabel") ? labels["dgvSPCLabel"] : String.Empty;
                    dgvSPC.Columns["description"].Visible = true;
                    dgvSPC.Columns["description_en"].Visible = false;
                    dgvSPC.Columns["description"].HeaderText = labels.ContainsKey(key: "dgvSPCDescription") ? labels["dgvSPCDescription"] : String.Empty;
                    break;

                case LanguageVersion.International:
                    dgvSPC.Columns["label"].Visible = false;
                    dgvSPC.Columns["label_en"].Visible = true;
                    dgvSPC.Columns["label_en"].HeaderText = labels.ContainsKey(key: "dgvSPCLabel") ? labels["dgvSPCLabel"] : String.Empty;
                    dgvSPC.Columns["description"].Visible = false;
                    dgvSPC.Columns["description_en"].Visible = true;
                    dgvSPC.Columns["description_en"].HeaderText = labels.ContainsKey(key: "dgvSPCDescription") ? labels["dgvSPCDescription"] : String.Empty;
                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ControlAttributeVariables nebo ControlTopic</para>
        /// <para lang="en">ControlAttributeVariables or ControlTopic is shown</para>
        /// </summary>
        private void ShowControlAttributeVariablesOrTopic()
        {
            string variablesSource =
                TDFunctions.FnEtlGetVariables.Execute(
                    database: Database,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    idTEtlTargetVariable: _idEtl,
                    transaction: SourceTransaction);

            string variablesTarget = NfiEstaFunctions.FnEtlCheckVariables.ExecuteScalar(
                FormUserPassword.TargetDatabase,
                variablesSource,
                TargetTransaction);

            if (string.IsNullOrEmpty(variablesTarget))
            {
                ShowControlTopic();
            }
            else
            {
                controlAttributeVariables =
                    new ControlAttributeVariables(
                        controlOwner: this,
                        idEtl: _idEtl,
                        refYearSetToPanelMapping: _refYearSetToPanelMapping,
                        selectedExportConnectionId: _selectedExportConnectionId,
                        selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                        selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                        sourceTransaction: SourceTransaction,
                        targetTransaction: TargetTransaction)
                    {
                        Dock = DockStyle.Fill
                    };

                controlAttributeVariables.LoadContent();
                controlAttributeVariables.InitializeLabels();

                CtrEtl?.ShowControl(control: controlAttributeVariables);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ControlTopic</para>
        /// <para lang="en">ControlTopic is shown</para>
        /// </summary>
        private void ShowControlTopic()
        {
            controlTopic = new ControlTopic(
                controlOwner: this,
                idEtl: _idEtl,
                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                selectedExportConnectionId: _selectedExportConnectionId,
                selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                sourceTransaction: SourceTransaction,
                targetTransaction: TargetTransaction)
            {
                Dock = DockStyle.Fill
            };

            controlTopic.LoadContent();
            controlTopic.InitializeLabels();

            CtrEtl?.ShowControl(control: controlTopic);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ControlTopic pokud je vlastníkem ControlEtl</para>
        /// <para lang="en">ControlTopic is shown if owner is ControlEtl</para>
        /// </summary>
        private void ShowControlTopicIfOwnerIsEtL()
        {
            controlTopic = new ControlTopic(
                controlOwner: this,
                idEtl: _idEtl,
                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                selectedExportConnectionId: _selectedExportConnectionId,
                selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                sourceTransaction: SourceTransaction,
                targetTransaction: TargetTransaction)
            {
                Dock = DockStyle.Fill
            };

            controlTopic.LoadContent();
            controlTopic.InitializeLabels();

            CtrEtl?.ShowControl(control: controlTopic);
        }

        /// <summary>
        /// <para lang="cs">Potvrdí změny a (nebo) ukončí aplikaci</para>
        /// <para lang="en">Confirms changes and (or) exits the application</para>
        /// </summary>
        private void ConfirmAndExitApplication()
        {
            Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

            DialogResult dialogResult = MessageBox.Show(
                                        text: messages.ContainsKey(key: "msgConfirm") ?
                                                messages["msgConfirm"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.YesNo,
                                        icon: MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                //terminate the ETL process and go to the initial window
                SourceTransaction.Commit();
                TargetTransaction.Commit();

                Database.Postgres?.CloseConnection();
                FormUserPassword.Disconnect();

                Control parentControlEtl = this.Parent;
                parentControlEtl.Controls.Remove(this);
            }
            else
            {
                //terminate the ETL process and go to the initial window
                SourceTransaction.Rollback();
                TargetTransaction.Rollback();

                Database.Postgres?.CloseConnection();
                FormUserPassword.Disconnect();

                Control parentControlEtl = this.Parent;
                parentControlEtl.Controls.Remove(this);
            }
        }

        /// <summary>
        /// <para lang="cs">Získává seřazené etl id z ovládacích prvků ve formuláři. Prochází všechny ovládací prvky typu Panel v 'flpCategoryComboBoxes' a z každého panelu extrahuje etl id z TextBoxů a ComboBoxů. Id jsou přidány do seznamu ve stejném pořadí, v jakém byly nalezeny.</para>
        /// <para lang="en">Retrieves ordered etl ids from controls in the form. Iterates through all Panel type controls in 'flpCategoryComboBoxes' and extracts etl ids from TextBoxes and ComboBoxes within each panel. Ids are added to a list in the same order they were found.</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Seznam etl id</para>
        /// <para lang="en">A list of etl ids</para>
        /// </returns>
        private List<int?> GetOrderedEtlIdsFromControls()
        {
            List<int?> orderedEtlIds = new List<int?>();

            foreach (Control panel in flpCategoryComboBoxes.Controls)
            {
                if (panel is Panel)
                {
                    foreach (Control control in panel.Controls)
                    {
                        if (control is TextBox)
                        {

                            int? etlId = control.Tag as int?;
                            orderedEtlIds.Add(etlId);
                        }
                        else if (control is ComboBox)
                        {
                            ComboBox comboBox = control as ComboBox;
                            int? selectedEtlId = Convert.ToInt32(comboBox.SelectedValue);
                            orderedEtlIds.Add(selectedEtlId);
                        }
                    }
                }
            }

            return orderedEtlIds;
        }

        /// <summary>
        /// <para lang="cs">Zkontroluje, zda jsou všechny výběry v ComboBoxech platné. Metoda projde všechny ComboBoxy na FlowLayoutPanel 'flpCategoryComboBoxes' a ověří, že vybraná hodnota není nulová a liší se od nuly.</para>
        /// <para lang="en">Checks whether all selections in ComboBoxes are valid. The method iterates through all ComboBoxes on the FlowLayoutPanel 'flpCategoryComboBoxes' and verifies that the selected value is not null and not equal to zero.</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud jsou všechny výběry v ComboBoxech platné; jinak vrací false.</para>
        /// <para lang="en">Returns true if all selections in ComboBoxes are valid; otherwise, returns false.</para>
        /// </returns>
        private bool AreAllComboBoxSelectionsValid()
        {
            foreach (Control control in flpCategoryComboBoxes.Controls)
            {
                // To check for nested ComboBoxes (if the ComboBoxes are placed inside other containers such as Panel)
                if (control.HasChildren)
                {
                    foreach (Control childControl in control.Controls)
                    {
                        if (childControl is ComboBox childCmb)
                        {
                            if (childCmb.SelectedValue == null || string.IsNullOrEmpty(childCmb.SelectedValue.ToString()) || Convert.ToInt32(childCmb.SelectedValue) == 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// <para lang="cs">Ověřuje, zda je hodnota sloupce 'auto' pro všechny řádky v DataGridView 'dgvSubPopulation' nastavena na false. Metoda projde všechny řádky a zkontroluje hodnotu v sloupci 'auto'.</para>
        /// <para lang="en">Verifies whether the 'auto' column value for all rows in the DataGridView 'dgvSubPopulation' is set to false. The method iterates through all rows and checks the value in the 'auto' column.</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud je hodnota sloupce 'auto' pro všechny řádky nastavena na false; jinak vrací false.</para>
        /// <para lang="en">Returns true if the 'auto' column value for all rows is set to false; otherwise, returns false.</para>
        /// </returns>
        private bool AllRowsAutoColumnFalse()
        {
            foreach (DataGridViewRow row in dgvSubPopulation.Rows)
            {
                if (row.Cells["auto"].Value != null && Convert.ToBoolean(row.Cells["auto"].Value))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para lang="cs">Aktualizuje stav tlačítka 'btnNoMatch' na základě toho, zda je hodnota sloupce 'auto' pro všechny řádky v DataGridView 'dgvSubPopulation' nastavena na false. Metoda nejprve zjistí, zda jsou všechny hodnoty nastaveny na false, a poté podle toho nastaví povolení tlačítka.</para>
        /// <para lang="en">Updates the state of the 'btnNoMatch' button based on whether the 'auto' column value for all rows in the DataGridView 'dgvSubPopulation' is set to false. The method first determines if all values are set to false, and then enables or disables the button accordingly.</para>
        /// </summary>
        private void UpdateNoMatchButton()
        {
            bool allAutoFalse = AllRowsAutoColumnFalse();

            btnNoMatch.Enabled = allAutoFalse;

            if (btnMatch.Enabled == true) { btnNoMatch.Enabled = false; }
        }

        /// <summary>
        /// <para lang="cs">Ověřuje, zda jsou na FlowLayoutPanel 'flpCategoryComboBoxes' zobrazeny pouze TextBoxy, tím, že zkontroluje, zda se mezi ovládacími prvky nenachází žádný ComboBox.</para>
        /// <para lang="en">Verifies whether only TextBoxes are displayed on the FlowLayoutPanel 'flpCategoryComboBoxes' by checking if there are no ComboBox controls among the controls.</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud jsou na FlowLayoutPanel 'flpCategoryComboBoxes' zobrazeny pouze TextBoxy; jinak vrací false.</para>
        /// <para lang="en">Returns true if only TextBoxes are displayed on the FlowLayoutPanel 'flpCategoryComboBoxes'; otherwise, returns false.</para>
        /// </returns>
        private bool CheckIfOnlyTextBoxesDisplayed()
        {
            return !flpCategoryComboBoxes.Controls.OfType<Control>().Any(control => control is ComboBox);
        }

        /// <summary>
        /// <para lang="cs">Pokračuje na další krok v procesu přenosu dat.</para>
        /// <para lang="en">Continues to the next step in the data transfer process.</para>
        /// </summary>
        private void Next()
        {
            Cursor = Cursors.WaitCursor;

            // section 10 Area domain category nad subpopulation category mapping of schema

            string adcMapping =
                TDFunctions.FnEtlExportAreaDomainCategoryMapping.Execute(
                    database: Database,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    idTEtlTargetVariable: _idEtl,
                    transaction: SourceTransaction);

            if (!string.IsNullOrEmpty(adcMapping))
            {
                // adcMapping is not empty
                NfiEstaFunctions.FnEtlImportAreaDomainCategoryMapping.Execute(
                    FormUserPassword.TargetDatabase,
                    adcMapping,
                    TargetTransaction);

                string spcMapping =
                    TDFunctions.FnEtlExportSubPopulationCategoryMapping.Execute(
                        database: Database,
                        refYearSetToPanelMapping: _refYearSetToPanelMapping,
                        idTEtlTargetVariable: _idEtl,
                        transaction: SourceTransaction);

                //spcMapping is not empty
                if (!string.IsNullOrEmpty(spcMapping))
                {
                    NfiEstaFunctions.FnEtlImportSubPopulationCategoryMapping.Execute(
                        FormUserPassword.TargetDatabase,
                        spcMapping,
                        TargetTransaction);

                    // if (_atomicAreaDomains == false || _atomicSubPopulations == false) is section 11 Node D of schema
                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        ShowControlAttributeVariablesOrTopic();
                    }
                }
                else
                {
                    //spcMapping is null or empty

                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        ShowControlAttributeVariablesOrTopic();
                    }
                }
            }
            else
            {
                // adcMapping is null or empty

                string spcMapping =
                    TDFunctions.FnEtlExportSubPopulationCategoryMapping.Execute(
                        database: Database,
                        refYearSetToPanelMapping: _refYearSetToPanelMapping,
                        idTEtlTargetVariable: _idEtl,
                        transaction: SourceTransaction);

                //spcMapping is not empty
                if (!string.IsNullOrEmpty(spcMapping))
                {
                    NfiEstaFunctions.FnEtlImportSubPopulationCategoryMapping.Execute(
                        FormUserPassword.TargetDatabase,
                        spcMapping,
                        TargetTransaction);

                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        ShowControlAttributeVariablesOrTopic();
                    }
                }
                else
                {
                    //spcMapping is null or empty

                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        ShowControlAttributeVariablesOrTopic();
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        #endregion Methods


        #region Events

        #region NextClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">The delegate of the function handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void NextClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event NextClickHandler NextClick;

        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Invoking a click event on the "Next" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaiseNextClick(object sender, EventArgs e)
        {
            NextClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            // section 10 Area domain category nad subpopulation category mapping of schema

            string adcMapping =
                TDFunctions.FnEtlExportAreaDomainCategoryMapping.Execute(
                    database: Database,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    idTEtlTargetVariable: _idEtl,
                    transaction: SourceTransaction);

            if (!string.IsNullOrEmpty(adcMapping))
            {
                // adcMapping is not empty
                NfiEstaFunctions.FnEtlImportAreaDomainCategoryMapping.Execute(
                    FormUserPassword.TargetDatabase,
                    adcMapping,
                    TargetTransaction);

                string spcMapping =
                    TDFunctions.FnEtlExportSubPopulationCategoryMapping.Execute(
                        database: Database,
                        refYearSetToPanelMapping: _refYearSetToPanelMapping,
                        idTEtlTargetVariable: _idEtl,
                        transaction: SourceTransaction);

                //spcMapping is not empty
                if (!string.IsNullOrEmpty(spcMapping))
                {
                    NfiEstaFunctions.FnEtlImportSubPopulationCategoryMapping.Execute(
                        FormUserPassword.TargetDatabase,
                        spcMapping,
                        TargetTransaction);

                    // if (_atomicAreaDomains == false || _atomicSubPopulations == false) is section 11 Node D of schema
                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        if (ControlOwner is ControlEtl)
                        {
                            string variablesSource = TDFunctions.FnEtlGetVariables.Execute(
                                database: Database,
                                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                                idTEtlTargetVariable: _idEtl,
                                transaction: SourceTransaction);

                            string variablesTarget = NfiEstaFunctions.FnEtlCheckVariables.ExecuteScalar(
                                FormUserPassword.TargetDatabase,
                                variablesSource,
                                TargetTransaction);

                            if (string.IsNullOrEmpty(variablesTarget))
                            {
                                ShowControlTopicIfOwnerIsEtL();
                            }
                            else
                            {
                                RaiseNextClick(sender, e);
                            }
                        }
                        else
                        {
                            ShowControlAttributeVariablesOrTopic();
                        }
                    }
                }
                else
                {
                    //spcMapping is null or empty

                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        if (ControlOwner is ControlEtl)
                        {
                            string variablesSource = TDFunctions.FnEtlGetVariables.Execute(
                                database: Database,
                                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                                idTEtlTargetVariable: _idEtl,
                                transaction: SourceTransaction);

                            string variablesTarget = NfiEstaFunctions.FnEtlCheckVariables.ExecuteScalar(
                                FormUserPassword.TargetDatabase,
                                variablesSource,
                                TargetTransaction);

                            if (string.IsNullOrEmpty(variablesTarget))
                            {
                                ShowControlTopicIfOwnerIsEtL();
                            }
                            else
                            {
                                RaiseNextClick(sender, e);
                            }
                        }
                        else
                        {
                            ShowControlAttributeVariablesOrTopic();
                        }
                    }
                }
            }
            else
            {
                // adcMapping is null or empty

                string spcMapping =
                    TDFunctions.FnEtlExportSubPopulationCategoryMapping.Execute(
                        database: Database,
                        refYearSetToPanelMapping: _refYearSetToPanelMapping,
                        idTEtlTargetVariable: _idEtl,
                        transaction: SourceTransaction);

                //spcMapping is not empty
                if (!string.IsNullOrEmpty(spcMapping))
                {
                    NfiEstaFunctions.FnEtlImportSubPopulationCategoryMapping.Execute(
                        FormUserPassword.TargetDatabase,
                        spcMapping,
                        TargetTransaction);

                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        if (ControlOwner is ControlEtl)
                        {
                            string variablesSource = TDFunctions.FnEtlGetVariables.Execute(
                                database: Database,
                                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                                idTEtlTargetVariable: _idEtl,
                                transaction: SourceTransaction);

                            string variablesTarget = NfiEstaFunctions.FnEtlCheckVariables.ExecuteScalar(
                                FormUserPassword.TargetDatabase,
                                variablesSource,
                                TargetTransaction);

                            if (string.IsNullOrEmpty(variablesTarget))
                            {
                                ShowControlTopicIfOwnerIsEtL();
                            }
                            else
                            {
                                RaiseNextClick(sender, e);
                            }
                        }
                        else
                        {
                            ShowControlAttributeVariablesOrTopic();
                        }
                    }
                }
                else
                {
                    //spcMapping is null or empty

                    if (_atomicAreaDomains == false || _atomicSubPopulations == false)
                    {
                        ConfirmAndExitApplication();
                    }
                    else
                    {
                        if (ControlOwner is ControlEtl)
                        {
                            string variablesSource = TDFunctions.FnEtlGetVariables.Execute(
                                database: Database,
                                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                                idTEtlTargetVariable: _idEtl,
                                transaction: SourceTransaction);

                            string variablesTarget =
                                NfiEstaFunctions.FnEtlCheckVariables.ExecuteScalar(
                                    database: FormUserPassword.TargetDatabase,
                                    variables: variablesSource,
                                    TargetTransaction);

                            if (string.IsNullOrEmpty(variablesTarget))
                            {
                                ShowControlTopicIfOwnerIsEtL();
                            }
                            else
                            {
                                RaiseNextClick(sender, e);
                            }
                        }
                        else
                        {
                            ShowControlAttributeVariablesOrTopic();
                        }
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        #endregion NextClick Event

        #endregion Events


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události překreslení zobrazení buněk v DataGridView dgvSubPopulation</para>
        /// <para lang="en">Event handler of the cell view redraw in DataGridView dgvSubPopulation</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSubPopulation_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //change the standard display of boolean results in the auto column to text
            if (e.ColumnIndex == dgvSubPopulation.Columns["auto"].Index && e.RowIndex >= 0)
            {
                e.PaintBackground(e.CellBounds, false);

                Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                bool auto = (bool)e.Value;
                string text = (LanguageVersion == LanguageVersion.National && auto) ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty
                  : (LanguageVersion == LanguageVersion.National && !auto) ? labels.ContainsKey(key: "no") ? labels["no"] : String.Empty
                  : auto ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty : labels.ContainsKey(key: "no") ? labels["no"] : String.Empty;

                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    //set the font to Microsoft Sans Serif, 9.75pt
                    e.CellStyle.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular);

                    //move the text lower by 3 pixels
                    Rectangle textRect = new Rectangle(e.CellBounds.X, e.CellBounds.Y + 3,
                                                        e.CellBounds.Width, e.CellBounds.Height - 3);
                    e.Graphics.DrawString(text, e.CellStyle.Font, brush, textRect);
                }

                e.Handled = true;
            }

            // Hide the checkbox in the "Select" column if the value in the "auto" column is true
            else if (dgvSubPopulation.Columns[e.ColumnIndex].Name == "Select" && e.RowIndex >= 0)
            {
                // Get the value in the "auto" column for the current row
                bool autoValue = (bool)dgvSubPopulation.Rows[e.RowIndex].Cells["auto"].Value;

                // If autoValue is true, hide the checkbox
                if (autoValue)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.ContentForeground);
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost změny hodnoty buňky v DataGridView.</para>
        /// <para lang="en">Handles the CellValueChanged event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSubPopulation_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            noticeReceived = false;

            // Ensure this is the checkbox column
            DataGridViewColumn column = dgvSubPopulation.Columns["Select"];

            if (column != null && e.ColumnIndex == column.Index && e.RowIndex >= 0)
            {
                DataGridViewCheckBoxCell checkBoxCell =
                    (DataGridViewCheckBoxCell)dgvSubPopulation.Rows[e.RowIndex].Cells["Select"];

                bool isChecked = Convert.ToBoolean(checkBoxCell.Value);

                // Set the value of splSubPopulation.Panel2Collapsed based on the checkbox state
                splSubPopulation.Panel2Collapsed = !isChecked;

                if (!isChecked)
                {
                    splSubPopulationCategory.Panel2.Controls.Clear();
                    selectedEtlIdsForCombobox.Clear();

                    btnMatch.Enabled = false;
                    btnNoMatch.Enabled = false;
                }

                if (isChecked)
                {
                    // Checkbox has been checked

                    //splSubPopulationCategory.Panel2.Refresh();

                    if (btnAuto.Enabled == false)
                    {
                        btnNoMatch.Enabled = true;
                    }

                    // Access data from the row
                    DataGridViewRow selectedRow = dgvSubPopulation.Rows[e.RowIndex];

                    int? id = (int?)selectedRow.Cells["id"].Value;
                    string labelEn = selectedRow.Cells["label_en"].Value.ToString();
                    string subPopulationString = selectedRow.Cells["sub_population"].Value.ToString();
                    subPopulation = subPopulationString.Split(';')
                        .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                        .ToList();

                    List<int?> etlIds =
                        TDFunctions.FnEtlGetEtlIdsOfSubPopulation.Execute(
                            database: Database,
                            exportConnectionId: _selectedExportConnectionId,
                            transaction: SourceTransaction);

                    switch (LanguageVersion)
                    {
                        case LanguageVersion.National:
                            result = NfiEstaFunctions.FnEtlCheckSubPopulationsForEtl.ExecuteQuery(
                               database: FormUserPassword.TargetDatabase,
                               id: id,
                               subPopulation: subPopulation,
                               labelEn: labelEn,
                               nationalLanguage: nationalLang.NatLang.ToString(),
                               etlId: etlIds,
                               transaction: TargetTransaction);
                            break;

                        case LanguageVersion.International:
                            result = NfiEstaFunctions.FnEtlCheckSubPopulationsForEtl.ExecuteQuery(
                                database: FormUserPassword.TargetDatabase,
                                id: id,
                                subPopulation: subPopulation,
                                labelEn: labelEn,
                                nationalLanguage: "en",
                                etlId: etlIds,
                                transaction: TargetTransaction);
                            break;
                    }

                    if (result.Rows.Count > 0)
                    {
                        initializingDataSource = true;
                        dgvSubPopulationEtlIdNull.DataSource = result;
                        dgvSubPopulationEtlIdNull.ClearSelection();
                        initializingDataSource = false;
                        dgvSubPopulationEtlIdNull.Columns["id"].Visible = false;
                        dgvSubPopulationEtlIdNull.Columns["etl_id"].Visible = false;
                        dgvSubPopulationEtlIdNull.Columns["check_label_en"].Visible = false;
                        LoadContentOfDgvSubPopulationEtlIdNull();
                        FormatDataGridView(dgvSubPopulationEtlIdNull);
                        AutoSizeColumn(dgvSubPopulationEtlIdNull, "label");
                        AutoSizeColumn(dgvSubPopulationEtlIdNull, "label_en");
                        AutoSizeColumn(dgvSubPopulationEtlIdNull, "atomic");

                        foreach (DataGridViewRow row in dgvSubPopulationEtlIdNull.Rows)
                        {
                            row.Selected = false;
                            btnMatch.Enabled = false;
                            btnNoMatch.Enabled = true;
                        }
                    }
                    else
                    {
                        Dictionary<string, string> messages = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                        DialogResult dialog =
                            MessageBox.Show(
                                text: messages.ContainsKey(key: "msgPairError") ?
                                        messages["msgPairError"] : String.Empty,
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Error);
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost změny výběru řádku v DataGridView.</para>
        /// <para lang="en">Handles the SelectionChanged event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void dgvSubPopulationEtlIdNull_SelectionChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> labels = Dictionary(
                  languageVersion: LanguageVersion,
                  languageFile: LanguageFile);

            if (initializingDataSource)
            {
                return;
            }

            Cursor = Cursors.WaitCursor;

            splSubPopulationCategory.Panel2.Controls.Clear();
            selectedEtlIdsForCombobox.Clear();

            noticeReceived = false;

            if (dgvSubPopulationEtlIdNull.SelectedRows.Count > 0)
            {
                string spcJson = TDFunctions.FnEtlGetSubPopulationCategoryJson.Execute(
                            database: Database,
                            refYearSetToPanelMapping: _refYearSetToPanelMapping,
                            idTEtlTargetVariable: _idEtl,
                            subPopulation: subPopulation,
                            transaction: SourceTransaction);

                DataGridViewRow selectedRowInTargetDB = dgvSubPopulationEtlIdNull.SelectedRows[0];

                if (selectedRowInTargetDB.Cells["etl_id"] != null && selectedRowInTargetDB.Cells["etl_id"].Value != null)
                {
                    int etlId = Convert.ToInt32(selectedRowInTargetDB.Cells["etl_id"].Value);

                    if (dgvSubPopulationEtlIdNull.SelectedRows.Count > 0)
                    {
                        FormUserPassword.TargetDatabase.Postgres.NoticeReceived += OnNoticeReceived;
                    }

                    categories = NfiEstaFunctions.FnEtlCheckSubPopulationCategoriesForEtl.ExecuteQuery(
                        database: FormUserPassword.TargetDatabase,
                        subPopulation: etlId,
                        subPopulationCategory: spcJson,
                        transaction: TargetTransaction
                        );
                    FormUserPassword.TargetDatabase.Postgres.NoticeReceived -= OnNoticeReceived;

                    // If any value in etl_id is integer, only integer values are included (null values are omitted)
                    List<int?> etlIdValues = categories.AsEnumerable()
                        .Where(row => row["etl_id"] != DBNull.Value)
                        .Select(row => row.Field<int?>("etl_id"))
                        .ToList();

                    // If all values in etl_id are null, it is null::int4[], not array[null]::int4[]
                    List<int?> etlIdsForCombobox = etlIdValues.Any(val => val != null)
                        ? etlIdValues.Where(val => val != null).ToList()
                        : null;

                    if (!noticeReceived)
                    {
                        GroupBox grpSPCTargetDB = new GroupBox();
                        grpSPCTargetDB.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                        grpSPCTargetDB.ForeColor = SystemColors.HotTrack;
                        grpSPCTargetDB.Text = labels.ContainsKey(key: "grpSPCTargetDB") ?
                                                labels["grpSPCTargetDB"] : String.Empty;
                        grpSPCTargetDB.Dock = DockStyle.Fill;
                        splSubPopulationCategory.Panel2.Controls.Add(grpSPCTargetDB);

                        flpCategoryComboBoxes = new FlowLayoutPanel();
                        flpCategoryComboBoxes.Dock = DockStyle.Fill;
                        grpSPCTargetDB.Controls.Add(flpCategoryComboBoxes);
                        flpCategoryComboBoxes.AutoScroll = true;
                        flpCategoryComboBoxes.BackColor = System.Drawing.SystemColors.Window;
                        flpCategoryComboBoxes.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
                        flpCategoryComboBoxes.Location = new System.Drawing.Point(0, 0);
                        flpCategoryComboBoxes.Name = "flpCategoryComboBoxes";
                        flpCategoryComboBoxes.Size = new System.Drawing.Size(596, 214);
                        flpCategoryComboBoxes.TabIndex = 1;
                        flpCategoryComboBoxes.WrapContents = false;

                        // Check if the headerLabel already exists in the panel
                        bool headerExists = false;
                        foreach (Control control in flpCategoryComboBoxes.Controls)
                        {
                            if (control is Label && control.Text == (labels.ContainsKey(key: "targetDBCategoriesHeader") ? labels["targetDBCategoriesHeader"] : String.Empty))
                            {
                                headerExists = true;
                                break;
                            }
                        }

                        // If headerLabel does not exist, add it
                        if (!headerExists)
                        {
                            Label headerLabel = new Label
                            {
                                Text = labels.ContainsKey(key: "targetDBCategoriesHeader") ? labels["targetDBCategoriesHeader"] : String.Empty,
                                Font = dgvSPC.ColumnHeadersDefaultCellStyle.Font,
                                TextAlign = ContentAlignment.MiddleLeft,
                                BackColor = Color.DarkBlue,
                                ForeColor = Color.White,
                                AutoSize = false,
                                Padding = new Padding(30, 0, 0, 0),
                                Width = flpCategoryComboBoxes.Width,
                                Height = dgvSPC.ColumnHeadersHeight,
                                Margin = new Padding(0, 0, 0, 0)  // No space around the headerLabel
                            };

                            flpCategoryComboBoxes.Controls.Add(headerLabel);
                        }

                        int rowHeight = dgvSPC.RowTemplate.Height;

                        foreach (DataRow row in categories.Rows)
                        {
                            System.Windows.Forms.Panel comboTextPanel = new System.Windows.Forms.Panel
                            {
                                Width = flpCategoryComboBoxes.Width,
                                Height = rowHeight,
                                Margin = new Padding(0, 0, 0, 1)  // Minimal space below the Panel to create a small gap between ComboBoxes
                            };

                            int? rowNumber = row.Field<int?>("id");
                            int? etlIdCategories = row.Field<int?>("etl_id");

                            // Create a label for row number
                            Label rowNumLabel = new Label
                            {
                                Text = rowNumber.ToString(),
                                Margin = new Padding(1),      // Set a small space
                                Width = 28,                   // Set the width explicitly
                                AutoSize = false,             // Disable auto-sizing
                                TextAlign = ContentAlignment.MiddleCenter, // Center the text horizontally
                            };

                            rowNumLabel.Location = new Point(0, (comboTextPanel.Height - rowNumLabel.Height) / 2);

                            comboTextPanel.Controls.Add(rowNumLabel);

                            bool? checkLabel = row.Field<bool?>("check_label");
                            if (checkLabel == true)
                            {
                                TextBox textBox = new TextBox
                                {
                                    Text = (LanguageVersion == LanguageVersion.National) ? row["label"].ToString() : row["label_en"].ToString(),
                                    Tag = etlIdCategories,
                                    BackColor = Color.PaleGreen,
                                    Width = flpCategoryComboBoxes.Width - rowNumLabel.Width - 5,
                                    ReadOnly = true,
                                    Location = new Point(rowNumLabel.Width + 5, (comboTextPanel.Height - rowNumLabel.Height) / 2)
                                };

                                if (LanguageVersion == LanguageVersion.National)
                                {
                                    toolTip.SetToolTip(textBox, row["label_en"].ToString());
                                }

                                comboTextPanel.Controls.Add(textBox);

                                bool onlyTextBoxesDisplayed = CheckIfOnlyTextBoxesDisplayed();

                                if (onlyTextBoxesDisplayed)
                                {
                                    btnMatch.Enabled = true;
                                }
                                else
                                {
                                    btnMatch.Enabled = false;
                                }
                            }
                            else
                            {
                                ComboBox comboBox = new ComboBox
                                {
                                    DropDownStyle = ComboBoxStyle.DropDown,
                                    Width = flpCategoryComboBoxes.Width - rowNumLabel.Width - 5,
                                    Location = new Point(rowNumLabel.Width + 5, (comboTextPanel.Height - rowNumLabel.Height) / 2),
                                };

                                comboBox.DropDown += (cbSender, args) =>
                                {
                                    ComboBox cb = cbSender as ComboBox;
                                    if (cb != null && cb.Items.Count == 0)
                                    {
                                        categoriesForRoller = NfiEstaFunctions.FnEtlGetSubPopulationCategoriesForRoller.ExecuteQuery(
                                                                            database: FormUserPassword.TargetDatabase,
                                                                            idOrdering: rowNumber,
                                                                            subPopulation: etlId,
                                                                            etlId: etlIdsForCombobox,
                                                                            transaction: TargetTransaction);

                                        DataRow newRow = categoriesForRoller.NewRow();
                                        newRow["label"] = "";
                                        newRow["label_en"] = "";
                                        newRow["id"] = 0;
                                        newRow["etl_id"] = 0;
                                        categoriesForRoller.Rows.InsertAt(newRow, 0);

                                        var selectedCategories = categoriesForRoller.AsEnumerable()
                                               .Where(comboboxRow => !selectedEtlIdsForCombobox.Contains(comboboxRow.Field<int?>("etl_id")))
                                               .CopyToDataTable();

                                        cb.DataSource = selectedCategories;
                                        cb.DisplayMember = (LanguageVersion == LanguageVersion.National) ? "label" : "label_en";
                                        cb.ValueMember = "etl_id";
                                    }
                                };

                                comboBox.KeyPress += (s, args) => args.Handled = true;
                                comboBox.KeyDown += (s, args) => args.SuppressKeyPress = true;
                                comboBox.MouseDown += (s, args) => ((ComboBox)s).DroppedDown = true;
                                comboBox.SelectionChangeCommitted += new EventHandler(comboBox_SelectionChangeCommitted);

                                comboTextPanel.Controls.Add(comboBox);
                            }

                            flpCategoryComboBoxes.Controls.Add(comboTextPanel);

                            btnMatch.Enabled = AreAllComboBoxSelectionsValid();
                            UpdateNoMatchButton();
                        }
                    }
                    else
                    {
                        btnMatch.Enabled = false;
                        UpdateNoMatchButton();
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost SelectionChangeCommitted pro ComboBox. Tato událost je vyvolána, když uživatel změní vybranou položku v ComboBoxu. Metoda aktualizuje seznam vybraných etl id pro ComboBox, přidává nově vybraný id, pokud již není v seznamu.</para>
        /// <para lang="en">Handles the SelectionChangeCommitted event for a ComboBox. This event is triggered when the user changes the selected item in the ComboBox. The method updates the list of selected etl ids for the ComboBox, adding the newly selected id if it is not already in the list.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (ComboBox)</para>
        /// <para lang="en">The source of the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ComboBox cmb = sender as ComboBox;
            if (cmb != null && cmb.SelectedValue != null)
            {
                selectedEtlIdForCombobox = Convert.ToInt32(cmb.SelectedValue);

                if (!selectedEtlIdsForCombobox.Contains(selectedEtlIdForCombobox))
                {
                    selectedEtlIdsForCombobox.Add(selectedEtlIdForCombobox);
                }

                if (LanguageVersion == LanguageVersion.National)
                {
                    if (cmb.SelectedItem is DataRowView selectedRow)
                    {
                        string labelEn = selectedRow["label_en"].ToString();
                        toolTip.SetToolTip(cmb, labelEn);
                    }
                }
            }

            if (selectedEtlIdForCombobox == 0)
            {
                btnMatch.Enabled = false;
                btnNoMatch.Enabled = true;
            }
            else
            {
                btnMatch.Enabled = AreAllComboBoxSelectionsValid();

                if (btnMatch.Enabled == true)
                {
                    btnNoMatch.Enabled = false;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost změny stavu "špinavé" buňky (buňky s neuloženými změnami) v DataGridView.</para>
        /// <para lang="en">Handles the CurrentCellDirtyStateChanged event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSubPopulation_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvSubPopulation.IsCurrentCellDirty)
            {
                // Commit the change and refresh the cell value
                dgvSubPopulation.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost, která nastává na začátku úpravy buňky v DataGridView.</para>
        /// <para lang="en">Handles the event that occurs at the start of cell editing in the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView).</para>
        /// <para lang="en">The source of the event (DataGridView).</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSubPopulation_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //to allow to edit only checkbox, not other parts of the row
            if (dgvSubPopulation.Columns[e.ColumnIndex].Name == "Select")
            {
                // If there's already a checked checkbox, cancel the edit
                bool hasCheckedBox = dgvSubPopulation.Rows.Cast<DataGridViewRow>().Any(r => Convert.ToBoolean(r.Cells["Select"].Value));

                if (hasCheckedBox && !(bool)dgvSubPopulation.Rows[e.RowIndex].Cells[e.ColumnIndex].Value)
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost kliknutí na obsah buňky v DataGridView.</para>
        /// <para lang="en">Handles the CellContentClick event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSubPopulation_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // If the clicked cell is in the "Select" column
            if (dgvSubPopulation.Columns[e.ColumnIndex].Name == "Select")
            {
                dgvSubPopulation.CommitEdit(DataGridViewDataErrorContexts.Commit);

                // Iterate over all the rows
                foreach (DataGridViewRow row in dgvSubPopulation.Rows)
                {
                    // If it is the row that was clicked, skip it
                    if (row.Index == e.RowIndex)
                        continue;

                    // Uncheck the checkbox in the "Select" column for all other rows
                    row.Cells["Select"].Value = false;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost změny vybraného řádku v DataGridView.</para>
        /// <para lang="en">Handles the SelectionChanged event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSubPopulation_SelectionChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            if (dgvSubPopulation.SelectedRows.Count > 0)
            {
                LoadContentOfDgvSPC();
            }

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost zobrazení ToolTipu v DataGridView.</para>
        /// <para lang="en">Handles the ToolTip event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void dgvSubPopulation_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            // Ensure that the tooltip is being requested for a valid cell
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                // Check if the language version is National
                if (LanguageVersion == LanguageVersion.National)
                {
                    Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                    // Retrieve the value in the column "label_en" for the hovered row
                    var labelEn = dgvSubPopulation.Rows[e.RowIndex].Cells["label_en"].Value;

                    // Set the tooltip text
                    if (labelEn != null)
                    {
                        e.ToolTipText = labelEn.ToString();
                    }
                    else
                    {
                        e.ToolTipText = labels.ContainsKey("EnglishVersionUnavailable")
                            ? labels["EnglishVersionUnavailable"]
                            : String.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluhuje událost CellPainting v DataGridView</para>
        /// <para lang="en">Handles the CellPainting event of the DataGridView</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSubPopulationEtlIdNull_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //change the standard display of boolean results in the atomic column to text
            if (e.ColumnIndex == dgvSubPopulationEtlIdNull.Columns["atomic"].Index && e.RowIndex >= 0)
            {
                e.PaintBackground(e.CellBounds, false);

                bool atomic = (bool)e.Value;
                string text = (LanguageVersion == LanguageVersion.National && atomic) ? "ano"
                  : (LanguageVersion == LanguageVersion.National && !atomic) ? "ne"
                  : atomic ? "yes" : "no";

                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    //set the font to Microsoft Sans Serif, 9.75pt
                    e.CellStyle.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular);

                    //move the text lower by 3 pixels
                    Rectangle textRect = new Rectangle(e.CellBounds.X, e.CellBounds.Y + 3,
                                                        e.CellBounds.Width, e.CellBounds.Height - 3);
                    e.Graphics.DrawString(text, e.CellStyle.Font, brush, textRect);
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost zobrazení ToolTipu v DataGridView.</para>
        /// <para lang="en">Handles the ToolTip event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void dgvSubPopulationEtlIdNull_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            // Ensure that the tooltip is being requested for a valid cell
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                // Check if the language version is National
                if (LanguageVersion == LanguageVersion.National)
                {
                    // Retrieve the value in the column "label_en" for the hovered row
                    var labelEn = dgvSubPopulationEtlIdNull.Rows[e.RowIndex].Cells["label_en"].Value;

                    // Set the tooltip text
                    e.ToolTipText = labelEn?.ToString() ?? "Anglická verze není dostupná";
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Ošetřuje událost zobrazení ToolTipu v DataGridView.</para>
        /// <para lang="en">Handles the ToolTip event of the DataGridView.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (DataGridView)</para>
        /// <para lang="en">The source of the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvSPC_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            // Ensure that the tooltip is being requested for a valid cell
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                // Check if the language version is National
                if (LanguageVersion == LanguageVersion.National)
                {
                    Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

                    // Retrieve the value in the column "label_en" for the hovered row
                    var labelEn = dgvSPC.Rows[e.RowIndex].Cells["label_en"].Value;

                    // Set the tooltip text
                    if (labelEn != null)
                    {
                        e.ToolTipText = labelEn.ToString();
                    }
                    else
                    {
                        e.ToolTipText = labels.ContainsKey("EnglishVersionUnavailable")
                            ? labels["EnglishVersionUnavailable"]
                            : String.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluhuje událost Click na tlačítku btnAuto</para>
        /// <para lang="en">Handles the Click event of the btnAuto button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (Button)</para>
        /// <para lang="en">The source of the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnAuto_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> messages = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            DialogResult dialogResult = MessageBox.Show(
                                        text: messages.ContainsKey(key: "msgAutoConfirm") ?
                                                messages["msgAutoConfirm"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.YesNo,
                                        icon: MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                if (dgvSubPopulation.Rows.Cast<DataGridViewRow>().Any(row => (bool)row.Cells["auto"].Value))
                {
                    foreach (DataGridViewRow row in dgvSubPopulation.Rows)
                    {
                        if ((bool)row.Cells["auto"].Value)
                        {
                            int? id = (int?)row.Cells["id"].Value;
                            string subPopulationString = row.Cells["sub_population"].Value.ToString();
                            List<int?> subPopulation = subPopulationString.Split(';')
                                .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                                .ToList();
                            string label = row.Cells["label"].Value.ToString();
                            string description = row.Cells["description"].Value.ToString();
                            string labelEn = row.Cells["label_en"].Value.ToString();
                            string descriptionEn = row.Cells["description_en"].Value.ToString();
                            bool? atomic = (bool?)row.Cells["atomic"].Value;

                            List<int?> etlIds =
                            TDFunctions.FnEtlGetEtlIdsOfSubPopulation.Execute(
                                database: Database,
                                exportConnectionId: _selectedExportConnectionId,
                                transaction: SourceTransaction);

                            switch (LanguageVersion)
                            {
                                case LanguageVersion.National:
                                    result = NfiEstaFunctions.FnEtlCheckSubPopulationsForEtl.ExecuteQuery(
                                        database: FormUserPassword.TargetDatabase,
                                        id: id,
                                        subPopulation: subPopulation,
                                        labelEn: labelEn,
                                        nationalLanguage: nationalLang.NatLang.ToString(),
                                        etlId: etlIds,
                                        transaction: TargetTransaction);

                                    break;

                                case LanguageVersion.International:
                                    result = NfiEstaFunctions.FnEtlCheckSubPopulationsForEtl.ExecuteQuery(
                                        database: FormUserPassword.TargetDatabase,
                                        id: id,
                                        subPopulation: subPopulation,
                                        labelEn: labelEn,
                                        nationalLanguage: "en",
                                        etlId: etlIds,
                                        transaction: TargetTransaction);

                                    break;
                            }

                            if ((bool)row.Cells["auto_transfer"].Value)
                            {
                                DataTable newSubPopulation = NfiEstaFunctions.FnEtlImportSubPopulation.ExecuteQuery(
                                                                    FormUserPassword.TargetDatabase,
                                                                    id,
                                                                    label,
                                                                    description,
                                                                    labelEn,
                                                                    descriptionEn,
                                                                    atomic,
                                                                    TargetTransaction);

                                etlIdSubPopulation = Convert.ToInt32(newSubPopulation.Rows[0]["etl_id"]);

                                TDFunctions.FnEtlSaveSubPopulation.Execute(
                                    database: Database,
                                    exportConnection: _selectedExportConnectionId,
                                    subPopulation: subPopulation,
                                    etlId: etlIdSubPopulation,
                                    transaction: SourceTransaction);

                                DataTable resultSPC = TDFunctions.FnEtlGetSubPopulationCategory.ExecuteQuery(
                                    database: Database,
                                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                                    idTEtlTargetVariable: _idEtl,
                                    subPopulation: subPopulation,
                                    transaction: SourceTransaction);

                                foreach (DataRow rowSPC in resultSPC.Rows)
                                {
                                    int? idSPC = (int?)rowSPC["id"];
                                    int etlSubPopulation = (int)rowSPC["t_etl_sub_population__id"];
                                    int subPopulationForSPC = (int)rowSPC["t_etl_sub_population__etl_id"];
                                    string labelSPC = (string)rowSPC["label"];
                                    string descriptionSPC = (string)rowSPC["description"];
                                    string labelEnSPC = (string)rowSPC["label_en"];
                                    string descriptionEnSPC = (string)rowSPC["description_en"];
                                    string subPopulationCategoryString = rowSPC["sub_population_category"].ToString();
                                    List<int?> subPopulationCategory = subPopulationCategoryString.Split(';')
                                        .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                                        .ToList();

                                    DataTable newSubPopulationCategory = NfiEstaFunctions.FnEtlImportSubPopulationCategory.ExecuteQuery(
                                        FormUserPassword.TargetDatabase,
                                        idSPC,
                                        subPopulationForSPC,
                                        labelSPC,
                                        descriptionSPC,
                                        labelEnSPC,
                                        descriptionEnSPC,
                                        TargetTransaction);

                                    int? etlIdSPC = Convert.ToInt32(newSubPopulationCategory.Rows[0]["etl_id"]);

                                    TDFunctions.FnEtlSaveSubPopulationCategory.ExecuteQuery(
                                        database: Database,
                                        subPopulationCategory: subPopulationCategory,
                                        etlId: etlIdSPC,
                                        etlSubPopulation: etlSubPopulation,
                                        transaction: SourceTransaction);
                                }
                            }
                            else
                            {
                                if (result.Rows.Count > 0)
                                {
                                    DataRow rowResult = result.Rows[0];

                                    int etlId = (int)rowResult["etl_id"];

                                    TDFunctions.FnEtlSaveSubPopulation.Execute(
                                        database: Database,
                                        exportConnection: _selectedExportConnectionId,
                                        subPopulation: subPopulation,
                                        etlId: etlId,
                                        transaction: SourceTransaction);

                                    DataTable resultSPC = TDFunctions.FnEtlGetSubPopulationCategory.ExecuteQuery(
                                        database: Database,
                                        refYearSetToPanelMapping: _refYearSetToPanelMapping,
                                        idTEtlTargetVariable: _idEtl,
                                        subPopulation: subPopulation,
                                        transaction: SourceTransaction);

                                    foreach (DataRow rowSPC in resultSPC.Rows)
                                    {
                                        int? idSPC = (int?)rowSPC["id"];
                                        int subPopulationForSPC = (int)rowSPC["t_etl_sub_population__etl_id"];
                                        string labelEnForSPC = (string)rowSPC["label_en"];
                                        string subPopulationCategoryString = rowSPC["sub_population_category"].ToString();
                                        List<int?> subPopulationCategory = subPopulationCategoryString.Split(';')
                                            .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                                            .ToList();
                                        int etlSubPopulation = (int)rowSPC["t_etl_sub_population__id"];

                                        DataTable checkSPC = NfiEstaFunctions.FnEtlCheckSubPopulationCategory.ExecuteQuery(
                                            FormUserPassword.TargetDatabase,
                                            idSPC,
                                            subPopulationForSPC,
                                            labelEnForSPC,
                                            TargetTransaction);

                                        int? etlIdSPC = Convert.ToInt32(checkSPC.Rows[0]["etl_id"]);

                                        TDFunctions.FnEtlSaveSubPopulationCategory.ExecuteQuery(
                                            database: Database,
                                            subPopulationCategory: subPopulationCategory,
                                            etlId: etlIdSPC,
                                            etlSubPopulation: etlSubPopulation,
                                            transaction: SourceTransaction);
                                    }
                                }
                            }
                        }
                    }

                    bool allRowsAuto = dgvSubPopulation.Rows
                                           .Cast<DataGridViewRow>()
                                           .All(row => (bool)row.Cells["auto"].Value);

                    if (allRowsAuto)
                    {
                        dataSourceForDgvSubPopulation.Rows.Clear();
                        resultSPC.Rows.Clear();
                        btnAuto.Enabled = false;
                        btnMatch.Enabled = false;
                        btnNoMatch.Enabled = false;
                        btnNext.Enabled = true;
                        Next();
                    }
                    else
                    {
                        LoadContent();
                        LoadContentOfDgvSPC();
                    }

                    //if (dgvSubPopulation.Rows.Count > 1)
                    //{
                    //    Console.WriteLine($"Počet zbývajících záznamů v tabulce: {dgvSubPopulation.Rows.Count}");
                    //    LoadContent();
                    //    LoadContentOfDgvSPC();
                    //}
                    //else
                    //{
                    //    dataSourceForDgvSubPopulation.Rows.Clear();
                    //    resultSPC.Rows.Clear();
                    //    btnAuto.Enabled = false;
                    //    btnMatch.Enabled = false;
                    //    btnNoMatch.Enabled = false;
                    //    btnNext.Enabled = true;
                    //}
                }
            }

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Obsluhuje událost Click na tlačítku btnMatch</para>
        /// <para lang="en">Handles the Click event of the btnMatch button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (Button)</para>
        /// <para lang="en">The source of the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnMatch_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            if (dgvSubPopulationEtlIdNull.CurrentRow != null)
            {
                Dictionary<string, string> messages = Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: LanguageFile);
                DialogResult dialog =
                    MessageBox.Show(
                        text: messages.ContainsKey(key: "msgPair") ?
                                messages["msgPair"] : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.YesNo,
                        icon: MessageBoxIcon.Information);

                if (dialog == DialogResult.Yes)
                {
                    int etlIdSubPopulation = Convert.ToInt32(dgvSubPopulationEtlIdNull.CurrentRow.Cells["etl_id"].Value);
                    List<int?> orderedEtlIds = GetOrderedEtlIdsFromControls();
                    int etlIdIndex = 0;

                    if (orderedEtlIds.GroupBy(x => x).Any(g => g.Count() > 1))
                    {
                        MessageBox.Show(
                        text: messages.ContainsKey(key: "msgDuplicity") ?
                                messages["msgDuplicity"] : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    }
                    else if (orderedEtlIds.Contains(0))
                    {
                        MessageBox.Show(
                        text: messages.ContainsKey(key: "msgInvalidValue") ?
                                messages["msgInvalidValue"] : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    }
                    else
                    {
                        foreach (DataGridViewRow row in dgvSubPopulation.Rows)
                        {
                            bool isSelected = Convert.ToBoolean(row.Cells["Select"].Value);
                            if (isSelected)
                            {
                                string subPopulationString = row.Cells["sub_population"].Value.ToString();
                                List<int?> subPopulation = subPopulationString.Split(';')
                                    .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                                    .ToList();

                                int? etlSubPopulation = TDFunctions.FnEtlSaveSubPopulation.Execute(
                                    database: Database,
                                    exportConnection: _selectedExportConnectionId,
                                    subPopulation: subPopulation,
                                    etlId: etlIdSubPopulation,
                                    transaction: SourceTransaction);

                                foreach (DataRow categoryRow in categories.Rows)
                                {
                                    string subPopulationCategoryString = categoryRow["sub_population_category"].ToString();
                                    List<int?> subPopulationCategory = subPopulationCategoryString.Split(';')
                                        .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                                        .ToList();

                                    TDFunctions.FnEtlSaveSubPopulationCategory.Execute(
                                        database: Database,
                                        subPopulationCategory: subPopulationCategory,
                                        etlId: orderedEtlIds[etlIdIndex],
                                        etlSubPopulation: etlSubPopulation,
                                        transaction: SourceTransaction);

                                    etlIdIndex++;
                                }

                                row.Cells["Select"].Value = false;
                            }
                        }

                        if (dgvSubPopulation.Rows.Count > 1)
                        {
                            splSubPopulation.Panel2Collapsed = true;
                            LoadContent();
                            LoadContentOfDgvSPC();
                            splSubPopulationCategory.Panel2.Controls.Clear();
                        }
                        else
                        {
                            splSubPopulation.Panel2Collapsed = true;
                            dataSourceForDgvSubPopulation.Rows.Clear();
                            resultSPC.Rows.Clear();
                            splSubPopulationCategory.Panel2.Controls.Clear();
                            btnAuto.Enabled = false;
                            btnMatch.Enabled = false;
                            btnNoMatch.Enabled = false;
                            btnNext.Enabled = true;
                            Next();
                        }
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Obsluhuje událost Click na tlačítku btnNoMatch</para>
        /// <para lang="en">Handles the Click event of the btnNoMatch button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá (Button)</para>
        /// <para lang="en">The source of the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnNoMatch_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> messages = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            DialogResult dialogResult = MessageBox.Show(
                                        text: messages.ContainsKey(key: "msgTransfer") ?
                                                messages["msgTransfer"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.YesNo,
                                        icon: MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                foreach (DataGridViewRow row in dgvSubPopulation.Rows)
                {
                    bool isSelected = Convert.ToBoolean(row.Cells["Select"].Value);
                    if (isSelected)
                    {
                        int? id = (int?)row.Cells["id"].Value;
                        string label = row.Cells["label"].Value.ToString();
                        string description = row.Cells["description"].Value.ToString();
                        string labelEn = row.Cells["label_en"].Value.ToString();
                        string descriptionEn = row.Cells["description_en"].Value.ToString();
                        bool? atomic = (bool?)row.Cells["atomic"].Value;
                        string subPopulationString = row.Cells["sub_population"].Value.ToString();
                        List<int?> subPopulation = subPopulationString.Split(';')
                            .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                            .ToList();

                        DataTable newSubPopulation = NfiEstaFunctions.FnEtlImportSubPopulation.ExecuteQuery(
                                                            FormUserPassword.TargetDatabase,
                                                            id,
                                                            label,
                                                            description,
                                                            labelEn,
                                                            descriptionEn,
                                                            atomic,
                                                            TargetTransaction);

                        etlIdSubPopulation = Convert.ToInt32(newSubPopulation.Rows[0]["etl_id"]);

                        TDFunctions.FnEtlSaveSubPopulation.Execute(
                                            database: Database,
                                            exportConnection: _selectedExportConnectionId,
                                            subPopulation: subPopulation,
                                            etlId: etlIdSubPopulation,
                                            transaction: SourceTransaction);

                        DataTable resultForSPC = TDFunctions.FnEtlGetSubPopulationCategory.ExecuteQuery(
                            database: Database,
                            refYearSetToPanelMapping: _refYearSetToPanelMapping,
                            idTEtlTargetVariable: _idEtl,
                            subPopulation: subPopulation,
                            transaction: SourceTransaction);

                        foreach (DataRow rowSPC in resultForSPC.Rows)
                        {
                            int? idSPC = (int?)rowSPC["id"];
                            int subPopulationForSPC = (int)rowSPC["t_etl_sub_population__etl_id"];
                            string labelSPC = rowSPC["label"].ToString();
                            string descriptionSPC = rowSPC["description"].ToString();
                            string labelEnSPC = rowSPC["label_en"].ToString();
                            string descriptionEnSPC = rowSPC["description_en"].ToString();
                            string subPopulationCategoryString = rowSPC["sub_population_category"].ToString();
                            List<int?> subPopulationCategory = subPopulationCategoryString.Split(';')
                                .Select(s => int.TryParse(s, out int i) ? (int?)i : null)
                                .ToList();
                            int? idTEtlSp = (int?)rowSPC["t_etl_sub_population__id"];

                            DataTable newSubPopulationCategory = NfiEstaFunctions.FnEtlImportSubPopulationCategory.ExecuteQuery(
                                FormUserPassword.TargetDatabase,
                                idSPC,
                                subPopulationForSPC,
                                labelSPC,
                                descriptionSPC,
                                labelEnSPC,
                                descriptionEnSPC,
                                TargetTransaction);

                            int? etlIdSPC = Convert.ToInt32(newSubPopulationCategory.Rows[0]["etl_id"]);

                            TDFunctions.FnEtlSaveSubPopulationCategory.ExecuteQuery(
                                database: Database,
                                subPopulationCategory: subPopulationCategory,
                                etlId: etlIdSPC,
                                etlSubPopulation: idTEtlSp,
                                transaction: SourceTransaction);
                        }

                        row.Cells["Select"].Value = false;
                    }

                    if (dgvSubPopulation.Rows.Count > 1)
                    {
                        splSubPopulation.Panel2Collapsed = true;
                        LoadContent();
                        LoadContentOfDgvSPC();
                        splSubPopulationCategory.Panel2.Controls.Clear();
                    }
                    else
                    {
                        splSubPopulation.Panel2Collapsed = true;
                        dataSourceForDgvSubPopulation.Rows.Clear();
                        resultSPC.Rows.Clear();
                        splSubPopulationCategory.Panel2.Controls.Clear();
                        btnAuto.Enabled = false;
                        btnMatch.Enabled = false;
                        btnNoMatch.Enabled = false;
                        btnNext.Enabled = true;
                        Next();
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Reaguje na přijetí upozornění z databáze prostřednictvím Npgsql. Nastaví příznak přijetí upozornění na true a zobrazí zprávu upozornění v dialogovém okně.</para>
        /// <para lang="en">Responds to receiving a notice from the database via Npgsql. Sets a notice received flag to true and displays the notice message in a message box.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který událost odesílá.</para>
        /// <para lang="en">The source of the event.</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události obsahující upozornění z databáze.</para>
        /// <para lang="en">Event parameters containing the database notice.</para>
        /// </param>
        private void OnNoticeReceived(object sender, NpgsqlNoticeEventArgs e)
        {
            if (dgvSubPopulationEtlIdNull.SelectedRows.Count > 0)
            {
                MessageBox.Show(e.Notice.MessageText, "Notice from Database", MessageBoxButtons.OK, MessageBoxIcon.Information);
                noticeReceived = true;
            }

            FormUserPassword.TargetDatabase.Postgres.NoticeReceived -= OnNoticeReceived;
        }

        #endregion Event Handlers
    }

}
