﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Formulář pro aktualizaci zvoleného tématu</para>
    /// <para lang="en">Form to update the selected topic</para>
    /// </summary>
    public partial class FormTopicUpdate
            : Form, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Id tématu předaný z ControlTopic</para>
        /// <para lang="en">Topic id passed by ControlTopic</para>
        /// </summary>
        private readonly int? _selectedId;

        /// <summary>
        /// <para lang="cs">Zkratka tématu předaná z ControlTopic</para>
        /// <para lang="en">Topic label passed by ControlTopic</para>
        /// </summary>
        private readonly string _selectedLabel;

        /// <summary>
        /// <para lang="cs">Popis tématu předaný z ControlTopic</para>
        /// <para lang="en">Topic description passed by ControlTopic</para>
        /// </summary>
        private readonly string _selectedDescription;

        /// <summary>
        /// <para lang="cs">Anglická zkratka tématu předaná z ControlTopic</para>
        /// <para lang="en">English label of the topic passed by ControlTopic</para>
        /// </summary>
        private readonly string _selectedLabelEn;

        /// <summary>
        /// <para lang="cs">Anglický popis tématu předaný z ControlTopic</para>
        /// <para lang="en">English description of the topic passed by ControlTopic</para>
        /// </summary>
        private readonly string _selectedDescriptionEn;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro aktualizaci zvoleného tématu</para>
        /// <para lang="en">Form constructor to update the selected topic</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="selectedId">
        /// <para lang="cs">Id tématu předaný z ControlTopic</para>
        /// <para lang="en">Topic id passed by ControlTopic</para>
        /// </param>
        /// <param name="selectedLabel">
        /// <para lang="cs">Zkratka tématu předaná z ControlTopic</para>
        /// <para lang="en">Topic label passed by ControlTopic</para>
        /// </param>
        /// <param name="selectedDescription">
        /// <para lang="cs">Popis tématu předaný z ControlTopic</para>
        /// <para lang="en">Topic description passed by ControlTopic</para>
        /// </param>
        /// <param name="selectedLabelEn">
        /// <para lang="cs">Anglická zkratka tématu předaná z ControlTopic</para>
        /// <para lang="en">English label of the topic passed by ControlTopic</para>
        /// </param>
        /// <param name="selectedDescriptionEn">
        /// <para lang="cs">Anglický popis tématu předaný z ControlTopic</para>
        /// <para lang="en">English description of the topic passed by ControlTopic</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public FormTopicUpdate(
            Control controlOwner,
            int? selectedId,
            string selectedLabel,
            string selectedDescription,
            string selectedLabelEn,
            string selectedDescriptionEn,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction)
        {
            InitializeComponent();

            _selectedId = selectedId;
            _selectedLabel = selectedLabel;
            _selectedDescription = selectedDescription;
            _selectedLabelEn = selectedLabelEn;
            _selectedDescriptionEn = selectedDescriptionEn;
            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;

            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is ControlTopic))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlTopic.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is ControlTopic))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlTopic.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((ControlTopic)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((ControlTopic)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ControlTopic)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ControlTopic)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlTopic ctrTopic)
                {
                    return ctrTopic.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",             "Aktualizace zvoleného tématu" },
                            { "grpTopic",               String.Empty },
                            { "lblLabel",               "Zkratka" },
                            { "lblDescription",         "Popis" },
                            { "lblLabelEn",             "Zkratka - anglicky" },
                            { "lblDescriptionEn",       "Popis - anglicky" },
                            { "btnOK",                  "Aktualizovat" },
                            { "btnCancel",              "Zrušit" },
                            { "msgTextBoxIsEmpty",      "Všechna pole musí být vyplněna." },
                            { "msgDuplicateEntry",      "Toto téma už je v seznamu uloženo." },
                            { "msgDuplicateLabel",      "Tato zkratka už je v seznamu uložena." },
                            { "msgDuplicateLabelEn",    "Tato anglická zkratka už je v seznamu uložena." }
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "FormTopicUpdate") ?
                    languageFile.NationalVersion.Data["FormTopicUpdate"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",             "Update the selected topic" },
                            { "grpTopic",               String.Empty },
                            { "lblLabel",               "Label - national" },
                            { "lblDescription",         "Description - national" },
                            { "lblLabelEn",             "Label - English" },
                            { "lblDescriptionEn",       "Description - English" },
                            { "btnOK",                  "Update" },
                            { "btnCancel",              "Cancel" },
                            { "msgTextBoxIsEmpty",      "All boxes must be completed." },
                            { "msgDuplicateEntry",      "This topic is already stored in the list." },
                            { "msgDuplicateLabel",      "This national label is already stored in the list." },
                            { "msgDuplicateLabelEn",    "This English label is already stored in the list." }
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "FormTopicUpdate") ?
                    languageFile.InternationalVersion.Data["FormTopicUpdate"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro aktualizaci zvoleného tématu</para>
        /// <para lang="en">Initializing the form to update the selected topic</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro aktualizaci tématu</para>
        /// <para lang="en">Initializing form labels to update the selected topic</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text =
               labels.ContainsKey(key: "frmCaption") ?
               labels["frmCaption"] : String.Empty;

            grpTopic.Text =
                labels.ContainsKey(key: "grpTopic") ?
                labels["grpTopic"] : String.Empty;

            lblLabel.Text =
                labels.ContainsKey(key: "lblLabel") ?
                labels["lblLabel"] : String.Empty;

            lblDescription.Text =
                labels.ContainsKey(key: "lblDescription") ?
                labels["lblDescription"] : String.Empty;

            lblLabelEn.Text =
                labels.ContainsKey(key: "lblLabelEn") ?
                labels["lblLabelEn"] : String.Empty;

            lblDescriptionEn.Text =
                labels.ContainsKey(key: "lblDescriptionEn") ?
                labels["lblDescriptionEn"] : String.Empty;

            btnOK.Text =
                labels.ContainsKey(key: "btnOK") ?
                labels["btnOK"] : String.Empty;

            btnCancel.Text =
                labels.ContainsKey(key: "btnCancel") ?
                labels["btnCancel"] : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "OK"</para>
        /// <para lang="en">Event handling the click on the "OK" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zrušit"</para>
        /// <para lang="en">Event handling the click on the "Cancel" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Zavření formuláře"</para>
        /// <para lang="en">Event handling form closing</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (FormTopicUpdate)</para>
        /// <para lang="en">Object that sends the event (FormTopicUpdate)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void FormTopicUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataTable topics = NfiEstaFunctions.FnEtlGetTopics.ExecuteQuery(
                FormUserPassword.TargetDatabase,
                TargetTransaction);

            if (this.DialogResult == DialogResult.OK)
            {
                string label = txtLabel.Text.Trim();
                string description = txtDescription.Text.Trim();
                string labelEn = txtLabelEn.Text.Trim();
                string descriptionEn = txtDescriptionEn.Text.Trim();

                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                if (String.IsNullOrEmpty(label) || String.IsNullOrEmpty(description) || String.IsNullOrEmpty(labelEn) || String.IsNullOrEmpty(descriptionEn))
                {
                    MessageBox.Show(
                         text:
                            messages.ContainsKey(key: "msgTextBoxIsEmpty") ?
                            messages["msgTextBoxIsEmpty"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (
                    topics.AsEnumerable().Any(a => a.Field<string>("label") == label &&
                               a.Field<string>("description") == description &&
                               a.Field<string>("label_en") == labelEn &&
                               a.Field<string>("description_en") == descriptionEn)
                    && !(label == _selectedLabel && description == _selectedDescription && labelEn == _selectedLabelEn && descriptionEn == _selectedDescriptionEn))
                {
                    //toto zabraňuje zápisu duplicitní kombinace hodnot z více sloupců do databázové tabulky
                    //this prevents writing duplicate combinations of values from multiple columns to the database table
                    MessageBox.Show(
                         text:
                            messages.ContainsKey(key: "msgDuplicateEntry") ?
                            messages["msgDuplicateEntry"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (
                    topics.AsEnumerable().Any(a => a.Field<string>("label") == label)
                    && !(label == _selectedLabel))
                {
                    MessageBox.Show(
                         text:
                            messages.ContainsKey(key: "msgDuplicateLabel") ?
                            messages["msgDuplicateLabel"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (
                    topics.AsEnumerable().Any(a => a.Field<string>("label_en") == labelEn)
                    && !(labelEn == _selectedLabelEn))
                {
                    MessageBox.Show(
                         text:
                            messages.ContainsKey(key: "msgDuplicateLabelEn") ?
                            messages["msgDuplicateLabelEn"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    NfiEstaFunctions.FnEtlUpdateTopic.Execute(
                    database: FormUserPassword.TargetDatabase,
                    _selectedId,
                    label: label,
                    description: description,
                    labelEn: labelEn,
                    descriptionEn: descriptionEn,
                    TargetTransaction);

                    e.Cancel = false;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Načtení dat do formuláře"</para>
        /// <para lang="en">Handling the "Loading data into form" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (FormTopicUpdate)</para>
        /// <para lang="en">Object that sends the event (FormTopicUpdate)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void FormTopicUpdate_Load(object sender, EventArgs e)
        {
            txtLabel.Text = _selectedLabel;
            txtDescription.Text = _selectedDescription;
            txtLabelEn.Text = _selectedLabelEn;
            txtDescriptionEn.Text = _selectedDescriptionEn;
        }

        #endregion Event Handlers

    }

}

