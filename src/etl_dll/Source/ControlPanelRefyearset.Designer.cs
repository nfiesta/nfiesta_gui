﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//


namespace Hanakova.ModuleEtl
{

    partial class ControlPanelRefyearset
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanelRefyearset));
            lblMainCaption = new System.Windows.Forms.Label();
            pnlButtons = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            btnNext = new System.Windows.Forms.Button();
            grpPanelsRefyearsets = new System.Windows.Forms.GroupBox();
            tlpPanelRefyearset = new System.Windows.Forms.TableLayoutPanel();
            dgvPanelsRefyearsets = new System.Windows.Forms.DataGridView();
            tsrPanelRefyearset = new System.Windows.Forms.ToolStrip();
            btnSelectAll = new System.Windows.Forms.ToolStripButton();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons.SuspendLayout();
            grpPanelsRefyearsets.SuspendLayout();
            tlpPanelRefyearset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvPanelsRefyearsets).BeginInit();
            tsrPanelRefyearset.SuspendLayout();
            tlpMain.SuspendLayout();
            SuspendLayout();
            // 
            // lblMainCaption
            // 
            lblMainCaption.AutoSize = true;
            lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblMainCaption.Location = new System.Drawing.Point(4, 0);
            lblMainCaption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblMainCaption.Name = "lblMainCaption";
            lblMainCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblMainCaption.Size = new System.Drawing.Size(1112, 35);
            lblMainCaption.TabIndex = 1;
            lblMainCaption.Text = "lblMainCaption";
            lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnPrevious);
            pnlButtons.Controls.Add(btnNext);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(4, 580);
            pnlButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(1112, 40);
            pnlButtons.TabIndex = 0;
            // 
            // btnPrevious
            // 
            btnPrevious.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnPrevious.Location = new System.Drawing.Point(679, 4);
            btnPrevious.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(247, 35);
            btnPrevious.TabIndex = 2;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.UseVisualStyleBackColor = true;
            btnPrevious.Click += BtnPrevious_Click;
            // 
            // btnNext
            // 
            btnNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnNext.Location = new System.Drawing.Point(933, 4);
            btnNext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(175, 35);
            btnNext.TabIndex = 1;
            btnNext.Text = "btnNext";
            btnNext.UseVisualStyleBackColor = true;
            btnNext.Click += BtnNext_Click;
            // 
            // grpPanelsRefyearsets
            // 
            grpPanelsRefyearsets.Controls.Add(tlpPanelRefyearset);
            grpPanelsRefyearsets.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanelsRefyearsets.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpPanelsRefyearsets.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpPanelsRefyearsets.Location = new System.Drawing.Point(4, 38);
            grpPanelsRefyearsets.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpPanelsRefyearsets.Name = "grpPanelsRefyearsets";
            grpPanelsRefyearsets.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpPanelsRefyearsets.Size = new System.Drawing.Size(1112, 536);
            grpPanelsRefyearsets.TabIndex = 0;
            grpPanelsRefyearsets.TabStop = false;
            // 
            // tlpPanelRefyearset
            // 
            tlpPanelRefyearset.ColumnCount = 1;
            tlpPanelRefyearset.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanelRefyearset.Controls.Add(dgvPanelsRefyearsets, 0, 1);
            tlpPanelRefyearset.Controls.Add(tsrPanelRefyearset, 0, 0);
            tlpPanelRefyearset.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpPanelRefyearset.Location = new System.Drawing.Point(4, 18);
            tlpPanelRefyearset.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpPanelRefyearset.Name = "tlpPanelRefyearset";
            tlpPanelRefyearset.RowCount = 2;
            tlpPanelRefyearset.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpPanelRefyearset.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpPanelRefyearset.Size = new System.Drawing.Size(1104, 515);
            tlpPanelRefyearset.TabIndex = 0;
            // 
            // dgvPanelsRefyearsets
            // 
            dgvPanelsRefyearsets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvPanelsRefyearsets.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvPanelsRefyearsets.Location = new System.Drawing.Point(4, 32);
            dgvPanelsRefyearsets.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvPanelsRefyearsets.Name = "dgvPanelsRefyearsets";
            dgvPanelsRefyearsets.Size = new System.Drawing.Size(1096, 480);
            dgvPanelsRefyearsets.TabIndex = 0;
            dgvPanelsRefyearsets.CellContentClick += DgvPanelsRefyearsets_CellContentClick;
            dgvPanelsRefyearsets.CellFormatting += DgvPanelsRefyearsets_CellFormatting;
            dgvPanelsRefyearsets.CellPainting += DgvPanelsRefyearsets_CellPainting;
            // 
            // tsrPanelRefyearset
            // 
            tsrPanelRefyearset.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnSelectAll });
            tsrPanelRefyearset.Location = new System.Drawing.Point(0, 0);
            tsrPanelRefyearset.Name = "tsrPanelRefyearset";
            tsrPanelRefyearset.Size = new System.Drawing.Size(1104, 25);
            tsrPanelRefyearset.TabIndex = 1;
            tsrPanelRefyearset.Text = "toolStrip1";
            // 
            // btnSelectAll
            // 
            btnSelectAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnSelectAll.ForeColor = System.Drawing.SystemColors.ControlText;
            btnSelectAll.Image = (System.Drawing.Image)resources.GetObject("btnSelectAll.Image");
            btnSelectAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSelectAll.Name = "btnSelectAll";
            btnSelectAll.Size = new System.Drawing.Size(74, 22);
            btnSelectAll.Text = "btnSelectAll";
            btnSelectAll.Click += BtnSelectAll_Click;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpPanelsRefyearsets, 0, 1);
            tlpMain.Controls.Add(pnlButtons, 0, 2);
            tlpMain.Controls.Add(lblMainCaption, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1120, 623);
            tlpMain.TabIndex = 2;
            // 
            // ControlPanelRefyearset
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(tlpMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "ControlPanelRefyearset";
            Size = new System.Drawing.Size(1120, 623);
            pnlButtons.ResumeLayout(false);
            grpPanelsRefyearsets.ResumeLayout(false);
            tlpPanelRefyearset.ResumeLayout(false);
            tlpPanelRefyearset.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dgvPanelsRefyearsets).EndInit();
            tsrPanelRefyearset.ResumeLayout(false);
            tsrPanelRefyearset.PerformLayout();
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.GroupBox grpPanelsRefyearsets;
        private System.Windows.Forms.DataGridView dgvPanelsRefyearsets;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        public System.Windows.Forms.Button btnNext;
        public System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.TableLayoutPanel tlpPanelRefyearset;
        private System.Windows.Forms.ToolStrip tsrPanelRefyearset;
        private System.Windows.Forms.ToolStripButton btnSelectAll;
    }

}
