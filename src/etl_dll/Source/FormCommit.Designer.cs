﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class FormCommit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpContent = new System.Windows.Forms.TableLayoutPanel();
            prgAdditivity = new System.Windows.Forms.ProgressBar();
            lblAdditivity = new System.Windows.Forms.Label();
            lblVariable = new System.Windows.Forms.Label();
            lblHierarchy = new System.Windows.Forms.Label();
            lblLdsity = new System.Windows.Forms.Label();
            prgVariable = new System.Windows.Forms.ProgressBar();
            prgHierarchy = new System.Windows.Forms.ProgressBar();
            prgLdsity = new System.Windows.Forms.ProgressBar();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            lblMainCaption = new System.Windows.Forms.Label();
            bgwVariable = new System.ComponentModel.BackgroundWorker();
            bgwHierarchy = new System.ComponentModel.BackgroundWorker();
            bgwLdsity = new System.ComponentModel.BackgroundWorker();
            tlpContent.SuspendLayout();
            tlpMain.SuspendLayout();
            SuspendLayout();
            // 
            // tlpContent
            // 
            tlpContent.ColumnCount = 2;
            tlpContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            tlpContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            tlpContent.Controls.Add(prgAdditivity, 1, 3);
            tlpContent.Controls.Add(lblAdditivity, 0, 3);
            tlpContent.Controls.Add(lblVariable, 0, 0);
            tlpContent.Controls.Add(lblHierarchy, 0, 1);
            tlpContent.Controls.Add(lblLdsity, 0, 2);
            tlpContent.Controls.Add(prgVariable, 1, 0);
            tlpContent.Controls.Add(prgHierarchy, 1, 1);
            tlpContent.Controls.Add(prgLdsity, 1, 2);
            tlpContent.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpContent.Location = new System.Drawing.Point(4, 39);
            tlpContent.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpContent.Name = "tlpContent";
            tlpContent.RowCount = 5;
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpContent.Size = new System.Drawing.Size(616, 125);
            tlpContent.TabIndex = 0;
            // 
            // prgAdditivity
            // 
            prgAdditivity.Dock = System.Windows.Forms.DockStyle.Fill;
            prgAdditivity.Location = new System.Drawing.Point(219, 90);
            prgAdditivity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            prgAdditivity.Name = "prgAdditivity";
            prgAdditivity.Size = new System.Drawing.Size(393, 23);
            prgAdditivity.TabIndex = 9;
            // 
            // lblAdditivity
            // 
            lblAdditivity.AutoSize = true;
            lblAdditivity.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAdditivity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblAdditivity.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblAdditivity.Location = new System.Drawing.Point(4, 87);
            lblAdditivity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblAdditivity.Name = "lblAdditivity";
            lblAdditivity.Size = new System.Drawing.Size(207, 29);
            lblAdditivity.TabIndex = 8;
            lblAdditivity.Text = "lblAdditivity";
            lblAdditivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVariable
            // 
            lblVariable.AutoSize = true;
            lblVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            lblVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblVariable.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblVariable.Location = new System.Drawing.Point(4, 0);
            lblVariable.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblVariable.Name = "lblVariable";
            lblVariable.Size = new System.Drawing.Size(207, 29);
            lblVariable.TabIndex = 0;
            lblVariable.Text = "lblVariable";
            lblVariable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHierarchy
            // 
            lblHierarchy.AutoSize = true;
            lblHierarchy.Dock = System.Windows.Forms.DockStyle.Fill;
            lblHierarchy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblHierarchy.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblHierarchy.Location = new System.Drawing.Point(4, 29);
            lblHierarchy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblHierarchy.Name = "lblHierarchy";
            lblHierarchy.Size = new System.Drawing.Size(207, 29);
            lblHierarchy.TabIndex = 1;
            lblHierarchy.Text = "lblHierarchy";
            lblHierarchy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLdsity
            // 
            lblLdsity.AutoSize = true;
            lblLdsity.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLdsity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLdsity.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblLdsity.Location = new System.Drawing.Point(4, 58);
            lblLdsity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblLdsity.Name = "lblLdsity";
            lblLdsity.Size = new System.Drawing.Size(207, 29);
            lblLdsity.TabIndex = 2;
            lblLdsity.Text = "lblLdsity";
            lblLdsity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // prgVariable
            // 
            prgVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            prgVariable.Location = new System.Drawing.Point(219, 3);
            prgVariable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            prgVariable.Name = "prgVariable";
            prgVariable.Size = new System.Drawing.Size(393, 23);
            prgVariable.TabIndex = 5;
            // 
            // prgHierarchy
            // 
            prgHierarchy.Dock = System.Windows.Forms.DockStyle.Fill;
            prgHierarchy.Location = new System.Drawing.Point(219, 32);
            prgHierarchy.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            prgHierarchy.Name = "prgHierarchy";
            prgHierarchy.Size = new System.Drawing.Size(393, 23);
            prgHierarchy.TabIndex = 6;
            // 
            // prgLdsity
            // 
            prgLdsity.Dock = System.Windows.Forms.DockStyle.Fill;
            prgLdsity.Location = new System.Drawing.Point(219, 61);
            prgLdsity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            prgLdsity.Name = "prgLdsity";
            prgLdsity.Size = new System.Drawing.Size(393, 23);
            prgLdsity.TabIndex = 7;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpContent, 0, 1);
            tlpMain.Controls.Add(lblMainCaption, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.6374283F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.36257F));
            tlpMain.Size = new System.Drawing.Size(624, 167);
            tlpMain.TabIndex = 1;
            // 
            // lblMainCaption
            // 
            lblMainCaption.AutoSize = true;
            lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblMainCaption.Location = new System.Drawing.Point(9, 0);
            lblMainCaption.Margin = new System.Windows.Forms.Padding(9, 0, 4, 0);
            lblMainCaption.Name = "lblMainCaption";
            lblMainCaption.Size = new System.Drawing.Size(611, 36);
            lblMainCaption.TabIndex = 1;
            lblMainCaption.Text = "lblMainCaption";
            lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bgwVariable
            // 
            bgwVariable.WorkerReportsProgress = true;
            bgwVariable.DoWork += BgwVariable_DoWork;
            bgwVariable.ProgressChanged += BgwVariable_ProgressChanged;
            bgwVariable.RunWorkerCompleted += BgwVariable_RunWorkerCompleted;
            // 
            // bgwHierarchy
            // 
            bgwHierarchy.WorkerReportsProgress = true;
            bgwHierarchy.DoWork += BgwHierarchy_DoWork;
            bgwHierarchy.ProgressChanged += BgwHierarchy_ProgressChanged;
            bgwHierarchy.RunWorkerCompleted += BgwHierarchy_RunWorkerCompleted;
            // 
            // bgwLdsity
            // 
            bgwLdsity.WorkerReportsProgress = true;
            bgwLdsity.DoWork += BgwLdsity_DoWork;
            bgwLdsity.ProgressChanged += BgwLdsity_ProgressChanged;
            bgwLdsity.RunWorkerCompleted += BgwLdsity_RunWorkerCompleted;
            // 
            // FormCommit
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(624, 167);
            Controls.Add(tlpMain);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormCommit";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            tlpContent.ResumeLayout(false);
            tlpContent.PerformLayout();
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpContent;
        private System.Windows.Forms.Label lblVariable;
        private System.Windows.Forms.Label lblHierarchy;
        private System.Windows.Forms.Label lblLdsity;
        private System.Windows.Forms.ProgressBar prgVariable;
        private System.Windows.Forms.ProgressBar prgHierarchy;
        private System.Windows.Forms.ProgressBar prgLdsity;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Label lblMainCaption;
        private System.ComponentModel.BackgroundWorker bgwVariable;
        private System.ComponentModel.BackgroundWorker bgwHierarchy;
        private System.ComponentModel.BackgroundWorker bgwLdsity;
        private System.Windows.Forms.ProgressBar prgAdditivity;
        private System.Windows.Forms.Label lblAdditivity;
    }

}