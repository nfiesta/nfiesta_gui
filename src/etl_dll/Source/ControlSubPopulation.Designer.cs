﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//


namespace Hanakova.ModuleEtl
{

    partial class ControlSubPopulation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            pnlButtons = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            lblMainCaption = new System.Windows.Forms.Label();
            splMain = new System.Windows.Forms.SplitContainer();
            splMiddlePanelWithButtons = new System.Windows.Forms.SplitContainer();
            splSubPopulation = new System.Windows.Forms.SplitContainer();
            grpSubPopulation = new System.Windows.Forms.GroupBox();
            dgvSubPopulation = new System.Windows.Forms.DataGridView();
            grpSubPopulationEtlIdNull = new System.Windows.Forms.GroupBox();
            dgvSubPopulationEtlIdNull = new System.Windows.Forms.DataGridView();
            tlpMiddlePanelWithButtons = new System.Windows.Forms.TableLayoutPanel();
            btnNoMatch = new System.Windows.Forms.Button();
            btnAuto = new System.Windows.Forms.Button();
            btnMatch = new System.Windows.Forms.Button();
            splSubPopulationCategory = new System.Windows.Forms.SplitContainer();
            grpSPC = new System.Windows.Forms.GroupBox();
            dgvSPC = new System.Windows.Forms.DataGridView();
            toolTip = new System.Windows.Forms.ToolTip(components);
            pnlButtons.SuspendLayout();
            tlpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMiddlePanelWithButtons).BeginInit();
            splMiddlePanelWithButtons.Panel1.SuspendLayout();
            splMiddlePanelWithButtons.Panel2.SuspendLayout();
            splMiddlePanelWithButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splSubPopulation).BeginInit();
            splSubPopulation.Panel1.SuspendLayout();
            splSubPopulation.Panel2.SuspendLayout();
            splSubPopulation.SuspendLayout();
            grpSubPopulation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvSubPopulation).BeginInit();
            grpSubPopulationEtlIdNull.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvSubPopulationEtlIdNull).BeginInit();
            tlpMiddlePanelWithButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splSubPopulationCategory).BeginInit();
            splSubPopulationCategory.Panel1.SuspendLayout();
            splSubPopulationCategory.SuspendLayout();
            grpSPC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvSPC).BeginInit();
            SuspendLayout();
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnNext);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(4, 580);
            pnlButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(1112, 40);
            pnlButtons.TabIndex = 0;
            // 
            // btnNext
            // 
            btnNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnNext.Enabled = false;
            btnNext.Location = new System.Drawing.Point(933, 4);
            btnNext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(175, 35);
            btnNext.TabIndex = 0;
            btnNext.Text = "btnNext";
            btnNext.UseVisualStyleBackColor = true;
            btnNext.Visible = false;
            btnNext.Click += BtnNext_Click;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 2);
            tlpMain.Controls.Add(lblMainCaption, 0, 0);
            tlpMain.Controls.Add(splMain, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1120, 623);
            tlpMain.TabIndex = 3;
            // 
            // lblMainCaption
            // 
            lblMainCaption.AutoSize = true;
            lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblMainCaption.Location = new System.Drawing.Point(4, 0);
            lblMainCaption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblMainCaption.Name = "lblMainCaption";
            lblMainCaption.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            lblMainCaption.Size = new System.Drawing.Size(1112, 35);
            lblMainCaption.TabIndex = 1;
            lblMainCaption.Text = "lblMainCaption";
            lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.Location = new System.Drawing.Point(4, 38);
            splMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splMain.Name = "splMain";
            splMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(splMiddlePanelWithButtons);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(splSubPopulationCategory);
            splMain.Size = new System.Drawing.Size(1112, 536);
            splMain.SplitterDistance = 284;
            splMain.SplitterWidth = 5;
            splMain.TabIndex = 2;
            // 
            // splMiddlePanelWithButtons
            // 
            splMiddlePanelWithButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            splMiddlePanelWithButtons.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            splMiddlePanelWithButtons.Location = new System.Drawing.Point(0, 0);
            splMiddlePanelWithButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splMiddlePanelWithButtons.Name = "splMiddlePanelWithButtons";
            splMiddlePanelWithButtons.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMiddlePanelWithButtons.Panel1
            // 
            splMiddlePanelWithButtons.Panel1.Controls.Add(splSubPopulation);
            // 
            // splMiddlePanelWithButtons.Panel2
            // 
            splMiddlePanelWithButtons.Panel2.Controls.Add(tlpMiddlePanelWithButtons);
            splMiddlePanelWithButtons.Size = new System.Drawing.Size(1112, 284);
            splMiddlePanelWithButtons.SplitterDistance = 235;
            splMiddlePanelWithButtons.SplitterWidth = 5;
            splMiddlePanelWithButtons.TabIndex = 0;
            // 
            // splSubPopulation
            // 
            splSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            splSubPopulation.Location = new System.Drawing.Point(0, 0);
            splSubPopulation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splSubPopulation.Name = "splSubPopulation";
            // 
            // splSubPopulation.Panel1
            // 
            splSubPopulation.Panel1.Controls.Add(grpSubPopulation);
            // 
            // splSubPopulation.Panel2
            // 
            splSubPopulation.Panel2.Controls.Add(grpSubPopulationEtlIdNull);
            splSubPopulation.Panel2Collapsed = true;
            splSubPopulation.Size = new System.Drawing.Size(1112, 235);
            splSubPopulation.SplitterDistance = 413;
            splSubPopulation.SplitterWidth = 5;
            splSubPopulation.TabIndex = 0;
            // 
            // grpSubPopulation
            // 
            grpSubPopulation.Controls.Add(dgvSubPopulation);
            grpSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSubPopulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpSubPopulation.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSubPopulation.Location = new System.Drawing.Point(0, 0);
            grpSubPopulation.Name = "grpSubPopulation";
            grpSubPopulation.Size = new System.Drawing.Size(1112, 235);
            grpSubPopulation.TabIndex = 0;
            grpSubPopulation.TabStop = false;
            // 
            // dgvSubPopulation
            // 
            dgvSubPopulation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvSubPopulation.Location = new System.Drawing.Point(3, 18);
            dgvSubPopulation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvSubPopulation.Name = "dgvSubPopulation";
            dgvSubPopulation.Size = new System.Drawing.Size(1106, 214);
            dgvSubPopulation.TabIndex = 2;
            dgvSubPopulation.CellBeginEdit += DgvSubPopulation_CellBeginEdit;
            dgvSubPopulation.CellContentClick += DgvSubPopulation_CellContentClick;
            dgvSubPopulation.CellPainting += DgvSubPopulation_CellPainting;
            dgvSubPopulation.CellToolTipTextNeeded += dgvSubPopulation_CellToolTipTextNeeded;
            dgvSubPopulation.CellValueChanged += DgvSubPopulation_CellValueChanged;
            dgvSubPopulation.CurrentCellDirtyStateChanged += DgvSubPopulation_CurrentCellDirtyStateChanged;
            dgvSubPopulation.SelectionChanged += DgvSubPopulation_SelectionChanged;
            // 
            // grpSubPopulationEtlIdNull
            // 
            grpSubPopulationEtlIdNull.Controls.Add(dgvSubPopulationEtlIdNull);
            grpSubPopulationEtlIdNull.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSubPopulationEtlIdNull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpSubPopulationEtlIdNull.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSubPopulationEtlIdNull.Location = new System.Drawing.Point(0, 0);
            grpSubPopulationEtlIdNull.Name = "grpSubPopulationEtlIdNull";
            grpSubPopulationEtlIdNull.Size = new System.Drawing.Size(96, 100);
            grpSubPopulationEtlIdNull.TabIndex = 0;
            grpSubPopulationEtlIdNull.TabStop = false;
            // 
            // dgvSubPopulationEtlIdNull
            // 
            dgvSubPopulationEtlIdNull.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvSubPopulationEtlIdNull.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvSubPopulationEtlIdNull.Location = new System.Drawing.Point(3, 18);
            dgvSubPopulationEtlIdNull.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvSubPopulationEtlIdNull.Name = "dgvSubPopulationEtlIdNull";
            dgvSubPopulationEtlIdNull.ReadOnly = true;
            dgvSubPopulationEtlIdNull.Size = new System.Drawing.Size(90, 79);
            dgvSubPopulationEtlIdNull.TabIndex = 0;
            dgvSubPopulationEtlIdNull.CellPainting += DgvSubPopulationEtlIdNull_CellPainting;
            dgvSubPopulationEtlIdNull.CellToolTipTextNeeded += dgvSubPopulationEtlIdNull_CellToolTipTextNeeded;
            dgvSubPopulationEtlIdNull.SelectionChanged += dgvSubPopulationEtlIdNull_SelectionChanged;
            // 
            // tlpMiddlePanelWithButtons
            // 
            tlpMiddlePanelWithButtons.ColumnCount = 5;
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpMiddlePanelWithButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            tlpMiddlePanelWithButtons.Controls.Add(btnNoMatch, 3, 0);
            tlpMiddlePanelWithButtons.Controls.Add(btnAuto, 1, 0);
            tlpMiddlePanelWithButtons.Controls.Add(btnMatch, 2, 0);
            tlpMiddlePanelWithButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMiddlePanelWithButtons.Location = new System.Drawing.Point(0, 0);
            tlpMiddlePanelWithButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpMiddlePanelWithButtons.Name = "tlpMiddlePanelWithButtons";
            tlpMiddlePanelWithButtons.RowCount = 1;
            tlpMiddlePanelWithButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMiddlePanelWithButtons.Size = new System.Drawing.Size(1112, 44);
            tlpMiddlePanelWithButtons.TabIndex = 0;
            // 
            // btnNoMatch
            // 
            btnNoMatch.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnNoMatch.Enabled = false;
            btnNoMatch.Location = new System.Drawing.Point(627, 5);
            btnNoMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnNoMatch.Name = "btnNoMatch";
            btnNoMatch.Size = new System.Drawing.Size(175, 33);
            btnNoMatch.TabIndex = 4;
            btnNoMatch.Text = "btnNoMatch";
            btnNoMatch.UseVisualStyleBackColor = true;
            btnNoMatch.Click += btnNoMatch_Click;
            // 
            // btnAuto
            // 
            btnAuto.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnAuto.Location = new System.Drawing.Point(253, 5);
            btnAuto.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnAuto.Name = "btnAuto";
            btnAuto.Size = new System.Drawing.Size(175, 33);
            btnAuto.TabIndex = 3;
            btnAuto.Text = "btnAuto";
            btnAuto.UseVisualStyleBackColor = true;
            btnAuto.Click += btnAuto_Click;
            // 
            // btnMatch
            // 
            btnMatch.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnMatch.Enabled = false;
            btnMatch.Location = new System.Drawing.Point(440, 5);
            btnMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnMatch.Name = "btnMatch";
            btnMatch.Size = new System.Drawing.Size(175, 33);
            btnMatch.TabIndex = 2;
            btnMatch.Text = "btnMatch";
            btnMatch.UseVisualStyleBackColor = true;
            btnMatch.Click += BtnMatch_Click;
            // 
            // splSubPopulationCategory
            // 
            splSubPopulationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            splSubPopulationCategory.Location = new System.Drawing.Point(0, 0);
            splSubPopulationCategory.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splSubPopulationCategory.Name = "splSubPopulationCategory";
            // 
            // splSubPopulationCategory.Panel1
            // 
            splSubPopulationCategory.Panel1.AutoScroll = true;
            splSubPopulationCategory.Panel1.Controls.Add(grpSPC);
            // 
            // splSubPopulationCategory.Panel2
            // 
            splSubPopulationCategory.Panel2.AutoScroll = true;
            splSubPopulationCategory.Panel2.BackColor = System.Drawing.SystemColors.Window;
            splSubPopulationCategory.Size = new System.Drawing.Size(1112, 247);
            splSubPopulationCategory.SplitterDistance = 412;
            splSubPopulationCategory.SplitterWidth = 5;
            splSubPopulationCategory.TabIndex = 0;
            // 
            // grpSPC
            // 
            grpSPC.Controls.Add(dgvSPC);
            grpSPC.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSPC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            grpSPC.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpSPC.Location = new System.Drawing.Point(0, 0);
            grpSPC.Name = "grpSPC";
            grpSPC.Size = new System.Drawing.Size(412, 247);
            grpSPC.TabIndex = 0;
            grpSPC.TabStop = false;
            // 
            // dgvSPC
            // 
            dgvSPC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvSPC.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvSPC.Location = new System.Drawing.Point(3, 18);
            dgvSPC.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvSPC.Name = "dgvSPC";
            dgvSPC.ReadOnly = true;
            dgvSPC.Size = new System.Drawing.Size(406, 226);
            dgvSPC.TabIndex = 0;
            dgvSPC.CellToolTipTextNeeded += DgvSPC_CellToolTipTextNeeded;
            // 
            // ControlSubPopulation
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(tlpMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "ControlSubPopulation";
            Size = new System.Drawing.Size(1120, 623);
            pnlButtons.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            splMiddlePanelWithButtons.Panel1.ResumeLayout(false);
            splMiddlePanelWithButtons.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMiddlePanelWithButtons).EndInit();
            splMiddlePanelWithButtons.ResumeLayout(false);
            splSubPopulation.Panel1.ResumeLayout(false);
            splSubPopulation.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splSubPopulation).EndInit();
            splSubPopulation.ResumeLayout(false);
            grpSubPopulation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvSubPopulation).EndInit();
            grpSubPopulationEtlIdNull.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvSubPopulationEtlIdNull).EndInit();
            tlpMiddlePanelWithButtons.ResumeLayout(false);
            splSubPopulationCategory.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splSubPopulationCategory).EndInit();
            splSubPopulationCategory.ResumeLayout(false);
            grpSPC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvSPC).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlButtons;

        /// <summary>
        /// btnNext
        /// </summary>
        public System.Windows.Forms.Button btnNext;

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.SplitContainer splMiddlePanelWithButtons;
        private System.Windows.Forms.SplitContainer splSubPopulation;
        private System.Windows.Forms.DataGridView dgvSubPopulation;
        private System.Windows.Forms.DataGridView dgvSubPopulationEtlIdNull;

        /// <summary>
        /// btnMatch
        /// </summary>
        public System.Windows.Forms.Button btnMatch;

        private System.Windows.Forms.SplitContainer splSubPopulationCategory;
        private System.Windows.Forms.DataGridView dgvSPC;

        /// <summary>
        /// btnAuto
        /// </summary>
        public System.Windows.Forms.Button btnAuto;

        /// <summary>
        /// btnNoMatch
        /// </summary>
        public System.Windows.Forms.Button btnNoMatch;

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TableLayoutPanel tlpMiddlePanelWithButtons;
        private System.Windows.Forms.GroupBox grpSubPopulation;
        private System.Windows.Forms.GroupBox grpSubPopulationEtlIdNull;
        private System.Windows.Forms.GroupBox grpSPC;
    }

}