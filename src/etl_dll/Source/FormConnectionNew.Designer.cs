﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class FormConnectionNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpConnection = new System.Windows.Forms.GroupBox();
            tlpConnection = new System.Windows.Forms.TableLayoutPanel();
            lblComment = new System.Windows.Forms.Label();
            lblPort = new System.Windows.Forms.Label();
            lblDbName = new System.Windows.Forms.Label();
            lblHost = new System.Windows.Forms.Label();
            txtComment = new System.Windows.Forms.TextBox();
            txtPort = new System.Windows.Forms.TextBox();
            txtDbName = new System.Windows.Forms.TextBox();
            txtHost = new System.Windows.Forms.TextBox();
            pnlButtons = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            btnOK = new System.Windows.Forms.Button();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            grpConnection.SuspendLayout();
            tlpConnection.SuspendLayout();
            pnlButtons.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(542, 204);
            pnlMain.TabIndex = 7;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpConnection, 0, 0);
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.Size = new System.Drawing.Size(542, 204);
            tlpMain.TabIndex = 5;
            // 
            // grpConnection
            // 
            grpConnection.Controls.Add(tlpConnection);
            grpConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpConnection.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpConnection.Location = new System.Drawing.Point(4, 3);
            grpConnection.Margin = new System.Windows.Forms.Padding(0);
            grpConnection.Name = "grpConnection";
            grpConnection.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpConnection.Size = new System.Drawing.Size(534, 152);
            grpConnection.TabIndex = 6;
            grpConnection.TabStop = false;
            grpConnection.Text = "grpConnection";
            // 
            // tlpConnection
            // 
            tlpConnection.ColumnCount = 2;
            tlpConnection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            tlpConnection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpConnection.Controls.Add(lblComment, 0, 3);
            tlpConnection.Controls.Add(lblPort, 0, 2);
            tlpConnection.Controls.Add(lblDbName, 0, 1);
            tlpConnection.Controls.Add(lblHost, 0, 0);
            tlpConnection.Controls.Add(txtComment, 1, 3);
            tlpConnection.Controls.Add(txtPort, 1, 2);
            tlpConnection.Controls.Add(txtDbName, 1, 1);
            tlpConnection.Controls.Add(txtHost, 1, 0);
            tlpConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpConnection.Location = new System.Drawing.Point(4, 18);
            tlpConnection.Margin = new System.Windows.Forms.Padding(0);
            tlpConnection.Name = "tlpConnection";
            tlpConnection.RowCount = 5;
            tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpConnection.Size = new System.Drawing.Size(526, 131);
            tlpConnection.TabIndex = 2;
            // 
            // lblComment
            // 
            lblComment.Dock = System.Windows.Forms.DockStyle.Fill;
            lblComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblComment.Location = new System.Drawing.Point(0, 75);
            lblComment.Margin = new System.Windows.Forms.Padding(0);
            lblComment.Name = "lblComment";
            lblComment.Size = new System.Drawing.Size(140, 25);
            lblComment.TabIndex = 3;
            lblComment.Text = "lblComment";
            lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPort
            // 
            lblPort.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblPort.Location = new System.Drawing.Point(0, 50);
            lblPort.Margin = new System.Windows.Forms.Padding(0);
            lblPort.Name = "lblPort";
            lblPort.Size = new System.Drawing.Size(140, 25);
            lblPort.TabIndex = 2;
            lblPort.Text = "lblPort";
            lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDbName
            // 
            lblDbName.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblDbName.Location = new System.Drawing.Point(0, 25);
            lblDbName.Margin = new System.Windows.Forms.Padding(0);
            lblDbName.Name = "lblDbName";
            lblDbName.Size = new System.Drawing.Size(140, 25);
            lblDbName.TabIndex = 1;
            lblDbName.Text = "lblDbName";
            lblDbName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHost
            // 
            lblHost.Dock = System.Windows.Forms.DockStyle.Fill;
            lblHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblHost.Location = new System.Drawing.Point(0, 0);
            lblHost.Margin = new System.Windows.Forms.Padding(0);
            lblHost.Name = "lblHost";
            lblHost.Size = new System.Drawing.Size(140, 25);
            lblHost.TabIndex = 0;
            lblHost.Text = "lblHost";
            lblHost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtComment
            // 
            txtComment.Dock = System.Windows.Forms.DockStyle.Fill;
            txtComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtComment.Location = new System.Drawing.Point(140, 75);
            txtComment.Margin = new System.Windows.Forms.Padding(0);
            txtComment.Name = "txtComment";
            txtComment.Size = new System.Drawing.Size(386, 20);
            txtComment.TabIndex = 7;
            // 
            // txtPort
            // 
            txtPort.Dock = System.Windows.Forms.DockStyle.Fill;
            txtPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtPort.Location = new System.Drawing.Point(140, 50);
            txtPort.Margin = new System.Windows.Forms.Padding(0);
            txtPort.Name = "txtPort";
            txtPort.Size = new System.Drawing.Size(386, 20);
            txtPort.TabIndex = 6;
            // 
            // txtDbName
            // 
            txtDbName.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtDbName.Location = new System.Drawing.Point(140, 25);
            txtDbName.Margin = new System.Windows.Forms.Padding(0);
            txtDbName.Name = "txtDbName";
            txtDbName.Size = new System.Drawing.Size(386, 20);
            txtDbName.TabIndex = 5;
            // 
            // txtHost
            // 
            txtHost.Dock = System.Windows.Forms.DockStyle.Fill;
            txtHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtHost.Location = new System.Drawing.Point(140, 0);
            txtHost.Margin = new System.Windows.Forms.Padding(0);
            txtHost.Name = "txtHost";
            txtHost.Size = new System.Drawing.Size(386, 20);
            txtHost.TabIndex = 4;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnCancel);
            pnlButtons.Controls.Add(btnOK);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(4, 155);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(534, 46);
            pnlButtons.TabIndex = 4;
            // 
            // btnCancel
            // 
            btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Location = new System.Drawing.Point(172, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 35);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += BtnCancel_Click;
            // 
            // btnOK
            // 
            btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Location = new System.Drawing.Point(353, 6);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(175, 35);
            btnOK.TabIndex = 0;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += BtnOK_Click;
            // 
            // FormConnectionNew
            // 
            AcceptButton = btnOK;
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(542, 204);
            Controls.Add(pnlMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormConnectionNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormConnectionNew";
            FormClosing += FormConnectionNew_FormClosing;
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            grpConnection.ResumeLayout(false);
            tlpConnection.ResumeLayout(false);
            tlpConnection.PerformLayout();
            pnlButtons.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpConnection;
        private System.Windows.Forms.TableLayoutPanel tlpConnection;
        private System.Windows.Forms.Label lblComment;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblDbName;
        private System.Windows.Forms.Label lblHost;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtDbName;
        private System.Windows.Forms.TextBox txtHost;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }

}