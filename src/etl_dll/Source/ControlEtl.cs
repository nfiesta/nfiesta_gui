﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;
using ZaJi.PostgreSQL.DataModel;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Modul pro ETL"</para>
    /// <para lang="en">Control "Module for the ETL"</para>
    /// </summary>
    public partial class ControlEtl
            : UserControl, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Databázové tabulky</para>
        /// <para lang="en">Database tables</para>
        /// </summary>
        private NfiEstaDB database;

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        private LanguageVersion languageVersion;

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        private LanguageFile languageFile;

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        private Setting setting;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Připojení do cílové databáze"</para>
        /// <para lang="en">Control "Connection to the target database"</para>
        /// </summary>
        private ControlConnection ctrConnection;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Synchronizace výběrového designu"</para>
        /// <para lang="en">Control "Sampling design synchronization"</para>
        /// </summary>
        private ControlSDesignSync ctrSDesignSync;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
        /// <para lang="en">Selection of the target variable"</para>
        /// </summary>
        private ControlTargetVariable ctrTargetVariable;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou"</para>
        /// <para lang="en">Control "Comparison of the selected target variable with target variables being already in Estimates"</para>
        /// </summary>
        private ControlTargetVariableCompare ctrTargetVariableCompare;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba kombinací panelů a refyearsetů"</para>
        /// <para lang="en">Control "Select panel and refyearset combinations"</para>
        /// </summary>
        private ControlPanelRefyearset ctrPanelRefyearset;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL plošné domény a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of area domain and its catgories"</para>
        /// </summary>
        private ControlAreaDomain ctrAreaDomain;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL subpopulace a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of subpopulation and its catgories"</para>
        /// </summary>
        private ControlSubPopulation ctrSubPopulation;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Kontrola atributových proměnných"</para>
        /// <para lang="en">Control "Checking attribute variables"</para>
        /// </summary>
        private ControlAttributeVariables ctrAttributeVariables;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Téma"</para>
        /// <para lang="en">Control "Topic"</para>
        /// </summary>
        private ControlTopic ctrTopic;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        public ControlEtl()
        {
            InitializeComponent();
            Initialize(
                controlOwner: null,
                languageVersion: LanguageVersion.International,
                languageFile: new LanguageFile(),
                setting: new Setting());
        }

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </param>
        public ControlEtl(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                languageVersion: languageVersion,
                languageFile: languageFile,
                setting: setting);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                return controlOwner;
            }
            set
            {
                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return languageVersion;
            }
            set
            {
                languageVersion = value;
                Setting.DBSetting.LanguageVersion = LanguageVersion;
                Database.Postgres.Setting = Setting.DBSetting;

                InitializeLabels();

                if (ctrTargetVariable != null)
                {
                    LanguageChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return languageFile ??
                    new LanguageFile();
            }
            set
            {
                languageFile = value ??
                    new LanguageFile();

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return setting ??
                    new Setting();
            }
            set
            {
                setting = value ??
                    new Setting();
                Database.Postgres.Setting = Setting.DBSetting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Připojení do cílové databáze"</para>
        /// <para lang="en">Control "Connection to the target database"</para>
        /// </summary>
        internal ControlConnection CtrConnection
        {
            get
            {
                return ctrConnection ??
                    throw new ArgumentNullException(
                        message: "ControlConnection is null.",
                        paramName: "ControlConnection");
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Synchronizace výběrového designu"</para>
        /// <para lang="en">Control "Sampling design synchronization"</para>
        /// </summary>
        internal ControlSDesignSync CtrSDesignSync
        {
            get
            {
                return ctrSDesignSync ??
                    throw new ArgumentNullException(
                        message: "ControlSDesignSync is null.",
                        paramName: "ControlSDesignSync");
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
        /// <para lang="en">Selection of the target variable"</para>
        /// </summary>
        internal ControlTargetVariable CtrTargetVariable
        {
            get
            {
                return ctrTargetVariable ??
                    throw new ArgumentNullException(
                        message: "ControlTargetVariable is null.",
                        paramName: "ControlTargetVariable");
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou"</para>
        /// <para lang="en">Control "Comparison of the selected target variable with target variables being already in Estimates"</para>
        /// </summary>
        internal ControlTargetVariableCompare CtrTargetVariableCompare
        {
            get
            {
                return ctrTargetVariableCompare;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba kombinací panelů a refyearsetů"</para>
        /// <para lang="en">Control "Select panel and refyearset combinations"</para>
        /// </summary>
        internal ControlPanelRefyearset CtrPanelRefyearset
        {
            get
            {
                return ctrPanelRefyearset ??
                    throw new ArgumentNullException(
                        message: "ControlPanelRefyearset is null.",
                        paramName: "ControlPanelRefyearset");
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL plošné domény a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of area domain and its catgories"</para>
        /// </summary>
        internal ControlAreaDomain CtrAreaDomain
        {
            get
            {
                return ctrAreaDomain ??
                    throw new ArgumentNullException(
                        message: "ControlAreaDomain is null.",
                        paramName: "ControlAreaDomain");
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL subpopulace a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of subpopulation and its catgories"</para>
        /// </summary>
        internal ControlSubPopulation CtrSubPopulation
        {
            get
            {
                return ctrSubPopulation ??
                    throw new ArgumentNullException(
                        message: "ControlSubPopulation is null.",
                        paramName: "ControlSubPopulation");
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Kontrola atributových proměnných"</para>
        /// <para lang="en">Control "Checking attribute variables"</para>
        /// </summary>
        internal ControlAttributeVariables CtrAttributeVariables
        {
            get
            {
                return ctrAttributeVariables ??
                    throw new ArgumentNullException(
                        message: "ControlAttributeVariables is null.",
                        paramName: "ControlAttributeVariables");
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Téma"</para>
        /// <para lang="en">Control "Topic"</para>
        /// </summary>
        internal ControlTopic CtrTopic
        {
            get
            {
                return ctrTopic ??
                    throw new ArgumentNullException(
                        message: "ControlTopic is null.",
                        paramName: "ControlTopic");
            }
        }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                                { "lblStatus",                      "$1" },
                                { "lblStatusExportConnection",      "Cílová databáze: $1" },
                                { "tsmiDatabase",                   "Databáze" },
                                { "tsmiConnect",                    "Připojit" },
                                { "tsmiTargetDataExtension",        "Data extenze nfiesta_target_data" },
                                { "msgReset",                       "Nastavení modulu pro přenos dat bylo resetováno na výchozí hodnoty." },
                                { "msgNoDatabaseConnection" ,       "Aplikace není připojena k databázi." },
                                { "msgNoTargetDataDbExtension" ,    "V databázi není nainstalovaná extenze $1." },
                                { "msgNoTargetDataDbSchema",        "V databázi neexistuje schéma $1." },
                                { "msgInvalidTargetDataDbVersion",  "Nainstalovaná verze databázové extenze $1 $2 se neshoduje s očekávanou verzí $3." },
                                { "msgValidTargetDataDbVersion",    "Databázová extenze $1 $2 [$3]." },
                                { "msgDatabaseExceptionReceived",   "Databáze hlásí chybu, nelze dál pokračovat v přenosu dat. Modul bude uzavřen, kontaktujte vývojáře." },
                                { "btnCloseToolTip",                "Zavřít modul" },
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlEtl") ?
                            languageFile.NationalVersion.Data["ControlEtl"] :
                            new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                                { "lblStatus",                      "$1" },
                                { "lblStatusExportConnection",      "Target database: $1" },
                                { "tsmiDatabase",                   "Database" },
                                { "tsmiConnect",                    "Connect" },
                                { "tsmiTargetDataExtension",        "Data from extension nfiesta_target_data"},
                                { "msgReset",                       "Data transfer module settings have been reset to default values." },
                                { "msgNoDatabaseConnection" ,       "Application is not connected to database." },
                                { "msgNoTargetDataDbExtension" ,    "Database extension $1 is not installed." },
                                { "msgNoTargetDataDbSchema",        "Schema $1 does not exist in the database." },
                                { "msgInvalidTargetDataDbVersion",  "Installed version of the database extension $1 $2 does not correspond with expected version $3." },
                                { "msgValidTargetDataDbVersion",    "Database extension $1 $2 [$3]." },
                                { "msgDatabaseExceptionReceived",   "The database reports an error, the data transfer cannot continue. The module will be closed, contact the developers." },
                                { "btnCloseToolTip",                "Close the module" },
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlEtl") ?
                            languageFile.InternationalVersion.Data["ControlEtl"] :
                            new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting)
        {
            // Controls
            // Pozn: Při inicializaci objektu je třeba Controls nastavit na null,
            // tj. před vytvořením objektu připojení k databázi,
            // jinak v nich dochází k přístupu k databázi, ke které ještě neexistuje připojení
            // Note: Controls must be set to null when initializing the object,
            // i.e. before creating the database connection object,
            // otherwise they access a database that is not yet connected
            ctrConnection = null;
            ctrSDesignSync = null;
            ctrTargetVariable = null;
            ctrTargetVariableCompare = null;
            ctrPanelRefyearset = null;
            ctrAreaDomain = null;
            ctrSubPopulation = null;
            ctrAttributeVariables = null;
            ctrTopic = null;
            pnlControls.Controls.Clear();

            // ControlOwner
            ControlOwner = controlOwner;

            // Database
            database = new NfiEstaDB();
            Database.Postgres.StayConnected = true;

            Database.Postgres.ConnectionError += Db_OnConnectionError;
            Database.Postgres.ExceptionReceived += Db_OnExceptionReceived;

            // LanguageVersion
            LanguageVersion = languageVersion;

            // LanguageFile
            LanguageFile = languageFile;

            // Setting
            Setting = setting;

            ssrMain.Visible = Setting.StatusStripVisible;

#if DEBUG
            tsmiSeparator.Visible = true;
            tsmiTargetDataExtension.Visible = true;
#endif

#if RELEASE
            tsmiSeparator.Visible = false;
            tsmiTargetDataExtension.Visible = false;
#endif
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing module control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            lblStatus.Text =
               (labels.ContainsKey(key: "lblStatus") ?
               labels["lblStatus"] : String.Empty)
               .Replace(
                   oldValue: "$1",
                   newValue: Database.Postgres.ConnectionInfo());

            if (!Database.Postgres.Initialized)
            {
                lblStatusExportConnection.Text = String.Empty;
            }
            else
            {
                lblStatusExportConnection.Text =
                    (labels.ContainsKey(key: "lblStatusExportConnection") ?
                    labels["lblStatusExportConnection"] : String.Empty)
                    .Replace(
                        oldValue: "$1",
                        newValue: TargetDBConnectionInfo.LastConnectionInfo);/*Database.Postgres.ConnectionInfo());*/
            }

            tsmiDatabase.Text =
                labels.ContainsKey(key: "tsmiDatabase") ?
                labels["tsmiDatabase"] : String.Empty;

            tsmiConnect.Text =
                labels.ContainsKey(key: "tsmiConnect") ?
                labels["tsmiConnect"] : String.Empty;

            tsmiTargetDataExtension.Text =
                labels.ContainsKey(key: "tsmiTargetDataExtension") ?
                labels["tsmiTargetDataExtension"] : String.Empty;

            string btnCloseToolTipText =
                labels.ContainsKey("btnCloseToolTip") ?
                labels["btnCloseToolTip"] : String.Empty;
            tipClose.SetToolTip(btnClose, btnCloseToolTipText);

            ctrConnection?.InitializeLabels();
            ctrSDesignSync?.InitializeLabels();
            ctrTargetVariable?.InitializeLabels();
            ctrTargetVariableCompare?.InitializeLabels();
            ctrPanelRefyearset?.InitializeLabels();
            ctrAreaDomain?.InitializeLabels();
            ctrSubPopulation?.InitializeLabels();
            ctrAttributeVariables?.InitializeLabels();
            ctrTopic?.InitializeLabels();

            DisplayTargetDataExtensionStatus();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Resetuje nastavení modulu pro ETL na výchozí hodnoty</para>
        /// <para lang="en">Resets ETL module settings to default values</para>
        /// </summary>
        /// <param name="displayMessage">
        /// <para lang="cs">Zobrazení zprávy o provedeném resetu nastavení modulu</para>
        /// <para lang="en">Display a message about the reset of the module settings</para>
        /// </param>
        public void Reset(bool displayMessage)
        {
            Setting = new Setting();

            if (displayMessage)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.ContainsKey(key: "msgReset") ?
                        messages["msgReset"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Připojit k databázi</para>
        /// <para lang="en">Connect to a database</para>
        /// </summary>
        private void Connect()
        {
            Disconnect();
            FormUserPassword.Disconnect();
            Initialize(
                controlOwner: ControlOwner,
                languageVersion: LanguageVersion,
                languageFile: LanguageFile,
                setting: Setting);

            Database.Postgres.Setting = Setting.DBSetting;
            Database.Connect(
                 applicationName: Setting.DBSetting.ApplicationName);
            Setting.DBSetting = Database.Postgres.Setting;

            pnlControls.Controls.Clear();

            if (DisplayTargetDataExtensionStatus())
            {
                ctrConnection = new ControlConnection(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };

                CtrConnection.NextClick += ControlConnection_Next;

                CtrConnection.LoadContent();

                pnlControls.Controls.Add(value: CtrConnection);

                //tsmiDatabase.Visible = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Odpojit od databáze</para>
        /// <para lang="en">Disconnect from database</para>
        /// </summary>
        public void Disconnect()
        {
            if (Database != null)
            {
                Database.Postgres?.CloseConnection();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stav instalace extenze nfiesta_target_data</para>
        /// <para lang="en">Method displays installation status of the database extension nfiesta_target_data</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud v připojené databázi je nainstalovaná extenze nfiesta_target_data
        /// v kompatibilní verzi s aplikací. Jinak false.</para>
        /// <para lang="en">It returns true, when there is installed database extension nfiesta_target_data
        /// in compatible version with application.</para>
        /// </returns>
        private bool DisplayTargetDataExtensionStatus()
        {
            Dictionary<string, string> messages = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            lblStatus.Text = Database.Postgres.ConnectionInfo();

            // Není připojeno k databázi
            // Nou connected to database
            if (!Database.Postgres.Initialized)
            {
                lblTargetDataExtension.Text = String.Empty;
                return false;
            }

            // Extenze neexistuje
            // Extension does not exist
            if (Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName] == null)
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text =
                    (messages.ContainsKey(key: "msgNoTargetDataDbExtension") ?
                     messages["msgNoTargetDataDbExtension"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName);
                return false;
            }

            // Extenze existuje
            // Extension exists
            ExtensionVersion dbExtensionVersion = new ExtensionVersion(
                version: Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Version);

            // Schéma neexistuje
            // Schema does not exist
            if (Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Schema == null)
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text =
                    (messages.ContainsKey(key: "msgNoTargetDataDbSchema") ?
                     messages["msgNoTargetDataDbSchema"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: TDSchema.Name);
                return false;
            }

            // Minor version does not match
            //
            if (!TDSchema.ExtensionVersion.MinorEquals(obj: dbExtensionVersion))
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text =
                    (messages.ContainsKey(key: "msgInvalidTargetDataDbVersion") ?
                     messages["msgInvalidTargetDataDbVersion"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: TDSchema.ExtensionVersion.ToString());
                return false;
            }

            // Neshoduje se patch verze
            // Patch version does not match
            if (!TDSchema.ExtensionVersion.Equals(obj: dbExtensionVersion))
            {
                lblTargetDataExtension.ForeColor = System.Drawing.Color.Red;
                lblTargetDataExtension.Text =
                    (messages.ContainsKey(key: "msgInvalidTargetDataDbVersion") ?
                     messages["msgInvalidTargetDataDbVersion"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[TDSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: TDSchema.ExtensionVersion.ToString());
                return true;
            }

            // Extenze je OK
            // Extension is OK
            lblTargetDataExtension.ForeColor = System.Drawing.SystemColors.ControlText;
            lblTargetDataExtension.Text =
                 (messages.ContainsKey(key: "msgValidTargetDataDbVersion") ?
                     messages["msgValidTargetDataDbVersion"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: TDSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: TDSchema.ExtensionVersion.ToString())
                    .Replace(oldValue: "$3", newValue: TDSchema.Name);
            return true;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí nebo skryje stavový řádek</para>
        /// <para lang="en">Shows or hides status strip</para>
        /// </summary>
        public void DisplayStatusStrip()
        {
            if (Setting.StatusStripVisible)
            {
                ShowStatusStrip();
            }
            else
            {
                HideStatusStrip();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Show status strip</para>
        /// </summary>
        public void ShowStatusStrip()
        {
            Setting.StatusStripVisible = true;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Hide status strip</para>
        /// </summary>
        public void HideStatusStrip()
        {
            Setting.StatusStripVisible = false;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_target_data</para>
        /// <para lang="en">Displays data from extension nfiesta_target_data"</para>
        /// </summary>
        private void TargetDataExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                MessageBox.Show(
                    text:
                        messages.ContainsKey(key: "msgNoDatabaseConnection") ?
                        messages["msgNoDatabaseConnection"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            List<DBStoredProcedure> storedProcedures =
                Database.Postgres.Catalog
                    .Schemas[TDSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)
                    .ToList<DBStoredProcedure>();

            FormTargetData frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Připojení do cílové databáze"</para>
        /// <para lang="en">Displays the control following
        /// the control "Connection to the target database"</para>
        /// </summary>
        private void ControlConnection_ShowNext()
        {
            //originally, ControlSDesignSync visible

            //if (ctrConnection != null)
            //{
            //    ctrSDesignSync = new ControlSDesignSync(
            //        controlOwner: this,
            //        selectedExportConnectionId: ctrConnection.SelectedExportConnectionId)
            //    {
            //        Dock = DockStyle.Fill,
            //    };

            //    CtrSDesignSync.PreviousClick += ControlSDesignSync_Previous;
            //    CtrSDesignSync.NextClick += ControlSDesignSync_Next;

            //    CtrSDesignSync.LoadContent();

            //    pnlControls.Controls.Clear();
            //    pnlControls.Controls.Add(value: CtrSDesignSync);;
            //}

            if (ctrConnection != null)
            {
                ctrTargetVariable = new ControlTargetVariable(
                    controlOwner: this,
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId) //originally ControlSDesignSync,  ControlSDesignSync visible
                {
                    Dock = DockStyle.Fill
                };

                CtrTargetVariable.PreviousClick += ControlTargetVariable_Previous;
                CtrTargetVariable.NextClick += ControlTargetVariable_Next;

                CtrTargetVariable.LoadContent();

                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrTargetVariable);
            }
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Synchronizace výběrového designu"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Sampling design synchronization"</para>
        /// </summary>
        private void ControlSDesignSync_ShowPrevious()
        {
            if (ctrConnection != null)
            {
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrConnection);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Synchronizace výběrového designu"</para>
        /// <para lang="en">Displays the control following
        /// the control "Sampling design synchronization"</para>
        /// </summary>
        private void ControlSDesignSync_ShowNext()
        {
            if (ctrSDesignSync != null)
            {
                ctrTargetVariable = new ControlTargetVariable(
                    controlOwner: this,
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId) //originally ControlSDesignSync,  ControlSDesignSync visible
                {
                    Dock = DockStyle.Fill
                };

                CtrTargetVariable.PreviousClick += ControlTargetVariable_Previous;
                CtrTargetVariable.NextClick += ControlTargetVariable_Next;

                CtrTargetVariable.LoadContent();

                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrTargetVariable);
            }
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Volba cílové proměnné"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Selection of the target variable"</para>
        /// </summary>
        private void ControlTargetVariable_ShowPrevious()
        {
            //originally, ControlSDesignSync visible

            //if (ctrSDesignSync != null)
            //{
            //    pnlControls.Controls.Clear();
            //    pnlControls.Controls.Add(value: CtrSDesignSync);
            //}

            if (ctrConnection != null)
            {
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrConnection);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba cílové proměnné"</para>
        /// <para lang="en">Displays the control following
        /// the control "Selection of the target variable"</para>
        /// </summary>
        private void ControlTargetVariable_ShowNext()
        {
            if (ctrTargetVariable != null)
            {
                SelectedTargetVariableValues selectedTargetVariableValues = new SelectedTargetVariableValues()
                {
                    SelectedTargetVariableId = CtrTargetVariable.SelectedTargetVariableId,
                    SelectedIndicatorLabel = CtrTargetVariable.SelectedIndicatorLabel,
                    SelectedStateOrChange = CtrTargetVariable.SelectedStateOrChange,
                    SelectedUnit = CtrTargetVariable.SelectedUnit,
                    SelectedLocalDensity = CtrTargetVariable.SelectedLocalDensity,
                    SelectedVersion = CtrTargetVariable.SelectedVersion,
                    SelectedDefinitionVariant = CtrTargetVariable.SelectedDefinitionVariant,
                    SelectedAreaDomain = CtrTargetVariable.SelectedAreaDomain,
                    SelectedPopulation = CtrTargetVariable.SelectedPopulation,
                };

                ctrTargetVariableCompare = new ControlTargetVariableCompare(
                    controlOwner: this,
                    selectedTargetVariableValues: selectedTargetVariableValues,
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId,
                    selectedIndicatorLabelCs: CtrTargetVariable.SelectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: CtrTargetVariable.SelectedIndicatorLabelEn,
                    sourceTransaction: CtrTargetVariable.SourceTransaction,
                    targetTransaction: CtrTargetVariable.TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                CtrTargetVariableCompare.PreviousClick += ControlTargetVariableCompare_Previous;
                CtrTargetVariableCompare.NextClick += ControlTargetVariableCompare_Next;

                CtrTargetVariableCompare.LoadContent();
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrTargetVariableCompare);
            }
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek předcházející
        /// ovládacímu prvku "Volba cílové proměnné"</para>
        /// <para lang="en">Displays the control preceding
        /// the control "Selection of the target variable"</para>
        /// </summary>
        private void ControlTargetVariableCompare_ShowPrevious()
        {
            if (ctrTargetVariable != null)
            {
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrTargetVariable);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou"</para>
        /// <para lang="en">Displays the control following
        /// the control "Comparison of the selected target variable with target variables being already in Estimates"</para>
        /// </summary>
        private void ControlTargetVariableCompare_ShowNext()
        {
            if (ctrTargetVariableCompare != null)
            {
                ctrPanelRefyearset = new ControlPanelRefyearset(
                    controlOwner: this,
                    idEtlFromTargetVariable: CtrTargetVariable.IdEtlFromControlTargetVariable,
                    idEtlFromTargetVariableCompare: CtrTargetVariableCompare.IdEtlFromControlTargetVariableCompare,
                    idEtl: null,
                    refYearSetToPanelMapping: null,
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId,
                    atomicAreaDomains: CtrTargetVariable.AtomicAreaDomains,
                    atomicSubPopulations: CtrTargetVariable.AtomicSubPopulations,
                    selectedIndicatorLabelCs: CtrTargetVariable.SelectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: CtrTargetVariable.SelectedIndicatorLabelEn,
                    sourceTransaction: CtrTargetVariableCompare.SourceTransaction,
                    targetTransaction: CtrTargetVariableCompare.TargetTransaction
                    )
                {
                    Dock = DockStyle.Fill,
                    FromTargetVariableCompare = true
                };

                CtrPanelRefyearset.NextClick += ControlPanelRefyearset_Next;

                CtrPanelRefyearset.LoadContent();
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrPanelRefyearset);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Volba kombinací panelů a roků měření"</para>
        /// <para lang="en">Displays the control following
        /// the control "Selection of panel and refyearset combinations"</para>
        /// </summary>
        private void ControlPanelRefyearset_ShowNext()
        {
            if (ctrPanelRefyearset != null)
            {
                ctrAreaDomain = new ControlAreaDomain(
                    controlOwner: this,
                    idEtl: CtrPanelRefyearset.EtlId,
                    refYearSetToPanelMapping: CtrPanelRefyearset.SelectedRefyearsetsToPanels,
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId,
                    atomicAreaDomains: CtrTargetVariable.AtomicAreaDomains,
                    atomicSubPopulations: CtrTargetVariable.AtomicSubPopulations,
                    selectedIndicatorLabelCs: CtrTargetVariable.SelectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: CtrTargetVariable.SelectedIndicatorLabelEn,
                    sourceTransaction: CtrPanelRefyearset.SourceTransaction,
                    targetTransaction: CtrPanelRefyearset.TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                CtrAreaDomain.NextClick += ControlAreaDomain_Next;

                CtrAreaDomain.LoadContent();
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrAreaDomain);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "ETL plošné domény a jejích kategorií"</para>
        /// <para lang="en">Displays the control following
        /// the control "ETL of area domain and its categories"</para>
        /// </summary>
        private void ControlAreaDomain_ShowNext()
        {
            if (ctrAreaDomain != null)
            {
                ctrSubPopulation = new ControlSubPopulation(
                    controlOwner: this,
                    idEtl: CtrPanelRefyearset.EtlId, //is this correct, should not be there controlAreaDomain...?
                    refYearSetToPanelMapping: CtrPanelRefyearset.SelectedRefyearsetsToPanels, //is this correct, should not be there controlAreaDomain...?
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId,
                    atomicAreaDomains: CtrTargetVariable.AtomicAreaDomains,
                    atomicSubPopulations: CtrTargetVariable.AtomicSubPopulations,
                    selectedIndicatorLabelCs: CtrTargetVariable.SelectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: CtrTargetVariable.SelectedIndicatorLabelEn,
                    sourceTransaction: CtrAreaDomain.SourceTransaction,
                    targetTransaction: CtrAreaDomain.TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                CtrSubPopulation.NextClick += ControlSubPopulation_Next;

                CtrSubPopulation.LoadContent();
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrSubPopulation);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "ETL subpopulace a jejích kategorií"</para>
        /// <para lang="en">Displays the control following
        /// the control "ETL of subpopulation and its categories"</para>
        /// </summary>
        private void ControlSubPopulation_ShowNext()
        {
            if (ctrSubPopulation != null)
            {
                ctrAttributeVariables = new ControlAttributeVariables(
                    controlOwner: this,
                    idEtl: CtrPanelRefyearset.EtlId,
                    refYearSetToPanelMapping: CtrPanelRefyearset.SelectedRefyearsetsToPanels,
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId,
                    selectedIndicatorLabelCs: CtrTargetVariable.SelectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: CtrTargetVariable.SelectedIndicatorLabelEn,
                    sourceTransaction: CtrSubPopulation.SourceTransaction,
                    targetTransaction: CtrSubPopulation.TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                CtrAttributeVariables.NextClick += ControlAttributeVariables_Next;

                CtrAttributeVariables.LoadContent();
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrAttributeVariables);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek následující
        /// ovládacímu prvku "Kontrola atributových proměnných"</para>
        /// <para lang="en">Displays the control following
        /// the control "Checking attribute variables"</para>
        /// </summary>
        private void ControlAttributeVariables_ShowNext()
        {
            if (ctrTopic != null)
            {
                ctrTopic = new ControlTopic(
                    controlOwner: this,
                    idEtl: CtrPanelRefyearset.EtlId,
                    refYearSetToPanelMapping: CtrAttributeVariables.SelectedRefyearsetsToPanels,
                    selectedExportConnectionId: CtrConnection.SelectedExportConnectionId,
                    selectedIndicatorLabelCs: CtrTargetVariable.SelectedIndicatorLabelCs,
                    selectedIndicatorLabelEn: CtrTargetVariable.SelectedIndicatorLabelEn,
                    sourceTransaction: CtrAttributeVariables.SourceTransaction,
                    targetTransaction: CtrAttributeVariables.TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                CtrTopic.LoadContent();
                pnlControls.Controls.Clear();
                pnlControls.Controls.Add(value: CtrTopic);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek</para>
        /// <para lang="en">Displays a control</para>
        /// </summary>
        public void ShowControl(Control control)
        {
            control.Dock = DockStyle.Fill;
            pnlControls.Controls.Clear();
            pnlControls.Controls.Add(value: control);
        }

        #endregion Methods


        #region Menu Application - Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Připojit"</para>
        /// <para lang="en">Handling the "Connect" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripMenuItem)</para>
        /// <para lang="en">Object that sends the event (ToolStripMenuItem)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ItemConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Data extenze nfiesta_target_data"</para>
        /// <para lang="en">Handling the "Data from extension nfiesta_target_data" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripMenuItem)</para>
        /// <para lang="en">Object that sends the event (ToolStripMenuItem)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ItemTargetDataExtension_Click(object sender, EventArgs e)
        {
            TargetDataExtension();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zavřít"</para>
        /// <para lang="en">Handling the "Close" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnClose_Click(object sender, EventArgs e)
        {
            pnlControls.Controls.Clear();
            Disconnect();
            FormUserPassword.Disconnect();
            lblStatusExportConnection.Visible = false;
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "Připojení do cílové databáze"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "Connection to the target database"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlConnection_Next(object sender, EventArgs e)
        {
            ControlConnection_ShowNext();
        }


        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Předchozí"
        /// v ovládacím prvku "Synchronizace výběrového designu"</para>
        /// <para lang="en">Handling the "Previous" button click event
        /// in the control "Sampling design synchronization"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlSDesignSync_Previous(object sender, EventArgs e)
        {
            ControlSDesignSync_ShowPrevious();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "Synchronizace výběrového designu"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "Sampling design synchronization"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlSDesignSync_Next(object sender, EventArgs e)
        {
            ControlSDesignSync_ShowNext();
        }


        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Předchozí"
        /// v ovládacím prvku "Volba cílové proměnné"</para>
        /// <para lang="en">Handling the "Previous" button click event
        /// in the control "Selection of the target variable"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlTargetVariable_Previous(object sender, EventArgs e)
        {
            ControlTargetVariable_ShowPrevious();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "Volba cílové proměnné"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "Selection of the target variable"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlTargetVariable_Next(object sender, EventArgs e)
        {
            ControlTargetVariable_ShowNext();
        }


        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Předchozí"
        /// v ovládacím prvku "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou"</para>
        /// <para lang="en">Handling the "Previous" button click event
        /// in the control "Comparison of the selected target variable with target variables being already in Estimates"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlTargetVariableCompare_Previous(object sender, EventArgs e)
        {
            ControlTargetVariableCompare_ShowPrevious();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "Comparison of the selected target variable with target variables being already in Estimates"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlTargetVariableCompare_Next(object sender, EventArgs e)
        {
            ControlTargetVariableCompare_ShowNext();
        }


        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "Volba kombinací panelů a refyearsetů"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "Selection of panel and refyearset combination"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlPanelRefyearset_Next(object sender, EventArgs e)
        {
            ControlPanelRefyearset_ShowNext();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "ETL plošné domény a jejích kategorií"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "ETL of area domain and its categories"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlAreaDomain_Next(object sender, EventArgs e)
        {
            ControlAreaDomain_ShowNext();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "ETL subpopulace a jejích kategorií"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "ETL of subpopulation and its categories"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlSubPopulation_Next(object sender, EventArgs e)
        {
            ControlSubPopulation_ShowNext();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"
        /// v ovládacím prvku "Kontrola atributových proměnných"</para>
        /// <para lang="en">Handling the "Next" button click event
        /// in the control "Checking attribute variables"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void ControlAttributeVariables_Next(object sender, EventArgs e)
        {
            ControlAttributeVariables_ShowNext();
        }

        #endregion Menu Application - Event Handlers


        #region Database - Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události "Chyba při připojení k databázi"</para>
        /// <para lang="en">Handling the "Database connection error" event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (PostgreSQLWrapper)</para>
        /// <para lang="en">Object that sends the event (PostgreSQLWrapper)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Db_OnConnectionError(object sender, ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionEventArgs e)
        {
            MessageBox.Show(
                text: e.ToString(),
                caption: e.MessageBoxCaption,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Error);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události "Chyba v SQL příkazu"</para>
        /// <para lang="en">Handling the event "Error in SQL statement"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (PostgreSQLWrapper)</para>
        /// <para lang="en">Object that sends the event (PostgreSQLWrapper)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void Db_OnExceptionReceived(object sender, ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionEventArgs e)
        {
            DialogResult result = MessageBox.Show(
                        text: e.ToString(),
                        caption: e.MessageBoxCaption,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Error);

            if (e.ToString().Contains("Error 03: fn_etl_try_delete_export_connection"))
            {
                return;
            }

            if (result == DialogResult.OK)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                DialogResult dialog = MessageBox.Show(
                                        text:
                                            messages.ContainsKey(key: "msgDatabaseExceptionReceived") ?
                                            messages["msgDatabaseExceptionReceived"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.OK,
                                        icon: MessageBoxIcon.Information);

                if (dialog == DialogResult.OK)
                {
                    if (SourceTransaction != null)
                    {
                        SourceTransaction.Rollback();
                    }
                    if (TargetTransaction != null)
                    {
                        TargetTransaction.Rollback();
                    }

                    if (pnlControls.InvokeRequired)
                    {
                        pnlControls.Invoke((MethodInvoker)delegate
                        {
                            pnlControls.Controls.Clear();
                        });
                    }
                    else
                    {
                        pnlControls.Controls.Clear();
                    }

                    Form formCommit = Application.OpenForms["FormCommit"];
                    if (formCommit != null)
                    {
                        if (formCommit.InvokeRequired)
                        {
                            formCommit.Invoke((MethodInvoker)delegate
                            {
                                formCommit.Close();
                            });
                        }
                        else
                        {
                            formCommit.Close();
                        }
                    }

                    Form formCheckAdSp = Application.OpenForms["FormCheckAdSp"];
                    if (formCheckAdSp != null) { formCheckAdSp.Close(); }

                    Form formCheckMetadata = Application.OpenForms["FormCheckMetadata"];
                    if (formCheckMetadata != null) { formCheckMetadata.Close(); }

                    Form formEtledTargetVariables = Application.OpenForms["FormEtledTargetVariables"];
                    if (formEtledTargetVariables != null) { formEtledTargetVariables.Close(); }

                    Form formEtledTVCheckMetadata = Application.OpenForms["FormEtledTVCheckMetadata"];
                    if (formEtledTVCheckMetadata != null) { formEtledTVCheckMetadata.Close(); }

                    Disconnect();
                    FormUserPassword.Disconnect();
                    lblStatusExportConnection.Visible = false;
                }
            }
        }

        #endregion Database - Event Handlers


        #region Events

        /// <summary>
        /// <para lang="cs">Událost vyvolaná při změně jazyka aplikace.</para>
        /// <para lang="en">Event raised when the application's language changes.</para>
        /// </summary>
        public static event EventHandler LanguageChanged;

        #endregion Events
    }

}
