﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class FormEtledTargetVariables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.tsmiLanguage = new System.Windows.Forms.ToolStripMenuItem();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnCheckSelected = new System.Windows.Forms.Button();
            this.btnCheckAll = new System.Windows.Forms.Button();
            this.cmbIdEtlTargetVariable = new System.Windows.Forms.ComboBox();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.pnlTargetVariableSelector = new System.Windows.Forms.Panel();
            this.menuStrip.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLanguage});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(944, 24);
            this.menuStrip.TabIndex = 6;
            this.menuStrip.Text = "menuStrip";
            // 
            // tsmiLanguage
            // 
            this.tsmiLanguage.Name = "tsmiLanguage";
            this.tsmiLanguage.Size = new System.Drawing.Size(94, 20);
            this.tsmiLanguage.Text = "tsmiLanguage";
            this.tsmiLanguage.Click += new System.EventHandler(this.TsmiLanguage_Click);
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.AutoSize = true;
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(3, 20);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(938, 1);
            this.lblMainCaption.TabIndex = 1;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnCheckSelected);
            this.pnlButtons.Controls.Add(this.btnCheckAll);
            this.pnlButtons.Controls.Add(this.cmbIdEtlTargetVariable);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(3, 464);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(938, 34);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnCheckSelected
            // 
            this.btnCheckSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckSelected.Location = new System.Drawing.Point(626, 0);
            this.btnCheckSelected.Name = "btnCheckSelected";
            this.btnCheckSelected.Size = new System.Drawing.Size(150, 30);
            this.btnCheckSelected.TabIndex = 3;
            this.btnCheckSelected.Text = "btnCheckSelected";
            this.btnCheckSelected.UseVisualStyleBackColor = true;
            this.btnCheckSelected.Click += new System.EventHandler(this.BtnCheckSelected_Click);
            // 
            // btnCheckAll
            // 
            this.btnCheckAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckAll.Location = new System.Drawing.Point(782, 0);
            this.btnCheckAll.Name = "btnCheckAll";
            this.btnCheckAll.Size = new System.Drawing.Size(150, 30);
            this.btnCheckAll.TabIndex = 2;
            this.btnCheckAll.Text = "btnCheckAll";
            this.btnCheckAll.UseVisualStyleBackColor = true;
            this.btnCheckAll.Click += new System.EventHandler(this.BtnCheckAll_Click);
            // 
            // cmbIdEtlTargetVariable
            // 
            this.cmbIdEtlTargetVariable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIdEtlTargetVariable.FormattingEnabled = true;
            this.cmbIdEtlTargetVariable.Location = new System.Drawing.Point(9, 10);
            this.cmbIdEtlTargetVariable.Name = "cmbIdEtlTargetVariable";
            this.cmbIdEtlTargetVariable.Size = new System.Drawing.Size(56, 21);
            this.cmbIdEtlTargetVariable.TabIndex = 17;
            this.cmbIdEtlTargetVariable.Visible = false;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.pnlButtons, 0, 3);
            this.tlpMain.Controls.Add(this.lblMainCaption, 0, 1);
            this.tlpMain.Controls.Add(this.pnlTargetVariableSelector, 0, 2);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 4;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(944, 501);
            this.tlpMain.TabIndex = 5;
            // 
            // pnlTargetVariableSelector
            // 
            this.pnlTargetVariableSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTargetVariableSelector.Location = new System.Drawing.Point(3, 24);
            this.pnlTargetVariableSelector.Name = "pnlTargetVariableSelector";
            this.pnlTargetVariableSelector.Size = new System.Drawing.Size(938, 434);
            this.pnlTargetVariableSelector.TabIndex = 2;
            // 
            // FormEtledTargetVariables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.tlpMain);
            this.MinimizeBox = false;
            this.Name = "FormEtledTargetVariables";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiLanguage;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.Panel pnlButtons;

        /// <summary>
        /// btnCheckSelected
        /// </summary>
        public System.Windows.Forms.Button btnCheckSelected;

        /// <summary>
        /// btnCheckAll
        /// </summary>
        public System.Windows.Forms.Button btnCheckAll;


        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.ComboBox cmbIdEtlTargetVariable;
        private System.Windows.Forms.Panel pnlTargetVariableSelector;
    }

}