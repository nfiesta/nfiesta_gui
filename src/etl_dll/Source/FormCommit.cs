﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;
//using System.IO;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Formulář pro uložení do databáze</para>
    /// <para lang="en">Form to save to the database</para>
    /// </summary>
    public partial class FormCommit
            : Form, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        private readonly int? _idEtl;

        /// <summary>
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů, který vrací fce fn_etl_get_target_variable</para>
        /// <para lang="en">List of panel and refyearset combination ids returned by fn_etl_get_target_variable</para>
        /// </summary>
        private readonly List<int?> _refYearSetToPanelMapping;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Kontruktor formuláře pro uložení do databáze</para>
        /// <para lang="en">Constructor of the save to the database form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="idEtl">
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="refYearSetToPanelMapping">
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů, který vrací fce fn_etl_get_target_variable</para>
        /// <para lang="en">List of panel and refyearset combination ids returned by fn_etl_get_target_variable</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public FormCommit(
            Control controlOwner,
            int? idEtl,
            List<int?> refYearSetToPanelMapping,
            int selectedExportConnectionId,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction)
        {
            InitializeComponent();

            _idEtl = idEtl;
            _refYearSetToPanelMapping = refYearSetToPanelMapping;
            _selectedExportConnectionId = selectedExportConnectionId;
            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;

            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is ControlTopic))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlTopic.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is ControlTopic))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlTopic.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((ControlTopic)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((ControlTopic)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ControlTopic)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ControlTopic)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlTopic ctrTopic)
                {
                    return ctrTopic.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",         String.Empty },
                            { "lblMainCaption",     "Přenos dat" },
                            { "lblVariable",        "Kategorizace proměnné:" },
                            { "lblHierarchy",       "Kategorizační hierarchie:" },
                            { "lblLdsity",          "Hodnoty na plochách:" },
                            { "lblAdditivity",      "Kontrola aditivity:" },
                            { "msgAdditivityError", "Zjištěna chyba při kontrole aditivity, proces přenosu dat byl neúspěšný a bude ukončen." },
                            { "msgEtlDone",         "Kontrola aditivity byla úspěšně provedena, proces přenosu dat je nyní kompletní a bude ukončen." },
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "FormCommit") ?
                    languageFile.NationalVersion.Data["FormCommit"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",         String.Empty },
                            { "lblMainCaption",     "Data transfer" },
                            { "lblVariable",        "Variable categorisations:" },
                            { "lblHierarchy",       "Categorisation hierarchies:" },
                            { "lblLdsity",          "Plot values:" },
                            { "lblAdditivity",      "Check additivity:" },
                            { "msgAdditivityError", "An additivity check error was detected, the data transfer process failed and will be closed." },
                            { "msgEtlDone",         "The additivity check has been successfully performed, the data transfer process is now complete and will be closed." },

                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "FormCommit") ?
                    languageFile.InternationalVersion.Data["FormCommit"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro uložení do databáze</para>
        /// <para lang="en">Initializing the save to the database form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro uložení do databáze</para>
        /// <para lang="en">Initializing the labels of the save to the database form</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text =
               labels.ContainsKey(key: "frmCaption") ?
               labels["frmCaption"] : String.Empty;

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            lblVariable.Text =
                labels.ContainsKey(key: "lblVariable") ?
                labels["lblVariable"] : String.Empty;

            lblHierarchy.Text =
                labels.ContainsKey(key: "lblHierarchy") ?
                labels["lblHierarchy"] : String.Empty;

            lblLdsity.Text =
                labels.ContainsKey(key: "lblLdsity") ?
                labels["lblLdsity"] : String.Empty;

            lblAdditivity.Text =
                labels.ContainsKey(key: "lblAdditivity") ?
                labels["lblAdditivity"] : String.Empty;

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Spuštění 1. páru funkcí (1. progressbar)</para>
        /// <para lang="en">Loading the 1st pair of functions (1. progressbar)</para>
        /// </summary>
        public void LoadContent()
        {
            if (!bgwVariable.IsBusy)
            {
                prgVariable.Style = ProgressBarStyle.Marquee;
                bgwVariable.RunWorkerAsync();
            }
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když je započata práce na pozadí pro přenos atributového členění</para>
        /// <para lang="en">Handling the event that is triggered when the transfer of attribute breakdowns starts in the background.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwVariable_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;

            string variables =
                TDFunctions.FnEtlExportVariable.Execute(
                    database: Database,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    idTEtlTargetVariable: _idEtl,
                    transaction: SourceTransaction);

            NfiEstaFunctions.FnEtlImportVariable.Execute(
                FormUserPassword.TargetDatabase,
                variables,
                TargetTransaction);

            worker.ReportProgress(100);

        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když se změní pokrok práce na pozadí pro přenos atributových členění</para>
        /// <para lang="en">Handling the event that is triggered when the progress of the attribute breakdown transfer changes in the background.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwVariable_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                prgVariable.Style = ProgressBarStyle.Blocks;
                prgVariable.Value = prgVariable.Maximum;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když je dokončena práce na pozadí pro přenos atributových členění</para>
        /// <para lang="en">Handling the event that is triggered when the background work for the transfer of the attribute breakdowns is completed.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwVariable_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            prgHierarchy.Style = ProgressBarStyle.Marquee;
            bgwHierarchy.RunWorkerAsync();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když je započata práce na pozadí pro přenos hierarchie atributového členění</para>
        /// <para lang="en">Handling the event that is triggered when the transfer of attribute breakdown hierarchy starts in the background.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwHierarchy_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;

            string variables =
                TDFunctions.FnEtlExportVariableHierarchy.Execute(
                    database: Database,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    idTEtlTargetVariable: _idEtl,
                    transaction: SourceTransaction);

            if (String.IsNullOrEmpty(variables))
            {
                variables = "[null]";
            }

            NfiEstaFunctions.FnEtlImportVariableHierarchy.Execute(
                FormUserPassword.TargetDatabase,
                variables,
                TargetTransaction);

            worker.ReportProgress(100);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když se změní pokrok práce na pozadí pro přenos hierarchií atributových členění</para>
        /// <para lang="en">Handling the event that is triggered when the progress of the attribute breakdown hierarchy transfer changes in the background.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwHierarchy_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                prgHierarchy.Style = ProgressBarStyle.Blocks;
                prgHierarchy.Value = prgHierarchy.Maximum;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když je dokončena práce na pozadí pro přenos hierarchií atributových členění</para>
        /// <para lang="en">Handling the event that is triggered when the background work for the transfer of the attribute breakdown hierarchies is completed.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwHierarchy_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            prgLdsity.Style = ProgressBarStyle.Marquee;
            bgwLdsity.RunWorkerAsync();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když je započata práce na pozadí pro přenos lokálních hustot a datasetů</para>
        /// <para lang="en">Handling the event that is triggered when the transfer of local densities and datasets starts in the background.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwLdsity_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;

            foreach (var refYear in _refYearSetToPanelMapping)
            {
                List<int?> singleRefYearList = new List<int?>() { refYear };

                string availableDataSetsAndLdsityValues =
                    TDFunctions.FnEtlExportLDsityValues.Execute(
                        database: Database,
                        refYearSetToPanelMapping: singleRefYearList,
                        idTEtlTargetVariable: _idEtl,
                        transaction: SourceTransaction);

                NfiEstaFunctions.FnEtlImportLDsityValues.Execute(
                    FormUserPassword.TargetDatabase,
                    availableDataSetsAndLdsityValues,
                    TargetTransaction);

                worker.ReportProgress((int)((_refYearSetToPanelMapping.IndexOf(refYear) + 1) / (double)_refYearSetToPanelMapping.Count * 100));
            }

            worker.ReportProgress(100);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když se změní pokrok práce na pozadí pro přenos lokálních hustot a datasetů</para>
        /// <para lang="en">Handling the event that is triggered when the progress of the local density and dataset transfer changes in the background.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BgwLdsity_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                prgLdsity.Style = ProgressBarStyle.Blocks;
                prgLdsity.Value = prgLdsity.Maximum;
                System.Media.SystemSounds.Exclamation.Play();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události, která se spustí, když je dokončena práce na pozadí pro přenos lokálních hustot a datasetů</para>
        /// <para lang="en">Handling the event that is triggered when the background work for the transfer of the local densities and datasets is completed.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (BackgroundWorker)</para>
        /// <para lang="en">Object that sends the event (BackgroundWorker)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private async void BgwLdsity_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!Database.Postgres.ExceptionFlag || !FormUserPassword.TargetDatabase.Postgres.ExceptionFlag)
            {
                TDFunctions.FnEtlSaveLog.Execute(Database, _refYearSetToPanelMapping, _idEtl, SourceTransaction);

                prgAdditivity.Style = ProgressBarStyle.Marquee; // Set style to Marquee to make it animated

                Exception caughtException = null;

                await Task.Run(() =>
                {
                    try
                    {
                        TargetTransaction.Commit();
                        SourceTransaction.Commit();
                    }
                    catch (Npgsql.PostgresException ex)
                    {
                        // Additivity check can only fail on the target database
                        SourceTransaction.Rollback();
                        caughtException = ex;
                    }
                });

                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                if (caughtException != null)
                {
                    MessageBox.Show(
                        text: messages.ContainsKey(key: "msgAdditivityError") ?
                                messages["msgAdditivityError"] : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    this.Close();

                    ControlTopic controlTopic = this.ControlOwner as ControlTopic;

                    if (controlTopic != null && controlTopic.Parent != null)
                    {
                        controlTopic.Parent.Controls.Remove(controlTopic);
                        controlTopic.Dispose();
                    }

                    ControlTargetVariable controlTargetVariable = new ControlTargetVariable(
                    controlOwner: this,
                    selectedExportConnectionId: _selectedExportConnectionId)
                    {
                        Dock = DockStyle.Fill
                    };
                    controlTargetVariable.LoadContent();
                    controlTargetVariable.InitializeLabels();
                    CtrEtl?.ShowControl(control: controlTargetVariable);
                }
                else
                {
                    prgAdditivity.Style = ProgressBarStyle.Blocks; // Change the style to Blocks to indicate completion
                    prgAdditivity.Value = prgAdditivity.Maximum; // Set the progress bar to full

                    MessageBox.Show(
                        text: messages.ContainsKey(key: "msgEtlDone") ?
                                messages["msgEtlDone"] : String.Empty,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    this.Close();

                    ControlTopic controlTopic = this.ControlOwner as ControlTopic;

                    if (controlTopic != null && controlTopic.Parent != null)
                    {
                        controlTopic.Parent.Controls.Remove(controlTopic);
                        controlTopic.Dispose();
                    }

                    ControlTargetVariable controlTargetVariable = new ControlTargetVariable(
                    controlOwner: this,
                    selectedExportConnectionId: _selectedExportConnectionId)
                    {
                        Dock = DockStyle.Fill
                    };
                    controlTargetVariable.LoadContent();
                    controlTargetVariable.InitializeLabels();
                    CtrEtl?.ShowControl(control: controlTargetVariable);
                }
            }
        }


        #endregion Event Handlers

    }

}
