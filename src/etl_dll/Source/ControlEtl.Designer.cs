﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class ControlEtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            pnlMain = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            pnlControls = new System.Windows.Forms.Panel();
            ssrMain = new System.Windows.Forms.StatusStrip();
            lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            lblTargetDataExtension = new System.Windows.Forms.ToolStripStatusLabel();
            lblStatusExportConnection = new System.Windows.Forms.ToolStripStatusLabel();
            lblNfiestaExtension = new System.Windows.Forms.ToolStripStatusLabel();
            lblSDesignExtension = new System.Windows.Forms.ToolStripStatusLabel();
            mnsMain = new System.Windows.Forms.MenuStrip();
            tsmiDatabase = new System.Windows.Forms.ToolStripMenuItem();
            tsmiConnect = new System.Windows.Forms.ToolStripMenuItem();
            tsmiSeparator = new System.Windows.Forms.ToolStripSeparator();
            tsmiTargetDataExtension = new System.Windows.Forms.ToolStripMenuItem();
            tipClose = new System.Windows.Forms.ToolTip(components);
            pnlMain.SuspendLayout();
            ssrMain.SuspendLayout();
            mnsMain.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.Color.WhiteSmoke;
            pnlMain.Controls.Add(btnClose);
            pnlMain.Controls.Add(pnlControls);
            pnlMain.Controls.Add(ssrMain);
            pnlMain.Controls.Add(mnsMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(2, 2);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(956, 536);
            pnlMain.TabIndex = 5;
            // 
            // btnClose
            // 
            btnClose.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnClose.Location = new System.Drawing.Point(923, 1);
            btnClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(29, 27);
            btnClose.TabIndex = 5;
            btnClose.Text = "X";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += BtnClose_Click;
            // 
            // pnlControls
            // 
            pnlControls.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlControls.Location = new System.Drawing.Point(0, 24);
            pnlControls.Margin = new System.Windows.Forms.Padding(0);
            pnlControls.Name = "pnlControls";
            pnlControls.Size = new System.Drawing.Size(956, 462);
            pnlControls.TabIndex = 4;
            // 
            // ssrMain
            // 
            ssrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { lblStatus, lblTargetDataExtension, lblStatusExportConnection, lblNfiestaExtension, lblSDesignExtension });
            ssrMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            ssrMain.Location = new System.Drawing.Point(0, 486);
            ssrMain.Name = "ssrMain";
            ssrMain.Padding = new System.Windows.Forms.Padding(1, 3, 1, 25);
            ssrMain.Size = new System.Drawing.Size(956, 50);
            ssrMain.TabIndex = 2;
            ssrMain.Text = "StatusMain";
            // 
            // lblStatus
            // 
            lblStatus.BackColor = System.Drawing.Color.Transparent;
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new System.Drawing.Size(953, 15);
            lblStatus.Text = "lblStatus";
            // 
            // lblTargetDataExtension
            // 
            lblTargetDataExtension.BackColor = System.Drawing.Color.Transparent;
            lblTargetDataExtension.Name = "lblTargetDataExtension";
            lblTargetDataExtension.Size = new System.Drawing.Size(951, 15);
            lblTargetDataExtension.Text = "lblTargetDataExtension";
            lblTargetDataExtension.Visible = false;
            // 
            // lblStatusExportConnection
            // 
            lblStatusExportConnection.BackColor = System.Drawing.Color.Transparent;
            lblStatusExportConnection.Name = "lblStatusExportConnection";
            lblStatusExportConnection.Size = new System.Drawing.Size(951, 15);
            lblStatusExportConnection.Text = "lblStatusExportConnection";
            lblStatusExportConnection.Visible = false;
            // 
            // lblNfiestaExtension
            // 
            lblNfiestaExtension.BackColor = System.Drawing.Color.Transparent;
            lblNfiestaExtension.Name = "lblNfiestaExtension";
            lblNfiestaExtension.Size = new System.Drawing.Size(951, 15);
            lblNfiestaExtension.Text = "lblNfiestaExtension";
            lblNfiestaExtension.Visible = false;
            // 
            // lblSDesignExtension
            // 
            lblSDesignExtension.BackColor = System.Drawing.Color.Transparent;
            lblSDesignExtension.Name = "lblSDesignExtension";
            lblSDesignExtension.Size = new System.Drawing.Size(951, 15);
            lblSDesignExtension.Text = "lblSdesignExtension";
            lblSDesignExtension.Visible = false;
            // 
            // mnsMain
            // 
            mnsMain.BackColor = System.Drawing.Color.Transparent;
            mnsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiDatabase });
            mnsMain.Location = new System.Drawing.Point(0, 0);
            mnsMain.Name = "mnsMain";
            mnsMain.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            mnsMain.Size = new System.Drawing.Size(956, 24);
            mnsMain.TabIndex = 1;
            mnsMain.Text = "MenuMain";
            // 
            // tsmiDatabase
            // 
            tsmiDatabase.BackColor = System.Drawing.Color.Transparent;
            tsmiDatabase.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiConnect, tsmiSeparator, tsmiTargetDataExtension });
            tsmiDatabase.Name = "tsmiDatabase";
            tsmiDatabase.Padding = new System.Windows.Forms.Padding(0);
            tsmiDatabase.Size = new System.Drawing.Size(82, 24);
            tsmiDatabase.Text = "tsmiDatabase";
            // 
            // tsmiConnect
            // 
            tsmiConnect.Name = "tsmiConnect";
            tsmiConnect.Size = new System.Drawing.Size(204, 22);
            tsmiConnect.Text = "tsmiConnect";
            tsmiConnect.Click += ItemConnect_Click;
            // 
            // tsmiSeparator
            // 
            tsmiSeparator.Name = "tsmiSeparator";
            tsmiSeparator.Size = new System.Drawing.Size(201, 6);
            tsmiSeparator.Visible = false;
            // 
            // tsmiTargetDataExtension
            // 
            tsmiTargetDataExtension.Name = "tsmiTargetDataExtension";
            tsmiTargetDataExtension.Size = new System.Drawing.Size(204, 22);
            tsmiTargetDataExtension.Text = "tsmiTargetDataExtension";
            tsmiTargetDataExtension.Visible = false;
            tsmiTargetDataExtension.Click += ItemTargetDataExtension_Click;
            // 
            // ControlEtl
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.DarkSeaGreen;
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlEtl";
            Padding = new System.Windows.Forms.Padding(2);
            Size = new System.Drawing.Size(960, 540);
            pnlMain.ResumeLayout(false);
            pnlMain.PerformLayout();
            ssrMain.ResumeLayout(false);
            ssrMain.PerformLayout();
            mnsMain.ResumeLayout(false);
            mnsMain.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.StatusStrip ssrMain;
        private System.Windows.Forms.MenuStrip mnsMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiDatabase;
        private System.Windows.Forms.ToolStripMenuItem tsmiConnect;
        private System.Windows.Forms.ToolStripSeparator tsmiSeparator;
        private System.Windows.Forms.ToolStripMenuItem tsmiTargetDataExtension;

        /// <summary>
        /// lblStatusExportConnection
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblStatusExportConnection;

        /// <summary>
        /// lblNfiestaExtension
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblNfiestaExtension;

        /// <summary>
        /// lblStatus
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblStatus;

        /// <summary>
        /// lblTargetDataExtension
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblTargetDataExtension;

        /// <summary>
        /// lblSDesignExtension
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblSDesignExtension;

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ToolTip tipClose;

        /// <summary>
        /// pnlControls
        /// </summary>
        public System.Windows.Forms.Panel pnlControls;
    }

}