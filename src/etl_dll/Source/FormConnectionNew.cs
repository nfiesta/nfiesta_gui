﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Formulář pro vytvoření nového databázového připojení</para>
    /// <para lang="en">Form to create a new database connection</para>
    /// </summary>
    public partial class FormConnectionNew
            : Form, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro vytvoření nového databázového připojení</para>
        /// <para lang="en">Form constructor to create a new database connection</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormConnectionNew(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is ControlConnection))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlConnection.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is ControlConnection))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlConnection.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((ControlConnection)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((ControlConnection)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ControlConnection)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ControlConnection)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlConnection ctrConnection)
                {
                    return ctrConnection.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikační číslo připojení</para>
        /// <para lang="en">Connection identifier</para>
        /// </summary>
        public int? NewConnectionId { get; private set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",             "Nové databázové připojení" },
                            { "grpConnection",          String.Empty },
                            { "lblHost",                "Server:" },
                            { "lblDbName",              "Databáze:" },
                            { "lblPort",                "Port:" },
                            { "lblComment",             "Popis:" },
                            { "btnOK",                  "Vytvořit" },
                            { "btnCancel",              "Zrušit" },
                            { "msgHostIsEmpty",         "Název serveru musí být vyplněn." },
                            { "msgDbNameIsEmpty",       "Název databáze musí být vyplněn." },
                            { "msgPortIsEmpty",         "Název portu musí být vyplněn." },
                            { "msgCommentIsEmpty",      "Popis musí být vyplněn." },
                            { "msgPortMustBeNumber",    "Port musí být číslo." },
                            { "msgDuplicateEntry",      "Toto připojení už je v seznamu uloženo." }
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "FormConnectionNew") ?
                    languageFile.NationalVersion.Data["FormConnectionNew"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",             "New database connection" },
                            { "grpConnection",          String.Empty },
                            { "lblHost",                "Server:" },
                            { "lblDbName",              "Database:" },
                            { "lblPort",                "Port:" },
                            { "lblComment",             "Description:" },
                            { "btnOK",                  "Create" },
                            { "btnCancel",              "Cancel" },
                            { "msgHostIsEmpty",         "The name of the server cannot be empty." },
                            { "msgDbNameIsEmpty",       "The name of the database cannot be empty." },
                            { "msgPortIsEmpty",         "The name of the port cannot be empty." },
                            { "msgCommentIsEmpty",      "Description cannot be empty." },
                            { "msgPortMustBeNumber",    "Port must be a number." },
                            { "msgDuplicateEntry",      "This database connection is already stored in the list." }
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "FormConnectionNew") ?
                    languageFile.InternationalVersion.Data["FormConnectionNew"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro vytvoření nového databázového připojení</para>
        /// <para lang="en">Initializing the form to create a new database connection</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro vytvoření nového databázového připojení</para>
        /// <para lang="en">Initializing form labels to create a new database connection</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text =
               labels.ContainsKey(key: "frmCaption") ?
               labels["frmCaption"] : String.Empty;

            grpConnection.Text =
                labels.ContainsKey(key: "grpConnection") ?
                labels["grpConnection"] : String.Empty;

            lblHost.Text =
                labels.ContainsKey(key: "lblHost") ?
                labels["lblHost"] : String.Empty;

            lblDbName.Text =
                labels.ContainsKey(key: "lblDbName") ?
                labels["lblDbName"] : String.Empty;

            lblPort.Text =
                labels.ContainsKey(key: "lblPort") ?
                labels["lblPort"] : String.Empty;

            lblComment.Text =
                labels.ContainsKey(key: "lblComment") ?
                labels["lblComment"] : String.Empty;

            btnOK.Text =
                labels.ContainsKey(key: "btnOK") ?
                labels["btnOK"] : String.Empty;

            btnCancel.Text =
                labels.ContainsKey(key: "btnCancel") ?
                labels["btnCancel"] : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události "Zavření formuláře"</para>
        /// <para lang="en">Event handling form closing</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (FormConnectionNew)</para>
        /// <para lang="en">Object that sends the event (FormConnectionNew)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void FormConnectionNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            TDExportConnectionList exportConnections =
                TDFunctions.FnEtlGetExportConnections.Execute(
                    database: Database);

            if (this.DialogResult == DialogResult.OK)
            {
                string host = txtHost.Text.Trim();
                string dbName = txtDbName.Text.Trim();
                bool portParsedSuccessfully = int.TryParse(txtPort.Text.Trim(), out int port);
                string comment = txtComment.Text.Trim();

                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                if (String.IsNullOrEmpty(txtHost.Text.Trim()))
                {
                    MessageBox.Show(
                         text: messages.ContainsKey(key: "msgHostIsEmpty") ?
                                messages["msgHostIsEmpty"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (String.IsNullOrEmpty(txtDbName.Text.Trim()))
                {
                    MessageBox.Show(
                         text: messages.ContainsKey(key: "msgDbNameIsEmpty") ?
                                messages["msgDbNameIsEmpty"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (String.IsNullOrEmpty(txtPort.Text.Trim()))
                {
                    MessageBox.Show(
                         text: messages.ContainsKey(key: "msgPortIsEmpty") ?
                                messages["msgPortIsEmpty"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (portParsedSuccessfully == false)
                {
                    MessageBox.Show(
                         text: messages.ContainsKey(key: "msgPortMustBeNumber") ?
                                messages["msgPortMustBeNumber"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (
                    exportConnections.Items.Any(a => a.DBName == dbName && a.Host == host && a.Port == port))
                {
                    //toto zabraňuje zápisu duplicitní kombinace hodnot z více sloupců do databázové tabulky
                    //this prevents writing duplicate combinations of values from multiple columns to the database table
                    MessageBox.Show(
                         text: messages.ContainsKey(key: "msgDuplicateEntry") ?
                                messages["msgDuplicateEntry"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (String.IsNullOrEmpty(txtComment.Text.Trim()))
                {
                    MessageBox.Show(
                         text: messages.ContainsKey(key: "msgCommentIsEmpty") ?
                                messages["msgCommentIsEmpty"] : String.Empty,
                         caption: String.Empty,
                         buttons: MessageBoxButtons.OK,
                         icon: MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    var formUserPassword =
                        new FormUserPassword(
                            controlOwner: this,
                            _SelectedExportConnectionHost: txtHost.Text,
                            _SelectedExportConnectionDbName: txtDbName.Text,
                            _SelectedExportConnectionPort: Int32.Parse(s: txtPort.Text));

                    if (formUserPassword.ShowDialog() != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }

                    NewConnectionId = TDFunctions.FnEtlSaveExportConnection.Execute(
                        database: Database,
                        host: host,
                        dbname: dbName,
                        port: port,
                        comment: comment);

                    e.Cancel = false;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "OK"</para>
        /// <para lang="en">Event handling the click on the "OK" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Zrušit"</para>
        /// <para lang="en">Event handling the click on the "Cancel" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        #endregion Event Handlers

    }

}