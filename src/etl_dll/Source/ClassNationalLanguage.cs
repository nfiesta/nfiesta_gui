﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using ZaJi.PostgreSQL;

namespace Hanakova
{
    namespace ModuleEtl
    {
        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        public class NationalLang
        {
            /// <summary>
            /// <para lang="cs">Národní jazyk</para>
            /// <para lang="en">National language</para>
            /// </summary>
            private Language natLang = Language.CS;

            /// <summary>
            /// <para lang="cs">Národní jazyk</para>
            /// <para lang="en">National language</para>
            /// </summary>
            public Language NatLang
            {
                get { return natLang; }
                set { natLang = value; }
            }

            //public string NationalLanguage
            //{
            //    get { return LanguageList.ISO_639_1(natLang); }
            //}

            //string nationalLanguage = LanguageList.ISO_639_1(Language.CS);
            //Language lng = LanguageList.LanguageFromISO_639_1(code: nationalLanguage); - opposite of the previous code
        }
    }
}



