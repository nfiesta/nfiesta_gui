﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class FormTopicNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpTopic = new System.Windows.Forms.GroupBox();
            tlpTopic = new System.Windows.Forms.TableLayoutPanel();
            lblDescriptionEn = new System.Windows.Forms.Label();
            lblLabelEn = new System.Windows.Forms.Label();
            lblDescription = new System.Windows.Forms.Label();
            lblLabel = new System.Windows.Forms.Label();
            txtDescriptionEn = new System.Windows.Forms.TextBox();
            txtLabelEn = new System.Windows.Forms.TextBox();
            txtDescription = new System.Windows.Forms.TextBox();
            txtLabel = new System.Windows.Forms.TextBox();
            pnlButtons = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            btnOK = new System.Windows.Forms.Button();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            grpTopic.SuspendLayout();
            tlpTopic.SuspendLayout();
            pnlButtons.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(542, 204);
            pnlMain.TabIndex = 8;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpTopic, 0, 0);
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.Size = new System.Drawing.Size(542, 204);
            tlpMain.TabIndex = 5;
            // 
            // grpTopic
            // 
            grpTopic.Controls.Add(tlpTopic);
            grpTopic.Dock = System.Windows.Forms.DockStyle.Fill;
            grpTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpTopic.ForeColor = System.Drawing.SystemColors.HotTrack;
            grpTopic.Location = new System.Drawing.Point(4, 3);
            grpTopic.Margin = new System.Windows.Forms.Padding(0);
            grpTopic.Name = "grpTopic";
            grpTopic.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpTopic.Size = new System.Drawing.Size(534, 152);
            grpTopic.TabIndex = 6;
            grpTopic.TabStop = false;
            grpTopic.Text = "grpTopic";
            // 
            // tlpTopic
            // 
            tlpTopic.ColumnCount = 2;
            tlpTopic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            tlpTopic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpTopic.Controls.Add(lblDescriptionEn, 0, 3);
            tlpTopic.Controls.Add(lblLabelEn, 0, 2);
            tlpTopic.Controls.Add(lblDescription, 0, 1);
            tlpTopic.Controls.Add(lblLabel, 0, 0);
            tlpTopic.Controls.Add(txtDescriptionEn, 1, 3);
            tlpTopic.Controls.Add(txtLabelEn, 1, 2);
            tlpTopic.Controls.Add(txtDescription, 1, 1);
            tlpTopic.Controls.Add(txtLabel, 1, 0);
            tlpTopic.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpTopic.Location = new System.Drawing.Point(4, 18);
            tlpTopic.Margin = new System.Windows.Forms.Padding(0);
            tlpTopic.Name = "tlpTopic";
            tlpTopic.RowCount = 5;
            tlpTopic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpTopic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpTopic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpTopic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpTopic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpTopic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpTopic.Size = new System.Drawing.Size(526, 131);
            tlpTopic.TabIndex = 2;
            // 
            // lblDescriptionEn
            // 
            lblDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblDescriptionEn.Location = new System.Drawing.Point(0, 75);
            lblDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionEn.Name = "lblDescriptionEn";
            lblDescriptionEn.Size = new System.Drawing.Size(140, 25);
            lblDescriptionEn.TabIndex = 3;
            lblDescriptionEn.Text = "lblDescriptionEn";
            lblDescriptionEn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelEn
            // 
            lblLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblLabelEn.Location = new System.Drawing.Point(0, 50);
            lblLabelEn.Margin = new System.Windows.Forms.Padding(0);
            lblLabelEn.Name = "lblLabelEn";
            lblLabelEn.Size = new System.Drawing.Size(140, 25);
            lblLabelEn.TabIndex = 2;
            lblLabelEn.Text = "lblLabelEn";
            lblLabelEn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescription
            // 
            lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblDescription.Location = new System.Drawing.Point(0, 25);
            lblDescription.Margin = new System.Windows.Forms.Padding(0);
            lblDescription.Name = "lblDescription";
            lblDescription.Size = new System.Drawing.Size(140, 25);
            lblDescription.TabIndex = 1;
            lblDescription.Text = "lblDescription";
            lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabel
            // 
            lblLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblLabel.Location = new System.Drawing.Point(0, 0);
            lblLabel.Margin = new System.Windows.Forms.Padding(0);
            lblLabel.Name = "lblLabel";
            lblLabel.Size = new System.Drawing.Size(140, 25);
            lblLabel.TabIndex = 0;
            lblLabel.Text = "lblLabel";
            lblLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescriptionEn
            // 
            txtDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescriptionEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtDescriptionEn.Location = new System.Drawing.Point(140, 75);
            txtDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            txtDescriptionEn.Name = "txtDescriptionEn";
            txtDescriptionEn.Size = new System.Drawing.Size(386, 20);
            txtDescriptionEn.TabIndex = 7;
            // 
            // txtLabelEn
            // 
            txtLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtLabelEn.Location = new System.Drawing.Point(140, 50);
            txtLabelEn.Margin = new System.Windows.Forms.Padding(0);
            txtLabelEn.Name = "txtLabelEn";
            txtLabelEn.Size = new System.Drawing.Size(386, 20);
            txtLabelEn.TabIndex = 6;
            // 
            // txtDescription
            // 
            txtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtDescription.Location = new System.Drawing.Point(140, 25);
            txtDescription.Margin = new System.Windows.Forms.Padding(0);
            txtDescription.Name = "txtDescription";
            txtDescription.Size = new System.Drawing.Size(386, 20);
            txtDescription.TabIndex = 5;
            // 
            // txtLabel
            // 
            txtLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtLabel.Location = new System.Drawing.Point(140, 0);
            txtLabel.Margin = new System.Windows.Forms.Padding(0);
            txtLabel.Name = "txtLabel";
            txtLabel.Size = new System.Drawing.Size(386, 20);
            txtLabel.TabIndex = 4;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnCancel);
            pnlButtons.Controls.Add(btnOK);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(4, 155);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(534, 46);
            pnlButtons.TabIndex = 4;
            // 
            // btnCancel
            // 
            btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Location = new System.Drawing.Point(172, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 35);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += BtnCancel_Click;
            // 
            // btnOK
            // 
            btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Location = new System.Drawing.Point(353, 6);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(175, 35);
            btnOK.TabIndex = 0;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += BtnOK_Click;
            // 
            // FormTopicNew
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(542, 204);
            Controls.Add(pnlMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormTopicNew";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormTopicNew";
            FormClosing += FormTopicNew_FormClosing;
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            grpTopic.ResumeLayout(false);
            tlpTopic.ResumeLayout(false);
            tlpTopic.PerformLayout();
            pnlButtons.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpTopic;
        private System.Windows.Forms.TableLayoutPanel tlpTopic;
        private System.Windows.Forms.Label lblDescriptionEn;
        private System.Windows.Forms.Label lblLabelEn;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblLabel;
        private System.Windows.Forms.TextBox txtDescriptionEn;
        private System.Windows.Forms.TextBox txtLabelEn;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtLabel;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }

}