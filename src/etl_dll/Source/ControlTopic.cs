﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Téma"</para>
    /// <para lang="en">Control "Topic"</para>
    /// </summary>
    public partial class ControlTopic
            : UserControl, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table</para>
        /// </summary>
        private readonly int? _idEtl;

        /// <summary>
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů</para>
        /// <para lang="en">List of panel and refyearset combination ids</para>
        /// </summary>
        private readonly List<int?> _refYearSetToPanelMapping;

        /// <summary>
        /// <para lang="cs">Id zvoleného tématu z tabulky c_topic cílové DB, kterou vrací fce fn_etl_get_topics</para>
        /// <para lang="en">Selected topic id from the c_topic table in the source DB, returned by the fn_etl_get_topics function</para>
        /// </summary>
        private int? selectedId;

        /// <summary>
        /// <para lang="cs">Zkratka zvoleného tématu z tabulky c_topic cílové DB, kterou vrací fce fn_etl_get_topics</para>
        /// <para lang="en">Selected topic label from the c_topic table in the source DB, returned by the fn_etl_get_topics function</para>
        /// </summary>
        private string selectedLabel;

        /// <summary>
        /// <para lang="cs">Popis zvoleného tématu z tabulky c_topic cílové DB, kterou vrací fce fn_etl_get_topics</para>
        /// <para lang="en">Selected topic description from the c_topic table in the source DB, returned by the fn_etl_get_topics function</para>
        /// </summary>
        private string selectedDescription;

        /// <summary>
        /// <para lang="cs">Anglická zkratka zvoleného tématu z tabulky c_topic cílové DB, kterou vrací fce fn_etl_get_topics</para>
        /// <para lang="en">Selected English label of the topic from the c_topic table in the source DB, returned by the fn_etl_get_topics function</para>
        /// </summary>
        private string selectedLabelEn;

        /// <summary>
        /// <para lang="cs">Anglický popis zvoleného tématu z tabulky c_topic cílové DB, kterou vrací fce fn_etl_get_topics</para>
        /// <para lang="en">Selected English description of the topic from the c_topic table in the source DB, returned by the fn_etl_get_topics function</para>
        /// </summary>
        private string selectedDescriptionEn;

        /// <summary>
        /// <para lang="cs">Etl id z tabulky t_etl_target_variable ve zdrojové databázi</para>
        /// <para lang="en">Etl id from the t_etl_target_variable table in the source database</para>
        /// </summary>
        private int? idOfTargetVariableInTargetDB;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelCs;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelEn;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;


        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="idEtl">
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="refYearSetToPanelMapping">
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů</para>
        /// <para lang="en">List of panel and refyearset combination ids</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="selectedIndicatorLabelCs">
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </param>
        /// <param name="selectedIndicatorLabelEn">
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public ControlTopic(
            Control controlOwner,
            int? idEtl,
            List<int?> refYearSetToPanelMapping,
            int selectedExportConnectionId,
            string selectedIndicatorLabelCs,
            string selectedIndicatorLabelEn,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction)
        {
            InitializeComponent();

            _idEtl = idEtl;
            _refYearSetToPanelMapping = refYearSetToPanelMapping;
            _selectedExportConnectionId = selectedExportConnectionId;
            _selectedIndicatorLabelCs = selectedIndicatorLabelCs;
            _selectedIndicatorLabelEn = selectedIndicatorLabelEn;
            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;

            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlEtl ctrEtl)
                {
                    return ctrEtl;
                }

                else if (ControlOwner is ControlPanelRefyearset ctrPanelRefyearset)
                {
                    return ctrPanelRefyearset.CtrEtl;
                }

                else if (ControlOwner is ControlAreaDomain ctrAreaDomain)
                {
                    return ctrAreaDomain.CtrEtl;
                }

                else if (ControlOwner is ControlSubPopulation ctrSubPopulation)
                {
                    return ctrSubPopulation.CtrEtl;
                }

                else if (ControlOwner is ControlAttributeVariables ctrAttributeVariables)
                {
                    return ctrAttributeVariables.CtrEtl;
                }

                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                     "Přiřazení témat(u) ke zvolené cílové proměnné" },
                            { "grpTopicAll",                        "Seznam témat:" },
                            { "lblTargetVariable",                  "Cílová proměnná:" },
                            { "lblTargetVariableValue",             "$1" },
                            { "tsrTopicAll",                        String.Empty },
                            { "btnTopicAllAdd",                     "Vytvořit nové téma" },
                            { "btnTopicAllUpdate",                  "Editovat zvolené téma" },
                            { "btnTopicAllDelete",                  "Smazat zvolené téma" },
                            { "btnNext",                            "Uložit do databáze" },
                            { "msgNoTopicSelected",                 "Není vybráno žádné téma." },
                            { "msgCannotDeleteSelectedTopic",       "Vybrané téma nelze smazat." },
                            { "msgNoTopicAssigned",                 "Ke zvolené cílové proměnné musí být přiřazeno alespoň 1 téma." },
                            { "dgvTopicAllLabel",                   "Téma"},
                            { "dgvTopicAllDescription",             "Popis"},
                            { "dgvTopicAllLinked",                  "Přiřazeno"},
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlTopic") ?
                    languageFile.NationalVersion.Data["ControlTopic"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                     "Assign a topic(s) to the selected target variable" },
                            { "grpTopicAll",                        "List of topics:" },
                            { "lblTargetVariable",                  "Target variable:" },
                            { "lblTargetVariableValue",             "$1" },
                            { "tsrTopicAll",                        String.Empty },
                            { "btnTopicAllAdd",                     "Create a new topic" },
                            { "btnTopicAllUpdate",                  "Edit the selected topic" },
                            { "btnTopicAllDelete",                  "Delete the selected topic" },
                            { "btnNext",                            "Save to database" },
                            { "msgNoTopicSelected",                 "No topic is selected." },
                            { "msgCannotDeleteSelectedTopic",       "The selected topic cannot be deleted." },
                            { "msgNoTopicAssigned",                 "At least 1 topic must be linked to the selected target variable." },
                            { "dgvTopicAllLabel",                   "Topic"},
                            { "dgvTopicAllDescription",             "Description"},
                            { "dgvTopicAllLinked",                  "Linked"},
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlTopic") ?
                    languageFile.InternationalVersion.Data["ControlTopic"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            nationalLang = new NationalLang();

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            grpTopicAll.Text =
                labels.ContainsKey(key: "grpTopicAll") ?
                labels["grpTopicAll"] : String.Empty;

            lblTargetVariable.Text =
                labels.ContainsKey(key: "lblTargetVariable") ?
                labels["lblTargetVariable"] : String.Empty;

            lblTargetVariableValue.Text =
                (LanguageVersion == LanguageVersion.National) ?
                    (labels.ContainsKey(key: "lblTargetVariableValue") ?
                    labels["lblTargetVariableValue"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: _selectedIndicatorLabelCs) :
                    (labels.ContainsKey(key: "lblTargetVariableValue") ?
                    labels["lblTargetVariableValue"] : String.Empty)
                    .Replace(oldValue: "$1", newValue: _selectedIndicatorLabelEn);

            tsrTopicAll.Text =
               labels.ContainsKey(key: "tsrTopicAll") ?
               labels["tsrTopicAll"] : String.Empty;

            btnTopicAllAdd.Text =
                labels.ContainsKey(key: "btnTopicAllAdd") ?
                labels["btnTopicAllAdd"] : String.Empty;

            btnTopicAllUpdate.Text =
                labels.ContainsKey(key: "btnTopicAllUpdate") ?
                labels["btnTopicAllUpdate"] : String.Empty;

            btnTopicAllDelete.Text =
                labels.ContainsKey(key: "btnTopicAllDelete") ?
                labels["btnTopicAllDelete"] : String.Empty;

            btnNext.Text =
                labels.ContainsKey(key: "btnNext") ?
                labels["btnNext"] : String.Empty;

            LoadContent();

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            idOfTargetVariableInTargetDB =
                TDFunctions.FnEtlGetEtlIdForTargetVariable.Execute(
                    database: Database,
                    idTEtlTargetVariable: _idEtl,
                    transaction: SourceTransaction);

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    DataTable resultNat = NfiEstaFunctions.FnEtlGetTopicsForTargetVariable.ExecuteQuery(
                        database: FormUserPassword.TargetDatabase,
                        targetVariable: idOfTargetVariableInTargetDB,
                        nationalLanguage: nationalLang.NatLang.ToString(),
                        linked: null,
                        transaction: TargetTransaction);

                    dgvTopicAll.DataSource = resultNat;

                    break;

                case LanguageVersion.International:

                    DataTable resultEn = NfiEstaFunctions.FnEtlGetTopicsForTargetVariable.ExecuteQuery(
                        database: FormUserPassword.TargetDatabase,
                        targetVariable: idOfTargetVariableInTargetDB,
                        "en",
                        linked: null,
                        transaction: TargetTransaction);

                    dgvTopicAll.DataSource = resultEn;

                    break;
            }

            FormatDataGridView(dgvTopicAll);

            dgvTopicAll.Columns["id"].Visible = false;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    dgvTopicAll.Columns["label"].Visible = true;
                    dgvTopicAll.Columns["label_en"].Visible = false;
                    dgvTopicAll.Columns["description"].Visible = true;
                    dgvTopicAll.Columns["description_en"].Visible = false;
                    dgvTopicAll.Columns["label"].HeaderText = labels.ContainsKey(key: "dgvTopicAllLabel") ? labels["dgvTopicAllLabel"] : String.Empty;
                    dgvTopicAll.Columns["description"].HeaderText = labels.ContainsKey(key: "dgvTopicAllDescription") ? labels["dgvTopicAllDescription"] : String.Empty;
                    dgvTopicAll.Columns["linked"].HeaderText = labels.ContainsKey(key: "dgvTopicAllLinked") ? labels["dgvTopicAllLinked"] : String.Empty;
                    break;

                case LanguageVersion.International:
                    dgvTopicAll.Columns["label"].Visible = false;
                    dgvTopicAll.Columns["label_en"].Visible = true;
                    dgvTopicAll.Columns["description"].Visible = false;
                    dgvTopicAll.Columns["description_en"].Visible = true;
                    dgvTopicAll.Columns["label_en"].HeaderText = labels.ContainsKey(key: "dgvTopicAllLabel") ? labels["dgvTopicAllLabel"] : String.Empty;
                    dgvTopicAll.Columns["description_en"].HeaderText = labels.ContainsKey(key: "dgvTopicAllDescription") ? labels["dgvTopicAllDescription"] : String.Empty;
                    dgvTopicAll.Columns["linked"].HeaderText = labels.ContainsKey(key: "dgvTopicAllLinked") ? labels["dgvTopicAllLinked"] : String.Empty;
                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví záhlaví, viditelnost, formáty podle typu dat a zarovnání ve sloupcích daného DataGridView</para>
        /// <para lang="en">Sets header, visibility, formats by data type and alignment in columns of given DataGridView</para>
        /// </summary>
        /// <param name="dataGridView">
        /// <para lang="cs">Objekt DataGridView, který se má formátovat</para>
        /// <para lang="en">The DataGridView object to be formatted</para>
        /// </param>
        public void FormatDataGridView(DataGridView dataGridView)
        {
            // General:
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeRows = false;

            dataGridView.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle()
            {
                BackColor = Color.LightCyan
            };

            dataGridView.AutoGenerateColumns = true;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView.BackgroundColor = SystemColors.Window;
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.None;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;

            dataGridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.DarkBlue,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = Color.White,
                SelectionBackColor = Color.DarkBlue,
                SelectionForeColor = Color.White,
                WrapMode = DataGridViewTriState.True
            };

            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            dataGridView.DefaultCellStyle = new DataGridViewCellStyle()
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.White,
                Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                ForeColor = SystemColors.ControlText,
                Format = "N3",
                NullValue = "NULL",
                SelectionBackColor = Color.Bisque,
                SelectionForeColor = SystemColors.ControlText,
                WrapMode = DataGridViewTriState.False
            };

            dataGridView.EnableHeadersVisualStyles = false;
            dataGridView.MultiSelect = true;
            dataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView.RowHeadersVisible = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na buňku v DataGridView. Specificky reaguje na kliknutí ve sloupci 'linked', kde ovládá zpracování spojení nebo odstranění záznamu na základě stavu checkboxu.</para>
        /// <para lang="en">Handles the cell click event in DataGridView. Specifically responds to clicks in the 'linked' column, controlling the processing of linking or removing a record based on the state of the checkbox.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události, včetně indexu sloupce a řádku, na který bylo kliknuto</para>
        /// <para lang="en">Event parameters, including the index of the column and row that was clicked</para>
        /// </param>
        private void dgvTopicAll_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvTopicAll.Columns["linked"].Index)
            {
                dgvTopicAll.CommitEdit(DataGridViewDataErrorContexts.Commit);

                int id = Convert.ToInt32(dgvTopicAll.Rows[e.RowIndex].Cells["id"].Value);
                bool isLinked = Convert.ToBoolean(dgvTopicAll.Rows[e.RowIndex].Cells["linked"].Value);

                if (isLinked)
                {
                    NfiEstaFunctions.FnEtlSaveTargetVariableToTopic.ExecuteQuery(
                        FormUserPassword.TargetDatabase,
                        idOfTargetVariableInTargetDB,
                        id,
                        TargetTransaction);
                }
                else
                {
                    List<int?> ids = new List<int?>() { id };

                    NfiEstaFunctions.FnEtlDeleteTargetVariableToTopic.ExecuteQuery(
                        FormUserPassword.TargetDatabase,
                        idOfTargetVariableInTargetDB,
                        ids,
                        TargetTransaction);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události změna výběru řádku v DataGridView</para>
        /// <para lang="en">Handling the event of the row selection change in DataGridView</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvTopicAll_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvTopicAll.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dgvTopicAll.SelectedRows[0];

                selectedId = Int32.Parse(row.Cells["id"].Value.ToString());
                selectedLabel = row.Cells["label"].Value.ToString();
                selectedDescription = row.Cells["description"].Value.ToString();
                selectedLabelEn = row.Cells["label_en"].Value.ToString();
                selectedDescriptionEn = row.Cells["description_en"].Value.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Vytvořit nové téma"</para>
        /// <para lang="en">Handling the "Create a new topic" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnTopicAllAdd_Click(object sender, EventArgs e)
        {
            FormTopicNew frm = new FormTopicNew(
                controlOwner: this,
                sourceTransaction: SourceTransaction,
                targetTransaction: TargetTransaction);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Aktualizovat téma"</para>
        /// <para lang="en">Handling the "Update the topic" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnTopicAllUpdate_Click(object sender, EventArgs e)
        {
            if (dgvTopicAll.SelectedRows.Count == 0)
            {
                // Není vybráno žádné téma
                // No topic is selected
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoTopicSelected") ?
                            messages["msgNoTopicSelected"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormTopicUpdate frm = new FormTopicUpdate(
                controlOwner: this,
                selectedId: selectedId,
                selectedLabel: selectedLabel,
                selectedDescription: selectedDescription,
                selectedLabelEn: selectedLabelEn,
                selectedDescriptionEn: selectedDescriptionEn,
                sourceTransaction: SourceTransaction,
                targetTransaction: TargetTransaction);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Smazat vybrané téma"</para>
        /// <para lang="en">Handling the "Delete the selected topic" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnTopicAllDelete_Click(object sender, EventArgs e)
        {
            if (dgvTopicAll.SelectedRows.Count == 0)
            {
                // Není vybráno žádné téma
                // No topic is selected
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoTopicSelected") ?
                            messages["msgNoTopicSelected"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            if (NfiEstaFunctions.FnEtlTryDeleteTopic.Execute(
                    FormUserPassword.TargetDatabase,
                    selectedId,
                    TargetTransaction))
            {
                NfiEstaFunctions.FnEtlDeleteTopic.Execute(
                    FormUserPassword.TargetDatabase,
                    selectedId,
                    TargetTransaction);

                LoadContent();
            }
            else
            {
                // Vybrané téma nelze smazat
                // The selected téma cannot be deleted
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgCannotDeleteSelectedTopic") ?
                            messages["msgCannotDeleteSelectedTopic"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Další" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            bool isAnyLinkedChecked = false;

            foreach (DataGridViewRow row in dgvTopicAll.Rows)
            {
                if (Convert.ToBoolean(row.Cells["linked"].Value))
                {
                    isAnyLinkedChecked = true;
                    break;
                }
            }

            if (!isAnyLinkedChecked)
            {
                Dictionary<string, string> messages = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);
                MessageBox.Show(
                   text: messages.ContainsKey(key: "msgNoTopicAssigned") ?
                            messages["msgNoTopicAssigned"] : String.Empty,
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);
            }
            else
            {
                FormCommit frm = new FormCommit(
                    controlOwner: this,
                    idEtl: _idEtl,
                    refYearSetToPanelMapping: _refYearSetToPanelMapping,
                    selectedExportConnectionId: _selectedExportConnectionId,
                    sourceTransaction: SourceTransaction,
                    targetTransaction: TargetTransaction);

                if (frm.ShowDialog() == DialogResult.OK)
                {

                }

                //frm.Show();
            }
        }

        #endregion Event Handlers
    }

}
