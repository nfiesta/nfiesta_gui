﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class FormCheckAdSp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            btnUpdateAll = new System.Windows.Forms.Button();
            btnClose = new System.Windows.Forms.Button();
            pnlUpperButtons = new System.Windows.Forms.Panel();
            btnSPC = new System.Windows.Forms.Button();
            btnSP = new System.Windows.Forms.Button();
            btnADC = new System.Windows.Forms.Button();
            btnAD = new System.Windows.Forms.Button();
            splMain = new System.Windows.Forms.SplitContainer();
            splTables = new System.Windows.Forms.SplitContainer();
            dgvType = new System.Windows.Forms.DataGridView();
            dgv = new System.Windows.Forms.DataGridView();
            tlpContent = new System.Windows.Forms.TableLayoutPanel();
            lblDesEnTargetDB = new System.Windows.Forms.Label();
            lblDesEnSourceDB = new System.Windows.Forms.Label();
            lblLabelNatSourceDB = new System.Windows.Forms.Label();
            btnUpdateLabelEn = new System.Windows.Forms.Button();
            btnUpdateDesEn = new System.Windows.Forms.Button();
            txtDesEnTargetDB = new System.Windows.Forms.TextBox();
            txtLabelEnTargetDB = new System.Windows.Forms.TextBox();
            txtLabelEnSourceDB = new System.Windows.Forms.TextBox();
            lblLabelEnTargetDB = new System.Windows.Forms.Label();
            lblLabelEnSourceDB = new System.Windows.Forms.Label();
            txtDesNatTargetDB = new System.Windows.Forms.TextBox();
            txtDesNatSourceDB = new System.Windows.Forms.TextBox();
            lblDesNatTargetDB = new System.Windows.Forms.Label();
            lblDesNatSourceDB = new System.Windows.Forms.Label();
            txtLabelNatTargetDB = new System.Windows.Forms.TextBox();
            txtLabelNatSourceDB = new System.Windows.Forms.TextBox();
            lblLabelNatTargetDB = new System.Windows.Forms.Label();
            btnUpdateLabelNat = new System.Windows.Forms.Button();
            btnUpdateDesNat = new System.Windows.Forms.Button();
            txtDesEnSourceDB = new System.Windows.Forms.TextBox();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            pnlUpperButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splTables).BeginInit();
            splTables.Panel1.SuspendLayout();
            splTables.Panel2.SuspendLayout();
            splTables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvType).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dgv).BeginInit();
            tlpContent.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.AutoScroll = true;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 3);
            tlpMain.Controls.Add(pnlUpperButtons, 0, 1);
            tlpMain.Controls.Add(splMain, 0, 2);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 4;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.Size = new System.Drawing.Size(1101, 845);
            tlpMain.TabIndex = 0;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(btnUpdateAll);
            pnlButtons.Controls.Add(btnClose);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(4, 802);
            pnlButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(1093, 40);
            pnlButtons.TabIndex = 10;
            // 
            // btnUpdateAll
            // 
            btnUpdateAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnUpdateAll.Location = new System.Drawing.Point(729, 1);
            btnUpdateAll.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnUpdateAll.Name = "btnUpdateAll";
            btnUpdateAll.Size = new System.Drawing.Size(175, 35);
            btnUpdateAll.TabIndex = 3;
            btnUpdateAll.Text = "btnUpdateAll";
            btnUpdateAll.UseVisualStyleBackColor = true;
            btnUpdateAll.Click += btnUpdateAll_Click;
            // 
            // btnClose
            // 
            btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnClose.Location = new System.Drawing.Point(911, 1);
            btnClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(175, 35);
            btnClose.TabIndex = 2;
            btnClose.Text = "btnClose";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // pnlUpperButtons
            // 
            pnlUpperButtons.Controls.Add(btnSPC);
            pnlUpperButtons.Controls.Add(btnSP);
            pnlUpperButtons.Controls.Add(btnADC);
            pnlUpperButtons.Controls.Add(btnAD);
            pnlUpperButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlUpperButtons.Location = new System.Drawing.Point(4, 12);
            pnlUpperButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlUpperButtons.Name = "pnlUpperButtons";
            pnlUpperButtons.Size = new System.Drawing.Size(1093, 46);
            pnlUpperButtons.TabIndex = 8;
            // 
            // btnSPC
            // 
            btnSPC.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnSPC.Location = new System.Drawing.Point(728, 7);
            btnSPC.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnSPC.Name = "btnSPC";
            btnSPC.Size = new System.Drawing.Size(175, 35);
            btnSPC.TabIndex = 7;
            btnSPC.Text = "btnSPC";
            btnSPC.UseVisualStyleBackColor = true;
            btnSPC.Click += btnSPC_Click;
            // 
            // btnSP
            // 
            btnSP.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnSP.Location = new System.Drawing.Point(546, 7);
            btnSP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnSP.Name = "btnSP";
            btnSP.Size = new System.Drawing.Size(175, 35);
            btnSP.TabIndex = 6;
            btnSP.Text = "btnSP";
            btnSP.UseVisualStyleBackColor = true;
            btnSP.Click += btnSP_Click;
            // 
            // btnADC
            // 
            btnADC.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnADC.Location = new System.Drawing.Point(364, 7);
            btnADC.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnADC.Name = "btnADC";
            btnADC.Size = new System.Drawing.Size(175, 35);
            btnADC.TabIndex = 5;
            btnADC.Text = "btnADC";
            btnADC.UseVisualStyleBackColor = true;
            btnADC.Click += btnADC_Click;
            // 
            // btnAD
            // 
            btnAD.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnAD.Location = new System.Drawing.Point(182, 7);
            btnAD.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnAD.Name = "btnAD";
            btnAD.Size = new System.Drawing.Size(175, 35);
            btnAD.TabIndex = 4;
            btnAD.Text = "btnAD";
            btnAD.UseVisualStyleBackColor = true;
            btnAD.Click += btnAD_Click;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.Location = new System.Drawing.Point(4, 64);
            splMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splMain.Name = "splMain";
            splMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(splTables);
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(tlpContent);
            splMain.Size = new System.Drawing.Size(1093, 732);
            splMain.SplitterDistance = 251;
            splMain.SplitterWidth = 5;
            splMain.TabIndex = 11;
            // 
            // splTables
            // 
            splTables.Dock = System.Windows.Forms.DockStyle.Fill;
            splTables.Location = new System.Drawing.Point(0, 0);
            splTables.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splTables.Name = "splTables";
            splTables.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splTables.Panel1
            // 
            splTables.Panel1.Controls.Add(dgvType);
            splTables.Panel1Collapsed = true;
            // 
            // splTables.Panel2
            // 
            splTables.Panel2.Controls.Add(dgv);
            splTables.Size = new System.Drawing.Size(1093, 251);
            splTables.SplitterDistance = 119;
            splTables.SplitterWidth = 5;
            splTables.TabIndex = 0;
            // 
            // dgvType
            // 
            dgvType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvType.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvType.Location = new System.Drawing.Point(0, 0);
            dgvType.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgvType.Name = "dgvType";
            dgvType.Size = new System.Drawing.Size(150, 119);
            dgvType.TabIndex = 0;
            dgvType.SelectionChanged += dgvType_SelectionChanged;
            // 
            // dgv
            // 
            dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            dgv.Location = new System.Drawing.Point(0, 0);
            dgv.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dgv.Name = "dgv";
            dgv.Size = new System.Drawing.Size(1093, 251);
            dgv.TabIndex = 0;
            dgv.CellPainting += dgv_CellPainting;
            dgv.SelectionChanged += dgv_SelectionChanged;
            // 
            // tlpContent
            // 
            tlpContent.AutoScroll = true;
            tlpContent.ColumnCount = 2;
            tlpContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            tlpContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpContent.Controls.Add(lblDesEnTargetDB, 0, 10);
            tlpContent.Controls.Add(lblDesEnSourceDB, 0, 9);
            tlpContent.Controls.Add(lblLabelNatSourceDB, 0, 0);
            tlpContent.Controls.Add(btnUpdateLabelEn, 1, 8);
            tlpContent.Controls.Add(btnUpdateDesEn, 1, 11);
            tlpContent.Controls.Add(txtDesEnTargetDB, 1, 10);
            tlpContent.Controls.Add(txtLabelEnTargetDB, 1, 7);
            tlpContent.Controls.Add(txtLabelEnSourceDB, 1, 6);
            tlpContent.Controls.Add(lblLabelEnTargetDB, 0, 7);
            tlpContent.Controls.Add(lblLabelEnSourceDB, 0, 6);
            tlpContent.Controls.Add(txtDesNatTargetDB, 1, 4);
            tlpContent.Controls.Add(txtDesNatSourceDB, 1, 3);
            tlpContent.Controls.Add(lblDesNatTargetDB, 0, 4);
            tlpContent.Controls.Add(lblDesNatSourceDB, 0, 3);
            tlpContent.Controls.Add(txtLabelNatTargetDB, 1, 1);
            tlpContent.Controls.Add(txtLabelNatSourceDB, 1, 0);
            tlpContent.Controls.Add(lblLabelNatTargetDB, 0, 1);
            tlpContent.Controls.Add(btnUpdateLabelNat, 1, 2);
            tlpContent.Controls.Add(btnUpdateDesNat, 1, 5);
            tlpContent.Controls.Add(txtDesEnSourceDB, 1, 9);
            tlpContent.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpContent.Location = new System.Drawing.Point(0, 0);
            tlpContent.Name = "tlpContent";
            tlpContent.RowCount = 12;
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            tlpContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpContent.Size = new System.Drawing.Size(1093, 476);
            tlpContent.TabIndex = 0;
            // 
            // lblDesEnTargetDB
            // 
            lblDesEnTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDesEnTargetDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblDesEnTargetDB.Location = new System.Drawing.Point(4, 390);
            lblDesEnTargetDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblDesEnTargetDB.Name = "lblDesEnTargetDB";
            lblDesEnTargetDB.Size = new System.Drawing.Size(202, 46);
            lblDesEnTargetDB.TabIndex = 1;
            lblDesEnTargetDB.Text = "lblDesEnTargetDB";
            lblDesEnTargetDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDesEnSourceDB
            // 
            lblDesEnSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDesEnSourceDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblDesEnSourceDB.Location = new System.Drawing.Point(4, 344);
            lblDesEnSourceDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblDesEnSourceDB.Name = "lblDesEnSourceDB";
            lblDesEnSourceDB.Size = new System.Drawing.Size(202, 46);
            lblDesEnSourceDB.TabIndex = 0;
            lblDesEnSourceDB.Text = "lblDesEnSourceDB";
            lblDesEnSourceDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelNatSourceDB
            // 
            lblLabelNatSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelNatSourceDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblLabelNatSourceDB.Location = new System.Drawing.Point(4, 0);
            lblLabelNatSourceDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblLabelNatSourceDB.Name = "lblLabelNatSourceDB";
            lblLabelNatSourceDB.Size = new System.Drawing.Size(202, 33);
            lblLabelNatSourceDB.TabIndex = 1;
            lblLabelNatSourceDB.Text = "lblLabelNatSourceDB";
            lblLabelNatSourceDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnUpdateLabelEn
            // 
            btnUpdateLabelEn.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnUpdateLabelEn.AutoSize = true;
            btnUpdateLabelEn.Location = new System.Drawing.Point(949, 307);
            btnUpdateLabelEn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnUpdateLabelEn.Name = "btnUpdateLabelEn";
            btnUpdateLabelEn.Size = new System.Drawing.Size(140, 34);
            btnUpdateLabelEn.TabIndex = 4;
            btnUpdateLabelEn.Text = "btnUpdateLabelEn";
            btnUpdateLabelEn.UseVisualStyleBackColor = true;
            btnUpdateLabelEn.Click += btnUpdateLabelEn_Click;
            // 
            // btnUpdateDesEn
            // 
            btnUpdateDesEn.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnUpdateDesEn.AutoSize = true;
            btnUpdateDesEn.Location = new System.Drawing.Point(949, 439);
            btnUpdateDesEn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnUpdateDesEn.Name = "btnUpdateDesEn";
            btnUpdateDesEn.Size = new System.Drawing.Size(140, 34);
            btnUpdateDesEn.TabIndex = 4;
            btnUpdateDesEn.Text = "btnUpdateDesEn";
            btnUpdateDesEn.UseVisualStyleBackColor = true;
            btnUpdateDesEn.Click += btnUpdateDesEn_Click;
            // 
            // txtDesEnTargetDB
            // 
            txtDesEnTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDesEnTargetDB.Location = new System.Drawing.Point(214, 393);
            txtDesEnTargetDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtDesEnTargetDB.Multiline = true;
            txtDesEnTargetDB.Name = "txtDesEnTargetDB";
            txtDesEnTargetDB.ReadOnly = true;
            txtDesEnTargetDB.Size = new System.Drawing.Size(875, 40);
            txtDesEnTargetDB.TabIndex = 3;
            // 
            // txtLabelEnTargetDB
            // 
            txtLabelEnTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnTargetDB.Location = new System.Drawing.Point(214, 274);
            txtLabelEnTargetDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtLabelEnTargetDB.Multiline = true;
            txtLabelEnTargetDB.Name = "txtLabelEnTargetDB";
            txtLabelEnTargetDB.ReadOnly = true;
            txtLabelEnTargetDB.Size = new System.Drawing.Size(875, 27);
            txtLabelEnTargetDB.TabIndex = 3;
            // 
            // txtLabelEnSourceDB
            // 
            txtLabelEnSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelEnSourceDB.Location = new System.Drawing.Point(214, 241);
            txtLabelEnSourceDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtLabelEnSourceDB.Multiline = true;
            txtLabelEnSourceDB.Name = "txtLabelEnSourceDB";
            txtLabelEnSourceDB.ReadOnly = true;
            txtLabelEnSourceDB.Size = new System.Drawing.Size(875, 27);
            txtLabelEnSourceDB.TabIndex = 2;
            // 
            // lblLabelEnTargetDB
            // 
            lblLabelEnTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnTargetDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblLabelEnTargetDB.Location = new System.Drawing.Point(4, 271);
            lblLabelEnTargetDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblLabelEnTargetDB.Name = "lblLabelEnTargetDB";
            lblLabelEnTargetDB.Size = new System.Drawing.Size(202, 33);
            lblLabelEnTargetDB.TabIndex = 1;
            lblLabelEnTargetDB.Text = "lblLabelEnTargetDB";
            lblLabelEnTargetDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelEnSourceDB
            // 
            lblLabelEnSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelEnSourceDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblLabelEnSourceDB.Location = new System.Drawing.Point(4, 238);
            lblLabelEnSourceDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblLabelEnSourceDB.Name = "lblLabelEnSourceDB";
            lblLabelEnSourceDB.Size = new System.Drawing.Size(202, 33);
            lblLabelEnSourceDB.TabIndex = 0;
            lblLabelEnSourceDB.Text = "lblLabelEnSourceDB";
            lblLabelEnSourceDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDesNatTargetDB
            // 
            txtDesNatTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDesNatTargetDB.Location = new System.Drawing.Point(214, 155);
            txtDesNatTargetDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtDesNatTargetDB.Multiline = true;
            txtDesNatTargetDB.Name = "txtDesNatTargetDB";
            txtDesNatTargetDB.ReadOnly = true;
            txtDesNatTargetDB.Size = new System.Drawing.Size(875, 40);
            txtDesNatTargetDB.TabIndex = 9;
            // 
            // txtDesNatSourceDB
            // 
            txtDesNatSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDesNatSourceDB.Location = new System.Drawing.Point(214, 109);
            txtDesNatSourceDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtDesNatSourceDB.Multiline = true;
            txtDesNatSourceDB.Name = "txtDesNatSourceDB";
            txtDesNatSourceDB.ReadOnly = true;
            txtDesNatSourceDB.Size = new System.Drawing.Size(875, 40);
            txtDesNatSourceDB.TabIndex = 8;
            // 
            // lblDesNatTargetDB
            // 
            lblDesNatTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDesNatTargetDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblDesNatTargetDB.Location = new System.Drawing.Point(4, 152);
            lblDesNatTargetDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblDesNatTargetDB.Name = "lblDesNatTargetDB";
            lblDesNatTargetDB.Size = new System.Drawing.Size(202, 46);
            lblDesNatTargetDB.TabIndex = 7;
            lblDesNatTargetDB.Text = "lblDesNatTargetDB";
            lblDesNatTargetDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDesNatSourceDB
            // 
            lblDesNatSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDesNatSourceDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblDesNatSourceDB.Location = new System.Drawing.Point(4, 106);
            lblDesNatSourceDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblDesNatSourceDB.Name = "lblDesNatSourceDB";
            lblDesNatSourceDB.Size = new System.Drawing.Size(202, 46);
            lblDesNatSourceDB.TabIndex = 6;
            lblDesNatSourceDB.Text = "lblDesNatSourceDB";
            lblDesNatSourceDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLabelNatTargetDB
            // 
            txtLabelNatTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelNatTargetDB.Location = new System.Drawing.Point(214, 36);
            txtLabelNatTargetDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtLabelNatTargetDB.Multiline = true;
            txtLabelNatTargetDB.Name = "txtLabelNatTargetDB";
            txtLabelNatTargetDB.ReadOnly = true;
            txtLabelNatTargetDB.Size = new System.Drawing.Size(875, 27);
            txtLabelNatTargetDB.TabIndex = 3;
            // 
            // txtLabelNatSourceDB
            // 
            txtLabelNatSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLabelNatSourceDB.Location = new System.Drawing.Point(214, 3);
            txtLabelNatSourceDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtLabelNatSourceDB.Multiline = true;
            txtLabelNatSourceDB.Name = "txtLabelNatSourceDB";
            txtLabelNatSourceDB.ReadOnly = true;
            txtLabelNatSourceDB.Size = new System.Drawing.Size(875, 27);
            txtLabelNatSourceDB.TabIndex = 2;
            // 
            // lblLabelNatTargetDB
            // 
            lblLabelNatTargetDB.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelNatTargetDB.ForeColor = System.Drawing.SystemColors.HotTrack;
            lblLabelNatTargetDB.Location = new System.Drawing.Point(4, 33);
            lblLabelNatTargetDB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblLabelNatTargetDB.Name = "lblLabelNatTargetDB";
            lblLabelNatTargetDB.Size = new System.Drawing.Size(202, 33);
            lblLabelNatTargetDB.TabIndex = 1;
            lblLabelNatTargetDB.Text = "lblLabelNatTargetDB";
            lblLabelNatTargetDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnUpdateLabelNat
            // 
            btnUpdateLabelNat.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnUpdateLabelNat.AutoSize = true;
            btnUpdateLabelNat.Location = new System.Drawing.Point(949, 69);
            btnUpdateLabelNat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnUpdateLabelNat.Name = "btnUpdateLabelNat";
            btnUpdateLabelNat.Size = new System.Drawing.Size(140, 34);
            btnUpdateLabelNat.TabIndex = 5;
            btnUpdateLabelNat.Text = "btnUpdateLabelNat";
            btnUpdateLabelNat.UseVisualStyleBackColor = true;
            btnUpdateLabelNat.Click += btnUpdateLabelNat_Click;
            // 
            // btnUpdateDesNat
            // 
            btnUpdateDesNat.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnUpdateDesNat.AutoSize = true;
            btnUpdateDesNat.Location = new System.Drawing.Point(949, 201);
            btnUpdateDesNat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btnUpdateDesNat.Name = "btnUpdateDesNat";
            btnUpdateDesNat.Size = new System.Drawing.Size(140, 34);
            btnUpdateDesNat.TabIndex = 10;
            btnUpdateDesNat.Text = "btnUpdateDesNat";
            btnUpdateDesNat.UseVisualStyleBackColor = true;
            btnUpdateDesNat.Click += btnUpdateDesNat_Click;
            // 
            // txtDesEnSourceDB
            // 
            txtDesEnSourceDB.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDesEnSourceDB.Location = new System.Drawing.Point(214, 347);
            txtDesEnSourceDB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtDesEnSourceDB.Multiline = true;
            txtDesEnSourceDB.Name = "txtDesEnSourceDB";
            txtDesEnSourceDB.ReadOnly = true;
            txtDesEnSourceDB.Size = new System.Drawing.Size(875, 40);
            txtDesEnSourceDB.TabIndex = 2;
            // 
            // FormCheckAdSp
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            AutoScroll = true;
            ClientSize = new System.Drawing.Size(1101, 845);
            Controls.Add(tlpMain);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MinimizeBox = false;
            Name = "FormCheckAdSp";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            pnlUpperButtons.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            splTables.Panel1.ResumeLayout(false);
            splTables.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splTables).EndInit();
            splTables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvType).EndInit();
            ((System.ComponentModel.ISupportInitialize)dgv).EndInit();
            tlpContent.ResumeLayout(false);
            tlpContent.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlUpperButtons;
        private System.Windows.Forms.Panel pnlButtons;

        /// <summary>
        /// btnUpdateAll
        /// </summary>
        public System.Windows.Forms.Button btnUpdateAll;

        /// <summary>
        /// btnClose
        /// </summary>
        public System.Windows.Forms.Button btnClose;

        /// <summary>
        /// btnSPC
        /// </summary>
        public System.Windows.Forms.Button btnSPC;

        /// <summary>
        /// btnSP
        /// </summary>
        public System.Windows.Forms.Button btnSP;

        /// <summary>
        /// btnADC
        /// </summary>
        public System.Windows.Forms.Button btnADC;

        /// <summary>
        /// btnAD
        /// </summary>
        public System.Windows.Forms.Button btnAD;

        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.SplitContainer splTables;
        private System.Windows.Forms.Label lblDesNatSourceDB;
        private System.Windows.Forms.Label lblDesNatTargetDB;
        private System.Windows.Forms.TextBox txtDesNatSourceDB;
        private System.Windows.Forms.TextBox txtDesNatTargetDB;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.TextBox txtLabelNatTargetDB;
        private System.Windows.Forms.Label lblLabelEnSourceDB;
        private System.Windows.Forms.Label lblLabelEnTargetDB;
        private System.Windows.Forms.TextBox txtLabelEnSourceDB;
        private System.Windows.Forms.TextBox txtLabelEnTargetDB;
        private System.Windows.Forms.Button btnUpdateLabelEn;
        private System.Windows.Forms.Label lblDesEnSourceDB;
        private System.Windows.Forms.Label lblDesEnTargetDB;
        private System.Windows.Forms.TextBox txtDesEnTargetDB;
        private System.Windows.Forms.Button btnUpdateDesEn;
        private System.Windows.Forms.DataGridView dgvType;
        private System.Windows.Forms.TableLayoutPanel tlpContent;
        private System.Windows.Forms.Label lblLabelNatTargetDB;
        private System.Windows.Forms.TextBox txtLabelNatSourceDB;
        private System.Windows.Forms.Label lblLabelNatSourceDB;
        private System.Windows.Forms.Button btnUpdateLabelNat;
        private System.Windows.Forms.Button btnUpdateDesNat;
        private System.Windows.Forms.TextBox txtDesEnSourceDB;
    }

}