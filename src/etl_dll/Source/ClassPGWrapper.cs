﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System.Collections.Generic;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;

namespace Hanakova
{
    namespace ModuleEtl
    {

        /// <summary>
        /// <para lang="cs">Modifikace třídy NfiEstaDB pro modul ETL</para>
        /// <para lang="en">Modification of NfiEstaDB class for ETL module</para>
        /// </summary>
        public class PGWrapper : NfiEstaDB
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Jazyková verze</para>
            /// <para lang="en">Language version</para>
            /// </summary>
            public LanguageVersion LanguageVersion { get; set; }

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
            /// <para lang="en">File with control labels</para>
            /// </summary>
            public LanguageFile LanguageFile { get; set; }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// <para lang="cs">Inicializuje novou instanci třídy <c>PGWrapper</c> s danou jazykovou verzí a jazykovým souborem. Tím se nastaví potřebné vlastnosti pro správnou lokalizaci textů v aplikaci.</para>
            /// <para lang="en">Initializes a new instance of the <c>PGWrapper</c> class with the specified language version and language file. This sets up the necessary properties for proper localization of texts in the application.</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Jazyková verze, která bude použita pro lokalizaci textů (národní nebo mezinárodní).</para>
            /// <para lang="en">The language version to be used for text localization (national or international).</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Objekt jazykového souboru obsahující překlady textů pro různé části aplikace.</para>
            /// <para lang="en">The language file object containing text translations for various parts of the application.</para>
            /// </param>
            public PGWrapper(LanguageVersion languageVersion, LanguageFile languageFile)
            {
                LanguageVersion = languageVersion;
                LanguageFile = languageFile;
            }

            #endregion Constructor

            #region Static Methods

            /// <summary>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Zvolený jazyk</para>
            /// <para lang="en">Selected language</para>
            /// </param>
            /// <param name="languageFile">
            /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
            /// <para lang="en">File with control labels for national and international version</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Slovník s texty hlášení</para>
            /// <para lang="en">Dictionary with message texts in selected language</para>
            /// </returns>
            public static Dictionary<string, string> Dictionary(
                LanguageVersion languageVersion,
                LanguageFile languageFile)
            {
                switch (languageVersion)
                {
                    case LanguageVersion.National:
                        return
                        (languageFile == null) ?
                        new Dictionary<string, string>()
                        {
                            { "targetDatabase",         "Cílová databáze:" },
                        } :
                        languageFile.NationalVersion.Data.ContainsKey(key: "PGWrapper") ?
                        languageFile.NationalVersion.Data["PGWrapper"] :
                        new Dictionary<string, string>();

                    case LanguageVersion.International:
                        return
                        (languageFile == null) ?
                        new Dictionary<string, string>()
                        {
                            { "targetDatabase",         "Target database:" },
                        } :
                        languageFile.InternationalVersion.Data.ContainsKey(key: "PGWrapper") ?
                        languageFile.InternationalVersion.Data["PGWrapper"] :
                        new Dictionary<string, string>();

                    default:
                        return
                        new Dictionary<string, string>();
                }
            }

            #endregion Static Methods

            #region Methods

            /// <summary>
            /// <para lang="cs">Oznámení o připojení do cílové databáze</para>
            /// <para lang="en">Notification of connection to the target database</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Oznámení o připojení do cílové databáze</para>
            /// <para lang="en">Notification of connection to the target database</para>
            /// </returns>
            public string ConnectionInfoForEtlModule()
            {
                Dictionary<string, string> labels = Dictionary(
                    languageVersion: LanguageVersion,
                    languageFile: LanguageFile);

                string targetDatabase = labels.ContainsKey("targetDatabase") ?
                    labels["targetDatabase"] : string.Empty;

                return $"{targetDatabase} {base.Postgres.ConnectionInfo()}";
            }

            #endregion Methods
        }
    }
}