﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class ControlTargetVariable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.tlpMiddlePart = new System.Windows.Forms.TableLayoutPanel();
            this.pnlTargetVariableSelector = new System.Windows.Forms.Panel();
            this.tlpOtherInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lblAtomicAreaDomain = new System.Windows.Forms.Label();
            this.lblAtomicSubPopulation = new System.Windows.Forms.Label();
            this.lblAtomicAreaDomainValue = new System.Windows.Forms.Label();
            this.lblAtomicSubPopulationValue = new System.Windows.Forms.Label();
            this.pnlOtherInfo = new System.Windows.Forms.Panel();
            this.cmbRefYearSetToPanelMapping = new System.Windows.Forms.ComboBox();
            this.cmbIdEtlTargetVariable = new System.Windows.Forms.ComboBox();
            this.cmbIndicatorLabelEn = new System.Windows.Forms.ComboBox();
            this.cmbIndicatorLabelCs = new System.Windows.Forms.ComboBox();
            this.cmbTargetVariableId = new System.Windows.Forms.ComboBox();
            this.lblToHideCmbTargetVariableId = new System.Windows.Forms.Label();
            this.tlpMain.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.tlpMiddlePart.SuspendLayout();
            this.tlpOtherInfo.SuspendLayout();
            this.pnlOtherInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.pnlButtons, 0, 2);
            this.tlpMain.Controls.Add(this.lblMainCaption, 0, 0);
            this.tlpMain.Controls.Add(this.tlpMiddlePart, 0, 1);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.TabIndex = 2;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnPrevious);
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(3, 503);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(954, 34);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevious.Location = new System.Drawing.Point(645, 3);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(150, 30);
            this.btnPrevious.TabIndex = 1;
            this.btnPrevious.Text = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.BtnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(801, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.AutoSize = true;
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(3, 0);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(954, 30);
            this.lblMainCaption.TabIndex = 1;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpMiddlePart
            // 
            this.tlpMiddlePart.ColumnCount = 1;
            this.tlpMiddlePart.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMiddlePart.Controls.Add(this.pnlTargetVariableSelector, 0, 0);
            this.tlpMiddlePart.Controls.Add(this.tlpOtherInfo, 0, 1);
            this.tlpMiddlePart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMiddlePart.Location = new System.Drawing.Point(3, 33);
            this.tlpMiddlePart.Name = "tlpMiddlePart";
            this.tlpMiddlePart.RowCount = 2;
            this.tlpMiddlePart.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMiddlePart.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpMiddlePart.Size = new System.Drawing.Size(954, 464);
            this.tlpMiddlePart.TabIndex = 2;
            // 
            // pnlTargetVariableSelector
            // 
            this.pnlTargetVariableSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTargetVariableSelector.Location = new System.Drawing.Point(3, 3);
            this.pnlTargetVariableSelector.Name = "pnlTargetVariableSelector";
            this.pnlTargetVariableSelector.Size = new System.Drawing.Size(948, 408);
            this.pnlTargetVariableSelector.TabIndex = 0;
            // 
            // tlpOtherInfo
            // 
            this.tlpOtherInfo.ColumnCount = 3;
            this.tlpOtherInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tlpOtherInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.46524F));
            this.tlpOtherInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.53476F));
            this.tlpOtherInfo.Controls.Add(this.lblAtomicAreaDomain, 0, 0);
            this.tlpOtherInfo.Controls.Add(this.lblAtomicSubPopulation, 0, 1);
            this.tlpOtherInfo.Controls.Add(this.lblAtomicAreaDomainValue, 1, 0);
            this.tlpOtherInfo.Controls.Add(this.lblAtomicSubPopulationValue, 1, 1);
            this.tlpOtherInfo.Controls.Add(this.pnlOtherInfo, 2, 1);
            this.tlpOtherInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpOtherInfo.Location = new System.Drawing.Point(3, 417);
            this.tlpOtherInfo.Name = "tlpOtherInfo";
            this.tlpOtherInfo.RowCount = 2;
            this.tlpOtherInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOtherInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOtherInfo.Size = new System.Drawing.Size(948, 44);
            this.tlpOtherInfo.TabIndex = 1;
            // 
            // lblAtomicAreaDomain
            // 
            this.lblAtomicAreaDomain.AutoSize = true;
            this.lblAtomicAreaDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAtomicAreaDomain.Location = new System.Drawing.Point(3, 0);
            this.lblAtomicAreaDomain.Name = "lblAtomicAreaDomain";
            this.lblAtomicAreaDomain.Size = new System.Drawing.Size(194, 20);
            this.lblAtomicAreaDomain.TabIndex = 20;
            this.lblAtomicAreaDomain.Text = "lblAtomicAreaDomain";
            this.lblAtomicAreaDomain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAtomicAreaDomain.Visible = false;
            // 
            // lblAtomicSubPopulation
            // 
            this.lblAtomicSubPopulation.AutoSize = true;
            this.lblAtomicSubPopulation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAtomicSubPopulation.Location = new System.Drawing.Point(3, 20);
            this.lblAtomicSubPopulation.Name = "lblAtomicSubPopulation";
            this.lblAtomicSubPopulation.Size = new System.Drawing.Size(194, 24);
            this.lblAtomicSubPopulation.TabIndex = 21;
            this.lblAtomicSubPopulation.Text = "lblAtomicSubPopulation";
            this.lblAtomicSubPopulation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAtomicSubPopulation.Visible = false;
            // 
            // lblAtomicAreaDomainValue
            // 
            this.lblAtomicAreaDomainValue.AutoSize = true;
            this.lblAtomicAreaDomainValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAtomicAreaDomainValue.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblAtomicAreaDomainValue.Location = new System.Drawing.Point(203, 0);
            this.lblAtomicAreaDomainValue.Name = "lblAtomicAreaDomainValue";
            this.lblAtomicAreaDomainValue.Size = new System.Drawing.Size(177, 20);
            this.lblAtomicAreaDomainValue.TabIndex = 22;
            this.lblAtomicAreaDomainValue.Text = "lblAtomicAreaDomainValue";
            this.lblAtomicAreaDomainValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAtomicAreaDomainValue.Visible = false;
            // 
            // lblAtomicSubPopulationValue
            // 
            this.lblAtomicSubPopulationValue.AutoSize = true;
            this.lblAtomicSubPopulationValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAtomicSubPopulationValue.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblAtomicSubPopulationValue.Location = new System.Drawing.Point(203, 20);
            this.lblAtomicSubPopulationValue.Name = "lblAtomicSubPopulationValue";
            this.lblAtomicSubPopulationValue.Size = new System.Drawing.Size(177, 24);
            this.lblAtomicSubPopulationValue.TabIndex = 23;
            this.lblAtomicSubPopulationValue.Text = "lblAtomicSubPopulationValue";
            this.lblAtomicSubPopulationValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAtomicSubPopulationValue.Visible = false;
            // 
            // pnlOtherInfo
            // 
            this.pnlOtherInfo.Controls.Add(this.lblToHideCmbTargetVariableId);
            this.pnlOtherInfo.Controls.Add(this.cmbRefYearSetToPanelMapping);
            this.pnlOtherInfo.Controls.Add(this.cmbIdEtlTargetVariable);
            this.pnlOtherInfo.Controls.Add(this.cmbIndicatorLabelEn);
            this.pnlOtherInfo.Controls.Add(this.cmbIndicatorLabelCs);
            this.pnlOtherInfo.Controls.Add(this.cmbTargetVariableId);
            this.pnlOtherInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOtherInfo.Location = new System.Drawing.Point(386, 23);
            this.pnlOtherInfo.Name = "pnlOtherInfo";
            this.pnlOtherInfo.Size = new System.Drawing.Size(559, 18);
            this.pnlOtherInfo.TabIndex = 26;
            // 
            // cmbRefYearSetToPanelMapping
            // 
            this.cmbRefYearSetToPanelMapping.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRefYearSetToPanelMapping.FormattingEnabled = true;
            this.cmbRefYearSetToPanelMapping.Location = new System.Drawing.Point(332, 0);
            this.cmbRefYearSetToPanelMapping.Name = "cmbRefYearSetToPanelMapping";
            this.cmbRefYearSetToPanelMapping.Size = new System.Drawing.Size(125, 21);
            this.cmbRefYearSetToPanelMapping.TabIndex = 19;
            this.cmbRefYearSetToPanelMapping.Visible = false;
            // 
            // cmbIdEtlTargetVariable
            // 
            this.cmbIdEtlTargetVariable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIdEtlTargetVariable.FormattingEnabled = true;
            this.cmbIdEtlTargetVariable.Location = new System.Drawing.Point(280, -1);
            this.cmbIdEtlTargetVariable.Name = "cmbIdEtlTargetVariable";
            this.cmbIdEtlTargetVariable.Size = new System.Drawing.Size(46, 21);
            this.cmbIdEtlTargetVariable.TabIndex = 17;
            this.cmbIdEtlTargetVariable.Visible = false;
            // 
            // cmbIndicatorLabelEn
            // 
            this.cmbIndicatorLabelEn.FormattingEnabled = true;
            this.cmbIndicatorLabelEn.Location = new System.Drawing.Point(100, -2);
            this.cmbIndicatorLabelEn.Name = "cmbIndicatorLabelEn";
            this.cmbIndicatorLabelEn.Size = new System.Drawing.Size(92, 21);
            this.cmbIndicatorLabelEn.TabIndex = 25;
            this.cmbIndicatorLabelEn.Visible = false;
            // 
            // cmbIndicatorLabelCs
            // 
            this.cmbIndicatorLabelCs.FormattingEnabled = true;
            this.cmbIndicatorLabelCs.Location = new System.Drawing.Point(3, -3);
            this.cmbIndicatorLabelCs.Name = "cmbIndicatorLabelCs";
            this.cmbIndicatorLabelCs.Size = new System.Drawing.Size(94, 21);
            this.cmbIndicatorLabelCs.TabIndex = 24;
            this.cmbIndicatorLabelCs.Visible = false;
            // 
            // cmbTargetVariableId
            // 
            this.cmbTargetVariableId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTargetVariableId.FormattingEnabled = true;
            this.cmbTargetVariableId.Location = new System.Drawing.Point(198, 0);
            this.cmbTargetVariableId.Name = "cmbTargetVariableId";
            this.cmbTargetVariableId.Size = new System.Drawing.Size(46, 21);
            this.cmbTargetVariableId.TabIndex = 18;
            this.cmbTargetVariableId.SelectedIndexChanged += new System.EventHandler(this.CmbTargetVariableId_SelectedIndexChanged);
            // 
            // lblToHideCmbTargetVariableId
            // 
            this.lblToHideCmbTargetVariableId.Location = new System.Drawing.Point(195, -1);
            this.lblToHideCmbTargetVariableId.Name = "lblToHideCmbTargetVariableId";
            this.lblToHideCmbTargetVariableId.Size = new System.Drawing.Size(73, 22);
            this.lblToHideCmbTargetVariableId.TabIndex = 26;
            // 
            // ControlTargetVariable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpMain);
            this.Name = "ControlTargetVariable";
            this.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.tlpMiddlePart.ResumeLayout(false);
            this.tlpOtherInfo.ResumeLayout(false);
            this.tlpOtherInfo.PerformLayout();
            this.pnlOtherInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        public System.Windows.Forms.Button btnPrevious;
        public System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.ComboBox cmbIdEtlTargetVariable;
        private System.Windows.Forms.ComboBox cmbTargetVariableId;
        private System.Windows.Forms.ComboBox cmbRefYearSetToPanelMapping;
        private System.Windows.Forms.Label lblAtomicAreaDomain;
        private System.Windows.Forms.Label lblAtomicSubPopulation;
        private System.Windows.Forms.Label lblAtomicAreaDomainValue;
        private System.Windows.Forms.Label lblAtomicSubPopulationValue;
        private System.Windows.Forms.ComboBox cmbIndicatorLabelCs;
        private System.Windows.Forms.ComboBox cmbIndicatorLabelEn;
        private System.Windows.Forms.TableLayoutPanel tlpMiddlePart;
        private System.Windows.Forms.Panel pnlTargetVariableSelector;
        private System.Windows.Forms.TableLayoutPanel tlpOtherInfo;
        private System.Windows.Forms.Panel pnlOtherInfo;
        private System.Windows.Forms.Label lblToHideCmbTargetVariableId;
    }

}
