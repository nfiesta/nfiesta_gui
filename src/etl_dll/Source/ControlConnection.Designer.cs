﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace Hanakova.ModuleEtl
{

    partial class ControlConnection
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlConnection));
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnCheckAdSp = new System.Windows.Forms.Button();
            this.btnCheckMetadata = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblMainCaption = new System.Windows.Forms.Label();
            this.tlpMiddlePart = new System.Windows.Forms.TableLayoutPanel();
            this.grpConnection = new System.Windows.Forms.GroupBox();
            this.tlpConnection = new System.Windows.Forms.TableLayoutPanel();
            this.tsrConnection = new System.Windows.Forms.ToolStrip();
            this.btnConnectionAdd = new System.Windows.Forms.ToolStripButton();
            this.btnConnectionUpdate = new System.Windows.Forms.ToolStripButton();
            this.btnConnectionDelete = new System.Windows.Forms.ToolStripButton();
            this.pnlConnection = new System.Windows.Forms.Panel();
            this.tlpMain.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.tlpMiddlePart.SuspendLayout();
            this.grpConnection.SuspendLayout();
            this.tlpConnection.SuspendLayout();
            this.tsrConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.pnlButtons, 0, 2);
            this.tlpMain.Controls.Add(this.lblMainCaption, 0, 0);
            this.tlpMain.Controls.Add(this.tlpMiddlePart, 0, 1);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.TabIndex = 0;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnCheckAdSp);
            this.pnlButtons.Controls.Add(this.btnCheckMetadata);
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(3, 503);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(954, 34);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnCheckAdSp
            // 
            this.btnCheckAdSp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckAdSp.Enabled = false;
            this.btnCheckAdSp.Location = new System.Drawing.Point(489, 3);
            this.btnCheckAdSp.Name = "btnCheckAdSp";
            this.btnCheckAdSp.Size = new System.Drawing.Size(150, 30);
            this.btnCheckAdSp.TabIndex = 3;
            this.btnCheckAdSp.Text = "btnCheckAdSp";
            this.btnCheckAdSp.UseVisualStyleBackColor = true;
            this.btnCheckAdSp.Click += new System.EventHandler(this.btnCheckAdSp_Click);
            // 
            // btnCheckMetadata
            // 
            this.btnCheckMetadata.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckMetadata.Enabled = false;
            this.btnCheckMetadata.Location = new System.Drawing.Point(645, 3);
            this.btnCheckMetadata.Name = "btnCheckMetadata";
            this.btnCheckMetadata.Size = new System.Drawing.Size(150, 30);
            this.btnCheckMetadata.TabIndex = 2;
            this.btnCheckMetadata.Text = "btnCheckMetadata";
            this.btnCheckMetadata.UseVisualStyleBackColor = true;
            this.btnCheckMetadata.Click += new System.EventHandler(this.BtnCheckMetadata_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(801, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(150, 30);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // lblMainCaption
            // 
            this.lblMainCaption.AutoSize = true;
            this.lblMainCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMainCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblMainCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMainCaption.Location = new System.Drawing.Point(3, 0);
            this.lblMainCaption.Name = "lblMainCaption";
            this.lblMainCaption.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblMainCaption.Size = new System.Drawing.Size(954, 30);
            this.lblMainCaption.TabIndex = 1;
            this.lblMainCaption.Text = "lblMainCaption";
            this.lblMainCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpMiddlePart
            // 
            this.tlpMiddlePart.ColumnCount = 2;
            this.tlpMiddlePart.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.95178F));
            this.tlpMiddlePart.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.048218F));
            this.tlpMiddlePart.Controls.Add(this.grpConnection, 0, 0);
            this.tlpMiddlePart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMiddlePart.Location = new System.Drawing.Point(3, 33);
            this.tlpMiddlePart.Name = "tlpMiddlePart";
            this.tlpMiddlePart.RowCount = 1;
            this.tlpMiddlePart.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMiddlePart.Size = new System.Drawing.Size(954, 464);
            this.tlpMiddlePart.TabIndex = 2;
            // 
            // grpConnection
            // 
            this.grpConnection.Controls.Add(this.tlpConnection);
            this.grpConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grpConnection.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpConnection.Location = new System.Drawing.Point(3, 3);
            this.grpConnection.Name = "grpConnection";
            this.grpConnection.Size = new System.Drawing.Size(938, 458);
            this.grpConnection.TabIndex = 0;
            this.grpConnection.TabStop = false;
            this.grpConnection.Text = "grpConnection";
            // 
            // tlpConnection
            // 
            this.tlpConnection.ColumnCount = 1;
            this.tlpConnection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpConnection.Controls.Add(this.tsrConnection, 0, 0);
            this.tlpConnection.Controls.Add(this.pnlConnection, 0, 1);
            this.tlpConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpConnection.Location = new System.Drawing.Point(3, 18);
            this.tlpConnection.Name = "tlpConnection";
            this.tlpConnection.RowCount = 2;
            this.tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpConnection.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpConnection.Size = new System.Drawing.Size(932, 437);
            this.tlpConnection.TabIndex = 0;
            // 
            // tsrConnection
            // 
            this.tsrConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsrConnection.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tsrConnection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnConnectionAdd,
            this.btnConnectionUpdate,
            this.btnConnectionDelete});
            this.tsrConnection.Location = new System.Drawing.Point(0, 0);
            this.tsrConnection.Name = "tsrConnection";
            this.tsrConnection.Size = new System.Drawing.Size(932, 25);
            this.tsrConnection.TabIndex = 1;
            this.tsrConnection.Text = "tsrConnection";
            // 
            // btnConnectionAdd
            // 
            this.btnConnectionAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnConnectionAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnConnectionAdd.Image")));
            this.btnConnectionAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConnectionAdd.Name = "btnConnectionAdd";
            this.btnConnectionAdd.Size = new System.Drawing.Size(24, 22);
            this.btnConnectionAdd.Text = "btnConnectionAdd";
            this.btnConnectionAdd.Click += new System.EventHandler(this.BtnConnectionAdd_Click);
            // 
            // btnConnectionUpdate
            // 
            this.btnConnectionUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnConnectionUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnConnectionUpdate.Image")));
            this.btnConnectionUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConnectionUpdate.Name = "btnConnectionUpdate";
            this.btnConnectionUpdate.Size = new System.Drawing.Size(24, 22);
            this.btnConnectionUpdate.Text = "btnConnectionUpdate";
            this.btnConnectionUpdate.Click += new System.EventHandler(this.BtnConnectionUpdate_Click);
            // 
            // btnConnectionDelete
            // 
            this.btnConnectionDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnConnectionDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnConnectionDelete.Image")));
            this.btnConnectionDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConnectionDelete.Name = "btnConnectionDelete";
            this.btnConnectionDelete.Size = new System.Drawing.Size(24, 22);
            this.btnConnectionDelete.Text = "btnConnectionDelete";
            this.btnConnectionDelete.Click += new System.EventHandler(this.BtnConnectionDelete_Click);
            // 
            // pnlConnection
            // 
            this.pnlConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlConnection.Location = new System.Drawing.Point(3, 28);
            this.pnlConnection.Name = "pnlConnection";
            this.pnlConnection.Size = new System.Drawing.Size(926, 406);
            this.pnlConnection.TabIndex = 2;
            // 
            // ControlConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpMain);
            this.Name = "ControlConnection";
            this.Size = new System.Drawing.Size(960, 540);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.tlpMiddlePart.ResumeLayout(false);
            this.grpConnection.ResumeLayout(false);
            this.tlpConnection.ResumeLayout(false);
            this.tlpConnection.PerformLayout();
            this.tsrConnection.ResumeLayout(false);
            this.tsrConnection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Label lblMainCaption;
        private System.Windows.Forms.TableLayoutPanel tlpMiddlePart;
        private System.Windows.Forms.GroupBox grpConnection;
        private System.Windows.Forms.TableLayoutPanel tlpConnection;
        private System.Windows.Forms.ToolStrip tsrConnection;
        private System.Windows.Forms.ToolStripButton btnConnectionAdd;
        private System.Windows.Forms.ToolStripButton btnConnectionUpdate;
        private System.Windows.Forms.ToolStripButton btnConnectionDelete;
        private System.Windows.Forms.Panel pnlConnection;
        public System.Windows.Forms.Button btnNext;
        public System.Windows.Forms.Button btnCheckMetadata;
        public System.Windows.Forms.Button btnCheckAdSp;
    }

}
