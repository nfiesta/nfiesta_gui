﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Controls;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
    /// <para lang="en">Control "Selection of the target variable"</para>
    /// </summary>
    internal partial class ControlTargetVariable
            : UserControl, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Připojení do cílové databáze"</para>
        /// <para lang="en">Control "Connection to the target database"</para>
        /// </summary>
        private ControlConnection ctrConnection;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL plošné domény a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of area domain and its catgories"</para>
        /// </summary>
        private ControlAreaDomain controlAreaDomain;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "ETL subpopulace a jejích kategorií"</para>
        /// <para lang="en">Control "ETL of subpopulation and its catgories"</para>
        /// </summary>
        private ControlSubPopulation controlSubPopulation;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba kombinací panelů a refyearsetů"</para>
        /// <para lang="en">Control "Select panel and refyearset combinations"</para>
        /// </summary>
        private ControlPanelRefyearset controlPanelRefyearset;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Porovnání zvolené cílové proměnné s cílovými proměnnými, které už v modulu Odhady jsou"</para>
        /// <para lang="en">Control "Comparison of the selected target variable with target variables being already in Estimates"</para>
        /// </summary>
        private ControlTargetVariableCompare ctrTargetVariableCompare;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private int selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        public ControlTargetVariable(
            Control controlOwner,
            int selectedExportConnectionId)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                selectedExportConnectionId: selectedExportConnectionId);

            ControlEtl.LanguageChanged += OnLanguageChanged;
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlEtl ctrEtl)
                {
                    return ctrEtl;
                }
                else if (ControlOwner is ControlPanelRefyearset ctrPanelRefYearSet)
                {
                    return ctrPanelRefYearSet.CtrEtl;
                }
                else if (ControlOwner is FormCommit frmCommit)
                {
                    return frmCommit.CtrEtl;
                }
                else if (ControlOwner is ControlConnection ctrConnection)
                {
                    return ctrConnection.CtrEtl;
                }
                else if (ControlOwner is ControlTargetVariable ctrTargetVariable)
                {
                    return ctrTargetVariable.CtrEtl;
                }
                else if (ControlOwner is ControlTargetVariableCompare ctrTargetVariableCompare)
                {
                    return ctrTargetVariableCompare.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        public int SelectedExportConnectionId
        {
            get
            {
                return selectedExportConnectionId;
            }
            set
            {
                selectedExportConnectionId = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Id zvolené cílové proměnné</para>
        /// <para lang="en">Id of the selected target variable</para>
        /// </summary>
        public int SelectedTargetVariableId { get; set; }

        /// <summary>
        /// <para lang="cs">Popisek zvoleného indikátoru</para>
        /// <para lang="en">Label of the selected indicator</para>
        /// </summary>
        public int SelectedIndicatorLabel { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolený indikátor je stavový, nebo změnový</para>
        /// <para lang="en">The selected indicator is a state or change one</para>
        /// </summary>
        public int SelectedStateOrChange { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolená jednotka</para>
        /// <para lang="en">The selected unit</para>
        /// </summary>
        public int SelectedUnit { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolený příspěvek lokální hustoty</para>
        /// <para lang="en">The selected local density contribution</para>
        /// </summary>
        public int SelectedLocalDensity { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolená verze</para>
        /// <para lang="en">The selected version</para>
        /// </summary>
        public int SelectedVersion { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolená definiční varianta</para>
        /// <para lang="en">The selected definition variant</para>
        /// </summary>
        public int SelectedDefinitionVariant { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolená plošná doména</para>
        /// <para lang="en">The selected area domain</para>
        /// </summary>
        public int SelectedAreaDomain { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolená populace</para>
        /// <para lang="en">The selected population</para>
        /// </summary>
        public int SelectedPopulation { get; set; }

        /// <summary>
        /// <para lang="cs">Id vrácené funkcí fn_etl_save_target_variable. Je uloženo v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id returned by the fn_etl_save_target_variable function. It is saved in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        public int? IdEtlFromControlTargetVariable { get; set; }

        /// <summary>
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        public int? IdEtl { get; set; }

        /// <summary>
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů, který vrací fce fn_etl_get_target_variable</para>
        /// <para lang="en">List of panel and refyearset combination ids returned by fn_etl_get_target_variable</para>
        /// </summary>
        public List<int?> RefYearSetToPanelMapping { get; set; }

        /// <summary>
        /// <para lang="cs">Atomické plošné domény</para>
        /// <para lang="en">Atomic area domains</para>
        /// </summary>
        public bool? AtomicAreaDomains { get; set; }

        /// <summary>
        /// <para lang="cs">Atomické subpopulace</para>
        /// <para lang="en">Atomic subpopulations</para>
        /// </summary>
        public bool? AtomicSubPopulations { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </summary>
        public string SelectedIndicatorLabelCs { get; set; }

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </summary>
        public string SelectedIndicatorLabelEn { get; set; }

        #endregion Properties


        #region Private Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Control "Target variable selection from json file of metadata"</para>
        /// </summary>
        public ControlTargetVariableSelector CtrTargetVariableSelector
        {
            get
            {
                if (pnlTargetVariableSelector.Controls.Count != 1)
                {
                    CreateNewTargetVariableSelector();
                }
                if (!(pnlTargetVariableSelector.Controls[0] is ControlTargetVariableSelector))
                {
                    CreateNewTargetVariableSelector();
                }
                return (ControlTargetVariableSelector)pnlTargetVariableSelector.Controls[0];
            }
        }

        #endregion Private Controls


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                 "Zdrojová databáze: Volba cílové proměnné" },
                            { "lblAtomicAreaDomain",            "Chybí atomické plošné domény?" },
                            { "lblAtomicSubPopulation",         "Chybí atomické subpopulace?" },
                            { "lblAtomicAreaDomainValueYes",    "ano"},
                            { "lblAtomicAreaDomainValueNo",     "ne"},
                            { "lblAtomicSubPopulationValueYes", "ano"},
                            { "lblAtomicSubPopulationValueNo",  "ne"},
                            { "ctrMetadataIndicatorText",       "Indikátor:" },
                            { "ctrMetadataElementsText",        "Metadata:" },
                            { "btnPrevious",                    "Předchozí" },
                            { "btnNext",                        "Další" },
                            { "msgNoRecord",                    "Ve zdrojové databázi nejsou žádná data, která by bylo možné přenést do cílové databáze. Proces přenosu dat bude ukončen." },
                            { "msgCheckMetadata",               "Zvolená cílová proměnná už byla do cílové databáze přenesena. Zkontrolovat metadata?"},
                            { "msgMetadataOK",                  "Metadata jsou v pořádku. Pokračovat v procesu přenosu dat?"}
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlTargetVariable") ?
                    languageFile.NationalVersion.Data["ControlTargetVariable"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                 "Source database: Selection of the target variable" },
                            { "lblAtomicAreaDomain",            "Are atomic area domains missing?" },
                            { "lblAtomicSubPopulation",         "Are atomic subpopulations missing?" },
                            { "lblAtomicAreaDomainValueYes",    "yes"},
                            { "lblAtomicAreaDomainValueNo",     "no"},
                            { "lblAtomicSubPopulationValueYes", "yes"},
                            { "lblAtomicSubPopulationValueNo",  "no"},
                            { "ctrMetadataIndicatorText",       "Indicator:" },
                            { "ctrMetadataElementsText",        "Metadata:" },
                            { "btnPrevious",                    "Previous" },
                            { "btnNext",                        "Next" },
                            { "msgNoRecord",                    "There is no data in the source database that can be transferred to the target database. The process of data transfer will be terminated." },
                            { "msgCheckMetadata",               "The selected target variable has already been transferred to the target database. Check the metadata?"},
                            { "msgMetadataOK",                  "The metadata is OK. Continue the data transfer process?"}
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlTargetVariable") ?
                    languageFile.InternationalVersion.Data["ControlTargetVariable"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            int selectedExportConnectionId)
        {
            ControlOwner = controlOwner;
            SelectedExportConnectionId = selectedExportConnectionId;
            nationalLang = new NationalLang();

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Cursor = Cursors.WaitCursor;

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            lblMainCaption.Text =
                 labels.ContainsKey(key: "lblMainCaption") ?
                 labels["lblMainCaption"] : String.Empty;

            CtrTargetVariableSelector.MetadataIndicatorText =
                labels.ContainsKey(key: "ctrMetadataIndicatorText") ?
                labels["ctrMetadataIndicatorText"] : String.Empty;

            CtrTargetVariableSelector.MetadataElementsText =
                labels.ContainsKey(key: "ctrMetadataElementsText") ?
                labels["ctrMetadataElementsText"] : String.Empty;

            CtrTargetVariableSelector.InitializeLabels();

            lblAtomicAreaDomain.Text =
                labels.ContainsKey(key: "lblAtomicAreaDomain") ?
                labels["lblAtomicAreaDomain"] : String.Empty;

            lblAtomicSubPopulation.Text =
                labels.ContainsKey(key: "lblAtomicSubPopulation") ?
                labels["lblAtomicSubPopulation"] : String.Empty;

            btnPrevious.Text =
                labels.ContainsKey(key: "btnPrevious") ?
                labels["btnPrevious"] : String.Empty;

            btnNext.Text =
                labels.ContainsKey(key: "btnNext") ?
                labels["btnNext"] : String.Empty;

            LoadContent();

            ctrConnection?.InitializeLabels();
            controlAreaDomain?.InitializeLabels();
            controlSubPopulation?.InitializeLabels();
            controlPanelRefyearset?.InitializeLabels();
            ctrTargetVariableCompare?.InitializeLabels();

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            TDFnEtlGetTargetVariableTypeList tdFnEtlGetTargetVariableList =
            TDFunctions.FnEtlGetTargetVariable.Execute(
                database: Database,
                exportConnection: SelectedExportConnectionId,
                nationalLanguage: nationalLang.NatLang,
                etl: false);

            //this code is not necessary, but the next if uses it
            TDFnEtlGetTargetVariableMetadataList tdFnEtlGetTargetVariablesExtendedList =
                new TDFnEtlGetTargetVariableMetadataList(
                    database: Database);
            tdFnEtlGetTargetVariablesExtendedList.ReLoad(
                fnEtlGetTargetVariable: tdFnEtlGetTargetVariableList);

            if (tdFnEtlGetTargetVariablesExtendedList.Items.Count == 0)
            {
                DialogResult dialog = MessageBox.Show(
                    text: labels.ContainsKey(key: "msgNoRecord") ?
                            labels["msgNoRecord"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                if (dialog == DialogResult.OK)
                {
                    //terminate the ETL process and go to the initial window
                    Database.Postgres?.CloseConnection();
                    FormUserPassword.Disconnect();

                    Control parentControlEtl = this.Parent;
                    parentControlEtl.Controls.Remove(this);
                }
            }

            CtrTargetVariableSelector.Data = tdFnEtlGetTargetVariableList;

            CtrTargetVariableSelector.LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Hodnoty cílové proměnné, zvolené v ControlTargetVariable</para>
        /// <para lang="en">Values of the target variable selected in the ControlTargetVariable</para>
        /// </summary>
        public void RefreshValues()
        {
            if (cmbTargetVariableId.Items.Count > 0)
            {
                SelectedTargetVariableId = (int)cmbTargetVariableId.SelectedValue;
            }

            SelectedIndicatorLabel = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
            SelectedStateOrChange = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
            SelectedUnit = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
            SelectedLocalDensity = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
            SelectedVersion = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
            SelectedDefinitionVariant = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
            SelectedAreaDomain = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
            SelectedPopulation = (int)CtrTargetVariableSelector.SelectedTargetVariableIdentifier;
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nový ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Creates new control "Target variable selection from json file of metadata"</para>
        /// </summary>
        private void CreateNewTargetVariableSelector()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            pnlTargetVariableSelector.Controls.Clear();

            ControlTargetVariableSelector ctrTargetVariableSelector =
                new ControlTargetVariableSelector(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    Design = TargetVariableSelectorDesign.Standard,
                    Spacing = new List<int> { 5 },
                    SplitterDistance = Setting.SplIndicatorDistance,
                    MetadataIndicatorText =
                        labels.ContainsKey(key: "ctrMetadataIndicatorText") ?
                        labels["ctrMetadataIndicatorText"] : String.Empty,
                    MetadataElementsText =
                        labels.ContainsKey(key: "ctrMetadataElementsText") ?
                        labels["ctrMetadataElementsText"] : String.Empty
                };

            Label lbl = new Label();
            lbl.CreateControl();
            ctrTargetVariableSelector.AdditionalControls.Add(item: lbl);

            ComboBox cmbIdTargetVariable = new ComboBox()
            {
                Size = new Size(0, 0),
            };
            cmbIdTargetVariable.CreateControl();
            ctrTargetVariableSelector.AdditionalControls.Add(item: cmbIdTargetVariable);

            cmbIdTargetVariable.SelectedIndexChanged += new EventHandler(CmbIdTargetVariable_SelectedIndexChanged);

            ctrTargetVariableSelector.SelectedTargetVariableChanged +=
                new EventHandler((sender, e) =>
                {
                    SelectedTargetVariableChanged?.Invoke(
                       sender: this,
                       e: new EventArgs());
                    lbl.Text = "Id: " + ctrTargetVariableSelector.SelectedTargetVariableIdentifier.ToString();
                    cmbIdTargetVariable.DataSource = ctrTargetVariableSelector.SelectedTargetVariableIdentifiers;
                });

            ctrTargetVariableSelector.SplitterMoved +=
                new EventHandler((sender, e) =>
                {
                    Setting.SplIndicatorDistance = ctrTargetVariableSelector.SplitterDistance;
                });

            ctrTargetVariableSelector.CreateControl();
            pnlTargetVariableSelector.Controls.Add(
                value: ctrTargetVariableSelector);
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události změna výběru cílové proměnné
        /// v ComboBoxu cmbIdTargetVariable</para>
        /// <para lang="en">Event handler of the change of target variable
        /// selection in the cmbIdTargetVariable ComboBox</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CmbIdTargetVariable_SelectedIndexChanged(object sender, EventArgs e)
        {
            TDFnEtlGetTargetVariableTypeList tdFnEtlGetTargetVariableListAll =
                TDFunctions.FnEtlGetTargetVariable.Execute(
                    database: Database,
                    exportConnection: SelectedExportConnectionId,
                    nationalLanguage: nationalLang.NatLang,
                    etl: false,
                    targetVariable: CtrTargetVariableSelector.SelectedTargetVariableIdentifier);

            TDFnEtlGetTargetVariableMetadataList tdFnEtlGetTargetVariablesExtendedList =
                new TDFnEtlGetTargetVariableMetadataList(
                database: Database);
            tdFnEtlGetTargetVariablesExtendedList.ReLoad(
                fnEtlGetTargetVariable: tdFnEtlGetTargetVariableListAll);

            TDFnEtlGetTargetVariableMetadataList tdFnEtlGetTargetVariableExtendedListAggregated =
                tdFnEtlGetTargetVariablesExtendedList.Aggregated();

            cmbIdEtlTargetVariable.ValueMember = "id_etl_target_variable";
            cmbIdEtlTargetVariable.DisplayMember = "id_etl_target_variable";
            cmbIdEtlTargetVariable.DataSource = tdFnEtlGetTargetVariableExtendedListAggregated.Data;

            DataRow row = tdFnEtlGetTargetVariableExtendedListAggregated.Data.Rows[0];
            string RefYearSetToPanelMappingString = row["refyearset2panel_mapping"].ToString();

            List<string> refYearSetToPanelMappingList = new List<string> { RefYearSetToPanelMappingString };

            cmbRefYearSetToPanelMapping.ValueMember = "refyearset2panel_mapping";
            cmbRefYearSetToPanelMapping.DisplayMember = "refyearset2panel_mapping";
            cmbRefYearSetToPanelMapping.DataSource = refYearSetToPanelMappingList;

            cmbIndicatorLabelCs.ValueMember = "indicator_label_cs";
            cmbIndicatorLabelCs.DisplayMember = "indicator_label_cs";
            cmbIndicatorLabelCs.DataSource = tdFnEtlGetTargetVariableExtendedListAggregated.Data;

            cmbIndicatorLabelEn.ValueMember = "indicator_label_en";
            cmbIndicatorLabelEn.DisplayMember = "indicator_label_en";
            cmbIndicatorLabelEn.DataSource = tdFnEtlGetTargetVariableExtendedListAggregated.Data;

            cmbTargetVariableId.ValueMember = "target_variable_id";
            cmbTargetVariableId.DisplayMember = "target_variable_id";
            cmbTargetVariableId.DataSource = tdFnEtlGetTargetVariableExtendedListAggregated.Data;
        }

        /// <summary>
        /// <para lang="cs">Obsluha události změna id cílové proměnné
        /// v ComboBoxu cmbTargetVariableId</para>
        /// <para lang="en">Event handler of the change of target variable
        /// selection in the cmbTargetVariableId ComboBox</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CmbTargetVariableId_SelectedIndexChanged(object sender, EventArgs e)
        {
            TDFnEtlGetTargetVariableTypeList tdFnEtlGetTargetVariableList = TDFunctions.FnEtlGetTargetVariable.Execute(
                database: Database,
                exportConnection: SelectedExportConnectionId,
                nationalLanguage: nationalLang.NatLang,
                etl: false,
                targetVariable: (int)cmbTargetVariableId.SelectedValue
               );

            if (tdFnEtlGetTargetVariableList.Items.Count > 0)
            {
                Dictionary<string, string> labels = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                TDFnEtlGetTargetVariableType tdFnEtlGetTargetVariable =
                    (TDFnEtlGetTargetVariableType)tdFnEtlGetTargetVariableList.Items[0];

                // Handle CheckAtomicAreaDomains
                bool? checkAtomicAreaDomains = tdFnEtlGetTargetVariable.CheckAtomicAreaDomains;
                AtomicAreaDomains = checkAtomicAreaDomains;

                if (AtomicAreaDomains.HasValue)
                {
                    if (AtomicAreaDomains.Value)
                    {
                        lblAtomicAreaDomainValue.Text =
                            labels.ContainsKey(key: "lblAtomicAreaDomainValueNo") ?
                            labels["lblAtomicAreaDomainValueNo"] : String.Empty;
                        lblAtomicAreaDomainValue.Visible = true;
                        lblAtomicAreaDomain.Visible = true;
                    }
                    else
                    {
                        lblAtomicAreaDomainValue.Text =
                            labels.ContainsKey(key: "lblAtomicAreaDomainValueYes") ?
                            labels["lblAtomicAreaDomainValueYes"] : String.Empty;
                        lblAtomicAreaDomainValue.Visible = true;
                        lblAtomicAreaDomain.Visible = true;
                    }
                }
                else
                {
                    // Handle the case when CheckAtomicAreaDomains is null.
                    // Display "null" when CheckAtomicAreaDomains is null:
                    lblAtomicAreaDomainValue.Text = "null";
                    lblAtomicAreaDomainValue.Visible = false;
                    lblAtomicAreaDomain.Visible = false;
                }

                // Handle CheckAtomicSubPopulations
                bool? checkAtomicSubPopulations = tdFnEtlGetTargetVariable.CheckAtomicSubPopulations;
                AtomicSubPopulations = checkAtomicSubPopulations;

                if (AtomicSubPopulations.HasValue)
                {
                    if (AtomicSubPopulations.Value)
                    {
                        lblAtomicSubPopulationValue.Text =
                            labels.ContainsKey(key: "lblAtomicSubPopulationValueNo") ?
                            labels["lblAtomicSubPopulationValueNo"] : String.Empty;
                        lblAtomicSubPopulationValue.Visible = true;
                        lblAtomicSubPopulation.Visible = true;
                    }
                    else
                    {
                        lblAtomicSubPopulationValue.Text =
                            labels.ContainsKey(key: "lblAtomicSubPopulationValueYes") ?
                            labels["lblAtomicSubPopulationValueYes"] : String.Empty;
                        lblAtomicSubPopulationValue.Visible = true;
                        lblAtomicSubPopulation.Visible = true;
                    }
                }
                else
                {
                    // Handle the case when CheckAtomicSubPopulations is null.
                    // Display "null" when CheckAtomicSubPopulations is null:
                    lblAtomicSubPopulationValue.Text = "null";
                    lblAtomicSubPopulationValue.Visible = false;
                    lblAtomicSubPopulation.Visible = false;
                }
            }
            else
            {
                // Handle the case when no items are returned in the list.
                lblAtomicAreaDomainValue.Text = "";
                lblAtomicSubPopulationValue.Text = "";
                lblAtomicAreaDomainValue.Visible = false;
                lblAtomicAreaDomain.Visible = false;
                lblAtomicSubPopulationValue.Visible = false;
                lblAtomicSubPopulation.Visible = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události při změně jazyka</para>
        /// <para lang="en">Handles the event when the language changes</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt, který vyvolal událost (ControlEtl)</para>
        /// <para lang="en">The object that raised the event (ControlEtl)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Argumenty události</para>
        /// <para lang="en">Event arguments</para>
        /// </param>
        private void OnLanguageChanged(object sender, EventArgs e)
        {
            InitializeLabels();
        }

        #endregion Event Handlers


        #region Events

        #region PreviousClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">The delegate of the function handling the "Previous" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void PreviousClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">"Previous" button click event</para>
        /// </summary>
        public event PreviousClickHandler PreviousClick;


        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Invoking a click event on the "Previous" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaisePreviousClick(object sender, EventArgs e)
        {
            PreviousClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Handling the "Previous" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnPrevious_Click(object sender, EventArgs e)
        {
            if (ControlOwner is not ControlEtl)
            {
                ctrConnection = new ControlConnection(
                    controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                };
                ctrConnection.LoadContent();
                ctrConnection.InitializeLabels();
                CtrEtl.ShowControl(control: ctrConnection);
                ctrConnection.CheckConnection(SelectedExportConnectionId);
            }
            else
            {
                RaisePreviousClick(sender, e);
            }
        }

        #endregion PreviousClick Event

        #region NextClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">The delegate of the function handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void NextClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">"Next" button click event</para>
        /// </summary>
        public event NextClickHandler NextClick;

        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Invoking a click event on the "Next" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaiseNextClick(object sender, EventArgs e)
        {
            NextClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            IdEtl = cmbIdEtlTargetVariable.SelectedValue == DBNull.Value ? null : (int?)cmbIdEtlTargetVariable.SelectedValue;

            string refYearSetToPanelMappingString = (string)cmbRefYearSetToPanelMapping.SelectedValue;
            RefYearSetToPanelMapping = refYearSetToPanelMappingString
                .Split(';')
                .Select(a => int.TryParse(a, out int result) ? (int?)result : null)
                .ToList();

            SelectedIndicatorLabelCs = (string)cmbIndicatorLabelCs.SelectedValue;
            SelectedIndicatorLabelEn = (string)cmbIndicatorLabelEn.SelectedValue;

            if (cmbIdEtlTargetVariable.SelectedValue != null && cmbIdEtlTargetVariable.SelectedValue != DBNull.Value)
            {
                //schema Target Variable node 15 - id exists in source_database(target_data).t_etl_target_variable (the selected target variable already has got the etl id)
                //and atomicAreaDomains or atomicSubPopulations are false
                if (AtomicAreaDomains == false && AtomicSubPopulations == false)
                {
                    SourceTransaction = Database.Postgres.BeginTransaction();
                    TargetTransaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();

                    controlAreaDomain = new ControlAreaDomain(
                        controlOwner: this,
                        idEtl: IdEtl,
                        refYearSetToPanelMapping: RefYearSetToPanelMapping,
                        selectedExportConnectionId: SelectedExportConnectionId,
                        atomicAreaDomains: AtomicAreaDomains,
                        atomicSubPopulations: AtomicSubPopulations,
                        selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                        selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                        sourceTransaction: SourceTransaction,
                        targetTransaction: TargetTransaction)
                    {
                        Dock = DockStyle.Fill
                    };

                    controlAreaDomain.LoadContent();
                    controlAreaDomain.InitializeLabels();

                    CtrEtl.ShowControl(control: controlAreaDomain);
                }
                else if (AtomicAreaDomains == false && AtomicSubPopulations == true)
                {
                    SourceTransaction = Database.Postgres.BeginTransaction();
                    TargetTransaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();

                    controlAreaDomain = new ControlAreaDomain(
                        controlOwner: this,
                        idEtl: IdEtl,
                        refYearSetToPanelMapping: RefYearSetToPanelMapping,
                        selectedExportConnectionId: SelectedExportConnectionId,
                        atomicAreaDomains: AtomicAreaDomains,
                        atomicSubPopulations: AtomicSubPopulations,
                        selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                        selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                        sourceTransaction: SourceTransaction,
                        targetTransaction: TargetTransaction)
                    {
                        Dock = DockStyle.Fill
                    };

                    controlAreaDomain.LoadContent();
                    controlAreaDomain.InitializeLabels();

                    CtrEtl.ShowControl(control: controlAreaDomain);
                }
                else if (AtomicAreaDomains == true && AtomicSubPopulations == false)
                {
                    SourceTransaction = Database.Postgres.BeginTransaction();
                    TargetTransaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();

                    controlSubPopulation = new ControlSubPopulation(
                        controlOwner: this,
                        idEtl: IdEtl,
                        refYearSetToPanelMapping: RefYearSetToPanelMapping,
                        selectedExportConnectionId: SelectedExportConnectionId,
                        atomicAreaDomains: AtomicAreaDomains,
                        atomicSubPopulations: AtomicSubPopulations,
                        selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                        selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                        sourceTransaction: SourceTransaction,
                        targetTransaction: TargetTransaction)
                    {
                        Dock = DockStyle.Fill
                    };

                    controlSubPopulation.LoadContent();
                    controlSubPopulation.InitializeLabels();

                    CtrEtl.ShowControl(control: controlSubPopulation);
                }
                else
                {
                    //schema Target Variable node 15 - id exists in source_database(target_data).t_etl_target_variable (the selected target variable already has got the etl id)
                    Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

                    DialogResult dialogResult = MessageBox.Show(
                                                text: messages.ContainsKey(key: "msgCheckMetadata") ?
                                                        messages["msgCheckMetadata"] : String.Empty,
                                                caption: String.Empty,
                                                buttons: MessageBoxButtons.YesNo,
                                                icon: MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        List<int?> etlId = new List<int?>() { IdEtl };

                        string metadata =
                            TDFunctions.FnEtlGetTargetVariableMetadatas.Execute(
                                database: Database,
                                etlTargetVariableIds: etlId,
                                nationalLanguage: nationalLang.NatLang);

                        DataTable dt = NfiEstaFunctions.FnEtlCheckTargetVariableMetadatas.ExecuteQuery(
                            database: FormUserPassword.TargetDatabase,
                            metadatas: metadata,
                            nationalLanguage: nationalLang.NatLang,
                            metadataDiff: true);

                        if (dt.AsEnumerable().Any())
                        {
                            FormCheckMetadata form = new FormCheckMetadata(
                                controlOwner: this,
                                idEtl: IdEtl,
                                refYearSetToPanelMapping: RefYearSetToPanelMapping,
                                selectedExportConnectionId: SelectedExportConnectionId,
                                atomicAreaDomains: AtomicAreaDomains,
                                atomicSubPopulations: AtomicSubPopulations,
                                selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                                selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                                sourceTransaction: SourceTransaction,
                                targetTransaction: TargetTransaction);

                            if (form.ShowDialog() == DialogResult.OK)
                            {

                            }
                        }
                        else
                        {
                            SourceTransaction = Database.Postgres.BeginTransaction();
                            TargetTransaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();

                            DialogResult result = MessageBox.Show(
                                                text: messages.ContainsKey(key: "msgMetadataOK") ?
                                                        messages["msgMetadataOK"] : String.Empty,
                                                caption: String.Empty,
                                                buttons: MessageBoxButtons.YesNoCancel,
                                                icon: MessageBoxIcon.Information);
                            if (result == DialogResult.Yes)
                            {
                                controlPanelRefyearset = new ControlPanelRefyearset(
                                    controlOwner: this,
                                    idEtlFromTargetVariable: null,
                                    idEtlFromTargetVariableCompare: null,
                                    idEtl: IdEtl,
                                    refYearSetToPanelMapping: RefYearSetToPanelMapping,
                                    selectedExportConnectionId: SelectedExportConnectionId,
                                    atomicAreaDomains: AtomicAreaDomains,
                                    atomicSubPopulations: AtomicSubPopulations,
                                    selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                                    selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                                    sourceTransaction: SourceTransaction,
                                    targetTransaction: TargetTransaction)
                                {
                                    Dock = DockStyle.Fill
                                };

                                controlPanelRefyearset.LoadContent();
                                controlPanelRefyearset.InitializeLabels();

                                CtrEtl.ShowControl(control: controlPanelRefyearset);
                            }
                            else if (result == DialogResult.No)
                            {
                                Database.Postgres?.CloseConnection();
                                FormUserPassword.Disconnect();
                                Control parentControlEtl = this.Parent;
                                parentControlEtl.Controls.Remove(this);
                            }
                            else
                            {
                                SourceTransaction.Rollback();
                                TargetTransaction.Rollback();
                            }
                        }
                    }
                    else
                    {
                        SourceTransaction = Database.Postgres.BeginTransaction();
                        TargetTransaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();

                        controlPanelRefyearset = new ControlPanelRefyearset(
                            controlOwner: this,
                            idEtlFromTargetVariable: null,
                            idEtlFromTargetVariableCompare: null,
                            idEtl: IdEtl,
                            refYearSetToPanelMapping: RefYearSetToPanelMapping,
                            selectedExportConnectionId: SelectedExportConnectionId,
                            atomicAreaDomains: AtomicAreaDomains,
                            atomicSubPopulations: AtomicSubPopulations,
                            selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                            selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                            sourceTransaction: SourceTransaction,
                            targetTransaction: TargetTransaction)
                        {
                            Dock = DockStyle.Fill
                        };

                        controlPanelRefyearset.LoadContent();
                        controlPanelRefyearset.InitializeLabels();

                        CtrEtl.ShowControl(control: controlPanelRefyearset);
                    }
                }
            }
            else
            {
                SourceTransaction = Database.Postgres.BeginTransaction();
                TargetTransaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();

                RefreshValues();
                string metadata =
                    TDFunctions.FnEtlGetTargetVariableMetadata.Execute(
                        database: Database,
                        targetVariable: SelectedTargetVariableId,
                        nationalLanguage: nationalLang.NatLang);

                List<int?> alreadyEtledTargetVariableEtlIds =
                    TDFunctions.FnEtlGetEtlIdsOfTargetVariable.Execute(
                        database: Database,
                        exportConnection: SelectedExportConnectionId);

                int? nfiestaTargetVariableId = NfiEstaFunctions.FnEtlCheckTargetVariable
                    .Execute(FormUserPassword.TargetDatabase, metadata, alreadyEtledTargetVariableEtlIds);

                if (nfiestaTargetVariableId != null)
                {
                    //schema Target Variable node 7 - id exists in target_database(nfiesta).c_target_variable
                    NfiEstaFunctions.FnEtlImportTargetVariableMetadata.Execute(
                        FormUserPassword.TargetDatabase,
                        (int)nfiestaTargetVariableId,
                        metadata,
                        TargetTransaction);

                    IdEtlFromControlTargetVariable =
                        TDFunctions.FnEtlSaveTargetVariable.Execute(
                            database: Database,
                            exportConnection: SelectedExportConnectionId,
                            targetVariable: SelectedTargetVariableId,
                            etlId: (int)nfiestaTargetVariableId,    //result in the target_data.t_etl_target_variable table
                            transaction: SourceTransaction);

                    controlPanelRefyearset = new ControlPanelRefyearset(
                            controlOwner: this,
                            idEtlFromTargetVariable: IdEtlFromControlTargetVariable,
                            idEtlFromTargetVariableCompare: null,
                            idEtl: null,
                            refYearSetToPanelMapping: null,
                            selectedExportConnectionId: SelectedExportConnectionId,
                            atomicAreaDomains: AtomicAreaDomains,
                            atomicSubPopulations: AtomicSubPopulations,
                            selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                            selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                            sourceTransaction: SourceTransaction,
                            targetTransaction: TargetTransaction)
                    {
                        Dock = DockStyle.Fill
                    };

                    controlPanelRefyearset.LoadContent();
                    controlPanelRefyearset.InitializeLabels();

                    CtrEtl.ShowControl(control: controlPanelRefyearset);
                }
                else
                {
                    FnEtlGetTargetVariableTypeList nfiEstaFnEtlGetTargetVariableList =
                        NfiEstaFunctions.FnEtlGetTargetVariable.Execute(
                            database: FormUserPassword.TargetDatabase,
                            nationalLanguage: nationalLang.NatLang,
                            etlId: alreadyEtledTargetVariableEtlIds);

                    //schema Target Variable node 9 - no record returned
                    if (nfiEstaFnEtlGetTargetVariableList.Items.Count == 0)
                    {
                        int? id = NfiEstaFunctions.FnEtlImportTargetVariable.Execute(
                            FormUserPassword.TargetDatabase,
                            metadata,
                            TargetTransaction);

                        IdEtlFromControlTargetVariable =
                            TDFunctions.FnEtlSaveTargetVariable.Execute(
                                database: Database,
                                exportConnection: SelectedExportConnectionId,
                                targetVariable: SelectedTargetVariableId,
                                etlId: id,      //result in the target_data.t_etl_target_variable table
                                transaction: SourceTransaction);

                        controlPanelRefyearset = new ControlPanelRefyearset(
                            controlOwner: this,
                            idEtlFromTargetVariable: IdEtlFromControlTargetVariable,
                            idEtlFromTargetVariableCompare: null,
                            idEtl: null,
                            refYearSetToPanelMapping: null,
                            selectedExportConnectionId: SelectedExportConnectionId,
                            atomicAreaDomains: AtomicAreaDomains,
                            atomicSubPopulations: AtomicSubPopulations,
                            selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                            selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                            sourceTransaction: SourceTransaction,
                            targetTransaction: TargetTransaction)
                        {
                            Dock = DockStyle.Fill
                        };

                        controlPanelRefyearset.LoadContent();
                        controlPanelRefyearset.InitializeLabels();

                        CtrEtl.ShowControl(control: controlPanelRefyearset);
                    }
                    if (ControlOwner is not ControlEtl)
                    {
                        SelectedTargetVariableValues selectedTargetVariableValues = new SelectedTargetVariableValues()
                        {
                            SelectedTargetVariableId = SelectedTargetVariableId,
                            SelectedIndicatorLabel = SelectedIndicatorLabel,
                            SelectedStateOrChange = SelectedStateOrChange,
                            SelectedUnit = SelectedUnit,
                            SelectedLocalDensity = SelectedLocalDensity,
                            SelectedVersion = SelectedVersion,
                            SelectedDefinitionVariant = SelectedDefinitionVariant,
                            SelectedAreaDomain = SelectedAreaDomain,
                            SelectedPopulation = SelectedPopulation,
                        };
                        ctrTargetVariableCompare = new ControlTargetVariableCompare(
                            controlOwner: this,
                            selectedTargetVariableValues: selectedTargetVariableValues,
                            selectedExportConnectionId: SelectedExportConnectionId,
                            selectedIndicatorLabelCs: SelectedIndicatorLabelCs,
                            selectedIndicatorLabelEn: SelectedIndicatorLabelEn,
                            sourceTransaction: SourceTransaction,
                            targetTransaction: TargetTransaction)
                        {
                            Dock = DockStyle.Fill
                        };
                        ctrTargetVariableCompare.LoadContent();
                        ctrTargetVariableCompare.InitializeLabels();
                        CtrEtl.ShowControl(control: ctrTargetVariableCompare);
                    }
                    else
                    {
                        RaiseNextClick(sender, e);
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        #endregion NextClick Event

        /// <summary>
        /// <para lang="cs">Událost "Změna vybraného indikátoru"</para>
        /// <para lang="en">"Selected indicator changed" event</para>
        /// </summary>
        public event EventHandler SelectedTargetVariableChanged;

        #endregion Events
    }

}
