﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Formulář pro kontrolu metadat cílové proměnné, která již má id v cílové databázi</para>
    /// <para lang="en">Form to check the metadata of a target variable that already has got an id in the target database</para>
    /// </summary>
    public partial class FormCheckMetadata
            : Form, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </summary>
        private readonly int? _idEtl;

        /// <summary>
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů, který vrací fce fn_etl_get_target_variable</para>
        /// <para lang="en">List of panel and refyearset combination ids returned by fn_etl_get_target_variable</para>
        /// </summary>
        private readonly List<int?> _refYearSetToPanelMapping;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        /// <summary>
        /// <para lang="cs">Seznam se záznamy cílové proměnné</para>
        /// <para lang="en">List with records of target variable</para>
        /// </summary>
        private FnEtlCheckTargetVariableMetadatasTypeList result;

        /// <summary>
        /// <para lang="cs">Sloupec metadata_source s původní hodnotou</para>
        /// <para lang="en">Metadata_source column with original value</para>
        /// </summary>
        private string defaultMetadataSource;

        /// <summary>
        /// <para lang="cs">Ve sloupci metadata_source DataGridView dgvDiff se zobrazí jen indikátor</para>
        /// <para lang="en">Only the indicator is displayed in the metadata_source column of the dgvDiffDataGridView</para>
        /// </summary>
        private string displayedMetadataSource;

        /// <summary>
        /// <para lang="cs">Instance BindingSource slouží k řízení propojení dat a synchronizace pro dgvDiff</para>
        /// <para lang="en">A BindingSource instance used to manage data binding and synchronization for dgvDiff</para>
        /// </summary>
        private readonly BindingSource bindingSource = new BindingSource();

        /// <summary>
        /// <para lang="cs">Transakce</para>
        /// <para lang="en">Transaction</para>
        /// </summary>
        private NpgsqlTransaction transaction;

        /// <summary>
        /// <para lang="cs">Transakce běží</para>
        /// <para lang="en">Transaction is running</para>
        /// </summary>
        private bool transactionRunning = false;

        /// <summary>
        /// <para lang="cs">Proběhla metoda ExecuteUpdate() pro národní verzi</para>
        /// <para lang="en">The ExecuteUpdate() method loaded within the national version</para>
        /// </summary>
        private bool csUpdateExecuted = false;

        /// <summary>
        /// <para lang="cs">Proběhla metoda ExecuteUpdate() pro anglickou verzi</para>
        /// <para lang="en">The ExecuteUpdate() method loaded within the English version</para>
        /// </summary>
        private bool enUpdateExecuted = false;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Atomické plošné domény</para>
        /// <para lang="en">Atomic area domains</para>
        /// </summary>
        private readonly bool? _atomicAreaDomains;

        /// <summary>
        /// <para lang="cs">Atomické subpopulace</para>
        /// <para lang="en">Atomic subpopulations</para>
        /// </summary>
        private readonly bool? _atomicSubPopulations;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelCs;

        /// <summary>
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </summary>
        private readonly string _selectedIndicatorLabelEn;

        /// <summary>
        /// <para lang="cs">Jazyková verze použitá pouze lokálně</para>
        /// <para lang="en">Language version used only locally</para>
        /// </summary>
        private LanguageVersion _localLanguageVersion;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro kontrolu metadat cílové proměnné, která již má id v cílové databázi</para>
        /// <para lang="en">Constructor of the form to check the metadata of a target variable that already has got an id in the target database</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="idEtl">
        /// <para lang="cs">Id záznamu v tabulce target_data.t_etl_target_variable.</para>
        /// <para lang="en">Id of the record in the target_data.t_etl_target_variable table.</para>
        /// </param>
        /// <param name="refYearSetToPanelMapping">
        /// <para lang="cs">Seznam id kombinací panelů a refyearsetů, který vrací fce fn_etl_get_target_variable</para>
        /// <para lang="en">List of panel and refyearset combination ids returned by fn_etl_get_target_variable</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        /// <param name="atomicAreaDomains">
        /// <para lang="cs">Atomické plošné domény</para>
        /// <para lang="en">Atomic area domains</para>
        /// </param>
        /// <param name="atomicSubPopulations">
        /// <para lang="cs">Atomické subpopulace</para>
        /// <para lang="en">Atomic subpopulations</para>
        /// </param>
        /// <param name="selectedIndicatorLabelCs">
        /// <para lang="cs">Zvolený indikátor v národním jazyce</para>
        /// <para lang="en">Selected indicator in national language</para>
        /// </param>
        /// <param name="selectedIndicatorLabelEn">
        /// <para lang="cs">Zvolený indikátor v angličtině</para>
        /// <para lang="en">Selected indicator in English</para>
        /// </param>
        /// <param name="sourceTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </param>
        /// <param name="targetTransaction">
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </param>
        public FormCheckMetadata(
            Control controlOwner,
            int? idEtl,
            List<int?> refYearSetToPanelMapping,
            int selectedExportConnectionId,
            bool? atomicAreaDomains,
            bool? atomicSubPopulations,
            string selectedIndicatorLabelCs,
            string selectedIndicatorLabelEn,
            NpgsqlTransaction sourceTransaction,
            NpgsqlTransaction targetTransaction
            )
        {
            InitializeComponent();

            ControlOwner = controlOwner;
            _idEtl = idEtl;
            _refYearSetToPanelMapping = refYearSetToPanelMapping;
            _selectedExportConnectionId = selectedExportConnectionId;
            _atomicAreaDomains = atomicAreaDomains;
            _atomicSubPopulations = atomicSubPopulations;
            _selectedIndicatorLabelCs = selectedIndicatorLabelCs;
            _selectedIndicatorLabelEn = selectedIndicatorLabelEn;
            SourceTransaction = sourceTransaction;
            TargetTransaction = targetTransaction;
            _localLanguageVersion = CtrEtl.LanguageVersion;

            Initialize(controlOwner: controlOwner);

            rtxSelectedLocalDensity.MouseHover += RichTextBox_MouseHover;
            rtxSelectedLocalDensityDes.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBLocalDensity.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBLocalDensityDes.MouseHover += RichTextBox_MouseHover;
            rtxSelectedObjectTypeDes.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBObjectTypeDes.MouseHover += RichTextBox_MouseHover;
            rtxSelectedObject.MouseHover += RichTextBox_MouseHover;
            rtxSelectedObjectDes.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBObject.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBObjectDes.MouseHover += RichTextBox_MouseHover;
            rtxSelectedDefinitionVariant.MouseHover += RichTextBox_MouseHover;
            rtxSelectedDefinitionVariantDes.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBDefinitionVariant.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBDefinitionVariantDes.MouseHover += RichTextBox_MouseHover;
            rtxSelectedAreaDomain.MouseHover += RichTextBox_MouseHover;
            rtxSelectedAreaDomainDes.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBAreaDomain.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBAreaDomainDes.MouseHover += RichTextBox_MouseHover;
            rtxSelectedPopulation.MouseHover += RichTextBox_MouseHover;
            rtxSelectedPopulationDes.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBPopulation.MouseHover += RichTextBox_MouseHover;
            rtxTargetDBPopulationDes.MouseHover += RichTextBox_MouseHover;
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is ControlTargetVariable))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlTargetVariable.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is ControlTargetVariable))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlTargetVariable.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((ControlTargetVariable)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            //get
            //{
            //    return ((ControlTargetVariable)ControlOwner).LanguageVersion;
            //}
            get { return _localLanguageVersion; }
            set { _localLanguageVersion = value; }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ControlTargetVariable)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ControlTargetVariable)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlTargetVariable ctrTargetVariable)
                {
                    return ctrTargetVariable.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL ve zdrojové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the source database</para>
        /// </summary>
        public NpgsqlTransaction SourceTransaction { get; set; }

        /// <summary>
        /// <para lang="cs">Hlavní transakce pro ETL v cílové databázi</para>
        /// <para lang="en">Main transaction for the ETL in the target database</para>
        /// </summary>
        public NpgsqlTransaction TargetTransaction { get; set; }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                             "Porovnání zvolené cílové proměnné s cílovou proměnnou v modulu Odhady" },
                            { "tsmiLanguage",                           "Změnit jazyk" },
                            { "lblMainCaption",                         String.Empty },
                            { "lblIndicator",                           "indikátor - zkratka" },
                            { "lblIndicatorDes",                        "indikátor - popis" },
                            { "lblStateOrChange",                       "stav nebo změna - zkratka" },
                            { "lblStateOrChangeDes",                    "stav nebo změna - popis" },
                            { "lblUnit",                                "jednotka - zkratka" },
                            { "lblUnitDes",                             "jednotka - popis" },
                            { "lblLocalDensity",                        "příspěvek lokální hustoty - zkratka" },
                            { "lblLocalDensityDes",                     "příspěvek lokální hustoty - popis" },
                            { "lblObjectType",                          "typ objektu - zkratka" },
                            { "lblObjectTypeDes",                       "typ objektu - popis" },
                            { "lblObject",                              "objekt - zkratka" },
                            { "lblObjectDes",                           "objekt - popis" },
                            { "lblVersion",                             "verze - zkratka" },
                            { "lblVersionDes",                          "verze - popis" },
                            { "lblDefinitionVariant",                   "definiční varianta - zkratka" },
                            { "lblDefinitionVariantDes",                "definiční varianta - popis" },
                            { "lblAreaDomain",                          "plošná doména - zkratka" },
                            { "lblAreaDomainDes",                       "plošná doména - popis" },
                            { "lblPopulation",                          "populace - zkratka" },
                            { "lblPopulationDes",                       "populace - popis" },
                            { "lblUseNegative",                         "záporný příspěvek" },
                            { "grpSelectedTargetVariable",              "Zvolená cílová proměnná:" },
                            { "grpTargetVariablesInTargetDatabase",     "Cílová proměnná v modulu Odhady:" },
                            { "btnNext",                                "Další" },
                            { "btnUpdate",                              "Aktualizovat" },
                            { "btnInsert",                              "Vložit" },
                            { "msgContinue",                            "Pokračovat v procesu přenosu dat?"},
                            { "msgConfirm",                             "Potvrdit změnu a uložit do cílové databáze?"},
                            { "metadataSource",                         "Cílová proměnná"},
                            { "metadataDiffNationalLanguage",           "Liší se v národní verzi?"},
                            { "metadataDiffEnglishLanguage",            "Liší se v anglické verzi?"},
                            { "missingMetadataNationalLanguage",        "Chybí národní verze v modulu Odhady?"},
                            { "yes",                                    "ano"},
                            { "no",                                     "ne"},
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "FormCheckMetadata") ?
                    languageFile.NationalVersion.Data["FormCheckMetadata"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                             "Comparison of the selected target variable with the target variable in the Estimates module" },
                            { "tsmiLanguage",                           "Change language" },
                            { "lblMainCaption",                         String.Empty },
                            { "lblIndicator",                           "indicator - label" },
                            { "lblIndicatorDes",                        "indicator - description" },
                            { "lblStateOrChange",                       "state or change - label" },
                            { "lblStateOrChangeDes",                    "state or change - decription" },
                            { "lblUnit",                                "unit - label" },
                            { "lblUnitDes",                             "unit - description" },
                            { "lblLocalDensity",                        "local density contribution - label" },
                            { "lblLocalDensityDes",                     "local density contribution - description" },
                            { "lblObjectType",                          "object type - label" },
                            { "lblObjectTypeDes",                       "object type - description" },
                            { "lblObject",                              "object - label" },
                            { "lblObjectDes",                           "object - description" },
                            { "lblVersion",                             "version - label" },
                            { "lblVersionDes",                          "version - description" },
                            { "lblDefinitionVariant",                   "definition variant - label" },
                            { "lblDefinitionVariantDes",                "definition description - label" },
                            { "lblAreaDomain",                          "area domain - label" },
                            { "lblAreaDomainDes",                       "area domain - description" },
                            { "lblPopulation",                          "population - label" },
                            { "lblPopulationDes",                       "population - description" },
                            { "lblUseNegative",                         "negative contribution" },
                            { "grpSelectedTargetVariable",              "Selected target variable:" },
                            { "grpTargetVariablesInTargetDatabase",     "Target variable in the Estimates module:" },
                            { "btnNext",                                "Next" },
                            { "btnUpdate",                              "Update" },
                            { "btnInsert",                              "Insert" },
                            { "msgContinue",                            "Continue the data transfer process?"},
                            { "msgConfirm",                             "Confirm the change and save to the target database?"},
                            { "metadataSource",                         "Target variable"},
                            { "metadataDiffNationalLanguage",           "Does the national version differ?"},
                            { "metadataDiffEnglishLanguage",            "Does the English version differ?"},
                            { "missingMetadataNationalLanguage",        "Is the national version missing in the Estimates module?"},
                            { "yes",                                    "yes"},
                            { "no",                                     "no"},
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "FormCheckMetadata") ?
                    languageFile.InternationalVersion.Data["FormCheckMetadata"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro kontrolu metadat cílové proměnné, která již má id v cílové databázi</para>
        /// <para lang="en">Initializing the form to check the metadata of a target variable that already has got an id in the target database</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            nationalLang = new NationalLang();

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro kontrolu metadat cílové proměnné, která již má id v cílové databázi</para>
        /// <para lang="en">Initializing labels of the form to check the metadata of a target variable that already has got an id in the target database</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text =
               labels.ContainsKey(key: "frmCaption") ?
               labels["frmCaption"] : String.Empty;

            tsmiLanguage.Text =
                labels.ContainsKey(key: "tsmiLanguage") ?
                labels["tsmiLanguage"] : String.Empty;

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            lblIndicator.Text =
                labels.ContainsKey(key: "lblIndicator") ?
                labels["lblIndicator"] : String.Empty;

            lblIndicatorDes.Text =
                labels.ContainsKey(key: "lblIndicatorDes") ?
                labels["lblIndicatorDes"] : String.Empty;

            lblStateOrChange.Text =
                labels.ContainsKey(key: "lblStateOrChange") ?
                labels["lblStateOrChange"] : String.Empty;

            lblStateOrChangeDes.Text =
                labels.ContainsKey(key: "lblStateOrChangeDes") ?
                labels["lblStateOrChangeDes"] : String.Empty;

            lblUnit.Text =
                labels.ContainsKey(key: "lblUnit") ?
                labels["lblUnit"] : String.Empty;

            lblUnitDes.Text =
                labels.ContainsKey(key: "lblUnitDes") ?
                labels["lblUnitDes"] : String.Empty;

            lblLocalDensity.Text =
                labels.ContainsKey(key: "lblLocalDensity") ?
                labels["lblLocalDensity"] : String.Empty;

            lblLocalDensityDes.Text =
                labels.ContainsKey(key: "lblLocalDensityDes") ?
                labels["lblLocalDensityDes"] : String.Empty;

            lblObjectType.Text =
                 labels.ContainsKey(key: "lblObjectType") ?
                 labels["lblObjectType"] : String.Empty;

            lblObjectTypeDes.Text =
                labels.ContainsKey(key: "lblObjectTypeDes") ?
                labels["lblObjectTypeDes"] : String.Empty;

            lblObject.Text =
                labels.ContainsKey(key: "lblObject") ?
                labels["lblObject"] : String.Empty;

            lblObjectDes.Text =
                labels.ContainsKey(key: "lblObjectDes") ?
                labels["lblObjectDes"] : String.Empty;

            lblVersion.Text =
                labels.ContainsKey(key: "lblVersion") ?
                labels["lblVersion"] : String.Empty;

            lblVersionDes.Text =
                labels.ContainsKey(key: "lblVersionDes") ?
                labels["lblVersionDes"] : String.Empty;

            lblDefinitionVariant.Text =
                labels.ContainsKey(key: "lblDefinitionVariant") ?
                labels["lblDefinitionVariant"] : String.Empty;

            lblDefinitionVariantDes.Text =
                labels.ContainsKey(key: "lblDefinitionVariantDes") ?
                labels["lblDefinitionVariantDes"] : String.Empty;

            lblAreaDomain.Text =
               labels.ContainsKey(key: "lblAreaDomain") ?
               labels["lblAreaDomain"] : String.Empty;

            lblAreaDomainDes.Text =
                labels.ContainsKey(key: "lblAreaDomainDes") ?
                labels["lblAreaDomainDes"] : String.Empty;

            lblPopulation.Text =
               labels.ContainsKey(key: "lblPopulation") ?
               labels["lblPopulation"] : String.Empty;

            lblPopulationDes.Text =
                labels.ContainsKey(key: "lblPopulationDes") ?
                labels["lblPopulationDes"] : String.Empty;

            lblUseNegative.Text =
                labels.ContainsKey(key: "lblUseNegative") ?
                labels["lblUseNegative"] : String.Empty;

            grpSelectedTargetVariable.Text =
                labels.ContainsKey(key: "grpSelectedTargetVariable") ?
                labels["grpSelectedTargetVariable"] : String.Empty;

            grpTargetVariablesInTargetDatabase.Text =
                labels.ContainsKey(key: "grpTargetVariablesInTargetDatabase") ?
                labels["grpTargetVariablesInTargetDatabase"] : String.Empty;

            btnNext.Text =
                labels.ContainsKey(key: "btnNext") ?
                labels["btnNext"] : String.Empty;

            btnUpdate.Text =
                labels.ContainsKey(key: "btnUpdate") ?
                labels["btnUpdate"] : String.Empty;

            btnInsert.Text =
                labels.ContainsKey(key: "btnInsert") ?
                labels["btnInsert"] : String.Empty;

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            LoadContent(transaction: null);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení na formuláři</para>
        /// <para lang="en">Loading database table data and displaying it in the form</para>
        /// </summary>
        public void LoadContent(NpgsqlTransaction transaction)
        {
            btnInsert.Visible = (LanguageVersion == LanguageVersion.National);

            bool shouldClearDataAndCollapsePanel = true;

            List<int?> etlId = new List<int?>() { _idEtl };

            string metadata =
                TDFunctions.FnEtlGetTargetVariableMetadatas.Execute(
                    database: Database,
                    etlTargetVariableIds: etlId,
                    nationalLanguage: nationalLang.NatLang,
                    transaction: transaction);

            result = NfiEstaFunctions.FnEtlCheckTargetVariableMetadatas
                .Execute(FormUserPassword.TargetDatabase, metadata, nationalLang.NatLang, true);

            FnEtlCheckTargetVariableMetadatasTypeList.SetColumnsVisibility(
                    (from a in FnEtlCheckTargetVariableMetadatasTypeList.Cols
                     orderby a.Value.DisplayIndex
                     select a.Value.Name).ToArray<string>());

            if (dgvDiff.Rows.Count == 0)
            {
                // if the DataGridView is empty, load the content as before (this code is because I want to preserve the checkboxstate)
                // binding source is added to handle remove of the row in the dgvDiff after update
                bindingSource.DataSource = result.Data;
                dgvDiff.DataSource = bindingSource;
                result.FormatDataGridView(dgvDiff);
                dgvDiff.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.AllCells;

                UpdateColumnHeaders();
            }

            if (dgvDiff.CurrentRow != null && result.Items.Count > 0)
            {
                int rowIndex = dgvDiff.CurrentRow.Index;
                dgvDiff.Rows[rowIndex].Cells["metadata_diff_national_language"].Value = result.Items[rowIndex].MetadataDiffNationalLanguage;
                dgvDiff.Rows[rowIndex].Cells["metadata_diff_english_language"].Value = result.Items[rowIndex].MetadataDiffEnglishLanguage;
                dgvDiff.Rows[rowIndex].Cells["missing_metadata_national_language"].Value = result.Items[rowIndex].MissingMetadataNationalLanguage;
                dgvDiff.Refresh();
            }

            dgvDiff.Columns["target_variable_source"].Visible = false;
            dgvDiff.Columns["target_variable_target"].Visible = false;
            dgvDiff.Columns["metadata_target"].Visible = false;
            dgvDiff.Columns["metadata_diff"].Visible = false;

            TargetVariableMetadataJson metadataSource = new TargetVariableMetadataJson();
            TargetVariableMetadataJson metadataTarget = new TargetVariableMetadataJson();

            // if the DataGridView already has rows, only update the row content (this code is because I want to preserve the checkboxstate)
            // originally there was: foreach (NfiEstaFnEtlCheckTargetVariableMetadatas item in result.Items)
            for (int i = 0; i < result.Items.Count; i++)
            {
                FnEtlCheckTargetVariableMetadatasType item = result.Items[i]; //newly added row (this code is because I want to preserve the checkboxstate)

                UpdateColumnHeaders();

                metadataSource.LoadFromText(item.MetadataSource);
                metadataTarget.LoadFromText(item.MetadataTarget);

                defaultMetadataSource = item.MetadataSource;
                displayedMetadataSource = (LanguageVersion == LanguageVersion.National) ? metadataSource.Indicator.LabelCs : metadataSource.Indicator.LabelEn;

                dgvDiff.Rows[i].Cells["metadata_source"].Value = displayedMetadataSource;

                if ((bool)item.MissingMetadataNationalLanguage && LanguageVersion == LanguageVersion.National)
                {
                    rtxSelectedIndicator.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Indicator.LabelCs : metadataSource.Indicator.LabelEn;
                    rtxSelectedIndicatorDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Indicator.DescriptionCs : metadataSource.Indicator.DescriptionEn;
                    rtxSelectedStateOrChange.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.StateOrChange.LabelCs : metadataSource.StateOrChange.LabelEn;
                    rtxSelectedStateOrChangeDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.StateOrChange.DescriptionCs : metadataSource.StateOrChange.DescriptionEn;
                    rtxSelectedUnit.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Unit.LabelCs : metadataSource.Unit.LabelEn;
                    rtxSelectedUnitDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Unit.DescriptionCs : metadataSource.Unit.DescriptionEn;

                    const string strCore = "core";
                    const string strDivision = "division";

                    rtxSelectedLocalDensity.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedLocalDensityDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObjectType.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeLabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeLabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObjectTypeDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeDescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeDescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObject.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                       .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                       .GroupBy(a => a.ObjectTypeLabelEn)
                       .Select(g => String.Join(
                               separator: " + ",
                               g.Select(a => a.ObjectLabelCs)
                               ))
                       .Aggregate((a, b) => $"{a} | {b}")
                   : metadataSource.LocalDensities
                       .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                       .GroupBy(a => a.ObjectTypeLabelEn)
                       .Select(g => String.Join(
                               separator: " + ",
                               g.Select(a => a.ObjectLabelEn)
                               ))
                       .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObjectDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectDescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectDescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedVersion.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedVersionDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedDefinitionVariant.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedDefinitionVariantDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedAreaDomain.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedAreaDomainDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedPopulation.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedPopulationDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedUseNegative.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.UseNegativeCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.UseNegativeEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedIndicator.ForeColor = Color.Black;
                    rtxSelectedIndicatorDes.ForeColor = Color.Black;
                    rtxSelectedStateOrChange.ForeColor = Color.Black;
                    rtxSelectedStateOrChangeDes.ForeColor = Color.Black;
                    rtxSelectedUnit.ForeColor = Color.Black;
                    rtxSelectedUnitDes.ForeColor = Color.Black;
                    rtxSelectedLocalDensity.ForeColor = Color.Black;
                    rtxSelectedLocalDensityDes.ForeColor = Color.Black;
                    rtxSelectedObjectType.ForeColor = Color.Black;
                    rtxSelectedObjectTypeDes.ForeColor = Color.Black;
                    rtxSelectedObject.ForeColor = Color.Black;
                    rtxSelectedObjectDes.ForeColor = Color.Black;
                    rtxSelectedVersion.ForeColor = Color.Black;
                    rtxSelectedVersionDes.ForeColor = Color.Black;
                    rtxSelectedDefinitionVariant.ForeColor = Color.Black;
                    rtxSelectedDefinitionVariantDes.ForeColor = Color.Black;
                    rtxSelectedAreaDomain.ForeColor = Color.Black;
                    rtxSelectedAreaDomainDes.ForeColor = Color.Black;
                    rtxSelectedPopulation.ForeColor = Color.Black;
                    rtxSelectedPopulationDes.ForeColor = Color.Black;
                    rtxSelectedUseNegative.ForeColor = Color.Black;

                    rtxTargetDBIndicator.Clear();
                    rtxTargetDBIndicatorDes.Clear();
                    rtxTargetDBStateOrChange.Clear();
                    rtxTargetDBStateOrChangeDes.Clear();
                    rtxTargetDBUnit.Clear();
                    rtxTargetDBUnitDes.Clear();
                    rtxTargetDBLocalDensity.Clear();
                    rtxTargetDBLocalDensityDes.Clear();
                    rtxTargetDBObjectType.Clear();
                    rtxTargetDBObjectTypeDes.Clear();
                    rtxTargetDBObject.Clear();
                    rtxTargetDBObjectDes.Clear();
                    rtxTargetDBVersion.Clear();
                    rtxTargetDBVersionDes.Clear();
                    rtxTargetDBDefinitionVariant.Clear();
                    rtxTargetDBDefinitionVariantDes.Clear();
                    rtxTargetDBAreaDomain.Clear();
                    rtxTargetDBAreaDomainDes.Clear();
                    rtxTargetDBPopulation.Clear();
                    rtxTargetDBPopulationDes.Clear();
                    rtxTargetDBUseNegative.Clear();

                    InsertCheckBoxColumnInDgv();
                }
                else
                {
                    rtxSelectedIndicator.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Indicator.LabelCs : metadataSource.Indicator.LabelEn;
                    rtxSelectedIndicatorDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Indicator.DescriptionCs : metadataSource.Indicator.DescriptionEn;
                    rtxTargetDBIndicator.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.Indicator.LabelCs : metadataTarget.Indicator.LabelEn;
                    rtxTargetDBIndicatorDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.Indicator.DescriptionCs : metadataTarget.Indicator.DescriptionEn;

                    rtxSelectedStateOrChange.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.StateOrChange.LabelCs : metadataSource.StateOrChange.LabelEn;
                    rtxSelectedStateOrChangeDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.StateOrChange.DescriptionCs : metadataSource.StateOrChange.DescriptionEn;
                    rtxTargetDBStateOrChange.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.StateOrChange.LabelCs : metadataTarget.StateOrChange.LabelEn;
                    rtxTargetDBStateOrChangeDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.StateOrChange.DescriptionCs : metadataTarget.StateOrChange.DescriptionEn;

                    rtxSelectedUnit.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Unit.LabelCs : metadataSource.Unit.LabelEn;
                    rtxSelectedUnitDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.Unit.DescriptionCs : metadataSource.Unit.DescriptionEn;
                    rtxTargetDBUnit.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.Unit.LabelCs : metadataTarget.Unit.LabelEn;
                    rtxTargetDBUnitDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.Unit.DescriptionCs : metadataTarget.Unit.DescriptionEn;

                    const string strCore = "core";
                    const string strDivision = "division";

                    rtxSelectedLocalDensity.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedLocalDensityDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBLocalDensity.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBLocalDensityDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObjectType.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeLabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeLabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObjectTypeDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeDescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeDescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBObjectType.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeLabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeLabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBObjectTypeDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeDescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectTypeDescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObject.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectLabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectLabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedObjectDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectDescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectDescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBObject.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectLabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectLabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBObjectDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectDescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.ObjectDescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedVersion.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedVersionDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBVersion.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBVersionDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Version.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedDefinitionVariant.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedDefinitionVariantDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBDefinitionVariant.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBDefinitionVariantDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.DefinitionVariant.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedAreaDomain.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedAreaDomainDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBAreaDomain.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBAreaDomainDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.AreaDomain.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedPopulation.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedPopulationDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBPopulation.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.LabelCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.LabelEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBPopulationDes.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.DescriptionCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.Population.DescriptionEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxSelectedUseNegative.Text = (LanguageVersion == LanguageVersion.National) ? metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.UseNegativeCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataSource.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.UseNegativeEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");

                    rtxTargetDBUseNegative.Text = (LanguageVersion == LanguageVersion.National) ? metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.UseNegativeCs)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}")
                    : metadataTarget.LocalDensities
                        .Where(a => a.ObjectTypeLabelEn == strCore || a.ObjectTypeLabelEn == strDivision)
                        .GroupBy(a => a.ObjectTypeLabelEn)
                        .Select(g => String.Join(
                                separator: " + ",
                                g.Select(a => a.UseNegativeEn)
                                ))
                        .Aggregate((a, b) => $"{a} | {b}");


                    RichTextBoxForeColorIfSourceDBIsNotEqualToTargetDB();

                    InsertCheckBoxColumnInDgv();

                    if (dgvDiff.CurrentRow != null)
                    {
                        bool metadataDiffNationalLanguage = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["metadata_diff_national_language"].Value);

                        if (LanguageVersion == LanguageVersion.National && metadataDiffNationalLanguage)
                        {
                            btnUpdate.Enabled = true;
                        }

                        bool metadataDiffEnglishLanguage = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["metadata_diff_english_language"].Value);

                        if (LanguageVersion == LanguageVersion.International && metadataDiffEnglishLanguage)
                        {
                            btnUpdate.Enabled = true;
                        }
                    }
                }

                if (item.MetadataDiffNationalLanguage.GetValueOrDefault() || item.MetadataDiffEnglishLanguage.GetValueOrDefault() || item.MissingMetadataNationalLanguage.GetValueOrDefault())
                {
                    shouldClearDataAndCollapsePanel = false;
                    break;
                }
            }

            if (shouldClearDataAndCollapsePanel)
            {
                ClearFormData();
                splHorizontal.Panel2Collapsed = true;
                return;
            }

            if (LanguageVersion == LanguageVersion.National)
            {
                bool missingMetadataNationalLanguage = result.Items.Any(item => item.MissingMetadataNationalLanguage.GetValueOrDefault());
                btnUpdate.Enabled = !missingMetadataNationalLanguage;
            }
        }

        /// <summary>
        /// <para lang="cs">Odstranění dat na formuláři</para>
        /// <para lang="en">Clearing the data in the form</para>
        /// </summary>
        private void ClearFormData()
        {
            // remove the item from the BindingSource
            if (dgvDiff.CurrentRow != null)
            {
                bindingSource.Remove(dgvDiff.CurrentRow.DataBoundItem);
            }

            //// clear all the RichTextBoxes
            //rtxSelectedIndicator.Clear();
            //rtxSelectedIndicatorDes.Clear();
            //rtxTargetDBIndicator.Clear();
            //rtxTargetDBIndicatorDes.Clear();
            //rtxSelectedStateOrChange.Clear();
            //rtxSelectedStateOrChangeDes.Clear();
            //rtxTargetDBStateOrChange.Clear();
            //rtxTargetDBStateOrChangeDes.Clear();
            //rtxSelectedUnit.Clear();
            //rtxSelectedUnitDes.Clear();
            //rtxTargetDBUnit.Clear();
            //rtxTargetDBUnitDes.Clear();
            //rtxSelectedLocalDensity.Clear();
            //rtxSelectedLocalDensityDes.Clear();
            //rtxTargetDBLocalDensity.Clear();
            //rtxTargetDBLocalDensityDes.Clear();
            //rtxSelectedObjectType.Clear();
            //rtxSelectedObjectTypeDes.Clear();
            //rtxTargetDBObjectType.Clear();
            //rtxTargetDBObjectTypeDes.Clear();
            //rtxSelectedObject.Clear();
            //rtxSelectedObjectDes.Clear();
            //rtxTargetDBObject.Clear();
            //rtxTargetDBObjectDes.Clear();
            //rtxSelectedVersion.Clear();
            //rtxSelectedVersionDes.Clear();
            //rtxTargetDBVersion.Clear();
            //rtxTargetDBVersionDes.Clear();
            //rtxSelectedDefinitionVariant.Clear();
            //rtxSelectedDefinitionVariantDes.Clear();
            //rtxTargetDBDefinitionVariant.Clear();
            //rtxTargetDBDefinitionVariantDes.Clear();
            //rtxSelectedAreaDomain.Clear();
            //rtxSelectedAreaDomainDes.Clear();
            //rtxTargetDBAreaDomain.Clear();
            //rtxTargetDBAreaDomainDes.Clear();
            //rtxSelectedPopulation.Clear();
            //rtxSelectedPopulationDes.Clear();
            //rtxTargetDBPopulation.Clear();
            //rtxTargetDBPopulationDes.Clear();
            //rtxSelectedUseNegative.Clear();
            //rtxTargetDBUseNegative.Clear();
        }

        /// <summary>
        /// <para lang="cs">Přepínání mezi jazyky hlaviček sloupců DataGridView dgvDIff</para>
        /// <para lang="en">Přepínání mezi jazyky hlaviček sloupců in the dgvDiff DataGridView</para>
        /// </summary>
        private void UpdateColumnHeaders()
        {
            Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    dgvDiff.Columns["metadata_source"].HeaderText = labels.ContainsKey(key: "metadataSource") ? labels["metadataSource"] : String.Empty;
                    dgvDiff.Columns["metadata_diff_national_language"].HeaderText = labels.ContainsKey(key: "metadataDiffNationalLanguage") ? labels["metadataDiffNationalLanguage"] : String.Empty;
                    dgvDiff.Columns["metadata_diff_english_language"].HeaderText = labels.ContainsKey(key: "metadataDiffEnglishLanguage") ? labels["metadataDiffEnglishLanguage"] : String.Empty;
                    dgvDiff.Columns["missing_metadata_national_language"].HeaderText = labels.ContainsKey(key: "missingMetadataNationalLanguage") ? labels["missingMetadataNationalLanguage"] : String.Empty;
                    break;

                case LanguageVersion.International:
                    dgvDiff.Columns["metadata_source"].HeaderText = labels.ContainsKey(key: "metadataSource") ? labels["metadataSource"] : String.Empty;
                    dgvDiff.Columns["metadata_diff_national_language"].HeaderText = labels.ContainsKey(key: "metadataDiffNationalLanguage") ? labels["metadataDiffNationalLanguage"] : String.Empty;
                    dgvDiff.Columns["metadata_diff_english_language"].HeaderText = labels.ContainsKey(key: "metadataDiffEnglishLanguage") ? labels["metadataDiffEnglishLanguage"] : String.Empty;
                    dgvDiff.Columns["missing_metadata_national_language"].HeaderText = labels.ContainsKey(key: "missingMetadataNationalLanguage") ? labels["missingMetadataNationalLanguage"] : String.Empty;
                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktualizace metadat</para>
        /// <para lang="en">Metadata update</para>
        /// </summary>
        public void ExecuteUpdate()
        {
            //update only if checkBox is checked (code if (dgvDiff.CurrentRow != null), bool isSelected = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["Select"].Value); if (isSelected))
            if (dgvDiff.CurrentRow != null)
            {
                bool isSelected = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["Select"].Value);

                if (isSelected)
                {
                    if (result.Items.Count > 0)
                    {
                        // find the row
                        FnEtlCheckTargetVariableMetadatasType item = result.Items[0];

                        int targetVariableTarget = item.TargetVariableTargetId;
                        string metadataSource = defaultMetadataSource;
                        bool metadataDiffNationalLanguage = (bool)item.MetadataDiffNationalLanguage;
                        bool metadataDiffEnglishLanguage = (bool)item.MetadataDiffEnglishLanguage;
                        bool missingMetadataNationalLanguage = (bool)item.MissingMetadataNationalLanguage;

                        if (!transactionRunning)
                        {
                            transaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();
                            transactionRunning = true;
                        }

                        if (nationalLang.NatLang == Language.CS && LanguageVersion == LanguageVersion.National && !csUpdateExecuted)
                        {
                            NfiEstaFunctions.FnEtlUpdateTargetVariableMetadata.Execute
                                (FormUserPassword.TargetDatabase,
                                targetVariableTarget,
                                metadataSource,
                                metadataDiffNationalLanguage,
                                metadataDiffEnglishLanguage,
                                missingMetadataNationalLanguage,
                                false,  //english_language
                                true,   //national_language
                                nationalLang.NatLang,
                                transaction);

                            csUpdateExecuted = true;

                            LoadContent(transaction);

                            ShowMessageBoxConfirm();
                        }

                        if (nationalLang.NatLang == Language.CS && LanguageVersion == LanguageVersion.International && !enUpdateExecuted)
                        {
                            NfiEstaFunctions.FnEtlUpdateTargetVariableMetadata.Execute
                                (FormUserPassword.TargetDatabase,
                                targetVariableTarget,
                                metadataSource,
                                metadataDiffNationalLanguage,
                                metadataDiffEnglishLanguage,
                                missingMetadataNationalLanguage,
                                true,  //english_language
                                false,   //national_language
                                nationalLang.NatLang,
                                transaction);

                            enUpdateExecuted = true;

                            LoadContent(transaction);

                            ShowMessageBoxConfirm();
                        }

                    }
                    else
                    {
                        ClearFormData();
                        splHorizontal.Panel2Collapsed = true;
                    }
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Vožení sloupce se zašrtávacími políčky do DataGridView dgvDiff</para>
        /// <para lang="en">Insert a check box column in the dgvDiff DataGridView</para>
        /// </summary>
        private void InsertCheckBoxColumnInDgv()
        {
            //insert the checkBoxColumn in the dataGridView
            bool selectColumnExists = false;
            foreach (DataGridViewColumn column in dgvDiff.Columns)
            {
                if (column.Name == "Select" && column is DataGridViewCheckBoxColumn) //because it appeared more times
                {
                    selectColumnExists = true;
                    break;
                }
            }

            if (!selectColumnExists)
            {
                DataGridViewCheckBoxColumn checkboxColumn = new DataGridViewCheckBoxColumn()
                {
                    Name = "Select",
                    HeaderText = "",
                    Width = 50
                };
                dgvDiff.Columns.Insert(0, checkboxColumn); //insert the column at the first position;
            }
            else
            {
                dgvDiff.Columns["Select"].DisplayIndex = 0; //move the existing "Select" column to the first position
            }
        }

        /// <summary>
        /// <para lang="cs">Obarvení textu červeně v RichTextBox, pokud se data ve zdrojové databázi neshodují s daty v cílové databázi</para>
        /// <para lang="en">Coloring the text red in RichTextBox if the data in the source database does not match the data in the target database</para>
        /// </summary>
        private void RichTextBoxForeColorIfSourceDBIsNotEqualToTargetDB()
        {
            if (rtxSelectedIndicator.Text != rtxTargetDBIndicator.Text)
            {
                rtxSelectedIndicator.ForeColor = Color.Red;
                rtxTargetDBIndicator.ForeColor = Color.Red;
            }
            else //if else missing, the color is not changing properly when switching language
            {
                rtxSelectedIndicator.ForeColor = Color.Black;
                rtxTargetDBIndicator.ForeColor = Color.Black;
            }

            if (rtxSelectedIndicatorDes.Text != rtxTargetDBIndicatorDes.Text)
            {
                rtxSelectedIndicatorDes.ForeColor = Color.Red;
                rtxTargetDBIndicatorDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedIndicatorDes.ForeColor = Color.Black;
                rtxTargetDBIndicatorDes.ForeColor = Color.Black;
            }

            if (rtxSelectedStateOrChange.Text != rtxTargetDBStateOrChange.Text)
            {
                rtxSelectedStateOrChange.ForeColor = Color.Red;
                rtxTargetDBStateOrChange.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedStateOrChange.ForeColor = Color.Black;
                rtxTargetDBStateOrChange.ForeColor = Color.Black;
            }

            if (rtxSelectedStateOrChangeDes.Text != rtxTargetDBStateOrChangeDes.Text)
            {
                rtxSelectedStateOrChangeDes.ForeColor = Color.Red;
                rtxTargetDBStateOrChangeDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedStateOrChangeDes.ForeColor = Color.Black;
                rtxTargetDBStateOrChangeDes.ForeColor = Color.Black;
            }

            if (rtxSelectedUnit.Text != rtxTargetDBUnit.Text)
            {
                rtxSelectedUnit.ForeColor = Color.Red;
                rtxTargetDBUnit.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedUnit.ForeColor = Color.Black;
                rtxTargetDBUnit.ForeColor = Color.Black;
            }

            if (rtxSelectedUnitDes.Text != rtxTargetDBUnitDes.Text)
            {
                rtxSelectedUnitDes.ForeColor = Color.Red;
                rtxTargetDBUnitDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedUnitDes.ForeColor = Color.Black;
                rtxTargetDBUnitDes.ForeColor = Color.Black;
            }

            if (rtxSelectedLocalDensity.Text != rtxTargetDBLocalDensity.Text)
            {
                rtxSelectedLocalDensity.ForeColor = Color.Red;
                rtxTargetDBLocalDensity.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedLocalDensity.ForeColor = Color.Black;
                rtxTargetDBLocalDensity.ForeColor = Color.Black;
            }

            if (rtxSelectedLocalDensityDes.Text != rtxTargetDBLocalDensityDes.Text)
            {
                rtxSelectedLocalDensityDes.ForeColor = Color.Red;
                rtxTargetDBLocalDensityDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedLocalDensityDes.ForeColor = Color.Black;
                rtxTargetDBLocalDensityDes.ForeColor = Color.Black;
            }

            if (rtxSelectedObjectType.Text != rtxTargetDBObjectType.Text)
            {
                rtxSelectedObjectType.ForeColor = Color.Red;
                rtxTargetDBObjectType.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedObjectType.ForeColor = Color.Black;
                rtxTargetDBObjectType.ForeColor = Color.Black;
            }

            if (rtxSelectedObjectTypeDes.Text != rtxTargetDBObjectTypeDes.Text)
            {
                rtxSelectedObjectTypeDes.ForeColor = Color.Red;
                rtxTargetDBObjectTypeDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedObjectTypeDes.ForeColor = Color.Black;
                rtxTargetDBObjectTypeDes.ForeColor = Color.Black;
            }

            if (rtxSelectedObject.Text != rtxTargetDBObject.Text)
            {
                rtxSelectedObject.ForeColor = Color.Red;
                rtxTargetDBObject.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedObject.ForeColor = Color.Black;
                rtxTargetDBObject.ForeColor = Color.Black;
            }

            if (rtxSelectedObjectDes.Text != rtxTargetDBObjectDes.Text)
            {
                rtxSelectedObjectDes.ForeColor = Color.Red;
                rtxTargetDBObjectDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedObjectDes.ForeColor = Color.Black;
                rtxTargetDBObjectDes.ForeColor = Color.Black;
            }

            if (rtxSelectedVersion.Text != rtxTargetDBVersion.Text)
            {
                rtxSelectedVersion.ForeColor = Color.Red;
                rtxTargetDBVersion.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedVersion.ForeColor = Color.Black;
                rtxTargetDBVersion.ForeColor = Color.Black;
            }

            if (rtxSelectedVersionDes.Text != rtxTargetDBVersionDes.Text)
            {
                rtxSelectedVersionDes.ForeColor = Color.Red;
                rtxTargetDBVersionDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedVersionDes.ForeColor = Color.Black;
                rtxTargetDBVersionDes.ForeColor = Color.Black;
            }

            if (rtxSelectedDefinitionVariant.Text != rtxTargetDBDefinitionVariant.Text)
            {
                rtxSelectedDefinitionVariant.ForeColor = Color.Red;
                rtxTargetDBDefinitionVariant.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedDefinitionVariant.ForeColor = Color.Black;
                rtxTargetDBDefinitionVariant.ForeColor = Color.Black;
            }

            if (rtxSelectedDefinitionVariantDes.Text != rtxTargetDBDefinitionVariantDes.Text)
            {
                rtxSelectedDefinitionVariantDes.ForeColor = Color.Red;
                rtxTargetDBDefinitionVariantDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedDefinitionVariantDes.ForeColor = Color.Black;
                rtxTargetDBDefinitionVariantDes.ForeColor = Color.Black;
            }

            if (rtxSelectedAreaDomain.Text != rtxTargetDBAreaDomain.Text)
            {
                rtxSelectedAreaDomain.ForeColor = Color.Red;
                rtxTargetDBAreaDomain.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedAreaDomain.ForeColor = Color.Black;
                rtxTargetDBAreaDomain.ForeColor = Color.Black;
            }

            if (rtxSelectedAreaDomainDes.Text != rtxTargetDBAreaDomainDes.Text)
            {
                rtxSelectedAreaDomainDes.ForeColor = Color.Red;
                rtxTargetDBAreaDomainDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedAreaDomainDes.ForeColor = Color.Black;
                rtxTargetDBAreaDomainDes.ForeColor = Color.Black;
            }

            if (rtxSelectedPopulation.Text != rtxTargetDBPopulation.Text)
            {
                rtxSelectedPopulation.ForeColor = Color.Red;
                rtxTargetDBPopulation.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedPopulation.ForeColor = Color.Black;
                rtxTargetDBPopulation.ForeColor = Color.Black;
            }

            if (rtxSelectedPopulationDes.Text != rtxTargetDBPopulationDes.Text)
            {
                rtxSelectedPopulationDes.ForeColor = Color.Red;
                rtxTargetDBPopulationDes.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedPopulationDes.ForeColor = Color.Black;
                rtxTargetDBPopulationDes.ForeColor = Color.Black;
            }

            if (rtxSelectedUseNegative.Text != rtxTargetDBUseNegative.Text)
            {
                rtxSelectedUseNegative.ForeColor = Color.Red;
                rtxTargetDBUseNegative.ForeColor = Color.Red;
            }
            else
            {
                rtxSelectedUseNegative.ForeColor = Color.Black;
                rtxTargetDBUseNegative.ForeColor = Color.Black;
            }

        }


        /// <summary>
        /// <para lang="cs">Aktualizuje povolený stav tlačítka btnUpdate na základě hodnot metadata_diff_national_language,
        /// metadata_diff_english_language aktuálního řádku a stavu buňky CheckBox ve sloupci "Select".</para>
        /// <para lang="en">Updates the enabled status of the btnUpdate button based on the current row's metadata_diff_national_language,
        /// metadata_diff_english_language values and the state of the CheckBox cell in the "Select" column.</para>
        /// </summary>
        private void UpdateBtnUpdateStatus()
        {
            if (dgvDiff.CurrentRow != null)
            {
                bool metadataDiffNationalLanguage = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["metadata_diff_national_language"].Value);
                bool metadataDiffEnglishLanguage = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["metadata_diff_english_language"].Value);
                bool missingMetadataNationalLanguage = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["missing_metadata_national_language"].Value);
                DataGridViewCheckBoxCell checkboxCell = (DataGridViewCheckBoxCell)dgvDiff.CurrentRow.Cells["Select"];

                // Check if the checkbox's value is not null before using it
                bool checkboxValue = checkboxCell.Value != null && (bool)checkboxCell.Value;

                if (LanguageVersion == LanguageVersion.National && metadataDiffNationalLanguage && checkboxValue && !missingMetadataNationalLanguage)
                {
                    btnUpdate.Enabled = true;
                }
                else if (LanguageVersion == LanguageVersion.International && metadataDiffEnglishLanguage && checkboxValue)
                {
                    btnUpdate.Enabled = true;
                }
                else
                {
                    btnUpdate.Enabled = false;
                }
            }
            else
            {
                btnUpdate.Enabled = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktualizuje povolený stav tlačítka btnInsert na základě aktuálního řádku DataGridView a zvoleného jazyka.
        /// Metoda kontroluje aktuální řádek v DataGridView, hodnotu buňky u "missing_metadata_national_language", checkbox u sloupce "Select"
        /// a zvolený jazyk.</para>
        /// <para lang="en">Updates the enabled state of the "Insert" button based on the current DataGridView row and selected language.
        /// This method checks the current row in the DataGridView, the "missing_metadata_national_language" cell value,
        /// the "Select" cell's checkbox value, and the selected language.</para>
        /// </summary>
        private void UpdateBtnInsertStatus()
        {
            if (dgvDiff.CurrentRow != null)
            {
                bool missingMetadataNationalLanguage = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["missing_metadata_national_language"].Value);
                DataGridViewCheckBoxCell checkboxCell = (DataGridViewCheckBoxCell)dgvDiff.CurrentRow.Cells["Select"];

                // Check if the checkbox's value is not null before using it
                bool checkboxValue = checkboxCell.Value != null && (bool)checkboxCell.Value;

                if (LanguageVersion == LanguageVersion.National && missingMetadataNationalLanguage && checkboxValue)
                {
                    btnInsert.Enabled = true;
                }
                else
                {
                    btnInsert.Enabled = false;
                }
            }
            else
            {
                btnInsert.Enabled = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí potvrzovací formulář a zpracovává volbu uživatele pro potvrzení nebo vrácení transakce</para>
        /// <para lang="en">Displays a confirmation form and handles the user's choice for committing or rolling back a transaction</para>
        /// </summary>
        private void ShowMessageBoxConfirm()
        {
            Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

            DialogResult dialogResult = MessageBox.Show(
                                        text: messages.ContainsKey(key: "msgConfirm") ?
                                                messages["msgConfirm"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.YesNo,
                                        icon: MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                if (transactionRunning)
                {
                    try
                    {
                        transaction?.Commit();
                    }
                    catch (InvalidOperationException /*ex*/)
                    {
                        //MessageBox.Show(ex.Message, "Invalid operation exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    transactionRunning = false;
                    btnUpdate.Enabled = false;
                }
            }
            else
            {
                if (transactionRunning)
                {
                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException /*ex*/)
                    {
                        //MessageBox.Show(ex.Message, "Invalid operation exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    transactionRunning = false;
                }

                // Reset the executed flags for the corresponding language
                if (nationalLang.NatLang == Language.CS && LanguageVersion == LanguageVersion.National)
                {
                    csUpdateExecuted = false;
                }
                else if (nationalLang.NatLang == Language.CS && LanguageVersion == LanguageVersion.International)
                {
                    enUpdateExecuted = false;
                }

                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí potvrzovací formulář a zpracovává volbu uživatele pro potvrzení nebo vrácení transakce pro insert</para>
        /// <para lang="en">Displays a confirmation form and handles the user's choice for committing or rolling back a transaction regarding the insert</para>
        /// </summary>
        private void ShowMessageBoxConfirmForInsert()
        {
            Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

            DialogResult dialogResult = MessageBox.Show(
                                        text: messages.ContainsKey(key: "msgConfirm") ?
                                                messages["msgConfirm"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.YesNo,
                                        icon: MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                // Commit the transaction
                if (transactionRunning)
                {
                    try
                    {
                        transaction?.Commit();
                    }
                    catch (InvalidOperationException /*ex*/)
                    {
                        //MessageBox.Show(ex.Message, "Invalid operation exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    transactionRunning = false;
                    btnInsert.Enabled = false;
                }
            }
            else
            {
                if (transactionRunning)
                {
                    // Reset the initial state
                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException /*ex*/)
                    {
                        //MessageBox.Show(ex.Message, "Invalid operation exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    transactionRunning = false;
                    btnUpdate.Enabled = false;
                }
                // Reload content after rolling back the transaction
                LoadContent();
            }
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Změnit jazyk"</para>
        /// <para lang="en">Handling the "Change language" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripMenuItem)</para>
        /// <para lang="en">Object that sends the event (ToolStripMenuItem)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void TsmiLanguage_Click(object sender, EventArgs e)
        {
            //CtrEtl.LanguageVersion =
            //    (LanguageVersion == LanguageVersion.International) ?
            //        LanguageVersion.National :
            //        LanguageVersion.International;
            LanguageVersion = (LanguageVersion == LanguageVersion.International) ?
               LanguageVersion.National :
               LanguageVersion.International;

            InitializeLabels();

            btnInsert.Visible = (LanguageVersion == LanguageVersion.National);

            LoadContent();

            UpdateBtnUpdateStatus();
            UpdateBtnInsertStatus();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Next" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            SourceTransaction = Database.Postgres.BeginTransaction();
            TargetTransaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();

            Dictionary<string, string> messages = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

            DialogResult dialogResult = MessageBox.Show(
                                        text: messages.ContainsKey(key: "msgContinue") ?
                                                messages["msgContinue"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.YesNoCancel,
                                        icon: MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                ControlPanelRefyearset controlPanelRefyearset = new ControlPanelRefyearset(
                controlOwner: this,
                idEtlFromTargetVariable: null,
                idEtlFromTargetVariableCompare: null,
                idEtl: _idEtl,
                refYearSetToPanelMapping: _refYearSetToPanelMapping,
                selectedExportConnectionId: _selectedExportConnectionId,
                atomicAreaDomains: _atomicAreaDomains,
                atomicSubPopulations: _atomicSubPopulations,
                selectedIndicatorLabelCs: _selectedIndicatorLabelCs,
                selectedIndicatorLabelEn: _selectedIndicatorLabelEn,
                sourceTransaction: SourceTransaction,
                targetTransaction: TargetTransaction)
                {
                    Dock = DockStyle.Fill
                };

                controlPanelRefyearset.LoadContent();
                controlPanelRefyearset.InitializeLabels();

                CtrEtl.ShowControl(control: controlPanelRefyearset);

                Close();
            }
            else if (dialogResult == DialogResult.No)
            {
                Close();

                ControlTargetVariable controlTargetVariable = ControlOwner as ControlTargetVariable;

                if (controlTargetVariable != null && controlTargetVariable.Parent != null)
                {
                    controlTargetVariable.Parent.Controls.Remove(controlTargetVariable);
                    controlTargetVariable.Dispose();
                }

                Database.Postgres?.CloseConnection();
                FormUserPassword.Disconnect();
            }
            else
            {
                SourceTransaction.Rollback();
                TargetTransaction.Rollback();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Aktualizovat"</para>
        /// <para lang="en">Handling the "Update" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            ExecuteUpdate();
            //LoadContent(transaction);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události překreslení zobrazení buněk v DataGridView dgvDiff</para>
        /// <para lang="en">Event handler of the cell view redraw in DataGridView dgvDiff</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvDiff_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // change the standard display of boolean results in certain columns to text
            if ((e.ColumnIndex == dgvDiff.Columns["metadata_diff_national_language"].Index
                    || e.ColumnIndex == dgvDiff.Columns["metadata_diff_english_language"].Index
                    || e.ColumnIndex == dgvDiff.Columns["missing_metadata_national_language"].Index)
                && e.RowIndex >= 0)
            {
                e.PaintBackground(e.CellBounds, false);

                Dictionary<string, string> labels = Dictionary(
                       languageVersion: LanguageVersion,
                       languageFile: LanguageFile);

                bool cellValue = (bool)e.Value;
                string text = (LanguageVersion == LanguageVersion.National && cellValue) ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty
                            : (LanguageVersion == LanguageVersion.National && !cellValue) ? labels.ContainsKey(key: "no") ? labels["no"] : String.Empty
                            : cellValue ? labels.ContainsKey(key: "yes") ? labels["yes"] : String.Empty : labels.ContainsKey(key: "no") ? labels["no"] : String.Empty;

                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    // set the font to Microsoft Sans Serif, 9.75pt
                    e.CellStyle.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular);

                    // move the text lower by 3 pixels
                    Rectangle textRect = new Rectangle(e.CellBounds.X, e.CellBounds.Y + 3,
                                                        e.CellBounds.Width, e.CellBounds.Height - 3);
                    e.Graphics.DrawString(text, e.CellStyle.Font, brush, textRect);
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na obsah buňky v DataGridView dgvDiff</para>
        /// <para lang="en">Event handler of the cell content click formatting in DataGridView dgvDiff</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvDiff_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvDiff.Columns["Select"].Index && e.RowIndex >= 0)
            {
                DataGridViewCheckBoxCell checkboxCell = (DataGridViewCheckBoxCell)dgvDiff.Rows[e.RowIndex].Cells["Select"];

                if (checkboxCell.Value == null)
                {
                    // checkbox is in indeterminate state
                    // handle it accordingly (e.g. toggle the collapsed panel)
                    splHorizontal.Panel2Collapsed = !splHorizontal.Panel2Collapsed;
                }
                else
                {
                    // checkbox is checked
                    splHorizontal.Panel2Collapsed = false;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události najetí myší na požadovaný RichTextBox</para>
        /// <para lang="en">Event handler of the mouse hover on the required RichTextBox</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (RichTextBox)</para>
        /// <para lang="en">Object that sends the event (RichTextBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RichTextBox_MouseHover(object sender, EventArgs e)
        {
            if (sender == null)
            {
                return;
            }

            RichTextBox richTextBox = (RichTextBox)sender;
            toolTip.SetToolTip(
                control: richTextBox,
                caption: richTextBox.Text);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Vložit"</para>
        /// <para lang="en">Handling the "Insert" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnInsert_Click(object sender, EventArgs e)
        {
            if (dgvDiff.CurrentRow != null)
            {
                bool isSelected = Convert.ToBoolean(dgvDiff.CurrentRow.Cells["Select"].Value);

                if (isSelected)
                {
                    // Find the row
                    FnEtlCheckTargetVariableMetadatasType item = result.Items[0];

                    int targetVariableTarget = item.TargetVariableTargetId;
                    string metadataSource = defaultMetadataSource;
                    bool missingMetadataNationalLanguage = (bool)item.MissingMetadataNationalLanguage;

                    if (missingMetadataNationalLanguage == true)
                    {
                        if (!transactionRunning)
                        {
                            transaction = FormUserPassword.TargetDatabase.Postgres.BeginTransaction();
                            transactionRunning = true;
                        }

                        NfiEstaFunctions.FnEtlInsertTargetVariableMetadata.Execute
                            (FormUserPassword.TargetDatabase,
                            targetVariableTarget,
                            metadataSource,
                            missingMetadataNationalLanguage,
                            nationalLang.NatLang,
                            transaction);

                        //ClearFormData();

                        //splHorizontal.Panel2Collapsed = true;
                        LoadContent(transaction);

                        ShowMessageBoxConfirmForInsert();
                    }
                }
            }

            //LoadContent(transaction);
            btnUpdate.Enabled = false;
        }

        /// <summary>
        /// <para lang="cs">Obsluha události CellValueChanged pro DataGridView dgvDiff.
        /// Pokud změněná buňka patří do sloupce "Select", aktualizuje tato metoda stav zapnutí tlačítka btnUpdate na základě
        /// aktuálního stavu buněk CheckBox ve sloupci "Select".</para>
        /// <para lang="en">Handles the CellValueChanged event for the DataGridView (dgvDiff).
        /// If the changed cell belongs to the "Select" column, this method updates the btnUpdate button's enabled status
        /// based on the current state of the CheckBox cells in the "Select" column.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void DgvDiff_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDiff.Columns.Contains("Select") && e.ColumnIndex == dgvDiff.Columns["Select"].Index && e.RowIndex >= 0)
            {
                UpdateBtnUpdateStatus();
                UpdateBtnInsertStatus();
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události CurrentCellDirtyStateChanged pro DataGridView dgvDiff.
        /// Pokud je aktuální buňka "dirty" (její hodnota se změnila, ale nebyla komitována), metoda komituje úpravu.
        /// Tím je zajištěno, že změny jsou okamžitě komitovány, což je užitečné zejména u sloupců se zaškrtávacím políčkem.</para>
        /// <para lang="en">Handles the CurrentCellDirtyStateChanged event for the DataGridView (dgvDiff).
        /// If the current cell is dirty (its value has changed but not committed), the method commits the edit.
        /// This ensures that the changes are committed immediately, which is particularly useful for CheckBox columns.</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (DataGridView)</para>
        /// <para lang="en">Object that sends the event (DataGridView)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události (DataGridView)</para>
        /// <para lang="en">Parameters of the event (DataGridView)</para>
        /// </param>
        private void DgvDiff_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvDiff.IsCurrentCellDirty)
            {
                dgvDiff.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        #endregion Event Handlers

    }

}
