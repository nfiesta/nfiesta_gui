﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Připojení do cílové databáze"</para>
    /// <para lang="en">Control "Connection to the target database"</para>
    /// </summary>
    internal partial class ControlConnection
            : UserControl, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné"</para>
        /// <para lang="en">Selection of the target variable"</para>
        /// </summary>
        private ControlTargetVariable controlTargetVariable;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení</para>
        /// <para lang="en">Selected database connection id</para>
        /// </summary>
        private int selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Hostitel (server) zvoleného databázového připojení</para>
        /// <para lang="en">Selected database connection host (server)</para>
        /// </summary>
        private string selectedExportConnectionHost;

        /// <summary>
        /// <para lang="cs">Název databáze zvoleného databázového připojení</para>
        /// <para lang="en">Selected database connection name</para>
        /// </summary>
        private string selectedExportConnectionDbName;

        /// <summary>
        /// <para lang="cs">Port zvoleného databázového připojení</para>
        /// <para lang="en">Selected database connection port</para>
        /// </summary>
        private int selectedExportConnectionPort;

        /// <summary>
        /// <para lang="cs">Popis zvoleného databázového připojení</para>
        /// <para lang="en">Selected database connection description</para>
        /// </summary>
        private string selectedExportConnectionComment;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        /// <summary>
        /// <para lang="cs">Výsledek volání funkce FnEtlCheckSubPopulationsForUpdate</para>
        /// <para lang="en">Result of calling the FnEtlCheckSubPopulationsForUpdate function</para>
        /// </summary>
        private DataTable diffSP;

        /// <summary>
        /// <para lang="cs">Výsledek volání funkce FnEtlCheckSubPopulationCategoriesForSubPopulations</para>
        /// <para lang="en">Result of calling the FnEtlCheckSubPopulationCategoriesForSubPopulations function</para>
        /// </summary>
        private List<int?> diffSPC;

        /// <summary>
        /// <para lang="cs">Výsledek volání funkce FnEtlCheckAreaDomainsForUpdate</para>
        /// <para lang="en">Result of calling the FnEtlCheckAreaDomainsForUpdate function</para>
        /// </summary>
        private DataTable diffAD;

        /// <summary>
        /// <para lang="cs">Výsledek volání funkce FnEtlCheckAreaDomainCategoriesForAreaDomains</para>
        /// <para lang="en">Result of calling the FnEtlCheckAreaDomainCategoriesForAreaDomains function</para>
        /// </summary>
        private List<int?> diffADC;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">DataGridView zobrazující tabulku t_export_connection</para>
        /// <para lang="en">DataGridView displaying the t_export_connection table</para>
        /// </summary>
        private DataGridView dgvConnection;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlConnection(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is INfiEstaControl))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of INfiEstaControl.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IETLControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IETLControl)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlEtl ctrEtl)
                {
                    return ctrEtl;
                }
                else if (ControlOwner is ControlTargetVariable ctrTargetVariable)
                {
                    return ctrTargetVariable.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení (read-only)</para>
        /// <para lang="en">Selected database connection id (read-only)</para>
        /// </summary>
        public int SelectedExportConnectionId
        {
            get
            {
                return selectedExportConnectionId;
            }
            set
            {
                selectedExportConnectionId = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Hostitel (server) zvoleného databázového připojení (read-only)</para>
        /// <para lang="en">Selected database connection host (server) (read-only)</para>
        /// </summary>
        public string SelectedExportConnectionHost
        {
            get
            {
                return selectedExportConnectionHost;
            }
            set
            {
                selectedExportConnectionHost = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Název databáze zvoleného databázového připojení (read-only)</para>
        /// <para lang="en">Selected database connection name (read-only)</para>
        /// </summary>
        public string SelectedExportConnectionDbName
        {
            get
            {
                return selectedExportConnectionDbName;
            }
            set
            {
                selectedExportConnectionDbName = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Port zvoleného databázového připojení (read-only)</para>
        /// <para lang="en">Selected database connection port (read-only)</para>
        /// </summary>
        public int SelectedExportConnectionPort
        {
            get
            {
                return selectedExportConnectionPort;
            }
            set
            {
                selectedExportConnectionPort = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Popis zvoleného databázového připojení (read-only)</para>
        /// <para lang="en">Selected database connection description (read-only)</para>
        /// </summary>
        public string SelectedExportConnectionComment
        {
            get
            {
                return selectedExportConnectionComment;
            }
            set
            {
                selectedExportConnectionComment = value;
            }
        }

        /// <summary>
        /// <para lang="cs">DataGridView zobrazující tabulku t_export_connection</para>
        /// <para lang="en">DataGridView displaying the t_export_connection table</para>
        /// </summary>
        public DataGridView DgvConnection
        {
            get
            {
                return dgvConnection;
            }
            set
            {
                dgvConnection = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                     "Volba cílové DB pro přenos lokálních hustot" },
                            { "grpConnection",                      "Nadefinovaná připojení:" },
                            { "tsrConnection",                      String.Empty },
                            { "btnConnectionAdd",                   "Vytvořit nové připojení" },
                            { "btnConnectionUpdate",                "Editovat zvolené připojení" },
                            { "btnConnectionDelete",                "Smazat zvolené připojení" },
                            { "btnNext",                            "Další"},
                            { "btnCheckMetadata",                   "Aktualizovat metadata"},
                            { "btnCheckAdSp",                       "Aktualizovat kategorizace"},
                            { "msgNoConnectionSelected",            "Není vybráno žádné připojení." },
                            { "msgDeleteSelectedConnection",        "U vybraného připojení již proběhl přenos dat. Chcete jej opravdu smazat?" },
                            { "msgNoEntry",                         "Nejsou zde žádná metadata ke kontrole." },
                            { "msgNoEntryForAdAndSp",               "Buď neexistují žádné kategorizace ke kontrole, nebo se stávající kategorizace shodují mezi zdrojovou a cílovou DB." },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}",
                                "Identifikační číslo" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}",
                                "Identifikační číslo" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}",
                                "Server" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}",
                                "Server" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}",
                                "Port" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}",
                                "Port" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}",
                                "Název databáze" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}",
                                "Název databáze" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}",
                                "Popis" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}",
                                "Popis" },
                            { "connected",                          "Připojeno"}
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "ControlConnection") ?
                    languageFile.NationalVersion.Data["ControlConnection"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "lblMainCaption",                     "Choice of the target DB for local density transfer" },
                            { "grpConnection",                      "Predefined connections:" },
                            { "tsrConnection",                      String.Empty },
                            { "btnConnectionAdd",                   "Create a new connection" },
                            { "btnConnectionUpdate",                "Edit the selected connection" },
                            { "btnConnectionDelete",                "Delete the selected connection" },
                            { "btnNext",                            "Next" },
                            { "btnCheckMetadata",                   "Update metadata" },
                            { "btnCheckAdSp",                       "Update categorisations"},
                            { "msgNoConnectionSelected",            "No connection is selected." },
                            { "msgDeleteSelectedConnection",        "The data has already been transferred within the selected connection. Do you really want to delete it?" },
                            { "msgNoEntry",                         "There is no metadata to check." },
                            { "msgNoEntryForAdAndSp",               "Either there are no categorisations to check or the existing categorisations match between the source and target DB." },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}",
                                "ID" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}",
                                "ID" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}",
                                "Server" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}",
                                "Server" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}",
                                "Port" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}",
                                "Port" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}",
                                "Database" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}",
                                "Database" },
                            { $"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}",
                                "Description" },
                            { $"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}",
                                "Description" },
                            { "connected",                          "Connected"}
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "ControlConnection") ?
                    languageFile.InternationalVersion.Data["ControlConnection"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        /// <summary>
        /// <para lang="cs">Uloží stavy zaškrtnutých checkboxů v DataGridView do slovníku.</para>
        /// <para lang="en">Saves the states of checked checkboxes in the DataGridView into a dictionary.</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Slovník, kde klíčem je identifikátor připojení a hodnotou je booleanovský stav zaškrtnutí checkboxu.</para>
        /// <para lang="en">Dictionary where the key is the connection identifier and the value is the boolean state of the checkbox.</para>
        /// </returns>
        private Dictionary<int, bool> SaveCheckedConnectionsStates()
        {
            var states = new Dictionary<int, bool>();

            if (DgvConnection != null)
            {
                foreach (DataGridViewRow row in DgvConnection.Rows)
                {
                    if (row.Cells["id"].Value != null && row.Cells["Select"].Value != null)
                    {
                        int connectionId = Int32.Parse(row.Cells["id"].Value.ToString());
                        bool isChecked = Convert.ToBoolean(row.Cells["Select"].Value);
                        states.Add(connectionId, isChecked);
                    }
                }
            }

            return states;
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            nationalLang = new NationalLang();

            Visible = (Database != null) &&
                (Database.Postgres != null) &&
                Database.Postgres.Initialized;

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Cursor = Cursors.WaitCursor;

            // To hold the checkbox state when switching languages
            var checkedStates = SaveCheckedConnectionsStates();

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            grpConnection.Text =
                labels.ContainsKey(key: "grpConnection") ?
                labels["grpConnection"] : String.Empty;

            tsrConnection.Text =
                labels.ContainsKey(key: "tsrConnection") ?
                labels["tsrConnection"] : String.Empty;

            btnConnectionAdd.Text =
                labels.ContainsKey(key: "btnConnectionAdd") ?
                labels["btnConnectionAdd"] : String.Empty;

            btnConnectionUpdate.Text =
                labels.ContainsKey(key: "btnConnectionUpdate") ?
                labels["btnConnectionUpdate"] : String.Empty;

            btnConnectionDelete.Text =
                labels.ContainsKey(key: "btnConnectionDelete") ?
                labels["btnConnectionDelete"] : String.Empty;

            btnNext.Text =
               labels.ContainsKey(key: "btnNext") ?
               labels["btnNext"] : String.Empty;

            btnCheckMetadata.Text =
               labels.ContainsKey(key: "btnCheckMetadata") ?
               labels["btnCheckMetadata"] : String.Empty;

            btnCheckAdSp.Text =
               labels.ContainsKey(key: "btnCheckAdSp") ?
               labels["btnCheckAdSp"] : String.Empty;

            TDExportConnectionList.SetColumnHeader(
               columnName: TDExportConnectionList.ColId.Name,
               headerTextCs: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}"],
               headerTextEn: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}"],
               toolTipTextCs: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}"],
               toolTipTextEn: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColId.Name}"]
            );

            TDExportConnectionList.SetColumnHeader(
               columnName: TDExportConnectionList.ColHost.Name,
               headerTextCs: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}"],
               headerTextEn: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}"],
               toolTipTextCs: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}"],
               toolTipTextEn: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColHost.Name}"]
            );

            TDExportConnectionList.SetColumnHeader(
               columnName: TDExportConnectionList.ColPort.Name,
               headerTextCs: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}"],
               headerTextEn: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}"],
               toolTipTextCs: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}"],
               toolTipTextEn: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColPort.Name}"]
            );

            TDExportConnectionList.SetColumnHeader(
               columnName: TDExportConnectionList.ColDBName.Name,
               headerTextCs: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}"],
               headerTextEn: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}"],
               toolTipTextCs: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}"],
               toolTipTextEn: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColDBName.Name}"]
            );

            TDExportConnectionList.SetColumnHeader(
               columnName: TDExportConnectionList.ColComment.Name,
               headerTextCs: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}"],
               headerTextEn: labels[$"col-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}"],
               toolTipTextCs: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}"],
               toolTipTextEn: labels[$"tip-{TDExportConnectionList.Name}.{TDExportConnectionList.ColComment.Name}"]
            );

            // Aby se hlavička DataGridView přepínala mezi dvěma jazyky
            // (příkaz  result.FormatDataGridView(dgvConnectionLocal) z LoadContent()
            // To switch the DataGridView header between two languages
            // (result.FormatDataGridView(dgvConnectionLocal) statement from LoadContent())
            LoadContent();

            controlTargetVariable?.InitializeLabels();

            // To hold the checkbox state when switching languages
            RestoreCheckedConnectionsStates(checkedStates);

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            pnlConnection.Controls.Clear();
            DataGridView dgvConnectionLocal = new DataGridView() { Dock = DockStyle.Fill };
            pnlConnection.Controls.Add(dgvConnectionLocal);

            TDExportConnectionList result = TDFunctions.FnEtlGetExportConnections.Execute(Database);
            TDExportConnectionList.SetColumnsVisibility(
                (from a in TDExportConnectionList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());

            dgvConnectionLocal.DataSource = result.Data;

            dgvConnectionLocal.SelectionChanged += (sender, e) =>
            {
                if (dgvConnectionLocal.SelectedRows.Count > 0)
                {
                    DataGridViewRow row = dgvConnectionLocal.SelectedRows[0];

                    SelectedExportConnectionId = Int32.Parse(row.Cells["id"].Value.ToString());
                    SelectedExportConnectionHost = row.Cells["host"].Value.ToString();
                    SelectedExportConnectionDbName = row.Cells["dbname"].Value.ToString();
                    SelectedExportConnectionPort = Int32.Parse(row.Cells["port"].Value.ToString());
                    SelectedExportConnectionComment = row.Cells["comment"].Value.ToString();
                }
            };

            result.FormatDataGridView(dgvConnectionLocal);

            dgvConnectionLocal.AutoSizeColumnsMode =
                       DataGridViewAutoSizeColumnsMode.AllCells;

            //insert the checkboxColumn in the dataGridView
            bool selectColumnExists = false;
            foreach (DataGridViewColumn column in dgvConnectionLocal.Columns)
            {
                if (column.Name == "Select" && column is DataGridViewCheckBoxColumn) //because it appeared more times
                {
                    selectColumnExists = true;
                    break;
                }
            }

            Dictionary<string, string> labels = Dictionary(
                           languageVersion: LanguageVersion,
                           languageFile: LanguageFile);

            if (!selectColumnExists)
            {
                DataGridViewCheckBoxColumn checkboxColumn = new DataGridViewCheckBoxColumn()
                {
                    Name = "Select",
                    HeaderText = labels.ContainsKey("connected") ? labels["connected"] : String.Empty,
                    Width = 50
                };

                dgvConnectionLocal.Columns.Add(checkboxColumn); // add the checkboxColumn to the DataGridView
                checkboxColumn.DisplayIndex = dgvConnectionLocal.Columns.Count - 1; // move the column to the last position
            }
            else
            {
                dgvConnectionLocal.Columns["Select"].DisplayIndex = dgvConnectionLocal.Columns.Count - 1; // move the existing "Select" column to the last position
            }

            // Connect to the target DB
            dgvConnectionLocal.CellClick += (sender, e) =>
            {
                if (dgvConnectionLocal.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
                {
                    // Uncheck all other checkboxes
                    foreach (DataGridViewRow otherRow in dgvConnectionLocal.Rows)
                    {
                        if (otherRow.Index != e.RowIndex) // Check if it's not the row that was clicked
                        {
                            otherRow.Cells["Select"].Value = false;
                        }
                    }

                    DataGridViewRow row = dgvConnectionLocal.Rows[e.RowIndex];

                    SelectedExportConnectionHost = row.Cells["host"].Value.ToString();
                    SelectedExportConnectionDbName = row.Cells["dbname"].Value.ToString();
                    SelectedExportConnectionPort = Int32.Parse(row.Cells["port"].Value.ToString());

                    // Disconnect the target DB
                    if (FormUserPassword.TargetDatabase != null)
                    {
                        FormUserPassword.TargetDatabase.Postgres.CloseConnection();
                    }

                    btnNext.Enabled = false;
                    btnCheckMetadata.Enabled = false;
                    btnCheckAdSp.Enabled = false;
                    CtrEtl.lblStatusExportConnection.Visible = false;

                    using (FormUserPassword frm = new FormUserPassword(
                        controlOwner: this,
                        _SelectedExportConnectionHost: SelectedExportConnectionHost,
                        _SelectedExportConnectionDbName: SelectedExportConnectionDbName,
                        _SelectedExportConnectionPort: SelectedExportConnectionPort))
                    {
                        var dialogResult = frm.ShowDialog();

                        if (dialogResult == DialogResult.OK && frm.ConnectionInitialized)
                        {
                            // Update the checkbox upon successful connection
                            row.Cells["Select"].Value = true; // Using row from the previous code
                        }
                        else
                        {
                            // Uncheck the checkbox if the connection fails or the dialog is cancelled
                            row.Cells["Select"].Value = false;
                        }
                    }

                    // Prevent DataGridView from processing the click in the standard way
                    dgvConnectionLocal.BeginEdit(false);
                    dgvConnectionLocal.EndEdit();
                }
            };

            DgvConnection = dgvConnectionLocal;
        }

        /// <summary>
        /// <para lang="cs">Aktualizuje stav checkboxu pro nově přidané připojení v DataGridView.</para>
        /// <para lang="en">Updates the checkbox status for a newly added connection in the DataGridView.</para>
        /// </summary>
        /// <param name="newConnectionId">
        /// <para lang="cs">Identifikátor nově vytvořeného připojení.</para>
        /// <para lang="en">Identifier of the newly created connection.</para>
        /// </param>
        private void UpdateCheckboxForNewConnection(int? newConnectionId)
        {
            foreach (DataGridViewRow row in DgvConnection.Rows)
            {
                int connectionId = Int32.Parse(row.Cells["id"].Value.ToString());
                if (connectionId == newConnectionId)
                {
                    row.Cells["Select"].Value = true;
                    SelectedExportConnectionId = connectionId;
                    break;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obnoví stavy zaškrtnutých checkboxů v DataGridView na základě předaného slovníku stavů.</para>
        /// <para lang="en">Restores the states of checked checkboxes in the DataGridView based on the provided dictionary of states.</para>
        /// </summary>
        /// <param name="states">
        /// <para lang="cs">Slovník obsahující stavy checkboxů, kde klíčem je identifikátor připojení a hodnotou je stav zaškrtnutí.</para>
        /// <para lang="en">Dictionary containing the states of checkboxes, where the key is the connection identifier and the value is the checked state.</para>
        /// </param>
        private void RestoreCheckedConnectionsStates(Dictionary<int, bool> states)
        {
            foreach (DataGridViewRow row in DgvConnection.Rows)
            {
                int connectionId = Int32.Parse(row.Cells["id"].Value.ToString());
                if (states.ContainsKey(connectionId))
                {
                    row.Cells["Select"].Value = states[connectionId];
                    SelectedExportConnectionId = connectionId;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Používá se v určitém případě v ControlTargetVariable při btnPrevious_Click, kdy je potřeba v ControlConnection zaškrtnout připojení, ke kterému je uživatel připojen, a povolit tlačítka</para>
        /// <para lang="en">In a specific case in ControlTargetVariable during btnPrevious_Click, it is necessary in ControlConnection to check the connection to which the user is connected and to enable the buttons</para>
        /// </summary>
        /// <param name="connectionId">
        /// <para lang="cs">Identifikátor připojení, který se má ověřit v DataGridView.</para>
        /// <para lang="en">The connection identifier to be verified in the DataGridView.</para>
        /// </param>
        public void CheckConnection(int connectionId)
        {
            foreach (DataGridViewRow row in dgvConnection.Rows)
            {
                if (row.Cells["id"].Value != null && Convert.ToInt32(row.Cells["id"].Value) == connectionId)
                {
                    row.Cells["Select"].Value = true;
                    break;
                }

                SelectedExportConnectionId = connectionId;
            }

            btnCheckAdSp.Enabled = true;
            btnCheckMetadata.Enabled = true;
            btnNext.Enabled = true;
        }

        #endregion Methods


        #region Events

        #region NextClick Event

        /// <summary>
        /// <para lang="cs">Delegát funkce obsluhující událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">The delegate of the function serving the click event on the "Next" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        public delegate void NextClickHandler(object sender, EventArgs e);

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event NextClickHandler NextClick;

        /// <summary>
        /// <para lang="cs">Vyvolání události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Invoking a click event on the "Next" button</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaiseNextClick(object sender, EventArgs e)
        {
            NextClick?.Invoke(sender, e);
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Handling the "Next" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            if (ControlOwner is ControlTargetVariable)
            {
                controlTargetVariable = new ControlTargetVariable(
                controlOwner: this,
                selectedExportConnectionId: SelectedExportConnectionId)
                {
                    Dock = DockStyle.Fill
                };
                controlTargetVariable.LoadContent();
                controlTargetVariable.InitializeLabels();
                CtrEtl?.ShowControl(control: controlTargetVariable);
            }
            else
            {
                RaiseNextClick(sender, e);
            }

            Cursor = Cursors.Default;
        }

        #endregion NextClick Event

        #endregion Events


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Vytvořit nové databázové připojení"</para>
        /// <para lang="en">Handling the "Create a new database connection" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnConnectionAdd_Click(object sender, EventArgs e)
        {
            FormConnectionNew frm = new FormConnectionNew(controlOwner: this);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                int? newConnectionId = frm.NewConnectionId; // Save NewConnectionId
                LoadContent();
                UpdateCheckboxForNewConnection(newConnectionId); // Update checkbox

                btnNext.Enabled = true;
                btnCheckMetadata.Enabled = true;
                btnCheckAdSp.Enabled = true;
                CtrEtl.lblStatusExportConnection.Visible = true;
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Editovat vybrané databázové připojení"</para>
        /// <para lang="en">Handling the "Edit the selected database connection" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnConnectionUpdate_Click(object sender, EventArgs e)
        {
            if (DgvConnection.SelectedRows.Count == 0)
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

                // Není vybráno žádné připojení
                // No connection is selected
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoConnectionSelected") ?
                            messages["msgNoConnectionSelected"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            FormConnectionEdit frm = new FormConnectionEdit(
                controlOwner: this,
                _SelectedExportConnectionId: SelectedExportConnectionId,
                _SelectedExportConnectionHost: SelectedExportConnectionHost,
                _SelectedExportConnectionDbName: SelectedExportConnectionDbName,
                _SelectedExportConnectionPort: SelectedExportConnectionPort,
                _SelectedExportConnectionDescription: SelectedExportConnectionComment);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                // Save the states of checked checkboxes before reloading the content
                var checkedConnections = SaveCheckedConnectionsStates();

                LoadContent();

                // Restore the states of checked checkboxes
                RestoreCheckedConnectionsStates(checkedConnections);
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Smazat vybrané databázové připojení"</para>
        /// <para lang="en">Handling the "Delete the selected database connection" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripButton)</para>
        /// <para lang="en">Object that sends the event (ToolStripButton)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnConnectionDelete_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> messages = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            if (DgvConnection.SelectedRows.Count == 0)
            {
                // Není vybráno žádné připojení
                // No connection is selected
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoConnectionSelected") ?
                            messages["msgNoConnectionSelected"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            int? result = TDFunctions.FnEtlTryDeleteExportConnection.Execute(
              database: Database,
              exportConnectionId: SelectedExportConnectionId);

            if (result == 0)
            {
                TDFunctions.FnEtlDeleteExportConnection.Execute(
                    database: Database,
                    exportConnectionId: SelectedExportConnectionId);

                LoadContent();

                btnCheckMetadata.Enabled = false;
                btnCheckAdSp.Enabled = false;
                btnNext.Enabled = false;
                CtrEtl.lblStatusExportConnection.Visible = false;
            }
            else if (result != null)
            {
                DialogResult dialogResult = MessageBox.Show(
                                        text: messages.ContainsKey(key: "msgDeleteSelectedConnection") ?
                                                messages["msgDeleteSelectedConnection"] : String.Empty,
                                        caption: String.Empty,
                                        buttons: MessageBoxButtons.YesNo,
                                        icon: MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                {
                    TDFunctions.FnEtlDeleteExportConnection.Execute(
                        database: Database,
                        exportConnectionId: SelectedExportConnectionId);

                    LoadContent();

                    btnCheckMetadata.Enabled = false;
                    btnCheckAdSp.Enabled = false;
                    btnNext.Enabled = false;
                    CtrEtl.lblStatusExportConnection.Visible = false;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Kontrola metadat"</para>
        /// <para lang="en">Handling the "Check metadata" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCheckMetadata_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> messages = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            TDFnEtlGetTargetVariableTypeList result =
                TDFunctions.FnEtlGetTargetVariable.Execute(
                    database: Database,
                    exportConnection: selectedExportConnectionId,
                    nationalLanguage: nationalLang.NatLang,
                    etl: true);

            if (result.Items.Count == 0)
            {
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoEntry") ?
                            messages["msgNoEntry"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
            else
            {
                FormEtledTargetVariables form =
                    new FormEtledTargetVariables(
                        controlOwner: this,
                        selectedExportConnectionId: SelectedExportConnectionId);

                if (form.ShowDialog() == DialogResult.OK)
                {

                }
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Kontrola pl. domén a subpopulací"</para>
        /// <para lang="en">Handling the "Check area domains ans subpopulations" click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void btnCheckAdSp_Click(object sender, EventArgs e)
        {
            string etledAD = TDFunctions.FnEtlGetAreaDomainsForUpdate.Execute(Database, SelectedExportConnectionId);
            string etledSP = TDFunctions.FnEtlGetSubPopulationsForUpdate.Execute(Database, SelectedExportConnectionId);
            string etledADC = TDFunctions.FnEtlGetAreaDomainCategoriesForAreaDomains.Execute(Database, SelectedExportConnectionId);
            string etledSPC = TDFunctions.FnEtlGetSubPopulationCategoriesForSubPopulations.Execute(Database, SelectedExportConnectionId);

            Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);

            if (string.IsNullOrEmpty(etledAD) && string.IsNullOrEmpty(etledSP)
                && string.IsNullOrEmpty(etledSPC) && string.IsNullOrEmpty(etledADC))
            {
                MessageBox.Show(
                   text: messages.ContainsKey(key: "msgNoEntryForAdAndSp") ?
                           messages["msgNoEntryForAdAndSp"] : String.Empty,
                   caption: String.Empty,
                   buttons: MessageBoxButtons.OK,
                   icon: MessageBoxIcon.Information);
            }
            else
            {
                if (!string.IsNullOrEmpty(etledAD))
                {
                    diffAD = NfiEstaFunctions.FnEtlCheckAreaDomainsForUpdate.ExecuteQuery(
                        FormUserPassword.TargetDatabase,
                        etledAD);
                }

                if (!string.IsNullOrEmpty(etledSP))
                {
                    diffSP = NfiEstaFunctions.FnEtlCheckSubPopulationsForUpdate.ExecuteQuery(
                        FormUserPassword.TargetDatabase,
                        etledSP);
                }

                if (!string.IsNullOrEmpty(etledADC))
                {
                    diffADC = NfiEstaFunctions.FnEtlCheckAreaDomainCategoriesForAreaDomains.Execute(
                        FormUserPassword.TargetDatabase,
                        etledADC);
                }

                if (!string.IsNullOrEmpty(etledSPC))
                {
                    diffSPC = NfiEstaFunctions.FnEtlCheckSubPopulationCategoriesForSubPopulations.Execute(
                        FormUserPassword.TargetDatabase,
                        etledSPC);
                }

                if (diffAD.Rows.Count == 0 && diffSP.Rows.Count == 0 && diffADC == null && diffSPC == null)
                {
                    MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoEntryForAdAndSp") ?
                            messages["msgNoEntryForAdAndSp"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                }
                else
                {
                    FormCheckAdSp form = new FormCheckAdSp(
                    controlOwner: this,
                    selectedExportConnectionId: SelectedExportConnectionId);

                    if (form.ShowDialog() == DialogResult.OK)
                    {

                    }
                }
            }
        }

        #endregion Event Handlers

    }

}
