﻿//
// Copyright 2020, 2023 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Controls;
using ZaJi.NfiEstaPg.Core;
using ZaJi.NfiEstaPg.TargetData;
using ZaJi.PostgreSQL;

namespace Hanakova.ModuleEtl
{

    /// <summary>
    /// <para lang="cs">Formulář pro volbu cílových proměnných, které již mají id v cílové databázi</para>
    /// <para lang="en">Form to select target variables that already have got ids in the target database</para>
    /// </summary>
    public partial class FormEtledTargetVariables
            : Form, INfiEstaControl, IETLControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Národní jazyk</para>
        /// <para lang="en">National language</para>
        /// </summary>
        private NationalLang nationalLang;

        /// <summary>
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </summary>
        private readonly int _selectedExportConnectionId;

        /// <summary>
        /// <para lang="cs">Seznam cílových proměnných</para>
        /// <para lang="en">List of target variables</para>
        /// </summary>
        private TDFnEtlGetTargetVariableTypeList tdFnEtlGetTargetVariableList;

        /// <summary>
        /// <para lang="cs">Jazyková verze použitá pouze lokálně</para>
        /// <para lang="en">Language version used only locally</para>
        /// </summary>
        private LanguageVersion _localLanguageVersion;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře pro volbu cílových proměnných, které již mají id v cílové databázi</para>
        /// <para lang="en">Constructor of a form to select target variables that already have got ids in the target database</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        /// <param name="selectedExportConnectionId">
        /// <para lang="cs">Id zvoleného databázového připojení, vybrané na 1. formuláři se seznamem připojení</para>
        /// <para lang="en">Selected database connection id, which is selected on the 1st form with the list of connections</para>
        /// </param>
        public FormEtledTargetVariables(
            Control controlOwner,
            int selectedExportConnectionId)
        {
            InitializeComponent();

            ControlOwner = controlOwner;

            _localLanguageVersion = CtrEtl.LanguageVersion;
            _selectedExportConnectionId = selectedExportConnectionId;

            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(controlOwner is ControlConnection))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlConnection.",
                        paramName: "controlOwner");
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument controlOwner must not be null.",
                        paramName: "controlOwner");
                }

                if (!(value is ControlConnection))
                {
                    throw new ArgumentException(
                        message: "Argument controlOwner must be type of ControlConnection.",
                        paramName: "controlOwner");
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((ControlConnection)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            //get
            //{
            //    return ((ControlConnection)ControlOwner).LanguageVersion;
            //}
            get { return _localLanguageVersion; }
            set { _localLanguageVersion = value; }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((ControlConnection)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro ETL</para>
        /// <para lang="en">Module for the ETL</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((ControlConnection)ControlOwner).Setting;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for the ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrEtl
        {
            get
            {
                if (ControlOwner is ControlConnection ctrConnection)
                {
                    return ctrConnection.CtrEtl;
                }
                else
                {
                    throw new ArgumentException(
                            message: "Invalid ControlOwner type.",
                            paramName: "ControlOwner");
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Zvolená cílová proměnná</para>
        /// <para lang="en">The selected target variable</para>
        /// </summary>
        public TDFnEtlGetTargetVariableMetadata SelectedTargetVariable { get; set; }

        #endregion Properties


        #region Private Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Control "Target variable selection from json file of metadata"</para>
        /// </summary>
        private ControlTargetVariableSelector CtrTargetVariableSelector
        {
            get
            {
                if (pnlTargetVariableSelector.Controls.Count != 1)
                {
                    CreateNewTargetVariableSelector();
                }
                if (!(pnlTargetVariableSelector.Controls[0] is ControlTargetVariableSelector))
                {
                    CreateNewTargetVariableSelector();
                }
                return (ControlTargetVariableSelector)pnlTargetVariableSelector.Controls[0];
            }
        }

        #endregion Private Controls


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            switch (languageVersion)
            {
                case LanguageVersion.National:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                 "Volba cílové proměnné, která již byla přenesena do cílové DB a jejíž údaje jsou aktuální" },
                            { "tsmiLanguage",               "Změnit jazyk" },
                            { "lblMainCaption",             String.Empty },
                            { "ctrMetadataIndicatorText",   "Indikátor:" },
                            { "ctrMetadataElementsText",    "Metadata:" },
                            { "btnCheckSelected",           "Kontrola zvolené" },
                            { "btnCheckAll",                "Kontrola všech" },
                            { "msgNoRecord",                "Metadata jsou v pořádku." }
                    } :
                    languageFile.NationalVersion.Data.ContainsKey(key: "FormEtledTargetVariables") ?
                    languageFile.NationalVersion.Data["FormEtledTargetVariables"] :
                    new Dictionary<string, string>();

                case LanguageVersion.International:
                    return
                    (languageFile == null) ?
                    new Dictionary<string, string>()
                    {
                            { "frmCaption",                 "Selection of a target variable that has already been transferred to the target DB and its data is current" },
                            { "tsmiLanguage",               "Change language"},
                            { "lblMainCaption",             String.Empty },
                            { "ctrMetadataIndicatorText",   "Indicator:" },
                            { "ctrMetadataElementsText",    "Metadata:" },
                            { "btnCheckSelected",           "Check selected" },
                            { "btnCheckAll",                "Check all" },
                            { "msgNoRecord",                "The metadata is OK." }
                    } :
                    languageFile.InternationalVersion.Data.ContainsKey(key: "FormEtledTargetVariables") ?
                    languageFile.InternationalVersion.Data["FormEtledTargetVariables"] :
                    new Dictionary<string, string>();

                default:
                    return
                    new Dictionary<string, string>();
            }
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře pro volbu cílových proměnných, které již mají id v cílové databázi</para>
        /// <para lang="en">Initializing a form to select target variables that already have got ids in the target database</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            nationalLang = new NationalLang();

            InitializeLabels();

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře pro volbu cílových proměnných, které již mají id v cílové databázi</para>
        /// <para lang="en">Initializing labels of a form to select target variables that already have got ids in the target database</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text =
               labels.ContainsKey(key: "frmCaption") ?
               labels["frmCaption"] : String.Empty;

            tsmiLanguage.Text =
                labels.ContainsKey(key: "tsmiLanguage") ?
                labels["tsmiLanguage"] : String.Empty;

            lblMainCaption.Text =
                labels.ContainsKey(key: "lblMainCaption") ?
                labels["lblMainCaption"] : String.Empty;

            CtrTargetVariableSelector.MetadataIndicatorText =
                labels.ContainsKey(key: "ctrMetadataIndicatorText") ?
                labels["ctrMetadataIndicatorText"] : String.Empty;

            CtrTargetVariableSelector.MetadataElementsText =
                labels.ContainsKey(key: "ctrMetadataElementsText") ?
                labels["ctrMetadataElementsText"] : String.Empty;

            CtrTargetVariableSelector.InitializeLabels();

            btnCheckSelected.Text =
               labels.ContainsKey(key: "btnCheckSelected") ?
               labels["btnCheckSelected"] : String.Empty;

            btnCheckAll.Text =
               labels.ContainsKey(key: "btnCheckAll") ?
               labels["btnCheckAll"] : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            tdFnEtlGetTargetVariableList =
                TDFunctions.FnEtlGetTargetVariable.Execute(
                    database: Database,
                    exportConnection: _selectedExportConnectionId,
                    nationalLanguage: nationalLang.NatLang,
                    etl: true);

            CtrTargetVariableSelector.Data = tdFnEtlGetTargetVariableList;

            CtrTargetVariableSelector.LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nový ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
        /// <para lang="en">Creates new control "Target variable selection from json file of metadata"</para>
        /// </summary>
        private void CreateNewTargetVariableSelector()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            pnlTargetVariableSelector.Controls.Clear();

            ControlTargetVariableSelector ctrTargetVariableSelector =
                new ControlTargetVariableSelector(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    Design = TargetVariableSelectorDesign.Standard,
                    Spacing = new List<int> { 5 },
                    SplitterDistance = Setting.SplIndicatorDistance,
                    MetadataIndicatorText =
                        labels.ContainsKey(key: "ctrMetadataIndicatorText") ?
                        labels["ctrMetadataIndicatorText"] : String.Empty,
                    MetadataElementsText =
                        labels.ContainsKey(key: "ctrMetadataElementsText") ?
                        labels["ctrMetadataElementsText"] : String.Empty
                };

            Label lbl = new Label();
            lbl.CreateControl();
            ctrTargetVariableSelector.AdditionalControls.Add(item: lbl);

            ComboBox cmbIdTargetVariable = new ComboBox()
            {
                Size = new Size(20, 20),
            };
            cmbIdTargetVariable.CreateControl();
            ctrTargetVariableSelector.AdditionalControls.Add(item: cmbIdTargetVariable);

            cmbIdTargetVariable.SelectedIndexChanged += new EventHandler(CmbIdTargetVariable_SelectedIndexChanged);

            ctrTargetVariableSelector.SelectedTargetVariableChanged +=
                new EventHandler((sender, e) =>
                {
                    SelectedTargetVariableChanged?.Invoke(
                       sender: this,
                       e: new EventArgs());
                    lbl.Text = "";
                    cmbIdTargetVariable.DataSource = ctrTargetVariableSelector.SelectedTargetVariableIdentifiers;
                });

            ctrTargetVariableSelector.SplitterMoved +=
                new EventHandler((sender, e) =>
                {
                    Setting.SplIndicatorDistance = ctrTargetVariableSelector.SplitterDistance;
                });

            ctrTargetVariableSelector.CreateControl();
            pnlTargetVariableSelector.Controls.Add(
                value: ctrTargetVariableSelector);
        }

        #endregion Methods


        #region Event Handlers

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Změnit jazyk"</para>
        /// <para lang="en">Handling the "Change language" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ToolStripMenuItem)</para>
        /// <para lang="en">Object that sends the event (ToolStripMenuItem)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void TsmiLanguage_Click(object sender, EventArgs e)
        {
            //CtrEtl.LanguageVersion = (LanguageVersion == LanguageVersion.International) ?
            //            LanguageVersion.National :
            //            LanguageVersion.International;
            LanguageVersion = (LanguageVersion == LanguageVersion.International) ?
               LanguageVersion.National :
               LanguageVersion.International;
            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Obsluha události změna výběru cílové proměnné
        /// v ComboBoxu cmbIdTargetVariable</para>
        /// <para lang="en">Event handler of the change of target variable
        /// selection in the cmbIdTargetVariable ComboBox</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ComboBox)</para>
        /// <para lang="en">Object that sends the event (ComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void CmbIdTargetVariable_SelectedIndexChanged(object sender, EventArgs e)
        {
            TDFnEtlGetTargetVariableTypeList tdFnEtlGetTargetVariableListAll =
                TDFunctions.FnEtlGetTargetVariable.Execute(
                    database: Database,
                    exportConnection: _selectedExportConnectionId,
                    nationalLanguage: nationalLang.NatLang,
                    etl: true,
                    targetVariable: CtrTargetVariableSelector.SelectedTargetVariableIdentifier);

            TDFnEtlGetTargetVariableMetadataList tdFnEtlGetTargetVariablesExtendedList =
                new TDFnEtlGetTargetVariableMetadataList(
                    database: Database);
            tdFnEtlGetTargetVariablesExtendedList.ReLoad(
                fnEtlGetTargetVariable: tdFnEtlGetTargetVariableListAll);

            TDFnEtlGetTargetVariableMetadataList tdFnEtlGetTargetVariableExtendedListAggregated =
                tdFnEtlGetTargetVariablesExtendedList.Aggregated();

            cmbIdEtlTargetVariable.ValueMember = "id_etl_target_variable";
            cmbIdEtlTargetVariable.DisplayMember = "id_etl_target_variable";
            cmbIdEtlTargetVariable.DataSource = tdFnEtlGetTargetVariableExtendedListAggregated.Data;
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Kontrola vybrané"</para>
        /// <para lang="en">Handling the "Check selected" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCheckSelected_Click(object sender, EventArgs e)
        {
            List<int?> etlIds = new List<int?>() { (int?)cmbIdEtlTargetVariable.SelectedValue };

            string metadata =
                TDFunctions.FnEtlGetTargetVariableMetadatas.Execute(
                    database: Database,
                    etlTargetVariableIds: etlIds,
                    nationalLanguage: nationalLang.NatLang);

            DataTable dt = NfiEstaFunctions.FnEtlCheckTargetVariableMetadatas.ExecuteQuery(
                database: FormUserPassword.TargetDatabase,
                metadatas: metadata,
                nationalLanguage: nationalLang.NatLang,
                metadataDiff: true);

            if (dt.AsEnumerable().Any())
            {
                FormEtledTVCheckMetadata form =
                    new FormEtledTVCheckMetadata(
                        controlOwner: this,
                        etlIds: etlIds);

                if (form.ShowDialog() == DialogResult.OK)
                {

                }
            }
            else
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoRecord") ?
                            messages["msgNoRecord"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Obsluha události kliknutí na tlačítko "Kontrola všech"</para>
        /// <para lang="en">Handling the "Check all" button click event</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (Button)</para>
        /// <para lang="en">Object that sends the event (Button)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void BtnCheckAll_Click(object sender, EventArgs e)
        {
            List<int?> etlIds = tdFnEtlGetTargetVariableList.Items
                .Select(a => ((TDFnEtlGetTargetVariableType)a).IdEtlTargetVariableMetadata)
                .ToList<int?>();

            string metadata = TDFunctions.FnEtlGetTargetVariableMetadatas.Execute(
                database: Database,
                etlTargetVariableIds: etlIds,
                nationalLanguage: nationalLang.NatLang);

            DataTable dt = NfiEstaFunctions.FnEtlCheckTargetVariableMetadatas.ExecuteQuery(FormUserPassword.TargetDatabase, metadata, nationalLang.NatLang, true);

            if (dt.AsEnumerable().Any())
            {
                FormEtledTVCheckMetadata form = new FormEtledTVCheckMetadata(
                    controlOwner: this,
                    etlIds: etlIds);

                if (form.ShowDialog() == DialogResult.OK)
                {

                }
            }
            else
            {
                Dictionary<string, string> messages = Dictionary(
                   languageVersion: LanguageVersion,
                   languageFile: LanguageFile);
                MessageBox.Show(
                    text: messages.ContainsKey(key: "msgNoRecord") ?
                            messages["msgNoRecord"] : String.Empty,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                Close();
            }
        }

        /// <summary>
        /// <para lang="cs">Událost "Změna vybraného indikátoru"</para>
        /// <para lang="en">"Selected indicator changed" event</para>
        /// </summary>
        public event EventHandler SelectedTargetVariableChanged;

        #endregion Event Handlers
    }

}
